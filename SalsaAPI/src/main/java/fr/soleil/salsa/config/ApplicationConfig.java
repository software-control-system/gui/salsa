package fr.soleil.salsa.config;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;

import fr.soleil.salsa.exception.ConfigurationException;
import fr.soleil.salsa.exception.LocatorException;
import fr.soleil.salsa.locator.ILocator;
import fr.soleil.salsa.locator.LocatorFactory;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * Reads the configuration for the application.<br>
 * <br>
 * The configuration file is : [api-config.properties].<br>
 * The locatorConfigFile property of this file is the name of the configuration file for the locator to be used.
 * It allows the use of local or remote business execution.
 * 
 * @author Francois Denhez
 * @author Etienne Boutry
 */
public class ApplicationConfig {

    public static final Logger LOGGER = LoggingUtil.getLogger(ApplicationConfig.class);

    /**
     * Name of the configuration file.
     */
    public static final String CONFIG_FILE_NAME = "api-config";

    /**
     * Name of the property for the configuration file of the locator?
     */
    private static final String LOCATOR_CONFIG_FILE_KEY = "locatorConfigFile";

    /**
     * The configuration file.
     */
    private static ResourceBundle configFile = null;

    /**
     * This class only has static metods and must not be instantiated : private constructor.
     */
    private ApplicationConfig() {
    }

    /**
     * Returns the locator, using the locator configuration file specified by the locatorConfigFile
     * property in the main configuration file.
     * @throws ConfigurationException
     */
    public static ILocator getLocator() throws ConfigurationException {
        loadConfigFile();
        String locatorFileName;
        try {
            locatorFileName = configFile.getString(LOCATOR_CONFIG_FILE_KEY);
            LOGGER.debug("Initialisation du locator {}", locatorFileName);
        }
        catch(MissingResourceException e) {
            String errorMessage = "Cannot read key " + LOCATOR_CONFIG_FILE_KEY + " in " + CONFIG_FILE_NAME
            + ".properties file " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConfigurationException(errorMessage);
        }

        return getLocator(locatorFileName);
    }


    /**
     * Returns the locator, using the locator configuration file specified by the locatorFileName parameter.
     * Prefer the use of the getLocator() (no parameter) method over this one.
     * @see ApplicationConfig#getLocator()
     * @throws ConfigurationException
     */
    public static ILocator getLocator(String locatorFileName) throws ConfigurationException {
        ILocator locator;
        try {
            locator = LocatorFactory.getLocator(locatorFileName);
            LOGGER.debug("Initialisation du locator {}", locator.getClass().getCanonicalName());
        }
        catch(LocatorException e) {
            String errorMessage = "Cannot create service locator see " + locatorFileName + " " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConfigurationException(errorMessage, e);
        }
        return locator;

    }

    /**
     * Loads the main configuration file.
     * @throws ConfigurationException
     */
    private static void loadConfigFile() throws ConfigurationException {
        if(configFile == null) {
            try {
                configFile = ResourceBundle.getBundle(CONFIG_FILE_NAME);
                LOGGER.debug("Load config file {}", CONFIG_FILE_NAME);
            }
            catch(MissingResourceException e) {
                String errorMessage = "Cannot find configuration file " + CONFIG_FILE_NAME + ".properties "
                + e.getMessage();
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new ConfigurationException(errorMessage, e);
            }
        }
    }
}

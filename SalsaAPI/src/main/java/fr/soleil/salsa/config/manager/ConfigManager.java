package fr.soleil.salsa.config.manager;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.api.ConfigApi;
import fr.soleil.salsa.copier.AutoCopier;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.exception.ScanNotFoundException;

public class ConfigManager {

    private final Map<Integer, IConfig<?>> configMap;

    private String scanServerName;

    public ConfigManager() {
        configMap = new HashMap<Integer, IConfig<?>>();
        scanServerName = null;
    }

    public IConfig<?> getConfig(Integer id) {
        IConfig<?> config;
        synchronized (configMap) {
            config = configMap.get(id);
            if (config == null) {
                try {
                    config = loadConfig(id);
                }
                catch (ScanNotFoundException e) {
                    config = null;
                }
            }
        }
        return config;
    }

    public IConfig<?> getConfigByPath(String path, IConfig<?> lastLoadedConfig) {
        IConfig<?> config = null;
        if (path != null) {
            try {
                config = ConfigApi.getConfigByPath(path, lastLoadedConfig);
            }
            catch (ScanNotFoundException e) {
                config = null;
            }
        }
        if (config != null) {
            config = getConfig(config.getId());
        }
        return config;
    }

    /**
     * The configuration are not fully loaded when the configuration is first created to save time.
     * This loads the full configuration.
     * 
     * @param config
     * @return
     */
    public IConfig<?> loadConfig(Integer id) throws ScanNotFoundException {
        IConfig<?> newConfig = null;
        if (id != null) {
            newConfig = wrap(ConfigApi.getConfigById(id));
        }
        if ((newConfig != null) && (newConfig.getId() != null)) {
            synchronized (configMap) {
                configMap.put(id, newConfig);
            }
        }
        return newConfig;
    }

    /**
     * Wraps a config, choosing the right type automatically.
     * 
     * @param config
     * @return
     */
    private IConfig<?> wrap(IConfig<?> configImpl) {
        IConfig<?> cm = AutoCopier.copy(configImpl);
        return cm;
    }

    public String getScanServerName() {
        return scanServerName;
    }

    public void setScanServerName(String scanServerName) {
        if (!ObjectUtils.sameObject(scanServerName, this.scanServerName)) {
            this.scanServerName = scanServerName;
            synchronized (configMap) {
                configMap.clear();
            }
        }
    }

    public void clean() {
        synchronized (configMap) {
            configMap.clear();
        }
    }

    public void forgetConfig(Integer id) {
        if (id != null) {
            synchronized (configMap) {
                configMap.remove(id);
            }
        }
    }

}

package fr.soleil.salsa.locator.tool;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cette classe contient une map associant une classe et son instace
 * 
 * @author Francois Denhez
 */
public class LocatorServicesCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocatorServicesCache.class);

    private Map<Object, Object> cache;

    public LocatorServicesCache() {
        super();

        initCache();
    }

    /**
     * On recupere un objet du cache
     * 
     * @param <T>
     * @param objectClass la classe de l'objet dont on desire l'instance
     * @return un instance de T
     */
    @SuppressWarnings("unchecked")
    public <T> T retrieve(Class<T> objectClass) {
        assert objectClass != null;

        T instance = null;

        if (cache != null && cache.containsKey(objectClass)) {
            instance = (T) cache.get(objectClass);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Recuperation en cache d'une instance de " + objectClass);
            }
        }

        return instance;
    }

    /**
     * On met un objet dans le cache
     */
    public synchronized void put(Object object) {
        assert object != null;

        if (cache == null) {
            initCache();
        }

        cache.put(object.getClass(), object);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Mise en cache d'une instance de " + object.getClass());
        }
    }

    /**
     * On supprime un objet du cache
     */
    public synchronized void remove(Class<?> objectClass) {
        assert objectClass != null;

        if (cache == null) {
            initCache();
        }

        cache.remove(objectClass);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(objectClass + " a ete retire du cache");
        }
    }

    /**
     * On vide l'ensemble du cache
     */
    public synchronized void clearCache() {
        if (cache != null) {
            cache.clear();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Cache vide");
            }
        }
    }

    /**
     * Initialisation du cache
     */
    private void initCache() {
        cache = new HashMap<Object, Object>();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Initialisation du cache");
        }
    }
}

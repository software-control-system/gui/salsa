package fr.soleil.salsa.locator;

import fr.soleil.salsa.exception.LocatorException;

/**
 * The interface that must be implemented by locators.<br />
 * A locator retrieves the service implementation to be used from its interface.<br />
 * On top of implementing this interface, a locator must have a constructor with the following parameters :<br />
 * - a String : its configuration file name.<br />
 * - a ResourceBundle : its configuration file.<br />
 * - a locator : a locator must redirect all requests it cannot solve towards this default locator.
 * This last parameter might be null.<br />
 * <br />
 * @author Francois Denhez
 */
public interface ILocator {

    /**
     * This method provides the implementation of the interface passed as parameter.
     * 
     * @param <T> The type we seek the implementation of.
     * @param interfaceClass The class of the interface we seek the implementation of.
     * @return an implementation of the interface.
     */
    public <T> T getService(Class<T> interfaceClass) throws LocatorException;
}

package fr.soleil.salsa.locator;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;

import fr.soleil.salsa.exception.LocatorException;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * Loads the locator to be used according to the configuration file.
 * 
 * @author Etienne Boutry
 */
public class LocatorFactory {

    public static final Logger LOGGER = LoggingUtil.getLogger(LocatorFactory.class);

    /**
     * Key for the class of the locator.
     */
    private static final String LOCATOR_CLASS_KEY = "locatorClass";

    /**
     * Key for the optional locator configuration file for a default locator, that will be used if
     * this locator does not have the key for a service. Cle pour le locator par defaut a utiliser
     * si le fichier de configuration en cours ne contient pas la cle d'un service.
     */
    private static final String DEFAULT_LOCATOR_KEY = "defaultTo";

    /**
     * Cache for the locators. Contains all the instanciated locators, using the name of their
     * configuration file as a key.
     */
    private static Map<String, ILocator> locatorMap = new HashMap<String, ILocator>();

    /**
     * LocatorFactory only has static methods and must not be instanciated : private constructor.
     */
    private LocatorFactory() {
    }

    /**
     * Returns the locator, using the locator configuration file specified by the configFileName
     * parameter. If the locator has already been loaded, this method recovers it from the cache.
     * Otherwise, it creates a new one.
     * 
     * @param configFileName
     * @return
     */
    public static ILocator getLocator(String configFileName) throws LocatorException {
        assert configFileName != null;
        ILocator locator = locatorMap.get(configFileName);
        if (locator == null) {
            locator = createLocator(configFileName);
            locatorMap.put(configFileName, locator);
        }
        return locator;
    }

    /**
     * Creates a new locator, using the configFileName parameter to find the configuration file.
     * 
     * @param configFileName
     * @return
     * @throws LocatorException
     */
    @SuppressWarnings("unchecked")
    private static ILocator createLocator(String configFileName) throws LocatorException {
        // Fichier de configuration.
        ResourceBundle configFile;
        try {
            configFile = ResourceBundle.getBundle(configFileName);
        } catch (MissingResourceException e) {
            String errorMessage = "Configuration file " + configFileName + ".properties not found " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new LocatorException(errorMessage, e);
        }

        // Classe du locator.
        String locatorClassName = configFile.getString(LOCATOR_CLASS_KEY);
        Class<? extends ILocator> locatorClass;
        try {
            locatorClass = (Class<? extends ILocator>) Class.forName(locatorClassName);
        } catch (ClassNotFoundException e) {
            String errorMessage = "Locator class " + locatorClassName + " not found. Check " + configFileName
            + ".properties " + e.getMessage();
            LOGGER.error(errorMessage);
            throw new LocatorException(errorMessage, e);
        }

        // Locator par defaut.
        ILocator defaultLocator;
        try {
            String defaultFileName = configFile.getString(DEFAULT_LOCATOR_KEY);
            if ((defaultFileName == null) || "".equals(defaultFileName.trim())) {
                defaultLocator = null;
            } else if (defaultFileName.equals(configFileName)) {
                String errorMessage = "Configuration file " + configFileName + " gives itself as its own default.";
                LOGGER.error(errorMessage);
                LocatorException locatorException = new LocatorException(errorMessage);
                LOGGER.debug("Stack trace", locatorException);
                throw locatorException;
            } else {
                defaultLocator = getLocator(defaultFileName);
                String infoMessage = "Initialization of the locator " + defaultLocator.getClass().getCanonicalName()
                + " as default locator for " + locatorClass.getCanonicalName();
                LOGGER.debug(infoMessage);
            }
        } catch (MissingResourceException e) {
            // Ce n'est pas une erreur : avoir un locator par defaut est optionnel.
            defaultLocator = null;
            LOGGER.warn("Default locator not found {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        // Constructeur du locator.
        Constructor<? extends ILocator> locatorConstructor;
        try {
            locatorConstructor = locatorClass.getConstructor(String.class, ResourceBundle.class, ILocator.class);
        } catch (SecurityException e) {
            String errorMessage = "Access to the constructor of " + locatorClassName + " with parameters is forbidden "
            + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new LocatorException(errorMessage, e);

        } catch (NoSuchMethodException e) {
            String errorMessage = "Constructor of " + locatorClassName + " with parameters not found " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new LocatorException(errorMessage, e);
        }

        // Instanciation du locator.
        ILocator locator;
        try {
            locator = locatorConstructor.newInstance(configFileName, configFile, defaultLocator);
            LOGGER.debug("Initialization of the locator " + locator.getClass().getCanonicalName()
                    + " as primary locator.");
        } catch (Exception e) {
            String errorMessage = "Cannot initialize the locator " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new LocatorException(errorMessage, e);
        }
        return locator;
    }
}

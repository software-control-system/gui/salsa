package fr.soleil.salsa.locator.tool;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import fr.soleil.salsa.exception.LocatorException;
import fr.soleil.salsa.locator.ILocator;

/**
 * Abstract class with basice tools for the implementation of a locator. A locator does not have to
 * extend this class, that is provided as convenience/ It does have to implement ILocator.
 * 
 * To use this class, one can either : - redefine getService(Class, String), - redefine
 * getService(Class). getService(Class) takes the class of the interface of the service to be
 * returned as parameter. getService(Class, String) takes the configuration key whose value is the
 * class of the interface of the service to be returned as parameter.
 * 
 * @author Etienne Boutry
 * 
 */
public abstract class ABaseLocator implements ILocator {

    /**
     * Name of the configuration file.
     */
    protected String configFileName;

    /**
     * Configuration file.
     */
    protected ResourceBundle configFile;

    /**
     * Default locator, for use when the configuration file has no key for the service that is
     * demanded.
     */
    protected ILocator defaultLocator;

    /**
     * Cache.
     */
    protected LocatorServicesCache cache;

    /**
     * Constructor
     * 
     * @param configFileName the name of the configuration file.
     * @param configFile the configuration file.
     * @param defaultLocator the default locator, for use when the configuration file has no key for
     *            the service.
     */
    public ABaseLocator(String configFileName, ResourceBundle configFile, ILocator defaultLocator) {
        assert configFile != null : "The locator cannot be instantiated without a ResourceBundle";
        assert configFileName != null : "The name of the configuration file is missing.";

        this.configFileName = configFileName;
        this.configFile = configFile;
        this.defaultLocator = defaultLocator;

        this.cache = new LocatorServicesCache();
    }

    /**
     * Returns the service that implements the given interface.
     * 
     * @param T : the class of the interface of the service.
     * @param interfaceClass : the class of the interface of the service.
     */
    public <T> T getService(Class<T> interfaceClass) throws LocatorException {
        // Concrete implementation of ABaseLocator must either redefine getService(String) or this
        // method.
        // This default implementation reads the configuration file, looking for a key that matches
        // the
        // name of the interface, and sends the result to getService(Class, String)
        assert interfaceClass != null;
        assert interfaceClass.isInterface();

        // Recovers the instance from the cache if it exists.
        T service = cache.retrieve(interfaceClass);

        // If the service is not in the cache, initializes it.
        if (service == null) {
            String configValue;

            try {
                // Reads the configuration key, looking for the key that matches the name of the
                // interface.
                configValue = configFile.getString(interfaceClass.getCanonicalName());

            }
            catch (MissingResourceException e) {
                // A missing value is not an error : it means that we must use the default locator.
                configValue = null;
            }

            if (configValue != null && !"".equals(configValue.trim())) {
                // Calls the getService(Class, String) to attempt to get the service from the value
                // read in the configuration file.
                service = this.getService(interfaceClass, configValue);

                assert (interfaceClass.isAssignableFrom(service.getClass())) : "Error in the locator "
                        + this.getClass().getCanonicalName()
                        + " : the service configured as \""
                        + configValue
                        + "\" does not implement the interface "
                        + interfaceClass.getCanonicalName() + ".";
            }
            else {
                service = this.dispatchToDefault(interfaceClass);
            }

            // Put the service into the cache.
            cache.put(service);
        }

        return service;
    }

    /**
     * Returns the service that implements the given interface, the configuration value being given
     * as the second argument. Renvoie le service en fonction de la valeur lue dans le fichier de
     * configuration.
     * 
     * @param T : the class of the interface of the service.
     * @param interfaceClass : the class of the interface of the service.
     * @param configValue : the value in the configuration file for this service.
     * @return
     * @throws LocatorException
     */
    public <T> T getService(Class<T> interfaceClass, String configValue) throws LocatorException {
        // Concrete implementation of ABaseLocator must either redefine getService(String) or this
        // method.
        // If getService(Class) is redefined, redefining this method is not mandatory, hence why
        // this method is not abstract.
        return null;
    }

    /**
     * When this locator cannot find the service, this method lets it dispatch the request to its
     * default locator.
     * 
     * @param <T>
     * @param interfaceClass
     * @return
     * @throws LocatorException
     */
    @SuppressWarnings("unchecked")
    protected <T> T dispatchToDefault(Class<T> interfaceClass) throws LocatorException {
        if (this.defaultLocator == null) {
            // If there is no default locator and the current locator has no matching key for the
            // service,
            // the service cannot be found.
            throw new LocatorException("The key " + interfaceClass.getCanonicalName()
                    + " could not be found in " + configFileName
                    + ".properties, and there is no default locator : the service "
                    + interfaceClass.getCanonicalName() + " cannot be found.");
        }
        ILocator locator = (ILocator) this.defaultLocator.getService(interfaceClass);
        return (T) locator;
    }

}

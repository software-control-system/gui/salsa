package fr.soleil.salsa.copier.exception;

import fr.soleil.salsa.exception.SalsaException;

public class SalsaCopyException extends SalsaException {

    private static final long serialVersionUID = -4603141631541953696L;

    public SalsaCopyException() {
        super();
    }

    public SalsaCopyException(String message) {
        super(message);
    }

    public SalsaCopyException(Throwable cause) {
        super(cause);
    }

    public SalsaCopyException(String message, Throwable cause) {
        super(message, cause);
    }

}

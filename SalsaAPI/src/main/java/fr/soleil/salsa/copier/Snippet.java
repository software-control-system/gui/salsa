package fr.soleil.salsa.copier;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;

import com.github.dozermapper.core.config.BeanContainer;
import com.github.dozermapper.core.events.Event;
import com.github.dozermapper.core.events.EventListener;
import com.github.dozermapper.core.fieldmap.FieldMap;
import com.github.dozermapper.core.fieldmap.HintContainer;

import fr.soleil.salsa.copier.exception.SalsaCopyException;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.ListModel;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * @author Administrateur
 * 
 */
public class Snippet implements EventListener {

    public static final Logger LOGGER = LoggingUtil.getLogger(Snippet.class);

    private static final String GET = "get";
    private static final String SET = "set";

    private Map<MyReference, ListModel<?, ? extends IEventSource<?>>> dicImplToModel;

    /**
     * Contains mappings of converted objects : the key is the src object and the value the
     * converted object.
     */
    private Map<Object, Object> mappings;
    private boolean transformListToListModel;

    // Workaround in case something goes wrong with dozer
    private static Snippet currentSnippet;

    // Workaround in case something goes wrong with dozer
    public static Snippet getCurrentSnippet() {
        return currentSnippet;
    }

    public Snippet(boolean transformListToListModel) {
        this.transformListToListModel = transformListToListModel;
        currentSnippet = this;
    }

    public Map<Object, Object> getMappings() {
        return mappings;
    }

    public void setMappings(Map<Object, Object> mappings) {
        this.mappings = mappings;
    }

    @Override
    public void onMappingFinished(Event event) {

    }

    @Override
    public void onMappingStarted(Event event) {
        dicImplToModel = new ConcurrentHashMap<>();
        mappings = new ConcurrentHashMap<>();
    }

    private Method getGetMethod(Object o, String fieldName) throws SalsaCopyException {
        try {
            return getAccessporMethod(GET, o, fieldName);
        } catch (Exception e) {
            throw new SalsaCopyException(e.getMessage(), e);
        }
    }

    private Method getSetMethod(Object o, String fieldName) throws SalsaCopyException {
        try {
            return getAccessporMethod(SET, o, fieldName);
        } catch (Exception e) {
            throw new SalsaCopyException(e.getMessage(), e);
        }
    }

    private Method getAccessporMethod(String action, Object o, String fieldName)
            throws SecurityException, NoSuchMethodException {
        char firstChar = fieldName.charAt(0);
        String methodName = action + Character.toString(firstChar).toUpperCase() + fieldName.substring(1);

        Class<?> c = o.getClass();

        Method[] methods = c.getMethods();
        for (Method m : methods) {
            if (m.getName().equals(methodName)) {
                return m;
            }
        }
        return null;
    }

    private Field getField(Class<?> clazz, String fieldName) {
        while (clazz != null) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                if (f.getName().equals(fieldName)) {
                    return f;
                }
            }
            clazz = clazz.getSuperclass();
        }
        return null;

    }

    private Field getField(Object o, String fieldName) throws SalsaCopyException {
        try {
            Class<?> c = o.getClass();
            return getField(c, fieldName);
        } catch (Exception e) {
            throw new SalsaCopyException(e.getMessage(), e);
        }
    }

    @Override
    public void onPostWritingDestinationValue(Event event) {
        // We store the object converted in a map
        if ((event.getDestinationValue() != null) && (event.getDestinationValue() instanceof IActuator)) {
            try {
                // We need to the srcValue
                String fieldName = event.getFieldMap().getSrcFieldName();
                // Field f = getField(event.getDestinationObject(), fieldName);
                Method m = getGetMethod(event.getSourceObject(), fieldName);
                Object srcValue = m.invoke(event.getSourceObject());
                // Storing of the object
                mappings.put(srcValue, event.getDestinationValue());
            } catch (Exception exc) {
                LOGGER.warn("postWritingDestinationValue {}", exc.getMessage());
                LOGGER.debug("Stack trace", exc);
            }
        }

        if (transformListToListModel) {
            try {
                if ((event.getDestinationValue() != null) && (event.getDestinationValue() instanceof List<?>)) {
                    List<?> dValue = (List<?>) event.getDestinationValue();
                    Object destObject = event.getDestinationObject();
                    if (destObject instanceof IObjectImpl<?>) {
                        destObject = ((IObjectImpl<?>) destObject).toModel();
                    }

                    IEventSource<?> dObject = (IEventSource<?>) destObject;

                    ListModel<?, ? extends IEventSource<?>> lm = null;

                    String fieldName = event.getFieldMap().getDestFieldName();

                    lm = dicImplToModel.get(new MyReference(dValue));
                    if (lm == null) {
                        lm = new ListModel<>(dValue, dObject, fieldName);
                    }

                    getField(event.getDestinationObject(), fieldName);
                    Method m = getSetMethod(event.getDestinationObject(), fieldName);
                    m.invoke(event.getDestinationObject(), lm);
                    dicImplToModel.put(new MyReference(dValue), lm);

                }
            } catch (Exception e) {
                LOGGER.warn("postWritingDestinationValue {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
    }

    @Override
    public void onPreWritingDestinationValue(Event event) {
        FieldMap fm = event.getFieldMap();
        Object o = event.getDestinationValue();
        if (o instanceof List<?>) {
            List<?> l = (List<?>) o;
            if (l.size() > 0) {
                // Correction: manage null objects.
                Class<?> c = null;
                HintContainer hc = new HintContainer(new BeanContainer());
                for (Object element : l) {
                    if (element != null) {
                        c = element.getClass();
                        break;
                    }
                }
                if (c == null) {
                    c = Void.class;
                }
                hc.setHintName(c.getName());
                fm.setDestHintContainer(hc);
            }
        }

    }
}

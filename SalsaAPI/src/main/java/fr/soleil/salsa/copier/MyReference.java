package fr.soleil.salsa.copier;

public class MyReference {

    private final Object ref;

    public MyReference(Object ref) {
        this.ref = ref;
    }

    @Override
    public boolean equals(Object obj) {
        MyReference ref2 = (MyReference) obj;
        return ref == ref2;
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return ref.hashCode();
    }
}

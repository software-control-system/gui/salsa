package fr.soleil.salsa.copier;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import com.github.dozermapper.core.DozerConverter;
import com.github.dozermapper.core.Mapper;
import com.github.dozermapper.core.MapperAware;
import com.github.dozermapper.core.MappingProcessor;
import com.github.dozermapper.core.events.DefaultEventManager;
import com.github.dozermapper.core.events.EventListener;
import com.github.dozermapper.core.events.EventManager;

import fr.soleil.salsa.logging.LoggingUtil;

// XXX GIRARDOT: added @SuppressWarnings("unchecked") because of the use of generics, and because classes do not come with their generic parameters
@Deprecated
@SuppressWarnings("unchecked")
public class MyConverter extends DozerConverter<List, List> implements MapperAware {

    public static final Logger LOGGER = LoggingUtil.getLogger(MyConverter.class);

    private Mapper mapper;

    public MyConverter() {
        super(List.class, List.class);
    }

    @Override
    public List convertTo(List source, List destination) {
        if (source == null) {
            return null;
        }

        // The table containing mappings realised
        Map<Object, Object> mappings = getMappings();

        List originalToMapped = new ArrayList();
        for (Object item : source) {
            Object mappedItem = mapper.map(item, item.getClass());
            if ((item != null) && item.getClass().isEnum()) {
                mappedItem = item;
            } else {
                if ((mappings != null) && (item != null) && mappings.containsKey(item)) {
                    // If this src object has already been converted,
                    // we use the same reference to the converted value.
                    mappedItem = mappings.get(item);
                } else {
                    mappedItem = mapper.map(item, item.getClass());
                }
            }
            originalToMapped.add(mappedItem);
        }
        return originalToMapped;
    }

    @Override
    public void setMapper(Mapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List convertFrom(List source, List destination) {
        if (source == null) {
            return null;
        }

        // The table containing mappings realised
        Map<Object, Object> mappings = getMappings();

        List originalToMapped = new ArrayList();
        for (Object item : source) {
            Object mappedItem = null;
            if ((item != null) && item.getClass().isEnum()) {
                mappedItem = item;
            } else {
                if ((mappings != null) && (item != null) && mappings.containsKey(item)) {
                    // If this src object has already been converted,
                    // we use the same reference to the converted value.
                    mappedItem = mappings.get(item);
                } else {
                    // XXX warning! item class must implement expected interface as first.
                    mappedItem = item != null ? mapper.map(item, item.getClass().getInterfaces()[0]) : null;
                }
            }
            // originalToMapped.put(item, mappedItem);
            originalToMapped.add(mappedItem);
        }
        return originalToMapped;
    }

    /**
     * @return
     */
    private Map<Object, Object> getMappings() {
        // Very very dirty to use introspection to
        // access private variable of the dozer library
        // but not other solution found.
        // :(
        Map<Object, Object> mappings = null;
        if (mapper instanceof MappingProcessor) {
            try {
                Snippet listener;
                MappingProcessor.class.getDeclaredFields();
                Field f = MappingProcessor.class.getDeclaredField("eventManager");
                f.setAccessible(true);
                EventManager dem = (EventManager) f.get(mapper);
                if (dem instanceof DefaultEventManager) {
                    f = DefaultEventManager.class.getDeclaredField("eventListeners");
                    f.setAccessible(true);
                    ArrayList<EventListener> listeners = (ArrayList<EventListener>) f.get(dem);
                    listener = (Snippet) listeners.get(0);
                } else {
                    // Workaround in case something goes wrong with dozer
                    LOGGER.warn(MyConverter.class.getName()
                            + ".getMappings(): Calling Snippet.getCurrentSnippet() due to new dozer implementation");
                    listener = Snippet.getCurrentSnippet();
                }
                mappings = listener.getMappings();
            } catch (Exception exc) {
                LOGGER.warn("Cannot getMappings {}", exc.getMessage());
                LOGGER.debug("Stack trace", exc);
            }
        }
        return mappings;
    }

}

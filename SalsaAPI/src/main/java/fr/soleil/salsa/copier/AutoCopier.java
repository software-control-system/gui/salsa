package fr.soleil.salsa.copier;

import java.util.ArrayList;
import java.util.List;

import com.github.dozermapper.core.DozerBeanMapper;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.events.EventListener;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.DirectoryModel;
import fr.soleil.salsa.entity.impl.DirectoryImpl;
import fr.soleil.salsa.entity.scan2D.IConfig2D;

public class AutoCopier {

    private static final String DOZER_XML = "dozer.xml";
    private static final String DOZER_PARTIAL_XML = "dozer.partial.xml";

    public static IConfig<?> copy(IConfig<?> c) {

        List<String> mapFiles = new ArrayList<String>();
        List<EventListener> eventListeners = new ArrayList<EventListener>();
        eventListeners.add(new Snippet(true));
        mapFiles.add(getAccesibleFileName(DOZER_XML));
        DozerBeanMapperBuilder mapperBuilder = DozerBeanMapperBuilder.create();
        mapperBuilder.withMappingFiles(mapFiles);
        mapperBuilder.withEventListeners(eventListeners);
        DozerBeanMapper mapper = (DozerBeanMapper) mapperBuilder.build();

        // Config1DModel m = mapper.map(c, Config1DModel.class);
        IConfig<?> m;
        if (c == null) {
            m = null;
        } else {
            m = mapper.map(c, IConfig.class);
        }
        finalizeCopy(c, m);
        copyTrajectory(c, m);
        return m;
    }

    public static IConfig<?> copyToImpl(IConfig<?> c) {
        IConfig<?> configImpl = toImpl(c, IConfig.class);
        configImpl.setEnableScanSpeed(c.isEnableScanSpeed());
        copyTrajectory(c, configImpl);
        return configImpl;
    }

    private static void copyTrajectory(IConfig<?> source, IConfig<?> dest) {
        if ((source.getDimensionX() != null) && (dest.getDimensionX() != null)) {
            List<? extends IRange> sourceRangeList = source.getDimensionX().getRangeList();
            List<? extends IRange> destRangeList = dest.getDimensionX().getRangeList();

            copyRangeList(sourceRangeList, destRangeList);

            if ((source instanceof IConfig2D) && (dest instanceof IConfig2D)) {
                sourceRangeList = ((IConfig2D) source).getDimensionY().getRangeList();
                destRangeList = ((IConfig2D) dest).getDimensionY().getRangeList();
                copyRangeList(sourceRangeList, destRangeList);
            }
        }
    }

    private static void copyRangeList(List<? extends IRange> sourceRangeList, List<? extends IRange> destRangeList) {
        if ((sourceRangeList != null) && (destRangeList != null) && (sourceRangeList.size() == destRangeList.size())) {
            for (int i = 0; i < sourceRangeList.size(); i++) {
                IRange rangeSource = sourceRangeList.get(i);
                IRange rangeDest = destRangeList.get(i);

                rangeDest.setStepsNumber(rangeSource.getStepsNumber());

                List<ITrajectory> sourceTrajectoryList = rangeSource.getTrajectoriesList();
                List<ITrajectory> destTrajectoryList = rangeDest.getTrajectoriesList();
                if ((sourceTrajectoryList != null) && (destTrajectoryList != null)
                        && (sourceTrajectoryList.size() == destTrajectoryList.size())) {
                    for (int j = 0; j < sourceTrajectoryList.size(); j++) {
                        ITrajectory sourceT = sourceTrajectoryList.get(j);
                        ITrajectory destT = destTrajectoryList.get(j);
                        if ((sourceT != null) && (destT != null)) {
                            destT.setDeltaConstant(sourceT.isDeltaConstant());
                            destT.setRelative(sourceT.getRelative());
                        }
                    }
                }
            }
        }
    }

    public static <T> T toImpl(Object c, Class<T> desiredClass) {

        List<String> mapFiles = new ArrayList<String>();
        List<EventListener> eventListeners = new ArrayList<EventListener>();
        eventListeners.add(new Snippet(false));
        mapFiles.add(getAccesibleFileName(DOZER_XML));
        DozerBeanMapperBuilder mapperBuilder = DozerBeanMapperBuilder.create();
        mapperBuilder.withMappingFiles(mapFiles);
        mapperBuilder.withEventListeners(eventListeners);
        DozerBeanMapper mapper = (DozerBeanMapper) mapperBuilder.build();

        // Config1DImpl m = mapper.map(c, Config1DImpl.class);
        T m;
        if (c == null) {
            m = null;
        } else {
            m = mapper.map(c, desiredClass);
        }
        finalizeCopy(c, m);
        return m;
    }

    private static Object map(Object o, Class<?> c, boolean toModelList) {
        List<String> mapFiles = new ArrayList<String>();
        List<EventListener> eventListeners = new ArrayList<EventListener>();
        eventListeners.add(new Snippet(toModelList));
        mapFiles.add(getAccesibleFileName(DOZER_XML));
        DozerBeanMapperBuilder mapperBuilder = DozerBeanMapperBuilder.create();
        mapperBuilder.withMappingFiles(mapFiles);
        mapperBuilder.withEventListeners(eventListeners);
        DozerBeanMapper mapper = (DozerBeanMapper) mapperBuilder.build();

        Object result;
        if (o == null) {
            result = null;
        } else {
            result = mapper.map(o, c);
        }
        finalizeCopy(o, result);
        return result;
    }

    public static IDirectory copyDirectoryToImpl(IDirectory d) {
        IDirectory result = (IDirectory) map(d, DirectoryImpl.class, false);
        return result;
    }

    public static IDirectory copyDirectoryToModel(IDirectory d) {
        IDirectory result = (IDirectory) map(d, DirectoryModel.class, false);
        return result;
    }

    public static IDimension copyDimensionToModel(IDimension dimension) {
        IDimension result = (IDimension) map(dimension, dimension.getClass(), true);
        return result;
    }

    public static void copyPartial(Object o, Object dst) {
        List<String> mapFiles = new ArrayList<String>();
        List<EventListener> eventListeners = new ArrayList<EventListener>();
        eventListeners.add(new Snippet(false));
        mapFiles.add(getAccesibleFileName(DOZER_PARTIAL_XML));
        // mapFiles.add("file:////D:/dozer.partial.xml");
        DozerBeanMapperBuilder mapperBuilder = DozerBeanMapperBuilder.create();
        mapperBuilder.withMappingFiles(mapFiles);
        mapperBuilder.withEventListeners(eventListeners);
        DozerBeanMapper mapper = (DozerBeanMapper) mapperBuilder.build();

        // Object result =
        mapper.map(o, dst);// , "partialTrajectory");
        finalizeCopy(o, dst);
    }

    /**
     * Return the url of the given resource if it exists in the classpath.
     * 
     * @param name
     * @return
     */
    private static String getAccesibleFileName(String name) {
        return ClassLoader.getSystemResource(name).toExternalForm();
    }

    private static void finalizeCopy(Object source, Object dest) {
        if (source instanceof IConfig<?>) {
            IConfig<?> scs = (IConfig<?>) source;
            IConfig<?> scd = (IConfig<?>) dest;
            scd.setModified(scs.isModified());
        }
    }

}

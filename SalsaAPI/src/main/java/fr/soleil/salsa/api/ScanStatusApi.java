package fr.soleil.salsa.api;

import org.slf4j.Logger;

import fr.soleil.salsa.config.ApplicationConfig;
import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.locator.ILocator;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.IScanStatusService;

/**
 * @author Alike API relative to the scan status informations retrieving.
 */
public class ScanStatusApi {

    public static final Logger LOGGER = LoggingUtil.getLogger(ScanStatusApi.class);

    /**
     * The locator that seeks the required service(s).
     */
    private static ILocator locator;

    /**
     * The service that allows control over scans.
     */
    private static IScanStatusService scanStatusService;

    /**
     * Static initialization.
     */
    static {
        try {
            locator = ApplicationConfig.getLocator();
            Object o = locator.getService(IScanStatusService.class);
            scanStatusService = (IScanStatusService) o;
        } catch (Exception e) {
            LOGGER.warn("Exception caught during service creation {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
    }

    /**
     * Gets the scan status.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    public static IScanStatus getStatus(String scanServerName) throws SalsaDeviceException {
        return scanStatusService.getScanServerStatus(scanServerName);
    }
}

package fr.soleil.salsa.api;

import org.slf4j.Logger;

import fr.soleil.salsa.config.ApplicationConfig;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;
import fr.soleil.salsa.locator.ILocator;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.IConfigService;

/**
 * The api to access configurations
 * 
 * @author Etienne BOUTRY
 * @author Boualem ABDELLI
 * 
 */
public class ConfigApi {

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigApi.class);

    /**
     * The locator that seeks the required service(s).
     */
    private static ILocator locator;

    /**
     * The service that allows control over scans.
     */
    private static IConfigService configService;

    /**
     * Static initialization.
     */
    static {
        try {
            locator = ApplicationConfig.getLocator();
            configService = locator.getService(IConfigService.class);
        } catch (Exception e) {
            LOGGER.warn("Exception caught during service creation {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
    }

    public static IDirectory getRootDirectory() {
        return configService.getRootDirectory();
    }

    public static IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException, ScanNotFoundException {
        return configService.saveConfig(config);
    }

    public static IConfig<?> getConfigById(Integer id) throws ScanNotFoundException {
        return configService.getConfigById(id);
    }

    public static IConfig<?> getConfigByPath(String path) throws ScanNotFoundException {
        return configService.getConfigByPath(path);
    }

    public static IConfig<?> getConfigByPath(String path, IConfig<?> lastLoadedConfig) throws ScanNotFoundException {
        return configService.getConfigByPath(path, lastLoadedConfig);
    }

    public static IDirectory saveDirectory(IDirectory directory) throws PersistenceException {
        return configService.saveDirectory(directory);
    }

    public static void deleteConfig(IConfig<?> config) throws PersistenceException {
        configService.deleteConfig(config);
    }

    public static void deleteDirectory(IDirectory directory) throws PersistenceException {
        configService.deleteDirectory(directory);
    }

}

package fr.soleil.salsa.api.item;

/**
 * The type of dimensions of a device.
 * SCALAR : a single scalar (0 dimension).
 * SPECTRUM : an array with 1 dimension.
 * IMAGE : an array with 2 dimensions.
 */
public enum DimensionType {
    /**
     * Single scalar.
     */
    SCALAR,
    /**
     * 1D array.
     */
    SPECTRUM,
    /**
     * 2D array.
     */
    IMAGE
}

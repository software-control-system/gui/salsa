package fr.soleil.salsa.api.util;

import java.util.List;
import java.util.ListIterator;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.TrajectoryUtil;

/**
 * set of function to calculate a trajectory
 * 
 * @author SAINTIN
 * 
 */
public abstract class TrajectoryCalculator {

    // Calculation of the Delta part from begin position , end position and number of steps
    public static double calculDelta(final double beginPos, final double endPos, final int steps) {
        return TrajectoryUtil.calculDelta(beginPos, endPos, steps);
    }

    public static double calculSpeed(final double delta, final int steps, final double[] integrationTime) {
        return TrajectoryUtil.calculSpeed(delta, steps, integrationTime);
    }

    public static IDimension getDimension(IConfig<?> config, boolean yActuator) {
        IDimension dimension = config.getDimensionX();
        if ((config instanceof IConfig2D) && yActuator) {
            dimension = ((IConfig2D) config).getDimensionY();
        }
        return dimension;
    }

    public static void addTrajectoryList(IDimension dimension, ITrajectory trajectory) {
        if ((dimension instanceof IDimension1D) && (trajectory instanceof ITrajectory1D)) {
            List<ITrajectory1D> trajectoryList = ((IDimension1D) dimension).getTrajectoriesList();
            trajectoryList.add((ITrajectory1D) trajectory);
        } else if ((dimension instanceof IDimensionHCS) && (trajectory instanceof ITrajectoryHCS)) {
            List<ITrajectoryHCS> trajectoryList = ((IDimensionHCS) dimension).getTrajectoriesList();
            trajectoryList.add((ITrajectoryHCS) trajectory);
        }
    }

    public static void setDimension(IConfig<?> config, IDimension dimension) {
        if ((config instanceof IConfig1D) && (dimension instanceof IDimension1D)) {
            ((IConfig1D) config).setDimensionX((IDimension1D) dimension);
        } else if ((config instanceof IConfig2D) && (dimension instanceof IDimension2DX)) {
            ((IConfig2D) config).setDimensionX((IDimension2DX) dimension);
        } else if ((config instanceof IConfig2D) && (dimension instanceof IDimension2DY)) {
            ((IConfig2D) config).setDimensionY((IDimension2DY) dimension);
        } else if ((config instanceof IConfigHCS) && (dimension instanceof IDimensionHCS)) {
            ((IConfigHCS) config).setDimensionX((IDimensionHCS) dimension);
        } else if ((config instanceof IConfigEnergy) && (dimension instanceof IDimensionEnergy)) {
            ((IConfigEnergy) config).setDimensionX((IDimensionEnergy) dimension);
        } else if ((config instanceof IConfigK) && (dimension instanceof IDimensionK)) {
            ((IConfigK) config).setDimensionX((IDimensionK) dimension);
        }
    }

    public static void swapRange(IConfig<?> config, int pos1, int pos2, boolean yActuator) {
        List<? extends IRange> rangeList = getRangeList(config, yActuator);
        ListIterator<? extends IRange> rangeIterator = rangeList.listIterator();

        while (rangeIterator.hasNext()) {
            IRange range = rangeIterator.next();
            List<ITrajectory> trajectoryList = range.getTrajectoriesList();

            ITrajectory t1 = trajectoryList.get(pos1);
            ITrajectory t2 = trajectoryList.get(pos2);
            swapTrajectory(t1, t2);
        }
    }

    public static void swapTrajectory(ITrajectory t1, ITrajectory t2) {
        TrajectoryUtil.swapTrajectory(t1, t2);
    }

    public static List<? extends IRange> getRangeList(IConfig<?> config, boolean yActuator) {
        List<? extends IRange> rangeList = null;
        IDimension dimension = config.getDimensionX();
        if ((config instanceof IConfig2D) && yActuator) {
            dimension = ((IConfig2D) config).getDimensionY();
        }

        if (dimension instanceof IDimension1D) {
            IDimension1D dimension1D = ((IDimension1D) dimension);
            rangeList = dimension1D.getRangesXList();
        } else if (dimension instanceof IDimension2DX) {
            IDimension2DX dimension2DX = ((IDimension2DX) dimension);
            rangeList = dimension2DX.getRangesList();
        } else if (dimension instanceof IDimension2DY) {
            IDimension2DY dimension2DY = ((IDimension2DY) dimension);
            rangeList = dimension2DY.getRangesList();
        } else if (dimension instanceof IDimensionEnergy) {
            IDimensionEnergy dimensionEnergy = ((IDimensionEnergy) dimension);
            rangeList = dimensionEnergy.getRangesEnergyList();
        } else if (dimension instanceof IDimensionHCS) {
            IDimensionHCS dimensionHCS = ((IDimensionHCS) dimension);
            rangeList = dimensionHCS.getRangesXList();
        }
        return rangeList;
    }

    public static void setRange(ITrajectory trajectory, IRange range) {
        if ((trajectory instanceof ITrajectory1D) && (range instanceof IRange1D)) {
            ((ITrajectory1D) trajectory).setRange((IRange1D) range);
        } else if ((trajectory instanceof ITrajectory2DX) && (range instanceof IRange2DX)) {
            ((ITrajectory2DX) trajectory).setRange((IRange2DX) range);
        } else if ((trajectory instanceof ITrajectory2DY) && (range instanceof IRange2DY)) {
            ((ITrajectory2DY) trajectory).setRange((IRange2DY) range);
        } else if ((trajectory instanceof ITrajectoryHCS) && (range instanceof IRangeHCS)) {
            ((ITrajectoryHCS) trajectory).setRange((IRangeHCS) range);
        } else if ((trajectory instanceof ITrajectoryEnergy) && (range instanceof IRangeEnergy)) {
            ((ITrajectoryEnergy) trajectory).setRange((IRangeEnergy) range);
        } else if ((trajectory instanceof ITrajectoryK) && (range instanceof IRangeK)) {
            ((ITrajectoryK) trajectory).setRange((IRangeK) range);
        }
    }
}

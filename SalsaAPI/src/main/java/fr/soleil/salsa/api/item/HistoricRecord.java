package fr.soleil.salsa.api.item;

import java.io.Serializable;

/**
 * Event in the historic.
 */
public class HistoricRecord implements Serializable {

    private static final long serialVersionUID = -4672322526547548634L;

    /**
     * The period.
     */
    private String period;

    /**
     * The type.
     */
    private String type;

    /**
     * The message.
     */
    private String message;

    /**
     * The period.
     */
    public String getPeriod() {
        return period;
    }

    /**
     * The period.
     */
    public void setPeriod(String period) {
        this.period = period;
    }

    /**
     * The type.
     */
    public String getType() {
        return type;
    }

    /**
     * The type.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * The message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * The message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
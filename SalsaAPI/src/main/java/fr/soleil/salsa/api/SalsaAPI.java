package fr.soleil.salsa.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.ScanState;
import fr.soleil.salsa.entity.impl.ContextImpl;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaLoggingException;
import fr.soleil.salsa.exception.SalsaScanConfigurationException;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesPersistence;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * A class that gives the basic functions to interact with Salsa API (scan, scan configurations,
 * etc...)
 * 
 * @author saintin
 */
public class SalsaAPI {
    public static final Logger LOGGER = LoggingUtil.getLogger(SalsaAPI.class);

    private static DevicePreferences devicePreferences;

    // Management of config in API
    private static Map<Integer, IConfig<?>> configMap = new HashMap<>();
    private static Map<String, Integer> configNameMap = new HashMap<>();

    private IConfig<?> currentConfig;

    /**
     * Save preferences
     */
    public void savePreferences() {
        DevicePreferencesPersistence.save(getDevicePreferences());
    }

    /**
     * Creates an {@link IContext} that contains the same information as a particular {@link DevicePreferences}
     * 
     * @param preferences The {@link DevicePreferences}
     * @return The expected {@link IContext}, or <code>null</code> if <code>preferences</code> was <code>null</code>
     */
    public static IContext extractContext(DevicePreferences preferences) {
        IContext result = null;
        if (preferences != null) {
            result = new ContextImpl();
            result.setScanServerName(preferences.getScanServer());
            result.setUserLogFile(preferences.getUserLogFile());
            result.setMaxLineNumber(preferences.getMaxLineNumber());
        }
        return result;
    }

    /**
     * Returns this {@link SalsaAPI}'s {@link DevicePreferences} transformed into an {@link IContext}
     * 
     * @return An {@link IContext}
     */
    public IContext getDevicePreferencesAsContext() {
        return extractContext(getDevicePreferences());
    }

    /**
     * Returns the {@link DevicePreferences} this {@link SalsaAPI} uses. If this {@link DevicePreferences} was not
     * previously set, it will be recovered from system
     * preferences
     * 
     * @return A {@link DevicePreferences}
     * @see #setDevicePreferences(DevicePreferences)
     * @see #recoverPreferences(String, String)
     */
    public static DevicePreferences getDevicePreferences() {
        if (devicePreferences == null) {
            devicePreferences = DevicePreferencesPersistence.getSystemPreferences();
        }
        return devicePreferences;
    }

    /**
     * Sets the {@link DevicePreferences} this {@link SalsaAPI} should use.
     * 
     * @param devicePreferences The {@link DevicePreferences} to set
     */
    public static void setDevicePreferences(DevicePreferences adevicePreferences) {
        devicePreferences = adevicePreferences;
        if (devicePreferences != null) {
            try {
                ScanApi.setDataRecorderEnable(devicePreferences.isDataRecorder());
            } catch (SalsaDeviceException e) {
                LOGGER.warn("Cannot setDataRecorderEnable {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
    }

    /**
     * Recovers the {@link DevicePreferences} from a given file path, using a default ScanServer if
     * such a File does not exist.
     * 
     * @param preferenceFilePath The path of the file that contains the preferences
     * @param defaultScanServer The default ScanServer to use
     * @see #getDevicePreferences()
     */
    public void recoverPreferences(String preferenceFilePath, String defaultScanServer) {
        setDevicePreferences(DevicePreferencesPersistence.load(preferenceFilePath, defaultScanServer));
    }

    /**
     * Returns this API's current {@link IConfig}
     * 
     * @return An {@link IConfig}
     */
    public IConfig<?> getCurrentConfig() {
        return currentConfig;
    }

    /**
     * Sets this API's current {@link IConfig}
     * 
     * @param currentConfig The {@link IConfig} to set
     */
    public void setCurrentConfig(IConfig<?> currentConfig) {
        this.currentConfig = currentConfig;
    }

    /**
     * Gets the scan status.
     * 
     * @param scanServerName The scanServer name
     * @return The corresponding {@link IScanStatus}
     * @throws SalsaDeviceException If a problem occurred while trying to get the status, or if
     *             there was no way to get such a status (bad parameter).
     */
    public static IScanStatus getStatus(String scanServerName) throws SalsaDeviceException {
        IScanStatus reasStatus = ScanStatusApi.getStatus(scanServerName);
        LOGGER.trace("{}.getStatus({})={}", SalsaAPI.class.getSimpleName(), scanServerName, reasStatus);
        return reasStatus;
    }

    /**
     * Gets the scan status, using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @return The corresponding {@link IScanStatus}
     * @throws SalsaDeviceException If a problem occurred while trying to get the status, or if
     *             there was no way to get such a status (bad parameter).
     * @see #getDevicePreferences()
     */
    public IScanStatus getStatus() throws SalsaDeviceException {
        return getStatus(getScanServerName());
    }

    /**
     * Gets suggestions from scanserver properties for devices such as sensors, actuators,
     * timebases.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @return the suggestions
     * @throws SalsaDeviceException if an error occurred with device
     */

    public static ISuggestions getDevicesSuggestions(String scanServerName) throws SalsaDeviceException {
        return ScanApi.getDevicesSuggestions(scanServerName);
    }

    /**
     * Gets suggestions from scanserver properties for devices such as sensors, actuators,
     * timebases.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @return
     * @throws SalsaDeviceException
     */

    public ISuggestions getDevicesSuggestions() throws SalsaDeviceException {
        return getDevicesSuggestions(getScanServerName());
    }

    /**
     * Reads and returns the state of a Scan Server
     * 
     * @param scanServerName The name of the Scan Server
     * @return A {@link ScanState}. If there is a problem with the given Scan Server, the default
     *         value {@link ScanState#STOPPED} is returned
     * @throws SalsaDeviceException If <code>scanServerName</code> is <code>null</code>
     */
    public static ScanState getScanState(String scanServerName) throws SalsaDeviceException {
        ScanState state = ScanApi.getScanState(scanServerName);
        LOGGER.trace("{}.getScanState({})={}", SalsaAPI.class.getSimpleName(), scanServerName, state);
        return state;
    }

    /**
     * Reads and returns the state of a Scan Server, using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @return A {@link ScanState}. If there is a problem with the given Scan Server, the default
     *         value {@link ScanState#STOPPED} is returned
     * @throws SalsaDeviceException If <code>scanServerName</code> is <code>null</code>
     */
    public ScanState getScanState() throws SalsaDeviceException {
        return getScanState(getScanServerName());
    }

    /**
     * Starts and logs a scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException if a problem occurred during scan start
     * @throws SalsaLoggingException if a problem occurred during scan logging
     * @throws SalsaScanConfigurationException If the config is not well formatted
     */
    public static void startScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaLoggingException, SalsaScanConfigurationException {
        LOGGER.info("{}.startScan({})", SalsaAPI.class.getSimpleName(), config);
        ScanApi.loadScan(config, context);
        ScanApi.startScan(config, context);
    }

    /**
     * Starts and logs a scan, using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @param config parameters of the scan.
     * @throws SalsaDeviceException if a problem occurred during scan start
     * @throws SalsaLoggingException if a problem occurred during scan logging
     * @throws SalsaScanConfigurationException If the config is not well formatted
     * @see #getDevicePreferences()
     */
    public void startScan(IConfig<?> config)
            throws SalsaDeviceException, SalsaLoggingException, SalsaScanConfigurationException {
        startScan(config, getDevicePreferencesAsContext());
    }

    /**
     * Starts a scan and logs a scan.
     * 
     * @param fullPath The path of the scan configuration to load
     * @param context the context of the scan.
     * @throws SalsaDeviceException if a problem occurred during scan start
     * @throws ScanNotFoundException if the given path does not correspond to any scan configuration
     * @throws SalsaLoggingException if a problem occurred during scan logging
     * @throws SalsaScanConfigurationException If the config is not well formatted
     */
    public static void startScan(String fullPath, IContext context)
            throws SalsaDeviceException, ScanNotFoundException, SalsaLoggingException, SalsaScanConfigurationException {
        startScan(getConfigByPath(fullPath), context);
    }

    /**
     * Starts a scan and logs a scan, using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @param fullPath The path of the scan configuration to load
     * @throws SalsaDeviceException if a problem occurred during scan start
     * @throws ScanNotFoundException if the given path does not correspond to any scan configuration
     * @throws SalsaLoggingException if a problem occurred during scan logging
     * @throws SalsaScanConfigurationException If the config is not well formatted
     * @see #getDevicePreferences()
     */
    public void startScan(String fullPath)
            throws SalsaDeviceException, ScanNotFoundException, SalsaLoggingException, SalsaScanConfigurationException {
        startScan(fullPath, getDevicePreferencesAsContext());
    }

    /**
     * Stops the scan (no logging).
     * 
     * @param context context of the scan.
     * @throws SalsaDeviceException if a problem occurred during scan stop
     * @throws SalsaLoggingException if an error occurred when logging
     */
    public static void stopScan(IContext context) throws SalsaDeviceException, SalsaLoggingException {
        LOGGER.info("{}.stopScan()", SalsaAPI.class.getSimpleName());
        ScanApi.stopScan(context);
    }

    /**
     * Stops the scan (no logging), using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @throws SalsaDeviceException if a problem occurred during scan stop
     * @throws SalsaLoggingException if an error occurred when logging
     * @see #getDevicePreferences()
     */
    public void stopScan() throws SalsaDeviceException, SalsaLoggingException {
        stopScan(getDevicePreferencesAsContext());
    }

    /**
     * Pauses the scan (no logging).
     * 
     * @param context context of the scan.
     * @throws SalsaDeviceException if a problem occurred during scan pause
     * @throws SalsaLoggingException if an error occurred when logging
     */
    public static void pauseScan(IContext context) throws SalsaDeviceException, SalsaLoggingException {
        LOGGER.info("{}.pauseScan()", SalsaAPI.class.getSimpleName());
        ScanApi.pauseScan(context);
    }

    /**
     * Pauses the scan (no logging), using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @throws SalsaDeviceException if a problem occurred during scan pause
     * @throws SalsaLoggingException if an error occurred when logging
     * @see #getDevicePreferences()
     */
    public void pauseScan() throws SalsaDeviceException, SalsaLoggingException {
        pauseScan(getDevicePreferencesAsContext());
    }

    /**
     * Resumes the scan (no logging).
     * 
     * @param context context of the scan.
     * @throws SalsaDeviceException if a problem occurred during scan resuming
     * @throws SalsaLoggingException if an error occurred when logging
     */
    public static void resumeScan(IContext context) throws SalsaDeviceException, SalsaLoggingException {
        LOGGER.info("{}.resumeScan()", SalsaAPI.class.getSimpleName());
        ScanApi.resumeScan(context);
    }

    /**
     * Resumes the scan (no logging), using this {@link SalsaAPI}'s {@link DevicePreferences}.
     * 
     * @throws SalsaDeviceException if a problem occurred during scan resuming
     * @throws SalsaLoggingException if an error occurred when logging
     * @see #getDevicePreferences()
     */
    public void resumeScan() throws SalsaDeviceException, SalsaLoggingException {
        resumeScan(getDevicePreferencesAsContext());
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @param scanServerName The name of the scan server to use.
     * @return The corresponding {@link IScanResult}
     * @throws SalsaScanConfigurationException If the Scan Server Scan Type is not supported
     * @throws SalsaDeviceException If a problem occurred while reading scan result
     */
    public static IScanResult readScanResult(String scanServerName)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        IScanResult result = ScanApi.readScanResult(scanServerName);
        LOGGER.info("{}.readScanResult({})={}", SalsaAPI.class.getSimpleName(), scanServerName, result);
        return result;
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @param scanServerName The name of the scan server to use read the real trajectory also
     * @return The corresponding {@link IScanResult}
     * @throws SalsaScanConfigurationException If the Scan Server Scan Type is not supported
     * @throws SalsaDeviceException If a problem occurred while reading scan result
     */
    public static IScanResult readScanResultWithTrajectories(String scanServerName)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        IScanResult result = ScanApi.readScanResult(scanServerName, true);
        LOGGER.info("{}.readScanResultWithTrajectories({})={}", SalsaAPI.class.getSimpleName(), scanServerName, result);
        return result;
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan, using this {@link SalsaAPI}'s
     * {@link DevicePreferences}.
     * 
     * @return The corresponding {@link IScanResult}
     * @throws SalsaDeviceException If a problem occurred while reading scan result
     * @throws SalsaScanConfigurationException If the Scan Server Scan Type is not supported
     * @see #getDevicePreferences()
     */
    public IScanResult readScanResult() throws SalsaDeviceException, SalsaScanConfigurationException {
        return readScanResult(getScanServerName());
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan, using this {@link SalsaAPI}'s
     * {@link DevicePreferences}.
     * 
     * @return The corresponding {@link IScanResult} and read the real trajectory also
     * @throws SalsaScanConfigurationException If the Scan Server Scan Type is not supported
     * @throws SalsaDeviceException If a problem occurred while reading scan result
     * @see #getDevicePreferences()
     */
    public IScanResult readScanResultWithTrajectories() throws SalsaDeviceException, SalsaScanConfigurationException {
        return readScanResultWithTrajectories(getScanServerName());
    }

    /**
     * Returns the root directory
     * 
     * @return an {@link IDirectory}
     */
    public static IDirectory getRootDirectory() {
        return ConfigApi.getRootDirectory();
    }

    /**
     * Returns the list of configuration
     * 
     * @return a Map <configurationPath, IConfig<?>>
     */
    public static Map<String, IConfig<?>> getConfigList() {
        Map<String, IConfig<?>> configurationMap = new HashMap<>();
        IDirectory rootDirectory = SalsaAPI.getRootDirectory();
        fillConfigurationMap(configurationMap, rootDirectory);
        return configurationMap;
    }

    private static void fillConfigurationMap(Map<String, IConfig<?>> map, IDirectory directory) {
        if (directory != null) {
            Collection<IConfig<?>> configList = directory.getConfigList();
            if (SalsaUtils.isFulfilled(configList)) {
                for (IConfig<?> config : configList) {
                    map.put(config.getFullPath(), config);
                }
            }
            Collection<IDirectory> directoryList = directory.getSubDirectoriesList();
            if (SalsaUtils.isFulfilled(directoryList)) {
                for (IDirectory subDir : directoryList) {
                    fillConfigurationMap(map, subDir);
                }
            }
        }
    }

    /**
     * Loads a scan configuration form database, identified by an id
     * 
     * @param id The id of the scan configuration to load
     * @return The loaded {@link IConfig}.
     * @throws ScanNotFoundException If loading failed
     */
    public static IConfig<?> getConfigById(Integer id) throws ScanNotFoundException {
        return getConfigById(id, true);
    }

    /**
     * Loads a scan configuration form database, identified by an id
     * 
     * @param id The id of the scan configuration to load
     * @param reload Force to reload value from config if true
     * @return The loaded {@link IConfig}.
     * @throws ScanNotFoundException If loading failed
     */
    public static IConfig<?> getConfigById(Integer id, boolean reload) throws ScanNotFoundException {
        IConfig<?> config = null;
        // Do not override the config load in the Map
        if (id != null) {
            if (reload) {
                // read the config and do not store in the Map
                config = ConfigApi.getConfigById(id);
            } else {
                config = configMap.get(id);
                if (config == null) {
                    config = ConfigApi.getConfigById(id);
                    if (config != null) {
                        configMap.put(id, config);
                        configNameMap.put(config.getFullPath(), id);
                    }
                }
            }
        }
        LOGGER.trace("{}.getConfigById({})={}", SalsaAPI.class.getSimpleName(), id, config);
        return config;
    }

    /**
     * Force to reload the configuration
     * 
     * @param id the id of the configuration
     * @return the reloaded configuration
     * @throws ScanNotFoundException if unable to found configuration
     */
    public static IConfig<?> reloadConfigById(Integer id) throws ScanNotFoundException {
        IConfig<?> config = getConfigById(id, true);
        configMap.put(id, config);
        return config;
    }

    /**
     * Loads a scan configuration form database, identified by a path
     * 
     * @param path The path of the scan configuration to load
     * @param reload Force to reload value from config if true
     * @return The loaded {@link IConfig}.
     * @throws ScanNotFoundException If loading failed
     */
    public static IConfig<?> getConfigByPath(String path, boolean reload) throws ScanNotFoundException {
        IConfig<?> config = null;
        // Do not write on the map if it reload
        if (SalsaUtils.isDefined(path)) {
            Integer id = configNameMap.get(path);
            if (id != null) {
                config = getConfigById(id, reload);
            } else {
                config = ConfigApi.getConfigByPath(path);
                if (config != null) {
                    configMap.put(config.getId(), config);
                    configNameMap.put(path, config.getId());
                }
            }
        }
        LOGGER.trace("{}.getConfigByPath({})={}", SalsaAPI.class.getSimpleName(), path, config);
        return config;
    }

    /**
     * Loads a scan configuration form database, identified by a path
     * 
     * @param path The path of the scan configuration to load
     * @return The loaded {@link IConfig}.
     * @throws ScanNotFoundException If loading failed
     */
    public static IConfig<?> getConfigByPath(String path) throws ScanNotFoundException {
        return getConfigByPath(path, true);
    }

    /**
     * Saves a scan configuration
     * 
     * @param config the {@link IConfig} to save
     * @return The effectively saved configuration. This means, once the configuration saved, the
     *         API tries to load it and returns the result. So, this result may be <code>null</code> if the
     *         configuration was not successfully saved.
     * @throws PersistenceException If a problem occurred while saving scan configuration
     * @throws ScanNotFoundException If a problem occurred when reloading the configuration
     */
    public static IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException, ScanNotFoundException {
        Integer oldId = null;
        if (config != null) {
            oldId = config.getId();
        }

        IConfig<?> savedConfig = ConfigApi.saveConfig(config);

        if ((oldId == null) && (savedConfig != null)) {
            Integer newId = savedConfig.getId();
            savedConfig = getConfigById(newId, true);
        }
        LOGGER.trace("{}.saveConfig({})={}", SalsaAPI.class.getSimpleName(), config, savedConfig);
        return savedConfig;
    }

    /**
     * Saves a directory in database
     * 
     * @param directory The {@link IDirectory} to save
     * @return The effectively saved directory. This means, once the directory saved, the API tries
     *         to load it and returns the result. So, this result may be <code>null</code> if the
     *         directory was not successfully saved.
     * @throws PersistenceException If a problem occurred while saving directory
     */
    public static IDirectory saveDirectory(IDirectory directory) throws PersistenceException {
        IDirectory saveDirectory = ConfigApi.saveDirectory(directory);
        LOGGER.trace("{}.saveDirectory({})={}", SalsaAPI.class.getSimpleName(), directory, saveDirectory);
        return saveDirectory;
    }

    /**
     * Deletes a scan configuration in database
     * 
     * @param config the {@link IConfig} to delete
     * @throws PersistenceException If a problem occurred during deletion
     */
    public static void deleteConfig(IConfig<?> config) throws PersistenceException {
        LOGGER.trace("{}.deleteConfig({})", SalsaAPI.class.getSimpleName(), config);
        ConfigApi.deleteConfig(config);
    }

    /**
     * Deletes a directory in database
     * 
     * @param directory the {@link IDirectory} to delete
     * @throws PersistenceException If a problem occurred during deletion
     */
    public static void deleteDirectory(IDirectory directory) throws PersistenceException {
        LOGGER.trace("{}.deleteDirectory({})", SalsaAPI.class.getSimpleName(), directory);
        ConfigApi.deleteDirectory(directory);
    }

    /**
     * This method recovers the range list of an {@link IDimension}. This method will disappear once
     * SalsaEntity module is simplified.
     * 
     * /**
     * set the scan server in partial recorder mode to allow an external system to manage the data
     * recording.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @param mode = true allow external system to manage the data recording
     * @return
     * @throws SalsaDeviceException
     */
    public void setDataRecorderPartialMode(boolean mode) throws SalsaDeviceException {
        LOGGER.trace("{}.setDataRecorderPartialMode({})", SalsaAPI.class.getSimpleName(), mode);
        ScanApi.setDataRecorderPartialMode(getScanServerName(), mode);
    }

    /**
     * allow data recording.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @param enable = true allow data recording
     * @return
     * @throws SalsaDeviceException
     */
    public void setDataRecorderEnable(boolean enable) throws SalsaDeviceException {
        ScanApi.setDataRecorderEnable(enable);
    }

    public boolean isDataRecorderEnable() throws SalsaDeviceException {
        return ScanApi.isDataRecorderEnable();
    }

    /**
     * Returns the name of the ScanServer stored in this {@link SalsaAPI}'s {@link DevicePreferences}
     * 
     * @return A {@link String} value
     */
    private String getScanServerName() {
        String scanServerName = null;
        if (getDevicePreferences() != null) {
            scanServerName = getDevicePreferences().getScanServer();
        }
        LOGGER.trace("Get \"{}\" for \"ScanServer\"", scanServerName);
        return scanServerName;
    }

    @Deprecated
    public static void stopScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaLoggingException {
        ScanApi.stopScan(config, context);
    }

    @Deprecated
    public void stopScan(IConfig<?> config) throws SalsaDeviceException, SalsaLoggingException {
        stopScan(config, getDevicePreferencesAsContext());
    }

    @Deprecated
    public static void pauseScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaLoggingException {
        ScanApi.pauseScan(config, context);
    }

    @Deprecated
    public void pauseScan(IConfig<?> config) throws SalsaDeviceException, SalsaLoggingException {
        pauseScan(config, getDevicePreferencesAsContext());
    }

    @Deprecated
    public static void resumeScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaLoggingException {
        ScanApi.resumeScan(config, context);
    }

    @Deprecated
    public void resumeScan(IConfig<?> config) throws SalsaDeviceException, SalsaLoggingException {
        resumeScan(config, getDevicePreferencesAsContext());
    }

}

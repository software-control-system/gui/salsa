package fr.soleil.salsa.api.item;

/**
 * Report on a sensor from a Tango server.
 */
public class SensorReport extends DeviceReport {

    private static final long serialVersionUID = -8647106555708788785L;

}

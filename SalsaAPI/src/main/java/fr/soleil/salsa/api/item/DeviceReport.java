package fr.soleil.salsa.api.item;

import java.io.Serializable;

/**
 * Report on a device from a Tango server.
 */
public abstract class DeviceReport implements Serializable {

    private static final long serialVersionUID = 9163230182114049722L;

    /**
     * The dimension type of a device (scalar, spectrum, image).
     */
    private DimensionType dimensionType;

    /**
     * The data read from the device, in the case of a scalar value.
     */
    private Double readScalarData;

    /**
     * The data read from the device, in the case of a spectrum (1D array) value.
     */
    private Double[] readSpectrumData;

    /**
     * The data read from the device, in the case of an image (2D array) value.
     */
    private Double[][] readImageData;

    /**
     * The data that is currently being set on the device, in the case of a scalar value. Modifying
     * this property has no effect on the device.
     */
    private Double writeScalarData;

    /**
     * The data that is currently being set on the device, in the case of a spectrum (1D array)
     * value. Modifying this property has no effect on the device.
     */
    private Double[] writeSpectrumData;

    /**
     * The data that is currently being set on the device, in the case of an image (2D array) value.
     * Modifying this property has no effect on the device.
     */
    private Double[][] writeImageData;

    /**
     * True if the data can be read.
     */
    private boolean readable;

    /**
     * True if the data can be written.
     */
    private boolean writeable;

    /**
     * The state of the device.
     */
    private String state;

    /**
     * The quality of the device.
     */
    private String quality;

    /**
     * The data format.
     */
    private String format;

    /**
     * The dimension type of a device (scalar, spectrum, image).
     * 
     * @return the dimensionType
     */
    public DimensionType getDimensionType() {
        return dimensionType;
    }

    /**
     * The dimension type of a device (scalar, spectrum, image).
     * 
     * @param dimensionType the dimensionType to set
     */
    public void setDimensionType(DimensionType dimensionType) {
        this.dimensionType = dimensionType;
    }

    /**
     * The data read from the device, in the case of a scalar value.
     * 
     * @return the readScalarData
     */
    public Double getReadScalarData() {
        return readScalarData;
    }

    /**
     * The data read from the device, in the case of a scalar value.
     * 
     * @param readScalarData the readScalarData to set
     */
    public void setReadScalarData(Double readScalarData) {
        this.readScalarData = readScalarData;
    }

    /**
     * The data read from the device, in the case of a spectrum (1D array) value.
     * 
     * @return the readSpectrumData
     */
    public Double[] getReadSpectrumData() {
        return readSpectrumData;
    }

    /**
     * The data read from the device, in the case of a spectrum (1D array) value.
     * 
     * @param readSpectrumData the readSpectrumData to set
     */
    public void setReadSpectrumData(Double[] readSpectrumData) {
        this.readSpectrumData = readSpectrumData;
    }

    /**
     * The data read from the device, in the case of an image (2D array) value.
     * 
     * @return the readImageData
     */
    public Double[][] getReadImageData() {
        return readImageData;
    }

    /**
     * The data read from the device, in the case of an image (2D array) value.
     * 
     * @param readImageData the readImageData to set
     */
    public void setReadImageData(Double[][] readImageData) {
        this.readImageData = readImageData;
    }

    /**
     * The data that is currently being set on the device, in the case of a scalar value. Modifying
     * this property has no effect on the device.
     * 
     * @return the writeScalarData
     */
    public Double getWriteScalarData() {
        return writeScalarData;
    }

    /**
     * The data that is currently being set on the device, in the case of a scalar value. Modifying
     * this property has no effect on the device.
     * 
     * @param writeScalarData the writeScalarData to set
     */
    public void setWriteScalarData(Double writeScalarData) {
        this.writeScalarData = writeScalarData;
    }

    /**
     * The data that is currently being set on the device, in the case of a spectrum (1D array)
     * value. Modifying this property has no effect on the device.
     * 
     * @return the writeSpectrumData
     */
    public Double[] getWriteSpectrumData() {
        return writeSpectrumData;
    }

    /**
     * The data that is currently being set on the device, in the case of a spectrum (1D array)
     * value. Modifying this property has no effect on the device.
     * 
     * @param writeSpectrumData the writeSpectrumData to set
     */
    public void setWriteSpectrumData(Double[] writeSpectrumData) {
        this.writeSpectrumData = writeSpectrumData;
    }

    /**
     * The data that is currently being set on the device, in the case of an image (2D array) value.
     * Modifying this property has no effect on the device.
     * 
     * @return the writeImageData
     */
    public Double[][] getWriteImageData() {
        return writeImageData;
    }

    /**
     * The data that is currently being set on the device, in the case of an image (2D array) value.
     * Modifying this property has no effect on the device.
     * 
     * @param writeImageData the writeImageData to set
     */
    public void setWriteImageData(Double[][] writeImageData) {
        this.writeImageData = writeImageData;
    }

    /**
     * True if the data can be read.
     * 
     * @return the readable
     */
    public boolean isReadable() {
        return readable;
    }

    /**
     * True if the data can be read.
     * 
     * @param readable the readable to set
     */
    public void setReadable(boolean readable) {
        this.readable = readable;
    }

    /**
     * True if the data can be written.
     * 
     * @return the writeable
     */
    public boolean isWriteable() {
        return writeable;
    }

    /**
     * True if the data can be written.
     * 
     * @param writeable the writeable to set
     */
    public void setWriteable(boolean writeable) {
        this.writeable = writeable;
    }

    /**
     * The state of the device.
     * 
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * The state of the device.
     * 
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * The quality of the device.
     * 
     * @return the quality
     */
    public String getQuality() {
        return quality;
    }

    /**
     * The quality of the device.
     * 
     * @param quality the quality to set
     */
    public void setQuality(String quality) {
        this.quality = quality;
    }

    /**
     * The data format.
     * 
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * The data format.
     * 
     * @param format the format to set
     */
    public void setFormat(String format) {
        this.format = format;
    }
}

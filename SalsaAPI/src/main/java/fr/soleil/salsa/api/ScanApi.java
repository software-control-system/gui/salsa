package fr.soleil.salsa.api;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import fr.soleil.salsa.api.item.HistoricRecord;
import fr.soleil.salsa.config.ApplicationConfig;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.ScanState;
import fr.soleil.salsa.entity.impl.HistoricLogLine;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaLoggingException;
import fr.soleil.salsa.exception.SalsaScanConfigurationException;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;
import fr.soleil.salsa.locator.ILocator;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.IConfigService;
import fr.soleil.salsa.service.ILogService;
import fr.soleil.salsa.service.IScanService;

/**
 * The facade to control scans.
 * 
 * @author Administrateur
 * 
 */
public class ScanApi {

    public static final Logger LOGGER = LoggingUtil.getLogger(ScanApi.class);

    /**
     * The locator that seeks the required service(s).
     */
    private static ILocator locator;

    /**
     * The service that allows control over scans.
     */
    private static IScanService scanService;

    /**
     * The service that saves and loads configurations.
     */
    private static IConfigService configService;

    /**
     * The service that allows logs over scans.
     */
    private static ILogService logService;

    /**
     * Static initialization.
     */
    static {
        try {
            locator = ApplicationConfig.getLocator();
            Object o = locator.getService(IScanService.class);
            scanService = (IScanService) o;
            Object a = locator.getService(ILogService.class);
            logService = (ILogService) a;
            Object b = locator.getService(IConfigService.class);
            configService = (IConfigService) b;
        } catch (Exception e) {
            LOGGER.warn("Exception caught during service creation {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
    }

    /**
     * Loads a scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     * @throws SalsaScanConfigurationException
     */
    public static void loadScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        scanService.loadScan(config, context);
    }

    /**
     * Starts and logs a scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     * @throws SalsaScanConfigurationException
     */
    public static void startScan(final IConfig<?> config, final IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        Exception salsaException = null;
        try {
            scanService.startScan(config, context);
        } catch (Exception exception) {
            salsaException = exception;
            LOGGER.error(exception.getMessage());
            LOGGER.debug("Stack trace", exception);
        }
        (new Thread("log scan after start") {
            @Override
            public void run() {
                try {
                    sleep(1000);
                    // Must log later for waiting the first scan step
                    logService.logScan(config, context, ILogService.START_ACTION);
                } catch (Exception e) {
                    LOGGER.warn("Exception caught for log scan {}", e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            };
        }).start();

        if (salsaException instanceof SalsaDeviceException) {
            throw (SalsaDeviceException) salsaException;
        }
        if (salsaException instanceof SalsaScanConfigurationException) {
            throw (SalsaScanConfigurationException) salsaException;
        }
        if (salsaException != null) {
            throw new SalsaDeviceException("Cannot startScan " + salsaException.getMessage(), salsaException);
        }
    }

    /**
     * Begins a scan.
     * 
     * @param config parameters of the scan.
     * @param context the context of the scan.
     * @throws SalsaDeviceException
     * @throws ScanNotFoundException
     * @throws SalsaScanConfigurationException
     */
    public static void startScan(String fullPath, final IContext context)
            throws SalsaDeviceException, ScanNotFoundException, SalsaScanConfigurationException {
        final IConfig<?> config = scanService.getConfig(fullPath);
        startScan(config, context);
    }

    @Deprecated
    public static void stopScan(IConfig<?> config, IContext context) throws SalsaDeviceException {
        stopScan(context);
    }

    /**
     * Stops the scan.
     * 
     * @param context context of the scan.
     * @throws SalsaDeviceException
     */
    public static void stopScan(IContext context) throws SalsaDeviceException {
        Exception salsaException = null;
        try {
            scanService.stopScan(context);
        } catch (Exception exception) {
            salsaException = exception;
            LOGGER.error(exception.getMessage());
        }
        try {
            logService.logScan(null, context, ILogService.STOP_ACTION);
        } catch (SalsaLoggingException e) {
            LOGGER.warn("Exception caught for log scan {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        if (salsaException instanceof SalsaDeviceException) {
            throw (SalsaDeviceException) salsaException;
        }
        if (salsaException != null) {
            throw new SalsaDeviceException("Cannot stopScan " + salsaException.getMessage(), salsaException);
        }
    }

    @Deprecated
    public static void pauseScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaLoggingException {
        pauseScan(context);
    }

    /**
     * Pauses the scan.
     * 
     * @param context context of the scan.
     * @throws SalsaDeviceException
     */
    public static void pauseScan(IContext context) throws SalsaDeviceException, SalsaLoggingException {
        Exception salsaException = null;
        try {
            scanService.pauseScan(context);
        } catch (Exception exception) {
            salsaException = exception;
            LOGGER.error(exception.getMessage());
            LOGGER.debug("Stack trace", exception);
        }

        try {
            logService.logScan(null, context, ILogService.PAUSE_ACTION);
        } catch (SalsaLoggingException e) {
            LOGGER.warn("Exception caught for log scan {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        if (salsaException instanceof SalsaDeviceException) {
            throw (SalsaDeviceException) salsaException;
        }
        if (salsaException != null) {
            throw new SalsaDeviceException("Cannot pauseScan " + salsaException.getMessage(), salsaException);
        }
    }

    @Deprecated
    public static void resumeScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaLoggingException {
        resumeScan(context);
    }

    /**
     * Resumes the scan.
     * 
     * @param context context of the scan.
     * @throws SalsaDeviceException
     */
    public static void resumeScan(IContext context) throws SalsaDeviceException, SalsaLoggingException {
        Exception salsaException = null;
        try {
            scanService.resumeScan(context);
        } catch (Exception exception) {
            salsaException = exception;
            LOGGER.error(exception.getMessage());
        }

        try {
            logService.logScan(null, context, ILogService.RESUME_ACTION);
        } catch (SalsaLoggingException e) {
            LOGGER.warn("Exception caught for log scan {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        if (salsaException instanceof SalsaDeviceException) {
            throw (SalsaDeviceException) salsaException;
        }
        if (salsaException != null) {
            throw new SalsaDeviceException("Cannot resumeScan " + salsaException.getMessage(), salsaException);
        }
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     * @throws SalsaScanConfigurationException
     * @throws ScanDeviceException
     * @throws SalsaScanConfigurationException
     */
    public static IScanResult readScanResult(String scanServerName)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        return readScanResult(scanServerName, false);
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @param scanServerName the name of the scan server to use.
     * @param read the real trajectory or not
     * @return
     * @throws SalsaScanConfigurationException
     * @throws ScanDeviceException
     * @throws SalsaScanConfigurationException
     */
    public static IScanResult readScanResult(String scanServerName, boolean withTrajectories)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        return scanService.readScanResult(scanServerName, withTrajectories);
    }

    /**
     * Checks if a scan result can be read.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     */
    public static boolean isScanResultReady(String scanServerName) throws SalsaDeviceException {
        return scanService.isScanResultReady(scanServerName);
    }

    /**
     * Returns the state of the scan.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    public static ScanState getScanState(String scanServerName) throws SalsaDeviceException {
        return scanService.getScanState(scanServerName);
    }

    /**
     * Reads the historic.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     */
    public static List<HistoricRecord> getHistoric(String scanServerName) throws SalsaDeviceException {
        return scanService.getHistoric(scanServerName);
    }

    /**
     * Clears the historic on the scan server.
     * 
     * @param scanServerName
     * @throws SalsaDeviceException
     */
    public static void clearHistoric(String scanServerName) throws SalsaDeviceException {
        scanService.clearHistoric(scanServerName);
    }

    /**
     * Clears the historic log.
     * 
     * @param context
     * @throws SalsaDeviceException
     */
    public static void clearHistoricLog(IContext context) throws SalsaDeviceException, SalsaLoggingException {
        logService.clearHistoricLog(context);
    }

    /**
     * Reads the historic log.
     * 
     * @param context
     * @throws SalsaDeviceException
     * @throws SalsaLoggingException
     * @return
     */
    public static ArrayList<HistoricLogLine> loadHistoricLog(IContext context)
            throws SalsaDeviceException, SalsaLoggingException {
        return logService.loadHistoricLog(context);
    }

    /**
     * Performs a scan function.
     * 
     * @param scanServerName the name of the scan server to use.
     * @param behaviour the function to be done.
     * @param sensor function parameter : sensor. Null if the function has no sensor parameter.
     * @param actuator function parameter : actuator. Null id the function has no actuator
     *            parameter.
     * @throws SalsaDeviceException
     */
    public static void doScanFunction(String scanServerName, Behaviour behaviour, ISensor sensor, IActuator actuator)
            throws SalsaDeviceException {
        scanService.doScanFunction(scanServerName, behaviour, sensor, actuator);
    }

    /**
     * Gets suggestions from scanserver properties for devices such as sensors, actuators,
     * timebases.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @return
     * @throws SalsaDeviceException
     */

    public static ISuggestions getDevicesSuggestions(String scanServerName) throws SalsaDeviceException {
        return scanService.getDevicesSuggestions(scanServerName);
    }

    /**
     * Logs the scan when it ends.
     * 
     * @param context of the scan.
     * @throws SalsaLoggingException
     */
    public static void endLogScan(IConfig<?> config, IContext context) throws SalsaLoggingException {
        logService.endLogScan(config, context);
    }

    /**
     * Loads the config by path.
     * 
     * @param path of the scan.
     * @throws PersistenceException
     * @throws ScanNotFoundException
     */
    public static IConfig<?> loadConfigByPath(String path) throws PersistenceException, ScanNotFoundException {
        IConfig<?> config = configService.loadConfigByPath(path);
        return config;
    }

    /**
     * Loads the config by id.
     * 
     * @param id of the scan.
     * @throws PersistenceException
     * @throws ScanNotFoundException
     */
    public static IConfig<?> loadConfigById(Integer id) throws PersistenceException, ScanNotFoundException {
        IConfig<?> config = configService.loadConfigById(id);
        return config;
    }

    /**
     * set the scan server in partial recorder mode to allow an external system to manage the data
     * recording.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @param mode = true allow external system to manage the data recording
     * @return
     * @throws SalsaDeviceException
     */
    public static void setDataRecorderPartialMode(String scanServerName, boolean mode) throws SalsaDeviceException {
        scanService.setDataRecorderPartialMode(scanServerName, mode);
    }

    /**
     * set the data recording enable
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @param enable = true allow the data recording
     * @return
     * @throws SalsaDeviceException
     */
    public static void setDataRecorderEnable(boolean enable) throws SalsaDeviceException {
        scanService.setDataRecorderEnable(enable);
    }

    public static boolean isDataRecorderEnable() throws SalsaDeviceException {
        return scanService.isDataRecorderEnable();
    }

}

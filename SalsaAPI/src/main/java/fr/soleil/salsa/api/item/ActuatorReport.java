package fr.soleil.salsa.api.item;

/**
 * Report on an actuator from a Tango server.
 */
public class ActuatorReport extends DeviceReport {

    private static final long serialVersionUID = 1726157110735898787L;

}

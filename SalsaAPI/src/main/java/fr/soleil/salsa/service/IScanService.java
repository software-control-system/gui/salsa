package fr.soleil.salsa.service;

import java.util.List;

import fr.soleil.salsa.api.item.HistoricRecord;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.ScanState;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaScanConfigurationException;
import fr.soleil.salsa.exception.ScanNotFoundException;

/**
 * The service that allows control over scans.
 * 
 * @author Administrateur
 * 
 */
public interface IScanService {

    /**
     * Loads a scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     * @throws SalsaScanConfigurationException
     */
    public void loadScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException;

    /**
     * Starts and logs a scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     * @throws SalsaScanConfigurationException
     */
    public void startScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException;

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     * @throws ScanDeviceException
     * @throws SalsaScanConfigurationException
     */
    public IScanResult readScanResult(String scanServerName)
            throws SalsaDeviceException, SalsaScanConfigurationException;

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @param scanServerName the name of the scan server to use.
     * @param read the real trajectory or not
     * @return
     * @throws ScanDeviceException
     * @throws SalsaScanConfigurationException
     */
    public IScanResult readScanResult(String scanServerName, boolean withTrajectories)
            throws SalsaDeviceException, SalsaScanConfigurationException;

    /**
     * Stops and logs the scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     * @throws
     */
    public void stopScan(IContext context) throws SalsaDeviceException;

    /**
     * Pauses and logs the scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     */
    public void pauseScan(IContext context) throws SalsaDeviceException;

    /**
     * Resumes and logs the scan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaDeviceException
     */
    public void resumeScan(IContext context) throws SalsaDeviceException;

    /**
     * Checks if a scan result can be read.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     */
    public boolean isScanResultReady(String scanServerName) throws SalsaDeviceException;

    /**
     * Returns the state of the scan.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    public ScanState getScanState(String scanServerName) throws SalsaDeviceException;

    /**
     * Reads the historic.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     */
    public List<HistoricRecord> getHistoric(String scanServerName) throws SalsaDeviceException;

    /**
     * Clears the historic on the scan server.
     * 
     * @param scanServerName
     * @throws SalsaDeviceException
     */
    public void clearHistoric(String scanServerName) throws SalsaDeviceException;

    /**
     * Performs a scan function.
     * 
     * @param scanServerName the name of the scan server to use.
     * @param behaviour the function to be done.
     * @param sensor function parameter : sensor. Null if the function has no sensor parameter.
     * @param actuator function parameter : actuator. Null id the function has no actuator
     *            parameter.
     * @throws SalsaDeviceException
     */
    public void doScanFunction(String scanServerName, Behaviour behaviour, ISensor sensor, IActuator actuator)
            throws SalsaDeviceException;

    /**
     * Gets suggestions from scanserver properties for devices such as sensors, actuators,
     * timebases.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @return
     * @throws SalsaDeviceException
     */
    public ISuggestions getDevicesSuggestions(String scanServerName) throws SalsaDeviceException;

    /**
     * Recovers an {@link IConfig} from a path.
     * 
     * @param path The path.
     * @return An {@link IConfig}.
     * @throws ScanNotFoundException If there is no config matching given path.
     */
    public IConfig<?> getConfig(String path) throws ScanNotFoundException;

    /**
     * Begins a scan.
     * 
     * @param config parameters of the scan.
     * @param context the context of the scan.
     * @throws SalsaDeviceException
     * @throws ScanNotFoundException
     * @throws SalsaScanConfigurationException
     */
    public IConfig<?> startScan(String path, IContext context)
            throws SalsaDeviceException, ScanNotFoundException, SalsaScanConfigurationException;

    /**
     * set the scan server in partial recorder mode to allow an external system to manage the data
     * recording.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @param mode = true allow external system to manage the data recording
     * @return
     * @throws SalsaDeviceException
     */
    public void setDataRecorderPartialMode(String scanServerName, boolean mode) throws SalsaDeviceException;

    /**
     * set the scan server in partial recorder mode to allow an external system to manage the data
     * recording.
     * 
     * @param scanServerName The scan server name (Can not be null)
     * @param enable = true activate the data recording
     * @return
     * @throws SalsaDeviceException
     */
    public void setDataRecorderEnable(boolean enable) throws SalsaDeviceException;

    public boolean isDataRecorderEnable() throws SalsaDeviceException;

}
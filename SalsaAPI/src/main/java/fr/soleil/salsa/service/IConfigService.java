package fr.soleil.salsa.service;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;

/**
 * The service that saves and load the configurations and directories .
 * 
 * @author Boualem ABDELLI
 * 
 */
public interface IConfigService {

    public IDirectory getRootDirectory();

    /**
     * Gets the config by id
     * 
     * @param id
     * @return IConfig
     */
    public IConfig<?> getConfigById(Integer id) throws ScanNotFoundException;

    /**
     * Gets the config by path
     * 
     * @param path
     * @return IConfig
     */
    public IConfig<?> getConfigByPath(String path) throws ScanNotFoundException;

    /**
     * Gets the config by path, if something changed since last loading
     * 
     * @param path The config path
     * @param lastLoadedConfig The last config loaded with the same path
     * @return An {@link IConfig}. If no change occurred since last loading, it returns
     *         <code>lastLoadedConfig</code>. Otherwise, it returns the config loaded from database.
     * @throws ScanNotFoundException If no config corresponds to the given path.
     */
    public IConfig<?> getConfigByPath(String path, IConfig<?> lastLoadedConfig)
            throws ScanNotFoundException;

    /**
     * Save configuration
     * 
     * @param config the configuration to save.
     */
    public IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException,
            ScanNotFoundException;

    /**
     * Save Directory
     * 
     * @param the directory to save.
     */
    public IDirectory saveDirectory(IDirectory directory) throws PersistenceException;

    /**
     * Delete Config
     * 
     * @param the config to delete.
     */
    public void deleteConfig(IConfig<?> config) throws PersistenceException;

    /**
     * Delete Directory
     * 
     * @param the directory to delete.
     */
    public void deleteDirectory(IDirectory directory) throws PersistenceException;

    /**
     * Loads configuration by path
     * 
     * @param path of the config to load.
     */
    public IConfig<?> loadConfigByPath(String path) throws PersistenceException,
            ScanNotFoundException;

    /**
     * Loads configuration by id
     * 
     * @param id of the config to load.
     */
    public IConfig<?> loadConfigById(Integer id) throws PersistenceException, ScanNotFoundException;

}

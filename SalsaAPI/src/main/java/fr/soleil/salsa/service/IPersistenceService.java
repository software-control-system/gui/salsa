package fr.soleil.salsa.service;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.OutOfSyncException;
import fr.soleil.salsa.exception.persistence.PersistenceException;

/**
 * 
 * Class relative to the persistence of scan configurations and directory.
 * 
 * @author Alike
 * 
 */
public interface IPersistenceService {

    /**
     * Provided the arborescent structure of scan configuration and directories.
     * 
     * Do not provide with fully usabled configuration (just id and scan type) to .
     * 
     * @return the root of the arborescent structure
     */
    public IDirectory getRootDirectory();

    /**
     * Gets the config by id
     * 
     * @param id
     * @return IConfig
     * @throws ScanNotFoundException If no config matches the given id
     */
    public IConfig<?> getConfigById(Integer id) throws ScanNotFoundException;

    /**
     * Saves configuration
     * 
     * Provide the configuration id and the associated timestamp.
     * 
     * 
     * On save operation, the timestamp of the parent directory is updated. This new timestamp is
     * provided in the result.
     * 
     * 
     * 
     * @param config the configuration to save.
     * 
     */
    public IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException,
            ScanNotFoundException;

    /**
     * Save or update a directory. If the id is specified, then update is done, else create. (i.e if
     * you want copy a directory, you can set the id to null).
     * 
     * If the id is not specified, the parent id must be specified.
     * 
     * ParentDirectory can not be updated, this kind of modification require creation of a new
     * directory.
     * 
     * Note : If the new directory contains config or subdirectories, they will not be saved.
     * 
     * @param directory
     * @return
     * @throws PersistenceException
     */
    public IDirectory saveDirectory(IDirectory directory) throws PersistenceException;

    /**
     * Delete a configuration.
     * 
     * Require the id and the timestamp.
     * 
     * @param the config to delete.
     * @throws OutOfSyncException when the config has been modified or deleted.
     */
    public void deleteConfig(IConfig<?> config) throws PersistenceException;

    /**
     * Delete Directory
     * 
     * @param the directory to delete.
     */
    public void deleteDirectory(IDirectory directory) throws PersistenceException;

}

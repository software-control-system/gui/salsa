package fr.soleil.salsa.service;

import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.exception.SalsaDeviceException;

/**
 * @author Alike
 * 
 *         Interface relative to the scan status informations retrieving.
 * 
 */
public interface IScanStatusService {

    /**
     * Gets several scan status informations.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    IScanStatus getScanServerStatus(String scanServerName) throws SalsaDeviceException;

}

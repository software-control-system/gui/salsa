package fr.soleil.salsa.service;

import java.util.ArrayList;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.impl.HistoricLogLine;
import fr.soleil.salsa.exception.SalsaLoggingException;

public interface ILogService {

    public final static String START_ACTION = "start";
    public final static String STOP_ACTION = "stop";
    public final static String RESUME_ACTION = "resume";
    public final static String PAUSE_ACTION = "pause";

    /**
     * Clears the historic log.
     * 
     * @param context context of the scan.
     * @throws SalsaLoggingException
     */
    public void clearHistoricLog(IContext context) throws SalsaLoggingException;

    /**
     * Reads historic line.
     * 
     * @param context context of the scan.
     * @throws SalsaLoggingException
     */
    public ArrayList<HistoricLogLine> loadHistoricLog(IContext context)
            throws SalsaLoggingException;

    /**
     * Begins a logscan.
     * 
     * @param config parameters of the scan.
     * @param context context of the scan.
     * @throws SalsaLoggingException If a problem occurred during Scan loging
     */
    public void logScan(IConfig<?> config, IContext context, String action)
            throws SalsaLoggingException;

    /**
     * Logs a scan when it ends.
     * 
     * @param context context of the scan.
     * @throws SalsaLoggingException
     */
    public void endLogScan(IConfig<?> config, IContext context) throws SalsaLoggingException;

}

package fr.soleil.salsa.service;

import fr.soleil.salsa.api.ConfigApi;
import fr.soleil.salsa.copier.AutoCopier;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;

/**
 * @author alikedba
 */
public class ConfigAPIService {

    public static IConfig<?> getConfigById(Integer id) throws ScanNotFoundException {
        IConfig<?> resultImpl = ConfigApi.getConfigById(id);
        IConfig<?> resultModel = AutoCopier.copy(resultImpl);
        return resultModel;

    }

    public static IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException, ScanNotFoundException {
        IConfig<?> result = config;
        if (config.isModified()) {
            IConfig<?> configImpl = AutoCopier.copyToImpl(config);
            IConfig<?> resultImpl = ConfigApi.saveConfig(configImpl);
            resultImpl.setModified(false);
            result = AutoCopier.copy(resultImpl);
        }
        return result;
    }

    public static IDirectory saveDirectory(IDirectory directoryModel) throws PersistenceException {
        IDirectory directoryImpl = AutoCopier.copyDirectoryToImpl(directoryModel);
        IDirectory resultImpl = ConfigApi.saveDirectory(directoryImpl);
        IDirectory resultModel = AutoCopier.copyDirectoryToModel(resultImpl);
        return resultModel;
    }

    public static IDirectory getRootDirectory() {
        IDirectory rootImpl = ConfigApi.getRootDirectory();
        IDirectory rootModel = AutoCopier.copyDirectoryToModel(rootImpl);
        return rootModel;
    }

}

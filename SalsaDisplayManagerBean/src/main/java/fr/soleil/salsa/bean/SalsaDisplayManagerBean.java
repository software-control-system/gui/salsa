package fr.soleil.salsa.bean;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import fr.soleil.bean.scanserver.ICurrentScanConfigurationBeanListener;
import fr.soleil.salsa.view.DisplayManagerBean;

public class SalsaDisplayManagerBean extends AbstractSalsaBean {

    private static final long serialVersionUID = 1981233193441805229L;

    private DisplayManagerBean bean;
    private boolean firstLoad;
    private boolean firstUnload;

    public SalsaDisplayManagerBean() {
        super();
        bean = new DisplayManagerBean(false, false);
        firstLoad = true;
        firstUnload = true;

        setLayout(new BorderLayout());
        add(bean, BorderLayout.CENTER);
    }

    public boolean isDataFitterBeanVisible() {
        return bean.isDataFitterBeanVisible();
    }

    public void setDataFitterBeanVisible(boolean dataFitterBeanVisible) {
        bean.setDataFitterBeanVisible(dataFitterBeanVisible);
    }

    public void addCurrentScanConfigurationBeanListener(ICurrentScanConfigurationBeanListener listener) {
        bean.addCurrentScanConfigurationBeanListener(listener);
    }

    public void removeCurrentScanConfigurationBeanListener(ICurrentScanConfigurationBeanListener listener) {
        bean.removeCurrentScanConfigurationBeanListener(listener);
    }

    @Override
    public void loadConfig() {
        super.loadConfig();
        if (firstLoad) {
            firstLoad = false;
            bean.initPreferences();
        }
    }

    @Override
    public void unloadConfig() {
        super.unloadConfig();
        if (firstUnload) {
            firstUnload = false;
            bean.stop();
        }
    }

    // Test without config
    public static void configureBeanWithoutConfig(SalsaDisplayManagerBean bean) {
        bean.setCurrentConfigAtLoad(false);
        bean.setCurrentConfigAtNewScan(false);
    }

    // Test with config at lod
    public static void configureBeanWithConfigAtLoad(SalsaDisplayManagerBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(false);
    }

    public static void configureBeanWithConfigAtNewScan(SalsaDisplayManagerBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(true);
    }

    public static void configureBeanWithConfigAtNewScanAndReload(SalsaDisplayManagerBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(true);
        bean.setReloadConfigAtRead(true);
    }

    /**
     * Main test
     * 
     * @param args
     */
    public static void main(String[] args) {
        SalsaDisplayManagerBean bean = new SalsaDisplayManagerBean();
        configureBeanWithoutConfig(bean);
        // configureBeanWithConfigAtLoad(bean);
        // configureBeanWithConfigAtNewScan(bean);
        // configureBeanWithConfigAtNewScanAndReload(bean);
        SalsaDisplayManagerBean panel = new SalsaDisplayManagerBean();
        bean.loadConfig();
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("SalsaDisplayManagerBean");
        frame.setContentPane(bean);
        frame.setContentPane(panel);
        frame.setSize(600, 500);
        frame.setVisible(true);
    }

}

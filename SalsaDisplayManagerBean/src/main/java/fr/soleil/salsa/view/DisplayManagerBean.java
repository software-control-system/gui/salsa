package fr.soleil.salsa.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import fr.soleil.bean.scanserver.CurrentScanConfigurationBean;
import fr.soleil.bean.scanserver.DataFitterConfigurationBean;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.border.CometeTitledBorder;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class DisplayManagerBean extends CurrentScanConfigurationBean {

    private static final long serialVersionUID = -4360675548662392206L;

    protected static final BatchExecutor EXECUTOR = new BatchExecutor();
    protected static final Font PROXY_TITLE_FONT, PROXY_TITLE_HIGHLIGHT_FONT;
    static {
        Font font, highlightFont;
        Border border = DataFitterConfigurationBean.PROXY_PANEL.getBorder();
        if (border instanceof CometeTitledBorder) {
            CometeTitledBorder ctBorder = (CometeTitledBorder) border;
            font = ctBorder.getTitleFont();
            Map<TextAttribute, ?> tmpAttributes = font.getAttributes();
            Map<TextAttribute, Object> attributes = new HashMap<>();
            attributes.putAll(tmpAttributes);
            attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
            highlightFont = new Font(attributes);
        } else {
            font = null;
            highlightFont = null;
        }
        PROXY_TITLE_FONT = font;
        PROXY_TITLE_HIGHLIGHT_FONT = highlightFont;
        MouseAdapter adapter = new MouseAdapter() {

            protected boolean isTitleZone(MouseEvent e, CometeTitledBorder ctBorder) {
                boolean titleZone;
                if (e == null || ctBorder == null) {
                    titleZone = false;
                } else {
                    if (DataFitterConfigurationBean.DEFAULT_TITLE.equals(ctBorder.getTitle())) {
                        titleZone = false;
                    } else {
                        Rectangle bounds = ctBorder.getLabelBounds(DataFitterConfigurationBean.PROXY_PANEL, 0, 0,
                                DataFitterConfigurationBean.PROXY_PANEL.getWidth(),
                                DataFitterConfigurationBean.PROXY_PANEL.getHeight());
                        titleZone = bounds != null & bounds.contains(e.getPoint());
                    }
                }
                return titleZone;
            }

            protected void treatBorderMouseEvent(MouseEvent e) {
                Border border = DataFitterConfigurationBean.PROXY_PANEL.getBorder();
                if (border instanceof CometeTitledBorder) {
                    CometeTitledBorder ctBorder = (CometeTitledBorder) border;
                    Font font;
                    if (isTitleZone(e, ctBorder)) {
                        font = PROXY_TITLE_HIGHLIGHT_FONT;
                    } else {
                        font = PROXY_TITLE_FONT;
                    }
                    if (!ObjectUtils.sameObject(font, ctBorder.getTitleFont())) {
                        ctBorder.setTitleFont(font);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                treatBorderMouseEvent(e);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Border border = DataFitterConfigurationBean.PROXY_PANEL.getBorder();
                if (border instanceof CometeTitledBorder) {
                    ((CometeTitledBorder) border).setTitleFont(PROXY_TITLE_FONT);
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                treatBorderMouseEvent(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    String controlPanel = UIPreferences.getInstance().getControlPanel();
                    if ((controlPanel != null) && !controlPanel.trim().isEmpty()) {
                        Border border = DataFitterConfigurationBean.PROXY_PANEL.getBorder();
                        if (border instanceof CometeTitledBorder) {
                            CometeTitledBorder ctBorder = (CometeTitledBorder) border;
                            if ((!DataFitterConfigurationBean.DEFAULT_TITLE.equals(ctBorder.getTitle()))
                                    && isTitleZone(e, ctBorder)) {
                                EXECUTOR.setBatch(controlPanel);
                                EXECUTOR.setBatchParameters(ctBorder.getTitle());
                                EXECUTOR.execute();
                            }
                        }
                    }
                }
            }

        };
        DataFitterConfigurationBean.PROXY_PANEL.addMouseListener(adapter);
        DataFitterConfigurationBean.PROXY_PANEL.addMouseMotionListener(adapter);

    }

    private static final Icon WARNING_ICON = Icons.getIcon("salsa.warning.big");

    private static final String OK = "OK";
    private static final String DEVICE_ERROR_TITLE = "WARNING : SALSA will not work";
    private static final String DEVICE_ERROR_MESSAGE = "Scanserver device %s is down, please restart it!";
    private static final String UNKNOWN = "UNKNOWN";

    private String dataFitterName;
    private boolean warningShown;
    private long scanErrorTms;
    private boolean showError;
    // Show a non modal dialog
    private JDialog errorDialog;
    private JPanel errorPanel;
    private JLabel errorMessage, errorIcon;
    private JButton okButton;

    public DisplayManagerBean() {
        this(true, true);
    }

    public DisplayManagerBean(final boolean showError, final boolean autostart) {
        super(Boolean.valueOf(UIPreferences.getInstance().isShowImageAsSpectrumStack()));
        this.showError = showError;
        scanErrorTms = 0;
        warningShown = false;
        dataFitterName = null;
        if (autostart) {
            initPreferences();
        }
    }

    public void initPreferences() {
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        if (devicePreferences != null) {
            String scanServerName = devicePreferences.getScanServer();
            setScanServerName(scanServerName);
            dataFitterName = devicePreferences.getDataFitter();
            setDataFitterDeviceName(dataFitterName == null || dataFitterName.trim().isEmpty() ? null : dataFitterName);

            CurrentScanDataModel.setScanPropertiesMap(UIPreferences.getInstance().getCurrentScanPropertiesMap());

            if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
                setScanServerDeviceName(scanServerName.toLowerCase());
                start();
            }
        }
    }

    private void showErrorDialog(final String title, final String message) {
        if (errorDialog == null) {
            errorDialog = new JDialog(CometeUtils.getFrameForComponent(this), false);
            errorDialog.setAlwaysOnTop(true);
            errorPanel = new JPanel(new GridBagLayout());
            errorIcon = new JLabel(WARNING_ICON);
            GridBagConstraints errorIconConstraints = new GridBagConstraints();
            errorIconConstraints.fill = GridBagConstraints.NONE;
            errorIconConstraints.gridx = 0;
            errorIconConstraints.gridy = 0;
            errorIconConstraints.weightx = 0;
            errorIconConstraints.weighty = 0;
            errorIconConstraints.anchor = GridBagConstraints.NORTHWEST;
            errorPanel.add(errorIcon, errorIconConstraints);
            errorMessage = new JLabel(message);
            errorMessage.setHorizontalAlignment(SwingConstants.CENTER);
            errorMessage.setVerticalAlignment(SwingConstants.CENTER);
            GridBagConstraints errorMessageConstraints = new GridBagConstraints();
            errorMessageConstraints.fill = GridBagConstraints.BOTH;
            errorMessageConstraints.gridx = 0;
            errorMessageConstraints.gridy = 1;
            errorMessageConstraints.weightx = 1;
            errorMessageConstraints.weighty = 1;
            errorMessageConstraints.insets = new Insets(5, 10, 5, 10);
            errorPanel.add(errorMessage, errorMessageConstraints);
            okButton = new JButton(OK);
            okButton.addActionListener((e) -> {
                hideErrorDialog();
            });
            errorIcon = new JLabel(WARNING_ICON);
            GridBagConstraints okButtonConstraints = new GridBagConstraints();
            okButtonConstraints.fill = GridBagConstraints.NONE;
            okButtonConstraints.gridx = 0;
            okButtonConstraints.gridy = 2;
            okButtonConstraints.weightx = 0;
            okButtonConstraints.weighty = 0;
            okButtonConstraints.anchor = GridBagConstraints.CENTER;
            errorPanel.add(okButton, okButtonConstraints);
            errorDialog.setContentPane(errorPanel);
            errorDialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        } else {
            errorMessage.setText(message);
        }
        if (!errorDialog.isVisible()) {
            errorDialog.pack();
            if (isShowing()) {
                errorDialog.setLocationRelativeTo(this);
            } else if (errorDialog.getOwner() != null && errorDialog.getOwner().isShowing()) {
                errorDialog.setLocationRelativeTo(errorDialog.getOwner());
            }
        }
        errorDialog.setTitle(title);
        errorDialog.setVisible(true);
    }

    private boolean isErrorDialogVisible() {
        return (errorDialog != null) && errorDialog.isVisible();
    }

    private void hideErrorDialog() {
        scanErrorTms = System.currentTimeMillis();
        errorDialog.setVisible(false);
    }

    @Override
    public void stateChanged(final String state) {
        super.stateChanged(state);
        boolean invalidState = ((!SalsaUtils.isDefined(state)) || UNKNOWN.equalsIgnoreCase(state)
                || StringScalarBox.DEFAULT_ERROR_STRING.equals(state));
        // display connection error popup if not already displayed and if connection error lasts since more than 10s
        if (showError && invalidState && !warningShown && !isErrorDialogVisible()) {
            long currentTms = System.currentTimeMillis();
            boolean errorShown = warningShown;
            if ((scanErrorTms == 0) || ((currentTms - scanErrorTms) > 10000)) {
                String scanServerName = getModel();
                if (SalsaUtils.isDefined(scanServerName)) {
                    if (scanErrorTms == 0) {
                        scanErrorTms = currentTms;
                    } else {
                        errorShown = true;
                        scanErrorTms = currentTms;
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            showErrorDialog(DEVICE_ERROR_TITLE, String.format(DEVICE_ERROR_MESSAGE, getModel()));
                        });
                    }
                } else {
                    scanErrorTms = 0;
                }
            }
            warningShown = errorShown;
        } else if (!invalidState) {
            // hide popup if no connection error
            warningShown = false;
            if (isErrorDialogVisible()) {
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    hideErrorDialog();
                });
            }
        }
    }

    public void refreshUIPreferences() {
        UIPreferences.getInstance().setCurrentScanPropertiesMap(CurrentScanDataModel.getScanPropertiesMap());
    }

}

package fr.soleil.salsa.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.ActuatorImpl;
import fr.soleil.salsa.entity.impl.SensorImpl;
import fr.soleil.salsa.entity.impl.TimebaseImpl;
import fr.soleil.salsa.entity.impl.scan1d.Config1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Dimension1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Range1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Trajectory1DImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;

public class ConfigDAOTest {

    public static void main(String[] args) {
        System.out.println("ConfigDAO.main()");
        // ConfigDAO dao = new ConfigDAO();

        System.out.println("TestsaveConfig : " + TestSaveConfig());
        /*
         * System.out.println("TestCreateNewDirectoryAtRoot : " +
         * TestCreateNewDirectoryAtRoot());
         * System.out.println("TestupdateConfig : " + TestUpdateConfig());
         * System.out.println("TestDeleteConfigAtRoot : " +
         * TestDeleteConfigAtRoot());
         * System.out.println("TestCreateConfigDeeply : " +
         * TestCreateConfigDeeply());
         * System.out.println("TestDeleteEmptyDirectoryAtRoot : " +
         * TestDeleteEmptyDirectoryAtRoot());
         * System.out.println("TestDeleteDirectoryRecursivelyWithoutConfig : " +
         * TestDeleteDirectoryRecursivelyWithoutConfig());
         * System.out.println("TestDeleteDirectoryRecursivelyWithConfig : " +
         * TestDeleteDirectoryRecursivelyWithConfig()); System.out
         * .println("TestDeleteDirectoryRecursivelyWithConfigAtomicity : " +
         * TestDeleteDirectoryRecursivelyWithConfigAtomicity());
         */
    }

    private static IConfig1D createDummyConfig1D(int _nbActuators, int _nbSensors, int _nbRanges) {

        Config1DImpl nc = new Config1DImpl();
        nc.setName("TST_NouvelleConfigAuto");
        nc.setScanNumber(666);

        nc.setTimebaseList(new ArrayList<ITimebase>());
        TimebaseImpl tb = new TimebaseImpl();
        tb.setName("timebase01");
        tb.setEnabled(true);
        nc.getTimebaseList().add(tb);
        // Sensors

        for (int i = 0; i < _nbSensors; i++) {
            ISensor s = new SensorImpl();
            s.setName("/mon/sensor/" + i);
            s.setEnabled(i % 2 == 1);
            nc.getSensorsList().add(s);
        }
        // Dimension
        Dimension1DImpl dim = new Dimension1DImpl();
        dim.setTrajectoriesList(new ArrayList<ITrajectory1D>());
        nc.setDimensionX(dim);

        dim.setRangesXList(new ArrayList<IRange1D>());
        for (int i = 0; i < _nbRanges; i++) {
            IRange1D r = new Range1DImpl();
            r.setDimension(dim);
            r.setStepsNumber(100);
            double[] intTime = new double[101];
            Arrays.fill(intTime, 4.5);
            r.setIntegrationTime(intTime);
            List<ITrajectory> traj = new ArrayList<>();
            r.setTrajectoriesList(traj);
            dim.getRangesXList().add(r);
        }

        // Actuators
        for (int i = 0; i < _nbActuators; i++) {
            IActuator a = new ActuatorImpl();
            a.setName("/mon/actuator/" + i);
            a.setEnabled(i % 2 == 0);
            dim.getActuatorsList().add(a);
        }

        for (int i = 0; i < dim.getRangesXList().size(); i++) {
            for (int j = 0; j < dim.getActuatorsList().size(); j++) {
                IActuator a = dim.getActuatorsList().get(j);
                IRange1D r = dim.getRangesXList().get(i);

                ITrajectory1D t = new Trajectory1DImpl();
                t.setBeginPosition(0.0);
                t.setEndPosition(4.0);
                t.setActuator(a);
                t.setRange(r);
                double[] integration = new double[t.getTrajectory().length];
                Arrays.fill(integration, 5.0);
                r.setIntegrationTime(integration);
                r.getTrajectoriesList().add(t);
                dim.getTrajectoriesList().add(t);

            }
        }

        return nc;

    }

    public static void showScan(IConfig<?> _c, int _d) throws ScanNotFoundException {

        // IConfig1D _c1 = (IConfig1D) _c;

        ConfigDAO dao = new ConfigDAO();
        IConfig1D c1 = (IConfig1D) dao.getConfigById(_c.getId());

        indent("**" + c1.getName(), _d);
        indent("  id : " + c1.getId(), _d);
        indent("  scanNumber : " + c1.getScanNumber(), _d);

        IDimension1D X = c1.getDimensionX();
        if (X != null) {
            for (int i = 0; i < X.getActuatorsList().size(); i++) {
                IActuator actuator = X.getActuatorsList().get(i);
                indent("  actuator" + i + " : " + actuator.getName() + (actuator.isEnabled() ? " (on)" : " (off)"), _d);
            }
        }

        for (int i = 0; i < c1.getSensorsList().size(); i++) {
            ISensor sensor = c1.getSensorsList().get(i);
            indent("  sensor" + i + " : " + sensor.getName() + (sensor.isEnabled() ? " (on)" : " (off)"), _d);
        }
        indent("  timestamp : " + _c.getTimestamp(), _d);

        /*
         * System.out.println("\tscanNumber" + _c.get);
         * System.out.println("\tscanNumber" + _c.getScanNumber());
         */
    }

    public static void indent(String _str, int _d) {
        for (int i = 0; i < _d; i++) {
            System.out.print("\t");
        }
        System.out.println(_str);
    }

    public static void show(IDirectory _d, int _depth) throws ScanNotFoundException {

        for (int i = 0; i < _depth; i++) {
            System.out.print("\t");
        }

        System.out.println("/" + _d.getName() + "/");
        List<IDirectory> subs = _d.getSubDirectoriesList();
        Iterator<IDirectory> i = subs.iterator();
        while (i.hasNext()) {
            IDirectory child = i.next();
            show(child, _depth + 1);
        }

        List<IConfig<?>> configs = _d.getConfigList();
        Iterator<IConfig<?>> i2 = configs.iterator();
        while (i2.hasNext()) {
            IConfig<?> config = i2.next();
            System.out.println("/" + _d.getName() + "/");
            System.out.println(config.getName());
            showScan(config, _depth);
        }
    }

    /*
     * private static boolean TestCreateNewDirectoryAtRoot() { String name =
     * "TST_newDir";
     * 
     * ConfigDAO dao = new ConfigDAO(); IDirectory root =
     * dao.getRootDirectory();
     * 
     * IDirectory newDir = new DirectoryImpl(); newDir.setDirectory(root);
     * newDir.setName(name); try { IDirectory returnedDirectory =
     * dao.saveDirectory(newDir);
     * 
     * if (returnedDirectory.getName().equals(name)) { return true; } } catch
     * (PersistenceException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } return false; }
     */

    /*
     * private static IDirectory appendDirectory(String name, IDirectory parent)
     * throws PersistenceException { ConfigDAO dao = new ConfigDAO();
     * 
     * IDirectory newDir = new DirectoryImpl(); newDir.setDirectory(parent);
     * newDir.setName("TST_" + name); IDirectory result =
     * dao.saveDirectory(newDir); return result; }
     */

    /*
     * private static IConfig appendConfig(String name, IDirectory parent)
     * throws PersistenceException { ConfigDAO dao = new ConfigDAO();
     * 
     * IConfig c = createDummyConfig1D(3, 4, 4); c.setDirectory(parent); IConfig
     * result = dao.saveConfig(c); return result; }
     */

    /*
     * private static boolean
     * TestDeleteDirectoryRecursivelyWithConfigAtomicity() { try { ConfigDAO dao
     * = new ConfigDAO(); IDirectory root = dao.getRootDirectory();
     * 
     * IDirectory child01 = appendDirectory("AEffacer01", root); IDirectory
     * child02 = appendDirectory("AEffacer02", child01); IConfig config01 =
     * appendConfig("config01", child01); IDirectory child03 =
     * appendDirectory("AEffacer03", child02); IConfig config02 =
     * appendConfig("config02", child02); IConfig config03 =
     * appendConfig("config03", child03); // IConfig config03bis =
     * appendConfig("config03bis", child03); //Timestamp oldTimestamp =
     * child01.getTimestamp(); Integer id01 = child01.getId(); Integer id02 =
     * child02.getId(); Integer id03 = child03.getId();
     * 
     * //IDirectory i01 = dao.getDirectoryById(id01); IDirectory i02 =
     * dao.getDirectoryById(id02); //IDirectory i03 =
     * dao.getDirectoryById(id03); IConfig c01 =
     * dao.getConfigById(config01.getId()); IConfig c02 =
     * dao.getConfigById(config02.getId()); IConfig c03 =
     * dao.getConfigById(config03.getId());
     * 
     * if (id01 == null || id02 == null || id03 == null || c01 == null || c02 ==
     * null || c03 == null) { return false; } IDirectory dirToDelete =
     * dao.getDirectoryById(id02); if (dirToDelete == null) { return false; }
     * 
     * root = dao.getRootDirectory(); appendConfig("config02bis", child02);
     * IDirectory root02 = findDirectory(root, i02.getId());
     * 
     * if (root02 == null) { return false; } try { dao.deleteDirectory(root02);
     * return false; } catch (Exception sqle) { } IDirectory r01 =
     * dao.getDirectoryById(id01); IDirectory r02 = dao.getDirectoryById(id02);
     * IDirectory r03 = dao.getDirectoryById(id03); IConfig cc01 =
     * dao.getConfigById(c01.getId()); IConfig cc02 =
     * dao.getConfigById(c02.getId()); IConfig cc03 =
     * dao.getConfigById(c03.getId()); if (r01 != null && r02 != null && r03 !=
     * null && cc01 != null && cc02 != null && cc03 != null) { return true; }
     * 
     * } catch (Exception exc) {
     * 
     * } return false; }
     */

    /*
     * private static boolean TestDeleteDirectoryRecursivelyWithConfig() { try {
     * ConfigDAO dao = new ConfigDAO(); IDirectory root =
     * dao.getRootDirectory();
     * 
     * IDirectory child01 = appendDirectory("AEffacer01", root); IDirectory
     * child02 = appendDirectory("AEffacer02", child01); IConfig config01 =
     * appendConfig("config01", child01); IDirectory child03 =
     * appendDirectory("AEffacer03", child02); IConfig config02 =
     * appendConfig("config02", child02); IConfig config03 =
     * appendConfig("config01", child03); // Timestamp oldTimestamp =
     * child01.getTimestamp(); Integer id01 = child01.getId(); Integer id02 =
     * child02.getId(); Integer id03 = child03.getId();
     * 
     * // IDirectory i01 = dao.getDirectoryById(id01); IDirectory i02 =
     * dao.getDirectoryById(id02); // IDirectory i03 =
     * dao.getDirectoryById(id03); IConfig c01 =
     * dao.getConfigById(config01.getId()); IConfig c02 =
     * dao.getConfigById(config02.getId()); IConfig c03 =
     * dao.getConfigById(config03.getId());
     * 
     * if (id01 == null || id02 == null || id03 == null || c01 == null || c02 ==
     * null || c03 == null) { return false; } IDirectory dirToDelete =
     * dao.getDirectoryById(id02); if (dirToDelete == null) { return false; }
     * 
     * root = dao.getRootDirectory(); IDirectory root02 = findDirectory(root,
     * i02.getId());
     * 
     * if (root02 == null) { return false; }
     * 
     * dao.deleteDirectory(root02);
     * 
     * IDirectory r01 = dao.getDirectoryById(id01); IDirectory r02 =
     * dao.getDirectoryById(id02); IDirectory r03 = dao.getDirectoryById(id03);
     * IConfig cc01 = dao.getConfigById(c01.getId()); IConfig cc02 =
     * dao.getConfigById(c02.getId()); IConfig cc03 =
     * dao.getConfigById(c03.getId()); if (r01 != null && r02 == null && r03 ==
     * null && cc01 != null && cc02 == null && cc03 == null) { return true; }
     * 
     * } catch (Exception exc) {
     * 
     * } return false; }
     */

    /*
     * private static boolean TestDeleteDirectoryRecursivelyWithoutConfig() {
     * try { ConfigDAO dao = new ConfigDAO(); IDirectory root =
     * dao.getRootDirectory();
     * 
     * IDirectory child01 = appendDirectory("AEffacer01", root); IDirectory
     * child02 = appendDirectory("AEffacer02", child01); IDirectory child03 =
     * appendDirectory("AEffacer03", child02); // Timestamp oldTimestamp =
     * child01.getTimestamp(); Integer id01 = child01.getId(); Integer id02 =
     * child02.getId(); Integer id03 = child03.getId();
     * 
     * // IDirectory i01 = dao.getDirectoryById(id01); IDirectory i02 =
     * dao.getDirectoryById(id02); // IDirectory i03 =
     * dao.getDirectoryById(id03);
     * 
     * if (id01 == null || id02 == null || id03 == null) { return false; }
     * IDirectory dirToDelete = dao.getDirectoryById(id02); if (dirToDelete ==
     * null) { return false; }
     * 
     * root = dao.getRootDirectory(); IDirectory root02 = findDirectory(root,
     * i02.getId());
     * 
     * if (root02 == null) { return false; }
     * 
     * dao.deleteDirectory(root02);
     * 
     * IDirectory r01 = dao.getDirectoryById(id01); IDirectory r02 =
     * dao.getDirectoryById(id02); IDirectory r03 = dao.getDirectoryById(id03);
     * if (r01 != null && r02 == null && r03 == null) { return true; }
     * 
     * } catch (Exception exc) {
     * 
     * } return false; }
     */

    /*
     * private static IDirectory findDirectory(IDirectory _d, int _directoryId)
     * { if (_d == null) return null; if (_d.getId().intValue() == _directoryId)
     * { return _d; }
     * 
     * for (IDirectory sub : _d.getSubDirectoriesList()) { IDirectory result =
     * findDirectory(sub, _directoryId); if (result != null) { return result; }
     * } return null; }
     */

    /*
     * private static boolean TestDeleteEmptyDirectoryAtRoot() { try { ConfigDAO
     * dao = new ConfigDAO(); IDirectory root = dao.getRootDirectory();
     * 
     * IDirectory child01 = appendDirectory("AEffacer", root); // Timestamp
     * oldTimestamp = child01.getTimestamp(); Integer id = child01.getId(); if
     * (id == null) { return false; } IDirectory dirToDelete =
     * dao.getDirectoryById(id); if (dirToDelete == null) { return false; }
     * 
     * dao.deleteDirectory(dirToDelete); IDirectory result =
     * dao.getDirectoryById(id);
     * 
     * if (result == null) { return true; } } catch (Exception exc) {
     * 
     * } return false; }
     */

    /*
     * private static boolean TestCreateConfigDeeply() { try { ConfigDAO dao =
     * new ConfigDAO(); IDirectory root = dao.getRootDirectory();
     * 
     * IDirectory child01 = appendDirectory("LVL01", root); IDirectory child02 =
     * appendDirectory("LVL02", child01); IDirectory child03 =
     * appendDirectory("LVL03", child02); Timestamp oldTimestamp =
     * child03.getTimestamp(); IConfig dummy = createDummyConfig1D(2, 3, 4);
     * dummy.setName("TST_ConfigLVL3"); dummy.setDirectory(child03); IConfig
     * result = dao.saveConfig(dummy);
     * 
     * if (result != null && result.getId() != null && result.getDirectory() !=
     * null && result.getDirectory().getId() != null &&
     * result.getDirectory().getTimestamp() != oldTimestamp) { return true; } }
     * catch (Exception exc) {
     * 
     * } return false; }
     */

    private static boolean TestSaveConfig() {
        ConfigDAO dao = new ConfigDAO();
        IDirectory root = dao.getRootDirectory();
        IConfig1D dummy = createDummyConfig1D(3, 2, 4);
        dummy.setDirectory(root);

        try {
            IConfig<?> result = dao.saveConfig(dummy);
            if (result != null && result.getId() != null
                    && ObjectUtils.sameObject(result.getDirectory().getId(), root.getId())) {
                return true;
            }
        } catch (PersistenceException e) {
            e.printStackTrace();
        } catch (ScanNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
    /*
     * private static boolean TestUpdateConfig() { ConfigDAO dao = new
     * ConfigDAO(); IDirectory root = dao.getRootDirectory(); IConfig1D dummy =
     * createDummyConfig1D(3, 2, 4); dummy.setDirectory(root); String newName =
     * "TST_Updated"; try { IConfig result = dao.saveConfig(dummy);
     * result.setName(newName); // IDirectory parent = dao.get IConfig result2 =
     * dao.saveConfig(result);
     * 
     * if (result2 != null && result2.getName().equals(newName)) { return true;
     * }
     * 
     * } catch (PersistenceException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } return false; }
     */

    /*
     * private static boolean TestDeleteConfigAtRoot() { ConfigDAO dao = new
     * ConfigDAO(); IDirectory root = dao.getRootDirectory(); IConfig1D dummy =
     * createDummyConfig1D(3, 2, 4); dummy.setDirectory(root);
     * 
     * try { IConfig result = dao.saveConfig(dummy); result.setName("Updated");
     * // IDirectory parent = dao.get IConfig result2 = dao.saveConfig(result);
     * 
     * IConfig exists = dao.getConfigById(result2.getId()); if (exists == null)
     * { return false; } dao.deleteConfig(result2); exists =
     * dao.getConfigById(result2.getId()); if (exists == null) { return true; }
     * 
     * } catch (PersistenceException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } return false; }
     */
}

-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Lun 19 Avril 2010 � 17:18
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.4-2ubuntu5

--
-- Base de donn�es: `salsa`
--

-- --------------------------------------------------------

--
-- Initialisation de la base de donn�es
--

use salsa ;

-- --------------------------------------------------------

--
-- Structure de la table `chart_properties`
--

CREATE TABLE IF NOT EXISTS `chart_properties` (
  `configid` int(11) NOT NULL,
  `data` longtext,
  PRIMARY KEY  (`configid`)
)  ENGINE=InnoDB ;

--
-- Structure de la table `plot_properties`
--

CREATE TABLE IF NOT EXISTS `plot_properties` (
  `configid` int(11) NOT NULL,
  `viewid` text NOT NULL,
  `data` longtext,
  PRIMARY KEY  (`configid`,`viewid`(700))
)  ENGINE=InnoDB ;

-- --------------------------------------------------------


--
-- Contraintes pour les tables export�es
--


--
-- Contraintes pour la table `chart_properties`
--
ALTER TABLE `chart_properties`
  ADD CONSTRAINT `chart_ibfk_1` FOREIGN KEY (`configid`) REFERENCES `config` (`configid`);

--
-- Contraintes pour la table `plot_properties`
--
ALTER TABLE `plot_properties`
  ADD CONSTRAINT `plot_ibfk_1` FOREIGN KEY (`configid`) REFERENCES `config` (`configid`);

-- --------------------------------------------------------

GRANT ALL PRIVILEGES ON salsa.* TO 'root'@'localhost' IDENTIFIED BY '' WITH GRANT OPTION;

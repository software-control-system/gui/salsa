-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Lun 19 Avril 2010 � 17:18
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.4-2ubuntu5

--
-- Base de donn�es: `salsa`
--

-- --------------------------------------------------------

--
-- Initialisation de la base de donn�es
--

CREATE DATABASE salsa ;

use salsa ;

-- --------------------------------------------------------

--
-- Structure de la table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `configid` int(11) NOT NULL auto_increment,
  `Name` varchar(256) default NULL,
  `type` varchar(50) default NULL,
  `data` longtext,
  `directoryid` int(11) default NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`configid`),
  KEY `directoryid` (`directoryid`)
) ENGINE=InnoDB ;

--
-- Structure de la table `directory`
--

CREATE TABLE IF NOT EXISTS `directory` (
  `directoryid` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `positionInDirectory` int(11) default NULL,
  `directory` int(11) default NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`directoryid`),
  KEY `directory` (`directory`)
)  ENGINE=InnoDB ;

--
-- Structure de la table `chart_properties`
--

CREATE TABLE IF NOT EXISTS `chart_properties` (
  `configid` int(11) NOT NULL,
  `data` longtext,
  PRIMARY KEY  (`configid`)
)  ENGINE=InnoDB ;

--
-- Structure de la table `plot_properties`
--

CREATE TABLE IF NOT EXISTS `plot_properties` (
  `configid` int(11) NOT NULL,
  `viewid` text NOT NULL,
  `data` longtext,
  PRIMARY KEY  (`configid`,`viewid`(700))
)  ENGINE=InnoDB ;

-- --------------------------------------------------------


--
-- Contraintes pour les tables export�es
--

--
-- Contraintes pour la table `config`
--
ALTER TABLE `config`
  ADD CONSTRAINT `config_ibfk_1` FOREIGN KEY (`directoryid`) REFERENCES `directory` (`directoryid`);

--
-- Contraintes pour la table `directory`
--
ALTER TABLE `directory`
  ADD CONSTRAINT `directory_ibfk_1` FOREIGN KEY (`directory`) REFERENCES `directory` (`directoryid`);

--
-- Contraintes pour la table `chart_properties`
--
ALTER TABLE `chart_properties`
  ADD CONSTRAINT `chart_ibfk_1` FOREIGN KEY (`configid`) REFERENCES `config` (`configid`);

--
-- Contraintes pour la table `plot_properties`
--
ALTER TABLE `plot_properties`
  ADD CONSTRAINT `plot_ibfk_1` FOREIGN KEY (`configid`) REFERENCES `config` (`configid`);

-- --------------------------------------------------------

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Contenu de la table `directory`
--

INSERT INTO `directory` (`name`, `positionInDirectory`, `directory`) VALUES
('root', 0, NULL);

-- --------------------------------------------------------
GRANT ALL PRIVILEGES ON salsa.* TO 'root'@'localhost' IDENTIFIED BY '' WITH GRANT OPTION;

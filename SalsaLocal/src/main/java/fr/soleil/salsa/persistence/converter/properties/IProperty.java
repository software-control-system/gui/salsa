package fr.soleil.salsa.persistence.converter.properties;

import org.w3c.dom.Element;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Interface relative to a xml property converter.
 * 
 */
public interface IProperty {

    /**
     * Converter the given xml element, return the object instance.
     * 
     * @param _c
     * @param _parent
     * @param _converterData
     * @return
     * @throws ConversionException
     */
    public Object fromXml(Class<?> _c, Element _parent, ConverterData _converterData)
            throws ConversionException;

    /**
     * Getter for the field name (in an object instance context).
     * 
     * @return
     */
    public String getFieldName();

    /**
     * Setter for the field name (in an object instance context).
     * 
     * @param fieldName
     */
    public void setFieldName(String fieldName);

    /**
     * Convert the given object instance into his xml format.
     * 
     * @param _xml
     * @param _o
     * @param _converterData
     * @throws ConversionException
     */
    public void toXml(Element _xml, Object _o, ConverterData _converterData)
            throws ConversionException;

}

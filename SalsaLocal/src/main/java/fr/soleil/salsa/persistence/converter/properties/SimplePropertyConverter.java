package fr.soleil.salsa.persistence.converter.properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Class relative to the conversion of an object instance as an xml element.
 * 
 */
public class SimplePropertyConverter implements IProperty {

    /**
     * Attribute (in an object instance context).
     */
    private String attribute;

    /**
     * Field name (in an object instance context).
     */
    private String fieldName;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param attribute
     */
    public SimplePropertyConverter(String fieldName, String attribute) {
        super();
        this.fieldName = fieldName;
        this.attribute = attribute;
    }

    @Override
    public Object fromXml(Class<?> _c, Element _xml, ConverterData data) {
        return null;
    }

    /**
     * Getter for attribute.
     * 
     * @return
     */
    public String getAttribute() {
        return attribute;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter for attribute.
     * 
     * @param attribute
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public void toXml(Element _xml, Object _o, ConverterData _cd) {
        Document d = _xml.getOwnerDocument();
        Element result = d.createElement(getAttribute());
        if (_o != null) {
            result.setAttribute("value", _o.toString());
        }
        _xml.appendChild(result);
    }

}

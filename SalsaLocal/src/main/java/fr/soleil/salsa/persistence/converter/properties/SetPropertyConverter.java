package fr.soleil.salsa.persistence.converter.properties;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.persistence.converter.ComplexTypeConverter;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Class relative to the conversion of a Set object into/from xml.
 * 
 */
public class SetPropertyConverter implements IProperty {

    /**
     * The field name (in an object instance context).
     */
    private String fieldName;

    /**
     * The type (in an object instance context).
     */
    private String type;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param type
     */
    public SetPropertyConverter(String fieldName, String type) {
        super();
        this.fieldName = fieldName;
        this.type = type;
    }

    @Override
    public Object fromXml(Class<?> _c, Element _xml, ConverterData _cd) throws ConversionException {
        try {
            HashSet<Object> result = new HashSet<Object>();
            ComplexTypeConverter ctc = _cd.getConverterHT().get(getType());

            NodeList childs = _xml.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                if (childs.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element item = (Element) childs.item(i);
                Object o = ctc.fromXml(result, item, _cd);
                result.add(o);
            }
            return result;
        }
        catch (Exception e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Getter for the type.
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    @Override
    public void setFieldName(String field) {
        this.fieldName = field;
    }

    /**
     * Setter for the type.
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void toXml(Element _xml, Object _o, ConverterData _cd) throws ConversionException {
        try {
            if (_o == null) {
                return;
            }

            Document d = _xml.getOwnerDocument();
            Element result = d.createElement(getFieldName());
            _xml.appendChild(result);
            ComplexTypeConverter ctc = _cd.getConverterHT().get(getType());
            Set<Object> list = castToObjectSet(_o);
            Iterator<Object> i = list.iterator();
            while (i.hasNext()) {
                Element itemXml = d.createElement(getFieldName());
                result.appendChild(itemXml);

                Object item = i.next();
                ctc.toXml(itemXml, item, _cd);
            }
        }
        catch (Exception e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

    /**
     * Casts an object to a set of objects. Used to isolate the SuppressWarnings("unchecked")
     * annotation from the application logic.
     * 
     * @param obj
     * @return
     */
    @SuppressWarnings("unchecked")
    private Set<Object> castToObjectSet(Object obj) {
        return (Set<Object>) obj;
    }
}

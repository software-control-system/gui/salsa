package fr.soleil.salsa.persistence.converter;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * @author Alike
 * 
 *         Act as a encapsulating object for 'cumulatives parameters' wich
 *         maintains informations of conversion xml/object in progress.
 *         Maintains up to date hashtables
 * 
 */
public class ConverterData {

    /**
     * The document relative to conversion.
     */
    private Document document;

    /**
     * All existing converter. The key is the converter name (the class name).
     * 
     */
    private Hashtable<String, ComplexTypeConverter> converterHT = new Hashtable<String, ComplexTypeConverter>();

    /**
     * Allow to find the matching xml node for an object.
     * We need it for post conversion work.
     * For instance, 
     * 1) object A has been converted
     * 2) later object B contains a reference to object A
     * 3) We look in the hashtable if A is already converted
     * 4) if true, we get the xml element for A, and we know we must add an xmlId on it. 
     */
    private Hashtable<Object, Element> fromModelToXML = new Hashtable<Object, Element>();

    /**
     * 
     */
    private Hashtable<String, Object> RefIdHT = new Hashtable<String, Object>();

    private List<Object> oPendingRef = new ArrayList<Object>();
    private List<Element> xPendingRef = new ArrayList<Element>();
    private List<String> aPendingRef = new ArrayList<String>();
    private List<String> fPendingRef = new ArrayList<String>();
    private List<String> kPendingRef = new ArrayList<String>();
    private List<Integer> iPendingRef = new ArrayList<Integer>();
    private List<List<Object>> lPendingRef = new ArrayList<List<Object>>();
    
    
    
    public List<Integer> getIPendingRef() {
        return iPendingRef;
    }

    public void setIPendingRef(List<Integer> pendingRef) {
        iPendingRef = pendingRef;
    }

    public List<List<Object>> getLPendingRef() {
        return lPendingRef;
    }

    public void setLPendingRef(List<List<Object>> pendingRef) {
        lPendingRef = pendingRef;
    }

    public List<String> getKPendingRef() {
        return kPendingRef;
    }

    public void setKPendingRef(List<String> pendingRef) {
        kPendingRef = pendingRef;
    }

    public List<String> getFPendingRef() {
        return fPendingRef;
    }

    public void setFPendingRef(List<String> pendingRef) {
        fPendingRef = pendingRef;
    }

    public List<String> getAPendingRef() {
        return aPendingRef;
    }

    public void setAPendingRef(List<String> pendingRef) {
        aPendingRef = pendingRef;
    }

    public List<Object> getOPendingRef() {
        return oPendingRef;
    }
    

    public void setOPendingRef(List<Object> pendingRef) {
        oPendingRef = pendingRef;
    }

    public List<Element> getXPendingRef() {
        return xPendingRef;
    }

    public void setXPendingRef(List<Element> pendingRef) {
        xPendingRef = pendingRef;
    }

    /**
     * Act as an id sequencer for each entity.
     * For instance, we want to generate a id for 'actuator',
     * idHT.get("actuator") give you the next available id for your actuator.
     */
    private Hashtable<String, Integer> idHT = new Hashtable<String, Integer>();

    /**
     * A class name => full class name (with package)
     */
    private PropertyResourceBundle classFullName;


    /**
     * A class name => full class name (with package)
     * @return
     */
    public PropertyResourceBundle getClassFullName() {
        return classFullName;
    }

    /**
     * Getter for complex type hashtable.
     * 
     * @return
     */
    public Hashtable<String, ComplexTypeConverter> getConverterHT() {
        return converterHT;
    }

    /**
     * Getter for document being processed.
     * @return
     */
    public Document getDocument() {
        return document;
    }

    /**
     * Getter for Mapping model <--> XML hashtable.
     * @return
     */
    public Hashtable<Object, Element> getFromModelToXML() {
        return fromModelToXML;
    }

    /**
     * Getter for the id 'sequencer'.
     * @return
     */
    public Hashtable<String, Integer> getIdHT() {
        return idHT;
    }

    /**
     * Getter for the reference id hashtable.
     * @return
     */
    public Hashtable<String, Object> getRefIdHT() {
        return RefIdHT;
    }

    /**
     * For debug purpose.
     */
    public void reset() {
        fromModelToXML = new Hashtable<Object, Element>();
        idHT = new Hashtable<String, Integer>();
    }

    /**
     * Setter for class full name.
     * @param classFullName
     */
    public void setClassFullName(PropertyResourceBundle classFullName) {
        this.classFullName = classFullName;
    }

    /**
     * Setter for the converter hashtable.
     * @param converterHT
     */
    public void setConverterHT(
            Hashtable<String, ComplexTypeConverter> converterHT) {
        this.converterHT = converterHT;
    }

    /**
     * Setter for the xml document being processed.
     * @param document
     */
    public void setDocument(Document document) {
        this.document = document;
    }

    /**
     * Setter for the object <--> XML hashtable.
     * @param fromModelToXML
     */
    public void setFromModelToXML(Hashtable<Object, Element> fromModelToXML) {
        this.fromModelToXML = fromModelToXML;
    }

    /**
     * Setter for the id 'sequencer'.
     * @param idHT
     */
    public void setIdHT(Hashtable<String, Integer> idHT) {
        this.idHT = idHT;
    }

    /**
     * Setter for the reference id hashtable.
     * @param refIdHT
     */
    public void setRefIdHT(Hashtable<String, Object> refIdHT) {
        RefIdHT = refIdHT;
    }

}

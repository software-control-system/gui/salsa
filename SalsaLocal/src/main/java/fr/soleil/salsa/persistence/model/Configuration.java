package fr.soleil.salsa.persistence.model;

import java.io.Serializable;
import java.sql.Timestamp;

import fr.soleil.salsa.entity.IDirectory;

/**
 * The configuration of entity
 * 
 * @author Administrateur
 * 
 */
public class Configuration implements Serializable {

    private static final long serialVersionUID = -5223226797939093206L;

    /**
     * The Id.
     */
    private Integer id = null;

    private String name = null;

    /**
     * The type.
     */
    private String type = null;

    /**
     * The identifier of Directory
     */
    private Integer directoryId = null;

    /**
     * The directory of configuration
     */
    private IDirectory directory = null;

    /**
     * The data.
     */
    private String data = null;

    /**
     * The timestamp
     */
    private Timestamp timestamp = null;

    /**
     * The identifier
     * 
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the identifier of the configuration
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the type of the configuration
     * 
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type of the configuration
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the data of the configuration
     * 
     * @return data
     */
    public String getData() {
        return data;
    }

    /**
     * Sets the data of the configuration
     * 
     * @param data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Gets the directory of the configuration
     * 
     * @return directory
     */
    public IDirectory getDirectory() {
        return directory;
    }

    /**
     * Sets the directory of the configuration
     * 
     * @param directory
     */
    public void setDirectory(IDirectory directory) {
        this.directory = directory;
    }

    /**
     * Gets the identifier of directory of the configuration
     * 
     * @return directoryId
     */
    public Integer getDirectoryId() {
        return directoryId;
    }

    /**
     * Sets the identifier of directory of the configuration
     * 
     * @param directoryId
     */
    public void setDirectoryId(Integer directoryId) {
        this.directoryId = directoryId;
    }

    /**
     * Gets the timestamp of the configuration
     * 
     * @return timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp of the configuration
     * 
     * @param timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void clean() {
        id = null;
        name = null;
        type = null;
        directoryId = null;
        directory = null;
        data = null;
        timestamp = null;
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }

}

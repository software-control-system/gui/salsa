package fr.soleil.salsa.persistence.converter.attributes;

import org.w3c.dom.Element;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Class relative to the conversion of an object as an xml attribute.
 * 
 */
public interface IAttribute {

    /**
     * Convert an xml attribute into his object instance.
     * 
     * @param _c
     * @param _xml
     * @param _converterData
     * @return
     * @throws ConversionException
     */
    public Object fromXml(Class<?> _c, Element _xml, ConverterData _converterData)
            throws ConversionException;

    /**
     * Getter for the field name (in an instance context)
     * 
     * @return
     */
    public String getFieldName();

    /**
     * Setter for the field name (in an instance context)
     * 
     * @param fieldName
     */
    public void setFieldName(String fieldName);

    /**
     * Conveter the give object into his xml format as an attribute.
     * 
     * @param _xml
     * @param _o
     * @param _converterData
     * @throws ConversionException
     */
    public void toXml(Element _xml, Object _o, ConverterData _converterData)
            throws ConversionException;

}

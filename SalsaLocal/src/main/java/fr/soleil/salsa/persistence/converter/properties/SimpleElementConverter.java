package fr.soleil.salsa.persistence.converter.properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.soleil.salsa.entity.impl.Stage;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike.
 * 
 *         Class relative to the conversion of an object instance into/from a xml element tag : <XXX
 *         value="YYY" />
 * 
 */
public class SimpleElementConverter implements IProperty {

    /**
     * Attribute (in an xml context).
     */
    private String attribute;

    /**
     * Field name (in an object instance context).
     */
    private String fieldName;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param attribute
     */
    public SimpleElementConverter(String fieldName, String attribute) {
        super();
        this.fieldName = fieldName;
        this.attribute = attribute;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object fromXml(Class<?> _c, Element _xml, ConverterData data) {

        String cname = _c.getSimpleName();
        String valueStr = _xml.getAttribute("value");
        if ("int".equals(cname) || "Integer".equals(cname)) {
            return Integer.parseInt(valueStr);
        } else if ("boolean".equals(cname) || "Boolean".equals(cname)) {
            return Boolean.parseBoolean(valueStr);
        } else if ("byte".equals(cname) || "Byte".equals(cname)) {
            return Byte.parseByte(valueStr);
        } /*
          * else if("char".equals(cname)) { return new Character(); }
          */
        else if ("short".equals(cname) || "Short".equals(cname)) {
            return Short.parseShort(valueStr);
        } else if ("long".equals(cname) || "Long".equals(cname)) {
            return Long.parseLong(valueStr);
        } else if ("float".equals(cname) || "Float".equals(cname)) {
            return Float.parseFloat(valueStr);
        } else if ("double".equals(cname) || "Double".equals(cname)) {
            return Double.parseDouble(valueStr);
        } else if ("String".equals(cname)) {
            return valueStr;
        } else if (_c.isEnum()) {
            if (_c.getName().equals(Stage.class.getName())) {
                valueStr = valueStr.replaceAll(" ", "_");
                valueStr = valueStr.toUpperCase();
            }
            return Enum.valueOf((Class<? extends Enum>) _c, valueStr);
        }
        return null;
    }

    /**
     * Getter for attribute.
     * 
     * @return
     */
    public String getAttribute() {
        return attribute;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter for attribute.
     * 
     * @param attribute
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public void toXml(Element _xml, Object _o, ConverterData _cd) {
        if (_o == null) {
            return;
        }
        Document d = _xml.getOwnerDocument();
        Element result = d.createElement(getAttribute());

        if (_o != null) {
            result.setAttribute("value", _o.toString());
        }
        _xml.appendChild(result);
    }

}

package fr.soleil.salsa.persistence.converter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.List;
import java.util.PropertyResourceBundle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.persistence.converter.attributes.IAttribute;
import fr.soleil.salsa.persistence.converter.attributes.SimpleAttributeConveter;
import fr.soleil.salsa.persistence.converter.properties.ComplexPropertyConverter;
import fr.soleil.salsa.persistence.converter.properties.ElementArrayConverter;
import fr.soleil.salsa.persistence.converter.properties.IProperty;
import fr.soleil.salsa.persistence.converter.properties.ListOfRefPropertyConverter;
import fr.soleil.salsa.persistence.converter.properties.ListPropertyConverter;
import fr.soleil.salsa.persistence.converter.properties.ReferenceConverter;
import fr.soleil.salsa.persistence.converter.properties.SetPropertyConverter;
import fr.soleil.salsa.persistence.converter.properties.SimpleElementConverter;
import fr.soleil.salsa.persistence.converter.properties.SimplePropertyConverter;

public class ConfigAsXmlHelper {

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigAsXmlHelper.class);

    private static ConverterData createConverterData() throws ConversionException {
        ConverterData converterData = new ConverterData();
        /* Loading short and long class name used */

        InputStream s = null;
        s = ConfigAsXmlHelper.class.getResourceAsStream("Model.properties");
        try {
            PropertyResourceBundle prb = new PropertyResourceBundle(s);
            s.close();
            converterData.setClassFullName(prb);
        } catch (Exception exc) {
            String errorMessage = "Cannot read Model.properties " + exc.getMessage();
            LOGGER.warn(errorMessage);
            LOGGER.debug("Stack trace", exc);
        }
        try {
            loadMaps(converterData.getConverterHT(), converterData);
        } catch (Exception e) {
            String errorMessage = "Cannot loadMaps " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConversionException(errorMessage, e);
        }

        return converterData;
    }

    public static Object fromXml(Document _d) throws ConversionException {
        ConverterData cd = createConverterData();
        String type = _d.getDocumentElement().getTagName();
        ComplexTypeConverter ctc = cd.getConverterHT().get(type);
        Object result;
        result = ctc.fromXml(null, (Element) _d.getFirstChild(), cd);

        for (int i = 0; i < cd.getLPendingRef().size(); i++) {
            List<Object> l = cd.getLPendingRef().get(i);
            int index = cd.getIPendingRef().get(i);
            String key = cd.getKPendingRef().get(i);
            if (cd.getRefIdHT().containsKey(key)) {
                Object o = cd.getRefIdHT().get(key);
                l.set(index, o);
            }
        }

        return result;
    }

    public static Object fromXml(String _xml) throws ConversionException {

        try {
            DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
            DocumentBuilder constructeur = fabrique.newDocumentBuilder();
            InputStream s = new ByteArrayInputStream(_xml.getBytes());
            Document document = constructeur.parse(s);
            Object result = fromXml(document);

            return result;
        } catch (ConversionException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = "Cannot convert fromXml " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConversionException(errorMessage, e);
        }

    }

    private static void loadMaps(Hashtable<String, ComplexTypeConverter> _ht, ConverterData _cd)
            throws SAXException, IOException, ParserConfigurationException {

        // lecture du contenu d'un fichier XML avec DOM
        // URL persistanceXml = ConfigAsXmlHelper.class.getResource("Persistance.xml");
        InputStream persistanceXmlS = ConfigAsXmlHelper.class.getResourceAsStream("Persistance.xml");

        /* -- */
        DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
        // creation d'un constructeur de documents
        DocumentBuilder constructeur = fabrique.newDocumentBuilder();
        // Document document = constructeur.parse(persistanceXml.getFile());
        Document document = constructeur.parse(persistanceXmlS);
        persistanceXmlS.close();
        Node root = document.getFirstChild();
        NodeList mapList = ((Element) root).getElementsByTagName("map");

        // We iterate foreach mapper
        for (int i = 0; i < mapList.getLength(); i++) {
            Element map = (Element) mapList.item(i);
            String className = map.getAttribute("class");
            ComplexTypeConverter ctc = new ComplexTypeConverter();
            loadProperties(ctc, map);
            ctc.setElementName(className);
            ctc.setClassName(className);
            _ht.put(className, ctc);
        }
    }

    private static void loadProperties(ComplexTypeConverter _ctc, Element _xml) {

        NodeList propertyList = _xml.getChildNodes();

        for (int j = 0; j < propertyList.getLength(); j++) {
            if (!(propertyList.item(j) instanceof Element)) {
                continue;
            }
            Element p = (Element) propertyList.item(j);
            String nodeName = p.getNodeName();
            String attribute = p.getAttribute("attribute");
            String fieldName = p.getAttribute("fieldName");
            String type = p.getAttribute("type");

            IProperty ip = null;
            IAttribute ia = null;

            if ("property".equals(nodeName)) {
                ip = new SimplePropertyConverter(fieldName, attribute);
            } else if ("simpleAttribute".equals(nodeName)) {
                ia = new SimpleAttributeConveter(fieldName, attribute);
            } else if ("simpleElement".equals(nodeName)) {
                ip = new SimpleElementConverter(fieldName, attribute);
            } else if ("elementArray".equals(nodeName)) {
                ip = new ElementArrayConverter(fieldName, attribute);
            } else if ("ComplexProperty".equals(nodeName)) {
                ip = new ComplexPropertyConverter(fieldName, type);
            } else if ("listProperty".equals(nodeName)) {
                ip = new ListPropertyConverter(fieldName, type);
            } else if ("setProperty".equals(nodeName)) {
                ip = new SetPropertyConverter(fieldName, type);
            } else if ("reference".equals(nodeName)) {
                ip = new ReferenceConverter(fieldName, attribute);
            } else if ("listOfRefProperty".equals(nodeName)) {
                ip = new ListOfRefPropertyConverter(fieldName, attribute);
            }
            if (ip != null) {
                _ctc.getProperties().add(ip);
            }
            if (ia != null) {
                _ctc.getAttributes().add(ia);
            }
        }
    }

    public static Document toXml(Object _o) throws ConversionException {
        try {
            ConverterData converterData = ConfigAsXmlHelper.createConverterData();
            String className = _o.getClass().getSimpleName();
            className = converterData.getClassFullName().getString(className);
            ComplexTypeConverter ctc = converterData.getConverterHT().get(className);
            DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
            Document result = fabrique.newDocumentBuilder().newDocument();
            result.appendChild(result.createElement(className));
            Element rootResult = (Element) result.getFirstChild();
            ctc.toXml(rootResult, _o, converterData);
            List<Element> elementList = converterData.getXPendingRef();
            int size = elementList.size();

            for (int i = 0; i < size; i++) {
                Object o = converterData.getOPendingRef().get(i);
                Element x = converterData.getXPendingRef().get(i);
                String a = converterData.getAPendingRef().get(i);
                String f = converterData.getFPendingRef().get(i);
                ReferenceConverter.computeRef(x, o, converterData, a, f);
            }

            return result;

        } catch (ConversionException e) {
            throw e;
        } catch (Exception e) {
            String errorMessage = "Cannot convert toXml " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConversionException(errorMessage, e);
        }
    }

    /**
     * Xml document into String.
     * 
     * @param _d
     * @return
     * @throws TransformerException
     */
    public static String xmlToString(Document _d) throws ConversionException {
        try {
            DOMSource domSource = new DOMSource(_d);
            StringWriter writer = new StringWriter();
            StreamResult sr = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(domSource, sr);
            String stringResult = writer.toString();
            return stringResult;
        } catch (TransformerException e) {
            String errorMessage = "Cannot convert xml to string " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConversionException(errorMessage, e);
        }
    }
}

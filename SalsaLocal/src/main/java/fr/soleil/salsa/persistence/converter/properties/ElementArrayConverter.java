package fr.soleil.salsa.persistence.converter.properties;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike.
 * 
 *         Class relative to the conversion of an object instance into/from a xml element tag : <XXX
 *         value="YYY" />
 * 
 */
public class ElementArrayConverter implements IProperty {

    /**
     * Attribute (in an xml context).
     */
    private String attribute;

    /**
     * Field name (in an object instance context).
     */
    private String fieldName;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param attribute
     */
    public ElementArrayConverter(String fieldName, String attribute) {
        super();
        this.fieldName = fieldName;
        this.attribute = attribute;
    }

    @Override
    public Object fromXml(Class<?> _c, Element _xml, ConverterData data) {
        String cname = _c.getSimpleName();
        String valueStr = _xml.getAttribute("value");
        Object result = null;
        if (!valueStr.startsWith("[")) {
            double dValue = Double.valueOf(valueStr);
            result = new double[1];
            ((double[]) result)[0] = dValue;
        } else {
            valueStr = StringUtils.chop(valueStr);
            valueStr = StringUtils.substring(valueStr, 1);
            String[] tokens = valueStr.split(",");

            if ("double[]".equals(cname)) {
                result = new double[tokens.length];
                for (int i = 0; i < tokens.length; i++) {
                    String string = tokens[i];
                    ((double[]) result)[i] = Double.valueOf(string);
                }
            }
        }

        return result;
    }

    /**
     * Getter for attribute.
     * 
     * @return
     */
    public String getAttribute() {
        return attribute;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter for attribute.
     * 
     * @param attribute
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public void toXml(Element _xml, Object _o, ConverterData _cd) {
        if (_o == null) {
            return;
        }
        Document d = _xml.getOwnerDocument();
        Element result = d.createElement(getAttribute());

        if (_o != null) {
            result.setAttribute("value", ArrayUtils.toString(_o));
        }
        _xml.appendChild(result);
    }

}

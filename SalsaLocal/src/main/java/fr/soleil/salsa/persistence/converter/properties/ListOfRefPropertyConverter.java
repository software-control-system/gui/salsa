package fr.soleil.salsa.persistence.converter.properties;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Class relative to the xml conversion of/from a List. For each item of the list, the
 *         converter object is used.
 * 
 */
public class ListOfRefPropertyConverter implements IProperty {

    /**
     * The field name (in a object instance context).
     */
    private String fieldName;

    /**
     * The type (in an object/xml instance context)
     */
    private String type;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param type
     */
    public ListOfRefPropertyConverter(String fieldName, String type) {
        super();
        this.fieldName = fieldName;
        this.type = type;
    }

    @Override
    public Object fromXml(Class<?> _c, Element _xml, ConverterData _cd) {

        ArrayList<Object> result = new ArrayList<Object>();
        // ComplexTypeConverter ctc = _cd.getConverterHT().get(getType());

        NodeList childs = _xml.getChildNodes();
        int index = 0;
        for (int i = 0; i < childs.getLength(); i++) {

            if (childs.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            Element item = (Element) childs.item(i);

            Object o = null;// ctc.fromXml(result, item, _cd);
            String refKey = item.getAttribute("refTo");
            if (_cd.getRefIdHT().containsKey(refKey)) {
                o = _cd.getRefIdHT().get(refKey);
            }
            else {
                o = null;
                _cd.getKPendingRef().add(refKey);
                _cd.getIPendingRef().add(index);
                _cd.getLPendingRef().add(result);

            }
            result.add(o);
            index++;
        }
        // return null;

        return result;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Getter for the type.
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    @Override
    public void setFieldName(String field) {
        this.fieldName = field;
    }

    /**
     * Setter for the type.
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void toXml(Element _xml, Object _o, ConverterData _cd) {
        if (_o == null) {
            return;
        }

        Document d = _xml.getOwnerDocument();
        Element result = d.createElement(getFieldName());
        _xml.appendChild(result);

        // ComplexTypeConverter ctc = _cd.getConverterHT().get(getType());

        List<Object> list = castToObjectList(_o);
        for (int i = 0; i < list.size(); i++) {
            Object item = list.get(i);
            ReferenceConverter rc = new ReferenceConverter(fieldName, type);
            rc.toXml(result, item, _cd);

            /*
            Element itemXml = d.createElement(getFieldName());
            result.appendChild(itemXml);
            
            Object item = list.get(i);
            ctc.toXml(itemXml, item, _cd);
            */
        }
    }

    /**
     * Casts an object to a list of objects. Used to isolate the SuppressWarnings("unchecked")
     * annotation from the application logic.
     * 
     * @param obj
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<Object> castToObjectList(Object obj) {
        return (List<Object>) obj;
    }
}

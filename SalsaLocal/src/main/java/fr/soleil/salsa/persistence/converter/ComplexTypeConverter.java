package fr.soleil.salsa.persistence.converter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.persistence.converter.attributes.IAttribute;
import fr.soleil.salsa.persistence.converter.properties.IProperty;

public class ComplexTypeConverter {

    public static final Logger LOGGER = LoggingUtil.getLogger(ComplexTypeConverter.class);

    /**
     * ClassName (simple).
     */
    private String className;

    /**
     * The tag of correponding element in the xml data.
     */
    private String elementName;

    /**
     * References to attributes (i.e xml attributes of nodes).
     */
    List<IAttribute> attributes = new ArrayList<>();

    /**
     * Reference to properties (i.e xml childs).
     */
    List<IProperty> properties = new ArrayList<>();

    /**
     * Find the given 'tag' among first level children (not recursively). There must be only one
     * child with the matching tag.
     * 
     * @param _parent
     * @param tag
     * @return the matching element
     */
    private Element findElement(Element _parent, String tag) {
        Element result = null;

        NodeList nl = _parent.getChildNodes();

        for (int i = 0; i < nl.getLength(); i++) {
            if (nl.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            Element e = (Element) nl.item(i);
            if (tag.equals(e.getTagName())) {
                result = e;
            }
        }
        return result;
    }

    /**
     * Casts to Class<? extends Enum>.
     * 
     * @param c
     * @return
     */
    @SuppressWarnings("unchecked")
    private Class<? extends Enum> toEnumClass(Class<?> c) {
        return (Class<? extends Enum>) c;
    }

    /**
     * Getter for attributes.
     * 
     * @return
     */
    public List<IAttribute> getAttributes() {
        return attributes;
    }

    /**
     * Getter for class name.
     * 
     * @return
     */
    public String getClassName() {
        return className;
    }

    /**
     * Getter for element name.
     * 
     * @return
     */
    public String getElementName() {
        return elementName;
    }

    /**
     * Return the corresponding field for a given field name.
     * 
     * @param c
     * @param fieldName
     * @return
     */
    private Field getField(Class<?> c, String fieldName) {

        Field f = null;

        while (c != null) {

            try {
                f = c.getDeclaredField(fieldName);
                return f;
            } catch (SecurityException e) {

            } catch (NoSuchFieldException e) {
            }
            c = c.getSuperclass();
        }
        return f;
    }

    /**
     * Getter for properties list.
     * 
     * @return
     */
    public List<IProperty> getProperties() {
        return properties;
    }

    /**
     * Setter for attributes list.
     * 
     * @param attributes
     */
    public void setAttributes(List<IAttribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * Setter for class name.
     * 
     * @param className
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * Setter for element name.
     * 
     * @param elementName
     */
    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    /**
     * Setter for properties list.
     * 
     * @param properties
     */
    public void setProperties(List<IProperty> properties) {
        this.properties = properties;
    }

    /**
     * Convert the given _xml data into ConfigImpl Object.
     * 
     * @param o
     * @param xml
     * @param cd
     * @return
     * @throws ConversionException
     */
    public Object fromXml(Object o, Element xml, ConverterData cd) throws ConversionException {
        try {
            String fullClassName = cd.getClassFullName().getString(className);
            Class<?> c = Class.forName(fullClassName);
            Object result;
            // Case where the class is an enumeration.
            if (c.isEnum()) {
                if (xml.hasAttribute("value")) {
                    String enumValueString = xml.getAttribute("value");
                    @SuppressWarnings("unchecked")
                    Object enumValue = Enum.valueOf(toEnumClass(c), enumValueString);
                    result = enumValue;
                } else {
                    result = null;
                }
            } else {
                result = c.newInstance();

                if (xml.hasAttribute("refId")) {
                    cd.getRefIdHT().put(xml.getAttribute("refId"), result);
                }

                for (IAttribute a : attributes) {
                    Field f = getField(c, a.getFieldName());
                    if (f != null) {
                        f.setAccessible(true);
                        Object value = a.fromXml(f.getType(), xml, cd);
                        if (f.getName().equals("timeBase") && (value == null)) {
                            f.set(result, false);
                        } else {
                            f.set(result, value);
                        }
                    }
                }
                for (IProperty p : properties) {
                    Field f = getField(c, p.getFieldName());
                    String previousName = p.getFieldName();
                    if (f == null) {
                        // RG: backward compatibility step 1:
                        // ConfigEnergy previously had its dimension named "dimensionEnergy".
                        // This dimension was renamed "dimensionX" since. --> apply renaming
                        if (className.toLowerCase().indexOf("configenergy") > -1) {
                            if ("dimensionEnergy".equals(previousName)) {
                                p.setFieldName("dimensionX");
                            } else if ("dimensionX".equals(previousName)) {
                                p.setFieldName("dimensionEnergy");
                            }
                            f = getField(c, p.getFieldName());
                        }
                    }
                    // RG: backward compatibility step 2:
                    // Restore property name to read the right node.
                    p.setFieldName(previousName);
                    if (f != null) {
                        f.setAccessible(true);
                        Element child = findElement(xml, p.getFieldName());
                        if (child != null) {
                            Object value = p.fromXml(f.getType(), child, cd);
                            f.set(result, value);
                        }
                    }
                }
            }
            return result;
        } catch (Exception e) {
            String errorMessage = "Cannot convert from Object from Xml " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConversionException(errorMessage, e);
        }
    }

    /**
     * Convert the given object into xml data. (the final xml structure is defined by descriptive
     * persistance.xml file).
     * 
     * @param xml
     * @param o
     * @param cd
     * @throws ConversionException
     */
    public void toXml(Element xml, Object o, ConverterData cd) throws ConversionException {
        try {
            if (o != null) {
                cd.getFromModelToXML().put(o, xml);
                for (IAttribute a : attributes) {
                    String fullClassName = cd.getClassFullName().getString(className);
                    if (fullClassName == null) {
                        fullClassName = className;
                    }
                    Class<?> c = Class.forName(fullClassName);
                    Object value = null;
                    if (c.isEnum()) {
                        value = ((Enum<?>) o).name();
                    } else {
                        Field f = getField(c, a.getFieldName());
                        if (f != null) {
                            f.setAccessible(true);
                            value = f.get(o);
                        }
                    }
                    a.toXml(xml, value, cd);

                }
                for (IProperty p : properties) {
                    String fullClassName = cd.getClassFullName().getString(className);
                    if (fullClassName == null) {
                        fullClassName = className;
                    }
                    Class<?> c = Class.forName(fullClassName);
                    Field f = getField(c, p.getFieldName());
                    if (f != null) {
                        f.setAccessible(true);
                        Object value;
                        value = f.get(o);
                        p.toXml(xml, value, cd);
                    }
                }
            }
        } catch (Exception e) {
            String errorMessage = "Cannot convert Xml to Object " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new ConversionException(errorMessage, e);
        }
    }

}

package fr.soleil.salsa.persistence.converter.properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Class relative to the conversion of an object instance into/from xml, as a reference (i.e
 *         just a 'pointer' to an existing object).
 * 
 */
public class ReferenceConverter implements IProperty {

    /**
     * Attribute (in an xml context).
     */
    private String attribute;

    /**
     * Field name (in an object instance context).
     */
    private String fieldName;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param attribute
     */
    public ReferenceConverter(String fieldName, String attribute) {
        super();
        this.fieldName = fieldName;
        this.attribute = attribute;
    }

    @Override
    public Object fromXml(Class<?> _c, Element _xml, ConverterData _cd) {

        String refKey = _xml.getAttribute("refTo");
        if (_cd.getRefIdHT().containsKey(refKey)) {
            return _cd.getRefIdHT().get(refKey);
        }
        return null;
    }

    /**
     * Getter for attribute.
     * 
     * @return
     */
    public String getAttribute() {
        return attribute;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter for attribute.
     * 
     * @param attribute
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public void toXml(Element _xml, Object _o, ConverterData _cd) {

        if (_o == null) {
            return;
        }

        Document d = _xml.getOwnerDocument();
        Element result = d.createElement(getFieldName());

        _xml.appendChild(result);

        computeRef(result, _o, _cd, attribute, fieldName);

    }

    public static void computeRef(Element _xml, Object _o, ConverterData _cd, String _attribute,
            String _fieldName) {
        int id = 0;
        if (_cd.getFromModelToXML().containsKey(_o)) {
            Element e = _cd.getFromModelToXML().get(_o);
            if (e.hasAttribute("refId")) {
                // OBject already referenced
                _xml.setAttribute("refTo", e.getAttribute("refId"));
            }
            else {
                // Non referenced object

                // We generate an unique id
                if (_cd.getIdHT().containsKey("Id")) {
                    id = _cd.getIdHT().remove("Id");
                }

                String idStr = e.getNodeName() + "_" + id;
                e.setAttribute("refId", idStr);

                _xml.setAttribute("refTo", idStr);
                _cd.getIdHT().put("Id", id + 1);

            }
        }
        else {
            _cd.getOPendingRef().add(_o);
            _cd.getXPendingRef().add(_xml);
            _cd.getAPendingRef().add(_attribute);
            _cd.getFPendingRef().add(_fieldName);
        }
    }

}

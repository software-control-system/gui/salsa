package fr.soleil.salsa.persistence.converter.properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.persistence.converter.ComplexTypeConverter;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike
 * 
 *         Class relative to the conversion of/from an object as an xml tag child.
 * 
 */
public class ComplexPropertyConverter implements IProperty {

    /**
     * The field name (in an object instance context).
     */
    private String fieldName;

    /**
     * The type (in an object instance context).
     */
    private String type;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param type
     */
    public ComplexPropertyConverter(String fieldName, String type) {
        super();
        this.fieldName = fieldName;
        this.type = type;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Getter for the type.
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    @Override
    public void setFieldName(String field) {
        this.fieldName = field;
    }

    /**
     * Setter for the type.
     * 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Object fromXml(Class<?> c, Element xml, ConverterData cd) throws ConversionException {
        ComplexTypeConverter ctc = cd.getConverterHT().get(getType());
        Object result;
        try {
            result = ctc.fromXml(null, xml, cd);
        } catch (Exception e) {
            throw new ConversionException(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public void toXml(Element xml, Object o, ConverterData cd) throws ConversionException {
        Document d = xml.getOwnerDocument();
        Element result = d.createElement(fieldName);
        xml.appendChild(result);

        ComplexTypeConverter ctc = cd.getConverterHT().get(type);
        try {
            ctc.toXml(result, o, cd);
        } catch (Exception e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

}

package fr.soleil.salsa.persistence.converter.attributes;

import org.w3c.dom.Element;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.persistence.converter.ConverterData;

/**
 * @author Alike Implementation of an attribute converter.
 */
public class SimpleAttributeConveter implements IAttribute {

    /**
     * Attribute name (in a xml context).
     */
    private String attribute;

    /**
     * Field name (in an object instance context).
     */
    private String fieldName;

    /**
     * Constructor.
     * 
     * @param fieldName
     * @param attribute
     */
    public SimpleAttributeConveter(String fieldName, String attribute) {
        super();
        this.fieldName = fieldName;
        this.attribute = attribute;
    }

    /* (non-Javadoc)
     * @see com.alike.conversion.xml.attributes.IAttribute#fromXml(java.lang.Class, org.w3c.dom.Element, com.alike.conversion.xml.ConverterData)
     */
    @Override
    public Object fromXml(Class<?> c, Element parent, ConverterData data) throws ConversionException {
        Object result = null;
        try {
            if (parent.hasAttribute(getAttribute())) {
                String cname = c.getSimpleName();
                String valueStr = parent.getAttribute(getAttribute());
                switch (cname) {
                    case "int":
                    case "Integer":
                        result = Integer.valueOf(valueStr);
                        break;
                    case "boolean":
                    case "Boolean":
                        result = Boolean.valueOf(valueStr);
                        break;
                    case "byte":
                    case "Byte":
                        result = Byte.valueOf(valueStr);
                        break;
                    case "short":
                    case "Short":
                        result = Short.valueOf(valueStr);
                        break;
                    case "long":
                    case "Long":
                        result = Long.valueOf(valueStr);
                        break;
                    case "float":
                    case "Float":
                        result = Float.valueOf(valueStr);
                        break;
                    case "double":
                    case "Double":
                        result = Double.valueOf(valueStr);
                        break;
                    case "String":
                        result = valueStr;
                        break;
//                    case "char":
//                    case "Character":
//                        result = Character.valueOf(valueStr.charAt(0));
//                        break;
                    default:
                        result = null;
                        break;
                }
            }
            return result;
        } catch (IllegalArgumentException e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

    /**
     * Getter for attribute.
     * 
     * @return
     */
    public String getAttribute() {
        return attribute;
    }

    /* (non-Javadoc)
     * @see com.alike.conversion.xml.attributes.IAttribute#getFieldName()
     */
    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Setter for attribute.
     * 
     * @param attribute
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /* (non-Javadoc)
     * @see com.alike.conversion.xml.attributes.IAttribute#setFieldName(java.lang.String)
     */
    @Override
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /* (non-Javadoc)
     * @see com.alike.conversion.xml.attributes.IAttribute#toXml(org.w3c.dom.Element, java.lang.Object, com.alike.conversion.xml.ConverterData)
     */
    @Override
    public void toXml(Element xml, Object o, ConverterData cd) {
        if (o != null) {
            if (o.getClass().isEnum()) {
                xml.setAttribute(attribute, ((Enum<?>) o).name());
            } else {
                xml.setAttribute(attribute, o.toString());
            }
        }
    }

}

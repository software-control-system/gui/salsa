package fr.soleil.salsa.business.tool;

import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Scan connector tool for 1D config. A new tool must be created for each scan.
 */
public class ScanConnectorToolHCS extends ScanConnectorToolBase {

    /**
     * Constructor.
     * 
     * @param scanServerName
     */
    public ScanConnectorToolHCS(String scanServerName) throws SalsaDeviceException {
        super(scanServerName);
    }

    /**
     * Starts a scan.
     * 
     * @param config
     * @param scanServerName
     *            the name of the scan server to use.
     * @throws SalsaDeviceException
     */
    public void startScan(IConfigHCS config, String scanServerName) throws SalsaDeviceException {
        if (config.getDimensionX().getActuatorsList().size() == 0) {
            throw new SalsaDeviceException("Cannot start scan : no actuators were defined.");
        }

        int timeOut;
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else {
            try {
                // Reads the initial value of the time out.
                timeOut = scanServerProxy.get_timeout_millis();
                initScanCommon(config, scanServerName);
                try {

                    // Actuators X
                    setDimensionAttribute(scanServerName, 1, config.getDimensionX(), config.isOnTheFly());

                    // Hardware continuous scan : yes !
                    actionName = writeAttributeLog("hwContinuous", "true");
                    setAttribute("hwContinuous", true, false);

                    // Get the only one range
                    int nbOfPoint = 1;

                    IDimensionHCS dimension = config.getDimensionX();
                    if (dimension != null) {
                        List<IRangeHCS> rangeList = dimension.getRangesXList();
                        if (SalsaUtils.isFulfilled(rangeList)) {
                            IRangeHCS range = rangeList.get(0);
                            nbOfPoint = range.getStepsNumber();
                        }
                    }

                    actionName = writeAttributeLog("hwContinuousNbPt", String.valueOf(nbOfPoint));
                    setAttribute("hwContinuousNbPt", nbOfPoint, false);

                    // End of specific configuration.

                    startScan();
                } finally {
                    // Sets the time out back to the initial value.
                    scanServerProxy.set_timeout_millis(timeOut);
                }
            } catch (DevFailed e) {
                String errorMessage = actionName + " Failed " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            } catch (Exception ex) {
                String errorMessage = actionName + " Failed " + ex.getMessage();
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", ex);
                throw new SalsaDeviceException(errorMessage, ex);
            }
        }
    }
}

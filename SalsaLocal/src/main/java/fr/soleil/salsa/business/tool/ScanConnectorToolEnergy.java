package fr.soleil.salsa.business.tool;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.exception.SalsaDeviceException;

/**
 * Scan connector tool for 1D config. A new tool must be created for each scan.
 */
public class ScanConnectorToolEnergy extends ScanConnectorToolBase {

    /**
     * Constructor.
     * 
     * @param scanServerName
     */
    public ScanConnectorToolEnergy(String scanServerName) throws SalsaDeviceException {
        super(scanServerName);
    }

    /**
     * Starts a scan.
     * 
     * @param config
     * @param scanServerName the name of the scan server to use.
     * @throws SalsaDeviceException
     */
    public void startScan(IConfigEnergy config, String scanServerName) throws SalsaDeviceException {
        if (config.getDimensionX().getActuatorsList().size() == 0) {
            throw new SalsaDeviceException("Cannot start scan : no energy actuators were defined.");
        }
        int timeOut;
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else {
            try {
                // Reads the initial value of the time out.
                timeOut = scanServerProxy.get_timeout_millis();
                initScanCommon(config, scanServerName);
                try {

                    // Actuators X
                    setDimensionAttribute(scanServerName, 1, config.getDimensionX(), config.isOnTheFly());

                    // Hardware continuous scan : no.
                    setAttribute("hwContinuous", false, false);

                    // End of specific configuration.

                    startScan();
                } finally {
                    // Sets the time out back to the initial value.
                    scanServerProxy.set_timeout_millis(timeOut);
                }
            } catch (DevFailed e) {
                throw new SalsaDeviceException(actionName + " Failed " + DevFailedUtils.toString(e), e);
            }
        }

    }

}

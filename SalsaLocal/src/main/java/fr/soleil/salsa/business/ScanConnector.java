package fr.soleil.salsa.business;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.salsa.api.item.HistoricRecord;
import fr.soleil.salsa.business.tool.ScanConnectorTool1D;
import fr.soleil.salsa.business.tool.ScanConnectorTool2D;
import fr.soleil.salsa.business.tool.ScanConnectorToolBase;
import fr.soleil.salsa.business.tool.ScanConnectorToolEnergy;
import fr.soleil.salsa.business.tool.ScanConnectorToolHCS;
import fr.soleil.salsa.business.tool.ScanConnectorToolK;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestionItem;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.ScanState;
import fr.soleil.salsa.entity.impl.ScanStatusImpl;
import fr.soleil.salsa.entity.impl.SuggestionCategoryImpl;
import fr.soleil.salsa.entity.impl.SuggestionItemImpl;
import fr.soleil.salsa.entity.impl.SuggestionsImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaScanConfigurationException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;
import fr.soleil.tango.clientapi.TangoAttribute;

/**
 * Allows connexion to the scan server.
 */
public class ScanConnector {

    public static final Logger LOGGER = LoggingUtil.getLogger(ScanConnector.class);

    private ScanConnectorToolBase scanConnectorToolBase = null;
    private ScanConnectorTool1D scanConnectorTool1D = null;
    private ScanConnectorTool2D scanConnectorTool2D = null;
    private ScanConnectorToolK scanConnectorToolK = null;
    private ScanConnectorToolHCS scanConnectorToolHCS = null;
    private ScanConnectorToolEnergy scanConnectorToolEnergy = null;
    private boolean dataRecorderPartialMode = false;
    private boolean dataRecorderEnable = true;

    /**
     * The constructor.
     */
    public ScanConnector() {
    }

    /**
     * Loads a scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     * @throws SalsaScanConfigurationException
     */
    public void loadScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        if (context != null) {
            if (config instanceof IConfig1D) {
                loadScan((IConfig1D) config, context);
            } else if (config instanceof IConfig2D) {
                loadScan((IConfig2D) config, context);
            } else if (config instanceof IConfigHCS) {
                loadScan((IConfigHCS) config, context);
            } else if (config instanceof IConfigK) {
                loadScan((IConfigK) config, context);
            } else if (config instanceof IConfigEnergy) {
                loadScan((IConfigEnergy) config, context);
            } else {
                String errorMessage = "Cannot load config, because Scan type not supported.";
                LOGGER.error(errorMessage);
                throw new SalsaScanConfigurationException(errorMessage);
            }
        } else {
            String errorMessage = "Cannot load config, because context is not defined.";
            LOGGER.error(errorMessage);
            throw new SalsaScanConfigurationException(errorMessage);
        }
    }

    /**
     * Starts a scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     * @throws SalsaScanConfigurationException
     */
    public void startScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        if (context != null) {
            String scanServerName = context.getScanServerName();
            if (SalsaUtils.isDefined(scanServerName)) {
                if (config instanceof IConfig1D) {
                    startScan((IConfig1D) config, context);
                } else if (config instanceof IConfig2D) {
                    startScan((IConfig2D) config, context);
                } else if (config instanceof IConfigHCS) {
                    startScan((IConfigHCS) config, context);
                } else if (config instanceof IConfigK) {
                    startScan((IConfigK) config, context);
                } else if (config instanceof IConfigEnergy) {
                    startScan((IConfigEnergy) config, context);
                } else {
                    String errorMessage = "Cannot start scan because Scan type not supported.";
                    LOGGER.error(errorMessage);
                    throw new SalsaScanConfigurationException(errorMessage);
                }
            } else {
                String errorMessage = "Cannot start scan because scanServerName in context is not defined";
                LOGGER.error(errorMessage);
                throw new SalsaDeviceException(errorMessage);
            }
        } else {
            String errorMessage = "Cannot start scan because context is not defined.";
            LOGGER.error(errorMessage);
            throw new SalsaScanConfigurationException(errorMessage);
        }
    }

    /**
     * Starts a 1D scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void startScan(IConfig1D config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (scanConnectorTool1D != null) {
            scanConnectorTool1D.setDataRecorderPartialMode(dataRecorderPartialMode);
            scanConnectorTool1D.setDataRecorderEnable(dataRecorderEnable);
            scanConnectorTool1D.startScan(config, scanServerName);
        } else {
            String errorMessage = "Cannot start scan config1D " + config.getFullPath()
                    + " because scanConnectorTool1D is not defined";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Loads a 1D scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void loadScan(IConfig1D config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            scanConnectorTool1D = new ScanConnectorTool1D(scanServerName);
        } else {
            String errorMessage = "Cannot load scan config1D  " + config.getFullPath() + " because " + scanServerName
                    + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Starts a 2D scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void startScan(IConfig2D config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (scanConnectorTool2D != null) {
            scanConnectorTool2D.setDataRecorderPartialMode(dataRecorderPartialMode);
            scanConnectorTool2D.setDataRecorderEnable(dataRecorderEnable);
            scanConnectorTool2D.startScan(config, scanServerName);
        } else {
            String errorMessage = "Cannot start scan config2D " + config.getFullPath()
                    + " because scanConnectorTool2D is not defined";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Loads a 2D scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void loadScan(IConfig2D config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            scanConnectorTool2D = new ScanConnectorTool2D(scanServerName);
        } else {
            String errorMessage = "Cannot load scan config2D  " + config.getFullPath() + " because " + scanServerName
                    + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Starts a scan HCS
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void startScan(IConfigHCS config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (scanConnectorToolHCS != null) {
            scanConnectorToolHCS.setDataRecorderPartialMode(dataRecorderPartialMode);
            scanConnectorToolHCS.setDataRecorderEnable(dataRecorderEnable);
            scanConnectorToolHCS.startScan(config, scanServerName);
        } else {
            String errorMessage = "Cannot start scan configHCS " + config.getFullPath()
                    + " because scanConnectorToolHCS is not defined";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Loads a HCS scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void loadScan(IConfigHCS config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            scanConnectorToolHCS = new ScanConnectorToolHCS(scanServerName);
        } else {
            String errorMessage = "Cannot load scan configHCS  " + config.getFullPath() + " because " + scanServerName
                    + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Starts a scan energy.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void startScan(IConfigEnergy config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (scanConnectorToolEnergy != null) {
            scanConnectorToolEnergy.setDataRecorderPartialMode(dataRecorderPartialMode);
            scanConnectorToolEnergy.setDataRecorderEnable(dataRecorderEnable);
            scanConnectorToolEnergy.startScan(config, scanServerName);
        } else {
            String errorMessage = "Cannot start scan configEnergy " + config.getFullPath()
                    + " because scanConnectorToolEnergy is not defined";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Loads a Energy scan.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void loadScan(IConfigEnergy config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            scanConnectorToolEnergy = new ScanConnectorToolEnergy(scanServerName);
        } else {
            String errorMessage = "Cannot load scan configEnergy  " + config.getFullPath() + " because "
                    + scanServerName + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Starts a scan K.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void startScan(IConfigK config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (scanConnectorToolK != null) {
            scanConnectorToolK.setDataRecorderPartialMode(dataRecorderPartialMode);
            scanConnectorToolK.setDataRecorderEnable(dataRecorderEnable);
            scanConnectorToolK.startScan(config, scanServerName);
        } else {
            String errorMessage = "Cannot start scan configK " + config.getFullPath()
                    + " because scanConnectorToolK is not defined";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Loads a scan K.
     * 
     * @param config
     *            config of the scan
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void loadScan(IConfigK config, IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            scanConnectorToolK = new ScanConnectorToolK(scanServerName);
        } else {
            String errorMessage = "Cannot load scan configK  " + config.getFullPath() + " because " + scanServerName
                    + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    // Get the values of properties don t throw a error if not exist !!
    public ISuggestions getDevicesSuggestions(String scanServerName) throws SalsaDeviceException {
        ISuggestions suggestions = new SuggestionsImpl();
        Database m_database = null;
        try {
            m_database = new Database();
        } catch (DevFailed e) {
            String errorMessage = "Cannot create database device " + DevFailedUtils.toString(e);
            LOGGER.warn(errorMessage);
            LOGGER.debug("Stack trace", e);
            return suggestions;
        }
        if (m_database != null) {
            // get ActuatorsFile property
            try {
                DbDatum m_dataActuators = m_database.get_device_property(scanServerName, "ActuatorsFile");
                String[] actuatorsFile = m_dataActuators.extractStringArray();
                suggestions.setActuatorSuggestionList(parseSuggestions(scanServerName, "ActuatorsFile", actuatorsFile));
            } catch (DevFailed e) {
                String errorMessage = "Cannot read property " + scanServerName + "/ActuatorsFile" + " "
                        + DevFailedUtils.toString(e);
                LOGGER.warn(errorMessage);
            }

            // get SensorsFile property
            try {
                DbDatum m_dataSensors = m_database.get_device_property(scanServerName, "SensorsFile");
                String[] sensorsFile = m_dataSensors.extractStringArray();
                suggestions.setSensorSuggestionList(parseSuggestions(scanServerName, "SensorsFile", sensorsFile));
            } catch (DevFailed e) {
                String errorMessage = "Cannot read property " + scanServerName + "/SensorsFile" + " "
                        + DevFailedUtils.toString(e);
                LOGGER.warn(errorMessage);
                LOGGER.debug("Stack trace", e);

            }

            // get TimebasesFile property
            try {
                DbDatum m_dataTimebases = m_database.get_device_property(scanServerName, "TimebasesFile");
                String[] timebasesFile = m_dataTimebases.extractStringArray();
                suggestions.setTimebaseSuggestionList(parseSuggestions(scanServerName, "TimebasesFile", timebasesFile));
            } catch (DevFailed e) {
                String errorMessage = "Cannot read property " + scanServerName + "/TimebasesFile" + " "
                        + DevFailedUtils.toString(e);
                LOGGER.warn(errorMessage);
                LOGGER.debug("Stack trace", e);
            }

        }

        return suggestions;

    }

    private List<ISuggestionCategory> parseSuggestions(String scanserver, String property, String[] data) {
        List<ISuggestionCategory> categories = new ArrayList<ISuggestionCategory>();

        if ((data != null) && (data.length > 0)) {
            String SEPARATOR = "::";

            SuggestionCategoryImpl currentCategory = null;
            for (String line : data) {
                String[] tab = line.split(SEPARATOR);
                if ((tab == null) || (tab.length != 3)) {
                    break;
                }
                String level = tab[0];
                String label = tab[1];
                String attribute = tab[2];
                if ("menu".equals(level)) {
                    currentCategory = new SuggestionCategoryImpl();
                    currentCategory.setSuggestionList(new ArrayList<ISuggestionItem>());
                    currentCategory.setLabel(label);
                    categories.add(currentCategory);
                } else if ("submenu".equals(level)) {
                    if (currentCategory != null) {
                        SuggestionItemImpl suggestion = new SuggestionItemImpl();
                        suggestion.setLabel(label);
                        suggestion.setValue(attribute);
                        currentCategory.getSuggestionList().add(suggestion);
                    } else {
                        String errorMessage = "Scan server ('" + scanserver + "') property '" + property
                                + "' provide with unexpected format";
                        LOGGER.warn(errorMessage);
                    }
                }
            }
        } else {
            String errorMessage = "Cannot parse empty property " + scanserver + "/" + property;
            LOGGER.warn(errorMessage);
        }
        return categories;
    }

    /**
     * Gets the scan status informations.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    public IScanStatus getScanStatus(String scanServerName) throws SalsaDeviceException {
        ScanStatusImpl result = new ScanStatusImpl();
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy != null) {
            try {
                result.setState(String.valueOf(CurrentScanDataModel.getState(scanServerName)));
                result.setStatus(CurrentScanDataModel.getStatus(scanServerName));

                result.setRunStartDate(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.RUN_START_DATE));

                result.setScanEndDate(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.SCAN_END_DATE));

                result.setRunEndDate(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.RUN_END_DATE));

                result.setScanDuration(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.SCAN_DURATION));

                result.setRunDuration(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.RUN_DURATION));

                result.setScanRemainingTime(CurrentScanDataModel.readStringAttribute(scanServerName,
                        CurrentScanDataModel.SCAN_REMAINING_TIME));

                result.setRunRemainingTime(CurrentScanDataModel.readStringAttribute(scanServerName,
                        CurrentScanDataModel.RUN_REMAINING_TIME));

                result.setScanElapsed(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.SCAN_ELAPSED));

                result.setRunElapsed(
                        CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.RUN_ELAPSED));

                result.setRunCompletion(
                        CurrentScanDataModel.readDoubleAttribute(scanServerName, CurrentScanDataModel.RUN_COMPLETION));

                result.setScanCompletion(
                        CurrentScanDataModel.readDoubleAttribute(scanServerName, CurrentScanDataModel.SCAN_COMPLETION));

                result.setDeadTime(
                        CurrentScanDataModel.readDoubleAttribute(scanServerName, CurrentScanDataModel.DEAD_TIME));

                result.setDeadTimePercentage(CurrentScanDataModel.readDoubleAttribute(scanServerName,
                        CurrentScanDataModel.DEAD_TIME_PERCENT));

                result.setDeadTimePerPoint(CurrentScanDataModel.readDoubleAttribute(scanServerName,
                        CurrentScanDataModel.DEAD_TIME_PER_POINT));
            } catch (Exception exc) {
                String errorMessage = "Cannot read scan status informations : " + exc.getMessage();
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", exc);
                throw new SalsaDeviceException(errorMessage, exc);
            }
        } else {
            String errorMessage = "Cannot read scan status informations " + scanServerName + " is null";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }

        return result;
    }

    /**
     * Reads the scan type.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     *             If a problem occurred while accessing the Scan Server
     * @throws SalsaScanConfigurationException
     *             If the Scan Server Scan Type is not supported
     */
    public IConfig.ScanType getScanType(String scanServerName)
            throws SalsaDeviceException, SalsaScanConfigurationException {

        IConfig.ScanType type = null;
        int scanType = CurrentScanDataModel.getScanType(scanServerName);
        switch (scanType) {
            case 0:
            case 1:
                type = IConfig.ScanType.SCAN_1D;
                break;
            case 2:
                type = IConfig.ScanType.SCAN_2D;
                break;
            default:
                throw new SalsaScanConfigurationException("Scan type not implemented yet");
        }

        return type;

    }

    /**
     * Returns the result of the current scan.
     * 
     * @param scanServerName
     *            the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     *             If a problem occurred while accessing the Scan Server
     * @throws SalsaScanConfigurationException
     *             If the Scan Server Scan Type is not supported
     */
    public IScanResult readScanResult(String scanServerName, boolean withTrajectories)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        if (scanConnectorToolBase == null) {
            scanConnectorToolBase = new ScanConnectorToolBase(scanServerName);
        }
        return scanConnectorToolBase.retrieveCommonScanResult(withTrajectories);
    }

    /**
     * Stops the scan.
     * 
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void stopScan(IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            try {
                DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
                if (scanServerProxy == null) {
                    LOGGER.error("Could not get DeviceProxy for " + scanServerName);
                } else {
                    scanServerProxy.command_inout("Abort");
                }
            } catch (DevFailed e) {
                String errorMessage = "Cannot execute " + scanServerName + "/Abort " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            }
        } else {
            String errorMessage = "Cannot Abort because " + scanServerName + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Pauses the scan.
     * 
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void pauseScan(IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            try {
                DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
                if (scanServerProxy == null) {
                    LOGGER.error("Could not get DeviceProxy for " + scanServerName);
                } else {
                    scanServerProxy.command_inout("Pause");
                }
            } catch (DevFailed e) {
                String errorMessage = "Cannot execute " + scanServerName + "/Pause " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            }
        } else {
            String errorMessage = "Cannot Pause because " + scanServerName + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Resumes the scan.
     * 
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void resumeScan(IContext context) throws SalsaDeviceException {
        String scanServerName = context.getScanServerName();
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            try {
                DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
                if (scanServerProxy == null) {
                    LOGGER.error("Could not get DeviceProxy for " + scanServerName);
                } else {
                    scanServerProxy.command_inout("Resume");
                }
            } catch (DevFailed e) {
                String errorMessage = "Cannot execute " + scanServerName + "/Resume " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            }
        } else {
            String errorMessage = "Cannot Resume because " + scanServerName + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Checks if a scan result can be read.
     * 
     * @param scanServerName
     *            the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     */
    public boolean isScanResultReady(String scanServerName) throws SalsaDeviceException {
        // If a scan result is ready, there is a data_01 attribute.
        return TangoAttributeHelper.isAttributeRunning(scanServerName, "data_01");
    }

    /**
     * Returns the state of the scan.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    public ScanState getScanState(String scanServerName) throws SalsaDeviceException {
        ScanState state = null;
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy != null) {
            try {
                // Time out
                scanServerProxy.set_timeout_millis(10000);
                DevState devState = scanServerProxy.state();
                if (devState != null) {
                    int stateInt = devState.value();
                    switch (stateInt) {
                        case DevState._ON:
                            state = ScanState.STOPPED;
                            break;
                        case DevState._MOVING:
                        case DevState._RUNNING:
                            state = ScanState.RUNNING;
                            break;
                        case DevState._STANDBY:
                            state = ScanState.PAUSED;
                            break;
                        case DevState._ALARM:
                            state = ScanState.ABORT;
                            break;
                        case DevState._FAULT:
                            state = ScanState.ABORT;
                            break;
                        default:
                            String errorMessage = devState.toString() + " is a not supported state";
                            LOGGER.error(errorMessage);
                            throw new SalsaDeviceException(errorMessage);
                    }
                } else {
                    String errorMessage = scanServerName + " is in unknown state";
                    LOGGER.error(errorMessage);
                    throw new SalsaDeviceException(errorMessage);
                }
            } catch (DevFailed e) {
                String errorMessage = "Cannot read state " + scanServerName + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            }
        } else {
            String errorMessage = scanServerName + " proxy is null";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
        return state;
    }

    /**
     * Reads the historic.
     * 
     * @param scanServerName
     * @return
     */
    public List<HistoricRecord> getHistoric(String scanServerName) throws SalsaDeviceException {
        String historicStringArray[] = CurrentScanDataModel.readHistoric(scanServerName);
        if ((historicStringArray == null) || (historicStringArray.length == 0)) {
            // scanServerManager.invalidateScanServerProxy(scanServerName);
            // throw new
            // SalsaDeviceException("Error : cannot read the historic attribute");
            return new ArrayList<HistoricRecord>();
        }
        // The historic attribute on the scan server is an array of strings that
        // contains the scan
        // historic.
        // Each record is made of 5 string in the array.
        // - the first one is the period,
        // - the second one is the type,
        // - the third one is the message,
        // - we do not use the last two.
        // So we are going to parse the attribute per set of 5 to build a list
        // of HistoricRecord.
        int recordsNumber = historicStringArray.length / 5;
        int stopIndex = 5 * recordsNumber; // This has the advantage over
        // historicStringArray.length
        // that we have a guaranteed multiple of 5.
        String periodAsString;
        // Double period;
        String type;
        String message;
        HistoricRecord record;
        List<HistoricRecord> recordsList = new ArrayList<HistoricRecord>();
        for (int index = 0; index < stopIndex; index += 5) {
            periodAsString = historicStringArray[index];
            type = historicStringArray[index + 1];
            message = historicStringArray[index + 2];
            record = new HistoricRecord();
            record.setPeriod(periodAsString);
            record.setType(type);
            record.setMessage(message);
            recordsList.add(record);
        }
        return recordsList;
    }

    /**
     * Clears the historic on the scan server.
     * 
     * @param scanServerName
     * @throws SalsaDeviceException
     */
    public void clearHistoric(String scanServerName) throws SalsaDeviceException {
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            try {
                DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
                if (scanServerProxy == null) {
                    LOGGER.error("Could not get DeviceProxy for " + scanServerName);
                } else {
                    scanServerProxy.command_inout("ClearHistoric");
                }
            } catch (DevFailed e) {
                String errorMessage = "Cannot execute command " + scanServerName + "/ClearHistoric "
                        + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            }
        } else {
            String errorMessage = "Cannot ClearHistoric because " + scanServerName + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }
    }

    /**
     * Performs a scan function.
     * 
     * @param scanServerName
     *            the name of the scan server to use.
     * @param behaviour
     *            the function to be done.
     * @param sensor
     *            function parameter : sensor. Null if the function has no
     *            sensor parameter.
     * @param actuator
     *            function parameter : actuator. Null id the function has no
     *            actuator parameter.
     * @throws SalsaDeviceException
     */
    public void doScanFunction(String scanServerName, Behaviour behaviour, ISensor sensor, IActuator actuator)
            throws SalsaDeviceException {
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            try {
                DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
                if (scanServerProxy == null) {
                    LOGGER.error("Could not get DeviceProxy for " + scanServerName);
                } else {
                    TangoAttribute afterRunActionType = new TangoAttribute(scanServerName + "/afterRunActionType");
                    LOGGER.trace("write {}/afterRunActionType={}", scanServerName, behaviour.getType());
                    afterRunActionType.write(behaviour.getType());

                    if (sensor != null) {
                        // We need the sensor position.
                        String sensorName = sensor.getName();
                        if (SalsaUtils.isDefined(sensorName)) {
                            String[] sensorsNamesArray = scanServerProxy.read_attribute("sensors").extractStringArray();
                            int sensorPosition;
                            for (sensorPosition = 0; sensorPosition < sensorsNamesArray.length; ++sensorPosition) {
                                if (sensorName.equals(sensorsNamesArray[sensorPosition])) {
                                    break;
                                }
                            }
                            if (sensorPosition < sensorsNamesArray.length) {
                                TangoAttribute afterRunActionSensor = new TangoAttribute(
                                        scanServerName + "/afterRunActionSensor");
                                LOGGER.trace("write {}/afterRunActionSensor={} for sensor {}", scanServerName,
                                        sensorPosition, sensorName);
                                afterRunActionSensor.write(sensorPosition);
                            } else {
                                String errorMessage = "Sensor " + sensor + " is unknow on the scan server ";
                                LOGGER.error(errorMessage);
                                throw new SalsaDeviceException(errorMessage);
                            }
                        }
                    }

                    if (actuator != null) {
                        // We need the actuator position.
                        String actuatorName = actuator.getName();
                        if (SalsaUtils.isDefined(actuatorName)) {
                            String[] actuatorsNamesArray = scanServerProxy.read_attribute("actuators")
                                    .extractStringArray();
                            int actuatorPosition;
                            for (actuatorPosition = 0; actuatorPosition < actuatorsNamesArray.length; ++actuatorPosition) {
                                if (actuatorName.equals(actuatorsNamesArray[actuatorPosition])) {
                                    break;
                                }
                            }
                            if (actuatorPosition < actuatorsNamesArray.length) {
                                TangoAttribute afterRunActionActuator = new TangoAttribute(
                                        scanServerName + "/afterRunActionActuator");
                                LOGGER.trace("write {}/afterRunActionActuator={} for actuator {}", scanServerName,
                                        actuatorPosition, actuatorName);
                                afterRunActionActuator.write(actuatorPosition);
                            } else {
                                String errorMessage = "Actuator " + actuator + " is unknow on the scan server ";
                                LOGGER.error(errorMessage);
                                throw new SalsaDeviceException(errorMessage);
                            }
                        }
                    }
                    LOGGER.trace("Execute command {}/ExecuteAction", scanServerName);
                    scanServerProxy.command_inout("ExecuteAction");
                }
            } catch (DevFailed e) {
                String errorMessage = "Cannot perform scan function : " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage);
            }
        } else {
            String errorMessage = "Cannot ExecuteAction command because " + scanServerName + " device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }

    }

    /**
     * Set Data recorder partial mode.
     * 
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void setDataRecorderPartialMode(String scanServerName, boolean mode) throws SalsaDeviceException {
        dataRecorderPartialMode = mode;
        if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
            try {
                TangoAttribute tangoAttribute = new TangoAttribute(scanServerName + "/dataRecorderPartialMode");
                tangoAttribute.insert(mode);
                tangoAttribute.write();
                LOGGER.trace("Write {}/dataRecorderPartialMode={}", scanServerName, mode);
            } catch (DevFailed e) {
                String errorMessage = "Cannot write " + scanServerName + "/dataRecorderPartialMode to " + mode + " "
                        + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            }
        } else {
            String errorMessage = "Cannot write " + scanServerName + "/dataRecorderPartialMode because device is down";
            LOGGER.error(errorMessage);
            throw new SalsaDeviceException(errorMessage);
        }

    }

    /**
     * Set Data recorder partial mode.
     * 
     * @param context
     *            context of the scan
     * @throws SalsaDeviceException
     */
    public void setDataRecorderEnable(boolean enable) throws SalsaDeviceException {
        dataRecorderEnable = enable;
    }

    public boolean isDataRecorderEnable() {
        return dataRecorderEnable;
    }

}

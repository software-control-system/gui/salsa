package fr.soleil.salsa.business.tool;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.exception.SalsaDeviceException;

/**
 * Scan connector tool for 2D config. A new tool must be created for each scan.
 */
public class ScanConnectorTool2D extends ScanConnectorToolBase {

    /**
     * Constructor.
     * 
     * @param scanServerName
     */
    public ScanConnectorTool2D(String scanServerName) throws SalsaDeviceException {
        super(scanServerName);
    }

    /**
     * Starts a scan.
     * 
     * @param config
     * @param scanServerName the name of the scan server to use.
     * @throws SalsaDeviceException
     */
    public void startScan(IConfig2D config, String scanServerName) throws SalsaDeviceException {
        if (config.getDimensionX().getActuatorsList().size() == 0) {
            throw new SalsaDeviceException("Cannot start 2D scan : no actuators were defined for dimension X.");
        }
        int timeOut;
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else {
            try {
                // Read the initial value of the time out.
                timeOut = scanServerProxy.get_timeout_millis();
                initScanCommon(config, scanServerName);

                try {
                    // Actuators X
                    setDimensionAttribute(scanServerName, 1, config.getDimensionX(), config.isOnTheFly());

                    // Actuators y
                    setDimensionAttribute(scanServerName, 2, config.getDimensionY(), config.isOnTheFly());

                    // Hardware continuous scan : no.
                    actionName = "write_attribute(\"hwContinuous\"," + false + ")";
                    setAttribute("hwContinuous", false, false);

                    // End of specific configuration.

                    startScan();
                } finally {
                    // Sets the time out back to the initial value.
                    scanServerProxy.set_timeout_millis(timeOut);
                }
            } catch (DevFailed e) {
                throw new SalsaDeviceException(actionName + " Failed " + DevFailedUtils.toString(e), e);
            }
        }

    }

}

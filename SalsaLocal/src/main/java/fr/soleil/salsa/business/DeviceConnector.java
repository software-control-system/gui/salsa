package fr.soleil.salsa.business;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;
import fr.soleil.tango.clientapi.TangoAttribute;

public class DeviceConnector {

    public static final Logger LOGGER = LoggingUtil.getLogger(DeviceConnector.class);

    /**
     * Private constructor as this class is not supposed to be instanciated. It only provides static
     * methods.
     */
    protected DeviceConnector() throws SalsaDeviceException {

    }

    /**
     * The current value.
     * 
     * @param device
     * @return
     * @throws SalsaDeviceException
     */
    public static Double getData(IDevice device) throws SalsaDeviceException {
        Double result = Double.NaN;
        try {
            TangoAttribute tangoAttribute = getTangoAttribute(device);
            if (tangoAttribute != null) {
                result = tangoAttribute.extract(Double.class);
            }
        } catch (DevFailed devFailed) {
            String errorMessage = "Error while trying to read " + device + " " + DevFailedUtils.toString(devFailed);
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", devFailed);
            SalsaDeviceException salsaDeviceException = new SalsaDeviceException(errorMessage, devFailed);
            throw salsaDeviceException;
        } catch (Exception e) {
            String errorMessage = "Error while trying to read " + device + " " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            SalsaDeviceException salsaDeviceException = new SalsaDeviceException(errorMessage, e);
            throw salsaDeviceException;
        }
        return result;
    }

    /**
     * Format to be used for display.
     * 
     * @param device
     * @throws SalsaDeviceException
     */
    public static String getFormat(IDevice device) throws SalsaDeviceException {
        String format = null;
        AttributeInfo info = getAttributeInfo(device);
        if (info != null) {
            format = info.format;
        }
        return format;
    }

    /**
     * Returns the TangORB attribute for the device.
     * 
     * @param device The {@link IDevice} from which to recover the {@link TangoAttribute}
     * @return The desired {@link TangoAttribute}, or <code>null</code> if device was
     *         <code>null</code> or if its name was <code>null</code> or empty
     * @throws SalsaDeviceException If a problem occurred while accessing the attribute
     */
    private static TangoAttribute getTangoAttribute(IDevice device) throws SalsaDeviceException {
        TangoAttribute result = null;
        if (device != null) {
            result = getTangoAttribute(device.getName());
        }
        return result;
    }

    /**
     * Returns the TangORB attribute for the device.
     * 
     * @param completeName The attribute complete name
     * @return The desired {@link TangoAttribute}, or <code>null</code> if completeName was
     *         <code>null</code> or empty
     * @throws SalsaDeviceException If a problem occurred while accessing the attribute
     */
    private static TangoAttribute getTangoAttribute(String completeName) throws SalsaDeviceException {
        TangoAttribute result = null;
        if (SalsaUtils.isDefined(completeName)) {
            try {
                result = new TangoAttribute(completeName);
            } catch (DevFailed e) {
                SalsaDeviceException salsaDeviceException = new SalsaDeviceException(
                        "Error while creating a TangORB attribute proxy for attribute " + completeName, e);
                throw salsaDeviceException;
            }
        }
        return result;
    }

    /**
     * Returns the attribute info for the device.
     * 
     * @param device
     * @return
     * @throws SalsaDeviceException
     */
    private static AttributeInfo getAttributeInfo(IDevice device) throws SalsaDeviceException {
        AttributeInfo attributeInfo = null;
        if (device != null) {
            String name = device.getName();
            try {
                if (name != null) {
                    int slashIndex = name.lastIndexOf('/');
                    if (slashIndex < 0) {
                        name = ApiUtil.get_db_obj().get_attribute_from_alias(name);
                        slashIndex = name.lastIndexOf('/');
                    }
                    if (slashIndex > 0) {
                        String deviceName = name.substring(0, slashIndex);
                        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
                        if (proxy == null) {
                            LOGGER.error("Could not get DeviceProxy for " + deviceName);
                        } else {
                            attributeInfo = proxy.get_attribute_info(name.substring(slashIndex + 1));
                        }
                    }
                }
            } catch (Exception e) {
                SalsaDeviceException salsaDeviceException = new SalsaDeviceException(
                        "Cannot read attribute info for attribute " + device.getName(), e);
                throw salsaDeviceException;
            }
        }
        return attributeInfo;
    }

}

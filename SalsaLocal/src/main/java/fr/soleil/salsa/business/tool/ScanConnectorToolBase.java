package fr.soleil.salsa.business.tool;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.CurrentConfigurationParser;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.business.DeviceConnector;
import fr.soleil.salsa.business.TrajectoryCalculator;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeIntegrated;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.ActuatorImpl;
import fr.soleil.salsa.entity.impl.ScanResultImpl;
import fr.soleil.salsa.entity.impl.SensorImpl;
import fr.soleil.salsa.entity.impl.Stage;
import fr.soleil.salsa.entity.impl.scan1d.ScanResult1DImpl;
import fr.soleil.salsa.entity.impl.scan2d.ScanResult2DImpl;
import fr.soleil.salsa.entity.scan2D.IScanResult2D;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaTrajectoryException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.ILogService;
import fr.soleil.salsa.tool.SalsaUtils;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;

/**
 * Base class for scan connector tools. A new tool must be created for each
 * scan.
 */
public class ScanConnectorToolBase {

    public static final Logger LOGGER = LoggingUtil.getLogger(ScanConnectorToolBase.class);

    private static final String RECORD_DATA = "recordData";

    protected String scanServerName;
    private boolean dataRecorderPartialMode = false;
    private boolean dataRecorderEnable = true;
    protected String actionName = null;

    /**
     * Constructor
     * 
     * @param scanServerName
     */
    public ScanConnectorToolBase(String scanServerName) throws SalsaDeviceException {
        this.scanServerName = scanServerName;
    }

    /**
     * Converts a List of Doubles (Object) to an array of doubles (primitive).
     * 
     * @param listDouble
     * @return
     */
    protected double[] toDoubleArray(List<Double> listDouble) {
        double[] doubleArray = new double[listDouble.size()];
        int index = 0;
        for (Double element : listDouble) {
            doubleArray[index] = element;
            ++index;
        }
        return doubleArray;
    }

    /**
     * Send a command to the scan server.
     * 
     * @param command
     */
    protected void command(String commandName) throws DevFailed {
        actionName = "command_inout(" + commandName + ")";
        if (TangoCommandHelper.doesCommandExist(scanServerName, commandName)) {
            LOGGER.trace("Execute command = {}/{}", scanServerName, commandName);
            new TangoCommand(scanServerName, commandName).execute();
        }
    }

    protected boolean mayWriteAttribute(String attributeName, boolean testIfIsRunning) {
        return !testIfIsRunning || TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName);
    }

    /**
     * Sets an attribute.
     * 
     * @param attr
     */
    protected void setAttribute(String attributeName, boolean attributeValue, boolean testIfIsRunning)
            throws DevFailed {
        if (mayWriteAttribute(attributeName, testIfIsRunning)) {
            new TangoAttribute(scanServerName + TangoDeviceHelper.SLASH + attributeName).write(attributeValue);
        }
    }

    /**
     * Sets an attribute.
     * 
     * @param attr
     */
    protected void setAttribute(String attributeName, int attributeValue, boolean testIfIsRunning) throws DevFailed {
        if (mayWriteAttribute(attributeName, testIfIsRunning)) {
            new TangoAttribute(scanServerName + TangoDeviceHelper.SLASH + attributeName).write(attributeValue);
        }
    }

    /**
     * Sets an attribute.
     * 
     * @param attr
     */
    protected void setAttribute(String attributeName, double attributeValue, boolean testIfIsRunning) throws DevFailed {
        if (mayWriteAttribute(attributeName, testIfIsRunning)) {
            new TangoAttribute(scanServerName + TangoDeviceHelper.SLASH + attributeName).write(attributeValue);
        }
    }

    /**
     * Sets an attribute.
     * 
     * @param attr
     */
    protected <T> void setAttribute(String attributeName, T attributeValue, boolean testIfIsRunning) throws DevFailed {
        if ((attributeValue != null) && mayWriteAttribute(attributeName, testIfIsRunning)) {
            new TangoAttribute(scanServerName + TangoDeviceHelper.SLASH + attributeName).write(attributeValue);
        }
    }

    protected String getStageAttributeName(Stage stage) {
        String hook = null;
        switch (stage) {
            case PRE_RUN:
                hook = CurrentScanDataModel.PRE_RUN_HOOK;
                break;
            case PRE_SCAN:
                hook = CurrentScanDataModel.PRE_SCAN_HOOK;
                break;
            case PRE_STEP:
                hook = CurrentScanDataModel.PRE_STEP_HOOK;
                break;
            case POST_ACTUATOR_MOVE:
                hook = CurrentScanDataModel.POST_ACTUATOR_HOOK;
                break;
            case POST_INTEGRATION:
                hook = CurrentScanDataModel.POST_INTEGRATION_HOOK;
                break;
            case POST_STEP:
                hook = CurrentScanDataModel.POST_STEP_HOOK;
                break;
            case POST_RUN:
                hook = CurrentScanDataModel.POST_RUN_HOOK;
                break;
            case POST_SCAN:
                hook = CurrentScanDataModel.POST_SCAN_HOOK;
                break;
            default:
                break;
        }
        return hook;
    }

    /**
     * Checks if a scan result can be read.
     * 
     * @param scanServerName
     *            the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     */
    public boolean isScanResultReady() throws SalsaDeviceException {
        return CurrentScanDataModel.isScanResultReady(scanServerName);
    }

    public boolean isDataRecorderPartialMode() {
        return dataRecorderPartialMode;
    }

    public void setDataRecorderPartialMode(boolean mode) {
        dataRecorderPartialMode = mode;
    }

    public boolean isDataRecorderEnable() {
        return dataRecorderEnable && isDataRecorderAvailable();
    }

    public void setDataRecorderEnable(boolean enable) {
        dataRecorderEnable = enable;
    }

    private boolean isDataRecorderAvailable() {
        boolean result = false;

        String scanServer = SalsaAPI.getDevicePreferences().getScanServer();

        Database database = TangoDeviceHelper.getDatabase();
        if ((database != null) && SalsaUtils.isDefined(scanServer)) {
            try {
                DbDatum dbDatum = database.get_device_property(scanServer, "DataRecorder");
                if (dbDatum != null) {
                    String dataRecorderDeviceName = dbDatum.extractString();
                    result = TangoDeviceHelper.isDeviceRunning(dataRecorderDeviceName);
                }
            } catch (DevFailed e) {
                LOGGER.error("Cannot read {}/DataRecorder property {}", scanServer, DevFailedUtils.toString(e));
                LOGGER.trace("Stack trace", e);
            }
        }

        return result;
    }

    protected void SetHooks(IScanAddOns sao, DeviceProxy scanServerProxy) throws DevFailed {

        // Empty the hooklist before
        Stage[] stages = Stage.values();
        for (Stage stage : stages) {
            String attributeName = getStageAttributeName(stage);
            if (attributeName != null) {
                String[] commandsTab = new String[0];
                DeviceAttribute da = new DeviceAttribute(attributeName);
                da.insert(commandsTab);
                scanServerProxy.write_attribute(da);
            }
        }

        if ((sao != null) && (sao.getHooks() != null)) {
            List<IHook> hooks = sao.getHooks();
            for (IHook h : hooks) {
                Stage stage = h.getStage();
                String attributeName = getStageAttributeName(stage);
                if (attributeName != null) {
                    List<String> commands = new ArrayList<>();
                    if (h.getCommandsList() == null) {
                        continue;
                    }
                    for (IHookCommand c : h.getCommandsList()) {
                        if ((c.getCommand() != null) && (c.getCommand().trim().length() > 0) && c.isEnable()) {
                            commands.add(c.getCommand());
                        }
                    }
                    String[] commandsTab = new String[commands.size()];
                    commandsTab = commands.toArray(commandsTab);
                    DeviceAttribute da = new DeviceAttribute(attributeName);
                    da.insert(commandsTab);
                    writeAttributeLog(attributeName, Arrays.toString(commandsTab));
                    scanServerProxy.write_attribute(da);
                }
            }
        }
    }

    protected void initScanCommon(IConfig<?> config, String scanServerName) throws SalsaDeviceException {
        LOGGER.trace("initScanCommon({})", config);
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else {
            try {

                // Time out
                actionName = "set_timeout_millis(10000)";
                LOGGER.trace("{}/{}", scanServerName, actionName);
                scanServerProxy.set_timeout_millis(10000);

                // First Init Clean
                // command("Clean");//The Clean command do not clean anything
                // bug 0023724
                actionName = "Init";
                command("Init");

                // Partial mode for recording,clean set mode to false
                boolean isPartialMode = isDataRecorderPartialMode();
                actionName = writeAttributeLog(CurrentScanDataModel.DATA_RECORDER_PARTIAL_MODE,
                        String.valueOf(isPartialMode));
                setAttribute(CurrentScanDataModel.DATA_RECORDER_PARTIAL_MODE, isPartialMode, false);

                // Run name JIRA SCAN-163
                String runName = config.getRunName();
                if (!SalsaUtils.isDefined(runName)) {
                    String configName = config.getName();

                    IDirectory directory = config.getDirectory();
                    while (directory != null) {
                        configName = directory.getName() + "." + configName;
                        directory = directory.getDirectory();
                    }
                    runName = configName;
                }

                actionName = writeAttributeLog(CurrentScanDataModel.RUN_NAME, runName);
                setAttribute(CurrentScanDataModel.RUN_NAME, runName, false);

                // Scan number.
                int scanNumber = config.getScanNumber();
                actionName = writeAttributeLog(CurrentScanDataModel.SCAN_NUMBER, String.valueOf(scanNumber));
                setAttribute(CurrentScanDataModel.SCAN_NUMBER, scanNumber, false);

                // Timebases
                List<ITimebase> timebasesList = config.getTimebaseList();
                List<String> timebasesNameList = new ArrayList<>(timebasesList.size());
                String deviceName = null;
                for (ITimebase timebase : timebasesList) {
                    deviceName = timebase.getName();
                    if (timebase.isEnabled() && SalsaUtils.isDefined(deviceName)) {
                        timebasesNameList.add(timebase.getName());
                    }
                }
                String[] timebasesNameArray = timebasesNameList.toArray(new String[timebasesNameList.size()]);

                actionName = writeAttributeLog(CurrentScanDataModel.TIMEBASES, Arrays.toString(timebasesNameArray));
                setAttribute(CurrentScanDataModel.TIMEBASES, timebasesNameArray, false);

                // Sensors
                List<ISensor> sensorsList = config.getSensorsList();
                List<String> sensorsNameList = new ArrayList<>(sensorsList.size());
                for (ISensor sensor : sensorsList) {
                    deviceName = sensor.getName();
                    if (sensor.isEnabled() && SalsaUtils.isDefined(deviceName)) {
                        sensorsNameList.add(sensor.getName());
                    }
                }
                String[] sensorsNameArray = sensorsNameList.toArray(new String[sensorsNameList.size()]);
                actionName = writeAttributeLog(CurrentScanDataModel.SENSORS_LIST, Arrays.toString(sensorsNameArray));
                setAttribute(CurrentScanDataModel.SENSORS_LIST, sensorsNameArray, false);

                // Actuator delay.
                double actuatorsDelay = config.getActuatorsDelay();
                actionName = writeAttributeLog(CurrentScanDataModel.ACTUATOR_DELAY, String.valueOf(actuatorsDelay));
                setAttribute(CurrentScanDataModel.ACTUATOR_DELAY, actuatorsDelay, false);

                // Timebase delay.
                double timebasesDelay = config.getTimebasesDelay();
                actionName = writeAttributeLog(CurrentScanDataModel.TIMEBASE_DELAY, String.valueOf(timebasesDelay));
                setAttribute(CurrentScanDataModel.TIMEBASE_DELAY, timebasesDelay, false);

                // Zig zag.
                boolean zigzag = config.isZigzag();
                actionName = writeAttributeLog(CurrentScanDataModel.ZIGZAG, String.valueOf(zigzag));
                setAttribute(CurrentScanDataModel.ZIGZAG, zigzag, false);

                // Enable actuator speed.
                boolean enableScanSpeed = config.isEnableScanSpeed();
                actionName = writeAttributeLog(CurrentScanDataModel.ENABLESCANSPEED, String.valueOf(enableScanSpeed));
                setAttribute(CurrentScanDataModel.ENABLESCANSPEED, enableScanSpeed, false);

                // Post scan behaviour.
                IPostScanBehaviour postScanBehaviour = config.getScanAddOn().getPostScanBehaviour();
                Behaviour behaviour = postScanBehaviour.getBehaviour();
                if (behaviour == null) {
                    behaviour = Behaviour.NOOP;
                }
                int behaviourType = behaviour.getType();
                actionName = writeAttributeLog(CurrentScanDataModel.AFTER_ACTION_TYPE, String.valueOf(behaviourType));
                setAttribute(CurrentScanDataModel.AFTER_ACTION_TYPE, behaviourType, false);

                if (behaviour.getArgumentCount() >= 1) {
                    int behaviourSensorIndex = postScanBehaviour.getSensor();
                    actionName = writeAttributeLog(CurrentScanDataModel.AFTER_ACTION_SENSOR,
                            String.valueOf(behaviourSensorIndex));
                    setAttribute(CurrentScanDataModel.AFTER_ACTION_SENSOR, behaviourSensorIndex, false);
                } else if (behaviour.getArgumentCount() >= 2) {
                    int behaviourActuatorIndex = postScanBehaviour.getActuator();
                    actionName = writeAttributeLog(CurrentScanDataModel.AFTER_ACTION_ACTUATOR,
                            String.valueOf(behaviourActuatorIndex));
                    setAttribute(CurrentScanDataModel.AFTER_ACTION_ACTUATOR, behaviourActuatorIndex, false);
                }

                // Error strategies.
                if ((config.getScanAddOn() != null) && (config.getScanAddOn().getErrorStrategy() != null)) {

                    IErrorStrategy errorStrat = config.getScanAddOn().getErrorStrategy();
                    IErrorStrategyItem[] categoriesESI = new IErrorStrategyItem[] {
                            errorStrat.getActuatorsErrorStrategy(), errorStrat.getSensorsErrorStrategy(),
                            errorStrat.getTimebasesErrorStrategy(), errorStrat.getHooksErrorStrategy() };
                    String[] categoriesStr = new String[] { "actuators", "sensors", "timebases", "hooks" };

                    for (int i = 0; i < categoriesStr.length; i++) {
                        String cat = categoriesStr[i];
                        IErrorStrategyItem esi = categoriesESI[i];

                        double errorStrategyTimeOut = esi.getTimeOut();
                        int errorStrategyRetryCount = esi.getRetryCount();
                        double errorStrategyRetryTimeOut = esi.getTimeBetweenRetries();
                        int errorStrategyType = esi.getStrategy().ordinal();

                        // Time out.
                        String catAttribute = cat + "TimeOut";
                        actionName = writeAttributeLog(catAttribute, String.valueOf(errorStrategyTimeOut));
                        scanServerProxy.write_attribute(new DeviceAttribute(catAttribute, errorStrategyTimeOut));
                        // Retry count.
                        catAttribute = cat + "RetryCount";
                        actionName = writeAttributeLog(catAttribute, String.valueOf(errorStrategyRetryCount));
                        scanServerProxy.write_attribute(new DeviceAttribute(catAttribute, errorStrategyRetryCount));
                        // Retry time out.
                        catAttribute = cat + "RetryTimeOut";
                        actionName = writeAttributeLog(catAttribute, String.valueOf(errorStrategyRetryTimeOut));
                        scanServerProxy.write_attribute(new DeviceAttribute(catAttribute, errorStrategyRetryTimeOut));
                        // Error strategy.
                        catAttribute = cat + "ErrorStrategy";
                        actionName = writeAttributeLog(catAttribute, String.valueOf(errorStrategyType));
                        scanServerProxy.write_attribute(new DeviceAttribute(catAttribute, errorStrategyType));
                    }
                    // Context validation error strategy.
                    int erreurStrategyValue = errorStrat.getContextValidationStrategy().ordinal();
                    actionName = writeAttributeLog(CurrentScanDataModel.CONTEXT_VALIDATION_STRATEGY,
                            String.valueOf(erreurStrategyValue));
                    setAttribute(CurrentScanDataModel.CONTEXT_VALIDATION_STRATEGY, erreurStrategyValue, false);

                    String value = errorStrat.getContextValidationDevice();
                    if (SalsaUtils.isDefined(value)) {
                        actionName = writeAttributeLog(CurrentScanDataModel.CONTEXT_VALIDATION, value);
                        setAttribute(CurrentScanDataModel.CONTEXT_VALIDATION, value, false);
                    }
                }

                /* Hooks */
                actionName = "set_hook";
                SetHooks(config.getScanAddOn(), scanServerProxy);

                // On the fly mode (software continuous mode).
                boolean onTheFly = config.isOnTheFly();
                actionName = writeAttributeLog(CurrentScanDataModel.ON_THE_FLY, String.valueOf(onTheFly));
                setAttribute(CurrentScanDataModel.ON_THE_FLY, onTheFly, false);

                if (isDataRecorderEnable()
                        && TangoAttributeHelper.isAttributeRunning(scanServerName, "dataRecorderConfig")) {
                    // Enable or disable DataRecorder
                    String script = config.getDataRecorderConfig();
                    if (script == null) {
                        script = ObjectUtils.EMPTY_STRING;
                    }
                    actionName = writeAttributeLog(CurrentScanDataModel.DATA_RECORDER_CONFIG, String.valueOf(script));
                    setAttribute(CurrentScanDataModel.DATA_RECORDER_CONFIG, script.trim(), true);
                }

                // Write scan info

                String[] scanConfig = config.getScanInfo();
                actionName = writeAttributeLog(CurrentScanDataModel.SCAN_INFO, Arrays.toString(scanConfig));
                setAttribute(CurrentScanDataModel.SCAN_INFO, scanConfig, true);

                // End of common configuration.
            } catch (DevFailed e) {
                if (actionName == null) {
                    actionName = ObjectUtils.EMPTY_STRING;
                }
                String errorMessage = actionName + " Failed " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                SalsaDeviceException salsex = new SalsaDeviceException(errorMessage, e);
                throw salsex;
            }
        }

    }

    protected String writeAttributeLog(String attributeName, String value) {
        String logAttribute = "write_attribute(\"" + attributeName + "\"," + value + ")";
        LOGGER.trace("{}.{}", scanServerName, logAttribute);
        return logAttribute;
    }

    protected void startScan() throws DevFailed {
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, RECORD_DATA)) {
            boolean record = isDataRecorderEnable();
            // Enable or disable DataRecorder
            actionName = writeAttributeLog(RECORD_DATA, String.valueOf(record));
            setAttribute(RECORD_DATA, record, true);
        }

        // Start scan.
        actionName = ILogService.START_ACTION;
        command(ILogService.START_ACTION);
    }

    protected void setDimensionAttribute(String scanServerName, int dimensionIndex, IDimension dimension,
            boolean onTheFly) throws SalsaDeviceException {
        String indexStr = String.valueOf(dimensionIndex);
        if (dimensionIndex == 1) {
            indexStr = ObjectUtils.EMPTY_STRING;
        }

        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else if (dimension != null) {
            try {
                List<String> actuatorsNamesList;
                String[] actuatorsNamesArray;
                List<IActuator> actuatorsList = dimension.getActuatorsList();
                actuatorsNamesList = new ArrayList<>(actuatorsList.size());
                String deviceName = null;
                for (IActuator actuator : actuatorsList) {
                    deviceName = actuator.getName();
                    if (actuator.isEnabled() && SalsaUtils.isDefined(deviceName)) {
                        actuatorsNamesList.add(deviceName);
                    }
                }
                actuatorsNamesArray = actuatorsNamesList.toArray(new String[actuatorsNamesList.size()]);

                String actuatorName = "actuators" + indexStr;
                actionName = writeAttributeLog(actuatorName, Arrays.toString(actuatorsNamesArray));
                setAttribute(actuatorName, actuatorsNamesArray, false);

                // Dimensions
                // Tango exchanges trajectories as double arrays that contains the positions, in order, actuator after
                // actuator, range after range, of the trajectories of all the actuators.
                // There is one such array per dimension.
                double[] allActuatorsPositionsArray;
                double initialValue;
                // The array of integrations times.
                double[] integrationsTimesArray = null;
                // Contains the positions in order, range after range, of the trajectories of an actuator.
                List<Double> actuatorPositionsList;
                // Contains the positions in order, actuator after actuator, range after range, of the trajectories of
                // all the actuators.
                List<Double> allActuatorsPositionsList;
                // The list of speeds.
                List<Double> speedList = new ArrayList<>();
                // The number of points, which is the total steps numbers + 1 per range.
                int totalStepsNumber;
                // The positions, sorted as Tango expect them.
                allActuatorsPositionsList = new ArrayList<>();

                // The number of enabled actuators.
                int enabledActuatorsNumber = 0;

                if (dimension instanceof IDimensionK) {
                    enabledActuatorsNumber = 1;
                    ITrajectoryK trajectoryK = ((IDimensionK) dimension).getRangeX().getTrajectory();
                    allActuatorsPositionsArray = TrajectoryCalculator.calculateKTrajectoriesPosition(trajectoryK);
                    integrationsTimesArray = TrajectoryCalculator.calculateIntegrationTimesK(trajectoryK);
                    totalStepsNumber = allActuatorsPositionsArray.length;
                    speedList.add(trajectoryK.getSpeed());
                } else {
                    // The positions must be sorted by actuator, so we loop over the actuators.
                    for (int index = 0; index < actuatorsList.size(); index++) {
                        IActuator actuator = actuatorsList.get(index);
                        deviceName = actuator.getName();
                        if (actuator.isEnabled() && SalsaUtils.isDefined(deviceName)) {
                            actuatorPositionsList = new ArrayList<>();
                            // For each actuators, the positions must be sorted by range.
                            List<ITrajectory> completetrajectoryList = findActuatorTrajectories(dimension, index);
                            List<ITrajectory> trajectoryList = new ArrayList<>();
                            if (onTheFly) {
                                trajectoryList.add(completetrajectoryList.get(0));
                            } else {
                                trajectoryList.addAll(completetrajectoryList);
                            }

                            initialValue = DeviceConnector.getData(actuator);

                            for (ITrajectory trajectory : trajectoryList) {
                                actuatorPositionsList
                                        .addAll(TrajectoryCalculator.calculateLinearTrajectoriesPosition(trajectory,
                                                initialValue, (onTheFly || (trajectory instanceof ITrajectoryHCS))));
                                // The speeds must be sorted in the same order, so we read them here.
                                speedList.add(trajectory.getSpeed());
                            }
                            allActuatorsPositionsList.addAll(actuatorPositionsList);
                            ++enabledActuatorsNumber;
                        }
                    }

                    // Integration Time and steps number.
                    List<? extends IRange> getRangeList = dimension.getRangeList();
                    List<IRange> rangeList = new ArrayList<>();

                    if (onTheFly) {
                        rangeList.add(getRangeList.get(0));
                    } else {
                        rangeList.addAll(getRangeList);
                    }

                    int stepsNumber;
                    totalStepsNumber = 0;
                    for (IRange range : rangeList) {
                        if ((range instanceof IRangeHCS) || onTheFly) {
                            stepsNumber = 1;
                        } else {
                            stepsNumber = range.getStepsNumber();
                        }

                        if (range instanceof IRangeIntegrated) {
                            double[] integrationTime = ((IRangeIntegrated) range).getIntegrationTime();
                            // Ignore Scan-2D-Y integration time or incorrect range configuration
                            if (integrationTime != null) {
                                integrationsTimesArray = ArrayUtils.addAll(integrationsTimesArray, integrationTime);
                            }

                        }
                        totalStepsNumber += stepsNumber + 1;
                    }

                    // Builds the array from the list.
                    allActuatorsPositionsArray = toDoubleArray(allActuatorsPositionsList);

                } // end if (dimension instanceof IDimensionK) ... else

                String pointNumberName = "pointNumber" + indexStr;
                actionName = writeAttributeLog(pointNumberName, String.valueOf(totalStepsNumber));
                setAttribute(pointNumberName, totalStepsNumber, false);

                // Trajectories.
                // Sends the array to Tango.
                String trajectoriesName = "trajectories" + indexStr;
                DeviceAttribute trajectoriesAttribute = new DeviceAttribute(trajectoriesName);
                if (enabledActuatorsNumber != 0) {
                    trajectoriesAttribute.insert(allActuatorsPositionsArray, totalStepsNumber, enabledActuatorsNumber);
                } else {
                    trajectoriesAttribute.insert(new double[] {}, 0, 1);
                }

                // Attribute only manage in X dimension
                if (dimensionIndex == 1) {
                    actionName = writeAttributeLog("integrationTimes", Arrays.toString(integrationsTimesArray));
                    setAttribute("integrationTimes", integrationsTimesArray, false);

                    // Speed.
                    double[] speedArray = toDoubleArray(speedList);
                    actionName = writeAttributeLog("scanSpeed", Arrays.toString(speedArray));
                    setAttribute("scanSpeed", speedArray, false);
                }

                actionName = writeAttributeLog(trajectoriesName, Arrays.toString(allActuatorsPositionsArray));
                scanServerProxy.write_attribute(trajectoriesAttribute);

            } catch (DevFailed e) {
                if (actionName == null) {
                    actionName = ObjectUtils.EMPTY_STRING;
                }
                String errorMessage = actionName + " Failed " + DevFailedUtils.toString(e);
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(errorMessage, e);
            } catch (SalsaTrajectoryException e) {
                LOGGER.error("Cannot calculate trajectory {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(e);
            } catch (Exception e) {
                LOGGER.error("Cannot setDimensionAttribute {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
                throw new SalsaDeviceException(e);
            }
        }
    }

    /**
     * Extract the trajectories of an actuator, and return them in the range
     * order.
     * 
     * @return
     */
    private List<ITrajectory> findActuatorTrajectories(IDimension dimension, int index) {
        List<? extends IRange> rangeList = dimension.getRangeList();
        List<ITrajectory> actuatorTrajectoriesList = new ArrayList<>(rangeList.size());
        List<ITrajectory> tmpTrajectoryList = null;
        for (IRange range : rangeList) {
            tmpTrajectoryList = range.getTrajectoriesList();
            if ((tmpTrajectoryList != null) && (index < tmpTrajectoryList.size())) {
                ITrajectory trajectory = range.getTrajectoriesList().get(index);
                actuatorTrajectoriesList.add(trajectory);
            }
        }
        return actuatorTrajectoriesList;
    }

    public IScanResult retrieveCommonScanResult(boolean withTrajectories) throws SalsaDeviceException {
        IScanResult scanResult = null;
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else {
            if (isScanResultReady()) {
                try {
                    // Type
                    actionName = "read_attribute(\"" + CurrentScanDataModel.SCAN_TYPE + "\")";
                    DeviceAttribute scanTypeAttribute = scanServerProxy.read_attribute(CurrentScanDataModel.SCAN_TYPE);
                    int scanType = scanTypeAttribute.extractLong();
                    LOGGER.trace("{}.{}={}", scanServerName, actionName, String.valueOf(scanType));
                    // 0 -> time scan
                    // 1 -> scan 1d
                    // 2 -> scan 2d
                    switch (scanType) {
                        case 0:
                            scanResult = new ScanResultImpl();
                            scanResult.setResultType(IScanResult.ResultType.RESULT_TIMESCAN);
                            break;
                        case 1:
                            scanResult = new ScanResult1DImpl();
                            scanResult.setResultType(IScanResult.ResultType.RESULT_1D);
                            break;
                        case 2:
                            scanResult = new ScanResult2DImpl();
                            scanResult.setResultType(IScanResult.ResultType.RESULT_2D);
                            break;
                    }

                    if (scanResult != null) {
                        scanResult.setScanServer(scanServerName);

                        // Name.
                        actionName = "read_attribute(\"" + CurrentScanDataModel.RUN_NAME + "\")";
                        scanResult.setRunName(CurrentScanDataModel.readRunName(scanServerName));

                        CurrentScanDataModel.updateDataModel(scanServerName);
                        LOGGER.trace(scanServerName + "." + actionName + "=" + scanResult.getRunName());

                        // Sensors
                        actionName = "read_attribute(\"" + CurrentScanDataModel.SENSORS_DATA_LIST + "\")";
                        String[] sensorsValueKeysArray = CurrentScanDataModel.getSensors(scanServerName);
                        LOGGER.trace("{}.{}={}", scanServerName, actionName, Arrays.toString(sensorsValueKeysArray));
                        ISensor sensor = null;
                        String sensorName = null;
                        String entityName = null;

                        if (sensorsValueKeysArray != null) {
                            for (String sensorValueKey : sensorsValueKeysArray) {
                                entityName = TangoDeviceHelper.getEntityName(sensorValueKey);
                                sensorName = TangoAttributeHelper.getLabel(scanServerName, entityName);
                                sensor = new SensorImpl();
                                sensor.setName(sensorName);
                                sensor.setEnabled(true);
                                scanResult.getSensorsList().add(sensor);
                                sensor.setScanServerAttributeName(sensorValueKey);
                            }
                        }

                        // Actuators dimension X
                        actionName = "read_attribute(\"" + CurrentScanDataModel.ACTUATORS_DATA_LIST + " "
                                + CurrentScanDataModel.YACTUATORS_DATA_LIST + "\")";
                        String[] actuatorsValueKeysArray = CurrentScanDataModel.getActuators(scanServerName);
                        LOGGER.trace("{}.{}={}", scanServerName, actionName, Arrays.toString(actuatorsValueKeysArray));
                        IActuator actuator = null;
                        String actuatorName = null;
                        List<IActuator> actuatorList = null;

                        if (actuatorsValueKeysArray != null) {
                            for (String actuatorValueKey : actuatorsValueKeysArray) {
                                entityName = TangoDeviceHelper.getEntityName(actuatorValueKey);
                                actuatorName = TangoAttributeHelper.getLabel(scanServerName, entityName);
                                actuator = new ActuatorImpl();
                                actuator.setName(actuatorName);
                                actuator.setEnabled(true);
                                // X Actuator
                                if (entityName.startsWith("actuator_1")) {
                                    actuatorList = scanResult.getActuatorsXList();
                                } else if (entityName.startsWith("actuator_2")
                                        && (scanResult instanceof IScanResult2D)) {
                                    actuatorList = ((IScanResult2D) scanResult).getActuatorsYList();
                                }

                                if (actuatorList != null) {
                                    actuatorList.add(actuator);
                                }

                                actuator.setScanServerAttributeName(actuatorValueKey);
                            }

                            if (withTrajectories) {
                                HashMap<IActuator, double[]> trajectoryMap = new HashMap<>();
                                actuatorList = scanResult.getActuatorsXList();
                                if (SalsaUtils.isFulfilled(actuatorList)) {
                                    double[] trajectoryX = CurrentConfigurationParser.readFlatTrajectory(scanServerName,
                                            1);
                                    fillTrajectoryMap(trajectoryMap, actuatorList, trajectoryX);
                                }

                                if (scanResult instanceof IScanResult2D) {
                                    actuatorList = ((IScanResult2D) scanResult).getActuatorsYList();
                                    if (SalsaUtils.isFulfilled(actuatorList)) {
                                        double[] trajectoryY = CurrentConfigurationParser
                                                .readFlatTrajectory(scanServerName, 2);
                                        fillTrajectoryMap(trajectoryMap, actuatorList, trajectoryY);
                                    }
                                }
                                scanResult.setTrajectoryMap(trajectoryMap);
                            }
                        }
                    }
                } catch (DevFailed e) {
                    String errorMessage = actionName + " Failed " + DevFailedUtils.toString(e);
                    LOGGER.error(errorMessage);
                    LOGGER.debug("Stack trace", e);
                    throw new SalsaDeviceException(errorMessage, e);
                } catch (Exception ex) {
                    String errorMessage = actionName + " Failed " + ex.getMessage();
                    LOGGER.error(errorMessage);
                    LOGGER.debug("Stack trace", ex);
                    throw new SalsaDeviceException(errorMessage, ex);
                }
            }
        }
        return scanResult;
    }

    private void fillTrajectoryMap(HashMap<IActuator, double[]> trajectoryMap, List<IActuator> actuatorList,
            double[] flatValues) {
        if ((trajectoryMap != null) && (actuatorList != null) && (flatValues != null) && (flatValues.length > 0)) {
            int nbactuator = actuatorList.size();
            if (nbactuator > 0) {
                int nbPoints = flatValues.length / nbactuator;
                double[] simpleTrajectory = null;
                for (int i = 0; i < nbactuator; i++) {
                    simpleTrajectory = new double[nbPoints];
                    System.arraycopy(flatValues, i * nbPoints, simpleTrajectory, 0, nbPoints);
                    trajectoryMap.put(actuatorList.get(i), simpleTrajectory);
                }
            }
        }
    }

    public static void main(String[] args) {

        String scanServerName = "ICA/SALSA/SCAN.1";
        if ((args != null) && (args.length > 0)) {
            scanServerName = args[0];
        }
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        final JTextArea area = new JTextArea();
        area.setEditable(false);

        final JButton button = new JButton("read scan result");

        try {
            final ScanConnectorToolBase connector = new ScanConnectorToolBase(scanServerName);

            button.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        IScanResult result = connector.retrieveCommonScanResult(true);
                        Map<IActuator, double[]> trajectoryMap = result.getTrajectoryMap();
                        StringBuilder builder = new StringBuilder();
                        if (result != null) {
                            builder.append("ScanServer=" + result.getScanServer() + "\n");
                            builder.append("Class=" + result.getClass().getName() + "\n");
                            builder.append("ScanType=" + result.getResultType() + "\n");
                            builder.append("RunName=" + result.getRunName() + "\n");
                            builder.append("SensorTimeStamps=" + result.getSensorsTimeStampsCompleteName() + "\n");
                            builder.append("ActuatorTimeStamps=" + result.getActuatorsTimeStampsCompleteName() + "\n");

                            List<ISensor> sensorList = result.getSensorsList();
                            builder.append("---Sensor size=" + sensorList.size() + "---\n");
                            for (ISensor sensor : sensorList) {
                                builder.append("Sensor name=" + sensor.getName() + "\n");
                                builder.append("Sensor attribute name=" + sensor.getScanServerAttributeName() + "\n");
                            }

                            List<IActuator> actuatorXList = result.getActuatorsXList();
                            builder.append("---Actuator X size=" + actuatorXList.size() + "---\n");
                            for (IActuator actuator : actuatorXList) {
                                builder.append("Actuator X name=" + actuator.getName() + "\n");
                                builder.append(
                                        "Actuator attribute name=" + actuator.getScanServerAttributeName() + "\n");
                                if ((trajectoryMap != null) && trajectoryMap.containsKey(actuator)) {
                                    builder.append(
                                            "Real trajectory =" + Arrays.toString(trajectoryMap.get(actuator)) + "\n");
                                }
                            }
                            if (result.getResultType() == IScanResult.ResultType.RESULT_2D) {
                                List<IActuator> actuatorYList = ((IScanResult2D) result).getActuatorsYList();
                                builder.append("---Actuator Y size=" + actuatorYList.size() + "---\n");
                                for (IActuator actuator : actuatorYList) {
                                    builder.append("Actuator Y name=" + actuator.getName() + "\n");
                                    builder.append(
                                            "Actuator attribute name=" + actuator.getScanServerAttributeName() + "\n");
                                    if ((trajectoryMap != null) && trajectoryMap.containsKey(actuator)) {
                                        builder.append("Real trajectory ="
                                                + Arrays.toString(trajectoryMap.get(actuator)) + "\n");
                                    }
                                }
                            }

                            area.setText(builder.toString());
                        }

                    } catch (SalsaDeviceException ex) {
                        LOGGER.error(ex.getMessage());
                        LOGGER.debug("Stack trace", ex);
                    }

                }

            });

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        panel.setLayout(new BorderLayout());
        panel.add(area, BorderLayout.CENTER);
        panel.add(button, BorderLayout.NORTH);

        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("reas scan result test");
        frame.setSize(600, 600);

        frame.setVisible(true);
    }
}

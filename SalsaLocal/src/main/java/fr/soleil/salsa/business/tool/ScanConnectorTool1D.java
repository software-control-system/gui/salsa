package fr.soleil.salsa.business.tool;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.exception.SalsaDeviceException;

/**
 * Scan connector tool for 1D config. A new tool must be created for each scan.
 */
public class ScanConnectorTool1D extends ScanConnectorToolBase {

    /**
     * Constructor.
     * 
     * @param scanServerName
     */
    public ScanConnectorTool1D(String scanServerName) throws SalsaDeviceException {
        super(scanServerName);
    }

    /**
     * Starts a scan.
     * 
     * @param config
     * @param scanServerName the name of the scan server to use.
     * @throws SalsaDeviceException
     */
    public void startScan(IConfig1D config, String scanServerName) throws SalsaDeviceException {
        int timeOut;
        DeviceProxy scanServerProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (scanServerProxy == null) {
            throw new SalsaDeviceException("Error : device proxy" + scanServerName + " is null",
                    new Exception(scanServerName + " proxy is null"));
        } else {
            try {
                // Read the initial value of the time out.
                timeOut = scanServerProxy.get_timeout_millis();
                initScanCommon(config, scanServerName);
                try {
                    // Actuators
                    actionName = "setDimensionAttribute";
                    setDimensionAttribute(scanServerName, 1, config.getDimensionX(), config.isOnTheFly());

                    // Hardware continuous scan : no.
                    actionName = "write_attribute(\"hwContinuous\"," + false + ")";
                    setAttribute("hwContinuous", false, false);

                    // End of specific configuration.

                    // Start scan.
                    startScan();
                } finally {
                    // Sets the time out back to the initial value.
                    scanServerProxy.set_timeout_millis(timeOut);
                }
            } catch (DevFailed e) {
                if (actionName == null) {
                    actionName = "";
                }
                throw new SalsaDeviceException(actionName + " Failed " + DevFailedUtils.toString(e), e);
            }
        }

    }

}

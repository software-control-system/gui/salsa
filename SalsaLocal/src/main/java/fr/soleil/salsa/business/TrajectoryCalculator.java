package fr.soleil.salsa.business;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeTrajectory;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.TrajectoryUtil;
import fr.soleil.salsa.exception.SalsaTrajectoryException;

/**
 * Tool for calculs related to trajectories.
 * 
 * @author Administrateur
 * 
 */
public class TrajectoryCalculator {

    private static List<Double> calculateLinearTrajectoriesPosition(ITrajectory trajectory,
            double initialValue, IRange range, boolean continuousScan) {
        if (!trajectory.getRelative()) {
            initialValue = 0d;
        }

        int stepsNumber = range.getStepsNumber();
        double beginPosition = trajectory.getBeginPosition();
        double endPosition = trajectory.getEndPosition();

        double[] positionValueList = TrajectoryUtil.calculateLinearTrajectory(initialValue,
                beginPosition, endPosition, stepsNumber, continuousScan);
        List<Double> positionsList = new ArrayList<Double>();

        if (positionValueList != null) {
            for (double value : positionValueList) {
                positionsList.add(value);
            }
        }
        return positionsList;
    }

    public static List<Double> calculateLinearTrajectoriesPosition(ITrajectory trajectory,
            double initialValue, boolean continuousScan) {
        List<Double> doubleValues = null;
        if (trajectory != null) {
            if (!continuousScan && trajectory.isCustomTrajectory()
                    && trajectory.getTrajectory() != null && trajectory.getTrajectory().length > 0) {
                double[] trajectoryValues = trajectory.getTrajectory();
                doubleValues = new ArrayList<Double>();
                for (double doubleValue : trajectoryValues) {
                    if (trajectory.getRelative()) {
                        doubleValues.add(doubleValue + initialValue);
                    }
                    else {
                        doubleValues.add(doubleValue);
                    }
                }
            }
            else if (trajectory instanceof IRangeTrajectory<?>) {
                doubleValues = calculateLinearTrajectoriesPosition(trajectory, initialValue,
                        ((IRangeTrajectory<?>) trajectory).getRange(), continuousScan);
            }
        }
        return doubleValues;
    }

    public static List<Double> calculateScanSpeedList(ITrajectory trajectory) {

        List<Double> scanSpeedList = new ArrayList<Double>();

        if (trajectory instanceof IRangeTrajectory<?>) {
            IRange range = ((IRangeTrajectory<?>) trajectory).getRange();
            double scanSpeed = trajectory.getSpeed();
            if (range != null) {
                int stepsNumber = range.getStepsNumber();
                if (range instanceof IRangeHCS) {
                    stepsNumber = 1;
                }

                for (int index = 0; index <= stepsNumber; index++) {
                    scanSpeedList.add(scanSpeed);
                }
            }
        }
        return scanSpeedList;
    }

    /**
     * Calculates the positions of an actuator depending on its trajectory.
     * 
     * @param trajectory
     * @param initialValue
     * @return
     */
    public static List<Double> calculateLinearTrajectoriesPosition(ITrajectory1D trajectory,
            double initialValue, boolean continusScan) {

        return calculateLinearTrajectoriesPosition(trajectory, initialValue, trajectory.getRange(),
                continusScan);
    }

    /**
     * Calculates the positions of an actuator depending on its trajectory.
     * 
     * @param trajectory
     * @param initialValue
     * @return
     */
    public static List<Double> calculateLinearTrajectoriesPosition(ITrajectoryHCS trajectory,
            double initialValue, IConfigHCS config, boolean continusScan) {

        return calculateLinearTrajectoriesPosition(trajectory, initialValue, trajectory.getRange(),
                continusScan);
    }

    /**
     * Calculates the positions of an actuator depending on its trajectory.
     * 
     * @param trajectory
     * @param initialValue
     * @return
     */
    public static List<Double> calculateLinearTrajectoriesPosition(ITrajectory2DX trajectory,
            double initialValue, boolean continusScan) {

        return calculateLinearTrajectoriesPosition(trajectory, initialValue, trajectory.getRange(),
                continusScan);
    }

    /**
     * Calculates the positions of an actuator depending on its trajectory.
     * 
     * @param trajectory
     * @param initialValue
     * @return
     */
    public static List<Double> calculateLinearTrajectoriesPosition(ITrajectoryEnergy trajectory,
            double initialValue, boolean continusScan) {

        return calculateLinearTrajectoriesPosition(trajectory, initialValue, trajectory.getRange(),
                continusScan);

    }

    /**
     * Calculates the positions of an actuator depending on its trajectory.
     * 
     * @param trajectory
     * @param initialValue
     * @return
     */
    public static List<Double> calculateLinearTrajectoriesPosition(ITrajectory2DY trajectory,
            double initialValue, boolean continusScan) {
        return calculateLinearTrajectoriesPosition(trajectory, initialValue, trajectory.getRange(),
                continusScan);
    }

    public static double[] calculateKTrajectoriesPosition(ITrajectoryK trajectoryK)
            throws SalsaTrajectoryException {

        return TrajectoryUtil.calculateKTrajectory(trajectoryK);
    }

    /**
     * Computes the integration times for a scan K.
     * 
     * @return
     */
    public static double[] calculateIntegrationTimesK(ITrajectoryK trajectoryK) {
        return TrajectoryUtil.calculateKIntegrationTimes(trajectoryK);
    }

}

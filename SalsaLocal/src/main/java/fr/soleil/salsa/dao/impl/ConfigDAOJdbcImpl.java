package fr.soleil.salsa.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.impl.DirectoryImpl;
import fr.soleil.salsa.entity.impl.scan1d.Config1DImpl;
import fr.soleil.salsa.entity.impl.scan2d.Config2DImpl;
import fr.soleil.salsa.entity.impl.scanenergy.ConfigEnergyImpl;
import fr.soleil.salsa.entity.impl.scanhcs.ConfigHCSImpl;
import fr.soleil.salsa.entity.impl.scank.ConfigKImpl;
import fr.soleil.salsa.exception.persistence.DirectoryOutOfSyncException;
import fr.soleil.salsa.exception.persistence.OutOfSyncException;
import fr.soleil.salsa.exception.persistence.ScanConfigOutOfSyncException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.persistence.model.Configuration;

/**
 * This class interacts with the database to save/delete data. To understand the
 * java sql types used in the different methods, you should take a look at MySQL
 * Reference Manual.
 * 
 * @author alike
 * @see Types
 * @see <a
 *      href="http://dev.mysql.com/doc/refman/5.0/en/connector-j-reference-type-conversions.html"
 *      >MySQL 5.0 Reference Manual: Java, JDBC and MySQL Types</a>
 */
public class ConfigDAOJdbcImpl {

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigDAOJdbcImpl.class);

    private Connection conn;
    private PreparedStatement preStmt;
    private Statement stmt;

    public void deleteConfig(IConfig<?> configuration) throws OutOfSyncException {
        if(configuration != null) {
            try {
                startOperation();
                deleteConfigNoCommit(configuration);
                if (conn != null) {
                    conn.commit();

                }
            } catch (OutOfSyncException oose) {
                rollback();
                String errorMessage = "Cannot delete config " + configuration.getFullPath()
                + " because it is out of synchronization";
                LOGGER.error(errorMessage);
                LOGGER.debug("Stack trace", oose);
                throw oose;
            } catch (Exception e) {
                String errorMessage = "Cannot delete config " + configuration.getFullPath() + " " + e.getMessage();
                LOGGER.debug("Stack trace", e);
                rollback();
                LOGGER.error(errorMessage);
            } finally {
                doCloseConn();
            }
        }
    }

    /**
     * Delete the configuration
     * 
     * @param EntityEjb
     *            : the entity to delete
     */
    private void deleteConfigNoCommit(IConfig<?> configuration) throws OutOfSyncException, SQLException {

        if ((configuration == null) || (configuration.getId() == null)) {
            return;
        }

        Configuration configResultat = null;

        // Load the config
        configResultat = getConfigById(configuration.getId());

        // Case when the entity is NULL
        if ((configResultat == null) || !configResultat.getTimestamp().equals(configuration.getTimestamp())) {
            throw new ScanConfigOutOfSyncException();
        }

        // delete the config
        deleteConfigNoCommitAux(configuration);
    }

    /**
     * Delete the {@link ChartProperties}
     * 
     * @param configId
     *            : the associated config id
     */
    private void deleteChartPropertiesNoCommit(int configId) throws SQLException {
        String query = "DELETE FROM chart_properties WHERE configid=" + configId;
        // Delete the chart properties
        if ((stmt == null) || stmt.isClosed()) {
            stmt = conn.createStatement();
        }
        if (stmt != null) {
            stmt.executeUpdate(query);
        }
    }

    /**
     * Delete the {@link PlotProperties}
     * 
     * @param configId
     *            : the associated config id
     */
    private void deletePlotPropertiesNoCommit(int configId) throws SQLException {
        String query = "DELETE FROM plot_properties WHERE configid=" + configId;
        // Delete the plot properties
        if ((stmt == null) || stmt.isClosed()) {
            stmt = conn.createStatement();
        }
        if (stmt != null) {
            stmt.executeUpdate(query);
        }
    }

    /**
     * delete the config by identifier
     * 
     * @param configId
     *            : the identifier of the config
     */
    private void deleteConfigNoCommitAux(IConfig<?> configuration) throws OutOfSyncException, SQLException {

        if ((configuration == null) || (configuration.getId() == null)) {
            return;
        }

        deleteChartPropertiesNoCommit(configuration.getId());
        deletePlotPropertiesNoCommit(configuration.getId());

        String sql = "DELETE FROM config WHERE configid=? AND timestamp=? ";

        // Delete the config
        preStmt = conn.prepareStatement(sql);
        preStmt.setObject(1, configuration.getId(), Types.INTEGER);
        preStmt.setObject(2, configuration.getTimestamp(), Types.TIMESTAMP);
        preStmt.executeUpdate();

    }

    public void deleteDirectory(IDirectory directory) throws OutOfSyncException {
        try {
            startOperation();
            deleteDirectoryNoCommit(directory);
            conn.commit();
        } catch (SQLException e) {
            CommonsJDBC.catchException(e);
            // return 0;
        } finally {
            doCloseConn();
        }
        // return 0;
    }

    /**
     * Delete the directory
     * 
     * @param DirectoryEjb
     *            :the DirectoryImpl to delete
     */
    private int deleteDirectoryNoCommit(IDirectory directory) throws OutOfSyncException, SQLException {

        if (directory == null) {
            return 0;// Operation of delete is not successful.
        }

        String sql = "DELETE FROM directory WHERE directoryid=? AND timestamp=? ";

        IDirectory directoryResult = null;

        int row = 0;

        // Check if the DirectoryImpl exists in the database
        directoryResult = getDirectoryById(directory.getId(), false);
        if (conn != null) {
            if ((directoryResult == null) || !directoryResult.getTimestamp().equals(directory.getTimestamp())) {
                conn.rollback();
                throw new DirectoryOutOfSyncException();
            }

            preStmt = conn.prepareStatement(sql);
            preStmt.setObject(1, directory.getId(), Types.INTEGER);
            preStmt.setObject(2, directory.getTimestamp(), Types.TIMESTAMP);

            // delete the directory
            row = preStmt.executeUpdate();
            stmt.clearBatch();
        }
        return row;

    }

    public boolean deleteDirectoryRecursively(IDirectory directory) throws OutOfSyncException {
        try {
            startOperation();
            deleteDirectoryRecursivelyNoCommit(directory);
            conn.commit();
            return true;
        } catch (SQLException e) {
            rollback();
            CommonsJDBC.catchException(e);
            throw new DirectoryOutOfSyncException();
        } finally {
            doCloseConn();
        }
        // return false;
    }

    /**
     * 
     * @param directory
     * @return
     * @throws OutOfSyncException
     */
    private boolean deleteDirectoryRecursivelyNoCommit(IDirectory directory) throws OutOfSyncException, SQLException {

        List<IDirectory> subDirs = directory.getSubDirectoriesList();
        for (IDirectory d : subDirs) {
            deleteDirectoryRecursivelyNoCommit(d);
        }

        // Delete the config belongs to directory
        List<IConfig<?>> confs = directory.getConfigList();
        for (IConfig<?> c : confs) {

            /*
             * Configuration cEjb = new Configuration(); cEjb.setId(c.getId());
             * cEjb.setTimestamp(c.getTimestamp());
             */
            if ((c != null) && (c.getId() != null)) {
                deleteConfigNoCommit(c);
            }
        }

        // Delete the directory
        deleteDirectoryNoCommit(directory);

        return true;
    }

    /**
     * Close the session
     */
    private void doCloseConn() {
        try {
            // Close the statement
            if (this.stmt != null) {
                this.stmt.close();
            }
            if (this.preStmt != null) {
                preStmt = null;

            }
            // if (this.conn != null) // Close the connection
            // this.conn.close();
        } catch (SQLException ex) {
            CommonsJDBC.catchException(ex);
        }
    }

    public Configuration getConfigById(Integer id) {
        Configuration result = null;
        try {
            startOperation();
            result = getConfigByIdAux(id);
        } catch (SQLException e) {
            CommonsJDBC.catchException(e);
            result = null;
        } finally {
            doCloseConn();
        }
        return result;
    }

    public Timestamp getConfigTimestampById(int configId) {
        Timestamp result = null;
        try {
            startOperation();
            result = getConfigTimestampByIdAux(configId);
        } catch (SQLException e) {
            CommonsJDBC.catchException(e);
            result = null;
        } finally {
            doCloseConn();
        }
        return result;
    }

    /**
     * Gets the configuration by id
     * 
     * @param configId
     * @return Configuration
     */
    private Configuration getConfigByIdAux(Integer configId) throws SQLException {

        Configuration configuration = null;
        String query = "SELECT configid, name, type, data, directoryid, timestamp FROM config WHERE configid="
            + configId;

        if (stmt != null) {
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {

                configuration = new Configuration();

                configuration.setId(rs.getInt("configid"));
                configuration.setName(rs.getString("name"));
                configuration.setType(rs.getString("type"));
                configuration.setData(rs.getString("data"));
                int directoryid = rs.getInt("directoryid");
                configuration.setDirectoryId(directoryid);
                configuration.setTimestamp(rs.getTimestamp("timestamp"));
            }
            rs.close();
            stmt.clearBatch();
            if (configuration != null) {
                int directoryid = configuration.getDirectoryId();
                IDirectory directory = getDirectoryById(directoryid, true);
                if (directory != null) {
                    configuration.setDirectory(directory);
                }
            }

        }
        return configuration;
    }

    /**
     * Gets the configuration time stamp by configuration id
     * 
     * @param configId
     *            The Id
     * @return A {@link Timestamp}
     */
    private Timestamp getConfigTimestampByIdAux(Integer configId) throws SQLException {

        Timestamp timestamp = null;
        String query = "SELECT timestamp FROM config WHERE configid=" + configId;

        if (stmt != null) {
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                timestamp = rs.getTimestamp("timestamp");
            }
            rs.close();
            stmt.clearBatch();
        }
        return timestamp;
    }

    /**
     * Gets the list of configuration
     * 
     * @return List<Configuration>
     */
    public List<Configuration> getConfigList() {
        List<Configuration> configList = new ArrayList<Configuration>();

        String query = "SELECT configid, Name, type, '' as data, directoryid, timestamp FROM config ORDER BY name ASC";

        try {
            startOperation();
            if (stmt != null) {
                ResultSet rs = stmt.executeQuery(query);

                Configuration config = null;
                while (rs.next()) {
                    config = new Configuration();

                    config.setId(rs.getInt("configid"));
                    config.setName(rs.getString("name"));
                    config.setType(rs.getString("type"));
                    config.setData(rs.getString("data"));
                    config.setDirectoryId(rs.getInt("directoryid"));
                    config.setTimestamp(rs.getTimestamp("timestamp"));
                    configList.add(config);
                }
                rs.close();
            }
        } catch (Exception e) {
            String errorMessage = "Cannot get configuration list " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
        } finally {
            doCloseConn();
        }

        return configList;
    }

    /**
     * Gets the DirectoryImpl by id
     * 
     * @param directoryId
     *            : the identifier of the directory
     * @return Directory
     */
    public IDirectory getDirectoryById(Integer directoryId, boolean recursive) {

        DirectoryImpl directory = null;
        String query = "SELECT * FROM directory where directoryid=" + directoryId;

        try {
            startOperation();
            if (stmt != null) {
                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {

                    directory = new DirectoryImpl();
                    directory.setId(rs.getInt("directoryid"));
                    directory.setName(rs.getString("name"));
                    directory.setPositionInDirectory(rs.getInt("positionInDirectory"));
                    directory.setTimestamp(rs.getTimestamp("timestamp"));
                    Integer dir = rs.getInt("directory");

                    if ((dir != null) && !rs.wasNull()) {
                        IDirectory parent = getDirectoryById(dir, recursive);
                        directory.setDirectory(parent);
                    }
                }
                rs.close();
                stmt.clearBatch();
            }
        } catch (SQLException e) {
            CommonsJDBC.catchException(e);
        } finally {
            doCloseConn();
        }
        if (recursive && (directory != null) && (directory.getDirectory() != null)) {
            directory.setDirectory(getDirectoryById(directory.getDirectory().getId(), recursive));
        }

        return directory;
    }

    /**
     * 
     * @return
     */
    private List<IDirectory> getDirectoryList() {
        List<IDirectory> directoryList = new ArrayList<IDirectory>();

        String query = "SELECT * FROM directory ORDER BY name ASC";

        try {
            startOperation();

            if (stmt != null) {
                ResultSet rs = stmt.executeQuery(query);

                DirectoryImpl directory = null;
                int positionInDirectory = 0;
                while (rs.next()) {
                    directory = new DirectoryImpl();
                    directory.setId(rs.getInt("directoryid"));
                    directory.setName(rs.getString("name"));
                    // directory.setPositionInDirectory(rs.getInt("positionInDirectory"));
                    directory.setPositionInDirectory(positionInDirectory++);
                    directory.setTimestamp(rs.getTimestamp("timestamp"));

                    int parentDirectoryId = rs.getInt("directory");

                    if (!rs.wasNull()) {
                        IDirectory parent = new DirectoryImpl();
                        parent.setId(parentDirectoryId);
                        directory.setDirectory(parent);
                        // directory.setParentDirectoryId(parentDirectoryId);
                    }
                    directoryList.add(directory);
                }
                rs.close();
            }
        } catch (SQLException e) {
            CommonsJDBC.catchException(e);
        } finally {
            doCloseConn();
        }

        return directoryList;
    }

    private Integer getGeneratedKey(Statement _s) throws SQLException {
        Integer result = null;
        ResultSet clefs = _s.getGeneratedKeys();
        if (clefs.next()) {
            result = clefs.getInt(1);
        }
        return result;
    }

    public IDirectory getRootDirectory() {

        List<IDirectory> directories = getDirectoryList();
        Hashtable<Integer, IDirectory> ht = new Hashtable<Integer, IDirectory>();

        IDirectory root = null;

        for (IDirectory d : directories) {
            if (d.getDirectory() == null) {
                root = d;
            }

            d.setSubDirectoriesList(new ArrayList<IDirectory>());
            d.setConfigList(new ArrayList<IConfig<?>>());
            ht.put(d.getId(), d);
        }

        for (IDirectory d : directories) {

            // Set parent
            if (d.getDirectory() != null) {
                int parentId = d.getDirectory().getId();
                IDirectory parent = ht.get(parentId);
                d.setDirectory(parent);

                parent.getSubDirectoriesList().add(d);
            }
        }

        List<Configuration> configs = getConfigList();

        for (Configuration c : configs) {
            String type = c.getType();
            IConfig<?> scan = null;
            if (type.equals("Config1DImpl") || type.equals("Config1D")) {
                scan = new Config1DImpl(); // new Config1D();
                scan.setType(IConfig.ScanType.SCAN_1D);
            } else if (type.equals("Config2DImpl") || type.equals("Config2D")) {
                scan = new Config2DImpl(); // new Config1D();
                scan.setType(IConfig.ScanType.SCAN_2D);
            } else if (type.equals("ConfigHCSImpl") || type.equals("ConfigHCS")) {
                scan = new ConfigHCSImpl();
                scan.setType(IConfig.ScanType.SCAN_HCS);
            } else if (type.equals("ConfigKImpl") || type.equals("ConfigK")) {
                scan = new ConfigKImpl();
                scan.setType(IConfig.ScanType.SCAN_K);
            } else if (type.equals("ConfigEnergyImpl") || type.equals("ConfigEnergy")) {
                scan = new ConfigEnergyImpl();
                scan.setType(IConfig.ScanType.SCAN_ENERGY);
            }
            scan.setName(c.getName());
            scan.setId(c.getId());
            scan.setTimestamp(c.getTimestamp());

            int directoryId = c.getDirectoryId();
            IDirectory directory = ht.get(directoryId);
            scan.setDirectory(directory);
            directory.getConfigList().add(scan);
        }

        return root;
    }

    /**
     * Saves a Configuration
     * 
     * @param config
     *            The {@link Configuration} to save
     * @param forceSaving
     *            = true ignore OutOfSync Error
     * @throws OutOfSyncException
     *             If the {@link Configuration} is <code>null</code> or out of
     *             synchronization with database
     */
    public void saveConfig(Configuration config, boolean forceSaving) throws OutOfSyncException {
        ScanConfigOutOfSyncException exception = null;
        if (config == null) {
            exception = new ScanConfigOutOfSyncException("Can't save a null configuration");
        } else {
            Configuration configuration = getConfigById(config.getId());
            boolean newConfig = (configuration == null);

            // If forceSaving = false test the synchronization
            if (!forceSaving) {
                // Modification of the timestamp when saved a new configuration
                Timestamp oldConfigTSP = config.getTimestamp();

                // Checking if the configuration is out of synchronization
                if (!newConfig && oldConfigTSP.before(configuration.getTimestamp())) {
                    exception = new ScanConfigOutOfSyncException("The configuration has been changed by other");
                }
            }

            if (exception == null) {
                StringBuilder queryBuilder = new StringBuilder();
                if (newConfig) {
                    queryBuilder.append("INSERT INTO config (name, type, data, directoryid) ");
                    queryBuilder.append(" VALUES (?,?,?,?)");
                } else {
                    queryBuilder.append("UPDATE config SET name=? ");
                    queryBuilder.append(" , type=? ");
                    queryBuilder.append(" , data=? ");
                    queryBuilder.append(" , directoryid=? ");
                    queryBuilder.append(" , timestamp=now() ");
                    queryBuilder.append(" WHERE configid=? ");

                }
                if (conn != null) {
                    try {
                        // Save in database
                        startOperation();
                        if (newConfig) {
                            preStmt = conn.prepareStatement(queryBuilder.toString(), Statement.RETURN_GENERATED_KEYS);
                        } else {
                            preStmt = conn.prepareStatement(queryBuilder.toString());
                        }
                        preStmt.setObject(1, config.getName(), Types.VARCHAR);
                        preStmt.setObject(2, config.getType(), Types.VARCHAR);
                        preStmt.setObject(3, config.getData(), Types.VARCHAR);
                        preStmt.setObject(4, config.getDirectoryId(), Types.INTEGER);
                        if (!newConfig) {
                            preStmt.setObject(5, config.getId(), Types.INTEGER);
                        }
                        LOGGER.trace("SQL request={}", preStmt.toString());
                        int resultValues = preStmt.executeUpdate();
                        LOGGER.trace("SQL request nb result = {}", resultValues);
                        if (newConfig) {
                            config.setId(getGeneratedKey(preStmt));
                        }
                        updateDirectoryTimestamp(conn, config.getDirectory());
                        conn.commit();
                        if (newConfig) {
                            stmt.clearBatch();
                        }
                    } catch (SQLException e) {
                        CommonsJDBC.catchException(e);
                    } catch (Exception e) {
                        String errorMessage = "Cannot save configuration " + config.getName() + " " + e.getMessage();
                        LOGGER.error(errorMessage);
                        LOGGER.debug("Stack trace", e);
                    } finally {
                        doCloseConn();
                    }
                }
            }

        }
        if (exception != null) {
            throw exception;
        }
    }

    /**
     * Create a new directory. The parent id must be specified.
     * 
     * Note : If the new directory contains config or subdirectories, they will
     * not be saved.
     * 
     * @param directory
     * @return
     * @throws DirectoryOutOfSyncException
     */
    public Integer createEmptyDirectory(IDirectory directory) throws DirectoryOutOfSyncException {

        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO directory (name, positionInDirectory, directory)");
        query.append("VALUES (?,?,?)");

        Integer parentId = null;
        if (directory.getDirectory() != null) {
            parentId = directory.getDirectory().getId();
        }

        Integer directoryUid = null;

        startOperation();

        if (conn != null) {
            try {
                preStmt = conn.prepareStatement(query.toString(), Statement.RETURN_GENERATED_KEYS);
                // Name
                preStmt.setObject(1, directory.getName(), Types.VARCHAR);
                // Position
                preStmt.setObject(2, directory.getPositionInDirectory(), Types.INTEGER);
                // Parent
                preStmt.setObject(3, parentId, Types.INTEGER);

                preStmt.executeUpdate();

                directoryUid = getGeneratedKey(preStmt);
                directory.setId(directoryUid);

                conn.commit();
            } catch (SQLException e) {
                CommonsJDBC.catchException(e);
                return null;
            } finally {
                doCloseConn();
            }
        }
        return directoryUid;
    }

    /**
     * Start the connection JDBC
     * 
     */
    private void startOperation() {
        conn = CommonsJDBC.getConnection();
        try {
            createStatement(conn);
        }
        // A timout is thrown if the connection is openend since a long time
        catch (CommunicationsException e) {
            CommonsJDBC.closeConnection();
            // Force the reconnection
            conn = CommonsJDBC.getConnection();
            try {
                createStatement(conn);
            } catch (SQLException e1) {
                CommonsJDBC.catchException(e1);
            }
        } catch (SQLException e) {
            CommonsJDBC.catchException(e);
        }
    }

    private void createStatement(Connection conn) throws SQLException {
        if (conn != null) {
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            conn.setAutoCommit(false);

            if (stmt != null) {
                stmt = null;
            }
            stmt = conn.createStatement();
        }
    }

    public void updateDirectory(IDirectory _d) throws DirectoryOutOfSyncException {
        try {
            startOperation();
            String query = " UPDATE directory SET name=?, directory=? ";
            query += " WHERE directoryid=? ";
            if (conn != null) {
                PreparedStatement preStmt = conn.prepareStatement(query.toString(), Statement.NO_GENERATED_KEYS);
                preStmt.setObject(1, _d.getName(), Types.VARCHAR);
                Integer parentDirectoryId = _d.getDirectory() != null ? _d.getDirectory().getId() : null;
                preStmt.setObject(2, parentDirectoryId, Types.INTEGER);
                preStmt.setObject(3, _d.getId(), Types.INTEGER);
                LOGGER.trace("SQL request ={}", preStmt.toString());
                int r = preStmt.executeUpdate();
                if (r == 0) {
                    throw new DirectoryOutOfSyncException();
                }
                conn.commit();
            }
        } catch (Exception exc) {
            String errorMessage = "Cannot updateDirectory " + _d + " " + exc.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", exc);
            throw new DirectoryOutOfSyncException();
        }
    }

    private void updateDirectoryTimestamp(Connection _c, IDirectory _d) throws SQLException {

        String query = " UPDATE directory SET timestamp=CURRENT_TIMESTAMP ";
        query += " WHERE directoryid=? AND timestamp=? ";

        PreparedStatement preStmt = _c.prepareStatement(query.toString(), Statement.NO_GENERATED_KEYS);

        preStmt.setObject(1, _d.getId(), Types.INTEGER);
        preStmt.setObject(2, _d.getTimestamp(), Types.TIMESTAMP);
        preStmt.executeUpdate();

        query = " SELECT timestamp FROM directory WHERE directoryid=? ";

        preStmt = _c.prepareStatement(query);
        preStmt.setObject(1, _d.getId(), Types.INTEGER);
        ResultSet rs = preStmt.executeQuery();
        if (rs.next()) {
            Timestamp t = rs.getTimestamp("timestamp");
            _d.setTimestamp(t);
        }
    }

    private boolean rollback() {
        if (conn != null) {
            try {
                conn.rollback();
                return true;
            } catch (SQLException sqle) {
                CommonsJDBC.catchException(sqle);
            }
        }
        return false;
    }

}

package fr.soleil.salsa.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.w3c.dom.Document;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.dao.IConfigDao;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeIntegrated;
import fr.soleil.salsa.entity.impl.DirectoryImpl;
import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.persistence.converter.ConfigAsXmlHelper;
import fr.soleil.salsa.persistence.model.Configuration;

public class ConfigDAO implements IConfigDao {

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigDAO.class);

    @Override
    public void deleteConfig(IConfig<?> config) throws PersistenceException {
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        dao.deleteConfig(config);
    }

    @Override
    public void deleteDirectory(IDirectory directory) throws PersistenceException {
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        dao.deleteDirectoryRecursively(directory);
    }

    public IDirectory getDirectoryById(Integer directoryId) throws PersistenceException {
        return getDirectoryById(directoryId, false);
    }

    private IDirectory getDirectoryById(Integer directoryId, boolean recursive) throws PersistenceException {
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        return dao.getDirectoryById(directoryId, recursive);
    }

    @Override
    public IConfig<?> getConfigById(Integer id) throws ScanNotFoundException {
        IConfig<?> result = null;

        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        Configuration c = dao.getConfigById(id);

        try {
            if (c != null) {
                result = (IConfig<?>) ConfigAsXmlHelper.fromXml(c.getData());

                result.setLoaded(true);
                result.setId(id);
                result.setTimestamp(c.getTimestamp());
                result.setDirectory(c.getDirectory());

                List<? extends IRange> rangeList = result.getDimensionX().getRangeList();
                if (rangeList != null) {
                    IRange range = null;
                    for (int i = 0; i < rangeList.size(); i++) {
                        range = rangeList.get(i);
                        if (range instanceof IRangeIntegrated) {
                            IRangeIntegrated iRangeIntegrated = (IRangeIntegrated) range;
                            if (iRangeIntegrated.getIntegrationTime() != null) {
                                if (iRangeIntegrated.getIntegrationTime().length != (iRangeIntegrated.getStepsNumber()
                                        + 1)) {
                                    double integrationTime = iRangeIntegrated.getIntegrationTime()[0];
                                    double[] integrationTimeArray = new double[Integer
                                            .valueOf(iRangeIntegrated.getStepsNumber()) + 1];
                                    Arrays.fill(integrationTimeArray, integrationTime);
                                    iRangeIntegrated.setIntegrationTime(integrationTimeArray);
                                }
                            }
                        }
                    }
                }

                if (c.getType().equals("Config1DImpl")) {
                    result.setType(IConfig.ScanType.SCAN_1D);
                } else if (c.getType().equals("Config2DImpl")) {
                    result.setType(IConfig.ScanType.SCAN_2D);
                } else if (c.getType().equals("ConfigHCS")) {
                    result.setType(IConfig.ScanType.SCAN_HCS);
                } else if (c.getType().equals("ConfigK")) {
                    result.setType(IConfig.ScanType.SCAN_K);
                } else if (c.getType().equals("ConfigEnergy")) {
                    result.setType(IConfig.ScanType.SCAN_ENERGY);
                }

                c.clean();
            }
            if (result == null) {
                StringBuilder builder = new StringBuilder("Failed to find configuration matching id = ");
                builder.append(id);
                throw new ScanNotFoundException(builder.toString());
            } else {
                return result;
            }
        } catch (ConversionException e) {
            throw new ScanNotFoundException(e.getMessage(), e);
        }

    }

    private Timestamp getConfigTimestamp(int configId) {
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        return dao.getConfigTimestampById(configId);
    }

    @Override
    public IDirectory getRootDirectory() {
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        IDirectory root = dao.getRootDirectory();
        return root;
    }

    @Override
    public IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException, ScanNotFoundException {
        IConfig<?> result = null;
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();
        Document d;
        try {
            d = ConfigAsXmlHelper.toXml(config);
        } catch (Exception e) {
            LOGGER.error("Cannot convert {}  to Xml format {}", config, e.getMessage());
            LOGGER.debug("Stack trace", e);
            throw new PersistenceException(e.getMessage(), e);
        }

        Configuration c = new Configuration();

        c.setId(config.getId());
        c.setName(config.getName());
        c.setType(config.getClass().getSimpleName());
        c.setDirectory(config.getDirectory());
        try {
            c.setData(ConfigAsXmlHelper.xmlToString(d));
        } catch (Exception e) {
            LOGGER.error("Cannot convert {}  to String format  {}", config, e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
        c.setDirectoryId(config.getDirectory().getId());
        c.setTimestamp(config.getTimestamp());

        dao.saveConfig(c, config.isForcedSaving());

        if (c.getId() != null) {
            result = getConfigById(c.getId());
            // The timestamp is not defined by the getConfigById,
            // so we need to set his value explicitly with
            // the one provided by the save operation.
            if ((result.getDirectory() != null) && (c.getDirectory() != null)) {
                result.getDirectory().setTimestamp(c.getDirectory().getTimestamp());
            }
            result.setModified(false);
        }
        c.clean();
        return result;
    }

    @Override
    public IDirectory saveDirectory(IDirectory directory) throws PersistenceException {
        ConfigDAOJdbcImpl dao = new ConfigDAOJdbcImpl();

        Integer id = directory.getId();
        if (id == null) {
            // Creation
            id = dao.createEmptyDirectory(directory);
        } else {
            // Update
            dao.updateDirectory(directory);
        }

        // We get the updated directory.
        IDirectory result = dao.getDirectoryById(id, false);
        if ((result.getDirectory() != null) && (result.getDirectory().getId() != null)) {
            Integer parentId = result.getDirectory().getId();
            IDirectory parent = getDirectoryById(parentId);
            result.setDirectory(parent);
        }

        return result;
    }

    private IConfig<?> findConfig(IDirectory parent, String configName) {
        IConfig<?> result = null;

        if (parent != null) {
            List<IConfig<?>> configs = parent.getConfigList();

            for (int i = 0; (i < configs.size()) && (result == null); i++) {
                IConfig<?> c = configs.get(i);
                if (c.getName().equals(configName)) {
                    result = c;
                }
            }
        }
        return result;
    }

    private IDirectory findSubDirectory(IDirectory parent, String childName) {
        IDirectory result = null;

        if (parent != null) {
            List<IDirectory> subDirs = parent.getSubDirectoriesList();
            if (subDirs != null) {
                for (int i = 0; (i < subDirs.size()) && (result == null); i++) {
                    IDirectory d = subDirs.get(i);
                    if (d.getName().equals(childName)) {
                        result = d;
                    }
                }
            }
        }

        return result;
    }

    private IDirectory findDirectory(IDirectory root, List<String> path) {

        DirectoryImpl dummy = new DirectoryImpl();
        List<IDirectory> dummySubs = new ArrayList<>();
        dummySubs.add(root);
        dummy.setSubDirectoriesList(dummySubs);

        IDirectory result = dummy;
        while (path.size() != 0) {
            String name = path.get(0);
            IDirectory d = findSubDirectory(result, name);
            if (d != null) {
                result = d;
                path.remove(0);
            } else {
                result = null;
                break;
            }
        }
        return result;
    }

    private IConfig<?> findConfig(IDirectory root, List<String> path, String configName) throws ScanNotFoundException {

        IConfig<?> c = null;
        IDirectory parent = findDirectory(root, path);
        if (parent != null) {
            c = findConfig(parent, configName);
        }
        if (c == null) {
            StringBuilder builder = new StringBuilder("Failed to find configuration '");
            if (root == null) {
                builder.append("null");
            } else {
                builder.append(root.getName());
            }
            for (String name : path) {
                builder.append("/").append(name);
            }
            builder.append("/").append(configName).append("'");

            LOGGER.error("Cannot find config {}", path);
            throw new ScanNotFoundException(builder.toString());
        } else {
            return c;
        }
    }

    @Override
    public IConfig<?> getConfigByPath(String fullPath) throws ScanNotFoundException {
        return getConfigByPath(fullPath, null);
    }

    @Override
    public IConfig<?> getConfigByPath(String fullPath, IConfig<?> lastLoadedConfig) throws ScanNotFoundException {
        IDirectory root = getRootDirectory();
        if ((fullPath != null) && (fullPath.indexOf(root.getFullPath()) < 0)) {
            fullPath = root.getFullPath() + "/" + fullPath;
        }
        String[] path = fullPath.split("/");
        List<String> pathList = new ArrayList<>();
        pathList.addAll(Arrays.asList(path));

        String configName = pathList.remove(pathList.size() - 1);
        IConfig<?> c = findConfig(root, pathList, configName);
        Integer configId = -1;
        if (c != null) {
            boolean shouldRecoverConfig;
            configId = c.getId();
            if ((lastLoadedConfig == null) || !ObjectUtils.sameObject(fullPath, lastLoadedConfig.getFullPath())
                    || !ObjectUtils.sameObject(configId, lastLoadedConfig.getId())) {
                shouldRecoverConfig = true;
            } else {
                Timestamp timestamp = getConfigTimestamp(configId);
                if ((timestamp == null) || (timestamp.getTime() > lastLoadedConfig.getLastModificationDate())) {
                    shouldRecoverConfig = true;
                } else {
                    shouldRecoverConfig = false;
                }
            }
            if (shouldRecoverConfig) {
                c = getConfigById(configId);
                IDirectory directory = c.getDirectory();
                if (directory != null) {
                    try {
                        directory = getDirectoryById(directory.getId(), true);
                    } catch (Exception e) {
                        directory = c.getDirectory();
                    }
                }
                c.setDirectory(directory);
            } else {
                c = lastLoadedConfig;
            }
        }
        return c;
    }

    @Override
    public IConfig<?> loadConfigByPath(String path) throws ScanNotFoundException {
        return getConfigByPath(path);
    }

    @Override
    public IConfig<?> loadConfigById(Integer id) throws PersistenceException, ScanNotFoundException {
        return getConfigById(id);
    }

}

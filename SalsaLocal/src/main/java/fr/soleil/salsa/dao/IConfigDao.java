package fr.soleil.salsa.dao;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.OutOfSyncException;
import fr.soleil.salsa.exception.persistence.PersistenceException;

/**
 * The dao interface that saves and load the configurations and directories .
 * 
 * @author Boualem ABDELLI
 * 
 */

public interface IConfigDao {

    /**
     * Provided the arborescent structure of scan configuration and directories.
     * 
     * Do not provide with fully usabled configuration (just id and scan type).
     * 
     * @return the root of the arborescent structure
     * 
     */
    public IDirectory getRootDirectory();

    /**
     * Gets the config by id
     * 
     * @param id
     * @return IConfig
     */
    public IConfig<?> getConfigById(Integer id) throws ScanNotFoundException;

    /**
     * Gets the config by the full path.
     * 
     * @param fullPath
     * @return
     * @throws ScanNotFoundException
     */
    public IConfig<?> getConfigByPath(String fullPath) throws ScanNotFoundException;

    /**
     * Gets the config by full path, if something changed since last loading
     * 
     * @param fullPath The config path
     * @param lastLoadedConfig The last config loaded with the same path
     * @return An {@link IConfig}. If no change occurred since last loading, it returns
     *         <code>lastLoadedConfig</code>. Otherwise, it returns the config loaded from database.
     * @throws ScanNotFoundException If no config corresponds to the given path.
     */
    public IConfig<?> getConfigByPath(String fullPath, IConfig<?> lastLoadedConfig)
            throws ScanNotFoundException;

    /**
     * Saves configuration
     * 
     * Provide the configuration id and the associated timestamp.
     * 
     * 
     * On save operation, the timestamp of the parent directory is updated. This new timestamp is
     * provided in the result.
     * 
     * 
     * 
     * @param config the configuration to save.
     * 
     */
    public IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException,
            ScanNotFoundException;

    /**
     * Save or update a directory. If the id is specified, then update is done, else create. (i.e if
     * you want copy a directory, you can set the id to null).
     * 
     * If the id is not specified, the parent id must be specified.
     * 
     * ParentDirectory can not be updated, this kind of modification require creation of a new
     * directory.
     * 
     * Note : If the new directory contains config or subdirectories, they will not be saved.
     */
    public IDirectory saveDirectory(IDirectory directory) throws PersistenceException;

    /**
     * Delete a configuration.
     * 
     * Require the id and the timestamp.
     * 
     * @param the config to delete.
     * @throws OutOfSyncException when the config has been modified or deleted.
     */
    public void deleteConfig(IConfig<?> config) throws PersistenceException;

    /**
     * Delete Directory
     * 
     * @param the directory to delete.
     * @throws PersistenceException
     */
    public void deleteDirectory(IDirectory directory) throws PersistenceException;

    /**
     * Load Config by path
     * 
     * @param the path of config to load.
     * @throws PersistenceException
     */
    public IConfig<?> loadConfigByPath(String path) throws PersistenceException,
            ScanNotFoundException;

    /**
     * Load Config by id
     * 
     * @param the id of config to load.
     * @throws PersistenceException
     */
    public IConfig<?> loadConfigById(Integer id) throws PersistenceException, ScanNotFoundException;

}

package fr.soleil.salsa.dao.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.slf4j.Logger;

import fr.soleil.lib.project.SystemUtils;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class CommonsJDBC {

    private static Logger LOGGER = LoggingUtil.getLogger(CommonsJDBC.class);

    private static Connection connection = null;
    public static Statement statement;
    private static Map<String, String> jdbcMap = new HashMap<>();
    private static ResourceBundle jdbcFile = null;

    private static final String CONFIG_JDBC_NAME = "jdbc";
    private static final String SALSA_SERVER_URL = "SALSA_SERVER_URL";
    private static final String SALSA_LOGIN = "SALSA_LOGIN";
    private static final String SALSA_PASSWORD = "SALSA_PASSWORD";

    static {
        checkParameters();
        doConnection();
    }

    public static void closeConnection() {
        doClose();
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                LOGGER.error("Cannot close Connection {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
    }

    public static Connection getConnection() {
        try {
            if ((connection == null) || connection.isClosed()) {
                doConnection();
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot getConnection {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
        return connection;
    }

    /***
     * Load parameters from the file properties
     */
    private static void loadProperties() {
        if (jdbcFile == null) {
            try {
                jdbcFile = ResourceBundle.getBundle(CONFIG_JDBC_NAME);
                Enumeration<String> keys = jdbcFile.getKeys();
                String key = null;
                while (keys.hasMoreElements()) {
                    key = keys.nextElement();
                    jdbcMap.put(key, jdbcFile.getString(key));
                }

            } catch (Exception ex) {
                LOGGER.error("Cannot loadProperties {}", ex.getMessage());
                LOGGER.debug("Stack trace", ex);
            }
        }
    }

    /***
     * Retrieve values of parameters of connection from the properties file and
     * assigned this values to parameters . .
     * 
     */
    private static void checkParameters() {

        loadProperties();

        if (Commons.jdbcDriver == null) {
            Commons.jdbcDriver = jdbcMap.get("driver_class");
        }

        if (Commons.urlConnection == null) {
            // jdbcMap.remove("url");
            // jdbcMap.put("url", Commons.urlConnection);
            Commons.urlConnection = jdbcMap.get("url");
        }
        if (Commons.login == null) {
            // jdbcMap.remove("username");
            // jdbcMap.put("username", Commons.login);
            Commons.login = jdbcMap.get("username");
        }
        if (Commons.password == null) {
            // jdbcMap.remove("password");
            // jdbcMap.put("password", Commons.password);
            Commons.password = jdbcMap.get("password");
        }

        /* We use environement context */

        String urlConnection = getProperty(SALSA_SERVER_URL);
        if (SalsaUtils.isDefined(urlConnection)) {
            Commons.urlConnection = urlConnection;
        }
        String login = getProperty(SALSA_LOGIN);
        if (SalsaUtils.isDefined(login)) {
            Commons.login = login;
        }
        String password = getProperty(SALSA_PASSWORD);
        if (SalsaUtils.isDefined(password)) {
            Commons.password = password;
        }
    }

    private static String getProperty(String key) {
        return SystemUtils.getSystemProperty(key);
    }

    /***
     * Connection of the database
     */
    private static void doConnection() {

        try {
            // Load the jdbc-odbc bridge driver
            Class.forName(Commons.jdbcDriver);

            // Attempt to connect to a driver. Each one // of the registered
            // drivers will be loaded until one is found that can process this
            // URL

            connection = DriverManager.getConnection(Commons.urlConnection, Commons.login, Commons.password);

            // Check for, and display and warnings generated // by the connect.

            checkForWarning(connection.getWarnings());

            // Get the DatabaseMetaData object and display
            // some information about the connection

            DatabaseMetaData dma = connection.getMetaData();
            LOGGER.info("Connected to {}", dma.getURL());
            LOGGER.info("Driver       {}", dma.getDriverName());
            LOGGER.info("Version      {}", dma.getDriverVersion());

            statement = connection.createStatement();

        } catch (Exception ex) {
            LOGGER.error("Cannot establish a connection : {}", ex.getMessage());
            LOGGER.debug("Stack trace", ex);
        }

    }

    /***
     * A SQLException was generated. Catch it and display the error information.
     * Note that there could be multiple error objects chained together
     * 
     * @param ex
     *            SQLException
     */
    public static void catchException(SQLException ex) {
        LOGGER.error("Exception catched : {}", ex.getMessage());
        LOGGER.debug("Stack trace", ex);
    }

    /***
     * Checks for and displays warnings. Returns true if a warning existed
     * 
     * @param SQLWarning
     * @return Boolean
     */
    private static boolean checkForWarning(SQLWarning warn) throws SQLException {

        boolean rc = false;

        // If a SQLWarning object was given, display the
        // warning messages. Note that there could be
        // multiple warnings chained together

        if (warn != null) {
            rc = true;
            while (warn != null) {
                StringBuilder warningMessage = new StringBuilder();
                warningMessage.append("\n *** Warning ***\n");
                warningMessage.append("\nSQLState: " + warn.getSQLState());
                warningMessage.append("\nMessage:  " + warn.getMessage());
                warningMessage.append("\nVendor:   " + warn.getErrorCode());
                LOGGER.error(warningMessage.toString());
                warn = warn.getNextWarning();
            }
        }
        return rc;
    }

    /***
     * After treatment, close the statement if it's not NULL
     * 
     */
    public static void doClose() {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException ex) {
            catchException(ex);
        }
    }

}

package fr.soleil.salsa.service.local;

import fr.soleil.salsa.dao.impl.ConfigDAO;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;
import fr.soleil.salsa.service.IPersistenceService;

/**
 * Local implementation of the class relative to the persistence of scan configurations and
 * directories.
 * 
 * @author Alike
 * 
 */
public class LocalPersistenceService implements IPersistenceService {

    @Override
    public void deleteConfig(IConfig<?> config) throws PersistenceException {
        ConfigDAO dao = new ConfigDAO();
        dao.deleteConfig(config);
    }

    @Override
    public void deleteDirectory(IDirectory directory) throws PersistenceException {
        ConfigDAO dao = new ConfigDAO();
        dao.deleteDirectory(directory);
    }

    @Override
    public IConfig<?> getConfigById(Integer id) throws ScanNotFoundException {
        ConfigDAO dao = new ConfigDAO();
        return dao.getConfigById(id);
    }

    @Override
    public IDirectory getRootDirectory() {
        ConfigDAO dao = new ConfigDAO();
        return dao.getRootDirectory();
    }

    @Override
    public IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException,
            ScanNotFoundException {
        ConfigDAO dao = new ConfigDAO();
        return dao.saveConfig(config);
    }

    @Override
    public IDirectory saveDirectory(IDirectory directory) throws PersistenceException {
        ConfigDAO dao = new ConfigDAO();
        return dao.saveDirectory(directory);
    }

}

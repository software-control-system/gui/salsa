package fr.soleil.salsa.service.local;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import fr.soleil.salsa.api.item.HistoricRecord;
import fr.soleil.salsa.business.ScanConnector;
import fr.soleil.salsa.dao.impl.ConfigDAO;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.ScanState;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaScanConfigurationException;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.service.IScanService;

/**
 * The service that allows control over scans. The local version of the scan service.
 * 
 * @author Administrateur
 * 
 */
public class LocalScanService implements IScanService {

    /**
     * Connects to the scan server.
     */
    private final ScanConnector scanConnector;

    /**
     * The constructor.
     */
    public LocalScanService() {
        scanConnector = new ScanConnector();
    }

    /**
     * Convert xml data into a String. Note : It's a "utility" method, that should be defined in
     * another class in order to be possibly used by other methods.
     * 
     * @param _d
     * @return
     * @throws TransformerException
     */
    public static String xmlToString(Document _d) throws TransformerException {
        DOMSource domSource = new DOMSource(_d);
        StringWriter writer = new StringWriter();
        StreamResult sr = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, sr);

        String stringResult = writer.toString();
        return stringResult;
    }

    public static boolean deleteLine(String fileName, int lineNumber) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            StringBuffer sb = new StringBuffer();
            String line;
            int nbLinesRead = 0;
            while ((line = reader.readLine()) != null) {
                if (nbLinesRead != lineNumber) {
                    sb.append(line + "\n");
                }
                nbLinesRead++;
            }
            reader.close();
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            out.write(sb.toString());
            out.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Loads a new scan.
     * 
     * @param config the configuration that defines the scan parameters.
     * @param context the context of the scan.
     * @throws SalsaScanConfigurationException
     * @throws ScanDeviceException
     * @see IScanService#startScan(IConfig)
     */
    @Override
    public void loadScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        scanConnector.loadScan(config, context);
    }

    /**
     * Starts a new scan.
     * 
     * @param config the configuration that defines the scan parameters.
     * @param context the context of the scan.
     * @throws SalsaScanConfigurationException
     * @throws ScanDeviceException
     * @see IScanService#startScan(IConfig)
     */
    @Override
    public void startScan(IConfig<?> config, IContext context)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        scanConnector.startScan(config, context);
    }

    /**
     * Stops the scan.
     * 
     * @throws SalsaDeviceException
     */
    @Override
    public void stopScan(IContext context) throws SalsaDeviceException {
        scanConnector.stopScan(context);
    }

    /**
     * Pauses the scan.
     * 
     * @throws SalsaDeviceException
     */
    @Override
    public void pauseScan(IContext context) throws SalsaDeviceException {
        scanConnector.pauseScan(context);
    }

    /**
     * Resumes the scan.
     * 
     * @throws SalsaDeviceException
     */
    @Override
    public void resumeScan(IContext context) throws SalsaDeviceException {
        scanConnector.resumeScan(context);
    }

    /**
     * Returns the result of the ongoing scan, or of the last finished scan.
     * 
     * @return
     * @throws SalsaScanConfigurationException
     * @throws ScanDeviceException
     */
    @Override
    public IScanResult readScanResult(String scanServerName)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        return readScanResult(scanServerName, false);
    }

    @Override
    public IScanResult readScanResult(String scanServerName, boolean withTrajectories)
            throws SalsaDeviceException, SalsaScanConfigurationException {
        return scanConnector.readScanResult(scanServerName, withTrajectories);
    }

    /**
     * Checks if a scan result can be read.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     * @throws SalsaDeviceException
     */
    @Override
    public boolean isScanResultReady(String scanServerName) throws SalsaDeviceException {
        return scanConnector.isScanResultReady(scanServerName);
    }

    /**
     * Returns the state of the scan.
     * 
     * @param scanServerName
     * @return
     * @throws SalsaDeviceException
     */
    @Override
    public ScanState getScanState(String scanServerName) throws SalsaDeviceException {
        return scanConnector.getScanState(scanServerName);
    }

    /**
     * Reads the historic.
     * 
     * @param scanServerName the name of the scan server to use.
     * @return
     */
    @Override
    public List<HistoricRecord> getHistoric(String scanServerName) throws SalsaDeviceException {
        return scanConnector.getHistoric(scanServerName);
    }

    /**
     * Clears the historic on the scan server.
     * 
     * @param scanServerName
     * @throws SalsaDeviceException
     */
    @Override
    public void clearHistoric(String scanServerName) throws SalsaDeviceException {
        scanConnector.clearHistoric(scanServerName);
    }

    /**
     * Performs a scan function.
     * 
     * @param scanServerName the name of the scan server to use.
     * @param behaviour the function to be done.
     * @param sensor function parameter : sensor. Null if the function has no sensor parameter.
     * @param actuator function parameter : actuator. Null id the function has no actuator
     *            parameter.
     * @throws SalsaDeviceException
     */
    @Override
    public void doScanFunction(String scanServerName, Behaviour behaviour, ISensor sensor, IActuator actuator)
            throws SalsaDeviceException {
        scanConnector.doScanFunction(scanServerName, behaviour, sensor, actuator);
    }

    @Override
    public ISuggestions getDevicesSuggestions(String scanServerName) throws SalsaDeviceException {
        return scanConnector.getDevicesSuggestions(scanServerName);
    }

    @Override
    public IConfig<?> getConfig(String path) throws ScanNotFoundException {
        ConfigDAO dao = new ConfigDAO();
        return dao.getConfigByPath(path);
    }

    /**
     * 
     * @param path
     * @param scanServerName
     * @throws SalsaDeviceException
     * @throws ScanNotFoundException
     * @throws SalsaScanConfigurationException
     */
    @Override
    public IConfig<?> startScan(String path, IContext context)
            throws SalsaDeviceException, ScanNotFoundException, SalsaScanConfigurationException {
        IConfig<?> c = getConfig(path);
        startScan(c, context);
        return c;
    }

    @Override
    public void setDataRecorderPartialMode(String scanServerName, boolean mode) throws SalsaDeviceException {
        scanConnector.setDataRecorderPartialMode(scanServerName, mode);
    }

    @Override
    public void setDataRecorderEnable(boolean enable) throws SalsaDeviceException {
        scanConnector.setDataRecorderEnable(enable);
    }

    @Override
    public boolean isDataRecorderEnable() throws SalsaDeviceException {
        return scanConnector.isDataRecorderEnable();
    }

}

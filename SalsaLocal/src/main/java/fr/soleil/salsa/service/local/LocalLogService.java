package fr.soleil.salsa.service.local;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.salsa.business.DeviceConnector;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.HistoricLogLine;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.entity.impl.scank.TrajectoryKImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.exception.SalsaLoggingException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.ILogService;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * The service that allows log over scans. The local version of the log service.
 * 
 * @author Alike
 * 
 */
public class LocalLogService implements ILogService {

    private static final String ACTUATOR = "actuator";
    private static final String ID = "id";
    private static final String E0 = "e0";
    private static final String E1 = "e1";
    private static final String E2 = "e2";
    private static final String EDELTAEDGE = "edeltaedge";
    private static final String EDELTAPRE_EDGE = "edeltapreEdge";
    private static final String EMIN = "emin";
    private static final String KDELTA = "kdelta";
    private static final String KMAX = "kmax";
    private static final String KMIN = "kmin";
    private static final String M = "m";
    private static final String N = "n";
    private static final String DEADTIME = "deadtime";
    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String DELTA = "delta";

    public static final Logger LOGGER = LoggingUtil.getLogger(LocalLogService.class);

    public static int DEFAULT_MAX_LINE_NUMBER = 25;

    @Override
    public void logScan(IConfig<?> config, IContext context, String action) throws SalsaLoggingException {
        LOGGER.trace("logScan({},{})", config, action);
        try {
            if (SalsaUtils.isDefined(context.getUserLogFile())) {
                String logFile = context.getUserLogFile().trim();
                // Read
                Date date = new Date();
                String logHour = null;
                DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
                DocumentBuilder constructeur = fabrique.newDocumentBuilder();
                File xmlFile = new File(logFile);
                boolean fileExist = xmlFile.exists();
                if (!fileExist) {
                    fileExist = xmlFile.createNewFile();
                }
                if (fileExist) {
                    Document document = constructeur.parse(xmlFile);
                    Element root = document.getDocumentElement();
                    Element element = document.createElement("event");
                    String logScanId = "0";
                    if (config != null) {
                        logScanId = Integer.toString(config.getId());
                    }
                    String logScan = null;
                    String scanserver = context.getScanServerName();
                    if ((config == null) && TangoDeviceHelper.isDeviceRunning(scanserver)) {
                        DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(scanserver);
                        if (deviceProxy == null) {
                            LOGGER.error("Could not get DeviceProxy for " + scanserver);
                        } else {
                            try {
                                DeviceAttribute attribute = deviceProxy.read_attribute(CurrentScanDataModel.RUN_NAME);
                                logScan = attribute.extractString();
                            } catch (DevFailed e) {
                                String errorMessage = "Cannot read " + scanserver + "/" + CurrentScanDataModel.RUN_NAME
                                        + " " + DevFailedUtils.toString(e);
                                LOGGER.warn(errorMessage);
                                LOGGER.debug("Stack trace", e);
                            }
                        }
                    } else if (config != null) {
                        logScan = config.getName();
                    }

                    // Get the real start date if it is a start action
                    if ((action != null) && action.equalsIgnoreCase(ILogService.START_ACTION)
                            && TangoDeviceHelper.isDeviceRunning(scanserver)) {

                        // For the moment runStartDate format = yyyy-MMM-dd HH:mm:ss
                        DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(scanserver);
                        if (deviceProxy == null) {
                            LOGGER.error("Could not get DeviceProxy for " + scanserver);
                        } else {
                            try {
                                DeviceAttribute attribute = deviceProxy
                                        .read_attribute(CurrentScanDataModel.RUN_START_DATE);
                                String runStartDate = attribute.extractString();
                                LOGGER.trace("Read {}/{}={}", scanserver, CurrentScanDataModel.RUN_START_DATE,
                                        runStartDate);
                                if (SalsaUtils.isDefined(runStartDate)) {
                                    int index = runStartDate.indexOf(" ");
                                    if (index > -1) {
                                        logHour = runStartDate.substring(index + 1);
                                        String stringDate = runStartDate.substring(0, index);
                                        SimpleDateFormat scanDateFormat = new SimpleDateFormat("yyyy-MMM-dd",
                                                Locale.US);
                                        Date adate = scanDateFormat.parse(stringDate);
                                        date = adate;
                                    }
                                }
                            } catch (DevFailed devFailed) {
                                String errorMessage = "Cannot read " + scanserver + "/"
                                        + CurrentScanDataModel.RUN_START_DATE + " "
                                        + DevFailedUtils.toString(devFailed);
                                LOGGER.warn(errorMessage);
                                LOGGER.debug("Stack trace", devFailed);
                            } catch (Exception e2) {
                                String errorMessage = "Cannot parse " + scanserver + "/"
                                        + CurrentScanDataModel.RUN_START_DATE + " " + e2.getMessage();
                                LOGGER.warn(errorMessage);
                                LOGGER.debug("Stack trace", e2);
                            }
                        }
                    }

                    DateFormat dateFormat = new SimpleDateFormat("MMddyy");
                    DateFormat hourFormat = new SimpleDateFormat("HH':'mm':'ss");

                    String logDate = dateFormat.format(date);
                    if (!SalsaUtils.isDefined(logHour)) {
                        logHour = hourFormat.format(date);
                    }

                    element.setAttribute("action", action);
                    element.setAttribute("scan", logScan);
                    element.setAttribute("scanId", logScanId);
                    element.setAttribute("date", logDate);
                    element.setAttribute("hour", logHour);

                    // Fichier Nexus =>
                    // targetDirectory + fileName + .nxs
                    // nxEntryName on considerera dans un premier temps qu il n y toujours qu un
                    // NXEntry
                    String nexusFileName = "";
                    String nxEntryName = "";

                    // In of Start action get the associated nexus file
                    if ((action != null) && action.equalsIgnoreCase(ILogService.START_ACTION)) {
                        nexusFileName = CurrentScanDataModel.getNexusFileName(scanserver);
                        nxEntryName = CurrentScanDataModel.getNexusFileEntry(scanserver);
                        LOGGER.trace("Read {}/{}={}", scanserver, CurrentScanDataModel.NEXUS_FILE, nexusFileName);
                    }

                    element.setAttribute("nexusFile", nexusFileName);
                    element.setAttribute("nxEntryName", nxEntryName);
                    if ((action != null) && action.equalsIgnoreCase(ILogService.START_ACTION) && (config != null)) {
                        Element actuators = document.createElement("actuators");
                        ArrayList<ITrajectory> trajectories = new ArrayList<ITrajectory>();
                        if (config instanceof IConfig1D) {
                            IConfig1D config1d = (IConfig1D) config;
                            IDimension1D dimensionX = config1d.getDimensionX();
                            List<IActuator> listActuator = dimensionX.getActuatorsList();
                            List<IRange1D> range1d = dimensionX.getRangesXList();
                            for (int rangeIndex = 0; rangeIndex < range1d.size(); rangeIndex++) {
                                IRange1D range1D = range1d.get(rangeIndex);
                                List<ITrajectory> listTrajectory = range1D.getTrajectoriesList();
                                int trajectorySize = listTrajectory.size();
                                if (listActuator.size() <= trajectorySize) {
                                    for (int trajectoryIndex = 0; trajectoryIndex < trajectorySize; trajectoryIndex++) {
                                        if (trajectoryIndex < listActuator.size()) {
                                            IActuator actuator = listActuator.get(trajectoryIndex);
                                            if ((actuator != null) && actuator.isEnabled()) {
                                                ITrajectory trajectory = copyToTrajectoryImpl(actuator,
                                                        listTrajectory.get(trajectoryIndex));
                                                if (trajectory != null) {
                                                    trajectories.add(trajectory);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        } else if (config instanceof IConfig2D) {
                            IConfig2D config2d = (IConfig2D) config;
                            IDimension2DX dimension2DX = config2d.getDimensionX();
                            List<IActuator> listActuator2DX = dimension2DX.getActuatorsList();
                            List<IRange2DX> range2DX = dimension2DX.getRangesList();

                            for (int i = 0; i < range2DX.size(); i++) {
                                List<ITrajectory> listTrajectory = range2DX.get(i).getTrajectoriesList();
                                for (int j = 0; j < listTrajectory.size(); j++) {
                                    IActuator actuator = listActuator2DX.get(j);
                                    if ((actuator != null) && actuator.isEnabled()) {
                                        ITrajectory trajectory = copyToTrajectoryImpl(actuator, listTrajectory.get(j));
                                        if (trajectory != null) {
                                            trajectories.add(trajectory);
                                        }
                                    }
                                }
                            }

                            IDimension2DY dimension2DY = config2d.getDimensionY();
                            List<IActuator> listActuator2DY = dimension2DY.getActuatorsList();
                            List<IRange2DY> range2DY = dimension2DY.getRangesList();

                            for (int i = 0; i < range2DY.size(); i++) {
                                List<ITrajectory> listTrajectory = range2DY.get(i).getTrajectoriesList();
                                for (int j = 0; j < listTrajectory.size(); j++) {
                                    IActuator actuator = listActuator2DY.get(j);
                                    if ((actuator != null) && actuator.isEnabled()) {
                                        ITrajectory trajectory = copyToTrajectoryImpl(actuator, listTrajectory.get(j));
                                        if (trajectory != null) {
                                            trajectories.add(trajectory);
                                        }
                                    }
                                }
                            }

                        } else if (config instanceof IConfigHCS) {
                            IConfigHCS configHCS = (IConfigHCS) config;
                            IDimensionHCS dimensionHCS = configHCS.getDimensionX();
                            List<IActuator> listActuatorHCS = dimensionHCS.getActuatorsList();

                            List<IRangeHCS> rangeHCS = dimensionHCS.getRangesXList();
                            for (int i = 0; i < rangeHCS.size(); i++) {
                                List<ITrajectory> listTrajectory = rangeHCS.get(i).getTrajectoriesList();
                                for (int j = 0; j < listTrajectory.size(); j++) {
                                    IActuator actuator = listActuatorHCS.get(j);
                                    if ((actuator != null) && actuator.isEnabled()) {
                                        ITrajectory trajectory = copyToTrajectoryImpl(actuator, listTrajectory.get(j));
                                        if (trajectory != null) {
                                            trajectories.add(trajectory);
                                        }
                                    }
                                }
                            }
                        } else if (config instanceof IConfigK) {
                            IConfigK configK = (IConfigK) config;
                            IDimensionK dimensionK = configK.getDimensionX();
                            ITrajectoryK trajectoryK = dimensionK.getRangeX().getTrajectory();
                            List<IActuator> listTrajectory = dimensionK.getActuatorsList();
                            for (int i = 0; i < listTrajectory.size(); i++) {
                                IActuator actuatork = listTrajectory.get(i);
                                if ((actuatork != null) && actuatork.isEnabled()) {
                                    ITrajectory trajectory = copyToTrajectoryImpl(actuatork, trajectoryK);
                                    if (trajectory != null) {
                                        trajectories.add(trajectory);
                                    }
                                }
                            }

                        } else if (config instanceof IConfigEnergy) {
                            IConfigEnergy configEnergy = (IConfigEnergy) config;
                            IDimensionEnergy dimensionEnergy = configEnergy.getDimensionX();
                            List<IActuator> listActuatorEnergy = dimensionEnergy.getActuatorsList();

                            List<IRangeEnergy> rangeEnergy = dimensionEnergy.getRangesEnergyList();
                            for (int i = 0; i < rangeEnergy.size(); i++) {
                                List<ITrajectory> listTrajectories = rangeEnergy.get(i).getTrajectoriesList();
                                for (int j = 0; j < listTrajectories.size(); j++) {
                                    IActuator actuator = listActuatorEnergy.get(j);
                                    if ((actuator != null) && actuator.isEnabled()) {
                                        ITrajectory trajectory = copyToTrajectoryImpl(actuator,
                                                listTrajectories.get(j));
                                        if (trajectory != null) {
                                            trajectories.add(trajectory);
                                        }
                                    }
                                }
                            }
                        }

                        if (SalsaUtils.isFulfilled(trajectories)) {
                            for (int i = 0; i < trajectories.size(); i++) {
                                ITrajectory trajectory = trajectories.get(i);
                                Element actuator = getActuator(document, trajectory);
                                actuators.appendChild(actuator);
                            }
                            element.appendChild(actuators);
                        }
                    }

                    if (root.getNodeName().equals("historic")) {
                        root.appendChild(element);

                        NodeList nodeList = root.getElementsByTagName("event");
                        int nbrNodes = nodeList.getLength();
                        int maxLineNumber;
                        try {
                            maxLineNumber = context.getMaxLineNumber();
                        } catch (Exception e) {
                            maxLineNumber = DEFAULT_MAX_LINE_NUMBER;
                        }
                        int nbrLinesToDelete = nbrNodes - maxLineNumber;
                        if (nbrNodes > maxLineNumber) {
                            for (int index = 0; index < nbrLinesToDelete; index++) {
                                root.removeChild(nodeList.item(0));

                                if (root.getFirstChild().getNodeType() == Node.TEXT_NODE) {
                                    root.removeChild(root.getFirstChild());
                                }
                            }
                        }
                        String str;
                        str = xmlToString(document);
                        FileWriter fw;
                        fw = new FileWriter(xmlFile);
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(str);
                        bw.flush();
                        fw.close();
                    }
                }
            }
        } catch (Exception e) {
            String errorMessage = "Cannot log scan " + e.getMessage();
            LOGGER.warn(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new SalsaLoggingException(e.getMessage(), e);
        }
    }

    private ITrajectory copyToTrajectoryImpl(IActuator actuator, ITrajectory trajectory) {
        ITrajectory trajectoryImpl = null;
        if ((trajectory != null) && (actuator != null)) {
            if (trajectory instanceof ITrajectoryK) {
                ITrajectoryK trajectoryK = (ITrajectoryK) trajectory;
                TrajectoryKImpl trajectoryKImpl = new TrajectoryKImpl();
                trajectoryKImpl.setRelative(false);
                trajectoryKImpl.setE0(trajectoryK.getE0());
                trajectoryKImpl.setE1(trajectoryK.getE1());
                trajectoryKImpl.setE2(trajectoryK.getE2());
                trajectoryKImpl.setEDeltaEdge(trajectoryK.getEDeltaEdge());
                trajectoryKImpl.setEDeltaPreEdge(trajectoryK.getEDeltaPreEdge());
                trajectoryKImpl.setEMin(trajectoryK.getEMin());
                trajectoryKImpl.setKDelta(trajectoryK.getKDelta());
                trajectoryKImpl.setKMax(trajectoryK.getKMax());
                trajectoryKImpl.setKMin(trajectoryK.getKMin());
                trajectoryKImpl.setM(trajectoryK.getM());
                trajectoryKImpl.setN(trajectoryK.getN());
                trajectoryKImpl.setDeadTime(trajectoryK.getDeadTime());
                trajectoryImpl = trajectoryKImpl;
            } else {
                trajectoryImpl = new TrajectoryImpl();
                Double beginPosition = trajectory.getBeginPosition();
                Double endPosition = trajectory.getEndPosition();
                if (trajectory.getRelative()) {
                    try {
                        Double initialValue = DeviceConnector.getData(actuator);
                        beginPosition = beginPosition + initialValue;
                        endPosition = endPosition + initialValue;
                    } catch (SalsaDeviceException e) {
                    }
                }
                trajectoryImpl.setRelative(trajectory.getRelative());
                trajectoryImpl.setBeginPosition(beginPosition);
                trajectoryImpl.setEndPosition(endPosition);
                trajectoryImpl.setDelta(trajectory.getDelta());
            }
            if (trajectoryImpl != null) {
                trajectoryImpl.setName(actuator.getName());
            }
        }
        return trajectoryImpl;
    }

    private Element getActuator(final Document document, ITrajectory trajectory) {
        Element actuator = document.createElement(ACTUATOR);
        if (trajectory != null) {
            String id = trajectory.getName();
            actuator.setAttribute(ID, id);

            if (trajectory instanceof ITrajectoryK) {
                ITrajectoryK trajectoryK = (ITrajectoryK) trajectory;
                actuator.setAttribute(E0, String.valueOf(trajectoryK.getE0()));
                actuator.setAttribute(E1, String.valueOf(trajectoryK.getE1()));
                actuator.setAttribute(E2, String.valueOf(trajectoryK.getE2()));
                actuator.setAttribute(EDELTAEDGE, String.valueOf(trajectoryK.getEDeltaEdge()));
                actuator.setAttribute(EDELTAPRE_EDGE, String.valueOf(trajectoryK.getEDeltaPreEdge()));
                actuator.setAttribute(EMIN, String.valueOf(trajectoryK.getEMin()));
                actuator.setAttribute(KDELTA, String.valueOf(trajectoryK.getKDelta()));
                actuator.setAttribute(KMAX, String.valueOf(trajectoryK.getKMax()));
                actuator.setAttribute(KMIN, String.valueOf(trajectoryK.getKMin()));
                actuator.setAttribute(M, String.valueOf(trajectoryK.getM()));
                actuator.setAttribute(N, String.valueOf(trajectoryK.getN()));
                actuator.setAttribute(DEADTIME, String.valueOf(trajectoryK.getDeadTime()));
            } else {
                actuator.setAttribute(FROM, String.valueOf(trajectory.getBeginPosition()));
                actuator.setAttribute(TO, String.valueOf(trajectory.getEndPosition()));
                actuator.setAttribute(DELTA, String.valueOf(trajectory.getDelta()));
            }
        }
        return actuator;
    }

    @Override
    public void clearHistoricLog(IContext context) throws SalsaLoggingException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            File xmlFile = new File(context.getUserLogFile());
            Document document = db.parse(xmlFile);
            Element root = document.getDocumentElement();
            if (root.getNodeName().equals("historic")) {

                NodeList nodeList = root.getElementsByTagName("event");

                int nbrNodes = nodeList.getLength();
                if (nbrNodes != 0) {
                    for (int index = 0; index < nbrNodes; index++) {
                        root.removeChild(nodeList.item(0));
                        if (root.getFirstChild().getNodeType() == Node.TEXT_NODE) {
                            root.removeChild(root.getFirstChild());
                        }
                    }
                    String str;
                    str = xmlToString(document);
                    FileWriter fw = new FileWriter(xmlFile);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(str);
                    bw.flush();
                    fw.close();
                }
            }
        } catch (ParserConfigurationException e) {
            throw new SalsaLoggingException(e.getMessage(), e);
        } catch (SAXException e) {
            throw new SalsaLoggingException(e.getMessage(), e);
        } catch (TransformerException e) {
            throw new SalsaLoggingException(e.getMessage(), e);
        } catch (IOException e) {
            throw new SalsaLoggingException(e.getMessage(), e);
        }
    }

    @Override
    public ArrayList<HistoricLogLine> loadHistoricLog(IContext context) throws SalsaLoggingException {
        ArrayList<HistoricLogLine> historicLogArray;
        if (SalsaUtils.isDefined(context.getUserLogFile())) {
            historicLogArray = new ArrayList<>();
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document document = null;
                File xml = new File(context.getUserLogFile());
                if (!xml.exists()) {
                    document = createNewFile(xml, db);
                }
                try {
                    document = db.parse(xml);
                } catch (Exception e) {
                    // The files is corrupted rename as old file and create a new one
                    String content = readDoc(xml);
                    File newFile = new File(xml.getAbsolutePath() + System.currentTimeMillis() + "_copy");
                    newFile.createNewFile();
                    try {
                        FileWriter fw = new FileWriter(newFile.getAbsolutePath());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(content);
                        bw.flush();
                        fw.close();
                    } catch (Exception writeException) {
                        String errorMessage = "Cannot write in " + newFile.getAbsolutePath() + " "
                                + writeException.getMessage();
                        LOGGER.warn(errorMessage);
                        LOGGER.debug("Stack trace", writeException);
                    }
                    document = createNewFile(xml, db);
                }

                Element root = document.getDocumentElement();
                if (root.getNodeName().equals("historic")) {
                    NodeList nodeEventList = root.getElementsByTagName("event");
                    ArrayList<Element> events = new ArrayList<Element>();
                    if (nodeEventList.getLength() != 0) {
                        int maxLineNumber;
                        try {
                            maxLineNumber = context.getMaxLineNumber();
                        } catch (Exception e) {
                            maxLineNumber = DEFAULT_MAX_LINE_NUMBER;
                        }
                        if (nodeEventList.getLength() < maxLineNumber) {
                            for (int index = 0; index < nodeEventList.getLength(); index++) {
                                createHistoricLogLine(nodeEventList, index, events, historicLogArray);
                            }
                        } else {
                            for (int index = nodeEventList.getLength() - maxLineNumber; index < nodeEventList
                                    .getLength(); index++) {
                                createHistoricLogLine(nodeEventList, index, events, historicLogArray);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                String errorMessage = "Cannot load historic file " + context.getUserLogFile() + " " + e.getMessage();
                LOGGER.warn(errorMessage);
                LOGGER.debug("Stack trace", e);
                throw new SalsaLoggingException(errorMessage, e);
            }
        } else {
            historicLogArray = null;
        }
        return historicLogArray;
    }

    private String readDoc(File f) {
        String text = "";
        int read, N = 1024 * 1024;
        char[] buffer = new char[N];

        try (FileReader fr = new FileReader(f); BufferedReader br = new BufferedReader(fr);) {
            while (true) {
                read = br.read(buffer, 0, N);
                text += new String(buffer, 0, read);

                if (read < N) {
                    break;
                }
            }
        } catch (Exception ex) {
            String errorMessage = "Cannot read file " + f.getAbsolutePath() + " " + ex.getMessage();
            LOGGER.warn(errorMessage);
            LOGGER.debug("Stack trace", ex);
        }

        return text;
    }

    private Document createNewFile(File file, DocumentBuilder db) throws IOException, TransformerException {
        Document document = null;
        if ((file != null) && (db != null)) {
            file.createNewFile();
            document = db.newDocument();
            document.setXmlVersion("1.0");
            document.setXmlStandalone(false);
            Element root = document.createElement("historic");
            document.appendChild(root);
            String str = xmlToString(document);
            FileWriter fw = new FileWriter(file.getAbsolutePath());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(str);
            bw.flush();
            fw.close();
        }
        return document;
    }

    public static String xmlToString(Document _d) throws TransformerException {
        DOMSource domSource = new DOMSource(_d);
        StringWriter writer = new StringWriter();
        StreamResult sr = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, sr);
        String stringResult = writer.toString();
        return stringResult;
    }

    /**
     * Creates an historic log line from a event node list and add it to the historic log array.
     * 
     * @param nodeEventList, index, events, historicLogArray
     */
    public void createHistoricLogLine(NodeList nodeEventList, int index, ArrayList<Element> events,
            ArrayList<HistoricLogLine> historicLogArray) {
        HistoricLogLine historicLogLine = new HistoricLogLine();
        Element elementEvent = (Element) nodeEventList.item(index);
        String action = null;
        if (elementEvent.hasAttribute("action")) {
            action = elementEvent.getAttribute("action");
            events.add(elementEvent);
        }

        NodeList nodeActuatorList = elementEvent.getElementsByTagName("actuators");
        int nbrNodeActuator = nodeActuatorList.getLength();
        if ((action != null) && action.equalsIgnoreCase(ILogService.START_ACTION) && (nbrNodeActuator != 0)) {
            for (int id = 0; id < nbrNodeActuator; id++) {
                Element elementActuator = (Element) nodeActuatorList.item(id);
                NodeList nodeActuatorElementList = elementActuator.getElementsByTagName(ACTUATOR);
                int nbrnodeActuatorElement = nodeActuatorElementList.getLength();
                if (nbrnodeActuatorElement != 0) {
                    ArrayList<ITrajectory> trajectoriesList = new ArrayList<ITrajectory>();
                    Element element = null;
                    for (int i = 0; i < nbrnodeActuatorElement; i++) {
                        element = (Element) nodeActuatorElementList.item(i);
                        ITrajectory tmpTrajectory = copyToTrajectoryImpl(element);
                        if (tmpTrajectory != null) {
                            trajectoriesList.add(tmpTrajectory);
                        }

                    }
                    historicLogLine.setTrajectories(trajectoriesList);
                }
            }
        } else {
            ArrayList<ITrajectory> trajectoriesList = new ArrayList<ITrajectory>();
            historicLogLine.setTrajectories(trajectoriesList);
        }

        String date = elementEvent.getAttribute("date");
        String time = elementEvent.getAttribute("hour");

        String month = date.substring(0, 2);
        String day = date.substring(2, 4);
        String year = "20" + date.substring(4, 6);
        String hour = time.substring(0, 2);
        String minute = time.substring(3, 5);
        String second = time.substring(6, 8);
        String stringDate = day + month + year + hour + minute + second;
        Date dateEvent = null;
        try {
            dateEvent = stringToDate(stringDate, "ddMMyyyyHHmmss");
        } catch (Exception e) {
            String errorMessage = "Cannot convert date " + stringDate + " " + e.getMessage();
            LOGGER.warn(errorMessage);
            LOGGER.debug("Stack trace", e);
        }
        historicLogLine.setDate(dateEvent);
        historicLogLine.setScanId(elementEvent.getAttribute("scanId"));
        historicLogLine.setAction(elementEvent.getAttribute("action"));
        historicLogLine.setNexusFile(elementEvent.getAttribute("nexusFile"));
        historicLogLine.setNxEntry(elementEvent.getAttribute("nxEntryName"));
        historicLogLine.setScan(elementEvent.getAttribute("scan"));
        historicLogArray.add(historicLogLine);
    }

    private double parseDouble(String value) {
        double result;
        try {
            result = Double.parseDouble(value);
        } catch (Exception e) {
            result = 0;
        }
        return result;
    }

    private ITrajectory copyToTrajectoryImpl(Element element) {
        ITrajectory tmpTrajectory = null;
        if ((element != null) && element.hasAttribute(ID)) {

            // IF IT IS K SCAN
            if (element.hasAttribute(KDELTA) && element.hasAttribute(E0) && element.hasAttribute(E1)
                    && element.hasAttribute(E2) && element.hasAttribute(EDELTAEDGE)
                    && element.hasAttribute(EDELTAPRE_EDGE) && element.hasAttribute(EMIN) && element.hasAttribute(KMAX)
                    && element.hasAttribute(KMIN) && element.hasAttribute(M) && element.hasAttribute(N)) {
                TrajectoryKImpl trajectoryK = new TrajectoryKImpl();
                trajectoryK.setE0(parseDouble(element.getAttribute(E0)));
                trajectoryK.setE1(parseDouble(element.getAttribute(E1)));
                trajectoryK.setE2(parseDouble(element.getAttribute(E2)));
                trajectoryK.setEDeltaEdge(parseDouble(element.getAttribute(EDELTAEDGE)));
                trajectoryK.setEDeltaPreEdge(parseDouble(element.getAttribute(EDELTAPRE_EDGE)));
                trajectoryK.setEMin(parseDouble(element.getAttribute(EMIN)));
                trajectoryK.setKDelta(parseDouble(element.getAttribute(KDELTA)));
                trajectoryK.setKMax(parseDouble(element.getAttribute(KMAX)));
                trajectoryK.setKMin(parseDouble(element.getAttribute(KMIN)));
                trajectoryK.setM(parseDouble(element.getAttribute(M)));
                trajectoryK.setN(parseDouble(element.getAttribute(N)));
                trajectoryK.setDeadTime(parseDouble(element.getAttribute(DEADTIME)));
                tmpTrajectory = trajectoryK;
            } else if (element.hasAttribute(FROM) && element.hasAttribute(TO) && element.hasAttribute(ID)
                    && element.hasAttribute(DELTA)) {

                tmpTrajectory = new TrajectoryImpl();
                tmpTrajectory.setBeginPosition(parseDouble(element.getAttribute(FROM)));
                tmpTrajectory.setEndPosition(parseDouble(element.getAttribute(TO)));
                tmpTrajectory.setDelta(parseDouble(element.getAttribute(DELTA)));

            }
            if (tmpTrajectory != null) {
                // Always set on false
                tmpTrajectory.setRelative(false);
                tmpTrajectory.setName(element.getAttribute(ID));
            }

        }
        return tmpTrajectory;
    }

    public static Date stringToDate(String sDate, String sFormat) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(sFormat);
        return sdf.parse(sDate);
    }

    @Override
    public void endLogScan(IConfig<?> config, IContext context) throws SalsaLoggingException {
        logScan(config, context, "finish");
    }
}

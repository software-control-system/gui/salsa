package fr.soleil.salsa.service.local;

import fr.soleil.salsa.business.ScanConnector;
import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.service.IScanStatusService;

/**
 * @author Alike
 *
 * Class relative to the scan status informations retrieving.
 */
public class LocalScanStatusService implements IScanStatusService  {

    /**
     * Connects to the scan server.
     */
    private ScanConnector scanConnector;


    /**
     * The constructor.
     */
    public LocalScanStatusService() {
        scanConnector = new ScanConnector();
    }
    
    /* (non-Javadoc)
     * @see fr.soleil.salsa.service.IScanStatusService#getScanServerStatus(java.lang.String)
     */
    public IScanStatus getScanServerStatus(String scanServerName) throws SalsaDeviceException {
        //if(true) return null;
        IScanStatus result = scanConnector.getScanStatus(scanServerName);
        return result;
    }
    
}

package fr.soleil.salsa.service.local;

import fr.soleil.salsa.dao.impl.ConfigDAO;
import fr.soleil.salsa.service.IConfigService;
import fr.soleil.salsa.service.local.tool.AConfigService;

/**
 * The service that saves and loads configurations.
 * The local version of the config service.
 * @author Boualem ABDELLI
 *
 */
public class LocalConfigService extends AConfigService implements IConfigService {

    public LocalConfigService(){
        super.setConfigDao(new ConfigDAO());
    }
}

package fr.soleil.salsa.service.local.tool;

import fr.soleil.salsa.dao.IConfigDao;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.exception.persistence.PersistenceException;
import fr.soleil.salsa.service.IConfigService;

/**
 * The abstract class that saves and loads configurations and directories.
 * 
 * @author Boualem ABDELLI
 * 
 */

public abstract class AConfigService implements IConfigService {

    /**
     * The config dao
     */
    private IConfigDao configDao;

    @Override
    public IConfig<?> saveConfig(IConfig<?> config) throws PersistenceException,
            ScanNotFoundException {
        return configDao.saveConfig(config);
    }

    @Override
    public IConfig<?> loadConfigByPath(String path) throws PersistenceException,
            ScanNotFoundException {
        return configDao.loadConfigByPath(path);
    }

    @Override
    public IConfig<?> loadConfigById(Integer id) throws PersistenceException, ScanNotFoundException {
        return configDao.loadConfigById(id);
    }

    @Override
    public IDirectory getRootDirectory() {
        return configDao.getRootDirectory();
    }

    @Override
    public IDirectory saveDirectory(IDirectory directory) throws PersistenceException {
        return configDao.saveDirectory(directory);
    }

    @Override
    public IConfig<?> getConfigById(Integer id) throws ScanNotFoundException {
        return configDao.getConfigById(id);
    }

    @Override
    public IConfig<?> getConfigByPath(String fullPath) throws ScanNotFoundException {
        return configDao.getConfigByPath(fullPath);
    }

    @Override
    public IConfig<?> getConfigByPath(String fullPath, IConfig<?> lastLoadedConfig)
            throws ScanNotFoundException {
        return configDao.getConfigByPath(fullPath, lastLoadedConfig);
    }

    @Override
    public void deleteConfig(IConfig<?> config) throws PersistenceException {
        configDao.deleteConfig(config);
    }

    @Override
    public void deleteDirectory(IDirectory directory) throws PersistenceException {
        configDao.deleteDirectory(directory);
    }

    /**
     * @return the configDao
     */
    public IConfigDao getConfigDao() {
        return configDao;
    }

    /**
     * @param configDao the configDao to set
     */
    public void setConfigDao(IConfigDao configDao) {
        this.configDao = configDao;
    }

}

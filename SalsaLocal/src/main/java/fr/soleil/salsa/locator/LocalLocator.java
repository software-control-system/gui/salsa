package fr.soleil.salsa.locator;

import java.lang.reflect.Constructor;
import java.util.ResourceBundle;

import org.slf4j.Logger;

import fr.soleil.salsa.exception.LocatorException;
import fr.soleil.salsa.locator.tool.ABaseLocator;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * This locator is used in local mode, to instantiate the local versions of the services.<br>
 * The class of the service is the one shown in the conrfiguration file.<br>
 * <br>
 * It uses the LocatorServicesCache from ABaseLocator to store the services into a cache.<br>
 * 
 * @author Francois Denhez
 * @author Etienne Boutry
 */
public class LocalLocator extends ABaseLocator {

    /**
     * The log.
     */
    public static final Logger LOGGER = LoggingUtil.getLogger(LocalLocator.class);

    /**
     * Constructor
     * @param configFileName the name of the configuration file.
     * @param configFile the configuration file.
     * @param defaultLocator the default locator, for use when the configuration file has no key for the service.
     */
    public LocalLocator(String configFileName, ResourceBundle configFile, ILocator defaultLocator) {
        super(configFileName, configFile, defaultLocator);
    }

    /**
     * Returns the service that implements the given interface, the configuration value being given as the second argument.
     * @param T : the class of the interface of the service.
     * @param interfaceClass : the class of the interface of the service.
     * @param configValue : the value in the configuration file for this service.
     * @return
     * @throws LocatorException
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getService(Class<T> interfaceClass, String configValue) throws LocatorException {
        Class<T> serviceClass;
        T service = null;
        try {
            serviceClass = (Class<T>) Class.forName(configValue);
        }
        catch(ClassNotFoundException e) {
            String errorMessage = "Cannot created service " + configValue + ". See "+ this.configFileName + ".properties " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
            throw new LocatorException(errorMessage, e);
        }
        Constructor<T> serviceConstructor;
        try {
            serviceConstructor = serviceClass.getConstructor();
        } catch (SecurityException e) {
            String errorMessage = "Cannot created service " + interfaceClass.getCanonicalName()
            + " the access to the constructor without parameters is forbidden.";
            LOGGER.error(errorMessage);
            throw new LocatorException(errorMessage);
        } catch (NoSuchMethodException e) {
            String errorMessage = "Cannot create service " + interfaceClass.getCanonicalName()
            + " has no constructor without parameters.";
            LOGGER.error(errorMessage);
            throw new LocatorException(errorMessage, e);
        }

        try {
            service = serviceConstructor.newInstance();
            String infoMessage = "Initialization of the service " + service.getClass().getCanonicalName();
            LOGGER.debug(infoMessage);
        } catch (Exception e) {
            String errorMessage = "Cannot create  " + interfaceClass.getCanonicalName() + " " + e.getMessage();
            LOGGER.error(errorMessage);
            LOGGER.debug("Stack trace", e);
        }
        return service;
    }

}

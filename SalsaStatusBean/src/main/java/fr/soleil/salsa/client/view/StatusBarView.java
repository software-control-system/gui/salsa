package fr.soleil.salsa.client.view;

import fr.soleil.bean.scanserver.ScanServerStatusBar;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.preferences.UIPreferencesEvent;
import fr.soleil.salsa.client.preferences.UIPreferencesEvent.UIPreferenceEventType;
import fr.soleil.salsa.client.preferences.UIPreferencesListener;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * @author Alike
 * 
 *         Class relative to the status bar view.
 */
public class StatusBarView extends ScanServerStatusBar implements UIPreferencesListener {

    private static final long serialVersionUID = 8890813177837024945L;

    /**
     * Constructor
     */
    public StatusBarView() {
        DevicePreferences devicePref = SalsaAPI.getDevicePreferences();
        if (devicePref != null) {
            setScanServerName(devicePref.getScanServer());
        }
        setExecutedBatchFile(UIPreferences.getInstance().getControlPanel());
        UIPreferences.getInstance().addPreferenceListener(this);
        start();
    }

    @Override
    public void preferenceChanged(UIPreferencesEvent event) {
        if ((event != null) && (event.getType() == UIPreferenceEventType.CONTROL_PANEL_CHANGED)) {
            setExecutedBatchFile(UIPreferences.getInstance().getControlPanel());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        UIPreferences.getInstance().removePreferenceListener(this);
        super.finalize();
    }
}

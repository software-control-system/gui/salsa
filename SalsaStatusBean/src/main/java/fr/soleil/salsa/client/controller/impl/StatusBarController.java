package fr.soleil.salsa.client.controller.impl;

/**
 * @author Alike
 * 
 *         Class relative to the status bar controller.
 */
public class StatusBarController {

    public StatusBarController() {
        // this(null);
    }

    // /**
    // * Default constructor.
    // */
    // public StatusBarController(StatusBarView view) {
    // super(view);
    // refresh();
    // }
    //
    // @Override
    // protected void initBeforeSetView() {
    // super.initBeforeSetView();
    // scanListeners = new ArrayList<WeakReference<IScanListener>>();
    // }
    //
    // @Override
    // protected StatusBarView generateView() {
    // return new StatusBarView(this);
    // }
    //
    // private void refresh() {
    // if (view != null) {
    // String factoryClassName = SalsaDAOFactory.class.getName();
    // view.setDaoFactory(factoryClassName);
    // view.switchDAOFactory(factoryClassName);
    // }
    // }
    //
    // public void addScanListener(IScanListener scanListener) {
    // if (scanListener != null) {
    // ArrayList<WeakReference<IScanListener>> toRemove = new
    // ArrayList<WeakReference<IScanListener>>();
    // synchronized (scanListeners) {
    // boolean canAdd = true;
    // for (WeakReference<IScanListener> ref : scanListeners) {
    // IScanListener listener = ref.get();
    // if (listener == null) {
    // toRemove.add(ref);
    // }
    // else if (listener.equals(scanListener)) {
    // canAdd = false;
    // }
    // }
    // scanListeners.retainAll(toRemove);
    // if (canAdd) {
    // scanListeners.add(new WeakReference<IScanListener>(scanListener));
    // }
    // }
    // toRemove.clear();
    // }
    // }
    //
    // public void removeScanListener(IScanListener scanListener) {
    // if (scanListener != null) {
    // ArrayList<WeakReference<IScanListener>> toRemove = new
    // ArrayList<WeakReference<IScanListener>>();
    // synchronized (scanListeners) {
    // for (WeakReference<IScanListener> ref : scanListeners) {
    // IScanListener listener = ref.get();
    // if ((listener == null) || listener.equals(scanListener)) {
    // toRemove.add(ref);
    // }
    // }
    // scanListeners.retainAll(toRemove);
    // }
    // toRemove.clear();
    // }
    // }
    //
    // public void removeAllScanListeners() {
    // synchronized (scanListeners) {
    // scanListeners.clear();
    // }
    // }
    //
    // public void notifyScanFinished() {
    // ArrayList<WeakReference<IScanListener>> toRemove = new
    // ArrayList<WeakReference<IScanListener>>();
    // synchronized (scanListeners) {
    // for (WeakReference<IScanListener> ref : scanListeners) {
    // IScanListener listener = ref.get();
    // if (listener != null) {
    // listener.notifyEndOfScanDetected();
    // }
    // }
    // scanListeners.retainAll(toRemove);
    // }
    // toRemove.clear();
    // }
    //
    // public void notifyScanRunning() {
    // ArrayList<WeakReference<IScanListener>> toRemove = new
    // ArrayList<WeakReference<IScanListener>>();
    // synchronized (scanListeners) {
    // for (WeakReference<IScanListener> ref : scanListeners) {
    // IScanListener listener = ref.get();
    // if (listener != null) {
    // listener.notifyScanRunning(true);
    // }
    // }
    // scanListeners.retainAll(toRemove);
    // }
    // toRemove.clear();
    // }

}

package fr.soleil.salsa.client.view;

import fr.soleil.bean.scanserver.ScanServerActionBean;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * Scan functions view.
 */
public class ScanFunctionsBean extends ScanServerActionBean {

    private static final long serialVersionUID = -6686557091364566778L;

    public ScanFunctionsBean() {
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        // Load the scanServerName
        if (devicePreferences != null) {
            String scanServerName = devicePreferences.getScanServer();
            setScanServerName(scanServerName);
            start();
        }
    }
}

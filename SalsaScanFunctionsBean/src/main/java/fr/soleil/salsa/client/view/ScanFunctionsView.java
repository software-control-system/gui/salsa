package fr.soleil.salsa.client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import fr.soleil.salsa.client.controller.IScanFunctionsController;
import fr.soleil.salsa.client.view.component.Option;

/**
 * Scan functions view.
 */
public class ScanFunctionsView extends JPanel implements IView<IScanFunctionsController> {

    private static final long serialVersionUID = 407034513691648523L;

    /**
     * A combo box GUI element.
     */
    private class ComboBox extends JComboBox<String> {

        private static final long serialVersionUID = 8066631652776899889L;

        /**
         * Defines the options.
         * 
         * @param optionList
         */
        public void setOptionList(final String[] optionList) {
            removeAllItems();
            if (optionList != null) {
                for (String element : optionList) {
                    addItem(element);
                }
            }
            if (getItemCount() > 0) {
                setSelectedIndex(0);
            } else {
                setSelectedItem(null);
            }
            repaint();
        }
    }

    /**
     * Enables / disables the sensor combo box on the EDT.
     */
    private class SetSensorEnabledTask implements Runnable {
        @Override
        public void run() {
            boolean enabled = sensorSelectEnabled && sensorsArray != null && sensorsArray.length > 0;
            sensorSelect.setEnabled(enabled);
        }
    }

    /**
     * Enables / disables the sensor combo box on the EDT.
     */
    private class SetActuatorEnabledTask implements Runnable {
        @Override
        public void run() {
            boolean enabled = actuatorSelectEnabled && actuatorsArray != null && actuatorsArray.length > 0;
            actuatorSelect.setEnabled(enabled);
        }
    }

    /**
     * Updates the sensor combo box on the EDT.
     */
    private class SetSensorsArrayTask implements Runnable {
        @Override
        public void run() {
            int selectedIndex = sensorSelect.getSelectedIndex();
            if (sensorsArray.length > 0) {
                String[] optionsStringArray = new String[sensorsArray.length];
                for (int index = 0; index < sensorsArray.length; ++index) {
                    optionsStringArray[index] = sensorsArray[index].getText();
                }
                sensorSelect.setOptionList(optionsStringArray);
                if (selectedIndex == -1) {
                    sensorSelect.setSelectedIndex(0);
                } else if (selectedIndex < sensorsArray.length) {
                    sensorSelect.setSelectedIndex(selectedIndex);
                } else {
                    sensorSelect.setSelectedIndex(0);
                }
            } else {
                sensorSelect.setOptionList(new String[0]);
            }
            boolean enabled = sensorSelectEnabled && sensorsArray != null && sensorsArray.length > 0;
            sensorSelect.setEnabled(enabled);
        }
    }

    /**
     * Updates the actuator combo box on the EDT.
     */
    private class SetActuatorsArrayTask implements Runnable {
        @Override
        public void run() {
            int selectedIndex = actuatorSelect.getSelectedIndex();
            if (actuatorsArray.length > 0) {
                String[] optionsStringArray = new String[actuatorsArray.length];
                for (int index = 0; index < actuatorsArray.length; ++index) {
                    optionsStringArray[index] = actuatorsArray[index].getText();
                }
                actuatorSelect.setOptionList(optionsStringArray);
                if (selectedIndex == -1) {
                    actuatorSelect.setSelectedIndex(0);
                } else if (selectedIndex < actuatorsArray.length) {
                    actuatorSelect.setSelectedIndex(selectedIndex);
                } else {
                    actuatorSelect.setSelectedIndex(0);
                }
            } else {
                actuatorSelect.setOptionList(new String[0]);
            }
            boolean enabled = actuatorSelectEnabled && actuatorsArray != null && actuatorsArray.length > 0;
            actuatorSelect.setEnabled(enabled);
        }
    }

    /**
     * The controller.
     */
    private IScanFunctionsController controller;

    /**
     * List of functions.
     */
    private Option[] functionsArray;

    /**
     * Combo box that contains the list of functions.
     */
    private ComboBox functionSelect;

    /**
     * List of sensors.
     */
    private Option[] sensorsArray;

    /**
     * Combo box that contains the list of sensors.
     */
    private ComboBox sensorSelect;

    /**
     * List of actuators.
     */
    private Option[] actuatorsArray;

    /**
     * Combo box that contains the list of actuators.
     */
    private ComboBox actuatorSelect;

    /**
     * The goto button.
     */
    private JButton gotoButton;

    /**
     * The help text label.
     */
    private JLabel helpTextLabel;

    /**
     * Is the sensor combo box enabled. If the combo box is empty, it will be disabled no matter
     * this property. It will be reenabled automatically if this property is true and elements are
     * added.
     */
    private boolean sensorSelectEnabled = false;

    /**
     * Is the actuator combo box enabled. If the combo box is empty, it will be disabled no matter
     * this property. It will be reenabled automatically if this property is true and elements are
     * added.
     */
    private boolean actuatorSelectEnabled = false;

    /**
     * Constructor
     */
    public ScanFunctionsView() {
        this(null);
    }

    /**
     * Constuctor.
     * 
     * @param controller The controller that controls this view.
     */
    public ScanFunctionsView(IScanFunctionsController controller) {
        this.controller = controller;
        initialize();
    }

    /**
     * Initializes this view.
     */
    private void initialize() {

        functionSelect = new ComboBox();
        functionSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (functionSelect.getSelectedIndex() != -1) {
                    if (functionsArray != null) {
                        Object value = functionsArray[functionSelect.getSelectedIndex()].getValue();
                        if (controller != null) {
                            controller.notifyFunctionSelected(value);
                        }
                    }
                }
            }
        });
        functionSelect.setEnabled(false);

        sensorSelect = new ComboBox();
        sensorSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object value;
                if (sensorSelect.getSelectedIndex() != -1) {
                    value = sensorsArray[sensorSelect.getSelectedIndex()].getValue();
                } else {
                    value = null;
                }
                if (controller != null) {
                    controller.notifySensorSelected(value);
                }
            }
        });
        sensorSelect.setEnabled(false);

        actuatorSelect = new ComboBox();
        actuatorSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object value;
                if (actuatorSelect.getSelectedIndex() != -1) {
                    value = actuatorsArray[actuatorSelect.getSelectedIndex()].getValue();
                } else {
                    value = null;
                }
                if (controller != null) {
                    controller.notifyActuatorSelected(value);
                }
            }
        });
        actuatorSelect.setEnabled(false);

        helpTextLabel = new JLabel();

        gotoButton = new JButton();
        gotoButton.setText("Go to");
        gotoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyGotoClicked();
                }
            }
        });

        // View layout
        JPanel selectsPanel = new JPanel();
        selectsPanel.setBorder(BorderFactory.createTitledBorder("Go to"));
        GridLayout selectsPanelLayout = new GridLayout(4, 1);
        selectsPanel.setLayout(selectsPanelLayout);
        selectsPanel.add(functionSelect);
        selectsPanel.add(sensorSelect);
        selectsPanel.add(actuatorSelect);
        selectsPanel.add(helpTextLabel);

        this.setLayout(new GridBagLayout());
        GridBagConstraints selectsPanelConstraint = new GridBagConstraints();
        selectsPanelConstraint.gridx = 0;
        selectsPanelConstraint.gridy = 0;
        selectsPanelConstraint.weightx = 1d;
        selectsPanelConstraint.weighty = 1d;
        selectsPanelConstraint.fill = GridBagConstraints.HORIZONTAL;
        selectsPanelConstraint.anchor = GridBagConstraints.NORTH;
        this.add(selectsPanel, selectsPanelConstraint);
        GridBagConstraints gotoButtonConstraints = new GridBagConstraints();
        gotoButtonConstraints.gridx = 1;
        gotoButtonConstraints.gridy = 0;
        gotoButtonConstraints.weighty = 1d;
        gotoButtonConstraints.anchor = GridBagConstraints.NORTH;
        this.add(gotoButton, gotoButtonConstraints);
    }

    /**
     * The controller.
     */
    @Override
    public IScanFunctionsController getController() {
        return controller;
    }

    @Override
    public void setController(IScanFunctionsController controller) {
        this.controller = controller;
    }

    /**
     * List of functions.
     */
    public Option[] getFunctionsArray() {
        return functionsArray;
    }

    /**
     * List of functions.
     */
    public void setFunctionsArray(Option[] functionsArray) {
        if (!Arrays.equals(this.functionsArray, functionsArray)) {
            this.functionsArray = functionsArray;

            int selectedIndex = functionSelect.getSelectedIndex();
            if (functionsArray != null) {
                if (functionsArray.length > 0) {
                    String[] optionsStringArray = new String[functionsArray.length];
                    for (int index = 0; index < functionsArray.length; ++index) {
                        optionsStringArray[index] = functionsArray[index].getText();
                    }
                    for (String option : optionsStringArray) {
                        functionSelect.addItem(option);
                    }
                    if (selectedIndex == -1) {
                        functionSelect.setSelectedIndex(0);
                    } else if (selectedIndex < functionsArray.length) {
                        functionSelect.setSelectedIndex(selectedIndex);
                    } else {
                        functionSelect.setSelectedIndex(0);
                    }
                }
                functionSelect.setEnabled(functionsArray.length > 0);
            }
        }
    }

    /**
     * The help text for the function.
     * 
     * @return
     */
    public String getHelpText() {
        return helpTextLabel.getText();
    }

    /**
     * The help text for the function;
     * 
     * @param helpText
     */
    public void setHelpText(String helpText) {
        this.helpTextLabel.setText(helpText);
    }

    /**
     * List of sensors.
     */
    public Option[] getSensorsArray() {
        return sensorsArray;
    }

    /**
     * List of sensors.
     */
    public void setSensorsArray(Option[] sensorsArray) {
        if (!Arrays.equals(this.sensorsArray, sensorsArray)) {
            this.sensorsArray = sensorsArray;
            SwingUtilities.invokeLater(new SetSensorsArrayTask());
        }
    }

    /**
     * Enables / disables the actuator combo box. If the combo box is empty, it will be disabled no
     * matter this property. It will be reenabled automatically if this property is true and
     * elements are added.
     * 
     * @param enable
     */
    public void setActuatorSelectEnabled(boolean enable) {
        if (this.actuatorSelectEnabled != enable) {
            this.actuatorSelectEnabled = enable;
            SwingUtilities.invokeLater(new SetActuatorEnabledTask());
        }
    }

    /**
     * Is the actuator combo box enabled. If the combo box is empty, it will be disabled no matter
     * this property. It will be reenabled automatically if this property is true and elements are
     * added.
     * 
     * @return
     */
    public boolean isActuatorSelectEnabled() {
        return actuatorSelectEnabled;
    }

    /**
     * List of actuators.
     */
    public Option[] getActuatorsArray() {
        return actuatorsArray;
    }

    /**
     * List of actuators.
     */
    public void setActuatorsArray(Option[] actuatorsArray) {
        if (!Arrays.equals(this.actuatorsArray, actuatorsArray)) {
            this.actuatorsArray = actuatorsArray;
            SwingUtilities.invokeLater(new SetActuatorsArrayTask());
        }
    }

    /**
     * Enables / disables the sensor combo box. If the combo box is empty, it will be disabled no
     * matter this property. It will be reenabled automatically if this property is true and
     * elements are added.
     * 
     * @param enable
     */
    public void setSensorSelectEnabled(boolean enable) {
        if (this.sensorSelectEnabled != enable) {
            this.sensorSelectEnabled = enable;
            SwingUtilities.invokeLater(new SetSensorEnabledTask());
        }
    }

    /**
     * Is the sensor combo box enabled. If the combo box is empty, it will be disabled no matter
     * this property. It will be reenabled automatically if this property is true and elements are
     * added.
     * 
     * @return
     */
    public boolean isSensorSelectEnabled() {
        return sensorSelectEnabled;
    }
}

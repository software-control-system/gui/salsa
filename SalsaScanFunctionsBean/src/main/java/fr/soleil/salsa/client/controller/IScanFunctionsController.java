package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.ScanFunctionsView;

/**
 * A controller for the scan functions view.
 */
public interface IScanFunctionsController extends IController<ScanFunctionsView> {

    /**
     * Callback used when the user chooses a function.
     * 
     * @param value
     */
    public void notifyFunctionSelected(Object value);

    /**
     * Callback used when the user chooses a sensor.
     * 
     * @param value
     */
    public void notifySensorSelected(Object value);

    /**
     * Callback used when the user chooses an actuator.
     * 
     * @param value
     */
    public void notifyActuatorSelected(Object value);

    /**
     * Callback used when the used clicks on the goto button.
     */
    public void notifyGotoClicked();
}

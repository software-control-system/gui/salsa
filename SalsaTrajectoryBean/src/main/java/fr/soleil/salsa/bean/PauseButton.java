package fr.soleil.salsa.bean;

import java.awt.Color;

import javax.swing.JFrame;

import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.view.ActionButton.ACTION_TYPE;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class PauseButton extends AbstractActionSalsaBean {

    private static final long serialVersionUID = 4291197101238212691L;

    protected IContext context = null;

    public PauseButton() {
        super(ACTION_TYPE.PAUSE);
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        context = SalsaAPI.extractContext(devicePreferences);
        setBackgroundButton(new Color(255, 230, 230));
    }

    @Override
    protected void executeAction(IConfig<?> config) throws SalsaException {
        if (context != null) {
            SalsaAPI.pauseScan(context);
        }
    }

    @Override
    protected String getActionMessage() {
        String configPath = getConfigPath();
        String message = "No configuration is defined.";
        if (SalsaUtils.isDefined(configPath)) {
            message = ACTION_TYPE.PAUSE.toString() + " " + configPath + " configuration";
        }
        return message;
    }

    @Override
    protected void setMovingState(boolean moving) {
        setEnabled(moving);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        PauseButton bean = new PauseButton();
        bean.setConfigPath("Katy/1DFast");
        bean.setConfirmation(true);
        frame.getContentPane().add(bean);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(bean.getClass().getName());
        frame.setVisible(true);
    }
}

package fr.soleil.salsa.bean;

import java.awt.Color;

import javax.swing.JFrame;

import fr.esrf.Tango.DevState;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.view.ActionButton.ACTION_TYPE;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class ResumeButton extends AbstractActionSalsaBean {

    private static final long serialVersionUID = 7213504352582347002L;

    protected IContext context = null;

    public ResumeButton() {
        super(ACTION_TYPE.RESUME);
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        context = SalsaAPI.extractContext(devicePreferences);
        setBackgroundButton(new Color(255, 250, 200));
    }

    @Override
    protected void executeAction(IConfig<?> config) throws SalsaException {
        if (context != null) {
            SalsaAPI.resumeScan(context);
        }
    }

    @Override
    protected String getActionMessage() {
        String configPath = getConfigPath();
        String message = "No configuration is defined.";
        if (SalsaUtils.isDefined(configPath)) {
            message = ACTION_TYPE.RESUME.toString() + " " + configPath + " configuration";
        }
        return message;
    }

    @Override
    public void stateChanged(String state) {
        super.stateChanged(state);
        if (DevState.STANDBY.toString().equals(state)) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }

    @Override
    protected void setMovingState(boolean moving) {
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        ResumeButton bean = new ResumeButton();
        bean.setConfigPath("Katy/1DFast");
        bean.setConfirmation(true);
        frame.getContentPane().add(bean);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(bean.getClass().getName());
        frame.setVisible(true);
    }
}

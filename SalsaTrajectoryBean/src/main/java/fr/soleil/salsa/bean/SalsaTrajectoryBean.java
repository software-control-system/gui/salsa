package fr.soleil.salsa.bean;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.slf4j.Logger;

import fr.soleil.bean.scanserver.ScanServerStatusBar;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.swing.panel.VisibilityTabbedPane;
import fr.soleil.salsa.client.controller.IGenericDeviceListListener;
import fr.soleil.salsa.client.controller.impl.ErrorStrategiesController;
import fr.soleil.salsa.client.controller.impl.GeneralController;
import fr.soleil.salsa.client.controller.impl.GenericActuatorListController;
import fr.soleil.salsa.client.controller.impl.GenericSensorListController;
import fr.soleil.salsa.client.controller.impl.GenericTimebaseListController;
import fr.soleil.salsa.client.controller.impl.HookCommandController;
import fr.soleil.salsa.client.controller.impl.ScanEnergyController;
import fr.soleil.salsa.client.controller.impl.ScanKTrajectoryController;
import fr.soleil.salsa.client.controller.impl.TrajectoryController;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.view.EnergyTrajectoryView;
import fr.soleil.salsa.client.view.ErrorStrategiesView;
import fr.soleil.salsa.client.view.GeneralView;
import fr.soleil.salsa.client.view.GenericDeviceListView;
import fr.soleil.salsa.client.view.HookTableListView;
import fr.soleil.salsa.client.view.ScanKTrajectoryView;
import fr.soleil.salsa.client.view.TrajectoryListView;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class SalsaTrajectoryBean extends AbstractSalsaBean implements ISalsaActionBeanListener {

    private static final long serialVersionUID = 3105210182908935317L;

    public static final Logger LOGGER = LoggingUtil.getLogger(SalsaTrajectoryBean.class);

    private static final String TRAJECTORY_LABEL = "Trajectories";
    private static final String ACTUATOR_LABEL = "Actuators";
    private static final String SENSOR_LABEL = "Sensors";
    private static final String TIMEBASE_LABEL = "Timebases";
    private static final String ERRORSTRATEGY_LABEL = "Error Strategies";
    private static final String GENERAL_LABEL = "General";
    private static final String HOOK_LABEL = "Hook";
    private boolean showError = true;

    private boolean userEnabled = true;
    private boolean sensorsEnable = true;
    private boolean sensorsVisible = true;
    private boolean actuatorsEnable = true;
    private boolean actuatorsVisible = true;
    private boolean timebasesEnable = true;
    private boolean timebasesVisible = true;
    private boolean hookEnable = true;
    private boolean hookVisible = true;
    private boolean errorStrategyEnable = true;
    private boolean errorStrategyVisible = true;
    private boolean generalEnable = true;
    private boolean generalVisible = true;

    public enum ConfigType {
        CONFIG1D, CONFIG2D, CONFIGK, CONFIGENERGY, NONE
    }

    // Controller
    private final ScanEnergyController scanEnergyController = new ScanEnergyController();
    private final ScanKTrajectoryController scanKController = new ScanKTrajectoryController();
    private final TrajectoryController scan1DController = new TrajectoryController();
    private final TrajectoryController scanXController = new TrajectoryController();
    private final TrajectoryController scanYController = new TrajectoryController(true);
    private final ErrorStrategiesController errorStrategyController = new ErrorStrategiesController();
    private final GeneralController generalController = new GeneralController();
    private final HookCommandController hookController = new HookCommandController();
    private final GenericSensorListController sensorListController = new GenericSensorListController();
    private final GenericTimebaseListController timebaseListController = new GenericTimebaseListController();
    private final GenericActuatorListController actuatorListController = new GenericActuatorListController();

    // Views
    EnergyTrajectoryView scanEnergyView = null;
    ScanKTrajectoryView scanKView = null;
    TrajectoryListView scan1DView = null;
    TrajectoryListView scanXView = null;
    TrajectoryListView scanYView = null;
    ErrorStrategiesView errorStrategyView = null;
    HookTableListView hookView = null;

    private final JPanel topPanel = new JPanel();
    private final JPanel buttonMenuPanel = new JPanel();

    private final VisibilityTabbedPane tabbedPane = new VisibilityTabbedPane();
    private final JPanel centerPanel = new JPanel();
    private final JPanel emptyPanel = new JPanel();
    private final JPanel trajectoryPanel = new JPanel();
    private GenericDeviceListView actuatorPanel = null;
    private GenericDeviceListView sensorPanel = null;
    private GenericDeviceListView timeBasePanel = null;
    private GeneralView generalPanel = null;
    private final JTabbedPane tabbedPane2D = new JTabbedPane();

    private final StartButton startButton = new StartButton();
    private final StopButton stopButton = new StopButton();
    private final PauseButton pauseButton = new PauseButton();
    private final ResumeButton resumeButton = new ResumeButton();
    private final JButton reloadButton = new JButton();

    private final ScanServerStatusBar statusBar = new ScanServerStatusBar();

    private final JLabel configNameLabel = new JLabel("no config");
    private final RecordDataButton recordButton = new RecordDataButton();

    // Layout
    CardLayout layout = new CardLayout();
    CardLayout layout2 = new CardLayout();

    public SalsaTrajectoryBean() {
        this(true);
    }

    public SalsaTrajectoryBean(final boolean standAlone) {
        setLayout(new BorderLayout());
        add(topPanel, BorderLayout.NORTH);
        centerPanel.setLayout(layout2);
        centerPanel.add(ConfigType.NONE.toString(), emptyPanel);
        centerPanel.add("CONFIG", tabbedPane);

        IGenericDeviceListListener actuatorListener = new IGenericDeviceListListener() {

            @Override
            public void deviceRenamed(final String oldName, final String newName) {
                // Nothing to do
            }

            @Override
            public void deviceRemoved(final String device) {
                // Nothing to do
            }

            @Override
            public void deviceEnabled(final String device, final boolean enabled) {
                IConfig<?> currentConfig = getConfig();
                if (actuatorsVisible && (currentConfig != null)) {
                    currentConfig.setActuatorEnable(device, enabled, IConfig.FIRST_DIMENSION);
                    if ((currentConfig instanceof IConfig1D) || (currentConfig instanceof IConfigHCS)) {
                        scan1DController.setConfig(currentConfig);
                    }
                    if (currentConfig instanceof IConfig2D) {
                        scanXController.setConfig(currentConfig);
                        scanYController.setConfig(currentConfig);
                    }
                }
            }

            @Override
            public void deviceAdded(final String device) {
                // Nothing to do
            }

            @Override
            public void devicesAdded(final List<String> devices) {
                // Nothing to do
            }

            @Override
            public void deviceSwap(final String device1, final String device2) {
                // Nothing to do
            }
        };

        actuatorListController.setAdminMode(false);
        actuatorListController.addGenericDeviceListListener(actuatorListener);
        actuatorPanel = actuatorListController.getView();
        actuatorPanel.setEditable(false);

        sensorListController.setAdminMode(false);
        sensorPanel = sensorListController.getView();
        sensorPanel.setEditable(false);

        timebaseListController.setAdminMode(false);
        timeBasePanel = timebaseListController.getView();
        timeBasePanel.setEditable(false);

        tabbedPane.add(TRAJECTORY_LABEL, trajectoryPanel);
        tabbedPane.add(ACTUATOR_LABEL, actuatorPanel);
        tabbedPane.add(SENSOR_LABEL, sensorPanel);
        tabbedPane.add(TIMEBASE_LABEL, timeBasePanel);

        errorStrategyView = new ErrorStrategiesView(errorStrategyController);
        errorStrategyController.setView(errorStrategyView);
        tabbedPane.add(ERRORSTRATEGY_LABEL, errorStrategyView);

        generalPanel = new GeneralView(generalController);
        generalPanel.setLightView(true);
        setGeneralEnable(true);
        generalController.setView(generalPanel);
        tabbedPane.add(GENERAL_LABEL, generalPanel);

        hookView = new HookTableListView();
        hookView.setController(hookController);
        hookController.setView(hookView);
        hookView.setReadOnly(true);
        tabbedPane.add(HOOK_LABEL, hookView);

        tabbedPane.setComponentIcon(actuatorPanel, Icons.getIcon("salsa.scanconfig.actuator"));
        tabbedPane.setComponentIcon(sensorPanel, Icons.getIcon("salsa.scanconfig.sensor"));
        tabbedPane.setComponentIcon(timeBasePanel, Icons.getIcon("salsa.scanconfig.timebase"));
        tabbedPane.setComponentIcon(errorStrategyView, Icons.getIcon("salsa.scanconfig.errorstrategy"));
        tabbedPane.setComponentIcon(generalPanel, Icons.getIcon("salsa.scanconfig.general"));
        tabbedPane.setComponentIcon(hookView, Icons.getIcon("salsa.scanconfig.hooks"));

        add(centerPanel, BorderLayout.CENTER);

        // BUILD THE MENU
        topPanel.setLayout(new BorderLayout());
        configNameLabel.setHorizontalAlignment(JLabel.CENTER);
        topPanel.add(configNameLabel, BorderLayout.NORTH);
        topPanel.add(buttonMenuPanel, BorderLayout.SOUTH);

        buttonMenuPanel.add(startButton);
        buttonMenuPanel.add(pauseButton);
        buttonMenuPanel.add(resumeButton);
        buttonMenuPanel.add(stopButton);

        startButton.addSalsaBeanListener(this);
        stopButton.addSalsaBeanListener(this);
        pauseButton.addSalsaBeanListener(this);
        resumeButton.addSalsaBeanListener(this);

        reloadButton.setIcon(Icons.getIcon("salsa.reload"));
        reloadButton.setToolTipText("Reload the configuration");
        buttonMenuPanel.add(reloadButton);
        buttonMenuPanel.add(recordButton);
        recordButton.setVisible(standAlone);

        reloadButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                reloadConfig();
            }
        });
        // buttonMenuPanel.add(new JButton("recordData"));

        scanEnergyView = new EnergyTrajectoryView(scanEnergyController, false);
        scanKView = new ScanKTrajectoryView(scanKController);
        scan1DView = new TrajectoryListView(scan1DController, true);
        scanXView = new TrajectoryListView(scanXController, true);
        scanYView = new TrajectoryListView(scanYController, true);

        scanEnergyController.setTrajectoryView(scanEnergyView);
        scanKController.setView(scanKView);
        scan1DController.setTrajectoryView(scan1DView);
        scanXController.setTrajectoryView(scanXView);
        scanYController.setTrajectoryView(scanYView);

        trajectoryPanel.setLayout(layout);

        JPanel emptyPanel = new JPanel();
        tabbedPane2D.add("Trajectory X", scanXView);
        tabbedPane2D.add("Trajectory Y", scanYView);
        tabbedPane2D.setIconAt(0, Icons.getIcon("salsa.xtrajectory"));
        tabbedPane2D.setIconAt(1, Icons.getIcon("salsa.ytrajectory"));

        trajectoryPanel.add(scan1DView, ConfigType.CONFIG1D.toString());
        trajectoryPanel.add(tabbedPane2D, ConfigType.CONFIG2D.toString());
        trajectoryPanel.add(scanKView, ConfigType.CONFIGK.toString());
        trajectoryPanel.add(scanEnergyView, ConfigType.CONFIGENERGY.toString());
        trajectoryPanel.add(emptyPanel, ConfigType.NONE.toString());

        layout.show(trajectoryPanel, ConfigType.NONE.toString());
        layout2.show(centerPanel, ConfigType.NONE.toString());

        setUIPreferences();

        if (getDevicePreferences() != null) {
            if (standAlone) {
                add(statusBar, BorderLayout.SOUTH);
                statusBar.setModel(getDevicePreferences().getScanServer());
            }
            recordButton.setSelected(getDevicePreferences().isDataRecorder());
        }
    }

    private void setUIPreferences() {
        UIPreferences uiPreferences = UIPreferences.getInstance();
        if (uiPreferences != null) {
            // Set control panel
            String controlPanel = uiPreferences.getControlPanel();
            statusBar.setExecutedBatchFile(controlPanel);
        }
    }

    @Override
    protected void notifyLoadingConfigSucceed(final IConfig<?> config) {
        super.notifyLoadingConfigSucceed(config);
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            updateLayout();
        });
        startButton.setConfig(config);
        stopButton.setConfig(config);
        pauseButton.setConfig(config);
        resumeButton.setConfig(config);
    }

    private void updateLayout() {
        hideAll();
        String layoutValue = ConfigType.NONE.toString();
        IConfig<?> currentConfig = getConfig();
        final List<JComponent> visibleComponents = new ArrayList<JComponent>();
        if (currentConfig != null) {
            layout2.show(centerPanel, "CONFIG");
            // actuator
            if (actuatorsVisible) {
                List<IActuator> actuators = currentConfig.getActuatorList(IConfig.FIRST_DIMENSION);
                actuatorListController.setDeviceList(actuators);
                tabbedPane.setComponentEnable(actuatorPanel, actuatorsEnable && SalsaUtils.isFulfilled(actuators));
            }

            // sensors
            if (sensorsVisible) {
                List<ISensor> sensors = currentConfig.getSensorsList();
                sensorListController.setConfig(currentConfig);
                sensorListController.setDeviceList(sensors);
                tabbedPane.setComponentEnable(sensorPanel, sensorsEnable && SalsaUtils.isFulfilled(sensors));
            }

            // Timebases
            if (timebasesVisible) {
                List<ITimebase> timebases = currentConfig.getTimebaseList();
                timebaseListController.setConfig(currentConfig);
                tabbedPane.setComponentEnable(timeBasePanel, timebasesEnable && SalsaUtils.isFulfilled(timebases));
            }

            if (hookVisible) {
                IScanAddOns scanAddOns = currentConfig.getScanAddOn();
                List<IHook> hookList = scanAddOns.getHooks();
                boolean hookExist = (scanAddOns != null) && SalsaUtils.isFulfilled(hookList);
                tabbedPane.setComponentEnable(hookView, hookEnable && hookExist);
                hookController.setConfig(currentConfig);
            }

            if (errorStrategyVisible) {
                errorStrategyController.setConfig(currentConfig);
            }

            if (generalVisible) {
                generalController.setConfig(currentConfig);
            }

            if ((currentConfig instanceof IConfig1D) || (currentConfig instanceof IConfigHCS)) {
                layoutValue = ConfigType.CONFIG1D.toString();
                scan1DController.setConfig(currentConfig);
                visibleComponents.add(scan1DView);
                if (currentConfig instanceof IConfig1D) {
                    tabbedPane.setComponentIcon(trajectoryPanel, Icons.getIcon("salsa.scanconfig.1d.small"));
                } else {
                    tabbedPane.setComponentIcon(trajectoryPanel, Icons.getIcon("salsa.scanconfig.hcs.small"));
                }
            } else if (currentConfig instanceof IConfig2D) {
                layoutValue = ConfigType.CONFIG2D.toString();
                scanYController.setConfig(currentConfig);
                scanXController.setConfig(currentConfig);
                visibleComponents.add(tabbedPane2D);
                visibleComponents.add(scanXView);
                visibleComponents.add(scanYView);
                tabbedPane.setComponentIcon(trajectoryPanel, Icons.getIcon("salsa.scanconfig.2d.small"));
            } else if (currentConfig instanceof IConfigK) {
                layoutValue = ConfigType.CONFIGK.toString();
                scanKController.setConfig(currentConfig);
                visibleComponents.add(scanKView);
                tabbedPane.setComponentIcon(trajectoryPanel, Icons.getIcon("salsa.scanconfig.k.small"));
            } else if (currentConfig instanceof IConfigEnergy) {
                layoutValue = ConfigType.CONFIGENERGY.toString();
                scanEnergyController.setConfig(currentConfig);
                visibleComponents.add(scanEnergyView);
                tabbedPane.setComponentIcon(trajectoryPanel, Icons.getIcon("salsa.scanconfig.energy.small"));
            }
        }

        layout.show(trajectoryPanel, layoutValue);

        for (JComponent comp : visibleComponents) {
            comp.setVisible(true);
            comp.repaint();
            comp.validate();
        }

        repaintComponent();

    }

    private void repaintComponent() {
        Thread refreshThread = new Thread("repaint SalsaTrajectoryBean later") {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                    SwingUtilities.invokeLater(() -> {
                        repaint();
                        validate();
                    });
                } catch (InterruptedException e) {
                    LOGGER.error(e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            };
        };
        refreshThread.start();
    }

    @Override
    public void configPathChanged(final String configPath) {
    }

    @Override
    public void loadingConfigSucceed(final IConfig<?> config) {
    }

    @Override
    public void loadingConfigFailed(final String configPath, final SalsaException exception) {
    }

    @Override
    public void actionPerformed(final Action action) {
        for (ISalsaBeanListener listener : beanListeners) {
            if (listener instanceof ISalsaActionBeanListener) {
                ((ISalsaActionBeanListener) listener).actionPerformed(action);
            }
        }
    }

    @Override
    public void executedActionSucceed(final Action action) {
    }

    public boolean isShowError() {
        return showError;
    }

    public void setShowError(final boolean showError) {
        this.showError = showError;
    }

    @Override
    public void executedActionFailed(final Action action, final SalsaException exception) {
        if (showError) {
            JXErrorPane.showDialog(this, new ErrorInfo("Execution error", "Failed to execute " + action,
                    exception.getMessage(), "SalsaAPI", exception, Level.WARNING, null));
        }
    }

    private void hideAll() {
        tabbedPane2D.setVisible(false);
        scanXView.setVisible(false);
        scanYView.setVisible(false);
        scanEnergyView.setVisible(false);
        scanKView.setVisible(false);
        scan1DView.setVisible(false);
        layout.show(trajectoryPanel, ConfigType.NONE.toString());
        layout2.show(centerPanel, ConfigType.NONE.toString());
    }

    @Override
    protected void notifyConfigPathChanged(final String configPath) {
        super.notifyConfigPathChanged(configPath);
        configNameLabel.setText(configPath);
    }

    @Override
    protected void notifyLoadingConfigFailed(final String aconfigPath, final SalsaException e) {
        super.notifyLoadingConfigFailed(aconfigPath, e);
        configNameLabel.setText(aconfigPath);
        layout.show(trajectoryPanel, ConfigType.NONE.toString());
    }

    @Override
    public void loadConfig() {
        super.loadConfig();
        startButton.loadConfig();
        stopButton.loadConfig();
        pauseButton.loadConfig();
        resumeButton.loadConfig();
        if (SalsaUtils.isDefined(statusBar.getModel())) {
            statusBar.start();
        }

    }

    @Override
    public void unloadConfig() {
        super.unloadConfig();
        startButton.unloadConfig();
        stopButton.unloadConfig();
        pauseButton.unloadConfig();
        resumeButton.unloadConfig();
        statusBar.stop();
    }

    public void addSalsaActionBeanListener(final ISalsaActionBeanListener listener) {
        startButton.addSalsaBeanListener(listener);
        stopButton.addSalsaBeanListener(listener);
        pauseButton.addSalsaBeanListener(listener);
        resumeButton.addSalsaBeanListener(listener);
    }

    public void removeSalsaActionBeanListener(final ISalsaActionBeanListener listener) {
        startButton.removeSalsaBeanListener(listener);
        stopButton.removeSalsaBeanListener(listener);
        pauseButton.removeSalsaBeanListener(listener);
        resumeButton.removeSalsaBeanListener(listener);
    }

    public boolean isUserEnabled() {
        return userEnabled;
    }

    public void setUserEnabled(final boolean userEnabled) {
        this.userEnabled = userEnabled;
        startButton.setUserEnabled(userEnabled);
        setSensorsEditable(userEnabled);
        setActuatorsEditable(userEnabled);
        setTimebasesEditable(userEnabled);
        setHookEditable(userEnabled);
        setErrorStrategyEditable(userEnabled);
        setGeneralEditable(userEnabled);
    }

    public boolean isConfirmation() {
        return startButton.isConfirmation();
    }

    public void setConfirmation(final boolean confirmation) {
        startButton.setConfirmation(confirmation);
    }

    public boolean isSensorsEnable() {
        return sensorsEnable;
    }

    public void setSensorsEnable(final boolean enabled) {
        sensorsEnable = enabled;
        if (sensorsVisible) {
            tabbedPane.setComponentEnable(sensorPanel, sensorsEnable);
        }
    }

    public boolean isSensorsEditable() {
        return sensorPanel.isEditable();
    }

    public void setSensorsEditable(final boolean sensorsEditable) {
        sensorPanel.setEditable(sensorsEditable);
    }

    public boolean isSensorsVisible() {
        return sensorsVisible;
    }

    public void setSensorsVisible(final boolean visible) {
        if (sensorsVisible != visible) {
            this.sensorsVisible = visible;
            tabbedPane.setComponentVisible(sensorPanel, sensorsVisible);
        }
    }

    public boolean isGeneralEnable() {
        return generalPanel.isEnabled();
    }

    public void setGeneralEnable(final boolean enabled) {
        generalEnable = enabled;
        tabbedPane.setComponentEnable(generalPanel, generalEnable);
    }

    public boolean isGeneralEditable() {
        return generalPanel.isEditable();
    }

    public void setGeneralEditable(final boolean editable) {
        generalPanel.setEditable(editable);
    }

    public boolean isGeneralVisible() {
        return generalVisible;
    }

    public void setGeneralVisible(final boolean visible) {
        if (generalVisible != visible) {
            this.generalVisible = visible;
            tabbedPane.setComponentVisible(generalPanel, generalVisible);
        }
    }

    public boolean isHookEnable() {
        return hookEnable;
    }

    public void setHookEnable(final boolean enable) {
        hookEnable = enable;
        tabbedPane.setComponentEnable(hookView, hookEnable);
    }

    public boolean isHookEditable() {
        return hookView.isEditable();
    }

    public void setHookEditable(final boolean editable) {
        hookView.setEditable(editable);
    }

    public boolean isHookVisible() {
        return hookVisible;
    }

    public void setHookVisible(final boolean visible) {
        if (hookVisible != visible) {
            this.hookVisible = visible;
            tabbedPane.setComponentVisible(hookView, hookVisible);
        }
    }

    public boolean isTimebasesEnable() {
        return timebasesEnable;
    }

    public void setTimebasesEnable(final boolean enable) {
        timebasesEnable = enable;
        tabbedPane.setComponentEnable(timeBasePanel, timebasesEnable);
    }

    public boolean isTimebasesEditable() {
        return timeBasePanel.isEditable();
    }

    public void setTimebasesEditable(final boolean editable) {
        timeBasePanel.setEditable(editable);
    }

    public boolean isTimebasesVisible() {
        return timebasesVisible;
    }

    public void setTimebasesVisible(final boolean visible) {
        if (timebasesVisible != visible) {
            this.timebasesVisible = visible;
            tabbedPane.setComponentVisible(timeBasePanel, timebasesVisible);
        }
    }

    public boolean isActuatorEnable() {
        return actuatorsEnable;
    }

    public void setActuatorEnable(final boolean enable) {
        this.actuatorsEnable = enable;
        tabbedPane.setComponentEnable(actuatorPanel, actuatorsEnable);
    }

    public boolean isActuatorsEditable() {
        return actuatorPanel.isEditable();
    }

    public void setActuatorsEditable(final boolean editable) {
        actuatorPanel.setEditable(editable);
    }

    public boolean isActuatorsVisible() {
        return actuatorsVisible;
    }

    public void setActuatorsVisible(final boolean visible) {
        if (actuatorsVisible != visible) {
            this.actuatorsVisible = visible;
            tabbedPane.setComponentVisible(actuatorPanel, actuatorsVisible);
        }
    }

    public boolean isErrorStrategyEnable() {
        return errorStrategyEnable;
    }

    public void setErrorStrategyEnable(final boolean enable) {
        errorStrategyEnable = enable;
        tabbedPane.setComponentEnable(errorStrategyView, errorStrategyEnable);
    }

    public boolean isErrorStrategyEditable() {
        return errorStrategyView.isEditable();
    }

    public void setErrorStrategyEditable(final boolean editable) {
        errorStrategyView.setEditable(editable);
    }

    public boolean isErrorStrategyVisible() {
        return errorStrategyVisible;
    }

    public void setErrorStrategyVisible(final boolean visible) {
        if (errorStrategyVisible != visible) {
            this.errorStrategyVisible = visible;
            tabbedPane.setComponentVisible(errorStrategyView, errorStrategyVisible);
        }
    }

    public boolean isRecordDataButtonVisible() {
        return recordButton.isVisible();
    }

    public void setRecordDataButtonVisible(final boolean visible) {
        recordButton.setVisible(visible);
    }

    public boolean isReloadButtonVisible() {
        return reloadButton.isVisible();
    }

    public void setReloadButtonVisible(final boolean visible) {
        reloadButton.setVisible(visible);
    }

    /**
     * Main test
     *
     * @param args
     */
    public static void main(final String[] args) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        final SalsaTrajectoryBean bean = new SalsaTrajectoryBean();
        // bean.setUserEnabled(false);
        // bean.setSensorsEditable(false);
        bean.setSensorsVisible(false);
        bean.setGeneralVisible(false);
        bean.setHookVisible(false);
        bean.setHookVisible(true);
        bean.setSensorsVisible(true);
        panel.add(bean, BorderLayout.CENTER);
        BookmarkComboConfigList combo = new BookmarkComboConfigList();
        panel.add(combo, BorderLayout.SOUTH);
        combo.setBookmark("Liste");
        combo.loadBookmark();
        combo.addConfigSelectionListener(new IConfigSelectionListener() {

            @Override
            public void configPathChanged(final String configPath) {
                bean.setConfigPath(configPath);
            }
        });
        configureBeanWithoutConfig(bean);
        // configureBeanWithConfigAtLoad(bean);
        // configureBeanWithConfigAtNewScan(bean);
        // configureBeanWithConfigAtNewScanAndReload(bean);
        bean.loadConfig();
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("SalsaTrajectoryBean");
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
    }

    // Test without config
    public static void configureBeanWithoutConfig(final ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(false);
        bean.setCurrentConfigAtNewScan(false);
    }

    // Test with config at lod
    public static void configureBeanWithConfigAtLoad(final ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(false);
    }

    public static void configureBeanWithConfigAtNewScan(final ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(true);
    }

    public static void configureBeanWithConfigAtNewScanAndReload(final ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(true);
        bean.setReloadConfigAtRead(true);
    }

}

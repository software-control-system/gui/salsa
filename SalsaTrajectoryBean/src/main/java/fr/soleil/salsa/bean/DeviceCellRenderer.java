package fr.soleil.salsa.bean;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import fr.soleil.salsa.entity.IDevice;

/**
 * Custom renderer for the cells of the table. Identical to the default renderer except : - it
 * doesn't display which cell has the focus, - also, it displays the content of the cell in a
 * tooltip, - rows with type "ERROR" are displayed in red.
 */
public class DeviceCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 2052018194962928987L;
    private final JLabel errorComponent = new JLabel();

    public DeviceCellRenderer() {
        super();
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean isSelected, boolean hasFocus,
            int row, int col) {

        JComponent component = (JComponent) super.getTableCellRendererComponent(jTable, value, isSelected, hasFocus,
                row, col);

        if ((value instanceof IDevice) && !((IDevice) value).isCommon()) {
            if (isSelected) {
                component.setForeground(Color.RED);
            } else {
                errorComponent.setText(value.toString());
                errorComponent.setForeground(Color.RED);
                errorComponent.setHorizontalAlignment(SwingConstants.LEFT);
                component = errorComponent;
            }
        } else {
            component.setForeground(Color.BLACK);
        }
        return component;
    }
}

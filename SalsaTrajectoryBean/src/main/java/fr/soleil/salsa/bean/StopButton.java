package fr.soleil.salsa.bean;

import java.awt.Color;

import javax.swing.JFrame;

import fr.esrf.Tango.DevState;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.view.ActionButton.ACTION_TYPE;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class StopButton extends AbstractActionSalsaBean {

    private static final long serialVersionUID = -3073560620083478413L;

    protected IContext context = null;
    boolean actionEnable = true;

    public StopButton() {
        super(ACTION_TYPE.STOP);
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        context = SalsaAPI.extractContext(devicePreferences);
        setBackgroundButton(new Color(217, 1, 21));
        setEnabled(true);
    }

    @Override
    protected void executeAction(IConfig<?> config) throws SalsaException {
        if ((context != null) && actionEnable) {
            SalsaAPI.stopScan(context);
        }
    }

    @Override
    protected String getActionMessage() {
        String configPath = getConfigPath();
        String message = "No configuration is defined.";
        if (SalsaUtils.isDefined(configPath)) {
            message = ACTION_TYPE.STOP.toString() + " " + configPath + " configuration";
        }
        return message;
    }

    @Override
    public void stateChanged(String state) {
        super.stateChanged(state);
        // Let the stop button always enable JIRA SCAN-136
        if (DevState.STANDBY.toString().equals(state) || DevState.MOVING.toString().equals(state)
                || DevState.RUNNING.toString().equals(state)) {
            actionEnable = true;
        } else {
            actionEnable = false;
        }
    }

    @Override
    protected void setMovingState(boolean moving) {
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        StopButton bean = new StopButton();
        bean.setConfigPath("Katy/1DFast");
        bean.setConfirmation(true);
        frame.getContentPane().add(bean);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(bean.getClass().getName());
        frame.setVisible(true);
    }
}

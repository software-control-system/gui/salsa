package fr.soleil.salsa.bean;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.CurrentConfigurationParser;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.ICurrentScanResultListener;
import fr.soleil.model.scanserver.Trajectory;
import fr.soleil.salsa.configuration.controller.IConfigTreeController;
import fr.soleil.salsa.configuration.view.ConfigTreeView;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IActuatorTrajectory;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeIntegrated;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.ActuatorImpl;
import fr.soleil.salsa.entity.impl.ErrorStrategyType;
import fr.soleil.salsa.entity.impl.ScanAddOnImp;
import fr.soleil.salsa.entity.impl.SensorImpl;
import fr.soleil.salsa.entity.impl.TimebaseImpl;
import fr.soleil.salsa.entity.impl.scan1d.Config1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Dimension1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Range1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Trajectory1DImpl;
import fr.soleil.salsa.entity.impl.scan2d.Config2DImpl;
import fr.soleil.salsa.entity.impl.scan2d.Dimension2DXImpl;
import fr.soleil.salsa.entity.impl.scan2d.Dimension2DYImpl;
import fr.soleil.salsa.entity.impl.scan2d.Range2DXImpl;
import fr.soleil.salsa.entity.impl.scan2d.Range2DYImpl;
import fr.soleil.salsa.entity.impl.scan2d.Trajectory2DXImpl;
import fr.soleil.salsa.entity.impl.scan2d.Trajectory2DYImpl;
import fr.soleil.salsa.entity.impl.scanenergy.ConfigEnergyImpl;
import fr.soleil.salsa.entity.impl.scanenergy.DimensionEnergyImpl;
import fr.soleil.salsa.entity.impl.scanenergy.RangeEnergyImpl;
import fr.soleil.salsa.entity.impl.scanenergy.TrajectoryEnergyImpl;
import fr.soleil.salsa.entity.impl.scanhcs.ConfigHCSImpl;
import fr.soleil.salsa.entity.impl.scanhcs.DimensionHCSImpl;
import fr.soleil.salsa.entity.impl.scanhcs.RangeHCSImpl;
import fr.soleil.salsa.entity.impl.scanhcs.TrajectoryHCSImpl;
import fr.soleil.salsa.entity.impl.scank.ConfigKImpl;
import fr.soleil.salsa.entity.impl.scank.DimensionKImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.tool.SalsaUtils;

public class CurrentTrajectoryBean extends SalsaTrajectoryBean implements ICurrentScanResultListener {

    private static final long serialVersionUID = -5841232099214756914L;

    private final IConfig1D config1D = new Config1DImpl();
    private final IConfig2D config2D = new Config2DImpl();
    private final IConfigK configK = new ConfigKImpl();
    private final IConfigEnergy configEnergy = new ConfigEnergyImpl();
    private final IConfigHCS configHCS = new ConfigHCSImpl();
    private IConfig<?> currentConfig = null;
    private String scanServer = null;
    private final JCheckBox trajectoryEnable = new JCheckBox("Trajectory enable");
    private final JButton newConfig = new JButton("New");
    private final JButton applyConfig = new JButton("Apply");
    private boolean configurationExist = false;

    private IConfigTreeController treeController = null;

    private enum DeviceType {
        ACTUATOR, SENSOR, TIMEBASE
    };

    public CurrentTrajectoryBean(boolean result) {
        super(false);
        initConfig();
        setReloadConfigAtRead(false);
        setCurrentConfigAtLoad(true);
        setCurrentConfigAtNewScan(true);
        setReloadButtonVisible(false);

        JPanel southPanel = new JPanel();
        // Replace status bar by CheckBox
        southPanel.add(trajectoryEnable);
        add(southPanel, BorderLayout.SOUTH);

        trajectoryEnable.setToolTipText("Enable the trajectory selection in scan result");
        if (!result) {
            applyConfig.setToolTipText("Apply to the corresponding configuration");
            newConfig.setToolTipText("Create a new configuration from the current scan result");

            southPanel.add(applyConfig);
            southPanel.add(newConfig);
            applyConfig.setVisible(false);
            newConfig.setVisible(false);

            applyConfig.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    if ((currentConfig != null) && configurationExist && (treeController != null)) {
                        IConfig<?> selectedConfig = treeController.getConfig();
                        if ((selectedConfig == null)
                                || !ObjectUtils.sameObject(selectedConfig.getId(), currentConfig.getId())) {
                            {
                                ConfigTreeView view = treeController.getView();
                                if (view != null) {
                                    view.searchPath(currentConfig.getFullPath());
                                    selectedConfig = treeController.getConfig();
                                }
                            }

                        }
                        if (selectedConfig != null) {
                            selectedConfig = copyConfig(currentConfig, selectedConfig);
                            treeController.notifyListeners(selectedConfig);
                        }
                    }
                }
            });

            newConfig.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    if ((currentConfig != null) && !configurationExist) {
                        treeController.notifyNewConfig(currentConfig);
                    }
                }
            });
        }

        if (getDevicePreferences() != null) {
            scanServer = getDevicePreferences().getScanServer();
        }

        loadConfig();
    }

    private void initConfig() {
        // Init all the configuration
        IDimensionHCS dimensionHCS = configHCS.getDimensionX();
        if (dimensionHCS == null) {
            dimensionHCS = new DimensionHCSImpl();
            configHCS.setDimensionX(dimensionHCS);
        }

        IDimension2DX dimension2DX = config2D.getDimensionX();
        if (dimension2DX == null) {
            dimension2DX = new Dimension2DXImpl();
            config2D.setDimensionX(dimension2DX);
        }
        IDimension2DY dimension2DY = config2D.getDimensionY();
        if (dimension2DY == null) {
            dimension2DY = new Dimension2DYImpl();
            config2D.setDimensionY(dimension2DY);
        }

        IDimensionEnergy dimensionEnergy = configEnergy.getDimensionX();
        if (dimensionEnergy == null) {
            dimensionEnergy = new DimensionEnergyImpl();
            configEnergy.setDimensionX(dimensionEnergy);
        }

        IDimensionK dimensionK = configK.getDimensionX();
        if (dimensionK == null) {
            dimensionK = new DimensionKImpl();
            configK.setDimensionX(dimensionK);
        }

        IDimension1D dimension1D = config1D.getDimensionX();
        if (dimension1D == null) {
            dimension1D = new Dimension1DImpl();
            config1D.setDimensionX(dimension1D);
        }
    }

    public void setConfigTreeController(IConfigTreeController controller) {
        treeController = controller;
    }

    private void refreshButton() {
        // Must be done in EDT (SCAN-976, PROBLEM-2582)
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            applyConfig.setVisible(currentConfig != null);
            newConfig.setVisible((currentConfig != null) && !configurationExist);
        });
    }

    @Override
    public void startIndexSelectedChanged(int start) {
        pointSelected(start, -1, true);

    }

    @Override
    public void endIndexSelectedChanged(int end) {
        pointSelected(end, -1, false);

    }

    @Override
    public void startPointSelectedChanged(int startX, int startY) {
        pointSelected(startX, startY, true);
    }

    @Override
    public void endPointSelectedChanged(int endX, int endY) {
        pointSelected(endX, endY, false);

    }

    public void pointSelected(int xIndex, int yIndex, boolean start) {
        if (trajectoryEnable.isSelected() && (currentConfig != null)) {
            IDimension dimension = currentConfig.getDimensionX();
            setIndex(xIndex, dimension, start);

            if ((currentConfig instanceof IConfig2D) && (yIndex > -1)) {
                dimension = ((IConfig2D) currentConfig).getDimensionY();
                setIndex(yIndex, dimension, start);
            }
        }
    }

    private void setIndex(int index, IDimension dimension, boolean start) {
        if ((dimension != null) && trajectoryEnable.isSelected()) {
            List<? extends IRange> rangeList = dimension.getRangeList();
            if (rangeList != null) {
                List<ITrajectory> trajectoryList = null;
                double[] trajectoryValues = null;
                double[] newTrajectoryValues = null;
                for (IRange range : rangeList) {
                    trajectoryList = range.getTrajectoriesList();
                    if (trajectoryList != null) {
                        for (ITrajectory trajectory : trajectoryList) {
                            trajectoryValues = trajectory.getTrajectory();
                            if ((trajectoryValues != null) && (index >= 0) && (index < trajectoryValues.length)) {
                                if (start) {
                                    // Copy from index to the end
                                    newTrajectoryValues = new double[trajectoryValues.length - index];
                                    System.arraycopy(trajectoryValues, index, newTrajectoryValues, 0,
                                            newTrajectoryValues.length);
                                } else {
                                    // Copy from 0 until index
                                    newTrajectoryValues = new double[index + 1];
                                    System.arraycopy(trajectoryValues, 0, newTrajectoryValues, 0,
                                            newTrajectoryValues.length);
                                    trajectory.setTrajectory(newTrajectoryValues);
                                }
                                trajectory.setTrajectory(newTrajectoryValues);
                            }
                        }
                    }
                }
                super.notifyLoadingConfigSucceed(currentConfig);
            }
        }
    }

    @Override
    protected void notifyLoadingConfigSucceed(IConfig<?> config) {
        if (currentConfig != config) {
            currentConfig = parseConfig(config);
            if (config != null) {
                currentConfig = copyConfig(config, currentConfig);
                configurationExist = true;
            } else {
                configurationExist = false;
            }
            super.setConfig(currentConfig);
            refreshButton();
            super.notifyLoadingConfigSucceed(currentConfig);
        }
    }

    @Override
    protected void notifyLoadingConfigFailed(String aconfigPath, SalsaException e) {
        currentConfig = parseConfig(null);
        configurationExist = false;
        if (currentConfig != null) {
            super.setConfig(currentConfig);
            refreshButton();
            super.notifyLoadingConfigSucceed(currentConfig);
        } else {
            super.notifyLoadingConfigFailed(aconfigPath, e);
        }
    }

    @SuppressWarnings("unchecked")
    private IConfig<?> parseConfig(IConfig<?> adestConfig) {

        IConfig<?> newConfig = null;
        IDimension dimensionX = null;
        IDimension dimensionY = null;

        if ((adestConfig instanceof IConfigHCS) || CurrentConfigurationParser.isHCS(scanServer)) {
            newConfig = configHCS;
        } else if (((adestConfig != null) && (adestConfig instanceof IConfig2D))
                || CurrentConfigurationParser.is2DScan(scanServer)) {
            newConfig = config2D;
            dimensionY = config2D.getDimensionY();
        } else if (((adestConfig != null) && (adestConfig instanceof IConfigEnergy))
                || CurrentConfigurationParser.isEnergy(scanServer)) {
            newConfig = configEnergy;
        } else {
            newConfig = config1D;
        }

        if ((newConfig != null) && (adestConfig == null)) {
            dimensionX = newConfig.getDimensionX();

            newConfig.setName(CurrentConfigurationParser.getConfigurationName(scanServer));
            newConfig.setZigzag(CurrentConfigurationParser.isZigZag(scanServer));
            newConfig.setOnTheFly(CurrentConfigurationParser.isOnTheFly(scanServer));
            newConfig.setEnableScanSpeed(CurrentConfigurationParser.isEnableScanSpeed(scanServer));

            // currentConfig.setScanAddOn(sourceConfig.getScanAddOn());
            newConfig.setActuatorsDelay(CurrentConfigurationParser.readActuatorDelay(scanServer));
            newConfig.setScanNumber(CurrentConfigurationParser.readScanNumber(scanServer));

            String[] sensors = CurrentConfigurationParser.readCurrentSensors(scanServer);
            List<ISensor> sensorList = newConfig.getSensorsList();
            sensorList = (List<ISensor>) initDeviceList(sensorList, sensors, DeviceType.SENSOR);
            newConfig.setSensorsList(sensorList);

            String[] timeBases = CurrentConfigurationParser.readCurrentTimeBases(scanServer);
            List<ITimebase> timeBaseList = newConfig.getTimebaseList();
            timeBaseList = (List<ITimebase>) initDeviceList(timeBaseList, timeBases, DeviceType.TIMEBASE);
            newConfig.setTimebaseList(timeBaseList);

            String[] actuatorsX = CurrentConfigurationParser.readCurrentActuators(scanServer, 1);
            List<IActuator> actuatorList = newConfig.getDimensionX().getActuatorsList();
            actuatorList = (List<IActuator>) initDeviceList(actuatorList, actuatorsX, DeviceType.ACTUATOR);
            newConfig.getDimensionX().setActuatorsList(actuatorList);

            // Set the X trajectories
            initTrajectories(1, dimensionX, actuatorList);

            if (newConfig instanceof IConfig2D) {
                String[] actuatorsY = CurrentConfigurationParser.readCurrentActuators(scanServer, 2);
                List<IActuator> actuatorList2 = ((IConfig2D) newConfig).getDimensionY().getActuatorsList();
                actuatorList2 = (List<IActuator>) initDeviceList(actuatorList2, actuatorsY, DeviceType.ACTUATOR);
                ((IConfig2D) newConfig).getDimensionY().setActuatorsList(actuatorList2);
                // Set the Y trajectories
                initTrajectories(2, dimensionY, actuatorList2);
            }

            // ScanAddOn
            IScanAddOns scanAddOns = newConfig.getScanAddOn();
            if (scanAddOns == null) {
                scanAddOns = new ScanAddOnImp();
            }

            IErrorStrategy errorStrategy = scanAddOns.getErrorStrategy();
            String contextValidation = CurrentConfigurationParser.readContextValidation(scanServer);
            if (SalsaUtils.isDefined(contextValidation)) {
                errorStrategy.setContextValidationDevice(contextValidation);
                int strategy = CurrentConfigurationParser.readErrorStrategy(scanServer,
                        CurrentScanDataModel.CONTEXT_VALIDATION);
                errorStrategy.setContextValidationStrategy(getErrorStrategyType(strategy));
            }

            String[] categoriesStr = new String[] { "actuators", "sensors", "timebases", "hooks" };
            IErrorStrategyItem[] categoriesESI = new IErrorStrategyItem[] { errorStrategy.getActuatorsErrorStrategy(),
                    errorStrategy.getSensorsErrorStrategy(), errorStrategy.getTimebasesErrorStrategy(),
                    errorStrategy.getHooksErrorStrategy() };
            String cat = null;
            IErrorStrategyItem item = null;
            for (int i = 0; i < categoriesStr.length; i++) {
                cat = categoriesStr[i];
                item = categoriesESI[i];
                double timeOut = CurrentConfigurationParser.readTimeOut(scanServer, cat);
                int retryCount = CurrentConfigurationParser.readRetryCount(scanServer, cat);
                double retryTimeOut = CurrentConfigurationParser.readRetryTimeOut(scanServer, cat);
                int strategy = CurrentConfigurationParser.readErrorStrategy(scanServer, cat);

                item.setTimeOut(timeOut);
                item.setRetryCount(retryCount);
                item.setTimeBetweenRetries(retryTimeOut);
                item.setStrategy(getErrorStrategyType(strategy));
            }
        }
        return newConfig;
    }

    private ErrorStrategyType getErrorStrategyType(int value) {
        ErrorStrategyType type = ErrorStrategyType.IGNORE;
        switch (value) {
            case 1:
                type = ErrorStrategyType.PAUSE;
                break;
            case 2:
                type = ErrorStrategyType.ABORT;
                break;
        }
        return type;
    }

    private void initTrajectories(int index, IDimension dimension, List<IActuator> actuatorList) {
        if (dimension != null) {
            try {
                List<Trajectory> trajectoryList = CurrentConfigurationParser.getCurrentTrajectory(scanServer, index);
                if (SalsaUtils.isFulfilled(trajectoryList)) {
                    IRange range = null;
                    ITrajectory configTrajectory = null;
                    Trajectory trajectory = null;
                    for (int i = 0; i < trajectoryList.size(); i++) {
                        trajectory = trajectoryList.get(i);
                        range = createRange(dimension);
                        configTrajectory = range.getTrajectoriesList().get(i);
                        if ((configTrajectory instanceof IActuatorTrajectory) && (actuatorList != null)
                                && (actuatorList.size() == trajectoryList.size())) {
                            ((IActuatorTrajectory) configTrajectory).setActuator(actuatorList.get(i));
                        }
                        range.setStepsNumber(trajectory.getStepNumber());
                        if (range instanceof IRangeIntegrated) {
                            ((IRangeIntegrated) range)
                                    .setIntegrationTime(CurrentConfigurationParser.readIntegrationTime(scanServer));
                        }
                        configTrajectory.setBeginPosition(trajectory.getFrom());
                        configTrajectory.setEndPosition(trajectory.getTo());
                        configTrajectory.setTrajectory(trajectory.getCompleteTrajectory());
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    private IRange createRange(IDimension dimension) {
        IRange range = null;
        ITrajectory trajectory = null;
        if (dimension != null) {
            if (dimension instanceof IDimensionHCS) {
                range = new RangeHCSImpl();
                trajectory = new TrajectoryHCSImpl();
                ((IDimensionHCS) dimension).getRangesXList().add((IRangeHCS) range);
            } else if (dimension instanceof IDimension2DX) {
                range = new Range2DXImpl();
                trajectory = new Trajectory2DXImpl();
                ((IDimension2DX) dimension).getRangesList().add((IRange2DX) range);
            } else if (dimension instanceof IDimension2DY) {
                range = new Range2DYImpl();
                trajectory = new Trajectory2DYImpl();
                ((IDimension2DY) dimension).getRangesList().add((IRange2DY) range);
            } else if (dimension instanceof IDimensionEnergy) {
                range = new RangeEnergyImpl();
                trajectory = new TrajectoryEnergyImpl();
                ((IDimensionEnergy) dimension).getRangesEnergyList().add((IRangeEnergy) range);
            } else if (dimension instanceof IDimension1D) {
                range = new Range1DImpl();
                trajectory = new Trajectory1DImpl();
                ((IDimension1D) dimension).getRangesXList().add((IRange1D) range);
            }
        }
        if ((range != null) && (trajectory != null)) {
            trajectory.setIRange(range);
            range.getTrajectoriesList().add(trajectory);
            range.setDimension(dimension);
        }
        return range;

    }

    @SuppressWarnings("unchecked")
    private List<? extends IDevice> initDeviceList(List<? extends IDevice> deviceList, String[] values,
            DeviceType deviceType) {

        List<? extends IDevice> returnDeviceList = deviceList;
        if ((values != null) && (values.length > 0)) {
            if (deviceList == null) {
                switch (deviceType) {
                    case ACTUATOR:
                        returnDeviceList = new ArrayList<IActuator>();
                        break;
                    case SENSOR:
                        returnDeviceList = new ArrayList<ISensor>();
                        break;
                    case TIMEBASE:
                        returnDeviceList = new ArrayList<ITimebase>();
                        break;
                    default:
                        returnDeviceList = new ArrayList<IDevice>();
                        break;
                }
            }

            if (deviceList != null) {
                deviceList.clear();
            }

            IDevice deviceImp = null;
            for (String deviceName : values) {
                deviceImp = null;
                switch (deviceType) {
                    case ACTUATOR:
                        deviceImp = new ActuatorImpl();
                        ((List<IActuator>) returnDeviceList).add((IActuator) deviceImp);
                        break;
                    case SENSOR:
                        deviceImp = new SensorImpl();
                        ((List<ISensor>) returnDeviceList).add((ISensor) deviceImp);
                        break;
                    case TIMEBASE:
                        deviceImp = new TimebaseImpl();
                        ((List<ITimebase>) returnDeviceList).add((ITimebase) deviceImp);
                        break;
                    default:
                        break;
                }
                if (deviceImp != null) {
                    deviceImp.setName(deviceName);
                    deviceImp.setScanServerAttributeName(scanServer);
                    deviceImp.setEnabled(true);
                }
            }

        }
        return returnDeviceList;
    }

    private IConfig<?> copyConfig(IConfig<?> sourceConfig, IConfig<?> adestConfig) {
        IConfig<?> destConfig = adestConfig;
        if (sourceConfig != null) {
            if ((destConfig == null) || ((destConfig != null)
                    && (destConfig.getClass().getName().equals(sourceConfig.getClass().getName())))) {
                if (sourceConfig instanceof IConfig1D) {
                    if (destConfig == null) {
                        destConfig = config1D;
                    }
                    ((IConfig1D) destConfig).setDimensionX(((IConfig1D) sourceConfig).getDimensionX());
                } else if (sourceConfig instanceof IConfig2D) {
                    if (destConfig == null) {
                        destConfig = config2D;
                    }
                    ((IConfig2D) destConfig).setDimensionX(((IConfig2D) sourceConfig).getDimensionX());
                    ((IConfig2D) destConfig).setDimensionY(((IConfig2D) sourceConfig).getDimensionY());
                } else if (sourceConfig instanceof IConfigK) {
                    if (destConfig == null) {
                        destConfig = configK;
                    }
                    ((IConfigK) destConfig).setDimensionX(((IConfigK) sourceConfig).getDimensionX());
                } else if (sourceConfig instanceof IConfigEnergy) {
                    if (destConfig == null) {
                        destConfig = configK;
                    }
                    ((IConfigEnergy) destConfig).setDimensionX(((IConfigEnergy) sourceConfig).getDimensionX());
                } else if (sourceConfig instanceof IConfigHCS) {
                    if (destConfig == null) {
                        destConfig = configHCS;
                    }
                    ((IConfigHCS) destConfig).setDimensionX(((IConfigHCS) sourceConfig).getDimensionX());
                }
            }
        }

        if ((destConfig != null) && (sourceConfig != null)) {
            destConfig.setName(sourceConfig.getName());
            destConfig.setDirectory(sourceConfig.getDirectory());
            destConfig.setZigzag(sourceConfig.isZigzag());
            destConfig.setOnTheFly(sourceConfig.isOnTheFly());
            destConfig.setEnableScanSpeed(sourceConfig.isEnableScanSpeed());
            destConfig.setTimebaseList(sourceConfig.getTimebaseList());
            destConfig.setSensorsList(sourceConfig.getSensorsList());
            destConfig.setScanAddOn(sourceConfig.getScanAddOn());
            destConfig.setActuatorsDelay(sourceConfig.getActuatorsDelay());
            destConfig.setScanNumber(sourceConfig.getScanNumber());
        } else {
            destConfig = parseConfig(adestConfig);
        }

        return destConfig;

    }

    @Override
    public boolean isSelectionActivated() {
        return trajectoryEnable.isSelected();
    }
}

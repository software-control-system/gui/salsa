package fr.soleil.salsa.bean;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.lib.project.swing.panel.VisibilityTabbedPane;
import fr.soleil.salsa.client.controller.IGenericDeviceListListener;
import fr.soleil.salsa.client.controller.impl.GenericActuatorListController;
import fr.soleil.salsa.client.controller.impl.GenericSensorListController;
import fr.soleil.salsa.client.controller.impl.GenericTimebaseListController;
import fr.soleil.salsa.client.view.GenericDeviceListView;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.impl.ConfigManager;
import fr.soleil.salsa.tool.SalsaUtils;

public class ConfigurationManagerBean extends JPanel {

    private static final long serialVersionUID = -728800867288254039L;

    private static final String ACTUATORX_LABEL = "ActuatorX";
    private static final String ACTUATORY_LABEL = "ActuatorY";
    private static final String SENSOR_LABEL = "Sensors";
    private static final String TIMEBASE_LABEL = "Timebases";
    private static final String NOCONFIG = "NOCONFIG";
    private static final String CONFIGEXIST = "CONFIGEXIST";

    private final ConfigManager configManager = new ConfigManager();

    // Layout
    CardLayout layout = new CardLayout();

    // Controller
    private final GenericSensorListController sensorListController = new GenericSensorListController();
    private final GenericTimebaseListController timeBaseListController = new GenericTimebaseListController();
    private final GenericActuatorListController actuatorXController = new GenericActuatorListController();
    private final GenericActuatorListController actuatorYController = new GenericActuatorListController();

    private final VisibilityTabbedPane tabbedPane = new VisibilityTabbedPane();
    private GenericDeviceListView sensorPanel = null;
    private GenericDeviceListView timeBasePanel = null;
    private GenericDeviceListView actuatorXPanel = null;
    private GenericDeviceListView actuatorYPanel = null;

    private final JPanel noConfigPanel = new JPanel();
    private final JLabel noConfiguration = new JLabel("Please select at least 2 scan configurations");

    public ConfigurationManagerBean() {
        // Init panel
        IGenericDeviceListListener sensorListener = new IGenericDeviceListListener() {

            @Override
            public void deviceRenamed(final String oldName, final String newName) {
                configManager.renameSensor(oldName, newName);
            }

            @Override
            public void deviceRemoved(final String device) {
                configManager.deleteSensor(device);
            }

            @Override
            public void deviceEnabled(final String device, final boolean enabled) {
                configManager.setSensorEnable(device, enabled);
            }

            @Override
            public void deviceAdded(final String device) {
                configManager.addSensor(device);
                sensorListController.setDeviceList(configManager.getSensorsList());
            }

            @Override
            public void devicesAdded(final List<String> devices) {
                if (devices != null) {
                    for (String device : devices) {
                        configManager.addSensor(device);
                    }
                    sensorListController.setDeviceList(configManager.getSensorsList());
                }
            }

            @Override
            public void deviceSwap(final String device1, final String device2) {
                configManager.swapSensor(device1, device2);
            }
        };

        sensorListController.setManagerMode(true);
        sensorListController.addGenericDeviceListListener(sensorListener);
        sensorPanel = sensorListController.getView();

        IGenericDeviceListListener timeBaseListener = new IGenericDeviceListListener() {

            @Override
            public void deviceRenamed(final String oldName, final String newName) {
                configManager.renameTimeBase(oldName, newName);
            }

            @Override
            public void deviceRemoved(final String device) {
                configManager.deleteTimeBase(device);
            }

            @Override
            public void deviceEnabled(final String device, final boolean enabled) {
                configManager.setTimeBaseEnable(device, enabled);
            }

            @Override
            public void deviceAdded(final String device) {
                configManager.addTimeBase(device);
                timeBaseListController.setDeviceList(configManager.getTimebaseList());
            }

            @Override
            public void devicesAdded(final List<String> devices) {
                if (devices != null) {
                    for (String device : devices) {
                        configManager.addTimeBase(device);
                    }
                    timeBaseListController.setDeviceList(configManager.getTimebaseList());
                }
            }

            @Override
            public void deviceSwap(final String device1, final String device2) {
                configManager.swapTimeBase(device1, device2);
            }
        };

        timeBaseListController.setManagerMode(true);
        timeBaseListController.addGenericDeviceListListener(timeBaseListener);
        timeBasePanel = timeBaseListController.getView();

        IGenericDeviceListListener actuatorXListener = new IGenericDeviceListListener() {

            @Override
            public void deviceRenamed(final String oldName, final String newName) {
                configManager.renameActuator(oldName, newName, IConfig.FIRST_DIMENSION);
            }

            @Override
            public void deviceRemoved(final String device) {
                configManager.deleteActuator(device, IConfig.FIRST_DIMENSION);
            }

            @Override
            public void deviceEnabled(final String device, final boolean enabled) {
                configManager.setActuatorEnable(device, enabled, IConfig.FIRST_DIMENSION);
            }

            @Override
            public void deviceAdded(final String device) {
                configManager.addActuator(device, IConfig.FIRST_DIMENSION);
                List<IDimension> dimensionList = configManager.getDimensionList();
                IDimension tmpDimension = null;
                if (SalsaUtils.isFulfilled(dimensionList)) {
                    tmpDimension = dimensionList.get(0);
                    actuatorXController.setDeviceList(tmpDimension.getActuatorsList());
                }
            }

            @Override
            public void devicesAdded(final List<String> devices) {
                if (devices != null) {
                    for (String device : devices) {
                        configManager.addActuator(device, IConfig.FIRST_DIMENSION);
                    }
                    List<IDimension> dimensionList = configManager.getDimensionList();
                    IDimension tmpDimension = null;
                    if (SalsaUtils.isFulfilled(dimensionList)) {
                        tmpDimension = dimensionList.get(0);
                        actuatorXController.setDeviceList(tmpDimension.getActuatorsList());
                    }
                }
            }

            @Override
            public void deviceSwap(final String device1, final String device2) {
                configManager.swapActuator(device1, device2, IConfig.FIRST_DIMENSION);
            }

        };

        actuatorXController.setManagerMode(true);
        actuatorXController.addGenericDeviceListListener(actuatorXListener);
        actuatorXPanel = actuatorXController.getView();

        IGenericDeviceListListener actuatorYListener = new IGenericDeviceListListener() {

            @Override
            public void deviceRenamed(final String oldName, final String newName) {
                configManager.renameActuator(oldName, newName, IConfig.SECOND_DIMENSION);
            }

            @Override
            public void deviceRemoved(final String device) {
                configManager.deleteActuator(device, IConfig.SECOND_DIMENSION);
            }

            @Override
            public void deviceEnabled(final String device, final boolean enabled) {
                configManager.setActuatorEnable(device, enabled, IConfig.SECOND_DIMENSION);
            }

            @Override
            public void deviceAdded(final String device) {
                configManager.addActuator(device, IConfig.SECOND_DIMENSION);
                List<IDimension> dimensionList = configManager.getDimensionList();
                IDimension tmpDimension = null;
                if ((dimensionList != null) && (dimensionList.size() > 1)) {
                    tmpDimension = dimensionList.get(1);
                    actuatorYController.setDeviceList(tmpDimension.getActuatorsList());
                }
            }

            @Override
            public void devicesAdded(final List<String> devices) {
                if (devices != null) {
                    for (String device : devices) {
                        configManager.addActuator(device, IConfig.SECOND_DIMENSION);
                    }
                    List<IDimension> dimensionList = configManager.getDimensionList();
                    IDimension tmpDimension = null;
                    if ((dimensionList != null) && (dimensionList.size() > 1)) {
                        tmpDimension = dimensionList.get(1);
                        actuatorYController.setDeviceList(tmpDimension.getActuatorsList());
                    }
                }
            }

            @Override
            public void deviceSwap(final String device1, final String device2) {
                configManager.swapActuator(device1, device2, IConfig.SECOND_DIMENSION);
            }
        };

        actuatorYController.setManagerMode(true);
        actuatorYController.addGenericDeviceListListener(actuatorYListener);
        actuatorYPanel = actuatorYController.getView();

        noConfigPanel.setLayout(new BorderLayout());
        noConfigPanel.add(noConfiguration, BorderLayout.CENTER);
        noConfiguration.setForeground(Color.RED);
        noConfiguration.setHorizontalAlignment(JLabel.CENTER);
        Font font = noConfiguration.getFont();
        if (font != null) {
            noConfiguration.setFont(font.deriveFont((float) 30));
        }
        setLayout(layout);

        tabbedPane.add(TIMEBASE_LABEL, timeBasePanel);
        tabbedPane.add(SENSOR_LABEL, sensorPanel);
        tabbedPane.add(ACTUATORX_LABEL, actuatorXPanel);
        tabbedPane.add(ACTUATORY_LABEL, actuatorYPanel);

        tabbedPane.setComponentIcon(timeBasePanel, Icons.getIcon("salsa.scanconfig.timebase"));
        tabbedPane.setComponentIcon(sensorPanel, Icons.getIcon("salsa.scanconfig.sensor"));
        tabbedPane.setComponentIcon(actuatorXPanel, Icons.getIcon("salsa.xtrajectory"));
        tabbedPane.setComponentIcon(actuatorYPanel, Icons.getIcon("salsa.ytrajectory"));

        tabbedPane.setComponentVisible(actuatorXPanel, false);
        tabbedPane.setComponentVisible(actuatorYPanel, false);

        add(noConfigPanel, NOCONFIG);
        add(tabbedPane, CONFIGEXIST);

        layout.show(this, NOCONFIG);

    }

    public void setConfigList(final List<IConfig<?>> aconfigList) {
        configManager.setConfigList(aconfigList);
        refreshPanel();
    }

    private void refreshPanel() {
        if (configManager.getConfigList().size() < 2) {
            sensorListController.setDeviceList(new ArrayList<IDevice>(0));
            timeBaseListController.setDeviceList(new ArrayList<IDevice>(0));
            actuatorXController.setDeviceList(new ArrayList<IDevice>(0));
            actuatorYController.setDeviceList(new ArrayList<IDevice>(0));
            tabbedPane.setComponentVisible(actuatorXPanel, false);
            tabbedPane.setComponentVisible(actuatorYPanel, false);
            // Display message in order to select at least 2 config
            layout.show(this, NOCONFIG);
        } else {
            sensorListController.setDeviceList(configManager.getSensorsList());
            timeBaseListController.setDeviceList(configManager.getTimebaseList());
            List<IDimension> dimensionList = configManager.getDimensionList();
            IDimension tmpDimension = null;
            for (int i = 0; i < dimensionList.size(); i++) {
                tmpDimension = dimensionList.get(i);
                if (i == 0) {
                    actuatorXController.setDeviceList(tmpDimension.getActuatorsList());
                    tabbedPane.setComponentVisible(actuatorXPanel, true);
                } else {
                    actuatorYController.setDeviceList(tmpDimension.getActuatorsList());
                    tabbedPane.setComponentVisible(actuatorYPanel, true);
                }
            }
            layout.show(this, CONFIGEXIST);
        }
    }

}

package fr.soleil.salsa.bean;

import javax.swing.JFrame;

import fr.esrf.Tango.DevState;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.view.ActionButton.ACTION_TYPE;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class StartButton extends AbstractActionSalsaBean {

    private static final long serialVersionUID = -3416069205891116942L;

    protected IContext context = null;

    public StartButton() {
        super(ACTION_TYPE.START);
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        context = SalsaAPI.extractContext(devicePreferences);
    }

    @Override
    protected void executeAction(IConfig<?> config) throws SalsaException {
        if ((config != null) && (context != null)) {
            SalsaAPI.startScan(config, context);
        }
    }

    @Override
    protected String getActionMessage() {
        String configPath = getConfigPath();
        String message = "No configuration is defined.";
        if (SalsaUtils.isDefined(configPath)) {
            message = ACTION_TYPE.START.toString() + " " + configPath + " configuration";
        }
        return message;
    }

    @Override
    public void setConfig(IConfig<?> aconfig) {
        super.setConfig(aconfig);
        setEnabled(aconfig != null);
    }

    @Override
    public void stateChanged(String state) {
        super.stateChanged(state);
        if (getConfig() != null) {
            if (DevState.MOVING.toString().equals(state) || DevState.RUNNING.toString().equals(state)
                    || DevState.STANDBY.toString().equals(state)) {
                setEnabled(false);
            } else {
                // All the other state if a config is defined including UNKNOWN state
                // see bug 0026352
                setEnabled(true);
            }
        }
    }

    @Override
    protected void setMovingState(boolean moving) {
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        StartButton bean = new StartButton();
        bean.setConfigPath("Katy/1DFast");
        bean.setConfirmation(true);
        frame.getContentPane().add(bean);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(bean.getClass().getName());
        frame.setVisible(true);
    }
}

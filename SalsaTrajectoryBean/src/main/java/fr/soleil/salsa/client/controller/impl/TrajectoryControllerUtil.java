package fr.soleil.salsa.client.controller.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.salsa.api.util.TrajectoryCalculator;
import fr.soleil.salsa.client.view.ITrajectoryView;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeIntegrated;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.ActuatorModel;
import fr.soleil.salsa.entity.impl.scan1d.Trajectory1DImpl;
import fr.soleil.salsa.entity.impl.scan2d.Trajectory2DXImpl;
import fr.soleil.salsa.entity.impl.scan2d.Trajectory2DYImpl;
import fr.soleil.salsa.entity.impl.scanenergy.TrajectoryEnergyImpl;
import fr.soleil.salsa.entity.impl.scanhcs.TrajectoryHCSImpl;
import fr.soleil.salsa.entity.impl.scank.TrajectoryKImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Controller for the view showing the list of configurations as a tree.
 *
 * @author SAINTIN
 *
 */
public class TrajectoryControllerUtil {

    public static ITrajectory getTrajectory(final IConfig<?> config, final int rangePosition, final String actuatorName,
            final boolean yActuator) {
        ITrajectory trajectory = null;
        IRange range = getRange(config, rangePosition, yActuator);
        if (range != null) {
            IDimension dimension = config.getDimensionX();

            if (yActuator && (config instanceof IConfig2D)) {
                dimension = ((IConfig2D) config).getDimensionY();
            }
            List<IActuator> actuatorList = dimension.getActuatorsList();
            IActuator actuator = null;
            int actuatorPosition = 0;
            if ((actuatorName != null) && SalsaUtils.isFulfilled(actuatorList)) {
                for (int i = 0; i < actuatorList.size(); i++) {
                    actuator = actuatorList.get(i);
                    if ((actuator != null) && actuator.getName().equalsIgnoreCase(actuatorName)) {
                        actuatorPosition = i;
                        break;
                    }
                }
            }

            List<ITrajectory> trajectoryList = range.getTrajectoriesList();
            if ((trajectoryList != null) && (trajectoryList.size() > actuatorPosition)) {
                trajectory = trajectoryList.get(actuatorPosition);
            }
        }
        return trajectory;
    }

    public static IRange getRange(final IConfig<?> config, final int rangePosition, final boolean yActuator) {
        IRange range = null;
        if (config != null) {
            List<? extends IRange> rangeList = TrajectoryCalculator.getRangeList(config, yActuator);
            if ((rangeList != null) && (rangePosition > -1) && (rangeList.size() > rangePosition)) {
                range = rangeList.get(rangePosition);
            }
        }
        return range;
    }

    public static void deleteRange(final IConfig<?> config, final boolean yActuator) {
        if (config != null) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            if (dimension != null) {
                List<? extends IRange> rangeList = dimension.getRangeList();

                // There must be at least one range
                if ((rangeList != null) && (rangeList.size() > 1)) {
                    IRange range = rangeList.remove(rangeList.size() - 1);
                    if (range != null) {
                        range.getTrajectoriesList().clear();
                        config.setModified(true);
                    }
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }
        }
    }

    /**
     * If begin position change we update data
     *
     * @param beginPosition
     */
    public static void computeBeginPositionChange(final IConfig<?> config, final String beginPosition,
            final String actuatorName, final int rangePosition, final boolean deltaLock, final boolean yActuator) {
        Double doubleValue = getValueFrom(beginPosition);
        if ((config != null) && (doubleValue != null)) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                double oldValue = t1.getBeginPosition();
                if (oldValue != doubleValue) {
                    t1.setBeginPosition(doubleValue);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }
        }
    }

    /**
     * If end position change we update data
     *
     * @param endPosition
     */
    public static void computeEndPositionChange(final IConfig<?> config, final String endPosition,
            final String actuatorName, final int rangePosition, final boolean deltaLock, final boolean yActuator) {

        Double doubleValue = getValueFrom(endPosition);

        if ((config != null) && (doubleValue != null)) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                double oldValue = t1.getEndPosition();
                if (oldValue != doubleValue) {
                    t1.setEndPosition(doubleValue);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }

        }
    }

    public static void customTrajectoryChange(final IConfig<?> config, final boolean custom, final String actuatorName,
            final int rangePosition, final boolean yActuator) {
        if (config != null) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                boolean oldValue = t1.isCustomTrajectory();
                if (oldValue != custom) {
                    t1.setCustomTrajectory(custom);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }

        }
    }

    public static void customTrajectoryValueChange(final IConfig<?> config, final double[] values,
            final String actuatorName, final int rangePosition, final boolean yActuator) {
        if (config != null) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                double[] oldValue = t1.getTrajectory();
                String oldValueString = Arrays.toString(oldValue);
                String newValueString = Arrays.toString(values);
                if (!oldValueString.equals(newValueString)) {
                    t1.setTrajectory(values);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }

        }
    }

    /**
     * If integration time change we update data
     *
     * @param integrationtime
     */
    public static void computeIntegrationTimeChange(final IConfig<?> config, final double[] integrationTime,
            final int rangePosition, final boolean yActuator) {

        if ((config != null) && (integrationTime != null)) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            IRange range = getRange(config, rangePosition, yActuator);

            if (range instanceof IRangeIntegrated) {
                IRangeIntegrated intergratedRange = (IRangeIntegrated) range;
                double[] oldValue = intergratedRange.getIntegrationTime();
                if (!ArrayUtils.equals(oldValue, integrationTime)) {
                    ((IRangeIntegrated) range).setIntegrationTime(integrationTime);
                    config.setModified(true);
                }

            }
            TrajectoryCalculator.setDimension(config, dimension);

        }
    }

    /**
     * If step number change we update data
     *
     * @param StepsNumbe
     */
    public static void computeStepsNumberChange(final IConfig<?> config, final Integer stepsNumber,
            final int rangePosition, final boolean yActuator) {
        if (config != null) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            IRange range = getRange(config, rangePosition, yActuator);
            int oldValue = range.getStepsNumber();
            if (oldValue != stepsNumber) {
                range.setStepsNumber(stepsNumber);
                config.setModified(true);
            }
            TrajectoryCalculator.setDimension(config, dimension);
        }
    }

    /**
     * If step number change we update data
     *
     * @param delta
     */
    public static void computeDeltaChange(final IConfig<?> config, final String delta, final String actuatorName,
            final int rangePosition, final boolean yActuator) {

        Double doubleValue = getValueFrom(delta);
        if ((config != null) && (doubleValue != null)) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                double oldValue = t1.getDelta();
                if (oldValue != doubleValue) {
                    t1.setDelta(doubleValue);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }
        }
    }

    /**
     *
     * @param relative
     */
    public static void computeRelativeChange(final IConfig<?> config, final Boolean relative, final String actuatorName,
            final int rangePosition, final boolean yActuator) {
        if (config != null) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                boolean oldValue = t1.getRelative();
                if (oldValue != relative) {
                    t1.setRelative(relative);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }
        }
    }

    /**
     *
     * @param delta
     */
    public static void computeDeltaConstantChange(final IConfig<?> config, final Boolean deltaConstant,
            final String actuatorName, final int rangePosition, final boolean yActuator) {
        if (config != null) {
            IDimension dimension = TrajectoryCalculator.getDimension(config, yActuator);
            ITrajectory t1 = getTrajectory(config, rangePosition, actuatorName, yActuator);
            if (t1 != null) {
                boolean oldValue = t1.isDeltaConstant();
                if (oldValue != deltaConstant) {
                    t1.setDeltaConstant(deltaConstant);
                    config.setModified(true);
                }
                TrajectoryCalculator.setDimension(config, dimension);
            }
        }
    }

    public static void setEnableScanSpeed(final IConfig<?> config, final Boolean value) {
        if (config != null) {
            boolean oldValue = config.isEnableScanSpeed();
            if (oldValue != value) {
                config.setEnableScanSpeed(value);
                config.setModified(true);
            }
        }
    }

    public static void setOnTheFly(final IConfig<?> config, final Boolean value) {
        if (config != null) {
            boolean oldValue = config.isOnTheFly();
            if (oldValue != value) {
                config.setOnTheFly(value);
                config.setModified(true);
            }
        }
    }

    public static List<IActuator> actuatorDiff(final List<IActuator> l1, final List<IActuator> l2) {
        List<IActuator> result = new ArrayList<>();
        List<IActuator> biggerList = l1;
        List<IActuator> smallerList = l2;

        if (l1.size() < l2.size()) {
            biggerList = l2;
            smallerList = l1;
        }

        ListIterator<IActuator> listIterator = biggerList.listIterator();
        while (listIterator.hasNext()) {
            IActuator actuator = listIterator.next();
            if ((actuator != null) && (!myContains(smallerList, actuator))) {
                result.add(actuator);
            }
        }
        return result;
    }

    /**
     * Return if the list contains the object o.
     *
     * @param l
     * @param o
     * @return
     */
    public static boolean myContains(final List<IActuator> l, final IActuator o) {
        boolean contain = false;
        if ((l != null) && (l != null)) {
            for (IActuator o1 : l) {
                if (o1.getName().equalsIgnoreCase(o.getName())) {
                    contain = true;
                    break;
                }
            }
        }

        return contain;
    }

    public static ITrajectory createTrajectory(final IConfig<?> config, final boolean yActuator) {
        ITrajectory trajectory = null;
        if (config != null) {
            if (config instanceof IConfig1D) {
                trajectory = new Trajectory1DImpl();
                ((ITrajectory1D) trajectory).setDeltaConstant(false);

            } else if (config instanceof IConfig2D) {
                if (yActuator) {
                    trajectory = new Trajectory2DYImpl();
                    ((ITrajectory2DY) trajectory).setDeltaConstant(false);
                } else {
                    trajectory = new Trajectory2DXImpl();
                    ((ITrajectory2DX) trajectory).setDeltaConstant(false);
                }
            } else if (config instanceof IConfigEnergy) {
                trajectory = new TrajectoryEnergyImpl();
            } else if (config instanceof IConfigHCS) {
                trajectory = new TrajectoryHCSImpl();
                ((ITrajectoryHCS) trajectory).setDeltaConstant(false);
            } else if (config instanceof IConfigK) {
                trajectory = new TrajectoryKImpl();
                ITrajectoryK trajectoryKModel = (ITrajectoryK) trajectory;

                // Pre Edge Parameters
                trajectoryKModel.setEMin(0d);
                trajectoryKModel.setEDeltaPreEdge(0d);
                trajectoryKModel.setTimePreEdge(0d);

                // Edge Parameters
                trajectoryKModel.setE0(0d);
                trajectoryKModel.setE1(0d);
                trajectoryKModel.setE2(0d);
                trajectoryKModel.setEDeltaEdge(0d);
                trajectoryKModel.setTimeEdge(0d);

                // Connexion Parameter
                trajectoryKModel.setM(0d);

                // Post Edge Parameters
                trajectoryKModel.setKMin(0d);
                trajectoryKModel.setKMax(0d);
                trajectoryKModel.setKDelta(0d);
                trajectoryKModel.setTimePostEdge(0d);
                trajectoryKModel.setN(0d);

                // Estimates
                trajectoryKModel.setDeadTime(0d);
            }
        }
        if (trajectory != null) {
            initBasicTrajectory(trajectory);
        }
        return trajectory;
    }

    public static void initBasicTrajectory(final ITrajectory trajectory) {
        trajectory.setBeginPosition(0.0);
        trajectory.setEndPosition(0.0);
        trajectory.setDelta(0.0);
        trajectory.setRelative(false);
        trajectory.setSpeed(0.0);
    }

    public static void setDeviceEnable(final IConfig<?> config, final String device, final boolean activate,
            final boolean yActuator) {
        if ((config != null) && (device != null)) {
            config.setActuatorEnable(device, activate, (yActuator) ? 2 : 1);
        }
    }

    public static void refresh(final ITrajectoryView view, final IConfig<?> config, final IDimension dimension,
            final boolean saved) {

        if ((view != null) && (config != null) && (dimension != null)) {
            List<? extends IRange> rangeList = dimension.getRangeList();
            List<IActuator> actuatorList = dimension.getActuatorsList();
            if (rangeList != null) {
                IRange range = null;
                for (int i = 0; i < rangeList.size(); i++) {
                    range = rangeList.get(i);
                    if (range instanceof IRangeIntegrated) {
                        double[] integrationTime = ((IRangeIntegrated) range).getIntegrationTime();
                        if (integrationTime != null) {
                            view.setIntegrationTime(i, integrationTime[0] + "", saved);
                        }
                    }
                    view.setStepNumber(i, range.getStepsNumber() + "", saved);

                    List<ITrajectory> trajectoryList = range.getTrajectoriesList();
                    if ((trajectoryList != null) && (actuatorList != null)
                            && (actuatorList.size() <= trajectoryList.size())) {
                        ITrajectory t = null;
                        IActuator actuator = null;
                        String actuatorName = null;
                        for (int j = 0; j < trajectoryList.size(); j++) {
                            if (j < actuatorList.size()) {
                                actuator = actuatorList.get(j);
                                actuatorName = actuator.getName();
                                t = trajectoryList.get(j);
                                view.setBeginPosition(actuatorName, i, t.getBeginPosition() + "", saved);
                                view.setEndPosition(actuatorName, i, t.getEndPosition() + "", saved);
                                view.setDelta(actuatorName, i, t.getDelta() + "", saved);
                                if (!saved) {
                                    view.setRelative(actuatorName, i, t.getRelative());
                                    view.setDeltaconstant(actuatorName, i, t.isDeltaConstant());
                                    view.setCustomTrajectory(actuatorName, i, t.isCustomTrajectory());
                                    view.setCustomTrajectoryValues(actuatorName, i, t.getTrajectory());
                                    view.setSpeed(actuatorName, i, t.getSpeed() + "", false);
                                }
                            }
                        }
                    }
                }
            }
            if (!saved) {
                view.setOnTheFlyEnabled(!((config instanceof IConfigHCS) && (config instanceof IConfigK)));
                // Enable the checkbox
                view.setEnableActuatorSpeedControlEnabled(config.isOnTheFly() || (config instanceof IConfigHCS));
                // Select the checkbox
                view.setEnableSpeedControl(config.isEnableScanSpeed());
                view.setOnTheFly(config.isOnTheFly());

            }
        }
    }

    /**
     * Returns elements that are contained in list l1 and not in list l2
     *
     * @param l1
     * @param l2
     * @return
     */
    public static List<IActuator> diff(final List<ActuatorModel> l1, final List<IActuator> l2) {
        List<IActuator> result = new ArrayList<>();
        for (int i = 0; i < l1.size(); i++) {

            ActuatorModel a = l1.get(i);

            if (!myContains(l2, a)) {
                result.add(a);
            }
        }
        return result;
    }

    private static Double getValueFrom(final String stringValue) {
        Double value = null;
        if (SalsaUtils.isDefined(stringValue)) {
            try {
                value = Double.valueOf(stringValue);
            } catch (NumberFormatException e) {
            }
        }
        return value;
    }
}

package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.AbstractTableModel;

import fr.soleil.salsa.client.util.CustomDataValue;

public class CustomDataTexteArea extends JDialog {

    private static final long serialVersionUID = -6868701340686743289L;
    private static CustomDataTexteArea instance = null;
    private static JPanel mainPanel = new JPanel();
    private static JPanel textAreaPanel = new JPanel();
    private static CustomTextArea textArea = new CustomTextArea();
    private static JButton applyButton = new JButton("Apply");
    private static JButton closeButton = new JButton("Close");
    private static JTable lineNumber = new JTable();
    private static CustomDataValue value = null;

    private static AbstractTableModel tableModel = new AbstractTableModel() {

        private static final long serialVersionUID = 4807669709436264035L;

        @Override
        public Object getValueAt(int row, int col) {
            return row;
        }

        @Override
        public int getRowCount() {
            if (value != null && value.getCustomData() != null) {
                return value.getCustomData().length;
            }
            return 0;
        }

        @Override
        public int getColumnCount() {
            return 1;
        }
    };

    private CustomDataTexteArea() {
        setTitle("Trajectory TextArea Editor");
    }

    private static void initGUI() {
        textAreaPanel.setLayout(new BorderLayout());
        textAreaPanel.add(textArea, BorderLayout.CENTER);
        textAreaPanel.add(lineNumber, BorderLayout.WEST);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setSize(400, 600);
        JScrollPane scrollPane = new JScrollPane(textAreaPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(applyButton);
        buttonPanel.add(closeButton);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        applyButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (value != null) {
                    double[] doubleValues = parseValue();
                    value.setCustomData(doubleValues);
                    setText();
                }
            }
        });

        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                instance.setVisible(false);
            }
        });

        lineNumber.setEnabled(false);
        lineNumber.setBackground(Color.LIGHT_GRAY);
        lineNumber.setModel(tableModel);
        lineNumber.setRowHeight(textArea.getRowSize());
    }

    public static void setCustomTrajectoryValue(CustomDataValue newValue) {
        value = newValue;
        setText();
    }

    private static void setText() {
        if (value != null && value.getCustomData() != null) {
            double[] values = value.getCustomData();
            StringBuilder builder = new StringBuilder();
            for (double val : values) {
                builder.append(val);
                builder.append("\n");
            }
            textArea.setText(builder.toString());
            tableModel.fireTableStructureChanged();
        }
    }

    private static double[] parseValue() {
        double[] doubleValues = null;
        String text = textArea.getText();
        // FIRST REPLACE ALL SEPARATOR WITH \n
        if (text != null) {
            // FIRST REPLACE ALL SEPARATOR WITH \n
            text = text.replaceAll(";", "\n");
            text = text.replaceAll(",", "\n");
            text = text.replaceAll("\t", "\n");

            // Then replace double \n by one \n
            text = text.replaceAll("\n\n", "\n");

            // Split the text with \n
            String[] stringValue = text.split("\n");

            if (stringValue != null && stringValue.length > 0) {
                doubleValues = new double[stringValue.length];
                double doubleValue = Double.NaN;
                String aString = null;
                for (int i = 0; i < stringValue.length; i++) {
                    aString = stringValue[i];
                    doubleValue = Double.NaN;
                    if (aString != null) {
                        try {
                            doubleValue = Double.parseDouble(aString.trim());
                        } catch (Exception e) {
                        }
                    }
                    doubleValues[i] = doubleValue;
                }
            }
        }

        return doubleValues;
    }

    public static void showFrame(Component parent) {
        if (instance == null) {
            instance = new CustomDataTexteArea();
            initGUI();
            instance.setContentPane(mainPanel);
            instance.setSize(mainPanel.getSize());
            instance.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            instance.setLocationRelativeTo(parent);
            instance.setAlwaysOnTop(true);
            instance.setModal(true);
        }

        instance.setVisible(true);
        instance.toFront();

    }

    public static void main(String[] args) {
        CustomDataValue customValue = new CustomDataValue();
        double[] doubleValue = new double[1000];
        for (int i = 0; i < doubleValue.length; i++) {
            doubleValue[i] = Math.sin(i);
        }
        customValue.setCustomData(doubleValue);
        CustomDataTexteArea.showFrame(null);
        CustomDataTexteArea.setCustomTrajectoryValue(customValue);
    }

    protected static class CustomTextArea extends JTextArea {
        private static final long serialVersionUID = 6976816801564523265L;

        public int getRowSize() {
            return getRowHeight();
        }
    }
}

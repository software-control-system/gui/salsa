package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.IView;
import fr.soleil.salsa.entity.IActuator;

public interface ITrajectoryListController<T extends IView<?>> extends IController<T> {

    /**
     * Notifies adding a new range action.
     */
    public void notifyNewRangeAction();

    /**
     * Notifies deleting a range action.
     */
    public void notifyDeleteRangeAction();

    /**
     * Notifies enabling actuator speed control.
     */
    public void notifyEnableActuatorSpeedControlAction(Boolean value);

    /**
     * Notifies on the fly option action.
     */
    public void notifyOnTheFlyAction(Boolean value);

    /**
     * Notifies adding a new actuator
     */
    public void notifyAddActuator(IActuator actuatorName);

    /**
     * Notifies deleting an actuator
     */
    public void notifyDeleteActuator(IActuator actuatorName);

    /**
     * Notifies integration time changes.
     */
    public void notifyIntegrationTimeChange(double[] value, int jPosition);

    /**
     * Notifies number of steps changes.
     */
    public void notifyNumberOfStepsChange(String value, int jPosition);

    /**
     * Notifies begin position value changes.
     */
    public void notifyFromValueChanged(String value, int rangePosition, String actuatorName, boolean deltaLock);

    /**
     * Notifies end position value changes.
     */
    public void notifyToValueChanged(String value, int rangePosition, String actuatorName, boolean deltaLock);

    /**
     * Notifies delta value changes.
     */
    public void notifyDeltaValueChanged(String value, int rangePosition, String actuatorName);

    /**
     * Notifies relative value changes.
     */
    public void notifyRelativeChanged(boolean value, int rangePosition, String actuatorName);

    /**
     * Notifies delta constant value changes.
     */
    public void notifyDeltaConstantChanged(boolean value, int rangePosition, String actuatorName);

    public void setDeviceEnable(String device, boolean enable);

    /**
     * Notifies that the trajectory became a custom trajectory
     */
    public void notifyCustomTrajectoryChanged(boolean value, int rangePosition, String actuatorName);

    /**
     * Notifies that the trajectory became a custom trajectory
     */
    public void notifyCustomTrajectoryValueChanged(double[] value, int rangePosition, String actuatorName);

}

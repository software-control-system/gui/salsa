package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;
import fr.soleil.lib.project.swing.TextTansfertHandler;
import fr.soleil.salsa.client.controller.ITrajectoryListController;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.impl.ActuatorImpl;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Contains all trajectories GUI elements.
 * 
 * @author Administrateur
 * 
 */

public class TrajectoryListView extends JPanel implements IView<ITrajectoryListController<?>>, ITrajectoryView {

    private static final long serialVersionUID = 7374862974684733630L;

    // GUI Elements
    private final Font littleFont = new Font(Font.SANS_SERIF, Font.PLAIN, 10);

    /**
     * The 1D controller.
     */
    private ITrajectoryListController<?> trajectoryController;

    /**
     * Adding a new range button.
     */
    private JButton newJButton;

    /**
     * Deleting a range button.
     */
    private JButton deleteJButton;

    /**
     * ToolBar containing New and Delete JButtons.
     */
    private JToolBar toolBar;

    /**
     * The center scrollPane.
     */
    private JScrollPane centerJScrollPane;

    /**
     * Panel containing fly and speed JCheckBox.
     */
    private JPanel southPanel;

    /**
     * Speed control JCheckBox .
     */
    private JCheckBox speedCheckBox;

    /**
     * On the fly JCheckBox
     */
    private JCheckBox flyCheckBox;

    // rangeIndex , trajectoryPanel column
    private final Map<Integer, JPanel> trajectoryPanels = new HashMap<Integer, JPanel>();

    /**
     * List of ranges.
     */
    private final List<RangeView> rangeList = new ArrayList<RangeView>();

    /**
     * List of devices names.
     */
    private final List<IDevice> devicesName = new ArrayList<IDevice>();

    /**
     * Center panel containing RangeView and DimensionView GUI elements.
     */
    private JPanel centerPanel;

    /**
     * Panel that contains New and Delete range buttons.
     */
    private JPanel topPanel;

    /**
     * Panel that contains actuators names.
     */
    private JPanel leftPanel;

    /**
     * Indicates if this is X trajectories or Y ones.
     */
    private boolean yTrajectory = false;

    /**
     * Indicates if this is HCS trajectories or not.
     */
    private boolean hcsTrajectory = false;

    private boolean activateFunction = false;

    /**
     * Containes parameters values used for HCS Scan.
     */
    private RangeView hcsRangeView = null;

    /**
     * Constructor
     */
    public TrajectoryListView() {
        this(null);
    }

    /**
     * Constructor.
     * 
     * @param controller
     */
    public TrajectoryListView(ITrajectoryListController<?> controller) {
        this.trajectoryController = controller;
        this.initialize();
    }

    public TrajectoryListView(ITrajectoryListController<?> controller, boolean activateFunction) {
        this.activateFunction = activateFunction;
        this.trajectoryController = controller;
        this.initialize();
    }

    /**
     * Initialization of the components of the trajectory view.
     */
    public void initialize() {
        this.setLayout(new BorderLayout());
        this.add(this.getToolBar(), BorderLayout.NORTH);
        this.add(this.getCenterScrollPane(), BorderLayout.CENTER);
        this.add(this.getSouthJPanel(), BorderLayout.SOUTH);
        enableControl(false);
    }

    @Override
    public ITrajectoryListController<?> getController() {
        return trajectoryController;
    }

    @Override
    public void setController(ITrajectoryListController<?> controller) {
        this.trajectoryController = controller;
        for (RangeView rangeView : rangeList) {
            rangeView.setTrajectoryController(controller);
        }
    }

    /**
     * Gets New JButton.
     * 
     * @return
     */
    private JButton getNewJButton() {
        if (this.newJButton == null) {
            newJButton = new JButton();
            newJButton.setEnabled(false);
            newJButton.setText("New");
            newJButton.setToolTipText("New Range");
            newJButton.setPreferredSize(new Dimension(100, 25));
            newJButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (trajectoryController != null) {
                        createRange();
                        trajectoryController.notifyNewRangeAction();
                        refreshPanelsSize();
                        refreshButtonEnable();
                    }
                }
            });
            newJButton.setVisible(!activateFunction);
        }
        return newJButton;
    }

    /**
     * Gets Delete JButton.
     * 
     * @return
     */
    private JButton getDeleteJButton() {
        if (this.deleteJButton == null) {
            deleteJButton = new JButton();
            deleteJButton.setEnabled(false);
            deleteJButton.setText("Delete");
            deleteJButton.setToolTipText("Delete Range");
            deleteJButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (trajectoryController != null) {
                        deleteRange();
                        trajectoryController.notifyDeleteRangeAction();
                    }
                }
            });
            deleteJButton.setVisible(!activateFunction);
        }
        return deleteJButton;
    }

    /**
     * Gets toolBar containing New and Delete JButtons
     * 
     * @return
     */
    private JToolBar getToolBar() {
        if (this.toolBar == null) {
            toolBar = new JToolBar();
            toolBar.setFloatable(false);
            toolBar.add(getNewJButton());
            toolBar.addSeparator();
            toolBar.add(getDeleteJButton());
            toolBar.addSeparator();
        }
        return toolBar;
    }

    /**
     * Gets center scrollPane
     * 
     * @return
     */
    private JScrollPane getCenterScrollPane() {
        if (this.centerJScrollPane == null) {

            centerPanel = new JPanel();
            // centerPanel.setLayout(null);
            centerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
            // centerPanel.setBackground(Color.MAGENTA);
            centerJScrollPane = new JScrollPane(centerPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            centerJScrollPane.setColumnHeaderView(getTopPanel());
            centerJScrollPane.setRowHeaderView(getLeftPanel());
            centerJScrollPane.getRowHeader().setVisible(false);

        }
        return centerJScrollPane;
    }

    /**
     * Gets speed control JCheckBox.
     * 
     * @return
     */
    private JCheckBox getSpeedCheckBox() {
        if (this.speedCheckBox == null) {
            speedCheckBox = new JCheckBox();
            speedCheckBox.setText("Enable actuator speed control");
            speedCheckBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    setEnableSpeedControl(speedCheckBox.isSelected());
                    if (trajectoryController != null) {
                        trajectoryController.notifyEnableActuatorSpeedControlAction(speedCheckBox.isSelected());
                    }
                }
            });
        }
        return speedCheckBox;
    }

    /**
     * Gets fly JCheckBox.
     * 
     * @return
     */
    private JCheckBox getFlyCheckBox() {
        if (this.flyCheckBox == null) {
            flyCheckBox = new JCheckBox();
            flyCheckBox.setText("On the fly");
            flyCheckBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (trajectoryController != null) {
                        trajectoryController.notifyOnTheFlyAction(flyCheckBox.isSelected());
                        trajectoryController.notifyEnableActuatorSpeedControlAction(flyCheckBox.isSelected());
                    }
                }
            });
        }
        return flyCheckBox;
    }

    /**
     * Gets south panel containing "On the fly" JCheckBox and
     * "Enable actuator speed control" JCheckBox
     * 
     * @return
     */
    private JPanel getSouthJPanel() {
        if (this.southPanel == null) {
            southPanel = new JPanel();
            southPanel.setLayout(new GridBagLayout());
            if (!this.hcsTrajectory) {
                southPanel.add(getFlyCheckBox());
            }
            southPanel.add(getSpeedCheckBox());
        }
        return southPanel;
    }

    /**
     * 
     * @return
     */
    private JPanel getTopPanel() {
        if (this.topPanel == null) {
            topPanel = new JPanel();
            topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        }
        return topPanel;
    }

    /**
     * 
     * @return
     */
    private JPanel getLeftPanel() {
        if (this.leftPanel == null) {
            leftPanel = new JPanel();
            leftPanel.setLayout(new FlowLayout());
            leftPanel.setPreferredSize(new Dimension(250, 90));
        }
        return leftPanel;
    }

    private RangeView getHCSRangeView() {
        if (hcsRangeView == null) {
            hcsRangeView = new RangeView(trajectoryController);
            hcsRangeView.setHCSEnabled(true);
        }
        return hcsRangeView;
    }

    /**
     * Adds a range.
     */
    @Override
    public void addRange(int nbRange) {
        if (rangeList.size() != nbRange) {
            for (int i = 0; i < nbRange; i++) {
                createRange();
            }
            refreshPanelsSize();
            refreshButtonEnable();
        }
    }

    private void createRange() {
        RangeView rangeView = new RangeView(this.trajectoryController, this.rangeList.size());
        rangeView.setYRangeEnabled(yTrajectory);
        rangeView.setHCSEnabled(hcsTrajectory);
        this.rangeList.add(rangeView);
        getTopPanel().add(rangeView);
        addTrajectoryToRange(rangeView);
    }

    /**
     * Deletes a range.
     */
    private void deleteRange() {
        if (this.rangeList.size() > 1) {
            RangeView range = this.rangeList.get(rangeList.size() - 1);
            this.rangeList.remove(range);
            removeTrajectoryPanel(range.getRangePosition());
            topPanel.remove(range);
            topPanel.revalidate();
            topPanel.repaint();
            refreshPanelsSize();
            refreshButtonEnable();
        }
    }

    private void refreshButtonEnable() {
        // If it is a timeScan
        int nbRange = rangeList.size();
        getDeleteJButton().setEnabled(nbRange > 1);
        if (hcsTrajectory || !SalsaUtils.isFulfilled(devicesName)) {
            getNewJButton().setEnabled(nbRange < 1);
        } else {
            getNewJButton().setEnabled(true);
        }
    }

    /**
     * Adds a trajectory to each range.
     */
    private void addTrajectory(IDevice device) {
        if (SalsaUtils.isFulfilled(rangeList) && (device != null)) {
            TrajectoryView trajectoryView = null;
            for (int rangePosition = 0; rangePosition < rangeList.size(); rangePosition++) {
                RangeView rangeView = this.rangeList.get(rangePosition);
                trajectoryView = new TrajectoryView(this.trajectoryController, device.getName(), rangePosition,
                        this.hcsTrajectory);
                rangeView.addTrajectory(trajectoryView);
                addTrajectoryPanel(trajectoryView, rangePosition);
            }
        }
    }

    private void refreshPanelsSize() {
        int totalWidth = 0;
        int totalHeight = 0;
        // Calculate the totalWidth number of range * TrajectoryView.WIDTH
        if (SalsaUtils.isFulfilled(rangeList)) {
            int nbRange = rangeList.size();
            totalWidth = TrajectoryView.WIDTH * nbRange + 30;

            // Set top panel size (range view panel)
            getTopPanel().setSize(totalWidth, RangeView.HEIGHT + 5);
            getTopPanel().setPreferredSize(topPanel.getSize());
            getTopPanel().revalidate();
            getTopPanel().repaint();

        }

        // Calculate the totalHeight number of enable device * TrajectoryView.HEIGHT
        if (SalsaUtils.isFulfilled(devicesName)) {
            int nbEnableDevice = 0;
            for (IDevice device : devicesName) {
                if (device.isEnabled()) {
                    nbEnableDevice++;
                }
            }
            totalHeight = nbEnableDevice * TrajectoryView.HEIGHT + 10;

            // Set left panel size (actuator name panel)
            getLeftPanel().setSize(230, totalHeight);
            getLeftPanel().setPreferredSize(getLeftPanel().getSize());
            getLeftPanel().revalidate();
            getLeftPanel().repaint();

            // Set column trajectoryPanel
            Collection<JPanel> panels = trajectoryPanels.values();
            for (JPanel panel : panels) {
                panel.setSize(TrajectoryView.WIDTH, totalHeight);
                panel.setPreferredSize(panel.getSize());
                panel.revalidate();
                panel.repaint();
            }

            // Set center panel size (trajectory panel)
            centerPanel.setSize(totalWidth, totalHeight);
            centerPanel.setPreferredSize(centerPanel.getSize());
            centerPanel.revalidate();
            centerPanel.repaint();
        }

        getCenterScrollPane().getRowHeader().setVisible(SalsaUtils.isFulfilled(devicesName));
        getCenterScrollPane().revalidate();
        getCenterScrollPane().repaint();
    }

    private void addTrajectoryPanel(TrajectoryView view, int rangeIndex) {
        JPanel trajectoryPanel = trajectoryPanels.get(rangeIndex);
        if (trajectoryPanel == null) {
            trajectoryPanel = new JPanel();
            // trajectoryPanel.setBackground(Color.CYAN);
            trajectoryPanel.setLayout(new VerticalFlowLayout());
            centerPanel.add(trajectoryPanel);
            trajectoryPanels.put(rangeIndex, trajectoryPanel);
        }
        trajectoryPanel.add(view);
    }

    private void removeTrajectoryPanel(TrajectoryView view, int rangeIndex) {
        JPanel trajectoryPanel = trajectoryPanels.get(rangeIndex);
        if (trajectoryPanel != null) {
            trajectoryPanel.remove(view);
            // TODO maybe remove panel if empty
//            if (trajectoryPanel.getComponentCount() == 0) {
//                centerPanel.remove(trajectoryPanel);
//                trajectoryPanels.remove(rangeIndex);
//            }
        }
    }

    private void removeTrajectoryPanel(int rangeIndex) {
        JPanel trajectoryPanel = trajectoryPanels.get(rangeIndex);
        if (trajectoryPanel != null) {
            centerPanel.remove(trajectoryPanel);
            trajectoryPanel.removeAll();
            trajectoryPanels.remove(rangeIndex);
        }
    }

    /**
     * Adds trajectory to one range.
     * 
     * @param range
     */
    private void addTrajectoryToRange(RangeView range) {
        for (IDevice device : devicesName) {
            if (device.isEnabled()) {
                TrajectoryView trajectoryView = new TrajectoryView(this.trajectoryController, device.getName(),
                        range.getRangePosition(), this.hcsTrajectory);
                range.addTrajectory(trajectoryView);
                addTrajectoryPanel(trajectoryView, range.getRangePosition());
            }
        }
    }

    /**
     * Adds a device(actuator) to the list of actuators.
     * 
     * @param actuatorName
     * @param addTrajectories
     */
    @Override
    public void addDevice(List<IActuator> actuatorList) {
        if (SalsaUtils.isFulfilled(actuatorList) && !SalsaUtils.isFulfilled(devicesName)) {
            for (IActuator actuator : actuatorList) {
                addSingleDevice(actuator);
            }
            refreshPanelsSize();
            refreshButtonEnable();
        }
    }

    @Override
    public void addDevice(IActuator actuator) {
        addSingleDevice(actuator);
        refreshPanelsSize();
        refreshButtonEnable();
    }

    private void addSingleDevice(IActuator actuator) {
        if ((actuator != null) && actuator.isEnabled()) {
            devicesName.add(actuator);
            String deviceName = actuator.getName();
            JComponent deviceNameComponent = new JLabel(deviceName);
            String model = TangoDeviceHelper.getDeviceName(deviceName);
            TextTansfertHandler.registerTransferHandler(deviceNameComponent, model);
            deviceNameComponent.setFont(littleFont);
            deviceNameComponent.setToolTipText(deviceName);
            deviceNameComponent.setPreferredSize(new Dimension(230, TrajectoryView.HEIGHT));
            leftPanel.add(deviceNameComponent);
            addTrajectory(actuator);
        }
    }

    /**
     * Add a device (actuator) to the list of devices.
     */
    @Override
    public void deleteDevice(IActuator actuator) {
        IDevice device = null;
        if (actuator != null) {
            for (int position = 0; position < devicesName.size(); position++) {
                device = devicesName.get(position);
                if (device.getName().equalsIgnoreCase(actuator.getName())) {
                    leftPanel.remove(position);
                    devicesName.remove(position);
                    TrajectoryView view = null;
                    RangeView rangeView = null;
                    for (int i = 0; i < this.rangeList.size(); i++) {
                        rangeView = this.rangeList.get(i);
                        view = rangeView.getTrajectoryForIndex(position);
                        rangeView.removeTrajectoryForIndex(position);
                        removeTrajectoryPanel(view, i);
                    }
                    refreshPanelsSize();
                    break;
                }
            }
        }
    }

    /**
     * Gets the number of steps at the specified j position.
     * 
     * @param j
     * @return
     */
    public String getStepNumber(int j) {
        if (this.rangeList.size() > j) {
            return this.rangeList.get(j).getStepNumber();
        }
        return "0";
    }

    private TrajectoryView getViewForDevice(String actuatorName, int rangePosition) {
        TrajectoryView view = null;
        if ((rangePosition > -1) && (rangePosition < this.rangeList.size())) {
            RangeView rangeView = this.rangeList.get(rangePosition);
            view = rangeView.getTrajectoryForActuator(actuatorName);
        }
        return view;
    }

    @Override
    public void setBeginPosition(String actuatorName, int rangePosition, String value, boolean saved) {
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setBeginPosition(value, saved);
        }
    }

    @Override
    public void setEndPosition(String actuatorName, int rangePosition, String value, boolean saved) {
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setEndPosition(value, saved);
        }
    }

    @Override
    public void setStepNumber(int rangePosition, String value, boolean saved) {
        if (!isHCSTrajectory()) {
            if (this.rangeList.size() > rangePosition) {
                this.rangeList.get(rangePosition).setStepNumber(value, saved);
            }
        } else {
            getHCSRangeView().setStepNumber(value, saved);
        }
    }

    @Override
    public void setIntegrationTime(int rangePosition, String value, boolean saved) {
        if (!isHCSTrajectory()) {
            if (this.rangeList.size() > rangePosition) {
                this.rangeList.get(rangePosition).setIntegrationTime(value, saved);
            }
        } else {
            getHCSRangeView().setIntegrationTime(value, saved);
        }
    }

    @Override
    public void setDelta(String actuatorName, int rangePosition, String value, boolean saved) {
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setDelta(value, saved);
        }
    }

    @Override
    public void setSpeed(String actuatorName, int rangePosition, String value, boolean saved) {
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setSpeed(value, saved);
        }
    }

    @Override
    public void setCustomTrajectory(String actuatorName, int rangePosition, boolean value) {
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setCustomTrajectory(value);
        }
    }

    @Override
    public void setCustomTrajectoryValues(String actuatorName, int rangePosition, double[] values) {
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setCustomTrajectoryValues(values);
        }
    }

    @Override
    public void setRelative(String actuatorName, int rangePosition, Boolean value) {
        if (value == null) {
            value = new Boolean(false);
        }
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setRelative(value);
        }
    }

    @Override
    /**
     * Sets delta constant value at the specified i and j position.
     * 
     * @param iPosition
     * @param jPosition
     * @param value
     */
    public void setDeltaconstant(String actuatorName, int rangePosition, Boolean value) {
        if (value == null) {
            value = new Boolean(false);
        }
        TrajectoryView view = getViewForDevice(actuatorName, rangePosition);
        if (view != null) {
            view.setDeltaConstant(value);
        }
    }

    /**
     * Deletes all trajectories views.
     */
    @Override
    public void clearTrajectoriesView() {
        if (!SwingUtilities.isEventDispatchThread()) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    clearView();
                }
            };
            SwingUtilities.invokeLater(runnable);
        } else {
            clearView();
        }
    }

    private void clearView() {
        // Clean graphical object
        leftPanel.removeAll();
        centerPanel.removeAll();
        topPanel.removeAll();
        Collection<JPanel> panels = trajectoryPanels.values();
        for (JPanel panel : panels) {
            panel.removeAll();
            panel.setVisible(false);
        }

        // Clean functional object
        devicesName.clear();
        for (RangeView rangeView : rangeList) {
            rangeView.clearValues();
            rangeView.setVisible(false);
        }
        rangeList.clear();
        trajectoryPanels.clear();

        refreshPanelsSize();
        getDeleteJButton().setEnabled(false);
        getNewJButton().setEnabled(false);
    }

    /**
     * Sets actuator name value.
     * 
     * @param oldName
     * @param newName
     */
    @Override
    public void setActuatorName(String oldName, String newName) {
        if (devicesName != null) {
            Component[] components = getLeftPanel().getComponents();
            if (components != null) {
                JLabel deviceLabel = null;
                for (Component comp : components) {
                    if (comp instanceof JLabel) {
                        deviceLabel = (JLabel) comp;
                        if (deviceLabel.getText().equalsIgnoreCase(oldName)) {
                            deviceLabel.setText(newName);
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Validates data for fields.
     */
    public static boolean validateField(String fieldValue) {
        for (int i = 0; i < fieldValue.length(); i++) {
            if ((fieldValue.codePointAt(i) < 48) || (fieldValue.codePointAt(i) > 57)) {
                if ((fieldValue.codePointAt(i) != 46) && (fieldValue.codePointAt(i) != 45)
                        && (fieldValue.codePointAt(i) != 43)) {
                    return false;
                } else if ((i != 0) && ((fieldValue.codePointAt(i) == 45) || (fieldValue.codePointAt(i) == 43))) {
                    return false;
                }
            }
        }
        if (fieldValue.equals("-") || fieldValue.equals("+")) {
            return false;
        }
        return true;
    }

    /**
     * Sets if this is X trajectories or Y ones.
     * 
     * @param trajectoryListView
     */
    @Override
    public void setYTrajectory(boolean yTrajectory) {
        this.yTrajectory = yTrajectory;
        if (rangeList != null) {
            for (RangeView rangeView : rangeList) {
                rangeView.setYRangeEnabled(yTrajectory);
            }
        }
    }

    /**
     * Returns if this is HCS trajectories or not.
     * 
     * @return
     */
    private boolean isHCSTrajectory() {
        return hcsTrajectory;
    }

    /**
     * Sets if this is HCS trajectories or not.
     * 
     * @param trajectoryListView
     */
    @Override
    public void setHCSTrajectory(boolean hcsTrajectory) {
        this.hcsTrajectory = hcsTrajectory;
        if (hcsTrajectory) {
            removeAll();
            setLayout(new BorderLayout());
            JScrollPane scrollPane = new JScrollPane(getHCSRangeView());
            scrollPane.setPreferredSize(new Dimension(180, 200));
            add(scrollPane, BorderLayout.WEST);
            add(getToolBar(), BorderLayout.NORTH);
            add(getCenterScrollPane(), BorderLayout.CENTER);
            southPanel = null;
            add(getSouthJPanel(), BorderLayout.SOUTH);
            revalidate();
            repaint();
            centerJScrollPane.getColumnHeader().setVisible(false);
        } else {
            removeAll();
            initialize();
            centerJScrollPane.getColumnHeader().setVisible(true);
        }

        if (rangeList != null) {
            for (RangeView rangeView : rangeList) {
                rangeView.setHCSEnabled(hcsTrajectory);
            }
        }

        refreshButtonEnable();
    }

    /**
     * Sets enable speed control option by the specified value.
     * 
     * @param value
     */
    @Override
    public void setEnableSpeedControl(Boolean value) {
        speedCheckBox.setSelected(value);
        Iterator<RangeView> rangeIterator = rangeList.iterator();
        while (rangeIterator.hasNext()) {
            rangeIterator.next().setEnableSpeedControl(value);
        }
    }

    /**
     * Sets on the fly option by the specified value.
     * 
     * @param value
     */
    @Override
    public void setOnTheFly(Boolean value) {
        this.getFlyCheckBox().setSelected(value);
        RangeView rangeView = null;
        for (int i = 0; i < rangeList.size(); i++) {
            if (i > 0) {
                rangeView = rangeList.get(i);
                rangeView.setVisible(!value);
            }
        }
    }

    public void enableControl(boolean enable) {
        getFlyCheckBox().setEnabled(enable);
        getSpeedCheckBox().setEnabled(enable);
    }

    @Override
    public void setOnTheFlyEnabled(boolean enabled) {
        this.getFlyCheckBox().setEnabled(enabled);

    }

    @Override
    public void setEnableActuatorSpeedControlEnabled(boolean enabled) {
        this.getSpeedCheckBox().setEnabled(enabled);

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle(TrajectoryListView.class.getName() + " Test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        TrajectoryListView view = new TrajectoryListView();

        // Add Range
        view.addRange(2);

        // Add actuator
        IActuator actuator = new ActuatorImpl();
        actuator.setName("tango/tangotest/1/ampli");
        actuator.setEnabled(true);
        List<IActuator> actuatorList = new ArrayList<IActuator>();
        actuatorList.add(actuator);

        actuator = new ActuatorImpl();
        actuator.setName("tango/tangotest/2/ampli");
        actuator.setEnabled(true);
        actuatorList.add(actuator);

        view.addDevice(actuatorList);

        // Set HCS
        view.setHCSTrajectory(true);
        view.setYTrajectory(false);

        view.clearTrajectoriesView();

        actuatorList.clear();

        // Add Range
        view.addRange(1);

        actuator = new ActuatorImpl();
        actuator.setName("tango/tangotest/1/short_scalar_w");
        actuator.setEnabled(true);
        actuatorList.add(actuator);

        actuator = new ActuatorImpl();
        actuator.setName("tango/tangotest/2/short_scalar_w");
        actuator.setEnabled(true);
        actuatorList.add(actuator);

        view.addDevice(actuatorList);

        // Set HCS
        view.setHCSTrajectory(false);
        view.setYTrajectory(true);

        frame.setContentPane(view);
        frame.setSize(700, 700);
        frame.setVisible(true);

    }

}

package fr.soleil.salsa.client.controller.impl;

import fr.soleil.salsa.client.controller.ITrajectoryListController;
import fr.soleil.salsa.client.view.ITrajectoryView;
import fr.soleil.salsa.client.view.TrajectoryListView;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.Range1DModel;
import fr.soleil.salsa.entity.event.Range2DXModel;
import fr.soleil.salsa.entity.event.Range2DYModel;
import fr.soleil.salsa.entity.event.RangeHCSModel;
import fr.soleil.salsa.entity.event.Trajectory1DModel;
import fr.soleil.salsa.entity.event.Trajectory2DXModel;
import fr.soleil.salsa.entity.event.Trajectory2DYModel;
import fr.soleil.salsa.entity.event.TrajectoryHCSModel;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;

public class TrajectoryController extends AbstractTrajectoryController<TrajectoryListView> implements
ITrajectoryListController<TrajectoryListView> {

    private boolean hcsConfig = false;
    private boolean yConfig = false;

    public TrajectoryController() {
        this(false);
    }

    public TrajectoryController(boolean yController) {
        super();
        setYTrajectory(yController);
        TrajectoryListView aView = new TrajectoryListView(this);
        setTrajectoryView(aView);
    }

    @Override
    public void setTrajectoryView(ITrajectoryView aview) {
        super.setTrajectoryView(aview);

        if (trajectoryview instanceof TrajectoryListView) {
            super.setView((TrajectoryListView) trajectoryview);
        }
    }

    @Override
    public void setConfig(IConfig<?> aconfig) {
        IConfig<?> newconfig = null;
        boolean configValid = false;
        hcsConfig = aconfig instanceof IConfigHCS;
        yConfig = aconfig instanceof IConfig2D;

        if (yController) {
            configValid = aconfig instanceof IConfig2D;
        } else {
            configValid = (aconfig instanceof IConfig1D) || yConfig || hcsConfig;
        }

        if (configValid) {
            newconfig = aconfig;
            setHCSTrajectory(hcsConfig);
        }

        super.setConfig(newconfig);

        this.setViewVisible(configValid);
        if (view != null) {
            view.enableControl(configValid);
        }
    }

    @Override
    protected ITrajectory createTrajectory() {
        ITrajectory newTrajectory = null;
        if (yController) {
            newTrajectory = new Trajectory2DYModel();
        } else if (hcsConfig) {
            newTrajectory = new TrajectoryHCSModel();
        } else if (yConfig) {
            newTrajectory = new Trajectory2DXModel();
        } else {
            newTrajectory = new Trajectory1DModel();
        }

        if (newTrajectory != null) {
            newTrajectory.setBeginPosition(0.0);
            newTrajectory.setEndPosition(0.0);
            newTrajectory.setDelta(0.0);
            newTrajectory.setRelative(false);
            newTrajectory.setSpeed(0.0);
        }
        return newTrajectory;
    }

    @Override
    protected IRange createRange() {
        IRange newRange = null;
        if (yController) {
            newRange = new Range2DYModel();
        } else if (hcsConfig) {
            newRange = new RangeHCSModel();
        } else if (yConfig) {
            newRange = new Range2DXModel();
        } else {
            newRange = new Range1DModel();
        }
        return newRange;
    }

    @Override
    public void notifyDeleteActuator(IActuator actuator) {
        // Nothing to do
    }

    @Override
    protected IDimension initDimension(IConfig<?> aconfig) {
        IDimension newDimension = null;
        if (yController && (aconfig instanceof IConfig2D)) {
            newDimension = ((IConfig2D) aconfig).getDimensionY();
        } else {
            newDimension = aconfig.getDimensionX();
        }

        return newDimension;
    }

    @Override
    protected TrajectoryListView generateView() {
        return null;
    }

}

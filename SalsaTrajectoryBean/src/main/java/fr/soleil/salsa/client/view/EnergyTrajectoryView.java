package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.table.AbstractTableModel;

import org.jdesktop.swingx.JXTable;
import org.slf4j.Logger;

import fr.soleil.salsa.client.controller.ITrajectoryListController;
import fr.soleil.salsa.client.util.CellNumberRenderer;
import fr.soleil.salsa.client.util.CellNumberValueEditor;
import fr.soleil.salsa.client.util.EnergyTrajectoryLine;
import fr.soleil.salsa.client.util.NumberValue;
import fr.soleil.salsa.client.view.component.DoubleField;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.logging.LoggingUtil;

/***
 * Scan energy view
 */
public class EnergyTrajectoryView extends JPanel implements IView<ITrajectoryListController<?>>, ITrajectoryView {

    private static final long serialVersionUID = -4852034498567122925L;

    public static final Logger LOGGER = LoggingUtil.getLogger(EnergyTrajectoryView.class);

    private ITrajectoryListController<?> controller;
    private final Map<Integer, EnergyTrajectoryLine> energyTrajectoryLines = new HashMap<>();

    /**
     * The table model.
     */
    private AbstractTableModel tableModel;

    private JXTable jTable;
    private JScrollPane jScrollPane;
    private JToolBar jToolBar;
    private JButton newButton;
    private JButton deleteButton;

    private JCheckBox onTheFlyCheck = null;
    private JCheckBox scanSpeedCheck = null;
    private JLabel scanSpeedLabel = null;
    private DoubleField scanSpeedField = null;

    /**
     * The names of the columns.
     */
    public String[] columnsNames = { "Range", "Start energy", "End energy", "Energy step", "Step number",
            "Integration time (s)", "Delta constant" };

    /**
     * Classes names for the model.
     */
    private final Class<?>[] columnsClasses = { String.class, NumberValue.class, NumberValue.class, NumberValue.class,
            NumberValue.class, NumberValue.class, Boolean.class };

    public EnergyTrajectoryView() {
        this(null);
    }

    public EnergyTrajectoryView(ITrajectoryListController<?> controller) {
        this(controller, true);
    }

    /**
     * Construct
     */
    public EnergyTrajectoryView(ITrajectoryListController<?> controller, boolean buttonEnabled) {
        this.controller = controller;
        jTable = null;
        jScrollPane = null;
        jToolBar = null;
        newButton = null;
        deleteButton = null;
        initialize();
        enableButtons(buttonEnabled);
    }

    /**
     * This method initializes this.
     */
    private void initialize() {
        this.setLayout(new BorderLayout());
        this.setSize(500, 250);
        this.add(this.getJToolBar(), BorderLayout.NORTH);
        this.add(this.getJScrollPane(), BorderLayout.CENTER);
    }

    /**
     * This method initializes jToolBar
     * 
     * @return javax.swing.JToolBar
     */
    private JToolBar getJToolBar() {
        if (jToolBar == null) {
            jToolBar = new JToolBar();
            jToolBar.setFloatable(false);
            jToolBar.add(getNewButton());
            jToolBar.addSeparator();
            jToolBar.add(getDeleteButton());
            jToolBar.addSeparator();

            // OnTheFly and ScanSpeed
            onTheFlyCheck = new JCheckBox("on the fly");
            jToolBar.add(onTheFlyCheck);
            onTheFlyCheck.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    if (controller != null) {
                        controller.notifyOnTheFlyAction(onTheFlyCheck.isSelected());
                    }
                }
            });

            scanSpeedCheck = new JCheckBox("enable actuator speed");
            scanSpeedCheck.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    if (controller != null) {
                        controller.notifyEnableActuatorSpeedControlAction(scanSpeedCheck.isSelected());
                    }
                }
            });

            jToolBar.add(scanSpeedCheck);

            scanSpeedLabel = new JLabel("Scan Speed");
            scanSpeedLabel.setVisible(false);

            jToolBar.add(scanSpeedLabel);

            scanSpeedField = new DoubleField();
            scanSpeedField.setBackground(Color.LIGHT_GRAY);
            scanSpeedField.setVisible(false);
            scanSpeedField.setEditable(false);
            jToolBar.add(scanSpeedField);

        }
        return jToolBar;
    }

    /**
     * This method initializes jScrollPane
     * 
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getJScrollPane() {
        if (jScrollPane == null) {
            jScrollPane = new JScrollPane();
            jScrollPane.setViewportView(getJTable());
        }
        return jScrollPane;
    }

    /**
     * This method initializes jTable
     * 
     * @return javax.swing.JTable
     */
    public JXTable getJTable() {
        if (jTable == null) {
            jTable = new JXTable();
            jTable.setSortable(false);
            // jTable.setEditable(true);
            jTable.setPreferredScrollableViewportSize(new java.awt.Dimension(300, 100));
            // jTable.setSelectionBackground(Color.green);
            jTable.setRowSelectionAllowed(true);
            jTable.setColumnSelectionAllowed(false);
            jTable.setDefaultEditor(NumberValue.class, new CellNumberValueEditor());
            // jTable.setDefaultEditor(Integer.class, new CellIntegerEditor());
            jTable.setDefaultRenderer(NumberValue.class, new CellNumberRenderer());
            // jTable.setDefaultRenderer(Integer.class, new
            // CellNumberRenderer());
            jTable.setModel(getTableModel());
        }
        return jTable;
    }

    /**
     * Clear the view.
     */
    public void clear() {
        energyTrajectoryLines.clear();
        if (tableModel != null) {
            tableModel.fireTableStructureChanged();
        }

    }

    /**
     * This method initializes newButton
     * 
     * @return javax.swing.JComponent
     */
    private JButton getNewButton() {
        if (newButton == null) {
            newButton = new JButton("New");
            newButton.setEnabled(false);
            newButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (controller != null) {
                        controller.notifyNewRangeAction();
                    }
                }
            });
        }
        return newButton;
    }

    /**
     * This method initializes deleteButton
     * 
     * @return javax.swing.JComponent
     */
    private JButton getDeleteButton() {
        if (deleteButton == null) {
            deleteButton = new JButton("Delete");
            deleteButton.setEnabled(false);
            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (controller != null) {
                        int lastIndex = energyTrajectoryLines.size() - 1;
                        energyTrajectoryLines.remove(lastIndex);
                        tableModel.fireTableStructureChanged();
                        controller.notifyDeleteRangeAction();
                    }
                }
            });
        }
        return deleteButton;
    }

    @Override
    public ITrajectoryListController<?> getController() {
        return controller;
    }

    @Override
    public void setController(ITrajectoryListController<?> controller) {
        this.controller = controller;
    }

    public void initTableModel() {

        this.tableModel = new AbstractTableModel() {

            private static final long serialVersionUID = 1639834079696538876L;

            @Override
            public boolean isCellEditable(int row, int column) {
                return (column > 0);
            }

            @Override
            public Class<?> getColumnClass(int column) {
                return columnsClasses[column];
            }

            @Override
            public String getColumnName(int col) {
                return columnsNames[col];
            }

            @Override
            public int getRowCount() {
                if (onTheFlyCheck.isSelected()) {
                    return 1;
                }
                return energyTrajectoryLines.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                Object value = null;
                EnergyTrajectoryLine line = getEnergyTrajectoryLine(row, false);
                if (line != null) {
                    switch (col) {
                        case 0: // start energy
                            value = line.getRange().toString();
                            break;
                        case 1: // start energy
                            value = line.getBeginPosition();
                            break;
                        case 2: // end energy
                            value = line.getEndPosition();
                            break;
                        case 3: // energy step
                            value = line.getDelta();
                            break;
                        case 4: // step number
                            value = line.getStepNumber();
                            break;
                        case 5: // integration time
                            value = line.getIntegrationTime();
                            break;
                        case 6: // delta constant
                            value = line.getDeltaConstant();
                            break;
                    }
                }
                return value;
            }

            @Override
            public void setValueAt(Object value, int row, int col) {
                switch (col) {
                    case 1: // start energy
                        if (controller != null) {
                            Boolean deltaConstant = (Boolean) tableModel.getValueAt(row, 6);
                            controller.notifyFromValueChanged(value.toString(), row, null, deltaConstant);
                        }
                        break;
                    case 2: // end energy
                        if (controller != null) {
                            Boolean deltaConstant = (Boolean) tableModel.getValueAt(row, 6);
                            controller.notifyToValueChanged(value.toString(), row, null, deltaConstant);
                        }
                        break;
                    case 3: // energy step
                        if (controller != null) {
                            controller.notifyDeltaValueChanged(value.toString(), row, null);
                        }
                        break;
                    case 4: // step number
                        if (controller != null) {
                            controller.notifyNumberOfStepsChange(value.toString(), row);
                        }
                        break;
                    case 5: // integration time
                        if (controller != null) {
                            double integrationTime = Double.valueOf(value.toString());
                            EnergyTrajectoryLine line = getEnergyTrajectoryLine(row, false);
                            double[] integrationTimeArray = new double[line.getStepNumber().intValue() + 1];
                            Arrays.fill(integrationTimeArray, integrationTime);
                            controller.notifyIntegrationTimeChange(integrationTimeArray, row);
                        }
                        break;
                    case 6: // integration time
                        if (controller != null) {
                            Boolean deltaConstant = (Boolean) value;
                            controller.notifyDeltaConstantChanged(deltaConstant, row, null);
                        }
                        break;
                }
            }

            @Override
            public int getColumnCount() {
                return columnsNames.length;
            }
        };
    }

    public AbstractTableModel getTableModel() {
        if (this.tableModel == null) {
            initTableModel();
        }
        return tableModel;
    }

    /**
     * Gets the energy trajectory lines. The view is a table, each line is a
     * trajectory.
     */
    private EnergyTrajectoryLine getEnergyTrajectoryLine(int position, boolean saved) {
        EnergyTrajectoryLine line = energyTrajectoryLines.get(position);
        if ((line == null) && !saved) {
            line = new EnergyTrajectoryLine();
            line.setRange(position);
            energyTrajectoryLines.put(position, line);
        }
        return line;
    }

    public void refresh() {
        if (tableModel != null) {
            tableModel.fireTableDataChanged();
        }
    }

    /**
     * Can enable / disable the buttons.
     * 
     * @param enabled
     */
    public void enableButtons(boolean enabled) {
        getNewButton().setEnabled(enabled);
        getDeleteButton().setEnabled(enabled);
    }

    @Override
    public void setIntegrationTime(int rangePosition, String integrationTime, boolean saved) {
        EnergyTrajectoryLine line = getEnergyTrajectoryLine(rangePosition, saved);
        if (line != null) {
            if (!saved) {
                try {
                    double value = Double.valueOf(integrationTime);
                    line.setIntegrationTime(value);
                } catch (Exception e) {
                    LOGGER.error("Cannot convert integrationTime {} to double {}", integrationTime, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            } else {
                line.getIntegrationTime().setDbValue(integrationTime);
            }
        }
        refresh();
    }

    @Override
    public void setStepNumber(int rangePosition, String stepNumber, boolean saved) {
        EnergyTrajectoryLine line = getEnergyTrajectoryLine(rangePosition, saved);
        if (line != null) {
            if (!saved) {
                try {
                    int value = Integer.valueOf(stepNumber);
                    line.setStepNumber(value);
                } catch (Exception e) {
                    LOGGER.error("Cannot convert stepNumber {} to integer {}", stepNumber, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            } else {
                line.getStepNumber().setDbValue(stepNumber);
            }
        }
        refresh();

    }

    @Override
    public void setBeginPosition(String actuatorName, int rangePosition, String beginPosition, boolean saved) {

        EnergyTrajectoryLine line = getEnergyTrajectoryLine(rangePosition, saved);
        if (line != null) {
            if (!saved) {
                try {
                    double value = Double.valueOf(beginPosition);
                    line.setBeginPosition(value);

                } catch (Exception e) {
                    LOGGER.error("Cannot convert beginPosition {} to double {}", beginPosition, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            } else {
                line.getBeginPosition().setDbValue(beginPosition);
            }
        }
        refresh();

    }

    @Override
    public void setEndPosition(String actuatorName, int rangePosition, String endPosition, boolean saved) {
        EnergyTrajectoryLine line = getEnergyTrajectoryLine(rangePosition, saved);
        if (line != null) {
            if (!saved) {
                try {
                    double value = Double.valueOf(endPosition);
                    line.setEndPosition(value);
                } catch (Exception e) {
                    LOGGER.error("Cannot convert {} to double {}", endPosition, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            } else {
                line.getEndPosition().setDbValue(endPosition);
            }
        }
        refresh();

    }

    @Override
    public void setDelta(String actuatorName, int rangePosition, String delta, boolean saved) {
        EnergyTrajectoryLine line = getEnergyTrajectoryLine(rangePosition, saved);
        if (line != null) {
            if (!saved) {
                try {
                    double value = Double.valueOf(delta);
                    line.setDelta(value);

                } catch (Exception e) {
                    LOGGER.error("Cannot convert delta {} to double {}", delta, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            } else {
                line.getDelta().setDbValue(delta);
            }
        }
        refresh();

    }

    @Override
    public void setSpeed(String actuatorName, int rangePosition, String speed, boolean saved) {
        if (rangePosition == 0) {
            scanSpeedField.setText(speed);
        }
    }

    @Override
    public void setRelative(String actuatorName, int rangePosition, Boolean relative) {
    }

    @Override
    public void setDeltaconstant(String actuatorName, int rangePosition, Boolean deltaConstant) {
        EnergyTrajectoryLine line = getEnergyTrajectoryLine(rangePosition, false);
        if (line != null) {
            line.setDeltaConstant(deltaConstant);
        }
        refresh();
    }

    @Override
    public void setEnableSpeedControl(Boolean enable) {
        scanSpeedCheck.setSelected(enable);
        scanSpeedField.setVisible(enable);
        scanSpeedLabel.setVisible(enable);
    }

    @Override
    public void setOnTheFly(Boolean onTheFly) {
        onTheFlyCheck.setSelected(onTheFly);
        setEnableActuatorSpeedControlEnabled(onTheFly);
    }

    @Override
    public void setYTrajectory(boolean enable) {
    }

    @Override
    public void setHCSTrajectory(boolean enable) {
    }

    @Override
    public void addRange(int nbRange) {
        getEnergyTrajectoryLine(energyTrajectoryLines.size(), false);
        if (tableModel != null) {
            tableModel.fireTableStructureChanged();
        }
    }

    @Override
    public void addDevice(List<IActuator> actuatorName) {
    }

    @Override
    public void addDevice(IActuator actuator) {
    }

    @Override
    public void clearTrajectoriesView() {
        clear();
    }

    @Override
    public void setOnTheFlyEnabled(boolean enabled) {
        onTheFlyCheck.setEnabled(enabled);
        tableModel.fireTableDataChanged();
    }

    @Override
    public void setEnableActuatorSpeedControlEnabled(boolean enabled) {
        scanSpeedCheck.setEnabled(enabled);
    }

    @Override
    public void setCustomTrajectory(String actuatorName, int rangePosition, boolean custom) {
        // nothing to do the trajectory is always linear

    }

    @Override
    public void setCustomTrajectoryValues(String actuatorName, int rangePosition, double[] values) {
        // nothing to do the trajectory is always linear

    }

    @Override
    public void setActuatorName(String oldName, String newName) {
        // nothing to do there is only on actuator

    }

    @Override
    public void deleteDevice(IActuator actuator) {
        // nothing to do there is only on actuator
    }

}

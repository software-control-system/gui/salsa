package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.tango.utils.TangoUtil;

import fr.esrf.TangoApi.AttributeProxy;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.Trajectory;
import fr.soleil.salsa.client.controller.ITrajectoryListController;
import fr.soleil.salsa.client.util.BackgroundRenderer;
import fr.soleil.salsa.client.util.ICustomDataListener;
import fr.soleil.salsa.client.view.component.DoubleField;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Contains all GUI elements for a dimension.
 * 
 * @author Saintin
 */
public class TrajectoryView extends JPanel implements ICustomDataListener {

    private static final long serialVersionUID = -8901894991093163711L;

    private static final Logger LOGGER = LoggingUtil.getLogger(TrajectoryView.class);
    private static final String TOOLTIP_FORMAT = "%s range #%s";
    private static final String FULL_ATTRIBUTE_NAME = "{} full attribute name -> {}";
    private static final String ACTUATOR_FORMAT = "%s format -> %s";
    private static final String FORMAT_NOT_SET_FOR_ATTRIBUTE = "Format not set for attribute %s %s";
    private static final String FROM = "FROM";
    private static final String TO = "TO";
    private static final String SCAN_SPEED = "Scan Speed";
    private static final String LINEAR_TRAJECTORY_KEY = String.valueOf(false);
    private static final String CUSTOM_TRAJECTORY_KEY = String.valueOf(true);

    // DataBase information
    private String fromValue;
    private String dbFromValue;
    private String toValue;
    private String dbToValue;
    private String deltaValue;
    private String dbDeltaValue;

    private double[] customTrajectories;

    private JPanel trajectoryPanel;
    private JLayeredPane linearTrajectoryPanel;
    private TrajectoryChart customTrajectoryChart;
    private CardLayout cardLayout;

    /**
     * "From" label.
     */
    private Label fromLabel;

    /**
     * "TO" label.
     */
    private Label toLabel;

    /**
     * Delta label
     */
    private Label deltaLabel;

    /**
     * Scan speed label
     */
    private Label scanSpeedLabel;

    /**
     * "From" field.
     */
    private DoubleField fromField;

    /**
     * "TO" field
     */
    private DoubleField toField;

    /**
     * Delta field
     */
    private DoubleField deltaField;

    /**
     * Scan speed field.
     */
    private DoubleField scanSpeedField;

    /**
     * Relative check box.
     */
    private CheckBox relativeCheckBox;

    /**
     * Delta check box.
     */
    private CheckBox deltaCheckBox;

    private JCheckBox customTrajectory;

    /**
     * The controller.
     */
    private ITrajectoryListController<?> trajectoryController;

    /**
     * i Position in the GUI.
     */
    private int rangePosition;

    /**
     * the actuator name
     */
    private final String actuatorName;

    /**
     * Used for HCS trajectory view.
     */
    private boolean hcsTrajectory;

    public static final int WIDTH = 205;

    public static final int HEIGHT = 120;

    public TrajectoryView() {
        this(null, null, 0, false);
    }

    public String getActuatorName() {
        return actuatorName;
    }

    /**
     * Constructor : iPosition = actuator index position. jPosition = range
     * index position.
     * 
     * @param trajectoryController
     * @param iPosition
     * @param jPosition
     */
    public TrajectoryView(ITrajectoryListController<?> trajectoryController, String actuatorName, int iPosition,
            boolean hcsTrajectory) {
        this.trajectoryController = trajectoryController;
        this.rangePosition = iPosition;
        setToolTipText(String.format(TOOLTIP_FORMAT, actuatorName, Integer.toString(rangePosition + 1)));
        this.actuatorName = actuatorName;
        this.hcsTrajectory = hcsTrajectory;
        initialize();
        refreshFormat();
    }

    private void refreshFormat() {
        Thread thread = new Thread(TrajectoryView.class.getSimpleName()) {
            @Override
            public void run() {
                if (SalsaUtils.isDefined(actuatorName)) {
                    String newActuatorName = actuatorName;
                    try {
                        newActuatorName = TangoUtil.getfullAttributeNameForAttribute(actuatorName);
                        AttributeProxy proxy = new AttributeProxy(newActuatorName);
                        LOGGER.trace(FULL_ATTRIBUTE_NAME, actuatorName, newActuatorName);
                        if (proxy != null) {
                            final String format = proxy.get_info().format;
                            LOGGER.trace(String.format(ACTUATOR_FORMAT, newActuatorName, format));
                            if (SalsaUtils.isDefined(format)) {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        setFormat(format);
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        LOGGER.debug(String.format(FORMAT_NOT_SET_FOR_ATTRIBUTE, newActuatorName, e.getMessage()), e);
                    }
                }
            }
        };
        thread.start();
    }

    public ITrajectoryListController<?> getTrajectoryController() {
        return trajectoryController;
    }

    public void setTrajectoryController(ITrajectoryListController<?> trajectoryController) {
        this.trajectoryController = trajectoryController;
    }

    public void setFormat(String format) {
        fromField.setFormat(format);
        toField.setFormat(format);
        deltaField.setFormat(format);
    }

    public void initialize() {
        fromLabel = new Label();
        fromLabel.setText(FROM);
        fromLabel.setPreferredSize(new Dimension(100, 15));
        fromLabel.setOpaque(true);
        fromLabel.setBackground(Color.WHITE);

        toLabel = new Label();
        toLabel.setText(TO);
        toLabel.setPreferredSize(new Dimension(100, 15));
        toLabel.setOpaque(true);
        toLabel.setBackground(Color.WHITE);

        char c = 916; // Delta code (capital letter).
        deltaLabel = new Label();
        deltaLabel.setText(String.valueOf(c));
        deltaLabel.setPreferredSize(new Dimension(10, 15));

        scanSpeedLabel = new Label();
        scanSpeedLabel.setText(SCAN_SPEED);
        scanSpeedLabel.setPreferredSize(new Dimension(100, 15));
        scanSpeedLabel.setVisible(false);

        setLayout(new BorderLayout());
        cardLayout = new CardLayout();

        linearTrajectoryPanel = new JLayeredPane();
        linearTrajectoryPanel.setLayout(new GridBagLayout());
        trajectoryPanel = new JPanel(cardLayout);

        trajectoryPanel.add(linearTrajectoryPanel, LINEAR_TRAJECTORY_KEY);
        cardLayout.show(trajectoryPanel, LINEAR_TRAJECTORY_KEY);

        GridBagConstraints fromLabelConstraint = new GridBagConstraints();
        GridBagConstraints toLabelConstraint = new GridBagConstraints();
        GridBagConstraints deltaLabelConstraint = new GridBagConstraints();
        GridBagConstraints scanSpeedLabelConstraint = new GridBagConstraints();

        GridBagConstraints fromFieldConstraint = new GridBagConstraints();
        GridBagConstraints toFieldConstraint = new GridBagConstraints();
        GridBagConstraints deltaFieldConstraint = new GridBagConstraints();
        GridBagConstraints scanSpeedFieldConstraint = new GridBagConstraints();

        GridBagConstraints relativeCheckBoxConstraint = new GridBagConstraints();
        GridBagConstraints deltaCheckBoxConstraint = new GridBagConstraints();

        fromLabelConstraint.gridx = 0;
        fromLabelConstraint.gridy = 0;

        toLabelConstraint.gridx = 1;
        toLabelConstraint.gridy = 0;
        toLabelConstraint.gridwidth = 3;

        fromFieldConstraint.gridx = 0;
        fromFieldConstraint.gridy = 1;

        toFieldConstraint.gridx = 1;
        toFieldConstraint.gridy = 1;
        toFieldConstraint.gridwidth = 3;

        deltaLabelConstraint.gridx = 1;
        deltaLabelConstraint.gridy = 2;

        deltaFieldConstraint.gridx = 2;
        deltaFieldConstraint.gridy = 2;

        scanSpeedLabelConstraint.gridx = 0;
        scanSpeedLabelConstraint.gridy = 3;

        scanSpeedFieldConstraint.gridx = 1;
        scanSpeedFieldConstraint.gridy = 3;
        scanSpeedFieldConstraint.gridwidth = 3;

        relativeCheckBoxConstraint.gridx = 0;
        relativeCheckBoxConstraint.gridy = 2;
        deltaCheckBoxConstraint.gridx = 3;
        deltaCheckBoxConstraint.gridy = 2;

        linearTrajectoryPanel.add(fromLabel, fromLabelConstraint);
        linearTrajectoryPanel.add(toLabel, toLabelConstraint);
        linearTrajectoryPanel.add(scanSpeedLabel, scanSpeedLabelConstraint);

        linearTrajectoryPanel.add(getFromField(), fromFieldConstraint);
        linearTrajectoryPanel.add(getToField(), toFieldConstraint);

        linearTrajectoryPanel.add(deltaLabel, deltaLabelConstraint);
        linearTrajectoryPanel.add(getDeltaField(), deltaFieldConstraint);

        linearTrajectoryPanel.add(getScanSpeedField(), scanSpeedFieldConstraint);

        linearTrajectoryPanel.add(getRelativeCheckBox(), relativeCheckBoxConstraint);
        linearTrajectoryPanel.add(getDeltaCheckBox(), deltaCheckBoxConstraint);
        add(getCustomTrajectoryCheckbox(), BorderLayout.NORTH);

        setHCSTrajectoryEnabled(hcsTrajectory);

        add(trajectoryPanel, BorderLayout.CENTER);
        setBorder(BorderFactory.createLoweredBevelBorder());
        setBounds(5, 0, WIDTH, HEIGHT);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
    }

    /**
     * Returns "From" field (Begin).
     * 
     * @return
     */
    public TextField getFromField() {
        if (fromField == null) {
            fromField = new DoubleField();
            fromField.setToolTipText(actuatorName);
            fromField.setColumns(6);
            setBackgroundBeginPosition();

            fromField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                        notifyFromValueChanged();
                    }
                    setBackgroundBeginPosition();
                }
            });

            fromField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    notifyFromValueChanged();
                    setBackgroundBeginPosition();
                }
            });
        }
        return fromField;
    }

    private void notifyFromValueChanged() {
        if (!TrajectoryListView.validateField(fromField.getText())) {
            return;
        }
        if (trajectoryController != null) {
            fromValue = fromField.getText();
            boolean deltaSelected = false;
            if (!hcsTrajectory) {
                deltaSelected = getDeltaCheckBox().isSelected();
            }
            trajectoryController.notifyFromValueChanged(fromValue, rangePosition, actuatorName, deltaSelected);
        }
    }

    /**
     * Returns "To" field (End).
     * 
     * @return
     */
    public TextField getToField() {
        if (toField == null) {
            toField = new DoubleField();
            toField.setColumns(6);
            setBackgroundEndosition();
            toField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                        notifyToValueChanged();
                    }
                    setBackgroundEndosition();
                }
            });
            toField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    notifyToValueChanged();
                    setBackgroundEndosition();
                }
            });
        }
        return toField;
    }

    private void notifyToValueChanged() {
        if (!TrajectoryListView.validateField(toField.getText())) {
            return;
        }
        if (trajectoryController != null) {
            toValue = toField.getText();
            boolean deltaSelected = false;
            if (!hcsTrajectory) {
                deltaSelected = getDeltaCheckBox().isSelected();
            }
            trajectoryController.notifyToValueChanged(toValue, rangePosition, actuatorName, deltaSelected);
        }
    }

    private void notifyDeltaValueChanged() {
        if (!TrajectoryListView.validateField(deltaField.getText())) {
            return;
        }
        if (trajectoryController != null) {
            deltaValue = deltaField.getText();
            trajectoryController.notifyDeltaValueChanged(deltaField.getText(), rangePosition, actuatorName);
        }
    }

    /**
     * Returns Delta field.
     * 
     * @return
     */
    public TextField getDeltaField() {
        if (deltaField == null) {
            deltaField = new DoubleField();
            deltaField.setColumns(5);
            deltaField.setHorizontalAlignment(4); // 4 = RIGHT
            setBackgroundDelta();
            deltaField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                        notifyDeltaValueChanged();
                    }
                    setBackgroundDelta();
                }

            });

            deltaField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    notifyDeltaValueChanged();
                    setBackgroundDelta();
                }
            });
        }
        return deltaField;
    }

    public void setEnableSpeed(boolean value) {
        if (scanSpeedField != null) {
            scanSpeedField.setVisible(value);
            scanSpeedLabel.setVisible(scanSpeedField.isVisible());
        }
    }

    /**
     * Returns scan speed field.
     * 
     * @return
     */
    public TextField getScanSpeedField() {
        if (scanSpeedField == null) {
            scanSpeedField = new DoubleField();
            scanSpeedField.setEditable(false);
            scanSpeedField.setVisible(false);
            scanSpeedField.setPreferredSize(new Dimension(100, 20));
            scanSpeedField.setBackground(Color.LIGHT_GRAY);
        }

        return scanSpeedField;
    }

    /**
     * Returns relative check box.
     * 
     * @return
     */
    public CheckBox getRelativeCheckBox() {
        if (relativeCheckBox == null) {
            relativeCheckBox = new CheckBox();
            relativeCheckBox.setText("Relative");
            relativeCheckBox.setPreferredSize(new Dimension(100, 30));
            relativeCheckBox.addActionListener((e) -> {
                if (trajectoryController != null) {
                    trajectoryController.notifyRelativeChanged(getRelative(), rangePosition, actuatorName);
                }
            });
        }
        return relativeCheckBox;
    }

    /**
     * returns delta check box.
     * 
     * @return
     */
    public CheckBox getDeltaCheckBox() {
        if (deltaCheckBox == null) {
            deltaCheckBox = new CheckBox();
            deltaCheckBox.setPreferredSize(new Dimension(30, 20));
            deltaCheckBox.setToolTipText("Delta constant");
            deltaCheckBox.addActionListener((e) -> {
                if (trajectoryController != null) {
                    trajectoryController.notifyDeltaConstantChanged(getDeltaCheckBox().isSelected(), rangePosition,
                            actuatorName);
                }
            });
        }
        return deltaCheckBox;
    }

    /**
     * Returns begin position.
     * 
     * @return
     */
    public String getBeginPosition() {
        return fromField.getText();
    }

    /**
     * Returns end position.
     * 
     * @return
     */
    public String getEndPosition() {
        return toField.getText();
    }

    /**
     * Returns relative is selected or not.
     * 
     * @return
     */
    public Boolean getRelative() {
        return relativeCheckBox.isSelected();
    }

    /**
     * Returns delta value.
     * 
     * @return
     */
    public String getDelta() {
        return deltaField.getText();
    }

    /**
     * Returns speed value.
     * 
     * @return
     */
    public String getSpeed() {
        return scanSpeedField.getText();
    }

    /**
     * Sets begin position.
     * 
     * @return
     */
    public void setBeginPosition(String beginPosition) {
        setBeginPosition(beginPosition, false);
    }

    public void setBeginPosition(String beginPosition, boolean savedValue) {
        if (savedValue) {
            dbFromValue = beginPosition;
        } else {
            fromValue = beginPosition;
            fromField.setText(beginPosition);
        }
        setBackgroundBeginPosition();
    }

    private void setBackgroundBeginPosition() {
        BackgroundRenderer.setBackgroundField(fromField, fromValue, dbFromValue);
    }

    /**
     * Sets end position.
     * 
     * @return
     */
    public void setEndPosition(String endPosition) {
        setEndPosition(endPosition, false);
    }

    public void setEndPosition(String endPosition, boolean savedValue) {
        if (savedValue) {
            dbToValue = endPosition;
        } else {
            toValue = endPosition;
            toField.setText(endPosition);
        }
        setBackgroundEndosition();
    }

    private void setBackgroundEndosition() {
        BackgroundRenderer.setBackgroundField(toField, toValue, dbToValue);
    }

    protected void initCustomTrajectoryChart() {
        if (customTrajectoryChart == null) {
            customTrajectoryChart = new TrajectoryChart();
            customTrajectoryChart.setTrajectory(customTrajectories);
            if (hcsTrajectory) {
                customTrajectoryChart.removeCustomTrajectoryListener(this);
            } else {
                customTrajectoryChart.addCustomTrajectoryListener(this);
            }
            trajectoryPanel.add(customTrajectoryChart, CUSTOM_TRAJECTORY_KEY);
            trajectoryPanel.revalidate();
        }
    }

    public void setCustomTrajectory(boolean custom) {
        if (customTrajectory != null) {
            customTrajectory.setSelected(custom);
            if (custom) {
                initCustomTrajectoryChart();
                cardLayout.show(trajectoryPanel, CUSTOM_TRAJECTORY_KEY);
            } else {
                cardLayout.show(trajectoryPanel, LINEAR_TRAJECTORY_KEY);
            }
        }
    }

    public void setCustomTrajectoryValues(double... doubleValues) {
        if (!ObjectUtils.sameObject(customTrajectories, doubleValues)) {
            customTrajectories = doubleValues;
            if (customTrajectoryChart == null) {
                customDataChange(customTrajectories);
            } else {
                customTrajectoryChart.setTrajectory(customTrajectories);
            }
        }
    }

    public void setRelative(Boolean relative) {
        if (relativeCheckBox != null) {
            if (relative == null) {
                relative = Boolean.valueOf(false);
            }
            relativeCheckBox.setSelected(relative);
            relativeCheckBox.setText("Relative");
        }
    }

    /**
     * Sets deltaConstant.
     * 
     * @return
     */
    public void setDeltaConstant(Boolean deltaConstant) {
        if (deltaCheckBox != null) {
            if (deltaConstant == null) {
                deltaConstant = Boolean.valueOf(false);
            }
            deltaCheckBox.setSelected(deltaConstant);
        }
    }

    /**
     * Sets delta value.
     * 
     * @return
     */
    public void setDelta(String delta) {
        setDelta(delta, false);
    }

    public void setDelta(String delta, boolean savedValue) {
        if (deltaField != null) {
            if (savedValue) {
                dbDeltaValue = delta;
            } else {
                deltaValue = delta;
                deltaField.setText(delta);
            }
            setBackgroundDelta();
        }
    }

    private void setBackgroundDelta() {
        if (hcsTrajectory) {
            deltaField.setBackground(Color.WHITE);
        } else {
            BackgroundRenderer.setBackgroundField(deltaField, deltaValue, dbDeltaValue);
        }
    }

    public void setSpeed(String speed, boolean savedValue) {
        scanSpeedField.setText(speed);
    }

    private JCheckBox getCustomTrajectoryCheckbox() {
        if (customTrajectory == null) {
            customTrajectory = new JCheckBox("Custom trajectory");
            customTrajectory.addActionListener((e) -> {
                if (trajectoryController != null) {
                    trajectoryController.notifyCustomTrajectoryChanged(customTrajectory.isSelected(), rangePosition,
                            actuatorName);

                }
                // TODO REMOVE this line
                setCustomTrajectory(customTrajectory.isSelected());
            });
        }
        return customTrajectory;
    }

    /**
     * Used for HCS trajectory view.
     */
    public void setHCSTrajectoryEnabled(boolean hcsTrajectory) {
        this.hcsTrajectory = hcsTrajectory;
        getDeltaCheckBox().setVisible(!hcsTrajectory);
        getCustomTrajectoryCheckbox().setVisible(!hcsTrajectory);
        if (customTrajectoryChart != null) {
            if (hcsTrajectory) {
                customTrajectoryChart.removeCustomTrajectoryListener(this);
            } else {
                customTrajectoryChart.addCustomTrajectoryListener(this);
            }
        }
        getDeltaField().setEditable(!this.hcsTrajectory);
    }

    /**
     * Get i position of the trajectory
     * 
     * @return
     */
    public int getIPosition() {
        return rangePosition;
    }

    /**
     * Set i position of the trajectory
     * 
     * @param position
     */
    public void setIPosition(int position) {
        rangePosition = position;
    }

    @Override
    public void customDataChange(double[] doubleValues) {
        if (trajectoryController != null) {
            trajectoryController.notifyCustomTrajectoryValueChanged(doubleValues, rangePosition, actuatorName);
        }
    }

    public static void main(String[] args) {
        Trajectory trajectory = new Trajectory();
        trajectory.setCompleteTrajectory(new double[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 10 });

        TrajectoryView view = new TrajectoryView();
        JFrame frame = new JFrame();
        frame.setContentPane(view);
        view.setSize(300, 400);
        frame.setSize(view.getSize());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("TrajectoryView test");
        view.setBeginPosition(String.valueOf(trajectory.getFrom()));
        view.setEndPosition(String.valueOf(trajectory.getTo()));
        view.setCustomTrajectoryValues(trajectory.getCompleteTrajectory());
        frame.setVisible(true);
    }

}

package fr.soleil.salsa.client.view;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;

public interface ITrajectoryView {

    public void setIntegrationTime(int rangePosition, String integrationTime, boolean saved);

    public void setStepNumber(int rangePosition, String stepNumber, boolean saved);

    public void setBeginPosition(String actuatorName, int rangePosition, String beginPosition,
            boolean saved);

    public void setEndPosition(String actuatorName, int rangePosition, String endPosition,
            boolean saved);

    public void setDelta(String actuatorName, int rangePosition, String delta, boolean saved);

    public void setSpeed(String actuatorName, int rangePosition, String speed, boolean saved);

    public void setRelative(String actuatorName, int rangePosition, Boolean relative);

    public void setDeltaconstant(String actuatorName, int rangePosition, Boolean deltaConstant);

    public void setCustomTrajectory(String actuatorName, int rangePosition, boolean custom);

    public void setCustomTrajectoryValues(String actuatorName, int rangePosition, double[] values);

    public void setEnableSpeedControl(Boolean enable);

    public void setOnTheFly(Boolean onTheFly);

    public void setOnTheFlyEnabled(boolean enabled);

    public void setEnableActuatorSpeedControlEnabled(boolean enabled);

    public void setYTrajectory(boolean yTrajectory);

    public void setHCSTrajectory(boolean hcsTrajectory);

    public void addRange(int nbRange);

    public void addDevice(List<IActuator> actuatorList);

    public void addDevice(IActuator actuator);

    public void deleteDevice(IActuator actuator);

    public void setActuatorName(String oldName, String newName);

    public void clearTrajectoriesView();

    public void setVisible(boolean visible);

}

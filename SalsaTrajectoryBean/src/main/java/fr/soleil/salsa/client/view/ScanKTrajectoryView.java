package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.TextField;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.salsa.client.controller.IScanKTrajectoryController;
import fr.soleil.salsa.client.util.BackgroundRenderer;
import fr.soleil.salsa.client.view.component.DoubleField;
import fr.soleil.salsa.tool.SalsaUtils;

public class ScanKTrajectoryView extends JScrollPane implements IView<IScanKTrajectoryController> {

    private static final long serialVersionUID = -8118032667360445853L;

    private static final String TIME_FORMULA_TEXT;
    private static final String TIME_FORMULA_TOOLTIP;
    private static final String ESTIMATED_DURATION_FORMAT;
    private static final String ESTIMATED_DURATION_UNKNOWN;
    static {
        String htmlFormat = "<html><body>%s</body></html>", bigFormat = "<html><body><big>%s</big></body></html>";
        String timeFormula = "Time(N) = Tmin \u00d7 (K / Kmin)<sup>N</sup>";
        String estimatedDurationFormula = "Estimated duration: %s";
        String unknownDuration = String.format(estimatedDurationFormula, "Unknown");
        TIME_FORMULA_TEXT = String.format(htmlFormat, timeFormula);
        TIME_FORMULA_TOOLTIP = String.format(bigFormat, timeFormula);
        ESTIMATED_DURATION_FORMAT = String.format(bigFormat, estimatedDurationFormula);
        ESTIMATED_DURATION_UNKNOWN = String.format(bigFormat, unknownDuration);
    }

    private static final int H_GAP = 10;
    private static final int V_GAP = 5;
    private static final int COLUMNS = 6;

    /**
     * The controller.
     */
    private IScanKTrajectoryController scanKTrajectoryController;

    /**
     * Panel that contains all GUI elements.
     */
    private JPanel centerPanel;

    /**
     * Panel that contains all parameters GUI elements.
     */
    private JPanel parametersPanel;

    /**
     * Edge panel.
     */
    private JPanel edgePanel;

    /**
     * Pre edge panel.
     */
    private JPanel preEdgePanel;

    /**
     * Post edge panel.
     */
    private JPanel postEdgePanel, postEdgeInnerPanel;

    /**
     * Connection panel.
     */
    private JPanel connectionPanel;

    /**
     * Estimates panel
     */
    private JPanel estimatesPanel;

    /**
     * Modify advanced parameters check box.
     */
    private ConstrainedCheckBox modifyAdvancedParametersCheckBox;

    // Functional parameter
    private String eMinValue;
    private String dbEMinValue;

    private String eDeltaPreEdgeValue;
    private String dbEDeltaPreEdgeValue;

    private String timePreEdgeValue;
    private String dbtimePreEdgeValue;

    private String e0Value;
    private String dbe0Value;

    private String e1Value;
    private String dbe1Value;

    private String e2Value;
    private String dbe2Value;

    private String eDeltaEdgeValue;
    private String dbeDeltaEdgeValue;

    private String timeEdgeValue;
    private String dbtimeEdgeValue;

    private String mValue;
    private String dbmValue;

    private String kMinValue;
    private String dbkMinValue;

    private String kMaxValue;
    private String dbkMaxValue;

    private String kDeltaValue;
    private String dbkDeltaValue;

    private String timePostEdgeValue;
    private String dbtimePostEdgeValue;

    private String nValue;
    private String dbnValue;

    private String deadTimeValue;
    private String dbDeadTimeValue;

    // ///////// GUI Parameters Elements /////////////////////////////

    private final JLabel eMinLabel;
    private DoubleField eMinField;

    private final JLabel eDeltaPreEdgeLabel;
    private DoubleField eDeltaPreEdgeField;

    private final JLabel timePreEdgeLabel;
    private DoubleField timePreEdgeField;

    private final JLabel e0Label;
    private DoubleField e0Field;

    private final JLabel e1Label;
    private DoubleField e1Field;

    private final JLabel e2Label;
    private DoubleField e2Field;

    private final JLabel eDeltaEdgeLabel;
    private DoubleField eDeltaEdgeField;

    private final JLabel timeEdgeLabel;
    private DoubleField timeEdgeField;

    private final JLabel mLabel;
    private DoubleField mField;

    private final JLabel timeFormulaLabel;

    private final JLabel kMinLabel;
    private DoubleField kMinField;

    private final JLabel kMaxLabel;
    private DoubleField kMaxField;

    private final JLabel kDeltaLabel;
    private DoubleField kDeltaField;

    private final JLabel timePostEdgeLabel;
    private DoubleField postTimeField;

    private final JLabel nLabel;
    private DoubleField nField;

    private final JLabel deadTimeLabel;
    private DoubleField deadTimeField;

    private final JLabel eMaxLabel;
    private DoubleField eMaxField;

    private final JLabel tMaxLabel;
    private DoubleField tMaxField;

    private final JLabel durationLabel;
    private JTextField durationField;

    // //////////Panels and Checkbox/////////////////////////////////////////

    /**
     * Constructor
     */
    public ScanKTrajectoryView() {
        this(null);
    }

    public ScanKTrajectoryView(IScanKTrajectoryController scanKTrajectoryController) {
        super();
        this.scanKTrajectoryController = scanKTrajectoryController;

        eMinLabel = new JLabel("Emin");
        eDeltaPreEdgeLabel = new JLabel("Edelta");
        timePreEdgeLabel = new JLabel("Time");
        e0Label = new JLabel("E0");
        e1Label = new JLabel("E1");
        e2Label = new JLabel("E2");
        eDeltaEdgeLabel = new JLabel("Edelta");
        timeEdgeLabel = new JLabel("Time");
        mLabel = new JLabel("m");
        timeFormulaLabel = new JLabel(TIME_FORMULA_TEXT);
        timeFormulaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        timeFormulaLabel.setToolTipText(TIME_FORMULA_TOOLTIP);
        timeFormulaLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        kMinLabel = new JLabel("Kmin");
        kMaxLabel = new JLabel("Kmax");
        kDeltaLabel = new JLabel("Kdelta");
        timePostEdgeLabel = new JLabel("Tmin");
        nLabel = new JLabel("N");
        deadTimeLabel = new JLabel("Dead time (%)");
        deadTimeLabel.setToolTipText("Estimated proportion of dead time in total scan duration");
        eMaxLabel = new JLabel("Emax");
        tMaxLabel = new JLabel("Tmax");
        durationLabel = new JLabel("Estimated duration");
        durationLabel.setToolTipText(ESTIMATED_DURATION_UNKNOWN);

        this.setViewportView(getCenterPanel());
        this.setVisible(true);
        setEnabled(false);
    }

    /**
     * Returns the controller.
     */
    @Override
    public IScanKTrajectoryController getController() {
        return scanKTrajectoryController;
    }

    @Override
    public void setController(IScanKTrajectoryController controller) {
        this.scanKTrajectoryController = controller;
    }

    protected GridBagConstraints generateGridBagConstraints(int x, int y, int topGap, int rightGap) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weighty = 0;
        if (rightGap > 0 || topGap > 0) {
            constraints.insets = new Insets(topGap, 0, 0, rightGap);
        }
        return constraints;
    }

    protected TitledBorder generateGapTitleBorder(String title, int gap, Color lineColor, Color titleColor) {
        TitledBorder border;
        LineBorder line = new LineBorder(lineColor == null ? new Color(184, 207, 229) : lineColor);
        if (gap > 0) {
            border = new TitledBorder(
                    new CompoundBorder(new CompoundBorder(new EmptyBorder(gap, 0, 0, 0), line),
                            new EmptyBorder(gap, gap, gap, gap)),
                    title, TitledBorder.LEFT, TitledBorder.TOP, null, titleColor);
        } else {
            border = new TitledBorder(line, title, TitledBorder.LEFT, TitledBorder.TOP, null, titleColor);
        }
        return border;
    }

    protected TitledBorder generateGapTitleBorder(String title, int gap, Color lineColor) {
        return generateGapTitleBorder(title, gap, lineColor, null);
    }

    protected TitledBorder generateGapTitleBorder(String title, int gap) {
        return generateGapTitleBorder(title, gap, null, null);
    }

    /**
     * Returns parameters pane.
     * 
     * @return
     */
    public JPanel getParametersPanel() {
        if (parametersPanel == null) {
            parametersPanel = new JPanel();
            parametersPanel.setLayout(new GridBagLayout());

            GridBagConstraints preEdgeConstraints = generateGridBagConstraints(0, 0, 0, H_GAP);
            preEdgeConstraints.fill = GridBagConstraints.BOTH;
            GridBagConstraints connexionConstraints = generateGridBagConstraints(0, 1, 0, H_GAP);
            connexionConstraints.anchor = GridBagConstraints.SOUTH;
            GridBagConstraints edgeConstraints = generateGridBagConstraints(1, 0, 0, H_GAP);
            edgeConstraints.fill = GridBagConstraints.BOTH;
            edgeConstraints.gridheight = 2;
            GridBagConstraints postEdgeConstraints = generateGridBagConstraints(2, 0, 0, H_GAP);
            postEdgeConstraints.fill = GridBagConstraints.BOTH;
            postEdgeConstraints.gridheight = 2;
            GridBagConstraints estimateConstraints = generateGridBagConstraints(3, 0, 0, 0);
            estimateConstraints.fill = GridBagConstraints.BOTH;
            estimateConstraints.gridheight = 2;
            GridBagConstraints checkBoxConstraints = generateGridBagConstraints(0, 2, 0, 0);
            checkBoxConstraints.gridwidth = GridBagConstraints.REMAINDER;

            parametersPanel.add(getPreEdgePanel(), preEdgeConstraints);
            parametersPanel.add(getConnectionPanel(), connexionConstraints);
            parametersPanel.add(getEdgePanel(), edgeConstraints);
            parametersPanel.add(getPostEdgePanel(), postEdgeConstraints);
            parametersPanel.add(getEstimatesPanel(), estimateConstraints);
            parametersPanel.add(getModifyAdvancedParametersCheckBox(), checkBoxConstraints);

            parametersPanel.setBorder(generateGapTitleBorder("Parameters", 10, Color.BLACK));
        }
        return parametersPanel;
    }

    /**
     * Returns center pane.
     * 
     * @return
     */
    public JPanel getCenterPanel() {
        if (this.centerPanel == null) {
            centerPanel = new JPanel(new BorderLayout());
            centerPanel.add(getParametersPanel(), BorderLayout.WEST);
            centerPanel.setBorder(new EmptyBorder(5, 10, 5, 10));
        }
        return centerPanel;
    }

    /**
     * Returns edge parameters Panel.
     * 
     * @return edgePanel
     */
    public JPanel getEdgePanel() {
        if (edgePanel == null) {
            edgePanel = new JPanel();
            edgePanel.setLayout(new GridBagLayout());

            GridBagConstraints e0LabelConstraints = generateGridBagConstraints(0, 0, 0, H_GAP);
            GridBagConstraints e0FieldConstraints = generateGridBagConstraints(1, 0, 0, 0);

            GridBagConstraints e1LabelConstraints = generateGridBagConstraints(0, 1, V_GAP, H_GAP);
            GridBagConstraints e1FieldConstraints = generateGridBagConstraints(1, 1, V_GAP, 0);

            GridBagConstraints e2LabelConstraints = generateGridBagConstraints(0, 2, V_GAP, H_GAP);
            GridBagConstraints e2FieldConstraints = generateGridBagConstraints(1, 2, V_GAP, 0);

            GridBagConstraints eDeltaEdgeLabelConstraints = generateGridBagConstraints(0, 3, V_GAP, H_GAP);
            GridBagConstraints eDeltaEdgeFieldConstraints = generateGridBagConstraints(1, 3, V_GAP, 0);

            GridBagConstraints timeEdgeLabelConstraints = generateGridBagConstraints(0, 4, V_GAP, H_GAP);
            GridBagConstraints timeEdgeFieldConstraints = generateGridBagConstraints(1, 4, V_GAP, 0);

            edgePanel.add(this.e0Label, e0LabelConstraints);
            edgePanel.add(this.getE0Field(), e0FieldConstraints);

            edgePanel.add(this.e1Label, e1LabelConstraints);
            edgePanel.add(this.getE1Field(), e1FieldConstraints);

            edgePanel.add(this.e2Label, e2LabelConstraints);
            edgePanel.add(this.getE2Field(), e2FieldConstraints);

            edgePanel.add(this.eDeltaEdgeLabel, eDeltaEdgeLabelConstraints);
            edgePanel.add(this.getEDeltaEdgeField(), eDeltaEdgeFieldConstraints);

            edgePanel.add(this.timeEdgeLabel, timeEdgeLabelConstraints);
            edgePanel.add(this.getTimeEdgeField(), timeEdgeFieldConstraints);

            edgePanel.setBorder(generateGapTitleBorder("Edge", V_GAP));
        }
        return edgePanel;
    }

    /**
     * Returns preEdge parameters Panel.
     * 
     * @return preEdgePanel
     */
    public JPanel getPreEdgePanel() {
        if (preEdgePanel == null) {
            preEdgePanel = new JPanel();
            preEdgePanel.setLayout(new GridBagLayout());

            GridBagConstraints eMinLabelConstraints = generateGridBagConstraints(0, 0, 0, H_GAP);
            GridBagConstraints eMinFieldConstraints = generateGridBagConstraints(1, 0, 0, 0);

            GridBagConstraints eDeltaPreEdgeLabelConstraints = generateGridBagConstraints(0, 1, V_GAP, H_GAP);
            GridBagConstraints eDeltaPreEdgeFieldConstraints = generateGridBagConstraints(1, 1, V_GAP, 0);

            GridBagConstraints timePreEdgeLabelConstraints = generateGridBagConstraints(0, 2, V_GAP, H_GAP);
            GridBagConstraints timePreEdgeFieldConstraints = generateGridBagConstraints(1, 2, V_GAP, 0);

            preEdgePanel.add(this.eMinLabel, eMinLabelConstraints);
            preEdgePanel.add(this.getEminField(), eMinFieldConstraints);

            preEdgePanel.add(this.eDeltaPreEdgeLabel, eDeltaPreEdgeLabelConstraints);
            preEdgePanel.add(this.getEDeltaPreEdgeField(), eDeltaPreEdgeFieldConstraints);

            preEdgePanel.add(this.timePreEdgeLabel, timePreEdgeLabelConstraints);
            preEdgePanel.add(this.getTimePreEdgeField(), timePreEdgeFieldConstraints);

            preEdgePanel.setBorder(generateGapTitleBorder("Pre Edge", V_GAP));
        }
        return preEdgePanel;
    }

    /**
     * Returns postEdge parameters Panel.
     * 
     * @return postEdgePanel
     */
    public JPanel getPostEdgePanel() {
        if (postEdgePanel == null) {
            postEdgePanel = new JPanel(new BorderLayout(0, 5));
            postEdgeInnerPanel = new JPanel(new GridBagLayout());

            GridBagConstraints kMinLabelConstraints = generateGridBagConstraints(0, 0, 0, H_GAP);
            GridBagConstraints kMinFieldConstraints = generateGridBagConstraints(1, 0, 0, 0);

            GridBagConstraints kMaxLabelConstraints = generateGridBagConstraints(0, 1, V_GAP, H_GAP);
            GridBagConstraints kMaxFieldConstraints = generateGridBagConstraints(1, 1, V_GAP, 0);

            GridBagConstraints kDeltaLabelConstraints = generateGridBagConstraints(0, 2, V_GAP, H_GAP);
            GridBagConstraints kDeltaFieldConstraints = generateGridBagConstraints(1, 2, V_GAP, 0);

            GridBagConstraints timeEdgeLabelConstraints = generateGridBagConstraints(0, 3, V_GAP, H_GAP);
            GridBagConstraints timeEdgeFieldConstraints = generateGridBagConstraints(1, 3, V_GAP, 0);

            GridBagConstraints nLabelConstraints = generateGridBagConstraints(0, 4, V_GAP, H_GAP);
            GridBagConstraints nFieldConstraints = generateGridBagConstraints(1, 4, V_GAP, 0);

            postEdgeInnerPanel.add(kMinLabel, kMinLabelConstraints);
            postEdgeInnerPanel.add(getKMinField(), kMinFieldConstraints);

            postEdgeInnerPanel.add(kMaxLabel, kMaxLabelConstraints);
            postEdgeInnerPanel.add(getKMaxField(), kMaxFieldConstraints);

            postEdgeInnerPanel.add(kDeltaLabel, kDeltaLabelConstraints);
            postEdgeInnerPanel.add(getKDeltaField(), kDeltaFieldConstraints);

            postEdgeInnerPanel.add(timePostEdgeLabel, timeEdgeLabelConstraints);
            postEdgeInnerPanel.add(getPostEdgeTimeField(), timeEdgeFieldConstraints);

            postEdgeInnerPanel.add(nLabel, nLabelConstraints);
            postEdgeInnerPanel.add(getNField(), nFieldConstraints);

            postEdgePanel.add(postEdgeInnerPanel, BorderLayout.CENTER);
            postEdgePanel.add(timeFormulaLabel, BorderLayout.SOUTH);

            postEdgePanel.setBorder(generateGapTitleBorder("Post Edge", V_GAP));
        }
        return postEdgePanel;
    }

    /**
     * Returns connection parameters Panel.
     * 
     * @return connexionPanel
     */
    public JPanel getConnectionPanel() {
        if (connectionPanel == null) {
            connectionPanel = new JPanel();
            connectionPanel.setLayout(new GridBagLayout());

            GridBagConstraints mLabelConstraints = generateGridBagConstraints(0, 0, 0, H_GAP);
            GridBagConstraints mFieldConstraints = generateGridBagConstraints(1, 0, 0, 0);

            connectionPanel.add(mLabel, mLabelConstraints);
            connectionPanel.add(getMField(), mFieldConstraints);
            connectionPanel.setBorder(generateGapTitleBorder("Connection", V_GAP));
        }
        return connectionPanel;
    }

    public JPanel getEstimatesPanel() {
        if (estimatesPanel == null) {
            estimatesPanel = new JPanel(new GridBagLayout());

            GridBagConstraints deadTimeLabelConstraints = generateGridBagConstraints(0, 0, 0, H_GAP);
            GridBagConstraints deadTimeFieldConstraints = generateGridBagConstraints(1, 0, 0, 0);

            GridBagConstraints eMaxLabelConstraints = generateGridBagConstraints(0, 1, V_GAP, H_GAP);
            GridBagConstraints eMaxFieldConstraints = generateGridBagConstraints(1, 1, V_GAP, 0);

            GridBagConstraints tMaxLabelConstraints = generateGridBagConstraints(0, 2, V_GAP, H_GAP);
            GridBagConstraints tMaxFieldConstraints = generateGridBagConstraints(1, 2, V_GAP, 0);

            GridBagConstraints durationLabelConstraints = generateGridBagConstraints(0, 3, V_GAP, H_GAP);
            GridBagConstraints durationFieldConstraints = generateGridBagConstraints(1, 3, V_GAP, 0);
            durationFieldConstraints.gridwidth = GridBagConstraints.REMAINDER;

            estimatesPanel.add(deadTimeLabel, deadTimeLabelConstraints);
            estimatesPanel.add(getDeadTimeField(), deadTimeFieldConstraints);
            estimatesPanel.add(eMaxLabel, eMaxLabelConstraints);
            estimatesPanel.add(getEMaxField(), eMaxFieldConstraints);
            estimatesPanel.add(tMaxLabel, tMaxLabelConstraints);
            estimatesPanel.add(getTMaxField(), tMaxFieldConstraints);
            estimatesPanel.add(durationLabel, durationLabelConstraints);
            estimatesPanel.add(getDurationField(), durationFieldConstraints);

            estimatesPanel.setBorder(generateGapTitleBorder("Estimates", V_GAP));
        }
        return estimatesPanel;
    }

    /**
     * Returns connexion parameters Panel.
     * 
     * @return connexionPanel
     */
    public ConstrainedCheckBox getModifyAdvancedParametersCheckBox() {
        if (modifyAdvancedParametersCheckBox == null) {
            modifyAdvancedParametersCheckBox = new ConstrainedCheckBox();
            modifyAdvancedParametersCheckBox.setText("Modify advanced parameters (m, Kdelta)");
            modifyAdvancedParametersCheckBox.setSelected(false);
            modifyAdvancedParametersCheckBox.addActionListener((e) -> {
                boolean editable = modifyAdvancedParametersCheckBox.isSelected();
                setEditable(getMField(), editable);
                setEditable(getKDeltaField(), editable);
            });
        }
        return modifyAdvancedParametersCheckBox;
    }

    // ///////////// Utility methods ////////////////////////////////////

    protected void setEditable(JTextField field, boolean editable) {
        field.setEditable(editable);
        field.setOpaque(editable);
    }

    protected DoubleField generateDoubleField(boolean editable) {
        DoubleField field = new DoubleField();
        field.setColumns(COLUMNS);
        if (!editable) {
            setEditable(field, editable);
        }
        return field;
    }

    protected void prepareDoubleField(DoubleField field, Runnable notifyMethod, Runnable setBackgroundMethod) {
        setBackgroundMethod.run();
        field.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
                if (field.isEditable() && field.isEnabled()) {
                    if (arg0.getKeyChar() == KeyEvent.VK_ENTER) {
                        notifyMethod.run();
                    }
                    setBackgroundMethod.run();
                }
            }
        });
        field.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                notifyMethod.run();
                setBackgroundMethod.run();
            }
        });
    }

    // ///////////// Labels and fields //////////////////////////////////

    // /---- Pre Edge fields -----///////

    public TextField getEminField() {
        if (eMinField == null) {
            eMinField = generateDoubleField(true);
            prepareDoubleField(eMinField, this::notifyEminValueChanged, this::setBackgroundEMin);
        }
        return eMinField;
    }

    private void notifyEminValueChanged() {
        if (TrajectoryListView.validateField(eMinField.getText())) {
            if (scanKTrajectoryController != null) {
                eMinValue = eMinField.getText();
                scanKTrajectoryController.notifyEminFieldChange(Double.parseDouble(eMinValue));
            }
        }
    }

    public TextField getEDeltaPreEdgeField() {
        if (eDeltaPreEdgeField == null) {
            eDeltaPreEdgeField = generateDoubleField(true);
            prepareDoubleField(eDeltaPreEdgeField, this::notifyEDeltaPreEdgeValueChanged,
                    this::setBackgroundEDeltaPreEdge);
        }
        return eDeltaPreEdgeField;
    }

    private void notifyEDeltaPreEdgeValueChanged() {
        if (TrajectoryListView.validateField(eDeltaPreEdgeField.getText())) {
            if (scanKTrajectoryController != null) {
                eDeltaPreEdgeValue = eDeltaPreEdgeField.getText();
                scanKTrajectoryController.notifyEDeltaPreEdgeFieldChange(Double.parseDouble(eDeltaPreEdgeValue));
            }
        }
    }

    public TextField getTimePreEdgeField() {
        if (timePreEdgeField == null) {
            timePreEdgeField = generateDoubleField(true);
            prepareDoubleField(timePreEdgeField, this::notifyTimePreEdgeValueChanged, this::setBackgroundTimePreEdge);
        }
        return timePreEdgeField;
    }

    private void notifyTimePreEdgeValueChanged() {
        if (TrajectoryListView.validateField(timePreEdgeField.getText())) {
            if (scanKTrajectoryController != null) {
                timePreEdgeValue = timePreEdgeField.getText();
                scanKTrajectoryController.notifyTimePreEdgeFieldChange(Double.parseDouble(timePreEdgeValue));
            }
        }
    }

    // /---- Edge fields -----///////

    public TextField getE0Field() {
        if (e0Field == null) {
            e0Field = generateDoubleField(true);
            prepareDoubleField(e0Field, this::notifyE0ValueChanged, this::setBackgroundE0);
        }
        return e0Field;
    }

    private void notifyE0ValueChanged() {
        if (TrajectoryListView.validateField(e0Field.getText())) {
            if (scanKTrajectoryController != null) {
                e0Value = e0Field.getText();
                scanKTrajectoryController.notifyE0FieldChange(Double.parseDouble(e0Value));
            }
        }
    }

    public TextField getE1Field() {
        if (e1Field == null) {
            e1Field = generateDoubleField(true);
            prepareDoubleField(e1Field, this::notifyE1ValueChanged, this::setBackgroundE1);
        }
        return e1Field;
    }

    private void notifyE1ValueChanged() {
        if (TrajectoryListView.validateField(e1Field.getText())) {
            if (scanKTrajectoryController != null) {
                e1Value = e1Field.getText();
                scanKTrajectoryController.notifyE1FieldChange(Double.parseDouble(e1Value));
            }
        }
    }

    public TextField getE2Field() {
        if (e2Field == null) {
            e2Field = generateDoubleField(true);
            prepareDoubleField(e2Field, this::notifyE2ValueChanged, this::setBackgroundE2);
        }
        return e2Field;
    }

    private void notifyE2ValueChanged() {
        if (TrajectoryListView.validateField(e2Field.getText())) {
            if (scanKTrajectoryController != null) {
                e2Value = e2Field.getText();
                scanKTrajectoryController.notifyE2FieldChange(Double.parseDouble(e2Value));
            }
        }
    }

    public TextField getEDeltaEdgeField() {
        if (eDeltaEdgeField == null) {
            eDeltaEdgeField = generateDoubleField(true);
            prepareDoubleField(eDeltaEdgeField, this::notifyEDeltaEdgeValueChanged, this::setBackgroundEDeltaEdge);
        }
        return eDeltaEdgeField;
    }

    private void notifyEDeltaEdgeValueChanged() {
        if (TrajectoryListView.validateField(eDeltaEdgeField.getText())) {
            if (scanKTrajectoryController != null) {
                eDeltaEdgeValue = eDeltaEdgeField.getText();
                scanKTrajectoryController.notifyEDeltaEdgeFieldChange(Double.parseDouble(eDeltaEdgeValue));
            }
        }
    }

    public TextField getTimeEdgeField() {
        if (timeEdgeField == null) {
            timeEdgeField = generateDoubleField(true);
            prepareDoubleField(timeEdgeField, this::notifyTimeEdgeValueChanged, this::setBackgroundTimeEdge);
        }
        return timeEdgeField;
    }

    private void notifyTimeEdgeValueChanged() {
        if (TrajectoryListView.validateField(timeEdgeField.getText())) {
            if (scanKTrajectoryController != null) {
                timeEdgeValue = timeEdgeField.getText();
                scanKTrajectoryController.notifyTimeEdgeFieldChange(Double.parseDouble(timeEdgeValue));
            }
        }
    }

    // /---- Post Edge fields -----///////

    public TextField getKMinField() {
        if (kMinField == null) {
            kMinField = generateDoubleField(false);
            prepareDoubleField(kMinField, this::notifyKMinValueChanged, this::setBackgroundKMin);
        }
        return kMinField;
    }

    private void notifyKMinValueChanged() {
        if (TrajectoryListView.validateField(kMinField.getText())) {
            if (scanKTrajectoryController != null) {
                kMinValue = kMinField.getText();
                scanKTrajectoryController.notifyKMinFieldChange(Double.parseDouble(kMinValue));
            }
        }
    }

    public TextField getKMaxField() {
        if (kMaxField == null) {
            kMaxField = generateDoubleField(true);
            prepareDoubleField(kMaxField, this::notifyKMaxValueChanged, this::setBackgroundKMax);
        }
        return kMaxField;
    }

    private void notifyKMaxValueChanged() {
        if (TrajectoryListView.validateField(kMaxField.getText())) {
            if (scanKTrajectoryController != null) {
                kMaxValue = kMaxField.getText();
                scanKTrajectoryController.notifyKMaxFieldChange(Double.parseDouble(kMaxValue));
            }
        }
    }

    public TextField getKDeltaField() {
        if (kDeltaField == null) {
            kDeltaField = generateDoubleField(false);
            prepareDoubleField(kDeltaField, this::notifyKDeltaValueChanged, this::setBackgroundKDelta);
        }
        return kDeltaField;
    }

    private void notifyKDeltaValueChanged() {
        if (TrajectoryListView.validateField(kDeltaField.getText())) {
            if (scanKTrajectoryController != null) {
                kDeltaValue = kDeltaField.getText();
                scanKTrajectoryController.notifyKDeltaFieldChange(Double.parseDouble(kDeltaValue));
            }
        }
    }

    public TextField getPostEdgeTimeField() {
        if (postTimeField == null) {
            postTimeField = generateDoubleField(true);
            prepareDoubleField(postTimeField, this::notifyPostEdgeTimeValueChanged, this::setBackgroundTimePostEdge);
        }
        return postTimeField;
    }

    private void notifyPostEdgeTimeValueChanged() {
        if (TrajectoryListView.validateField(postTimeField.getText())) {
            if (scanKTrajectoryController != null) {
                timePostEdgeValue = postTimeField.getText();
                scanKTrajectoryController.notifyPostEdgeTimeFieldChange(Double.parseDouble(timePostEdgeValue));
            }
        }
    }

    public TextField getNField() {
        if (nField == null) {
            nField = generateDoubleField(true);
            prepareDoubleField(nField, this::notifyNValueChanged, this::setBackgroundN);
        }
        return nField;
    }

    private void notifyNValueChanged() {
        if (TrajectoryListView.validateField(nField.getText())) {
            if (scanKTrajectoryController != null) {
                nValue = nField.getText();
                scanKTrajectoryController.notifyNFieldChange(Double.parseDouble(nValue));
            }
        }
    }

    // /---- Connexion fields -----///////

    public TextField getMField() {
        if (mField == null) {
            mField = generateDoubleField(false);
            prepareDoubleField(mField, this::notifyMValueChanged, this::setBackgroundM);
        }
        return mField;
    }

    private void notifyMValueChanged() {
        if (TrajectoryListView.validateField(mField.getText())) {
            if (scanKTrajectoryController != null) {
                mValue = mField.getText();
                scanKTrajectoryController.notifyMFieldChange(Double.parseDouble(mValue));
            }
        }
    }

    // /---- Estimates fields -----///////

    public DoubleField getDeadTimeField() {
        if (deadTimeField == null) {
            deadTimeField = generateDoubleField(true);
            prepareDoubleField(deadTimeField, this::notifyDeadTimeValueChanged, this::setBackgroundDeadTime);
        }
        return deadTimeField;
    }

    private void notifyDeadTimeValueChanged() {
        if (TrajectoryListView.validateField(deadTimeField.getText())) {
            if (scanKTrajectoryController != null) {
                deadTimeValue = deadTimeField.getText();
                scanKTrajectoryController.notifyDeadTimeFieldChange(Double.parseDouble(deadTimeValue));
            }
        }
    }

    public DoubleField getEMaxField() {
        if (eMaxField == null) {
            eMaxField = generateDoubleField(false);
        }
        return eMaxField;
    }

    public DoubleField getTMaxField() {
        if (tMaxField == null) {
            tMaxField = generateDoubleField(false);
        }
        return tMaxField;
    }

    public JTextField getDurationField() {
        if (durationField == null) {
            durationField = new JTextField(10);
            durationField.setToolTipText(ESTIMATED_DURATION_UNKNOWN);
            setEditable(durationField, false);
        }
        return durationField;
    }

    // -------------------------------------------------------------//

    /**
     * Parameters SETS
     */

    public void setEMinField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbEMinValue = value;
        } else {
            this.eMinValue = value;
            getEminField().setText(value);
        }
        setBackgroundEMin();
    }

    private void setBackgroundEMin() {
        BackgroundRenderer.setBackgroundField(getEminField(), eMinValue, dbEMinValue);
    }

    public void setEDeltaPreEdgeField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbEDeltaPreEdgeValue = value;
        } else {
            this.eDeltaPreEdgeValue = value;
            getEDeltaPreEdgeField().setText(value);
        }
        setBackgroundEDeltaPreEdge();
    }

    private void setBackgroundEDeltaPreEdge() {
        BackgroundRenderer.setBackgroundField(getEDeltaPreEdgeField(), eDeltaPreEdgeValue, dbEDeltaPreEdgeValue);
    }

    public void setTimePreEdgeField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbtimePreEdgeValue = value;
        } else {
            this.timePreEdgeValue = value;
            getTimePreEdgeField().setText(value);
        }
        setBackgroundTimePreEdge();
    }

    private void setBackgroundTimePreEdge() {
        BackgroundRenderer.setBackgroundField(getTimePreEdgeField(), timePreEdgeValue, dbtimePreEdgeValue);
    }

    public void setTimePostEdgeField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbtimePostEdgeValue = value;
        } else {
            this.timePostEdgeValue = value;
            getPostEdgeTimeField().setText(value);
        }
        setBackgroundTimePostEdge();
    }

    private void setBackgroundTimePostEdge() {
        BackgroundRenderer.setBackgroundField(getPostEdgeTimeField(), timePostEdgeValue, dbtimePostEdgeValue);
    }

    public void setE0Field(String value, boolean savedValue) {
        if (savedValue) {
            this.dbe0Value = value;
        } else {
            this.e0Value = value;
            getE0Field().setText(value);
        }
        setBackgroundE0();
    }

    private void setBackgroundE0() {
        BackgroundRenderer.setBackgroundField(getE0Field(), e0Value, dbe0Value);
    }

    public void setE1Field(String value, boolean savedValue) {
        if (savedValue) {
            this.dbe1Value = value;
        } else {
            this.e1Value = value;
            getE1Field().setText(value);
        }
        setBackgroundE1();
    }

    private void setBackgroundE1() {
        BackgroundRenderer.setBackgroundField(getE1Field(), e1Value, dbe1Value);
    }

    public void setE2Field(String value, boolean savedValue) {
        if (savedValue) {
            this.dbe2Value = value;
        } else {
            this.e2Value = value;
            getE2Field().setText(value);
        }
        setBackgroundE2();
    }

    private void setBackgroundE2() {
        BackgroundRenderer.setBackgroundField(getE2Field(), e2Value, dbe2Value);
    }

    public void setEDeltaEdgeField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbeDeltaEdgeValue = value;
        } else {
            this.eDeltaEdgeValue = value;
            getEDeltaEdgeField().setText(value);
        }
        setBackgroundEDeltaEdge();
    }

    private void setBackgroundEDeltaEdge() {
        BackgroundRenderer.setBackgroundField(getEDeltaEdgeField(), eDeltaEdgeValue, dbeDeltaEdgeValue);
    }

    public void setTimeEdgeField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbtimeEdgeValue = value;
        } else {
            this.timeEdgeValue = value;
            getTimeEdgeField().setText(value);
        }
        setBackgroundTimeEdge();
    }

    private void setBackgroundTimeEdge() {
        BackgroundRenderer.setBackgroundField(getTimeEdgeField(), timeEdgeValue, dbtimeEdgeValue);
    }

    public void setMField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbmValue = value;
        } else {
            this.mValue = value;
            getMField().setText(value);
        }
        setBackgroundM();
    }

    private void setBackgroundM() {
        BackgroundRenderer.setBackgroundField(getMField(), mValue, dbmValue);
    }

    public void setKMin(String value, boolean savedValue) {
        if (savedValue) {
            this.dbkMinValue = value;
        } else {
            this.kMinValue = value;
            getKMinField().setText(value);
        }
        setBackgroundKMin();

    }

    private void setBackgroundKMin() {
        BackgroundRenderer.setBackgroundField(getKMinField(), kMinValue, dbkMinValue);
    }

    public void setKMaxField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbkMaxValue = value;
        } else {
            this.kMaxValue = value;
            getKMaxField().setText(value);
        }
        setBackgroundKMax();
    }

    private void setBackgroundKMax() {
        BackgroundRenderer.setBackgroundField(getKMaxField(), kMaxValue, dbkMaxValue);
    }

    public void setKDeltaField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbkDeltaValue = value;
        } else {
            this.kDeltaValue = value;
            getKDeltaField().setText(value);
        }
        setBackgroundKDelta();
    }

    private void setBackgroundKDelta() {
        BackgroundRenderer.setBackgroundField(getKDeltaField(), kDeltaValue, dbkDeltaValue);
    }

    public void setNField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbnValue = value;
        } else {
            this.nValue = value;
            getNField().setText(value);
        }
        setBackgroundN();
    }

    private void setBackgroundN() {
        BackgroundRenderer.setBackgroundField(getNField(), nValue, dbnValue);
    }

    public void setDeadTimeField(String value, boolean savedValue) {
        if (savedValue) {
            this.dbDeadTimeValue = value;
        } else {
            this.deadTimeValue = value;
            getDeadTimeField().setText(value);
        }
        setBackgroundDeadTime();
    }

    private void setBackgroundDeadTime() {
        BackgroundRenderer.setBackgroundField(getDeadTimeField(), deadTimeValue, dbDeadTimeValue);
    }

    public void setEMaxField(String value) {
        getEMaxField().setText(value);
    }

    public void setTMaxField(String value) {
        getTMaxField().setText(value);
    }

    public void setDurationField(String value) {
        getDurationField().setText(value);
        String tooltip = (!SalsaUtils.isDefined(value)) || StringScalarBox.DEFAULT_ERROR_STRING.equals(value)
                ? ESTIMATED_DURATION_UNKNOWN
                : String.format(ESTIMATED_DURATION_FORMAT, value);
        getDurationField().setToolTipText(tooltip);
        durationLabel.setToolTipText(tooltip);
    }

    /**
     * Parameters Values GETS
     */

    public double getEMin() {
        return Double.parseDouble(getEminField().getText());
    }

    public double getEDeltaPreEdge() {
        return Double.parseDouble(getEDeltaPreEdgeField().getText());
    }

    public double getTimePreEdge() {
        return Double.parseDouble(getTimePreEdgeField().getText());
    }

    public double getTimePostEdge() {
        return Double.parseDouble(getPostEdgeTimeField().getText());
    }

    public double getE0() {
        return Double.parseDouble(getE0Field().getText());
    }

    public double getE1() {
        return Double.parseDouble(getE1Field().getText());
    }

    public double getE2() {
        return Double.parseDouble(getE2Field().getText());
    }

    public double getEDeltaEdge() {
        return Double.parseDouble(getEDeltaEdgeField().getText());
    }

    public double getTimeEdge() {
        return Double.parseDouble(getTimeEdgeField().getText());
    }

    public double getM() {
        return Double.parseDouble(getMField().getText());
    }

    public double getKMin() {
        return Double.parseDouble(getKMinField().getText());
    }

    public double getKMax() {
        return Double.parseDouble(getKMaxField().getText());
    }

    public double getKDelta() {
        return Double.parseDouble(getKDeltaField().getText());
    }

    public double getN() {
        return Double.parseDouble(getNField().getText());
    }

    @Override
    public void setEnabled(boolean enabled) {
        getEminField().setEnabled(enabled);
        getEDeltaEdgeField().setEnabled(enabled);
        getEDeltaPreEdgeField().setEnabled(enabled);
        getTimeEdgeField().setEnabled(enabled);
        getTimePreEdgeField().setEnabled(enabled);
        getPostEdgeTimeField().setEnabled(enabled);
        getE0Field().setEnabled(enabled);
        getE1Field().setEnabled(enabled);
        getE2Field().setEnabled(enabled);
        getKMaxField().setEnabled(enabled);
        getKDeltaField().setEnabled(enabled);
        getNField().setEnabled(enabled);
        getMField().setEnabled(enabled);
        getKMinField().setEnabled(enabled);
        getModifyAdvancedParametersCheckBox().setEnabled(enabled);

        if (!enabled) {
            setEditable(getMField(), false);
            setEditable(getKMinField(), false);
            setEditable(getKDeltaField(), false);

            getEminField().setText(ObjectUtils.EMPTY_STRING);
            getEDeltaEdgeField().setText(ObjectUtils.EMPTY_STRING);
            getEDeltaPreEdgeField().setText(ObjectUtils.EMPTY_STRING);
            getTimeEdgeField().setText(ObjectUtils.EMPTY_STRING);
            getTimePreEdgeField().setText(ObjectUtils.EMPTY_STRING);
            getPostEdgeTimeField().setText(ObjectUtils.EMPTY_STRING);
            getE0Field().setText(ObjectUtils.EMPTY_STRING);
            getE1Field().setText(ObjectUtils.EMPTY_STRING);
            getE2Field().setText(ObjectUtils.EMPTY_STRING);
            getKMaxField().setText(ObjectUtils.EMPTY_STRING);
            getKDeltaField().setText(ObjectUtils.EMPTY_STRING);
            getNField().setText(ObjectUtils.EMPTY_STRING);
            getMField().setText(ObjectUtils.EMPTY_STRING);
            getKMinField().setText(ObjectUtils.EMPTY_STRING);
            getModifyAdvancedParametersCheckBox().setSelected(false);

            getEminField().setBackground(Color.LIGHT_GRAY);
            getEminField().setBackground(Color.LIGHT_GRAY);
            getEDeltaEdgeField().setBackground(Color.LIGHT_GRAY);
            getEDeltaPreEdgeField().setBackground(Color.LIGHT_GRAY);
            getTimeEdgeField().setBackground(Color.LIGHT_GRAY);
            getTimePreEdgeField().setBackground(Color.LIGHT_GRAY);
            getPostEdgeTimeField().setBackground(Color.LIGHT_GRAY);
            getE0Field().setBackground(Color.LIGHT_GRAY);
            getE1Field().setBackground(Color.LIGHT_GRAY);
            getE2Field().setBackground(Color.LIGHT_GRAY);
            getKMaxField().setBackground(Color.LIGHT_GRAY);
            getKDeltaField().setBackground(Color.LIGHT_GRAY);
            getNField().setBackground(Color.LIGHT_GRAY);
            getMField().setBackground(Color.LIGHT_GRAY);
            getKMinField().setBackground(Color.LIGHT_GRAY);
        }
    }
}

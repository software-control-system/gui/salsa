package fr.soleil.salsa.client.util;

/****
 * Model data which contain the different properties of Scan energy view.
 * 
 * @author Tarek
 */
public class EnergyTrajectoryLine {

    private Integer range;

    /**
     * Begin position
     */
    private NumberValue startEnergy = new NumberValue();

    /**
     * End position
     */
    private NumberValue endEnergy = new NumberValue();

    /**
     * The Delta
     */
    private NumberValue energyStep = new NumberValue();

    /**
     * The step number
     */
    private NumberValue stepNumber = new NumberValue();

    private Boolean deltaConstant = false;

    /**
     * The Integration time
     */
    private NumberValue IntegrationTime = new NumberValue();

    public EnergyTrajectoryLine() {
        startEnergy.setDoubleValue(0.0);
        endEnergy.setDoubleValue(0.0);
        energyStep.setDoubleValue(0.0);
        stepNumber.setIntegerValue(1);
        IntegrationTime.setDoubleValue(1.0);
    }

    public NumberValue getBeginPosition() {
        return startEnergy;
    }

    public void setBeginPosition(Double startEnergy) {
        this.startEnergy.setDoubleValue(startEnergy);

    }

    public NumberValue getEndPosition() {
        return endEnergy;
    }

    public void setEndPosition(Double endEnergy) {
        this.endEnergy.setDoubleValue(endEnergy);
    }

    public NumberValue getDelta() {
        return energyStep;
    }

    public void setDelta(Double energyStep) {
        this.energyStep.setDoubleValue(energyStep);
    }

    public NumberValue getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber.setIntegerValue(stepNumber);
    }

    public NumberValue getIntegrationTime() {
        return IntegrationTime;
    }

    public void setIntegrationTime(Double IntegrationTime) {
        this.IntegrationTime.setDoubleValue(IntegrationTime);
    }

    public Integer getRange() {
        return range;
    }

    public void setRange(Integer range) {
        this.range = range;
    }

    public Boolean getDeltaConstant() {
        return deltaConstant;
    }

    public void setDeltaConstant(Boolean deltaConstant) {
        this.deltaConstant = deltaConstant;
    }

    @Override
    public boolean equals(Object obj) {

        EnergyTrajectoryLine model = null;
        if (obj instanceof EnergyTrajectoryLine)
            model = (EnergyTrajectoryLine) obj;
        else
            return false;

        if (this.getBeginPosition() == model.getBeginPosition()
                && this.getEndPosition() == model.getEndPosition()
                && this.getDelta() == model.getDelta()
                && this.getStepNumber() == model.getStepNumber()
                && this.getIntegrationTime() == model.getIntegrationTime()
                && this.getDeltaConstant() == model.getDeltaConstant())
            return true;
        else
            return false;

    }
}

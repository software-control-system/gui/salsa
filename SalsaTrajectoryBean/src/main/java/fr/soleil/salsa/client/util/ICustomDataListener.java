package fr.soleil.salsa.client.util;

public interface ICustomDataListener {

    public void customDataChange(double[] doubleValues);

}

package fr.soleil.salsa.client.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JMenuItem;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.salsa.client.util.CustomDataValue;
import fr.soleil.salsa.client.util.ICustomDataListener;

public class TrajectoryChart extends Chart implements ICustomDataListener {

    private static final long serialVersionUID = -3574910418804088529L;

    private CustomDataValue value = new CustomDataValue();
    private Map<String, Object> datas = new HashMap<String, Object>();
    private static final String VIEWID = "Trajectory";

    private TrajectoryChart detailChart = null;
    private JFrame detailFrame = new JFrame();

    public TrajectoryChart() {
        this(false);
    }

    private TrajectoryChart(boolean light) {
        super();
        setManagementPanelVisible(false);
        setFreezePanelVisible(false);
        setLegendVisible(false);
        setMathExpressionEnabled(false);
        setAutoScale(true, IChartViewer.X);
        setAutoScale(true, IChartViewer.Y1);
        setAxisDrawOpposite(true, IChartViewer.X);
        setAxisDrawOpposite(true, IChartViewer.Y1);
        setAxisSelectionVisible(false);

        setEditable(false);

        setMenuVisible(Chart.MENU_CHART_PROPERTIES, false);
        setMenuVisible(Chart.MENU_DATAVIEW_PROPERTIES, false);
        setMenuVisible(Chart.MENU_SAVE_DATA_FILE, false);
        setMenuVisible(Chart.MENU_SHOW_STATISTICS, false);
        setMenuVisible(Chart.MENU_SAVE_SETTINGS, false);
        setMenuVisible(Chart.MENU_LOAD_SETTINGS, false);
        setMenuVisible(Chart.MENU_LOAD_DATA, false);
        setMenuVisible(Chart.MENU_SAVE_SNAPSHOT, false);
        setMenuVisible(Chart.MENU_PRINT_CHART, false);
        setMenuVisible(Chart.MENU_ABSCISSA_MARGIN_ERROR, false);
        setMenuVisible(Chart.MENU_EVALUATE_EXPRESSION, false);
        setMenuVisible(Chart.MENU_CLEAR_EXPRESSIONS, false);
        setMenuVisible(Chart.MENU_FREEZE_VALUES, false);
        setMenuVisible(Chart.MENU_CLEAR_FREEZES, false);

        if (!light) {
            detailChart = new TrajectoryChart(true);
            value.addCustomTrajectoryListener(this);

            // Custom menu
            addMenuSeparator();
            detailFrame.setContentPane(detailChart);
            detailChart.setSize(400, 300);
            detailFrame.setSize(detailChart.getSize());
            detailFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            detailFrame.setTitle("Graph details");
            detailFrame.setLocationRelativeTo(this);

            JMenuItem showGraph = new JMenuItem("Show graph details");
            addMenuItem(showGraph);
            showGraph.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    detailFrame.setVisible(true);
                    detailFrame.toFront();
                }
            });

            JMenuItem modifyMenu = new JMenuItem("Modify trajectory");
            addMenuItem(modifyMenu);
            modifyMenu.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    showTextAreaEditor();
                }
            });
        }

    }

    public void addCustomTrajectoryListener(ICustomDataListener listener) {
        value.addCustomTrajectoryListener(listener);
    }

    public void removeCustomTrajectoryListener(ICustomDataListener listener) {
        value.removeCustomTrajectoryListener(listener);
    }

    private void showTextAreaEditor() {
        CustomDataTexteArea.setCustomTrajectoryValue(value);
        CustomDataTexteArea.showFrame(this);
    }

    public void setTrajectory(double[] customTrajectory) {
        value.setCustomData(customTrajectory);
    }

    @Override
    public void customDataChange(double[] doubleValues) {
        setDatas(doubleValues);
    }

    private void setDatas(double[] doubleValues) {
        if (doubleValues != null) {
            if (detailChart != null) {
                detailChart.setDatas(doubleValues);
            }
            double[] newDoubleValues = new double[doubleValues.length * 2];
            for (int i = 0; i < doubleValues.length; i++) {
                newDoubleValues[i * 2] = i;
                newDoubleValues[i * 2 + 1] = doubleValues[i];
            }

            datas.put(VIEWID, newDoubleValues);
            setData(datas);
            setDataViewCometeColor(VIEWID, CometeColor.RED);
            setDataViewAxis(VIEWID, IChartViewer.Y1);
            // setDataViewClickable(VIEWID, false);
            setDataViewMarkerStyle(VIEWID, IChartViewer.MARKER_BOX);
            setDataViewMarkerCometeColor(VIEWID, CometeColor.RED);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        TrajectoryChart chart = new TrajectoryChart();
        chart.setSize(150, 200);
        frame.setContentPane(chart);
        frame.setSize(chart.getSize());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        double[] doubleValue = new double[1000];
        for (int i = 0; i < doubleValue.length; i++) {
            doubleValue[i] = Math.sin(i);
        }
        chart.setTrajectory(doubleValue);

    }
}

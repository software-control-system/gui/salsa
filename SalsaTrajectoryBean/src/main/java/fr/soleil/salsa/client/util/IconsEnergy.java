package fr.soleil.salsa.client.util;

import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class IconsEnergy {

    private static final String BUNDLE_NAME = "fr.soleil.salsa.client.view.energy.icons.iconsEnergy"; //$NON-NLS-1$

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    public static Icon getIconEnergy(String key) {
        ImageIcon icon = null;

        try {
            URL url = ClassLoader.getSystemResource(RESOURCE_BUNDLE.getString(key));
            if (url != null)
                icon = new ImageIcon(url);
        }
        catch (MissingResourceException e) {
            icon = null;
        }
        return icon;
    }

}

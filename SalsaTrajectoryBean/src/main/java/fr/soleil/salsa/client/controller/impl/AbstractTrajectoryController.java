package fr.soleil.salsa.client.controller.impl;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.view.ITrajectoryView;
import fr.soleil.salsa.client.view.IView;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeIntegrated;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;
import fr.soleil.salsa.exception.ScanNotFoundException;

public abstract class AbstractTrajectoryController<T extends IView<?>> extends AController<T> {
    /***
     * The config model
     */
    protected IConfig<?> config = null;
    protected IDimension dimension = null;
    protected IConfig<?> dbConfig = null;
    protected IDimension dbDimension = null;

    protected ITrajectoryView trajectoryview = null;
    protected boolean yController = false;

    /**
     * Listener for dimension1D.
     */
    private final IListener<EntityPropertyChangedEvent<IConfig<?>>> configListener = new IListener<EntityPropertyChangedEvent<IConfig<?>>>() {
        @Override
        public void notifyEvent(final EntityPropertyChangedEvent<IConfig<?>> event) {

            if ((config != null) && (event != null)) {
                List<EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?>> propertyList = event
                        .getPropertyChangeList();
                if (propertyList != null) {
                    for (EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?> propertyChange : propertyList) {
                        String propertyName = propertyChange.getPropertyName();
                        Object newValue = propertyChange.getNewValue();
                        if ((propertyName != null) && (newValue != null)) {
                            if (propertyName.startsWith("addActuator") && (newValue instanceof IActuator)) {
                                if ((propertyName.endsWith("Y") == yController)) {
                                    notifyAddActuator((IActuator) newValue);
                                }
                            }
                            if ("renameActuator".equals(propertyName)) {
                                Object oldValue = propertyChange.getOldValue();
                                if ((trajectoryview != null) && (oldValue != null)) {
                                    trajectoryview.setActuatorName(oldValue.toString(), newValue.toString());
                                }
                            }
                            if ("deleteActuator".equals(propertyName) && (newValue instanceof IActuator)) {
                                trajectoryview.deleteDevice((IActuator) newValue);
                            }
                            // Is called when a actuator is moved (up or down button)
                            if (propertyName.startsWith("swapActuator")) {
                                if ((propertyName.endsWith("Y") == yController)) {
                                    setConfig(config);
                                }
                            }

                            if (propertyName.startsWith("swapActuator")) {
                                if ((propertyName.endsWith("Y") == yController)) {
                                    setConfig(config);
                                }
                            }

                            if ("setActuatorEnable".equals(propertyName)) {
                                setConfig(config);
                            }
                        }
                    }
                }
            }

        };
    };

    public AbstractTrajectoryController() {
        this(null);
    }

    public AbstractTrajectoryController(final T view) {
        super(view);
    }

    public void setTrajectoryView(final ITrajectoryView aview) {
        if (aview != null) {
            trajectoryview = aview;
            setYTrajectory(yController);
        }
    }

    @SuppressWarnings("unchecked")
    public void setConfig(final IConfig<?> aconfig) {
        this.dimension = null;
        this.dbConfig = null;
        this.dbDimension = null;
        if (this.config instanceof IEventSource) {
            ((IEventSource) this.config).removeListener(configListener);
        }

        this.config = aconfig;
        if (config != null) {
            this.config = aconfig;
            dimension = initDimension(this.config);
            try {
                dbConfig = SalsaAPI.getConfigById(config.getId(), true);
                dbDimension = initDimension(dbConfig);
            } catch (ScanNotFoundException e) {
            }
        }

        if (!SwingUtilities.isEventDispatchThread()) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    buildTrajectoryView();
                }
            };
            SwingUtilities.invokeLater(runnable);
        } else {
            buildTrajectoryView();
        }

        if (this.config instanceof IEventSource) {
            ((IEventSource) this.config).addListener(configListener);
        }

    }

    private void buildTrajectoryView() {
        if (trajectoryview != null) {
            trajectoryview.clearTrajectoriesView();
            if (dimension != null) {
                // Create Range
                List<? extends IRange> rangeList = dimension.getRangeList();
                if (rangeList != null) {
                    trajectoryview.addRange(rangeList.size());
                }

                // Create Trajectory
                if (dimension.getActuatorsList() != null) {
                    List<IActuator> actuatorList = dimension.getActuatorsList();
                    trajectoryview.addDevice(actuatorList);
                }
                refresh();
            }
        }
    }

    protected abstract IDimension initDimension(IConfig<?> aconfig);

    protected abstract ITrajectory createTrajectory();

    protected abstract IRange createRange();

    public void notifyNewRangeAction() {
        if (this.dimension != null) {
            List<? extends IRange> rangeList = dimension.getRangeList();
            IRange newRange = createRange();
            if ((newRange != null) && (rangeList != null)) {
                newRange.setDimension(dimension);
                newRange.setStepsNumber(1);
                if (newRange instanceof IRangeIntegrated) {
                    ((IRangeIntegrated) newRange).setIntegrationTime(new double[] { 1.0, 1.0 });
                }
                List<IActuator> actuatorsList = dimension.getActuatorsList();
                if (actuatorsList != null) {
                    ITrajectory trajectory = null;
                    List<ITrajectory> trajectoriesList = null;
                    for (int i = 0; i < actuatorsList.size(); i++) {
                        trajectory = createTrajectory();
                        if (trajectory != null) {
                            trajectoriesList = newRange.getTrajectoriesList();
                            trajectory.setIRange(newRange);
                            if (trajectoriesList == null) {
                                trajectoriesList = new ArrayList<>();
                            }
                            trajectoriesList.add(trajectory);
                        }
                    }
                    dimension.addRange(newRange);
                }
                config.setModified(true);
                refresh();
            }
        }
    }

    /**
     * Writes the trajectory data from the config into the view.
     */
    public void refresh() {
        // set the current configuration value
        refresh(false);
        // set the database configuration value
        refresh(true);
    }

    private void refresh(final boolean saved) {
        IDimension currentDimension = dimension;
        IConfig<?> currentConfig = config;
        if (saved) {
            currentConfig = dbConfig;
            currentDimension = dbDimension;
        }

        TrajectoryControllerUtil.refresh(trajectoryview, currentConfig, currentDimension, saved);
    }

    /**
     * Called when one or more properties of the bean have been chaged.
     *
     * @param event
     */
    public void notifyTrajectoryPropertyChanged(final EntityPropertyChangedEvent<ITrajectory2DX> event) {
        this.refresh();
    }

    /**
     * Called when one or more properties of the bean have been chaged.
     *
     * @param event
     */
    public void notifyRangePropertyChanged(final EntityPropertyChangedEvent<IRange2DX> event) {
        this.refresh();
    }

    public void notifyEnableActuatorSpeedControlAction(final Boolean value) {
        if (config != null) {
            if (config.isEnableScanSpeed() != value) {
                config.setEnableScanSpeed(value);
                config.setModified(true);
                this.refresh();
            }
        }
    }

    public void notifyOnTheFlyAction(final Boolean value) {
        if (config != null) {
            if (config.isOnTheFly() != value) {
                config.setOnTheFly(value);
                config.setModified(true);
                this.refresh();
            }
        }
    }

    public void notifyIntegrationTimeChange(final double[] value, final int jPosition) {
        TrajectoryControllerUtil.computeIntegrationTimeChange(config, value, jPosition, yController);
        refresh();
    }

    /**
     * Called when number of steps value changes.
     */
    public void notifyNumberOfStepsChange(final String value, final int jPosition) {
        TrajectoryControllerUtil.computeStepsNumberChange(config, Integer.valueOf(value), jPosition, yController);
        refresh();
    }

    /**
     * Called when delta value changes.
     */
    public void notifyDeltaValueChanged(final String value, final int rangePosition, final String actuatorName) {
        TrajectoryControllerUtil.computeDeltaChange(config, value, actuatorName, rangePosition, yController);
        refresh();
    }

    public void notifyRelativeChanged(final boolean relative, final int rangePosition, final String actuatorName) {
        TrajectoryControllerUtil.computeRelativeChange(config, relative, actuatorName, rangePosition, yController);
        refresh();
    }

    public void notifyDeltaConstantChanged(final boolean deltaConstant, final int rangePosition,
            final String actuatorName) {
        TrajectoryControllerUtil.computeDeltaConstantChange(config, deltaConstant, actuatorName, rangePosition,
                yController);
        refresh();
    }

    /**
     * Called when begin value changes.
     */
    public void notifyFromValueChanged(final String value, final int rangePosition, final String actuatorName,
            final boolean deltaLock) {
        TrajectoryControllerUtil.computeBeginPositionChange(config, value, actuatorName, rangePosition, deltaLock,
                yController);
        refresh();
    }

    /**
     * Called when end value changes.
     */
    public void notifyToValueChanged(final String value, final int rangePosition, final String actuatorName,
            final boolean deltaLock) {
        TrajectoryControllerUtil.computeEndPositionChange(config, value, actuatorName, rangePosition, deltaLock,
                yController);
        refresh();
    }

    public void notifyCustomTrajectoryChanged(final boolean value, final int rangePosition, final String actuatorName) {
        TrajectoryControllerUtil.customTrajectoryChange(config, value, actuatorName, rangePosition, yController);
        refresh();
    }

    public void notifyCustomTrajectoryValueChanged(final double[] value, final int rangePosition,
            final String actuatorName) {
        TrajectoryControllerUtil.customTrajectoryValueChange(config, value, actuatorName, rangePosition, yController);
        refresh();

    }

    /**
     * Enables Y trajectories view.
     *
     * @param value
     */
    public void setYTrajectory(final boolean yTrajectory) {
        yController = yTrajectory;
        if (trajectoryview != null) {
            trajectoryview.setYTrajectory(yTrajectory);
        }
    }

    /**
     * Enables HCS trajectories view.
     *
     * @param value
     */
    public void setHCSTrajectory(final boolean hcsTrajectory) {
        if (trajectoryview != null) {
            trajectoryview.setHCSTrajectory(hcsTrajectory);
        }
    }

    /**
     * Called when integration time changed in HCS scan.
     */
    public void notifyIntegrationTimeHCSChanged(final double[] value) {
        TrajectoryControllerUtil.computeIntegrationTimeChange(config, value, 0, yController);
        refresh();
    }

    public void notifyAddActuator(final IActuator actuator) {
        if (trajectoryview != null) {
            trajectoryview.addDevice(actuator);
        }
    }

    /**
     * Deletes a range and the list of associated trajectories from the model
     * and the view.
     */
    public void notifyDeleteRangeAction() {
        if ((this.config != null) && (dimension != null)) {
            TrajectoryControllerUtil.deleteRange(config, yController);
            refresh();
        }
    }

    public void setDeviceEnable(final String device, final boolean enable) {
        TrajectoryControllerUtil.setDeviceEnable(config, device, enable, yController);
    }

}

package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.ScanKTrajectoryView;

public interface IScanKTrajectoryController extends IController<ScanKTrajectoryView> {

    /**
     * Called when Emin Field changes.
     */
    public void notifyEminFieldChange(double value);

    /**
     * Called when Edelta Field changes.
     */
    public void notifyEDeltaPreEdgeFieldChange(double value);

    /**
     * Called when PreEdge Time Field changes.
     */
    public void notifyTimePreEdgeFieldChange(double value);

    /**
     * Called when E0 Field changes.
     */
    public void notifyE0FieldChange(double value);

    /**
     * Called when E1 Field changes.
     */
    public void notifyE1FieldChange(double value);

    /**
     * Called when E2 Field changes.
     */
    public void notifyE2FieldChange(double value);

    /**
     * Called when Edelta Field changes.
     */
    public void notifyEDeltaEdgeFieldChange(double value);

    /**
     * Called when Edge Time Field changes.
     */
    public void notifyTimeEdgeFieldChange(double value);

    /**
     * Called when Kmin Field changes.
     */
    public void notifyKMinFieldChange(double value);

    /**
     * Called when Kmax Field changes.
     */
    public void notifyKMaxFieldChange(double value);

    /**
     * Called when Kdelta Field changes.
     */
    public void notifyKDeltaFieldChange(double value);

    /**
     * Called when PostEdge Time Field changes.
     */
    public void notifyPostEdgeTimeFieldChange(double value);

    /**
     * Called when N Field changes.
     */
    public void notifyNFieldChange(double value);

    /**
     * Called when M Field changes.
     */
    public void notifyMFieldChange(double value);

    /**
     * Called when dead time Field changes.
     */
    public void notifyDeadTimeFieldChange(double value);

}

package fr.soleil.salsa.client.controller.impl;

import org.slf4j.Logger;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IScanKTrajectoryController;
import fr.soleil.salsa.client.view.ScanKTrajectoryView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.TrajectoryUtil;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.logging.LoggingUtil;

public class ScanKTrajectoryController extends AController<ScanKTrajectoryView> implements IScanKTrajectoryController {

    public static final Logger LOGGER = LoggingUtil.getLogger(ScanKTrajectoryController.class);

    /**
     * The TrajectoryModel
     */
    private ITrajectoryK trajectory = null;
    private ITrajectoryK dbTrajectory = null;
    private IConfigK config = null;
    private final ConfigListener configListener;

    public ScanKTrajectoryController() {
        this(null);
    }

    public ScanKTrajectoryController(ScanKTrajectoryView view) {
        super(view);
        configListener = new ConfigListener();
    }

    @Override
    protected ScanKTrajectoryView generateView() {
        return new ScanKTrajectoryView(this);
    }

    public void setConfig(IConfig<?> config) {
        if (this.config instanceof IEventSource<?>) {
            try {
                @SuppressWarnings("unchecked")
                IEventSource<EntityPropertyChangedEvent<IConfigK>> source = (IEventSource<EntityPropertyChangedEvent<IConfigK>>) this.config;
                source.removeListener(configListener);
            } catch (Exception e) {
                LOGGER.error("failed to remove config listener", e);
            }
        }
        this.config = null;
        this.trajectory = null;
        this.dbTrajectory = null;

        if (view != null) {
            view.setEnabled(false);
        }

        boolean isConfigK;
        if (config instanceof IConfigK) {
            this.config = (IConfigK) config;
            if (this.config instanceof IEventSource<?>) {
                try {
                    @SuppressWarnings("unchecked")
                    IEventSource<EntityPropertyChangedEvent<IConfigK>> source = (IEventSource<EntityPropertyChangedEvent<IConfigK>>) this.config;
                    source.addListener(configListener);
                } catch (Exception e) {
                    LOGGER.error("failed to add config listener", e);
                }
            }
            isConfigK = true;
            IDimensionK dimension = this.config.getDimensionX();
            if (dimension != null) {
                trajectory = dimension.getTrajectory();
            }
            // Get Database information
            try {
                IConfigK dbconfig = (IConfigK) SalsaAPI.getConfigById(this.config.getId(), true);
                if (dbconfig != null && dbconfig.getDimensionX() != null) {
                    dbTrajectory = dbconfig.getDimensionX().getTrajectory();
                }
            } catch (ScanNotFoundException e) {
                LOGGER.error("failed to get config by id", e);
            }
        } else {
            isConfigK = false;
        }
        refresh();

        setViewVisible(isConfigK);
        if (view != null) {
            view.setEnabled(isConfigK);
        }
    }

    @Override
    public void notifyEminFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getEMin(), value)) {
                trajectory.setEMin(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyEDeltaPreEdgeFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getEDeltaPreEdge(), value)) {
                trajectory.setEDeltaPreEdge(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyTimePreEdgeFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getTimePreEdge(), value)) {
                trajectory.setTimePreEdge(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyE0FieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getE0(), value)) {
                trajectory.setE0(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyE1FieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getE1(), value)) {
                trajectory.setE1(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyE2FieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getE2(), value)) {
                trajectory.setE2(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyEDeltaEdgeFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getEDeltaEdge(), value)) {
                trajectory.setEDeltaEdge(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyTimeEdgeFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getTimeEdge(), value)) {
                trajectory.setTimeEdge(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyKMinFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getKMin(), value)) {
                trajectory.setKMin(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyKMaxFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getKMax(), value)) {
                trajectory.setKMax(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyKDeltaFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getKDelta(), value)) {
                trajectory.setKDelta(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyPostEdgeTimeFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getTimePostEdge(), value)) {
                trajectory.setTimePostEdge(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyNFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getN(), value)) {
                trajectory.setN(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyMFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getM(), value)) {
                trajectory.setM(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyDeadTimeFieldChange(double value) {
        if (trajectory != null) {
            if (!ObjectUtils.sameDouble(trajectory.getDeadTime(), value)) {
                trajectory.setDeadTime(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    /**
     * Writes the trajectory data from the config into the view.
     */
    public void refresh() {
        // set the current configuration value
        refresh(false);
        // set the database configuration value
        refresh(true);
    }

    private void refresh(boolean saved) {
        if (view != null) {
            ITrajectoryK currentTrajectory = trajectory;
            if (saved) {
                currentTrajectory = dbTrajectory;
            }

            if (currentTrajectory != null) {
                // Pre Edge Parameters
                view.setEMinField(toString(currentTrajectory.getEMin()), saved);
                view.setEDeltaPreEdgeField(toString(currentTrajectory.getEDeltaPreEdge()), saved);
                view.setTimePreEdgeField(toString(currentTrajectory.getTimePreEdge()), saved);

                // Edge Parameters
                view.setE0Field(toString(currentTrajectory.getE0()), saved);
                view.setE1Field(toString(currentTrajectory.getE1()), saved);
                view.setE2Field(toString(currentTrajectory.getE2()), saved);
                view.setEDeltaEdgeField(toString(currentTrajectory.getEDeltaEdge()), saved);
                view.setTimeEdgeField(toString(currentTrajectory.getTimeEdge()), saved);

                // Connexion Parameter
                view.setMField(toString(currentTrajectory.getM()), saved);

                // Pre Edge Parameters
                view.setKMin(toString(currentTrajectory.getKMin()), saved);
                view.setKMaxField(toString(currentTrajectory.getKMax()), saved);
                view.setKDeltaField(toString(currentTrajectory.getKDelta()), saved);
                view.setTimePostEdgeField(toString(currentTrajectory.getTimePostEdge()), saved);
                view.setNField(toString(currentTrajectory.getN()), saved);

                // Estimates
                view.setDeadTimeField(toString(currentTrajectory.getDeadTime()), saved);
                if (!saved) {
                    double actuatorDelay = (config == null ? MathConst.NAN_FOR_NULL : config.getActuatorsDelay());
                    double eMax = TrajectoryUtil.convertKToEnergy(currentTrajectory.getE0(),
                            currentTrajectory.getKMax(), TrajectoryUtil.KEV);
                    view.setEMaxField(toString(eMax));
                    double tMax = TrajectoryUtil.calculateKTime(currentTrajectory.getKMax(),
                            currentTrajectory.getKMin(), currentTrajectory.getTimePostEdge(), currentTrajectory.getN());
                    view.setTMaxField(toString(tMax));
                    double preEdgeDuration = TrajectoryUtil.calculatePreEdgeDuration(currentTrajectory.getEMin(),
                            currentTrajectory.getE1(), currentTrajectory.getEDeltaPreEdge(),
                            currentTrajectory.getTimePreEdge(), actuatorDelay);
                    double edgeDuration = TrajectoryUtil.calculateEdgeDuration(currentTrajectory.getE1(),
                            currentTrajectory.getE2(), currentTrajectory.getEDeltaEdge(),
                            currentTrajectory.getTimeEdge(), actuatorDelay);
                    double postEdgeDuration = TrajectoryUtil.calculatePostEdgeDuration(
                            currentTrajectory.getTimePostEdge(), tMax, currentTrajectory.getKMin(),
                            currentTrajectory.getKMax(), currentTrajectory.getKDelta(), actuatorDelay);
                    view.setDurationField(toReadableDuration((preEdgeDuration + edgeDuration + postEdgeDuration)
                            / (1 - currentTrajectory.getDeadTime() / 100)));
                }
            }
        }
    }

    private String toString(double value) {
        return Double.isNaN(value) || Double.isInfinite(value) ? StringScalarBox.DEFAULT_ERROR_STRING
                : Double.toString(value);
    }

    private String toReadableDuration(double duration) {
        return Double.isNaN(duration) || Double.isInfinite(duration) ? StringScalarBox.DEFAULT_ERROR_STRING
                : DateUtil.elapsedTimeToStringBuilder(null, Math.round(duration * 1000), false).toString();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * 
     * A multi-events listener.
     * 
     * @param <T>
     */
    private class ConfigListener implements IListener<EntityPropertyChangedEvent<IConfigK>> {

        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IConfigK> event) {
            refresh();
        }
    }

}

package fr.soleil.salsa.client.controller.impl;

import fr.soleil.salsa.client.controller.ITrajectoryListController;
import fr.soleil.salsa.client.view.EnergyTrajectoryView;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.RangeEnergyModel;
import fr.soleil.salsa.entity.event.TrajectoryEnergyModel;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;

/**
 * Controller for the view showing the view for the scan energy.
 * 
 * @author ALIKE
 * 
 */
public class ScanEnergyController extends AbstractTrajectoryController<EnergyTrajectoryView>
        implements ITrajectoryListController<EnergyTrajectoryView> {

    public ScanEnergyController() {
        super(null);
    }

    /**
     * Create the default range.
     * 
     * @return
     */
    @Override
    public IRange createRange() {
        RangeEnergyModel rem = new RangeEnergyModel();
        rem.setId(new Integer(0));
        rem.setIntegrationTime(new double[] { 1.0 });
        rem.setStepsNumber(new Integer(1));
        return rem;
    }

    /**
     * Create the default trajectory.
     * 
     * @param range
     * @return
     */
    @Override
    public ITrajectory createTrajectory() {
        TrajectoryEnergyModel result = new TrajectoryEnergyModel();
        result.setBeginPosition(new Double(0));
        result.setEndPosition(new Double(0));
        result.setDelta(new Double(0));
        result.setId(new Integer(0));
        result.setRelative(new Boolean(false));
        result.setSpeed(new Double(0));
        return result;
    }

    @Override
    public void setConfig(IConfig<?> iconfig) {
        if ((trajectoryview == null) && (view != null)) {
            setTrajectoryView(view);
        }
        IConfig<?> newconfig = null;
        boolean energyDimension = iconfig instanceof IConfigEnergy;
        if (energyDimension) {
            newconfig = iconfig;
        }
        super.setConfig(newconfig);
        setViewVisible(energyDimension);
        if (view != null) {
            view.enableButtons(energyDimension);
        }
    }

    @Override
    protected IDimension initDimension(IConfig<?> aconfig) {
        IDimension newDimension = null;
        if (aconfig instanceof IConfigEnergy) {
            newDimension = aconfig.getDimensionX();
        }
        return newDimension;
    }

    @Override
    public void notifyDeleteActuator(IActuator actuator) {
        // Nothing todo
    }

    @Override
    protected EnergyTrajectoryView generateView() {
        return new EnergyTrajectoryView(this);
    }

}

package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.EnergyTrajectoryView;

public interface IScanEnergyTrajectoryController extends IController<EnergyTrajectoryView> {

    public void notifyBeginPositionAfterChanged(Double value, int row);

    public void notifyEndPositionAfterChanged(Double value, int row);

    public void notifyDeltaAfterChanged(Double value, int row);

    public void notifyStepNumberAfterChanged(Integer value, int row);

    public void notifyIntegrationTimeAfterChanged(Double value, int row);

    public void notifyDeltaConstant(Boolean value, int row);

    /**
     * The new range button has been clicked.
     */
    public void notifyNewRange();

    /**
     * The delete range button has been clicked.
     * 
     * @param row : the row selected when the delete range button has been clicked.
     */
    public void notifyDeleteRange(int row);

    /**
     * The select element button has been clicked.
     */
    public void notifySelectElement();
}

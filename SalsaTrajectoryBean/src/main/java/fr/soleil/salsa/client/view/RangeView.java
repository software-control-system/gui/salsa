package fr.soleil.salsa.client.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;
import fr.soleil.salsa.client.controller.ITrajectoryListController;
import fr.soleil.salsa.client.util.BackgroundRenderer;
import fr.soleil.salsa.client.view.component.DoubleField;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Contains all GUI elements of a range.
 * 
 * @author Alike
 * 
 */

public class RangeView extends JPanel {

    private static final long serialVersionUID = 3074025315965471683L;

    private boolean yRangeEnable = false;
    private String integrationTimeValue = null;
    private String dbIntegrationTimeValue = null;

    private String stepValue = null;
    private String dbStepValue = null;

    /**
     * Name of the range field.
     */
    private Label rangeNameLabel;

    /**
     * Time of integration label.
     */
    private Label integrationTimeLabel;

    /**
     * Number of steps label.
     */
    private Label numberOfStepsLabel;

    private final Label completeTimeLabel = new Label();
    private DoubleField completeTime;

    /**
     * Time of integration field.
     */
    private DoubleField integrationTimeField;

    /**
     * Number of steps field.
     */
    private TextField numberOfStepsField;

    /**
     * Map of trajectories of related to this range.
     * ActuatorName, View
     */
    private Map<String, TrajectoryView> trajectoryMap;

    /**
     * List of trajectories of related to this range.
     */
    private List<TrajectoryView> trajectories;

    /**
     * The controller.
     */
    private ITrajectoryListController<?> trajectoryController;

    public static final int HEIGHT = 110;
    public static final int HEIGHT_Y = 90;
    public static final int HEIGHT_HCS = 45;

    /**
     * j Position in the GUI.
     */
    private final int rangePosition;

    public RangeView() {
        this(null, 0);
    }

    public RangeView(ITrajectoryListController<?> trajectoryController) {
        this(trajectoryController, 0);
    }

    public RangeView(ITrajectoryListController<?> trajectoryController, int rangePosition) {
        this.trajectoryController = trajectoryController;
        this.rangePosition = rangePosition;
        this.initialize();
    }

    public ITrajectoryListController<?> getTrajectoryController() {
        return trajectoryController;
    }

    public void setTrajectoryController(ITrajectoryListController<?> trajectoryController) {
        this.trajectoryController = trajectoryController;
        for (TrajectoryView view : trajectories) {
            view.setTrajectoryController(trajectoryController);
        }
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        for (TrajectoryView trajectory : trajectories) {
            trajectory.setVisible(aFlag);
        }
    }

    public void setEnableSpeedControl(boolean value) {
        for (TrajectoryView trajectory : trajectories) {
            trajectory.setEnableSpeed(value);
        }
    }

    /**
     * Initialization of a range view.
     */
    public void initialize() {
        this.rangeNameLabel = new Label();
        String rangeNumber = String.valueOf(rangePosition + 1);
        this.rangeNameLabel.setText("Range " + rangeNumber);
        this.rangeNameLabel.setBackground(new Color(204, 255, 204));
        this.rangeNameLabel.setOpaque(true);
        this.rangeNameLabel.setHorizontalAlignment(JLabel.CENTER);
        this.rangeNameLabel.setVerticalAlignment(JLabel.CENTER);
        this.rangeNameLabel.setPreferredSize(new Dimension(200, 30));
        this.rangeNameLabel.setBorder(BorderFactory.createEtchedBorder());

        this.integrationTimeLabel = new Label();
        this.integrationTimeLabel.setText("Integration Time ");
        this.integrationTimeLabel.setFont(new Font(null, 0, 11));
        this.integrationTimeLabel.setPreferredSize(new Dimension(100, 20));

        this.numberOfStepsLabel = new Label();
        this.numberOfStepsLabel.setText("Number of Steps ");
        this.numberOfStepsLabel.setFont(new Font(null, 0, 11));
        this.numberOfStepsLabel.setPreferredSize(new Dimension(100, 20));

        this.completeTimeLabel.setText("Acquisition time ");
        this.completeTimeLabel.setFont(new Font(null, 0, 11));
        this.completeTimeLabel.setPreferredSize(new Dimension(100, 20));

        this.completeTime = new DoubleField(1d, "%6d");
        this.completeTime.setPreferredSize(new Dimension(100, 20));
        this.completeTime.setEditable(false);
        this.completeTime.setBackground(Color.LIGHT_GRAY);

        this.setLayout(new GridBagLayout());

        GridBagConstraints rangeNameLabelConstraint = new GridBagConstraints();
        GridBagConstraints integrationTimeLabelConstraint = new GridBagConstraints();
        GridBagConstraints numberOfStepsLabelConstraint = new GridBagConstraints();
        GridBagConstraints integrationTimeFieldConstraint = new GridBagConstraints();
        GridBagConstraints numberOfStepsFieldConstraint = new GridBagConstraints();
        GridBagConstraints completeTimeLabelConstraint = new GridBagConstraints();
        GridBagConstraints completeTimeFieldConstraint = new GridBagConstraints();

        rangeNameLabelConstraint.gridwidth = 2;

        integrationTimeLabelConstraint.gridx = 0;
        integrationTimeLabelConstraint.gridy = 1;

        integrationTimeFieldConstraint.gridx = 1;
        integrationTimeFieldConstraint.gridy = 1;

        numberOfStepsLabelConstraint.gridx = 0;
        numberOfStepsLabelConstraint.gridy = 2;

        numberOfStepsFieldConstraint.gridx = 1;
        numberOfStepsFieldConstraint.gridy = 2;

        completeTimeLabelConstraint.gridx = 0;
        completeTimeLabelConstraint.gridy = 3;

        completeTimeFieldConstraint.gridx = 1;
        completeTimeFieldConstraint.gridy = 3;

        this.add(rangeNameLabel, rangeNameLabelConstraint);
        this.add(integrationTimeLabel, integrationTimeLabelConstraint);
        this.add(numberOfStepsLabel, numberOfStepsLabelConstraint);
        this.add(this.getIntegrationTimeField(), integrationTimeFieldConstraint);
        this.add(this.getNumberOfStepsField(), numberOfStepsFieldConstraint);
        this.add(completeTimeLabel, completeTimeLabelConstraint);
        this.add(completeTime, completeTimeFieldConstraint);
        this.setBorder(BorderFactory.createRaisedBevelBorder());
        this.setBounds(5, 5, TrajectoryView.WIDTH, HEIGHT);
        this.setPreferredSize(new Dimension(TrajectoryView.WIDTH, HEIGHT));
        this.trajectories = new ArrayList<>();
        this.trajectoryMap = new HashMap<>();
        setYRangeEnabled(false);
    }

    /**
     * Gets Integration Time JTextField.
     * 
     * @return
     */
    public TextField getIntegrationTimeField() {
        if (integrationTimeField == null) {
            integrationTimeField = new DoubleField(1d, "%8.2f");
            integrationTimeField.setPreferredSize(new Dimension(100, 20));
            setBackgroundIntegrationTime();
            integrationTimeField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                        notifyIntegrationTimeValueChanged();
                    }
                    setBackgroundIntegrationTime();
                }
            });

            integrationTimeField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    notifyIntegrationTimeValueChanged();
                    setBackgroundIntegrationTime();
                }
            });
        }
        return integrationTimeField;
    }

    private void notifyIntegrationTimeValueChanged() {
        if (trajectoryController != null) {
            integrationTimeValue = integrationTimeField.getText();
            double integrationTime = Double.valueOf(integrationTimeValue);
            double[] integrationTimeArray = new double[Integer.valueOf(numberOfStepsField.getText()) + 1];
            Arrays.fill(integrationTimeArray, integrationTime);
            trajectoryController.notifyIntegrationTimeChange(integrationTimeArray, rangePosition);
        }
    }

    private void notifyStepValueChanged() {
        if (!TrajectoryListView.validateField(numberOfStepsField.getText())
                || numberOfStepsField.getText().contains(".")) {
            return;
        }
        if (trajectoryController != null) {
            stepValue = numberOfStepsField.getText();
            trajectoryController.notifyNumberOfStepsChange(numberOfStepsField.getText(), rangePosition);
            notifyIntegrationTimeValueChanged();
        }
    }

    /**
     * Gets Number of steps JTextField.
     * 
     * @return
     */
    public TextField getNumberOfStepsField() {
        if (numberOfStepsField == null) {
            numberOfStepsField = new TextField();
            numberOfStepsField.setText("1");
            numberOfStepsField.setPreferredSize(new Dimension(100, 20));
            setBackgroundStep();
            numberOfStepsField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                        notifyStepValueChanged();
                    }
                    setBackgroundStep();
                }
            });

            numberOfStepsField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    notifyStepValueChanged();
                    setBackgroundStep();
                }
            });
        }
        return numberOfStepsField;
    }

    /**
     * Adds a dimension to the range.
     * 
     * @param dimension
     */
    public void addTrajectory(TrajectoryView trajectoryView) {
        if (trajectoryView != null) {
            this.trajectories.add(trajectoryView);
            if (trajectoryView.getActuatorName() != null) {
                trajectoryMap.put(trajectoryView.getActuatorName(), trajectoryView);
            }
        }
    }

    /**
     * Removes last added dimension from dimension's list of the range.
     */
    public void removeLastTrajectory() {
        if (this.trajectories != null) {
            trajectories.remove(trajectories.get(trajectories.size() - 1));
        }
    }

    public int getNbTrajectory() {
        return trajectories.size();
    }

    public TrajectoryView getTrajectoryForActuator(String actuatorName) {
        return trajectoryMap.get(actuatorName);
    }

    public TrajectoryView getTrajectoryForIndex(int index) {
        TrajectoryView view = null;
        if ((index > -1) && (index < trajectories.size())) {
            view = trajectories.get(index);
        }
        return view;
    }

    public void removeTrajectoryForIndex(int index) {
        TrajectoryView view = getTrajectoryForIndex(index);
        if (view != null) {
            trajectoryMap.remove(view.getActuatorName());
            trajectories.remove(index);
        }
    }

    public void clearValues() {
        trajectories.clear();
        trajectoryMap.clear();
    }

    /**
     * Gets steps number field.
     * 
     * @return
     */
    public String getStepNumber() {
        return this.numberOfStepsField.getText();
    }

    /**
     * Gets integration time.
     * 
     * @return
     */
    public String getIntegrationTime() {
        return this.integrationTimeField.getText();
    }

    /**
     * Gets the position of the range.
     * 
     * @return
     */
    public int getRangePosition() {
        return rangePosition;
    }

    /**
     * Sets steps number field.
     * 
     * @return
     */
    public void setStepNumber(String stepNumber) {
        setStepNumber(stepNumber, false);
    }

    public void setStepNumber(String stepNumber, boolean savedValue) {

        if (savedValue) {
            this.dbStepValue = stepNumber;
        } else {
            this.stepValue = stepNumber;
            this.numberOfStepsField.setText(stepNumber);
            computeAcquisitionTime();
        }
        setBackgroundStep();
    }

    private void computeAcquisitionTime() {
        String nbStepText = numberOfStepsField.getText();
        String integrationTimeText = integrationTimeField.getText();

        if (TrajectoryListView.validateField(nbStepText) && TrajectoryListView.validateField(integrationTimeText)
                && SalsaUtils.isDefined(nbStepText)) {
            double nbStepVal = Double.valueOf(nbStepText);
            try {
                double integrationTimeVal = Double.valueOf(integrationTimeText);
                completeTime.setText(String.valueOf(nbStepVal * integrationTimeVal));
            } catch (NumberFormatException e) {
                // TODO: handle exception
            }
        }
    }

    private void setBackgroundStep() {
        BackgroundRenderer.setBackgroundField(this.numberOfStepsField, stepValue, dbStepValue);
    }

    /**
     * Sets integration time value.
     * 
     * @return
     */
    public void setIntegrationTime(String integrationTime) {
        setIntegrationTime(integrationTime, false);
    }

    public void setIntegrationTime(String integrationTime, boolean savedValue) {
        if (savedValue) {
            this.dbIntegrationTimeValue = integrationTime;
        } else {
            this.integrationTimeValue = integrationTime;
            this.integrationTimeField.setText(integrationTime);
            computeAcquisitionTime();
        }
        setBackgroundIntegrationTime();
    }

    private void setBackgroundIntegrationTime() {
        BackgroundRenderer.setBackgroundField(this.integrationTimeField, integrationTimeValue, dbIntegrationTimeValue);
    }

    /**
     * Used to choose if this is a X range view or Y one. yRangeValue = true (Y
     * Range choosed) yRangeValue = false (X Range choosed)
     * 
     * @param yRangeValue
     */
    public void setYRangeEnabled(boolean yRangeValue) {
        yRangeEnable = yRangeValue;
        getIntegrationTimeField().setVisible(!yRangeEnable);
        this.integrationTimeLabel.setVisible(!yRangeEnable);
        this.completeTimeLabel.setVisible(!yRangeEnable);
        this.completeTime.setVisible(!yRangeEnable);
        if (yRangeValue) {
            this.setBounds(5, 5, TrajectoryView.WIDTH, HEIGHT_Y);
            this.setPreferredSize(new Dimension(TrajectoryView.WIDTH, HEIGHT_Y));
        } else {
            this.setBounds(5, 5, TrajectoryView.WIDTH, HEIGHT);
            this.setPreferredSize(new Dimension(TrajectoryView.WIDTH, HEIGHT));
        }
    }

    public void setHCSEnabled(boolean HCSValue) {
        if (!yRangeEnable && HCSValue) {
            String rangeNumber = "";
            if (!HCSValue) {
                rangeNumber = String.valueOf(rangePosition + 1);
            }
            this.rangeNameLabel.setText("Range " + rangeNumber);

            for (TrajectoryView view : trajectoryMap.values()) {
                view.setHCSTrajectoryEnabled(HCSValue);
            }

            if (HCSValue) {
                this.setBounds(5, 5, TrajectoryView.WIDTH, HEIGHT_HCS);
                this.setPreferredSize(new Dimension(TrajectoryView.WIDTH, HEIGHT_HCS));
            } else {
                this.setBounds(5, 5, TrajectoryView.WIDTH, HEIGHT);
                this.setPreferredSize(new Dimension(TrajectoryView.WIDTH, HEIGHT));
            }
        }
    }

    public static void main(String[] args) throws Exception {
        JFrame frame = new JFrame(RangeView.class.getName() + " test");
        RangeView rangeView = new RangeView();
        rangeView.setHCSEnabled(true);
        frame.setContentPane(rangeView);
        frame.setVisible(true);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}

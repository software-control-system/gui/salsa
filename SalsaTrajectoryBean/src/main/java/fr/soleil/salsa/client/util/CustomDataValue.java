package fr.soleil.salsa.client.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class CustomDataValue {

    private double[] customData;
    private final Collection<ICustomDataListener> listeners = new ArrayList<>();

    public void addCustomTrajectoryListener(ICustomDataListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeCustomTrajectoryListener(ICustomDataListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    private void fireCustomDataChanged() {
        for (ICustomDataListener listener : listeners) {
            listener.customDataChange(customData);
        }
    }

    public double[] getCustomData() {
        return customData;
    }

    public void setCustomData(double[] newCustomData) {
        String currentData = Arrays.toString(customData);
        String newData = Arrays.toString(newCustomData);
        if (!currentData.equals(newData)) {
            this.customData = newCustomData;
            fireCustomDataChanged();
        }
    }

}

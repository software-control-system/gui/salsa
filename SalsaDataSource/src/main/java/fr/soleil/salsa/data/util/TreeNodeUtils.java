package fr.soleil.salsa.data.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import javax.swing.ImageIcon;

import org.slf4j.Logger;

import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.event.TreeNodeEvent.NodeEventReason;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.config.manager.ConfigManager;
import fr.soleil.salsa.data.service.EntityPositionComparator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IEntity;
import fr.soleil.salsa.entity.event.DirectoryModel;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * A utility class used to interact with {@link ITreeNode}s
 * 
 * @author girardot
 */
public class TreeNodeUtils {

    public static final Logger LOGGER = LoggingUtil.getLogger(TreeNodeUtils.class);

    /**
     * A {@link BasicTreeNode} that check for data strict equality (
     * <code>=</code>, instead of <code>equals</code>) to set data. This is used
     * to have a distinction between loaded and not loaded configs.
     * 
     * @author girardot
     */
    protected static class EqualityBasicTreeNode extends BasicTreeNode {
        @Override
        public void setData(Object data) {
            if (data != this.data) {
                this.data = data;
                warnListeners(new TreeNodeEvent(this, NodeEventReason.DATA));
            }
        }
    }

    /**
     * Filters a multiple selection by removing elements that are children of
     * other elements in the selection.
     * 
     * @param multipleSelection
     */
    public static void filterMultipleSelection(List<ITreeNode> multipleSelection) {
        if (multipleSelection != null) {
            int size = multipleSelection.size();
            int cursorSearch = 0;
            int cursorCompare;
            ITreeNode searchNode;
            ITreeNode compareNode;
            while (cursorSearch < size - 1) {
                searchNode = multipleSelection.get(cursorSearch);
                for (cursorCompare = cursorSearch + 1; cursorCompare < size; ++cursorCompare) {
                    compareNode = multipleSelection.get(cursorCompare);
                    if (isChild(compareNode, searchNode)) {
                        multipleSelection.remove(cursorCompare);
                        --size;
                        --cursorCompare;
                    } else if (isChild(searchNode, compareNode)) {
                        multipleSelection.remove(cursorSearch);
                        --size;
                        --cursorSearch;
                        break;
                    }
                }
                ++cursorSearch;
            }
        }
    }

    /**
     * Returns <code>true</code> if the child parameter is indeed a child of
     * parent. This is recursive : <code>true</code> will be returned for grand
     * children etc...
     * 
     * @param childNode
     *            The child node
     * @param parentNode
     *            The parent node
     * @return A <code>boolean</code> value
     */
    public static boolean isChild(ITreeNode childNode, ITreeNode parentNode) {
        boolean result = false;
        // if (parentNode.getData() instanceof IDirectory) {
        // IDirectory parent = ((IDirectory) parentNode).getDirectory();
        // IEntity entity = null;
        // if (childNode.getData() instanceof IEntity) {
        // entity = (IEntity) childNode.getData();
        // }
        // if (entity != null) {
        // for (IDirectory search = entity.getDirectory(); (search != null) &&
        // (!result); search =
        // search
        // .getDirectory()) {
        // if (search.equals(parent)) {
        // result = true;
        // }
        // }
        // }
        // }
        if ((parentNode != null) && (childNode != null)) {
            result = parentNode.isAncestorOf(childNode);
        }
        return result;
    }

    /**
     * Finds the first {@link ITreeNode} that contains an {@link IDirectory} in
     * its ancestor hierarchy (including the node itself)
     * 
     * @param node
     *            The {@link ITreeNode} from which to start the search
     * @return An {@link ITreeNode}
     */
    public static ITreeNode findDirectoryNode(ITreeNode node) {
        ITreeNode directoryNode = node;
        while ((directoryNode != null) && (!(directoryNode.getData() instanceof IDirectory))) {
            directoryNode = directoryNode.getParent();
        }
        return directoryNode;
    }

    /**
     * Builds an {@link ITreeNode}, based on a given {@link IEntity}
     * 
     * @param entity
     *            The {@link IEntity}
     * @param recursive
     *            Whether to build children {@link ITreeNode}s, when concerned
     * @return An {@link ITreeNode}. <code>null</code> if the entity is
     *         <code>null</code>
     */
    public static ITreeNode buildNode(IEntity entity, boolean recursive) {
        ITreeNode node = null;
        if (entity != null) {
            node = new EqualityBasicTreeNode();
            node.setData(entity);
            node.setName(entity.getName());
            if (entity instanceof IDirectory) {
                // node.setImage(ImageTool.getCometeImage((ImageIcon) Icons
                // .getIcon("salsa.scanconfig.folder")));
                if (recursive) {
                    IDirectory directory = (IDirectory) entity;
                    List<IEntity> entityList = new ArrayList<IEntity>();
                    entityList.addAll(directory.getSubDirectoriesList());
                    entityList.addAll(directory.getConfigList());
                    Collections.sort(entityList, new EntityPositionComparator());
                    ITreeNode[] nodes = new ITreeNode[entityList.size()];
                    int index = 0;
                    for (IEntity child : entityList) {
                        nodes[index++] = buildNode(child, recursive);
                    }
                    node.addNodes(nodes);
                }
            } else if (entity instanceof IConfig1D) {
                node.setImage(ImageTool.getCometeImage((ImageIcon) Icons.getIcon("salsa.scanconfig.1d.big")));
            } else if (entity instanceof IConfig2D) {
                node.setImage(ImageTool.getCometeImage((ImageIcon) Icons.getIcon("salsa.scanconfig.2d.big")));
            } else if (entity instanceof IConfigK) {
                node.setImage(ImageTool.getCometeImage((ImageIcon) Icons.getIcon("salsa.scanconfig.k.big")));
            } else if (entity instanceof IConfigHCS) {
                node.setImage(ImageTool.getCometeImage((ImageIcon) Icons.getIcon("salsa.scanconfig.hcs.big")));
            } else if (entity instanceof IConfigEnergy) {
                node.setImage(ImageTool.getCometeImage((ImageIcon) Icons.getIcon("salsa.scanconfig.energy.big")));
            }
        }
        return node;
    }

    /**
     * The configuration are not fully loaded when the configuration is first
     * created to save time. This loads the full configuration.
     * 
     * @param node
     *            The {@link ITreeNode} that contains the {@link IConfig} to
     *            load. If the node is not linked with an {@link IConfig}, this
     *            may mean you want to load children configs. In which case, you
     *            have to put <code>recursive</code> parameter to
     *            <code>true</code>
     * @param configManager
     *            The {@link ConfigManager} that is able to load {@link IConfig}
     * @param recursive
     *            Whether to recursive load configs for children nodes, if the
     *            given node is in fact a directory node. If <code>false</code>
     *            and the given node is a directory node, nothing is done.
     * @return
     */
    public static void loadConfig(ITreeNode node, ConfigManager configManager, boolean recursive) {
        if ((node != null) && (node.getData() instanceof IEntity)) {
            if (node.getData() instanceof IConfig<?>) {
                IConfig<?> config = (IConfig<?>) node.getData();
                IConfig<?> newConfig;
                try {
                    if ((config.getId() != null) && (!config.isLoaded())) {
                        newConfig = configManager.loadConfig(config.getId());
                        // Replaces the old, not loaded config with the new,
                        // fully loaded config in
                        // the directory.
                        updateConfigDirectory(config, newConfig);
                        node.setData(newConfig);
                    }
                } catch (ScanNotFoundException snfe) {
                    LOGGER.error("Cannot load configuration {} {}", config, snfe.getMessage());
                    LOGGER.trace("Stack trace", snfe);
                }
            } else if (recursive) {
                for (ITreeNode child : node.getChildren()) {
                    loadConfig(child, configManager, recursive);
                }
            }
        }
    }

    /**
     * Replaces the old config by the new config in the tree. It is necessary
     * because the Salsa API might return a different instance of IConfig<?>
     * when saving etc...
     * 
     * @param oldConfig
     *            The old config
     * @param newConfig
     *            The new config
     */
    private static void updateConfigDirectory(IConfig<?> oldConfig, IConfig<?> newConfig) {
        boolean wasModified = newConfig.isModified();
        IDirectory directory = oldConfig.getDirectory();
        newConfig.setDirectory(directory);
        newConfig.setModified(wasModified);
        ListIterator<IConfig<?>> oldConfigIterator = directory.getConfigList().listIterator();
        IConfig<?> searchConfig;
        while (oldConfigIterator.hasNext()) {
            searchConfig = oldConfigIterator.next();
            if (searchConfig.equals(oldConfig)) {
                oldConfigIterator.set(newConfig);
                break;
            }
        }
    }

    /**
     * Creates a copy of a given {@link ITreeNode}. The result will contain a
     * duplicata of the given {@link ITreeNode}'s {@link IEntity}, if concerned.
     * <p>
     * The copy and its associated {@link IEntity} will have <code>null</code>
     * parents.
     * </p>
     * 
     * @param node
     *            The {@link ITreeNode} to duplicate
     * @param recursive
     *            Whether to recursively duplicate children nodes/entities (i.e.
     *            duplicate children, grand children, etc...)
     * @return An {@link ITreeNode}
     */
    public static ITreeNode getDuplicata(ITreeNode node, boolean recursive) {
        ITreeNode duplicata = null;
        if ((node != null) && (node.getData() instanceof IEntity)) {
            IEntity copyEntity = getDuplicata((IEntity) node.getData(), null, recursive);
            duplicata = buildNode(copyEntity, recursive);
            if ((duplicata != null) && (copyEntity != null)) {
                duplicata.setName(copyEntity.getName());
                duplicata.setImage(node.getImage());
            }
        }
        return duplicata;
    }

    /**
     * Creates a copy of a given {@link IEntity}.
     * 
     * @param entity
     *            The {@link IEntity} to duplicate
     * @param parent
     *            The parent directory to set to the copy
     * @param recursive
     *            Whether to recursively duplicate copy (i.e.: duplicate its
     *            children, grand children, etc...)
     * @return An {@link IEntity}
     */
    private static IEntity getDuplicata(IEntity entity, IDirectory parent, boolean recursive) {
        IEntity duplicata = null;
        if (entity instanceof IConfig<?>) {
            ConfigSerializer configSerializer = new ConfigSerializer();
            IConfig<?> copyConfig = configSerializer.cloneConfig((IConfig<?>) entity);
            copyConfig.setModified(true);
            duplicata = copyConfig;
        } else if (entity instanceof IDirectory) {
            IDirectory directory = (IDirectory) entity;
            IDirectory copyDirectory = new DirectoryModel();
            if (recursive) {
                List<IConfig<?>> configList = new ArrayList<IConfig<?>>();
                for (IConfig<?> config : directory.getConfigList()) {
                    configList.add((IConfig<?>) getDuplicata(config, copyDirectory, recursive));
                }
                copyDirectory.setConfigList(configList);
                List<IDirectory> subDirectoriesList = new ArrayList<IDirectory>();
                for (IDirectory dir : directory.getSubDirectoriesList()) {
                    subDirectoriesList.add((IDirectory) getDuplicata(dir, copyDirectory, recursive));
                }
                copyDirectory.setSubDirectoriesList(subDirectoriesList);
            }
            duplicata = copyDirectory;
        }
        if ((duplicata != null) && (entity != null)) {
            duplicata.setName(entity.getName() + "_copy");
            duplicata.setPositionInDirectory(entity.getPositionInDirectory());
            duplicata.setId(null);
            duplicata.setDirectory(parent);
        }
        return duplicata;
    }

}

package fr.soleil.salsa.data.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;

import fr.soleil.salsa.api.ConfigApi;
import fr.soleil.salsa.copier.AutoCopier;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.event.ConfigModel;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.logging.LoggingUtil;

public class ConfigSerializer {

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigSerializer.class);

    public IConfig<?> cloneConfig(IConfig<?> copiedConfigModel) {

        IConfig<?> copiedConfig = unwrap(copiedConfigModel);
        IConfig<?> copyConfig;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(copiedConfig);
            byte[] buffer = byteArrayOutputStream.toByteArray();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            copyConfig = (IConfig<?>) objectInputStream.readObject();
        }
        catch (IOException e) {
            // The streams work on the RAM, so an IOException should never happen.
            LOGGER.error("Cannot clone config {} {}", copiedConfigModel, e.getMessage());
            LOGGER.trace("Stack trace", e);
            copyConfig = null;
        }
        catch (ClassNotFoundException e) {
            // This should never happen as well.
            LOGGER.error("Cannot clone config {} {}", copiedConfigModel, e.getMessage());
            LOGGER.trace("Stack trace", e);
            copyConfig = null;
        }

        IConfig<?> resultModel = wrap(copyConfig);
        return resultModel;

    }

    /**
     * Wraps a config, choosing the right type automatically.
     * 
     * @param config
     * @return
     */
    private static IConfig<?> wrap(IConfig<?> configImpl) {
        IConfig<?> cm = AutoCopier.copy(configImpl);
        // IConfig ci = AutoCopier.copyToImpl(cm);
        return cm;
    }

    /**
     * Wraps a config, choosing the right type automatically.
     * 
     * @param config
     * @return
     */
    private static IConfig<?> unwrap(IConfig<?> configModel) {
        if (configModel instanceof ConfigModel<?>) {
            configModel = loadConfig(configModel);
        }

        IConfig<?> configImpl = AutoCopier.copyToImpl(configModel);

        return configImpl;

    }

    /**
     * The configuration are not fully loaded when the configuration is first created to save time.
     * This loads the full configuration.
     * 
     * @param config
     * @return
     */
    private static IConfig<?> loadConfig(IConfig<?> config) {
        IConfig<?> newConfig;
        if ((config.getId() != null) && !config.isLoaded()) {
            IConfig<?> loadedConfig;
            try {
                loadedConfig = ConfigApi.getConfigById(config.getId());
                newConfig = wrap(loadedConfig);
            }
            catch (ScanNotFoundException e) {
                LOGGER.error("Cannot load configuration {} {}", config, e.getMessage());
                LOGGER.trace("Stack trace", e);
                newConfig = config;
            }
        }
        else {
            newConfig = config;
        }
        return newConfig;
    }
}

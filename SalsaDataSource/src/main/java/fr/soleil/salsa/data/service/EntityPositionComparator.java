package fr.soleil.salsa.data.service;

import java.util.Comparator;

import fr.soleil.salsa.entity.IEntity;

/**
 * A {@link Comparator} that compares {@link IEntity}'s positions in directory
 * 
 * @author girardot
 */
public class EntityPositionComparator implements Comparator<IEntity> {

    @Override
    public int compare(IEntity o1, IEntity o2) {
        return getPosition(o1).compareTo(getPosition(o2));
    }

    /**
     * Calculates the not <code>null</code> position of an {@link IEntity} in its parent directory
     * 
     * @param entity The {@link IEntity}
     * @return An {@link Integer}
     */
    protected Integer getPosition(IEntity entity) {
        return ((entity == null) || (entity.getPositionInDirectory() == null) ? Integer.valueOf(-1)
                : entity.getPositionInDirectory());
    }

}

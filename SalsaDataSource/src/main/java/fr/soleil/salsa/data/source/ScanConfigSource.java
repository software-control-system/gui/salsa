package fr.soleil.salsa.data.source;

import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;

import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.api.ConfigApi;
import fr.soleil.salsa.data.util.TreeNodeUtils;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IEntity;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.ConfigAPIService;

/**
 * An {@link AbstractDataSource} that knows a {@link ITreeNode} structure, based on {@link IEntity}s
 * (i.e. database scan configuration structure)
 *
 * @author girardot
 */
public class ScanConfigSource extends AbstractDataSource<ITreeNode> implements ITreeNodeListener {

    public static final Logger LOGGER = LoggingUtil.getLogger(ScanConfigSource.class);

    private static final GenericDescriptor DATA_TYPE = new GenericDescriptor(ITreeNode.class);

    private ITreeNode rootNode;

    /**
     * Constructor
     *
     * @param key The {@link IKey} that identifies this source
     */
    public ScanConfigSource(final IKey key) {
        super(key);
    }

    @Override
    public GenericDescriptor getDataType() {
        return DATA_TYPE;
    }

    @Override
    public ITreeNode getData() {
        return rootNode;
    }

    @Override
    public void setData(final ITreeNode data) {
        if (data == null) {
            cleanListeners(rootNode);
            rootNode = buildNode(ConfigAPIService.getRootDirectory(), true);
            notifyMediators();
        } else if (data.getData() instanceof IEntity) {
            saveEntityNode(data, true);
        }
    }

    @Override
    public void nodeChanged(final TreeNodeEvent event) {
        if (event != null) {
            if (event.getReason() != null) {
                ITreeNode sourceNode = event.getSource();
                ITreeNode[] children = event.getChildren();
                switch (event.getReason()) {
                    case NAME:
                        saveEntityNode(sourceNode, false);
                        break;
                    case INSERTED:
                        if (children != null) {
                            for (ITreeNode node : children) {
                                saveEntityNode(node, true);
                            }
                        }
                        break;
                    case REMOVED:
                        if (children != null) {
                            for (ITreeNode node : children) {
                                deleteEntityNode(node);
                            }
                        }
                        break;
                    default:
                        // nothing to do for other cases
                        break;
                }
            }
        }
    }

    /**
     * Builds an {@link ITreeNode}, based on a given {@link IEntity}
     *
     * @param entity The {@link IEntity}
     * @param recursive Whether to build children {@link ITreeNode}s, when concerned
     * @return An {@link ITreeNode}. <code>null</code> if the entity is <code>null</code>
     */
    protected ITreeNode buildNode(final IEntity entity, final boolean recursive) {
        ITreeNode node = TreeNodeUtils.buildNode(entity, recursive);
        listenToNode(node, recursive);
        return node;
    }

    /**
     * Listens to an {@link ITreeNode}, and its descendants if asked
     *
     * @param node The {@link ITreeNode} to which to listen to
     * @param recursive Whether to listen to descendants
     */
    protected void listenToNode(final ITreeNode node, final boolean recursive) {
        if (node != null) {
            if (!node.isTreeNodeListenerRegistered(this)) {
                node.addTreeNodeListener(this);
            }
            if (recursive) {
                List<ITreeNode> children = node.getChildren();
                if (children != null) {
                    for (ITreeNode child : children) {
                        listenToNode(child, recursive);
                    }
                }
            }
        }
    }

    /**
     * Saves an {@link IEntity} linked with a given {@link ITreeNode}
     *
     * @param node The {@link ITreeNode} that contains the {@link IEntity} to save
     * @param recursive Whether to save children entity nodes
     * @see #saveConfigNode(ITreeNode)
     * @see #saveDirectoryNode(ITreeNode, boolean)
     */
    protected void saveEntityNode(final ITreeNode node, final boolean recursive) {
        if (node != null) {
            IEntity entity = (IEntity) node.getData();
            // check name
            if (!ObjectUtils.sameObject(node.getName(), entity.getName())) {
                entity.setName(node.getName());
            }
            if (node.getParent() != null) {
                // check parent directory
                if (node.getParent().getData() instanceof IDirectory) {
                    IDirectory directory = (IDirectory) node.getParent().getData();
                    if (!ObjectUtils.sameObject(directory, entity.getDirectory())) {
                        entity.setDirectory(directory);
                    }
                }
            }
            if (entity instanceof IConfig<?>) {
                saveConfigNode(node);
            } else if (entity instanceof IDirectory) {
                saveDirectoryNode(node, true);
            }
        }
    }

    /**
     * Saves the {@link IConfig} linked with a given {@link ITreeNode}
     *
     * @param node The {@link ITreeNode} that contains the {@link IConfig} to save
     */
    protected void saveConfigNode(final ITreeNode node) {
        node.removeTreeNodeListener(this);
        IConfig<?> oldConfig = (IConfig<?>) node.getData();
        IConfig<?> newConfig = saveConfig(oldConfig, false);
        if (newConfig != null) {
            IDirectory directory = oldConfig.getDirectory();
            if (directory != null) {
                List<IConfig<?>> children = directory.getConfigList();
                if (children != null) {
                    int index = children.indexOf(oldConfig);
                    if (index > -1) {
                        children.set(index, newConfig);
                    }
                }
                newConfig.setDirectory(directory);
            }
            node.setData(newConfig);
        }
        node.addTreeNodeListener(this);
    }

    private IConfig<?> saveConfig(final IConfig<?> oldConfig, final boolean forcedSaved) {
        IConfig<?> newConfig = null;
        if (oldConfig != null) {
            oldConfig.setForcedSaving(forcedSaved);
            try {
                newConfig = ConfigAPIService.saveConfig(oldConfig);
                if (newConfig != null) {
                    newConfig.setForcedSaving(false);
                    oldConfig.setForcedSaving(false);
                    oldConfig.setTimestamp(newConfig.getTimestamp());
                }
            } catch (SalsaException e) {
                LOGGER.error("Cannot save configuration {} {}", oldConfig, e.getMessage());
                LOGGER.trace("Stack trace", e);
                int result = JOptionPane.NO_OPTION;
                result = JOptionPane.showConfirmDialog(null, e.getMessage() + "\n would you like to save anyway ?",
                        "Operation failed", JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    saveConfig(oldConfig, true);
                }
            }
        }
        return newConfig;
    }

    /**
     * Saves the {@link IDirectory} linked with a given {@link ITreeNode}
     *
     * @param node The {@link ITreeNode} that contains the {@link IDirectory} to save
     * @param recursive Whether to save children entity nodes
     */
    protected void saveDirectoryNode(final ITreeNode node, final boolean recursive) {
        node.removeTreeNodeListener(this);
        IDirectory directory = (IDirectory) node.getData();
        IDirectory savedDirectory = null;
        try {
            savedDirectory = ConfigAPIService.saveDirectory(directory);
            if ((directory.getDirectory() != null) && (savedDirectory.getDirectory() != null)) {
                directory.getDirectory().setTimestamp(savedDirectory.getDirectory().getTimestamp());
            }
            savedDirectory.setDirectory(directory.getDirectory());
            savedDirectory.setConfigList(directory.getConfigList());
            savedDirectory.setSubDirectoriesList(directory.getSubDirectoriesList());
            node.setData(savedDirectory);
        } catch (SalsaException e) {
            LOGGER.error("Cannot save directory {} {}", savedDirectory, e.getMessage());
            LOGGER.trace("Stack trace", e);
            savedDirectory = null;
        }
        if (savedDirectory != null) {
            for (ITreeNode child : node.getChildren()) {
                if (child.getData() instanceof IEntity) {
                    ((IEntity) child.getData()).setDirectory(savedDirectory);
                    if (recursive) {
                        saveEntityNode(child, recursive);
                    }
                }
            }
        }
        node.addTreeNodeListener(this);
    }

    /**
     * Deletes the {@link IEntity} associated with a given {@link ITreeNode}
     *
     * @param node The {@link ITreeNode}
     * @see #deleteConfigNode(ITreeNode)
     * @see #deleteDirectoryNode(ITreeNode)
     */
    protected void deleteEntityNode(final ITreeNode node) {
        if (node != null) {
            node.removeTreeNodeListener(this);
            IEntity entity = (IEntity) node.getData();
            IDirectory directory = entity.getDirectory();
            if (entity instanceof IConfig<?>) {
                deleteConfigNode(node);
            } else if (entity instanceof IDirectory) {
                deleteDirectoryNode(node);
            }
            entity.setDirectory(null);
            if (directory != null) {
                List<IConfig<?>> children = directory.getConfigList();
                if (children != null) {
                    children.remove(entity);
                }
            }
        }
    }

    /**
     * Deletes the {@link IConfig} associated with a given {@link ITreeNode}
     *
     * @param node The {@link ITreeNode}
     */
    protected void deleteConfigNode(final ITreeNode node) {
        IConfig<?> config = (IConfig<?>) node.getData();
        try {
            ConfigApi.deleteConfig(config);
        } catch (SalsaException e) {
            LOGGER.error("Cannot delete configuration {} {}", config, e.getMessage());
            LOGGER.trace("Stack trace", e);
        }
    }

    /**
     * Deletes the {@link IDirectory} associated with a given {@link ITreeNode}. Recursively deletes
     * the {@link ITreeNode}'s children, and the {@link IDirectory}'s children
     *
     * @param node The {@link ITreeNode}
     */
    protected void deleteDirectoryNode(final ITreeNode node) {
        IDirectory directory = (IDirectory) node.getData();
        List<ITreeNode> children = node.getChildren();
        int size = children.size();
        for (ITreeNode child : children) {
            if (node.getData() instanceof IEntity) {
                deleteEntityNode(child);
            }
        }
        for (int i = 0; i < size; i++) {
            node.removeNode(i);
        }
        try {
            ConfigApi.deleteDirectory(directory);
        } catch (SalsaException e) {
            LOGGER.error("Cannot delete directory {} {}", directory, e.getMessage());
            LOGGER.trace("Stack trace", e);
        }
    }

    /**
     * Recursively removes all listeners from an {@link ITreeNode} and its descendants
     *
     * @param node The concerned {@link ITreeNode}
     */
    protected void cleanListeners(final ITreeNode node) {
        if (node != null) {
            node.removeAllTreeNodeListeners();
            List<ITreeNode> children = node.getChildren();
            for (ITreeNode child : children) {
                cleanListeners(child);
            }
        }
    }

}

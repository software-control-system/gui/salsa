package fr.soleil.salsa.bean;

import javax.swing.JPanel;

import fr.soleil.salsa.configuration.view.ConfigTreeView;

public class BookmarksBean extends JPanel implements IConfigSelectionComponent,
        IBookmarksSelectionComponent, IBookmarkSelectionListener {

    // Bookmarks management

    private static final long serialVersionUID = 7864308278103713961L;

    private BookmarksCombo bookmarksCombo = new BookmarksCombo();

    private BookmarkComboConfigList configurationCombo = new BookmarkComboConfigList();

    public BookmarksBean() {
        bookmarksCombo.setEditable(false);
        bookmarksCombo.addBookmarkSelectionListener(this);
        add(bookmarksCombo);
        add(configurationCombo);
        bookmarksCombo.loadBookmarks();
        configurationCombo.loadBookmark();
    }

    @Override
    public void addConfigSelectionListener(IConfigSelectionListener listener) {
        configurationCombo.addConfigSelectionListener(listener);
        if (listener instanceof ConfigTreeView) {
            ((ConfigTreeView) listener).setBookmarkSelectionComponent(this);
        }
    }

    @Override
    public void removeConfigSelectionListener(IConfigSelectionListener listener) {
        configurationCombo.removeConfigSelectionListener(listener);
        if (listener instanceof ConfigTreeView) {
            ((ConfigTreeView) listener).setBookmarkSelectionComponent(null);
        }
    }

    public void refreshBookmarks() {
        configurationCombo.unloadBookmark();
        bookmarksCombo.refreshBookmarks();
        configurationCombo.loadBookmark();
    }

    public void setBookmark(String bookmark) {
        configurationCombo.setBookmark(bookmark);
    }

    public void setBookmaksComboVisible(boolean visible) {
        bookmarksCombo.setVisible(visible);
    }

    @Override
    public void addBookmarkSelectionListener(IBookmarkSelectionListener listener) {
        bookmarksCombo.addBookmarkSelectionListener(listener);

    }

    @Override
    public void removeBookmarkSelectionListener(IBookmarkSelectionListener listener) {
        bookmarksCombo.removeBookmarkSelectionListener(listener);
    }

    @Override
    public void loadBookmarks() {
        bookmarksCombo.loadBookmarks();
    }

    @Override
    public void unloadBookmarks() {
        bookmarksCombo.unloadBookmarks();
    }

    @Override
    public void bookmarksChanged(String bookmarks) {
        configurationCombo.setBookmark(bookmarks);
    }

}

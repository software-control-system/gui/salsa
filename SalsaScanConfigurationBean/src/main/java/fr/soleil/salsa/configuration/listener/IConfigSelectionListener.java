package fr.soleil.salsa.configuration.listener;

import java.util.EventListener;

import fr.soleil.salsa.entity.IConfig;

/**
 * Something that listens to {@link IConfig} selection
 * 
 * @author girardot
 */
public interface IConfigSelectionListener extends EventListener {

    /**
     * Notifies this {@link IConfigSelectionListener} about an {@link IConfig} selection change
     * 
     * @param currentConfig The newly selected {@link IConfig}
     */
    public void configSelectionChanged(IConfig<?> currentConfig);

}

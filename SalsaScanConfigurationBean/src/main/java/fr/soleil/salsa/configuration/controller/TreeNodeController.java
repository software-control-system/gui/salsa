package fr.soleil.salsa.configuration.controller;

import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.controller.BasicTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * A {@link BasicTargetController} to interact with {@link ITree}s
 * 
 * @author girardot
 */
public class TreeNodeController extends BasicTargetController<ITreeNode> {

    @Override
    protected ITreeNode generateDefaultValue() {
        return null;
    }

    @Override
    protected void setDataToTarget(ITarget target, ITreeNode data,
            AbstractDataSource<ITreeNode> source) {
        ((ITree) target).setRootNode(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data == null) || (data instanceof ITreeNode);
    }

    @Override
    // TODO remove this method
    public boolean isTargetAssignable(ITarget target) {
        return target instanceof ITree;
    }

    // TODO set Override method
    protected boolean isCompatibleWith(ITarget target) {
        return target instanceof ITree;
    }

}

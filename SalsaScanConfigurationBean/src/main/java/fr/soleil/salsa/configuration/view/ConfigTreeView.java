package fr.soleil.salsa.configuration.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.bean.IBookmarksSelectionComponent;
import fr.soleil.salsa.bean.IConfigSelectionListener;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.view.IView;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.configuration.controller.IConfigTreeController;
import fr.soleil.salsa.configuration.view.tool.ConfigTreeCellRenderer;
import fr.soleil.salsa.configuration.view.tool.DragAndDropEntityTree;
import fr.soleil.salsa.data.util.TreeNodeUtils;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IEntity;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * View showing the configuration tree.
 */
public class ConfigTreeView extends JPanel implements IView<IConfigTreeController>, IConfigSelectionListener {

    private static final long serialVersionUID = -4040399312166630196L;

    /**
     * Constant returned by confirmDeleteConfig if the user chooses to delete
     * the config.
     */
    public final int DELETE_CONFIG = 0;

    /**
     * Constant returned by confirmDeleteConfig if the user chooses to delete
     * the directory.
     */
    public final int DELETE_DIRECTORY = 1;

    /**
     * The controller.
     */
    private IConfigTreeController controller;

    /**
     * The tree containing the configurations.
     */
    private DragAndDropEntityTree configTree;

    /**
     * The scrolling panel containing the tree.
     */
    private JScrollPane scrollPane;

    private JMenuItem newDirectoryItem;

    private JMenuItem newConfig1DItem;

    private JMenuItem newConfig2DItem;

    private JMenuItem newConfigHCSItem;

    private JMenuItem newConfigScanKItem;

    private JMenuItem newConfigScanEnergyItem;

    private JMenuItem saveItem;

    private JMenuItem deleteItem;

    private JMenuItem renameItem;

    // Tree management
    private JButton expandallButton;

    private JButton collapseallButton;

    // Tree search tool
    private JTextField searchField;

    private JButton searchButtonNext;

    private JButton searchButtonPrevious;

    // Bookmarks management

    private JButton createBookmarks;

    private JButton deleteBookmarks;

    private final JComboBox<String> bookmarksCombo = new JComboBox<>();

    private final JMenu bookmarkMenu = new JMenu();

    private final JMenu addBookmarkMenu = new JMenu();

    private final JMenu removeBookmarkMenu = new JMenu();

    private String lastSelectedPath = null;

    private final JComboBox<ConfigItem> configurationCombo = new JComboBox<>();

    private IBookmarksSelectionComponent bookmarkSelectionComponent = null;

    /**
     * Default Constructor
     */
    public ConfigTreeView() {
        this(null);
    }

    /**
     * Constructor.
     * 
     * @param controller
     */
    public ConfigTreeView(IConfigTreeController controller) {
        super(new BorderLayout());
        this.initialize();
        setController(controller);
    }

    public void setBookmarkSelectionComponent(IBookmarksSelectionComponent component) {
        bookmarkSelectionComponent = component;
    }

    public void expandAll(boolean expand) {
        if (configTree != null) {
            configTree.expandAll(expand);
        }
    }

    public void searchPath(String contains) {
        if (SalsaUtils.isDefined(contains) && (configTree != null)) {
            String[] split = contains.trim().split("/");

            if ((split != null) && (split.length > 0)) {
                // Search all the match tree path.
                // If several response do nothing
                // If one match Expend the match node
                // ITreeNode rootNode = configTree.getRootNode();
                ITreeNode node = configTree.getRootNode();
                int index = 0;
                if ((node != null) && node.getName().equals(split[index])) {
                    index++;
                    while ((node != null) && (index < split.length)) {
                        List<ITreeNode> childrenNode = node.getChildren();
                        for (ITreeNode childNode : childrenNode) {
                            if (childNode.getName().equals(split[index])) {
                                node = childNode;
                                index++;
                                break;
                            }
                        }
                    }
                }

                if (node != null) {
                    configTree.selectNodes(false, node);
                }
            }
        }
    }

    /**
     * Initialization.
     * 
     * @return void
     */
    private void initialize() {
        configTree = new DragAndDropEntityTree();

        configTree.setModel(null);
        configTree.setCellRenderer(new ConfigTreeCellRenderer());
        configTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);

        scrollPane = new JScrollPane(configTree);
        scrollPane.setColumnHeaderView(createConfigTreeManagementPanel());
        add(scrollPane, BorderLayout.CENTER);

        configTree.getPopupMenu().removeAll();

        newDirectoryItem = new JMenuItem();
        newDirectoryItem.setText("New Directory");
        newDirectoryItem.setIcon(Icons.getIcon("salsa.scanconfig.folder-new"));
        newDirectoryItem.setMnemonic(KeyEvent.VK_R);
        newDirectoryItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
        newDirectoryItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String directoryName = askNewEntityName(IDirectory.class);
                if (directoryName != null) {
                    if (controller != null) {
                        controller.notifyNewDirectory(directoryName);
                    }
                }
            }
        });
        configTree.getPopupMenu().add(newDirectoryItem);

        newConfig1DItem = new JMenuItem();
        newConfig1DItem.setText("New 1D");
        newConfig1DItem.setIcon(Icons.getIcon("salsa.scanconfig.1d.small"));
        newConfig1DItem.setMnemonic(KeyEvent.VK_N);
        newConfig1DItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        newConfig1DItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newScan(IConfig.ScanType.SCAN_1D);
            }
        });
        configTree.getPopupMenu().add(newConfig1DItem);

        newConfig2DItem = new JMenuItem();
        newConfig2DItem.setText("New 2D");
        newConfig2DItem.setIcon(Icons.getIcon("salsa.scanconfig.2d.small"));
        newConfig2DItem.setMnemonic(KeyEvent.VK_E);
        newConfig2DItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        newConfig2DItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newScan(IConfig.ScanType.SCAN_2D);
            }
        });
        configTree.getPopupMenu().add(newConfig2DItem);

        newConfigHCSItem = new JMenuItem();
        newConfigHCSItem.setText("New HCS");
        newConfigHCSItem.setIcon(Icons.getIcon("salsa.scanconfig.hcs.small"));
        newConfigHCSItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newScan(IConfig.ScanType.SCAN_HCS);
            }
        });
        configTree.getPopupMenu().add(newConfigHCSItem);

        newConfigScanKItem = new JMenuItem();
        newConfigScanKItem.setText("New scan K");
        newConfigScanKItem.setIcon(Icons.getIcon("salsa.scanconfig.k.small"));
        newConfigScanKItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newScan(IConfig.ScanType.SCAN_K);
            }
        });
        configTree.getPopupMenu().add(newConfigScanKItem);

        newConfigScanEnergyItem = new JMenuItem();
        newConfigScanEnergyItem.setText("New scan energy");
        newConfigScanEnergyItem.setIcon(Icons.getIcon("salsa.scanconfig.energy.small"));
        newConfigScanEnergyItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newScan(IConfig.ScanType.SCAN_ENERGY);
            }
        });
        configTree.getPopupMenu().add(newConfigScanEnergyItem);

        configTree.getPopupMenu().addSeparator();

        saveItem = new JMenuItem();
        saveItem.setText("Save");
        saveItem.setIcon(Icons.getIcon("salsa.scanconfig.save.small"));
        saveItem.setMnemonic(KeyEvent.VK_S);
        saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configTree.notifySaveSelectedNodes();
            }
        });
        configTree.getPopupMenu().add(saveItem);

        configTree.getPopupMenu().addSeparator();

        bookmarkMenu.setText("Bookmarks");
        bookmarkMenu.setToolTipText("Add / Remove configuration to / from bookmarks");
        bookmarkMenu.setIcon(Icons.getIcon("salsa.scantree.bookmarks"));

        addBookmarkMenu.setText("Add to bookmarks");
        removeBookmarkMenu.setText("Remove from bookmarks");
        bookmarkMenu.setEnabled(false);
        addBookmarkMenu.setEnabled(false);
        removeBookmarkMenu.setEnabled(false);
        bookmarkMenu.add(addBookmarkMenu);
        bookmarkMenu.add(removeBookmarkMenu);
        configTree.getPopupMenu().add(bookmarkMenu);

        configTree.getPopupMenu().addSeparator();

        configTree.getPopupMenu().add(configTree.getCutAction());
        configTree.getPopupMenu().add(configTree.getCopyAction());
        configTree.getPopupMenu().add(configTree.getPasteAction());

        deleteItem = new JMenuItem();
        deleteItem.setText("Delete");
        deleteItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        deleteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedNodes();
            }
        });
        configTree.getPopupMenu().add(deleteItem);

        renameItem = new JMenuItem();
        renameItem.setText("Rename");
        renameItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
        renameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TreePath path = configTree.getSelectionPath();
                if (path != null) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                    String configPath = extractCompletePath(node);
                    ITreeNode treeNode = configTree.getModel().getTreeNode(node);
                    if (treeNode.getData() instanceof IEntity) {
                        IEntity entity = (IEntity) treeNode.getData();
                        String entityName = askRenameEntityName(entity.getClass(), treeNode.getName());
                        if (entityName != null) {
                            // updating node name will generate an event, that
                            // will make the source
                            // save the entity with an updated name
                            treeNode.setName(entityName);
                            configTree.repaint();
                            DefaultMutableTreeNode newNode = (DefaultMutableTreeNode) path.getLastPathComponent();
                            String newConfigPath = extractCompletePath(newNode);
                            UIPreferences.getInstance().replaceEntryInBookmarks(configPath, newConfigPath);
                            buildBookmarks();
                            updateMenuEnabled(true);

                        }
                    }
                }
            }
        });

        configTree.getPopupMenu().add(renameItem);

        configTree.getPopupMenu().addSeparator();

        configTree.getPopupMenu().add(configTree.getRefreshAction());
        configTree.getPopupMenu().add(configTree.getRepainAction());

        // Due to a bug in Swing, accelerators do not work for jPopupMenu. This
        // is a workaround.
        configTree.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent(e);
                JMenuItem menuItem;
                ActionListener[] listeners;
                for (MenuElement menuElement : configTree.getPopupMenu().getSubElements()) {
                    if (menuElement instanceof JMenuItem) {
                        menuItem = (JMenuItem) menuElement;
                        if (keyStroke.equals(menuItem.getAccelerator())) {
                            listeners = menuItem.getActionListeners();
                            if (listeners.length > 0) {
                                listeners[0].actionPerformed(null);
                            }
                        }
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    private JPanel createConfigTreeManagementPanel() {
        JPanel configTreeManagement = new JPanel();
        configTreeManagement.setLayout(new BorderLayout());
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new FlowLayout());

        configTreeManagement.add(searchPanel, BorderLayout.NORTH);

        JLabel searchlabel = new JLabel(Icons.getIcon("salsa.scantree.search"));
        searchlabel.setToolTipText("Search tool");

        expandallButton = new JButton(Icons.getIcon("salsa.scantree.expandall"));
        expandallButton.setToolTipText("Expand all");

        expandallButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                expandAll(true);
            }
        });

        collapseallButton = new JButton(Icons.getIcon("salsa.scantree.collapseall"));
        collapseallButton.setToolTipText("Collapse all");

        collapseallButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                expandAll(false);
            }
        });

        searchField = new JTextField();
        Dimension searchFieldDim = new Dimension();
        searchFieldDim.setSize(100, 25);
        searchField.setPreferredSize(searchFieldDim);

        searchButtonNext = new JButton();
        // searchButtonNext.setIcon((ImageIcon)
        // Icons.getIcon("salsa.scantree.search"));
        searchButtonNext.setToolTipText("Search next occurence");
        searchButtonNext.setText(">>");

        searchButtonNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((searchField != null) && (configTree != null)) {
                    String textToFind = searchField.getText();
                    configTree.searchNextNode(textToFind);
                }
            }
        });

        searchButtonPrevious = new JButton();
        // searchButtonPrevious.setIcon((ImageIcon)
        // Icons.getIcon("salsa.scantree.search"));
        searchButtonPrevious.setToolTipText("Search previous occurence");
        searchButtonPrevious.setText("<<");

        searchButtonPrevious.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((searchField != null) && (configTree != null)) {
                    String textToFind = searchField.getText();
                    configTree.searchPreviousNode(textToFind);
                }
            }
        });

        searchPanel.add(searchlabel);
        searchPanel.add(expandallButton);
        searchPanel.add(collapseallButton);
        searchPanel.add(searchField);
        searchPanel.add(searchButtonPrevious);
        searchPanel.add(searchButtonNext);

        JPanel bookmarkPanel = new JPanel();
        bookmarkPanel.setLayout(new FlowLayout());

        JLabel bookmarkslabel = new JLabel(Icons.getIcon("salsa.scantree.bookmarks"));
        bookmarkslabel.setToolTipText("Bookmarks management");
        bookmarkPanel.add(bookmarkslabel);

        createBookmarks = new JButton("+");
        createBookmarks.setToolTipText("Add bookmark");
        createBookmarks.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {

                Object item = bookmarksCombo.getSelectedItem();
                if (item != null) {
                    String bookmark = item.toString();
                    if (SalsaUtils.isDefined(bookmark)) {
                        UIPreferences.getInstance().addBookmarks(bookmark);
                        buildBookmarks();
                        updateMenuEnabled(true);
                        if (bookmarkSelectionComponent != null) {
                            bookmarkSelectionComponent.refreshBookmarks();
                        }
                    }
                }

            }
        });
        bookmarkPanel.add(createBookmarks);

        deleteBookmarks = new JButton("-");
        deleteBookmarks.setToolTipText("Delete bookmark");
        deleteBookmarks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Object item = bookmarksCombo.getSelectedItem();
                if (item != null) {
                    String bookmark = item.toString();
                    if (SalsaUtils.isDefined(bookmark)) {
                        UIPreferences.getInstance().removeBookmark(bookmark);
                        buildBookmarks();
                        updateMenuEnabled(true);
                        if (bookmarkSelectionComponent != null) {
                            bookmarkSelectionComponent.refreshBookmarks();
                        }
                    }
                }

            }
        });

        bookmarkPanel.add(deleteBookmarks);

        bookmarksCombo.setEditable(true);
        bookmarkPanel.add(bookmarksCombo);

        bookmarksCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (arg0.getModifiers() != 0) {
                    refreshConfigBookmarks();
                } else {
                    refrehsBookmarsButton();
                }
            }
        });

        configurationCombo.setRenderer(new ConfigItemRenderer());
        configurationCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (arg0.getModifiers() != 0) {
                    Object object = configurationCombo.getSelectedItem();
                    if ((object != null) && (object instanceof ConfigItem)) {
                        searchPath(((ConfigItem) object).getKey());
                    }
                }
            }
        });

        bookmarkPanel.add(configurationCombo);

        configTreeManagement.add(bookmarkPanel, BorderLayout.SOUTH);

        return configTreeManagement;

    }

    private class ConfigItem {
        private String value;
        private String key;

        public ConfigItem(String key, String value) {
            super();
            this.value = value;
            this.key = key;
        }

        public String getKey() {
            return key;
        }

        @Override
        public String toString() {
            return value;
        }

    }

    private class ConfigItemRenderer extends BasicComboBoxRenderer {

        private static final long serialVersionUID = 5802138256652281636L;

        @Override
        public Component getListCellRendererComponent(@SuppressWarnings("unchecked") JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
                if (-1 < index) {
                    if ((value != null) && (value instanceof ConfigItem)) {
                        list.setToolTipText(((ConfigItem) value).getKey());
                    }
                }
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(list.getFont());
            setText((value == null) ? ObjectUtils.EMPTY_STRING : value.toString());
            return this;
        }
    }

    @Override
    public IConfigTreeController getController() {
        return controller;
    }

    @Override
    public void setController(IConfigTreeController controller) {
        if (!ObjectUtils.sameObject(controller, this.controller)) {
            if (this.controller != null) {
                configTree.removeTreeNodeSelectionListener(this.controller);
            }
            this.controller = controller;
            if (this.controller != null) {
                configTree.addTreeNodeSelectionListener(this.controller);
            }
            buildBookmarks();
        }
    }

    /**
     * Deletes the selected nodes
     */
    public void deleteSelectedNodes() {
        List<ITreeNode> selection = configTree.getSelectedNodesSortedByPath();
        TreeNodeUtils.filterMultipleSelection(selection);
        for (ITreeNode node : selection) {
            if (node.getParent() != null) {
                ITreeNode parent = node.getParent();
                int ok = JOptionPane.YES_OPTION;
                if (node.getData() instanceof IEntity) {
                    ok = confirmDeleteEntity((IEntity) node.getData());
                }
                if (ok == JOptionPane.YES_OPTION) {
                    parent.removeNode(node);
                }
            }
        }
    }

    /**
     * Copies selected nodes
     */
    public void copyNodes() {
        configTree.copyOrCutSelectedNodes(true);
    }

    /**
     * Cuts selected nodes
     */
    public void cutyNodes() {
        configTree.copyOrCutSelectedNodes(false);
    }

    /**
     * Pastes previously selected nodes
     */
    public void pasteNodes() {
        configTree.pasteNodes();
    }

    /**
     * Saves selected configs/directories
     */
    public void saveSelectedNodes() {
        configTree.notifySaveSelectedNodes();
    }

    /**
     * Saves all configs/directories
     */
    public void saveAll() {
        configTree.notifySaveAll();
    }

    /**
     * Returns the scan configuration tree used by this view
     * 
     * @return An {@link ITree}
     */
    public ITree getConfigTree() {
        return configTree;
    }

    /**
     * Displays a popup that asks an {@link IEntity} name.
     * 
     * @param entityClass
     *            The class of {@link IEntity} for which to ask a new name
     * @return The {@link IEntity} name
     */
    private String askNewEntityName(Class<? extends IEntity> entityClass) {
        String defaultName = (IConfig.class.isAssignableFrom(entityClass) ? "Configuration" : "Directory");
        StringBuilder message = new StringBuilder(defaultName);
        message.append(" name:");
        StringBuilder titleBuilder = new StringBuilder("New ");
        titleBuilder.append(defaultName.toLowerCase());
        String result = JOptionPane.showInputDialog(this, message, titleBuilder.toString(), JOptionPane.PLAIN_MESSAGE);
        if ((result != null) && result.contains("/")) {
            JOptionPane.showMessageDialog(this, "The caracter '/' is not allowed");
            return askNewEntityName(entityClass);
        }
        return result;
    }

    /**
     * Displays a popup that asks an {@link IEntity} name.
     * 
     * @param entityClass
     *            The class of {@link IEntity} for which to ask a new name
     * @return The {@link IEntity} name
     */
    private String askRenameEntityName(Class<? extends IEntity> entityClass, String defaultValue) {
        String defaultName = (IConfig.class.isAssignableFrom(entityClass) ? "Configuration" : "Directory");
        StringBuilder message = new StringBuilder(defaultName);
        message.append(" name:");
        StringBuilder titleBuilder = new StringBuilder("Rename ");
        titleBuilder.append(defaultName.toLowerCase());
        String result = JOptionPane.showInputDialog(this, titleBuilder.toString(), defaultValue);
        if ((result != null) && result.contains("/")) {
            JOptionPane.showMessageDialog(this, "The caracter '/' is not allowed");
            return askNewEntityName(entityClass);
        }
        return result;
    }

    /**
     * New Scan 1d process.
     */
    public void newScan(IConfig.ScanType scanType) {
        String configName = askNewEntityName(IConfig.class);
        if (controller != null) {
            controller.notifyNewConfig(configName, scanType);
        }
    }

    /**
     * New directory process.
     */
    public void newDirectory() {
        String configName = askNewEntityName(IDirectory.class);
        if (controller != null) {
            controller.notifyNewDirectory(configName);
        }
    }

    /**
     * Asks the user a confirmation for deleting an {@link IEntity}.
     * 
     * @param entity
     *            The {@link IEntity} to delete
     * @return The selected choice in {@link JOptionPane}
     * @see JOptionPane#showConfirmDialog(java.awt.Component, Object, String,
     *      int, int, javax.swing.Icon)
     */
    public int confirmDeleteEntity(IEntity entity) {
        StringBuilder message = new StringBuilder("Are you sure you want to delete the ");
        message.append(entity.getName()).append(" ");
        if (entity instanceof IConfig<?>) {
            message.append("config");
        } else {
            message.append("directory");
        }
        message.append("?");
        int choice = JOptionPane.showConfirmDialog(this, message, "Delete", JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE, null);
        return choice;
    }

    /**
     * Adds an {@link IEntity} in this {@link ConfigTreeView}'s {@link ITree}
     * 
     * @param entity
     *            The {@link IEntity} to add
     */
    public void addEntity(IEntity entity) {
        if (entity != null) {
            configTree.addNodes(TreeNodeUtils.buildNode(entity, false));
        }
    }

    /**
     * Selects a path in tree, and makes sure this path is visible
     * 
     * @param path
     *            The path to select
     * @param onlyIfNoSelection
     *            If <code>true</code>, the path will be selected and visible
     *            only if there is no path already selected in tree
     */
    public void selectPath(String path, boolean onlyIfNoSelection) {
        if (SalsaUtils.isDefined(path) && (configTree != null) && (configTree.getModel() != null)) {
            if ((!onlyIfNoSelection) || (configTree.getSelectionPath() == null)) {
                String[] splittedPath = path.split("/");
                int pathIndex = 0;
                DefaultMutableTreeNode node = null;
                while (pathIndex < splittedPath.length) {
                    boolean found = false;
                    if (node == null) {
                        node = configTree.getModel().getRoot();
                        found = splittedPath[pathIndex].equals(extractName(node));
                    } else {
                        for (int childIndex = 0; childIndex < node.getChildCount(); childIndex++) {
                            DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(childIndex);
                            if (splittedPath[pathIndex].equals(extractName(child))) {
                                found = true;
                                node = child;
                                break;
                            }
                        }
                    }
                    if (found) {
                        pathIndex++;
                    } else {
                        break;
                    }
                }
                if ((node != null) && (pathIndex == splittedPath.length)) {
                    TreePath toSelect = new TreePath(node.getPath());
                    configTree.expandPath(toSelect);
                    configTree.setSelectionPath(toSelect);
                    configTree.setLeadSelectionPath(toSelect);
                }
            }
        }
    }

    /**
     * Extracts the {@link ITreeNode}'s name for a given
     * {@link DefaultMutableTreeNode}
     * 
     * @param node
     *            The {@link DefaultMutableTreeNode} from which to extract the
     *            name
     * @return A {@link String}
     */
    private String extractName(DefaultMutableTreeNode node) {
        String name = null;
        if ((node != null) && (configTree != null) && (configTree.getModel() != null)) {
            ITreeNode treeNode = configTree.getModel().getTreeNode(node);
            if (treeNode != null) {
                name = treeNode.getName();
            }
        }
        return name;
    }

    private String extractCompletePath(DefaultMutableTreeNode node) {
        String name = null;
        if ((node != null) && (configTree != null) && (configTree.getModel() != null)) {
            ITreeNode treeNode = configTree.getModel().getTreeNode(node);
            if (treeNode != null) {
                name = treeNode.getName();
                ITreeNode parent = treeNode.getParent();
                while (parent != null) {
                    name = parent.getName() + "/" + name;
                    parent = parent.getParent();
                }
            }
        }
        return name;
    }

    public void updateMenuEnabled(boolean force) {
        TreePath path = configTree.getSelectionPath();
        if (path != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
            String nodePath = extractCompletePath(node);
            if ((SalsaUtils.isDefined(nodePath) && (!nodePath.equalsIgnoreCase(lastSelectedPath))) || force) {
                lastSelectedPath = nodePath;
                boolean isLeaf = node.isLeaf();
                bookmarkMenu.setEnabled(isLeaf);
                addBookmarkMenu.removeAll();
                addBookmarkMenu.setEnabled(false);
                removeBookmarkMenu.removeAll();
                removeBookmarkMenu.setEnabled(false);

                Map<String, Map<String, String>> bookmarks = UIPreferences.getInstance().getBookmarks();
                Set<String> bookmarkKeySet = bookmarks.keySet();
                List<JMenuItem> addItemList = new ArrayList<JMenuItem>();
                List<JMenuItem> removeItemList = new ArrayList<JMenuItem>();

                for (String bookmarkKey : bookmarkKeySet) {
                    Map<String, String> configMap = bookmarks.get(bookmarkKey);
                    if (configMap.containsKey(nodePath)) {
                        JMenuItem removeItem = new JMenuItem(bookmarkKey);
                        removeItem.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent arg0) {
                                if (controller != null) {
                                    String bookmarkLabel = ((JMenuItem) arg0.getSource()).getText();
                                    UIPreferences.getInstance().removeEntryFromBookmark(bookmarkLabel,
                                            lastSelectedPath);
                                    buildBookmarks();
                                    updateMenuEnabled(true);

                                }
                            }
                        });
                        removeItemList.add(removeItem);
                    } else {
                        JMenuItem addItem = new JMenuItem(bookmarkKey);
                        addItem.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent arg0) {
                                if (controller != null) {
                                    String bookmarkLabel = ((JMenuItem) arg0.getSource()).getText();
                                    String customLabel = JOptionPane.showInputDialog(configTree,
                                            "Add to " + bookmarkLabel + " bookmark", lastSelectedPath);
                                    if (SalsaUtils.isDefined(customLabel)) {
                                        UIPreferences.getInstance().addEntryToBookmarks(bookmarkLabel, lastSelectedPath,
                                                customLabel);
                                        buildBookmarks();
                                        updateMenuEnabled(true);
                                    }

                                }
                            }
                        });

                        addItemList.add(addItem);
                    }
                }

                addBookmarkMenu.setEnabled(!addItemList.isEmpty());
                for (JMenuItem addItem : addItemList) {
                    addBookmarkMenu.add(addItem);
                }
                removeBookmarkMenu.setEnabled(!removeItemList.isEmpty());
                for (JMenuItem removeItem : removeItemList) {
                    removeBookmarkMenu.add(removeItem);
                }

            }
        }

    }

    private void refreshConfigBookmarks() {
        configurationCombo.removeAllItems();
        Map<String, Map<String, String>> bookmarks = UIPreferences.getInstance().getBookmarks();
        Object object = bookmarksCombo.getSelectedItem();
        if (object != null) {
            String selectedBookmarks = object.toString();
            Map<String, String> map = bookmarks.get(selectedBookmarks);
            if (map != null) {
                createBookmarks.setEnabled(false);
                deleteBookmarks.setEnabled(true);
                Set<Entry<String, String>> entrySet = map.entrySet();
                for (Entry<String, String> entry : entrySet) {
                    configurationCombo.addItem(new ConfigItem(entry.getKey(), entry.getValue()));
                }
            } else {
                createBookmarks.setEnabled(true);
                deleteBookmarks.setEnabled(false);
            }
        }
    }

    private void refrehsBookmarsButton() {
        Map<String, Map<String, String>> bookmarks = UIPreferences.getInstance().getBookmarks();
        Object object = bookmarksCombo.getSelectedItem();
        if (object != null) {
            String selectedBookmarks = object.toString();
            Map<String, String> map = bookmarks.get(selectedBookmarks);
            if (map != null) {
                createBookmarks.setEnabled(false);
                deleteBookmarks.setEnabled(true);
            } else {
                createBookmarks.setEnabled(true);
                deleteBookmarks.setEnabled(false);
            }
        }
    }

    private void buildBookmarks() {
        Map<String, Map<String, String>> bookmarks = UIPreferences.getInstance().getBookmarks();
        if (bookmarks != null) {
            Set<String> bookmarkKeySet = bookmarks.keySet();
            String[] keyArray = bookmarkKeySet.toArray(new String[bookmarkKeySet.size()]);
            Arrays.sort(keyArray);
            bookmarksCombo.removeAllItems();
            for (String bookmarkKey : keyArray) {
                bookmarksCombo.addItem(bookmarkKey);
            }
            refreshConfigBookmarks();
        }
    }

    @Override
    public void configPathChanged(String configPath) {
        searchPath(configPath);
    }

}

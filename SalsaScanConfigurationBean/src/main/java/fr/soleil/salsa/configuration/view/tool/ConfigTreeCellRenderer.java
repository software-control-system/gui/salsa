package fr.soleil.salsa.configuration.view.tool;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;

import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;

/**
 * A {@link ConfigTreeCellRenderer} that can display {@link IConfig}s as modified
 * 
 * @author girardot
 */
public class ConfigTreeCellRenderer extends CometeTreeCellRenderer {

    private static final long serialVersionUID = -2531006063256948716L;

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
            boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JComponent component = (JComponent) super.getTreeCellRendererComponent(tree, value, sel,
                expanded, leaf, row, hasFocus);
        ITreeNode treeNode = getTreeNode(value, tree);
        if (treeNode != null) {
            Object data = treeNode.getData();
            if (data instanceof IConfig<?>) {
                IConfig<?> config = (IConfig<?>) data;
                String name = treeNode.getName();
                if ((name != null) && (config.isModified())) {
                    ((JLabel) component).setText(name + "*");
                }
            }
            else if (data instanceof IDirectory) {
                String folderURL = expanded ? "salsa.scanconfig.folder-open"
                        : "salsa.scanconfig.folder";
                ((JLabel) component).setIcon(Icons.getIcon(folderURL));
            }
            component.setPreferredSize(null);
            Dimension size = component.getPreferredSize();
            component.setPreferredSize(new Dimension(size.width + 5, size.height));
        }
        return component;
    }
}

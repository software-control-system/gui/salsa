package fr.soleil.salsa.configuration.controller;

import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.configuration.listener.IConfigSelectionListener;
import fr.soleil.salsa.configuration.view.ConfigTreeView;
import fr.soleil.salsa.entity.IConfig;

/**
 * Interface of a controller of a config tree view.
 */
public interface IConfigTreeController extends IController<ConfigTreeView>, ITreeNodeSelectionListener,
        ITreeNodeListener {

    /**
     * Method called when the user clicks on a new configuration.
     */
    public void notifyNewConfig(String configName, IConfig.ScanType type);

    /**
     * Method called when the user clicks on a new configuration.
     */
    public void notifyNewConfig(IConfig<?> newConfig);

    /**
     * Method called when the user clicks on new directory.
     */
    public void notifyNewDirectory(String directoryName);

    /**
     * Selects the scan configuration that corresponds to the current scan
     * result, if possible
     */
    public void selectScanResult();

    /**
     * Adds an {@link IConfigSelectionListener} to this
     * {@link IConfigTreeController}
     * 
     * @param listener
     *            The {@link IConfigSelectionListener} to add
     */
    public void addConfigSelectionListener(IConfigSelectionListener listener);

    /**
     * Removes an {@link IConfigSelectionListener} from this
     * {@link IConfigTreeController}
     * 
     * @param listener
     *            The {@link IConfigSelectionListener} to remove
     */
    public void removeConfigSelectionListener(IConfigSelectionListener listener);

    public IConfig<?> getConfig();

    public void notifyListeners(final IConfig<?> currentConfig);

}

package fr.soleil.salsa.configuration.controller.impl;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.event.TreeNodeEvent.NodeEventReason;
import fr.soleil.comete.definition.event.TreeNodeSelectionEvent;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.controller.AErrorManagingController;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.configuration.controller.IConfigTreeController;
import fr.soleil.salsa.configuration.controller.TreeNodeController;
import fr.soleil.salsa.configuration.listener.IConfigSelectionListener;
import fr.soleil.salsa.configuration.view.ConfigTreeView;
import fr.soleil.salsa.data.source.ScanConfigSource;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.DirectoryModel;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.impl.DisplayImpl;
import fr.soleil.salsa.entity.impl.ScanAddOnImp;
import fr.soleil.salsa.entity.impl.scan1d.Config1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Dimension1DImpl;
import fr.soleil.salsa.entity.impl.scan1d.Range1DImpl;
import fr.soleil.salsa.entity.impl.scan2d.Config2DImpl;
import fr.soleil.salsa.entity.impl.scan2d.Dimension2DXImpl;
import fr.soleil.salsa.entity.impl.scan2d.Dimension2DYImpl;
import fr.soleil.salsa.entity.impl.scan2d.Range2DXImpl;
import fr.soleil.salsa.entity.impl.scan2d.Range2DYImpl;
import fr.soleil.salsa.entity.impl.scanenergy.ConfigEnergyImpl;
import fr.soleil.salsa.entity.impl.scanenergy.DimensionEnergyImpl;
import fr.soleil.salsa.entity.impl.scanhcs.ConfigHCSImpl;
import fr.soleil.salsa.entity.impl.scanhcs.DimensionHCSImpl;
import fr.soleil.salsa.entity.impl.scanhcs.RangeHCSImpl;
import fr.soleil.salsa.entity.impl.scank.ConfigKImpl;
import fr.soleil.salsa.entity.impl.scank.DimensionKImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class ConfigTreeController extends AErrorManagingController<ConfigTreeView>
        implements IConfigTreeController, IListener<EntityPropertyChangedEvent<IConfig<?>>> {

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigTreeController.class);
    private final String scanResultName;

    private final List<WeakReference<IConfigSelectionListener>> listeners;
    private TreeNodeController nodeController;
    private ScanConfigSource configSource;
    private IConfig<?> lastSelectedConfig;
    private ITreeNode lastListenedNode;

    /**
     * Default Constructor
     * 
     * @param errorController
     *            The {@link IErrorController} that manages error messages
     */
    public ConfigTreeController(IErrorController errorController) {
        this(null, errorController);
    }

    /**
     * Constructs a new {@link ConfigTreeController} with a given
     * {@link ConfigTreeView} and an {@link IErrorController}
     * 
     * @param view
     *            The {@link ConfigTreeView} to assiciate with this
     *            {@link ConfigTreeController}
     * @param errorController
     *            The {@link IErrorController} that manages error messages
     */
    public ConfigTreeController(ConfigTreeView view, IErrorController errorController) {
        super(view, errorController);
        listeners = new ArrayList<>();
        scanResultName = null;
    }

    @Override
    protected ConfigTreeView generateView() {
        return new ConfigTreeView(this);
    }

    @Override
    public void selectionChanged(TreeNodeSelectionEvent event) {
        if ((event != null) && event.isSelected() && (view != null) && (view.getConfigTree() != null)
                && (event.getSource() == view.getConfigTree())) {
            updateSelectedConfig();
        }
    }

    private void updateSelectedConfig() {
        IConfig<?> config = null;
        ITreeNode[] selectedNodes = view.getConfigTree().getSelectedNodes();
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            ITreeNode toCheck = selectedNodes[0];
            if (toCheck != lastListenedNode) {
                if (lastListenedNode != null) {
                    lastListenedNode.removeTreeNodeListener(this);
                }
                lastListenedNode = toCheck;
                if (lastListenedNode != null) {
                    lastListenedNode.addTreeNodeListener(this);
                }
            }
            if (toCheck.getData() instanceof IConfig<?>) {
                config = (IConfig<?>) toCheck.getData();
            }
        }
        // if (config != null) {
        if (config != lastSelectedConfig) {
            // if (lastSelectedConfig instanceof IEventSource<?>) {
            // try {
            // ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) lastSelectedConfig).removeListener(this);
            // } catch (Exception e) {
            // // Ignore this case: we don't care if we can't listen to
            // // config
            // }
            // }
            lastSelectedConfig = config;
            // if (lastSelectedConfig instanceof IEventSource<?>) {
            // try {
            // ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) lastSelectedConfig).addListener(this);
            // } catch (Exception e) {
            // // Ignore this case: we don't care if we can't listen to
            // // config
            // }
            // }
            if (view != null) {
                view.updateMenuEnabled(false);
            }
            // }
        }

        notifyListeners(config);
    }

    @SuppressWarnings("unchecked")
    public List<IConfig<?>> getSelectedConfiguration() {
        List<IConfig<?>> configList = new ArrayList<>();
        ITreeNode[] selectedNodes = view.getConfigTree().getSelectedNodes();
        if (selectedNodes != null) {
            IConfig<?> tmpConfig = null;
            for (ITreeNode node : selectedNodes) {
                if (node.getData() instanceof IConfig<?>) {
                    tmpConfig = (IConfig<?>) node.getData();
                    try {
                        configList.add(tmpConfig);
                        if (tmpConfig instanceof IEventSource<?>) {
                            ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) tmpConfig).addListener(this);
                        }
                    } catch (Exception e) {
                        // Ignore this case: we don't care if we can't listen to
                        // config
                    }
                }
            }
        }
        return configList;
    }

    @Override
    public void notifyNewConfig(String configName, IConfig.ScanType type) {
        if (SalsaUtils.isDefined(configName) && (type != null)) {
            IConfig<?> config;
            IConfig<?> configModel = null;
            switch (type) {
                case SCAN_1D:
                    config = new Config1DImpl();
                    config.setScanAddOn(new ScanAddOnImp());
                    config.getScanAddOn().setDisplay(new DisplayImpl());
                    ((IConfig1D) config).setDimensionX(new Dimension1DImpl());
                    // Creates one range for a new 1D config.
                    IRange1D range1DToAdd = new Range1DImpl();
                    range1DToAdd.setDimension(((IConfig1D) config).getDimensionX());
                    range1DToAdd.setIntegrationTime(new double[] { 1.0 });
                    range1DToAdd.setStepsNumber(1);
                    ((IConfig1D) config).getDimensionX().getRangesXList().add(range1DToAdd);
                    break;

                case SCAN_2D:
                    config = new Config2DImpl();
                    config.setScanAddOn(new ScanAddOnImp());
                    config.getScanAddOn().setDisplay(new DisplayImpl());
                    ((IConfig2D) config).setDimensionX(new Dimension2DXImpl());
                    // Creates one X range for a new 2D config.
                    IRange2DX xRangeToAdd = new Range2DXImpl();
                    xRangeToAdd.setDimension(((IConfig2D) config).getDimensionX());
                    xRangeToAdd.setIntegrationTime(new double[] { 1.0 });
                    xRangeToAdd.setStepsNumber(1);
                    ((IConfig2D) config).getDimensionX().getRangesList().add(xRangeToAdd);
                    // Creates one Y range for a new 2D config.
                    ((IConfig2D) config).setDimensionY(new Dimension2DYImpl());
                    Range2DYImpl yRangeToAdd = new Range2DYImpl();
                    yRangeToAdd.setDimension(((IConfig2D) config).getDimensionY());
                    yRangeToAdd.setStepsNumber(1);
                    ((IConfig2D) config).getDimensionY().getRangesList().add(yRangeToAdd);
                    break;

                case SCAN_HCS:
                    config = new ConfigHCSImpl();
                    config.setScanAddOn(new ScanAddOnImp());
                    config.getScanAddOn().setDisplay(new DisplayImpl());
                    ((IConfigHCS) config).setDimensionX(new DimensionHCSImpl());
                    // Creates one range for a new HCS config.
                    IRangeHCS rangeHCSToAdd = new RangeHCSImpl();
                    rangeHCSToAdd.setDimension(((IConfigHCS) config).getDimensionX());
                    ((IConfigHCS) config).getDimensionX().getRangesXList().add(rangeHCSToAdd);
                    break;

                case SCAN_K:
                    config = new ConfigKImpl();
                    config.setScanAddOn(new ScanAddOnImp());
                    config.getScanAddOn().setDisplay(new DisplayImpl());
                    ((IConfigK) config).setDimensionX(new DimensionKImpl());
                    break;

                case SCAN_ENERGY:
                    config = new ConfigEnergyImpl();
                    config.setScanAddOn(new ScanAddOnImp());
                    config.getScanAddOn().setDisplay(new DisplayImpl());
                    ((IConfigEnergy) config).setDimensionX(new DimensionEnergyImpl());
                    break;

                default:
                    config = null;
                    break;
            }
            if (config != null) {
                // configModel = AutoCopier.copy(config);
                if (config instanceof IObjectImpl<?>) {
                    configModel = (IConfig<?>) ((IObjectImpl<?>) config).toModel();
                    configModel.setName(configName);
                    configModel.setType(type);
                    configModel.setLoaded(true);
                    configModel.setModified(true);
                } else {
                    configModel = config;
                }
                view.addEntity(configModel);
            }
        }
    }

    @Override
    public void notifyNewConfig(IConfig<?> newConfig) {
        if (newConfig != null) {
            IConfig<?> configModel = null;
            if (newConfig instanceof IObjectImpl<?>) {
                // configModel = AutoCopier.copy(config);
                configModel = (IConfig<?>) ((IObjectImpl<?>) newConfig).toModel();
                configModel.setName(newConfig.getName());
                configModel.setType(newConfig.getType());
                configModel.setLoaded(true);
                configModel.setModified(true);
            } else {
                configModel = newConfig;
            }

            view.addEntity(configModel);
        }
    }

    @Override
    public void notifyNewDirectory(String directoryName) {
        if (SalsaUtils.isDefined(directoryName)) {
            DirectoryModel directory = new DirectoryModel();
            directory.setName(directoryName);
            view.addEntity(directory);
        }
    }

    @Override
    public void selectScanResult() {
        selectScanResult(false);
    }

    @Override
    public void addConfigSelectionListener(IConfigSelectionListener listener) {
        if (listeners != null) {
            synchronized (listeners) {
                boolean canAdd = true;
                List<WeakReference<IConfigSelectionListener>> toRemove = new ArrayList<>();
                for (WeakReference<IConfigSelectionListener> ref : listeners) {
                    IConfigSelectionListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    } else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                }
                listeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    listeners.add(new WeakReference<>(listener));
                }
            }
        }
    }

    @Override
    public void removeConfigSelectionListener(IConfigSelectionListener listener) {
        if (listeners != null) {
            synchronized (listeners) {
                List<WeakReference<IConfigSelectionListener>> toRemove = new ArrayList<>();
                for (WeakReference<IConfigSelectionListener> ref : listeners) {
                    IConfigSelectionListener temp = ref.get();
                    if ((temp == null) || temp.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                listeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    @Override
    public void setView(ConfigTreeView view) {
        if (nodeController == null) {
            nodeController = new TreeNodeController();
        }
        if (configSource == null) {
            configSource = new ScanConfigSource(null);
        }
        if (!ObjectUtils.sameObject(view, this.view)) {
            if (this.view != null) {
                nodeController.removeAllReferencesTo(this.view.getConfigTree());
            }
            super.setView(view);
            if (this.view != null) {
                nodeController.addLink(configSource, this.view.getConfigTree());
                configSource.setData(null);
                view.expandAll(false);
            }
        }
    }

    @Override
    public void notifyEvent(EntityPropertyChangedEvent<IConfig<?>> event) {
        if (event != null) {
            List<EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?>> list = event.getPropertyChangeList();
            StringBuilder builder = new StringBuilder("-------------------");
            builder.append("\nproperty change:");
            builder.append("config: ").append(event.getEntity() == null ? "null" : event.getEntity().getName());
            for (EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?> change : list) {
                builder.append("\n- ").append(change.getPropertyName());
                builder.append(" - old='").append(change.getOldValue()).append("' ");
                builder.append(" - new='").append(change.getNewValue()).append("'");
            }
            builder.append("\n-------------------");
            LOGGER.trace(builder.toString());
        }
        if (view != null) {
            view.revalidate();
            view.repaint();
        }
    }

    /**
     * Selects the scan configuration that corresponds to the current scan
     * result, if possible
     * 
     * @param onlyIfNodeSelection
     *            If <code>true</code>, the configuration will be selected only
     *            if there is no path already selected in tree
     */
    private void selectScanResult(boolean onlyIfNodeSelection) {
        if (scanResultName != null) {
            String path = scanResultName;
            if (SalsaUtils.isDefined(path)) {
                if (path.indexOf('/') < 0) {
                    if (path.indexOf('.') > -1) {
                        path.replace('.', '/');
                    }
                }
                if (getView() != null) {
                    getView().selectPath(path, onlyIfNodeSelection);
                }
            }
        }
    }

    /**
     * Notifies {@link IConfigSelectionListener}s for configuration change
     */
    @Override
    public void notifyListeners(final IConfig<?> currentConfig) {
        if (listeners != null) {
            (new Thread("ConfigTreeController.notifyListeners") {
                @Override
                public void run() {
                    try {
                        for (WeakReference<IConfigSelectionListener> ref : listeners) {
                            IConfigSelectionListener listener = ref.get();
                            if (listener != null) {
                                listener.configSelectionChanged(currentConfig);
                            }
                        }
                    }
                    // Concurrent modification
                    catch (Exception e) {
                        LOGGER.warn("Cannot notify listener {}", e.getMessage());
                        LOGGER.debug("Stack trace", e);
                    }

                }
            }).start();
        }
    }

    /**
     * Returns the {@link List} of all unsaved configs
     * 
     * @return A {@link List}
     */
    public List<IConfig<?>> getUnsavedConfigs() {
        List<IConfig<?>> unsavedConfigList = new ArrayList<>();
        if (getView() != null) {
            addUnsavedConfigs(getView().getConfigTree().getRootNode(), unsavedConfigList);
        }
        return unsavedConfigList;
    }

    /**
     * Recursively adds all unsaved configs in a {@link List}, starting from a
     * given {@link ITreeNode}
     * 
     * @param node
     *            The {@link ITreeNode} from which to start
     * @param configList
     *            The {@link List}
     */
    private void addUnsavedConfigs(ITreeNode node, List<IConfig<?>> configList) {
        if (node != null) {
            if (node.getData() instanceof IConfig<?>) {
                IConfig<?> config = (IConfig<?>) node.getData();
                if (config.isModified()) {
                    configList.add(config);
                }
            } else {
                List<ITreeNode> children = node.getChildren();
                if (children != null) {
                    for (ITreeNode child : children) {
                        addUnsavedConfigs(child, configList);
                    }
                }
            }
        }
    }

    @Override
    public void nodeChanged(TreeNodeEvent event) {
        if ((event != null) && (event.getReason() == NodeEventReason.DATA)) {
            updateSelectedConfig();
        }
    }

    @Override
    public IConfig<?> getConfig() {
        return lastSelectedConfig;
    }

}

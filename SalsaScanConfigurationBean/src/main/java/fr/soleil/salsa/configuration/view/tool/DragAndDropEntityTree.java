package fr.soleil.salsa.configuration.view.tool;

import java.awt.Cursor;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.TransferHandler;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;

import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Tree;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.comete.swing.util.DefaultCometeTreeModel;
import fr.soleil.lib.project.swing.listener.TreeSelectionUpdater;
import fr.soleil.lib.project.swing.listener.TreeUiUpdater;
import fr.soleil.salsa.config.manager.ConfigManager;
import fr.soleil.salsa.data.util.TreeNodeUtils;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IEntity;
import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * A {@link Tree} that supports inner drag and drop and copy/paste operations, and that manages
 * {@link ITreeNode}s that contain {@link IEntity}s.
 */
public class DragAndDropEntityTree extends Tree
        implements TreeSelectionListener, ClipboardOwner, MouseListener, MouseMotionListener {

    private static final long serialVersionUID = 2484252271936093763L;

    public static final Logger LOGGER = LoggingUtil.getLogger(DragAndDropEntityTree.class);

    /**
     * Support for the drag and drop operations in the configurations tree. Indicates the kind of
     * data being moved around.
     */
    public static final DataFlavor TREE_NODE_FLAVOR = new DataFlavor(TransferableSelection.class,
            "Config Tree Selection");

    /**
     * The {@link Transferable} that contains the nodes to drag and drop
     */
    private TransferableSelection dndSelection;

    /**
     * The Clipboard used to copy/paste nodes
     */
    private final Clipboard clipboard;

    /**
     * The {@link JPopupMenu} used by this tree
     */
    private JPopupMenu popupMenu;

    /**
     * The {@link AbstractAction} to copy nodes
     */
    private AbstractAction copyAction;

    /**
     * The {@link AbstractAction} to cut nodes
     */
    private AbstractAction cutAction;

    /**
     * The {@link AbstractAction} to paste nodes
     */
    private AbstractAction pasteAction;

    /**
     * The {@link AbstractAction} to refresh node structure
     */
    private AbstractAction refreshAction;

    /**
     * The {@link AbstractAction} to force model to repaint tree
     */
    private AbstractAction repainAction;

    /**
     * The {@link ConfigManager} that is able to load {@link IConfig}s
     */
    private ConfigManager configManager;

    private final TreeModelListener viewUpdater;

    /**
     * Constructor.
     */
    public DragAndDropEntityTree() {
        this(new DefaultCometeTreeModel(null));
    }

    /**
     * Constructor.
     * 
     * @param newModel The {@link CometeTreeModel} to use
     */
    public DragAndDropEntityTree(CometeTreeModel newModel) {
        super(newModel);
        updateUI();
        clipboard = new Clipboard(getClass().getName());
        viewUpdater = new TreeUiUpdater(this, true, true);
        construct();
    }

    @Override
    public void setModel(TreeModel newModel) {
        if (getModel() != null) {
            getModel().removeTreeModelListener(viewUpdater);
        }
        super.setModel(newModel);
        if (getModel() != null) {
            getModel().addTreeModelListener(viewUpdater);
        }
    }

    /**
     * Common to all constructors.
     */
    private void construct() {
        setDragEnabled(true);
        configManager = new ConfigManager();
        copyAction = new AbstractAction("Copy") {
            private static final long serialVersionUID = 9217466802582309262L;

            @Override
            public void actionPerformed(ActionEvent e) {
                copyOrCutSelectedNodes(true);
            }
        };
        copyAction.putValue(AbstractAction.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        cutAction = new AbstractAction("Cut") {
            private static final long serialVersionUID = 9217466802582309262L;

            @Override
            public void actionPerformed(ActionEvent e) {
                copyOrCutSelectedNodes(false);
            }
        };
        cutAction.putValue(AbstractAction.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        cutAction.setEnabled(false);
        pasteAction = new AbstractAction("Paste") {
            private static final long serialVersionUID = -1506462121489580377L;

            @Override
            public void actionPerformed(ActionEvent e) {
                pasteNodes();
            }
        };
        pasteAction.putValue(AbstractAction.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
        pasteAction.setEnabled(false);
        refreshAction = new AbstractAction("Refresh Structure") {
            private static final long serialVersionUID = -3560264849417287270L;

            @Override
            public void actionPerformed(ActionEvent e) {
                refreshNodes(true);
            }
        };
        refreshAction.putValue(AbstractAction.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        repainAction = new AbstractAction("Revalidate tree") {
            private static final long serialVersionUID = 9101170935061795927L;

            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        };
        repainAction.putValue(AbstractAction.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke(KeyEvent.VK_F5, ActionEvent.ALT_MASK));
        popupMenu = new JPopupMenu();
        popupMenu.add(copyAction);
        popupMenu.add(cutAction);
        popupMenu.add(pasteAction);
        popupMenu.add(refreshAction);
        addMouseListener(new TreeSelectionUpdater(true));
        addMouseListener(this);
        addMouseMotionListener(this);
        setDragEnabled(true);
        setTransferHandler(new TreeTransfertHandler());
    }

    /**
     * Returns the {@link JPopupMenu} used by this tree
     * 
     * @return A {@link JPopupMenu}
     */
    public JPopupMenu getPopupMenu() {
        return popupMenu;
    }

    /**
     * Returns the {@link AbstractAction} used to copy nodes
     * 
     * @return An {@link AbstractAction}
     */
    public AbstractAction getCopyAction() {
        return copyAction;
    }

    /**
     * Returns the {@link AbstractAction} used to cut nodes
     * 
     * @return An {@link AbstractAction}
     */
    public AbstractAction getCutAction() {
        return cutAction;
    }

    /**
     * Returns the {@link AbstractAction} used to paste nodes
     * 
     * @return An {@link AbstractAction}
     */
    public AbstractAction getPasteAction() {
        return pasteAction;
    }

    /**
     * Returns the {@link AbstractAction} used to refresh nodes
     * 
     * @return An {@link AbstractAction}
     */
    public AbstractAction getRefreshAction() {
        return refreshAction;
    }

    /**
     * Returns the {@link AbstractAction} used to repaint tree
     * 
     * @return An {@link AbstractAction}
     */
    public AbstractAction getRepainAction() {
        return repainAction;
    }

    /**
     * Displays the popup menu if desired by user
     * 
     * @param e The {@link MouseEvent} that corresponds to the user mouse action
     */
    private void showPopupMenu(MouseEvent e) {
        if ((e != null) && (e.isPopupTrigger()) && (popupMenu != null) && (popupMenu.getComponentCount() > 0)) {
            if (!popupMenu.isVisible()) {
                popupMenu.pack();
            }
            popupMenu.show(this, e.getX(), e.getY());
        }
    }

    /**
     * Generates a {@link TransferableSelection} that corresponds to selected nodes
     * 
     * @param forCopy Whether the selection is used for a "copy" action
     * 
     * @return A {@link TransferableSelection}. <code>null</code> if no node is selected
     */
    private TransferableSelection generateTransferableSelection(boolean forCopy) {
        TransferableSelection result;
        List<DefaultMutableTreeNode> selectionList = getSelectionSortedByPath();
        if (SalsaUtils.isFulfilled(selectionList)) {
            result = new TransferableSelection(selectionList, forCopy);
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Returns the selected {@link DefaultMutableTreeNode}, sorted by their path in tree
     * 
     * @return A {@link DefaultMutableTreeNode} {@link List}
     */
    private List<DefaultMutableTreeNode> getSelectionSortedByPath() {
        List<DefaultMutableTreeNode> selectionList = new ArrayList<DefaultMutableTreeNode>();
        int nbRows = getRowCount();
        TreePath treePath;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            if (isRowSelected(rowIndex)) {
                treePath = getPathForRow(rowIndex);
                selectionList.add((DefaultMutableTreeNode) treePath.getLastPathComponent());
            }
        }
        return selectionList;
    }

    /**
     * Extracts a {@link List} of {@link ITreeNode} from another {@link List}, that is supposed to
     * contain some {@link DefaultMutableTreeNode}s
     * 
     * @param selection The {@link List} from which to extract {@link ITreeNode}s.
     * @return An {@link ITreeNode} {@link List}
     */
    private List<ITreeNode> extractTreeNodes(List<?> selection) {
        List<ITreeNode> nodes = new ArrayList<ITreeNode>();
        if (getModel() != null) {
            for (Object child : selection) {
                if (child instanceof DefaultMutableTreeNode) {
                    ITreeNode node = getModel().getTreeNode((DefaultMutableTreeNode) child);
                    if (node.getData() instanceof IEntity) {
                        IEntity entity = (IEntity) node.getData();
                        if (entity.getDirectory() != null) {
                            nodes.add(node);
                        }
                    }
                }
            }
        }
        return nodes;
    }

    /**
     * Returns the selected {@link ITreeNode}, sorted by their path in tree
     * 
     * @return An {@link ITreeNode} {@link List}
     */
    public List<ITreeNode> getSelectedNodesSortedByPath() {
        return extractTreeNodes(getSelectionSortedByPath());
    }

    /**
     * Drops some nodes in another one. This method filters the nodes to extract the corresponding
     * {@link ITreeNode}s, calls the method {@link #dropNodes(List, ITreeNode)}, and then expands
     * the {@link TreePath} that corresponds to the target node
     * 
     * @param destinationPath The {@link TreePath} that represents the destination node
     * @param nodesContainer The {@link Transferable} that contains the nodes to drop
     * @see #dropNodes(List, ITreeNode)
     */
    private void dropSelection(TreePath destinationPath, Transferable nodesContainer) {
        if (destinationPath != null) {
            boolean isCopy = false;
            if (nodesContainer instanceof TransferableSelection) {
                isCopy = ((TransferableSelection) nodesContainer).isCopy();
            }
            DefaultMutableTreeNode target = (DefaultMutableTreeNode) destinationPath.getLastPathComponent();
            List<?> droppedSelection;
            try {
                droppedSelection = castToList(nodesContainer);
                List<ITreeNode> nodes = extractTreeNodes(droppedSelection);
                TreeNodeUtils.filterMultipleSelection(nodes);
                dropNodes(nodes, getModel().getTreeNode(target), isCopy);
                expandAll(target.isLeaf() && (!target.isRoot()) ? destinationPath.getParentPath() : destinationPath,
                        true);
            } catch (ConversionException e) {
                LOGGER.error("Cannot drop selection {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
    }

    /**
     * <big><u>EXPERT USAGE ONLY</u></big>
     * <p>
     * Adds some new {@link ITreeNode}s in selected {@link ITreeNode}, if it contains an
     * {@link IDirectory}, or its parent {@link ITreeNode}
     * </p>
     * 
     * @param nodes The {@link ITreeNode}s to add
     */
    public void addNodes(ITreeNode... nodes) {
        if ((nodes != null) && (getModel() != null)) {
            DefaultMutableTreeNode target;
            TreePath selected = getSelectionPath();
            if (selected == null) {
                target = getModel().getRoot();
            } else {
                target = (DefaultMutableTreeNode) selected.getLastPathComponent();
            }
            dropNodes(Arrays.asList(nodes), getModel().getTreeNode(target), false);

        }
    }

    /**
     * Drops some {@link ITreeNode}s or their copies in another one
     * 
     * @param nodes The {@link ITreeNode}s to drop
     * @param target The target {@link ITreeNode}
     * @param Whether nodes should be duplicated instead of dropped
     */
    private void dropNodes(List<ITreeNode> nodes, ITreeNode target, boolean duplicate) {
        if ((target != null) && SalsaUtils.isFulfilled(nodes)) {
            for (ITreeNode node : nodes) {
                TreeNodeUtils.loadConfig(node, configManager, true);
            }
            List<ITreeNode> toDrop;
            if (duplicate) {
                toDrop = new ArrayList<ITreeNode>();
                for (ITreeNode node : nodes) {
                    toDrop.add(TreeNodeUtils.getDuplicata(node, true));
                }
            } else {
                toDrop = nodes;
            }
            ITreeNode realTarget = TreeNodeUtils.findDirectoryNode(target);
            if (realTarget != null) {
                realTarget.addNodes(toDrop.toArray(new ITreeNode[toDrop.size()]));
                IDirectory directory = (IDirectory) realTarget.getData();
                for (ITreeNode node : toDrop) {
                    if (node.getData() instanceof IEntity) {
                        ((IEntity) node.getData()).setDirectory(directory);
                    }
                }
            }
        }
    }

    /**
     * Cast a {@link Transferable} data to a list.
     * 
     * @param transferable The {@link Transferable} that contains the data to cast to {@link List}
     * @return A {@link List}
     * @throws ConversionException If the transferable data could not be cast to a {@link List}
     */
    private static List<?> castToList(Transferable transferable) throws ConversionException {
        try {
            return (List<?>) transferable.getTransferData(TREE_NODE_FLAVOR);
        } catch (Exception e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

    /**
     * Copies or cuts selected nodes to clipboard
     * 
     * @param copy <code>true</code> to copy, <code>false</code> to cut
     */
    public void copyOrCutSelectedNodes(boolean copy) {
        Transferable transferable = generateTransferableSelection(copy);
        clipboard.setContents(transferable, this);
        pasteAction.setEnabled(transferable != null);
    }

    /**
     * Pastes some nodes from clipboard in a parent node
     */
    public void pasteNodes() {
        Transferable transferable = clipboard.getContents(this);
        dropSelection(getSelectionPath(), transferable);
        clipboard.setContents(null, this);
        pasteAction.setEnabled(false);
    }

    public void refreshNodes(boolean askConfirmation) {
        int ok = JOptionPane.YES_OPTION;
        if (askConfirmation) {
            ok = JOptionPane.showConfirmDialog(this,
                    "This will reload the whole tree structure, and you will loose all your modifications.\nContinue?",
                    "Confirm Refresh", JOptionPane.YES_NO_OPTION);
        }
        if (ok == JOptionPane.YES_OPTION) {
            targetDelegate.warnMediators(new NodeInformation(this, null));
        }
    }

    /**
     * Notifies mediators for the need to save some nodes
     */
    public void notifySaveSelectedNodes() {
        List<ITreeNode> nodes = getSelectedNodesSortedByPath();
        TreeNodeUtils.filterMultipleSelection(nodes);
        if (SalsaUtils.isFulfilled(nodes)) {
            for (ITreeNode node : nodes) {
                targetDelegate.warnMediators(new NodeInformation(this, node));
            }
            nodes.clear();
        }
        repaint();
    }

    /**
     * Notifies mediators for the need to save all nodes
     */
    public void notifySaveAll() {
        ITreeNode rootNode = getRootNode();
        if (rootNode != null) {
            targetDelegate.warnMediators(new NodeInformation(this, rootNode));
        }
        repaint();
    }

    // Method from TreeSelectionListener.

    @Override
    public void valueChanged(final TreeSelectionEvent evt) {
        TreePath selectedPath = getSelectionPath();
        if (selectedPath != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();

            if (getModel() != null) {
                TreeNodeUtils.loadConfig(getModel().getTreeNode(node), configManager, false);
            }
        }
        TreePath[] selection = getSelectionPaths();
        if ((selection != null) && (getModel() != null)) {
            for (TreePath tmpTreePath : selection) {
                DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) tmpTreePath.getLastPathComponent();
                TreeNodeUtils.loadConfig(getModel().getTreeNode(tmpNode), configManager, false);
            }
        }

        cutAction.setEnabled((selection != null) && (selection.length > 0));
        dndSelection = generateTransferableSelection(false);
        // For fixing the Mantis bug 22949
        (new Thread("DragAndDropEntityTree.superValueChanged") {
            @Override
            public void run() {
                superValueChanged(evt);
            };
        }).start();
    }

    private void superValueChanged(TreeSelectionEvent evt) {
        super.valueChanged(evt);
    }

    // Method from ClipboardOwner

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        // Nothing to do, as this tree manages its own Clipboard
    }

    // Methods from MouseListener

    @Override
    public void mouseClicked(MouseEvent e) {
        showPopupMenu(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        showPopupMenu(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        showPopupMenu(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    // Methods from MouseMotionListener

    @Override
    public void mouseDragged(MouseEvent e) {
        // update lead path to indicate where nodes will be dropped
        if ((e != null) && (dndSelection != null)) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TreePath path = getPathForLocation(e.getX(), e.getY());
            if (path != null) {
                setLeadSelectionPath(path);
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // Nothing to do
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * A multiple selection that can be transfered by drag and drop.
     */
    private static class TransferableSelection implements Transferable {

        private final boolean copy;

        /**
         * Support for the drag and drop operations in the configurations tree. Indicates the kinds
         * of data that can be moved around.
         */
        private static DataFlavor flavorsArray[] = { TREE_NODE_FLAVOR };

        /**
         * The selected nodes.
         */
        private final List<?> selectedNodesList;

        /**
         * Constructor.
         * 
         * @param selectedNodesList The {@link List} of selected {@link DefaultMutableTreeNode}s
         */
        public TransferableSelection(List<DefaultMutableTreeNode> selectedNodesList, boolean copy) {
            super();
            this.selectedNodesList = selectedNodesList;
            this.copy = copy;
        }

        public boolean isCopy() {
            return copy;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor df) {
            return TREE_NODE_FLAVOR.equals(df);
        }

        @Override
        public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
            if (TREE_NODE_FLAVOR.equals(df)) {
                return selectedNodesList;
            } else {
                throw new UnsupportedFlavorException(df);
            }
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return flavorsArray;
        }
    }

    /**
     * A {@link TransferHandler} that manages drag and drop for {@link DragAndDropEntityTree}
     */
    private class TreeTransfertHandler extends TransferHandler {

        private static final long serialVersionUID = 3864847306435554856L;

        /**
         * Creates a new instance.
         */
        public TreeTransfertHandler() {
            super();
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            return generateTransferableSelection(false);
        }

        @Override
        public int getSourceActions(JComponent component) {
            int action = NONE;
            if (component instanceof DragAndDropEntityTree) {
                action = COPY_OR_MOVE;
            }
            return action;
        }

        @Override
        public boolean canImport(JComponent component, DataFlavor[] flavors) {
            boolean canImport = false;
            if ((component instanceof DragAndDropEntityTree) && (flavors != null)) {
                for (DataFlavor f : flavors) {
                    // We can only import TREE_NODE_FLAVOR so far.
                    // We may add additional data flavors here in the future.
                    if (TREE_NODE_FLAVOR.equals(f)) {
                        canImport = true;
                        break;
                    }
                }
            }
            return canImport;
        }

        @Override
        public boolean importData(JComponent component, Transferable t) {
            boolean imported = false;
            if (canImport(component, t.getTransferDataFlavors())) {
                dropSelection(getSelectionPath(), t);
                imported = true;
            }
            return imported;
        }

        @Override
        protected void exportDone(JComponent source, Transferable data, int action) {
            dndSelection = null;
        }
    }

}
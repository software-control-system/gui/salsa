package fr.soleil.salsa.configuration.view.tool;

import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.target.information.TargetInformation;

/**
 * A {@link TargetInformation} to warn for some changes in an {@link ITreeNode}s managed by an
 * {@link ITree}
 * 
 * @author girardot
 */
public class NodeInformation extends TargetInformation<ITreeNode> {

    public NodeInformation(ITree target, ITreeNode info) {
        super(target, info);
    }

    @Override
    public ITree getConcernedTarget() {
        return (ITree) super.getConcernedTarget();
    }

}

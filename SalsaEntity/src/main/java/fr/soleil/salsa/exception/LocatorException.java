package fr.soleil.salsa.exception;

/**
 * Exception levee lorsque l'instancation d'une interface avec le locator echoue<br>
 * 
 * @author Francois Denhez
 */
public class LocatorException extends SalsaException {

    private static final long serialVersionUID = 6658294100090732781L;

    public LocatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public LocatorException(String message) {
        super(message);
    }

    public LocatorException(Throwable cause) {
        super(cause);
    }

}

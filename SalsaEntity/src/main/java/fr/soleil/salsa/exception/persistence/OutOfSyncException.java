package fr.soleil.salsa.exception.persistence;

/**
 * Base class for Exception relative to out of sync exception for scan or directories
 * 
 * @author Alike
 * 
 */
public abstract class OutOfSyncException extends PersistenceException {

    private static final long serialVersionUID = 8411595464218018431L;

    public OutOfSyncException() {
        super();
    }

    public OutOfSyncException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutOfSyncException(String message) {
        super(message);
    }

    public OutOfSyncException(Throwable cause) {
        super(cause);
    }

}

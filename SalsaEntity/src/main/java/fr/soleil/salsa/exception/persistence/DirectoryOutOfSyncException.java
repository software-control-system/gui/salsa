package fr.soleil.salsa.exception.persistence;

/**
 * Class relative to Exception which can happen when directory data are not up to date, or when a
 * config or subdirectory have been created/modified/deleted in this directory.
 * 
 * @author Alike
 */
public class DirectoryOutOfSyncException extends OutOfSyncException {

    private static final long serialVersionUID = 9087418000453154908L;

    @Override
    public String getMessage() {
        // Default message for this kind of Exception.
        // TODO : It would be nice to externalize
        String message = "Operation failed.\nThis directory is out of sync, please refresh. All your modifications will be lost (You can copy your configuration to save it to another file).";
        return message;
    }

}

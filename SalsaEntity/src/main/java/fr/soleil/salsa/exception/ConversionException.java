package fr.soleil.salsa.exception;

public class ConversionException extends SalsaException {

    private static final long serialVersionUID = 5673016666230269113L;

    public ConversionException() {
        super();
    }

    public ConversionException(String message) {
        super(message);
    }

    public ConversionException(Throwable cause) {
        super(cause);
    }

    public ConversionException(String message, Throwable cause) {
        super(message, cause);
    }

}

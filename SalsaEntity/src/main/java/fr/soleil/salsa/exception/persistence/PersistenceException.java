package fr.soleil.salsa.exception.persistence;

import fr.soleil.salsa.exception.SalsaException;

public class PersistenceException extends SalsaException {

    private static final long serialVersionUID = 7588235961152092473L;

    public PersistenceException() {
        super();
    }

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }

}

package fr.soleil.salsa.exception;


/**
 * Exception launched when a problem with the devices occurs.
 */
public class SalsaDeviceException extends SalsaException {

    private static final long serialVersionUID = 8802523366427567368L;

    /**
     * Constructor.
     */
    public SalsaDeviceException() {
    }

    /**
     * Constructor.
     * 
     * @param message
     */
    public SalsaDeviceException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param cause
     */
    public SalsaDeviceException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     * 
     * @param message
     * @param cause
     */
    public SalsaDeviceException(String message, Throwable cause) {
        super(message, cause);
    }

}

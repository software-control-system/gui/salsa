package fr.soleil.salsa.exception.persistence;

import java.sql.SQLException;

public class PersistenceSqlException extends PersistenceException {

    private static final long serialVersionUID = 6715056693332533612L;

    /**
     * The nested exception.
     */
    private SQLException innerException;

    /**
     * Gets the nested exception.
     * 
     * @return
     */
    public SQLException getInnerException() {
        return innerException;
    }

    /**
     * Sets the nested exception.
     * 
     * @param innerException
     */
    public void setInnerException(SQLException innerException) {
        this.innerException = innerException;
    }

}

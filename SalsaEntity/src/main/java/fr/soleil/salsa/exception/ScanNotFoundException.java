package fr.soleil.salsa.exception;

public class ScanNotFoundException extends SalsaException {

    private static final long serialVersionUID = 7363404953849093880L;

    public ScanNotFoundException() {
        super();
    }

    public ScanNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScanNotFoundException(String message) {
        super(message);
    }

    public ScanNotFoundException(Throwable cause) {
        super(cause);
    }

}

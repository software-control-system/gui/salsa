package fr.soleil.salsa.exception.persistence;

/**
 * Class relative to Exception which can happen when a Scan is out of sync. This happen when the
 * scan have been modified/deleted.
 * 
 * @author Alike
 * 
 */
public class ScanConfigOutOfSyncException extends OutOfSyncException {

    private static final long serialVersionUID = 7352655078983043286L;

    private final static String DEFAULT_MESSAGE = "Operation failed.\nThis configuration is out of sync, please refresh. All your modifications will be lost (You can copy your configuration to save it to another file).";

    public ScanConfigOutOfSyncException() {
        this(DEFAULT_MESSAGE);
    }

    public ScanConfigOutOfSyncException(String message) {
        super(message);
    }

    public ScanConfigOutOfSyncException(Throwable cause) {
        this(DEFAULT_MESSAGE, cause);
    }

    public ScanConfigOutOfSyncException(String message, Throwable cause) {
        super(message, cause);
    }

}

package fr.soleil.salsa.exception;

public class SalsaLoggingException extends SalsaException {

    private static final long serialVersionUID = 7722552857567704784L;

    public SalsaLoggingException() {
        super();
    }

    public SalsaLoggingException(String message) {
        super(message);
    }

    public SalsaLoggingException(Throwable cause) {
        super(cause);
    }

    public SalsaLoggingException(String message, Throwable cause) {
        super(message, cause);
    }

}

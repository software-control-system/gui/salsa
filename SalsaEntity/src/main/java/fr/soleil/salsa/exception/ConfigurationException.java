package fr.soleil.salsa.exception;


/**
 * Exception sent in case of problem during the initialization.
 * 
 * @author Francois Denhez
 * 
 */
public class ConfigurationException extends SalsaException {

    private static final long serialVersionUID = -1143443332473235313L;

    /**
     * Constructor.
     * 
     * @param message
     * @param cause
     */
    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     * 
     * @param message
     */
    public ConfigurationException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param cause
     */
    public ConfigurationException(Throwable cause) {
        super(cause);
    }
}

package fr.soleil.salsa.exception;

import fr.soleil.salsa.entity.IConfig;

/**
 * An {@link Exception} that occurs when there is a problem with an {@link IConfig}
 * 
 * @author girardot
 * 
 */
public class SalsaScanConfigurationException extends SalsaException {

    private static final long serialVersionUID = 285458079577566637L;

    public SalsaScanConfigurationException() {
        super();
    }

    public SalsaScanConfigurationException(String message) {
        super(message);
    }

    public SalsaScanConfigurationException(Throwable cause) {
        super(cause);
    }

    public SalsaScanConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

}

package fr.soleil.salsa.exception;

/**
 * An exception that can happen with trajectory calculation
 * 
 * @author girardot
 */
public class SalsaTrajectoryException extends SalsaException {

    private static final long serialVersionUID = -8838139255884637955L;

    public SalsaTrajectoryException() {
        super();
    }

    public SalsaTrajectoryException(String message) {
        super(message);
    }

    public SalsaTrajectoryException(Throwable cause) {
        super(cause);
    }

    public SalsaTrajectoryException(String message, Throwable cause) {
        super(message, cause);
    }

}

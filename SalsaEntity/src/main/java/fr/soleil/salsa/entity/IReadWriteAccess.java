package fr.soleil.salsa.entity;

import java.io.Serializable;

/**
 * @author Alike
 *
 */
public interface IReadWriteAccess extends Serializable {

    /**
     * true if readable.
     * @return
     */
    boolean isReadable();

    /**
     * Sets true if readable.
     * @param readable
     */
    void setReadable(boolean readable);

    /**
     * True if writable.
     * @return
     */
    boolean isWritable();

    /**
     * Sets true if writable.
     * @param writable
     */
    void setWritable(boolean writable);

}

package fr.soleil.salsa.entity.impl.scan2d;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.event.Range2DYModel;
import fr.soleil.salsa.entity.scan2D.IRange2DY;

/**
 * @author Alike
 * 
 */
public class Range2DYImpl extends Range2DImpl implements IRange2DY {

    private static final long serialVersionUID = 2257908245172873639L;

    @Override
    protected IRange initModel() {
        return new Range2DYModel(this);
    }
}

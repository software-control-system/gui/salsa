package fr.soleil.salsa.entity.impl.scanhcs;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.event.RangeHCSModel;
import fr.soleil.salsa.entity.impl.RangeImpl;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;

public class RangeHCSImpl extends RangeImpl implements IRangeHCS {

    private static final long serialVersionUID = 4641828847767768625L;
    /**
     * The integration time.
     */
    private double[] integrationTime;

    @Override
    protected IRange initModel() {
        return new RangeHCSModel(this);
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IRange1D#getIntegrationTime()
     */
    @Override
    public double[] getIntegrationTime() {
        return integrationTime;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IRange1D#setIntegrationTime(double)
     */
    @Override
    public void setIntegrationTime(double[] integrationTime) {
        this.integrationTime = integrationTime;
    }

}

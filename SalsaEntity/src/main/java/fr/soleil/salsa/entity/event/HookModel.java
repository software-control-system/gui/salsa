package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.impl.HookImpl;
import fr.soleil.salsa.entity.impl.Stage;

public class HookModel extends AEventHandlingModelDecorator<IHook> implements IHook {

    private static final long serialVersionUID = 5191822778837195271L;

    /**
     * @param baseBean
     */
    public HookModel(IHook baseBean) {
        super(baseBean);
    }

    /**
     * 
     */
    public HookModel() {
        super(new HookImpl());
    }

    @Override
    public List<IHookCommand> getCommandsList() {
        return baseBean.getCommandsList();
    }

    @Override
    public Integer getId() {
        return baseBean.getId();
    }

    @Override
    public Stage getStage() {
        return baseBean.getStage();
    }

    @Override
    public void setCommandsList(List<IHookCommand> commandsList) {
        List<IHookCommand> oldValue = this.baseBean.getCommandsList();
        this.baseBean.setCommandsList(commandsList);
        this.firePropertyChange("commandsList", oldValue, commandsList);
    }

    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    @Override
    public void setStage(Stage stage) {
        Stage oldValue = this.baseBean.getStage();
        this.baseBean.setStage(stage);
        this.firePropertyChange("stage", oldValue, stage);
    }

    @Override
    public IHook toModel() {
        return this;
    }

}

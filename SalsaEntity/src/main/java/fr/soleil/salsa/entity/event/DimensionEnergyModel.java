package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.impl.scanenergy.DimensionEnergyImpl;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;

public class DimensionEnergyModel extends DimensionModel<IDimensionEnergy> implements
        IDimensionEnergy {

    private static final long serialVersionUID = -8488770575191085299L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param dimensionEnergy the decorated base bean.
     */
    public DimensionEnergyModel(IDimensionEnergy dimensionEnergy) {
        super(dimensionEnergy);
    }

    /**
     * Default constructor, that creates a new instance of Dimension1DImpl and wraps it.
     */
    public DimensionEnergyModel() {
        this(new DimensionEnergyImpl());
    }

    @Override
    public List<IRangeEnergy> getRangesEnergyList() {
        return this.baseBean.getRangesEnergyList();
    }

    @Override
    public List<ITrajectoryEnergy> getTrajectoriesEnergyList() {
        return this.baseBean.getTrajectoriesEnergyList();
    }

    @Override
    public void setRangesEnergyList(List<IRangeEnergy> rangeEnergy) {
        List<IRangeEnergy> oldValue = this.baseBean.getRangesEnergyList();
        this.baseBean.setRangesEnergyList(rangeEnergy);
        this.firePropertyChange("rangeEnergy", oldValue, rangeEnergy);
    }

    @Override
    public void setTrajectoriesEnergyList(List<ITrajectoryEnergy> trajectoriesEnergy) {
        List<ITrajectoryEnergy> oldValue = this.baseBean.getTrajectoriesEnergyList();
        this.baseBean.setTrajectoriesEnergyList(trajectoriesEnergy);
        this.firePropertyChange("rangeEnergy", oldValue, trajectoriesEnergy);
    }

}

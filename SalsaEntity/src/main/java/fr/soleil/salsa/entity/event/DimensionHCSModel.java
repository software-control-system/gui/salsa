package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.impl.scanhcs.DimensionHCSImpl;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;

public class DimensionHCSModel extends DimensionModel<IDimensionHCS> implements IDimensionHCS {

    private static final long serialVersionUID = -7053594610239499477L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param dimensionHCS the decorated base bean.
     */
    public DimensionHCSModel(IDimensionHCS dimensionHCS) {
        super(dimensionHCS);
    }

    /**
     * Default constructor, that creates a new instance of DimensionHCSImpl and wraps it.
     */
    public DimensionHCSModel() {
        this(new DimensionHCSImpl());
    }

    /**
     * 
     */
    public List<IRangeHCS> getRangesXList() {
        return baseBean.getRangesXList();
    }

    /**
     * 
     */
    public void setRangesXList(List<IRangeHCS> rangesX) {
        List<IRangeHCS> oldValue = this.baseBean.getRangesXList();
        this.baseBean.setRangesXList(rangesX);
        this.firePropertyChange("rangesX", oldValue, rangesX);
    }

    /**
     * 
     */
    public List<ITrajectoryHCS> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    /**
     * 
     */
    public void setTrajectoriesList(List<ITrajectoryHCS> trajectories) {
        List<ITrajectoryHCS> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectories);
        this.firePropertyChange("trajectories", oldValue, trajectories);
    }

}

package fr.soleil.salsa.entity.impl.flyscan;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.ConfigFlyScanModel;
import fr.soleil.salsa.entity.flyscan.IConfigFlyScan;
import fr.soleil.salsa.entity.impl.ConfigImpl;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.impl.RangeImpl;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Class relative to Config 1d.
 *
 * @author Saintin
 *
 */
public class ConfigFlyScanImpl extends ConfigImpl<IDimension> implements IConfigFlyScan {

    private static final long serialVersionUID = 2193084467982721027L;

    /**
     * Constructor.
     */
    public ConfigFlyScanImpl() {
        super();
        setType(ScanType.SCAN_FLY);
    }

    @Override
    protected IConfig<IDimension> initModel() {
        ConfigFlyScanModel configModel = new ConfigFlyScanModel(this);
        if (SalsaUtils.isFulfilled(dimensionList)) {
            List<IDimension> dimensionListModel = new ArrayList<IDimension>();
            for (IDimension dimension : dimensionList) {
                if (dimension != null && dimension instanceof IObjectImpl<?>) {
                    IDimension dimensionModel = (IDimension) ((IObjectImpl<?>) dimension).toModel();
                    List<IActuator> actuatorList = dimension.getActuatorsList();
                    List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
                    dimensionModel.setActuatorsList(actuatorListModel);
                    dimensionListModel.add(dimensionModel);
                }

            }
            dimensionList.clear();
            dimensionList.addAll(dimensionListModel);
        }
        return configModel;
    }

    @Override
    public List<IActuator> getActuatorList(final int dimension) {
        ArrayList<IActuator> allActuators = null;
        if (dimensionList != null) {
            allActuators = new ArrayList<IActuator>();
            for (IDimension idimension : dimensionList) {
                allActuators.addAll(idimension.getActuatorsList());
            }
        }
        return allActuators;
    }

    @Override
    public List<IDimension> getDimensionList() {
        return dimensionList;
    }

    @Override
    public IDimension addDimension() {
        if (dimensionList == null) {
            dimensionList = new ArrayList<IDimension>();
        }
        IDimension dimension = new DimensionImpl();
        dimensionList.add(dimension);
        return dimension;
    }

    @Override
    public void removeDimension() {
        if (SalsaUtils.isFulfilled(dimensionList)) {
            dimensionList.remove(dimensionList.size() - 1);
        }
    }

    @Override
    public List<IActuator> getActuatorList(final IDimension dimension) {
        List<IActuator> actuatorList = null;
        if (dimension != null) {
            actuatorList = dimension.getActuatorsList();
        }
        return actuatorList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addActuator(final IDimension dimension, final IActuator actuator) {
        if (dimension != null && actuator != null) {
            List<IActuator> actuatorList = getActuatorList(dimension);
            if (actuatorList == null) {
                actuatorList = new ArrayList<IActuator>();
            }
            actuatorList.add(actuator);

            List<? extends IRange> rangeList = dimension.getRangeList();
            if (rangeList == null) {
                rangeList = new ArrayList<IRange>();
            }

            if (rangeList.isEmpty()) {
                // Have at list one range
                IRange range = new RangeImpl();
                ((List<IRange>) rangeList).add(range);
                dimension.setRangeList(rangeList);
            }

            // Fly Scan can have only one range
            IRange range = rangeList.get(0);
            List<ITrajectory> trajectoryList = range.getTrajectoriesList();
            if (trajectoryList == null) {
                trajectoryList = new ArrayList<ITrajectory>();
            }

            int actuatorSize = actuatorList.size();
            int trajectorySize = trajectoryList.size();
            if (trajectorySize < actuatorSize) {
                ITrajectory trajectory = null;
                IActuator tmpActuator = null;
                for (int i = trajectorySize; i < actuatorSize; i++) {
                    tmpActuator = actuatorList.get(i);
                    trajectory = new TrajectoryImpl();
                    trajectory.setName(tmpActuator.getName());
                    trajectoryList.add(trajectory);
                }
                range.setTrajectoriesList(trajectoryList);
            }
        }
    }

    @Override
    public void removeActuator(final IDimension dimension, final IActuator actuator) {
        if (dimension != null && actuator != null) {
            List<IActuator> actuatorList = getActuatorList(dimension);
            if (actuatorList != null && actuatorList.contains(actuator)) {
                int indexActuator = actuatorList.indexOf(actuator);
                List<? extends IRange> rangeList = dimension.getRangeList();
                if (rangeList != null) {
                    List<ITrajectory> trajectoryList = null;
                    for (IRange range : rangeList) {
                        trajectoryList = range.getTrajectoriesList();
                        if (trajectoryList != null && indexActuator < trajectoryList.size()) {
                            trajectoryList.remove(indexActuator);
                        }
                    }
                }
                actuatorList.remove(indexActuator);
            }
        }
    }

    @Override
    public void setTrajectory(final IActuator actuator, final ITrajectory trajectory) {
        if (actuator != null && trajectory != null) {
            if (dimensionList != null) {
                List<IActuator> actuatorList = null;
                for (IDimension dimension : dimensionList) {
                    actuatorList = dimension.getActuatorsList();
                    if (actuatorList != null && actuatorList.contains(actuator)) {
                        int indexActuator = actuatorList.indexOf(actuator);
                        List<? extends IRange> rangeList = dimension.getRangeList();
                        if (rangeList != null) {
                            List<ITrajectory> trajectoryList = null;
                            for (IRange range : rangeList) {
                                trajectoryList = range.getTrajectoriesList();
                                if (trajectoryList != null && indexActuator < trajectoryList.size()) {
                                    trajectoryList.set(indexActuator, trajectory);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public ITrajectory getTrajectory(final IActuator actuator) {
        if (actuator != null) {
            if (dimensionList != null) {
                List<IActuator> actuatorList = null;
                for (IDimension dimension : dimensionList) {
                    actuatorList = dimension.getActuatorsList();
                    if (actuatorList != null && actuatorList.contains(actuator)) {
                        int indexActuator = actuatorList.indexOf(actuator);
                        List<? extends IRange> rangeList = dimension.getRangeList();
                        if (rangeList != null) {
                            List<ITrajectory> trajectoryList = null;
                            for (IRange range : rangeList) {
                                trajectoryList = range.getTrajectoriesList();
                                if (trajectoryList != null && indexActuator < trajectoryList.size()) {
                                    return trajectoryList.get(indexActuator);
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void setActuatorName(final IActuator actuator, final String name) {
        if (actuator != null && name != null) {
            actuator.setName(name);
        }

    }

    @Override
    public void setActuatorEnabled(final IActuator actuator, final boolean enabled) {
        if (actuator != null) {
            actuator.setEnabled(enabled);
        }
    }

}

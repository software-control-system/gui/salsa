package fr.soleil.salsa.entity;

import java.util.Collection;
import java.util.Date;

/**
 * The interface of a historic log line that save an event of log of a scan.
 */
public interface IHistoricLogLine {

    /**
     * Gets the date.
     * 
     * @return date
     */
    public Date getDate();

    /**
     * Sets the date.
     * 
     * @param date
     */
    public void setDate(Date date);

    /**
     * Gets the action.
     * 
     * @return action
     */
    public String getAction();

    /**
     * Sets the action.
     * 
     * @param action
     */
    public void setAction(String action);

    /**
     * Gets the scan.
     * 
     * @return scan
     */
    public String getScan();

    /**
     * Sets the scan.
     * 
     * @param scan
     */
    public void setScan(String scan);

    /**
     * Gets the scan id.
     * 
     * @return scan
     */
    public String getScanId();

    /**
     * Sets the scan id.
     * 
     * @param scan
     */
    public void setScanId(String scanid);

    /**
     * Gets the nexusFile.
     * 
     * @return nexusFile
     */
    public String getNexusFile();

    /**
     * Gets the nxEntry.
     * 
     * @return nxEntry
     */
    public String getNxEntry();

    /**
     * Sets the nxEntry.
     * 
     * @param nxEntry
     */
    public void setNxEntry(String nxEntry);

    /**
     * Sets the nexusFile.
     * 
     * @param nexusFile
     */
    public void setNexusFile(String nexusFile);

    /**
     * Gets the trajectories.
     * 
     * @return trajectories
     */
    public Collection<ITrajectory> getTrajectories();

    /**
     * Sets the trajectories.
     * 
     * @param trajectories
     */
    public void setTrajectories(Collection<ITrajectory> trajectories);

}

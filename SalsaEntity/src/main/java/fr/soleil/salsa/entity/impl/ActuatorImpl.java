package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.event.ActuatorModel;

/**
 * Implementation of {@link IActuator} for the actuator entity. An actuator is a device that lets
 * the scan progress from one step to the next. For instance, it could be an engine that moves the
 * sample between each mesure.
 * 
 * @author Administrateur
 */
public class ActuatorImpl extends DeviceImpl implements IActuator {

    private static final long serialVersionUID = -1493053426772155106L;

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.name != null ? this.name.hashCode() : 0;
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj parameter is an
     * instance of the base interface : another implementation of the interface is considered equal
     * to this one if it represents the same entity.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        }
        else if (obj == null) {
            equals = false;
        }
        else if (!(obj instanceof IActuator)) {
            equals = false;
        }
        else {
            String thatName = ((IActuator) obj).getName();
            equals = name != null ? name.equals(thatName) : thatName == null;
        }
        return equals;
    }

    protected IDevice initModel() {
        return new ActuatorModel(this);
    }
}

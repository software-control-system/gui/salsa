package fr.soleil.salsa.entity.scan2D;

import fr.soleil.salsa.entity.IRangeTrajectory;

/**
 * @author Alike
 * 
 */
public interface ITrajectory2DX extends ITrajectory2D, IRangeTrajectory<IRange2DX> {

    /**
     * Get the delta constant.
     * 
     * @return
     */
    public Boolean getDeltaConstant();

    /**
     * Set the delta constant.
     * 
     * @param deltaConstant
     */
    public void setDeltaConstant(Boolean deltaConstant);

}

package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.DisplayAxis;
import fr.soleil.salsa.entity.IDisplay;
import fr.soleil.salsa.entity.impl.DisplayImpl;

/**
 * Parameters of the display manager, event handling version.
 */
public class DisplayModel extends AEventHandlingModelDecorator<IDisplay> implements IDisplay {

    private static final long serialVersionUID = 3672106675683350609L;

    /**
     * Constructor.
     * 
     * @param baseBean
     */
    public DisplayModel(IDisplay baseBean) {
        super(baseBean);
    }

    public DisplayModel() {
        super(new DisplayImpl());
    }

    /**
     * @return
     * @see fr.soleil.salsa.entity.IDisplay#getAxisList()
     */
    public List<DisplayAxis> getAxisList() {
        return baseBean.getAxisList();
    }

    /**
     * @param axisList
     * @see fr.soleil.salsa.entity.IDisplay#setAxisList(java.util.List)
     */
    public void setAxisList(List<DisplayAxis> axisList) {
        List<DisplayAxis> oldValue = baseBean.getAxisList();
        baseBean.setAxisList(axisList);
        firePropertyChange("axisList", oldValue, axisList);
    }

    @Override
    public void setTimeBase(boolean timeBase) {
        boolean oldValue = baseBean.isTimeBase();
        baseBean.setTimeBase(timeBase);
        firePropertyChange("timeBase", oldValue, timeBase);
    }

    @Override
    public boolean isTimeBase() {
        return baseBean.isTimeBase();
    }

    @Override
    public IDisplay toModel() {
        return this;
    }

}

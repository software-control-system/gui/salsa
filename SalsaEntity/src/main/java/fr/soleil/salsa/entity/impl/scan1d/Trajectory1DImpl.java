package fr.soleil.salsa.entity.impl.scan1d;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.Trajectory1DModel;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;

/**
 * 
 * @author Alike
 */
public class Trajectory1DImpl extends TrajectoryImpl implements ITrajectory1D {

    private static final long serialVersionUID = -235021890901400362L;

    /**
     * The actuator.
     */
    private IActuator actuator = null;

    /**
     * The range.
     */
    private IRange1D range = null;

    /**
     * @see fr.soleil.salsa.entity.scan1d.ITrajectory1D#getActuator()
     */
    @Override
    public IActuator getActuator() {
        return actuator;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.ITrajectory1D#getRange()
     */
    @Override
    public IRange1D getRange() {
        return range;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.ITrajectory1D#setActuator(fr.soleil.salsa.entity.IActuator)
     */
    @Override
    public void setActuator(IActuator actuator) {
        this.actuator = actuator;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.ITrajectory1D#setRange(fr.soleil.salsa.entity.scan1d.IRange1D)
     */
    @Override
    public void setRange(IRange1D range) {
        this.range = range;
    }

    protected ITrajectory initModel() {
        return new Trajectory1DModel(this);
    }

}

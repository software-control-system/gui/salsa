package fr.soleil.salsa.entity.scanhcs;

import java.util.List;

import fr.soleil.salsa.entity.IDimension;

public interface IDimensionHCS extends IDimension{

    /**
     * Get the range.
     * @return
     */
    public List<IRangeHCS> getRangesXList();

    /**
     * Get trajectories.
     * @return
     */
    public List<ITrajectoryHCS> getTrajectoriesList();

    /**
     * Set the range.
     * @param rangesX
     */
    public void setRangesXList(List<IRangeHCS> rangesX);

    /**
     * Set the trajectories.
     * @param trajectories
     */
    public void setTrajectoriesList(List<ITrajectoryHCS> trajectories);
}

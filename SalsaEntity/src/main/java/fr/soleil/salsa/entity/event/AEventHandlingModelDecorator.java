package fr.soleil.salsa.entity.event;

import java.util.Collection;
import java.util.HashSet;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.entity.IObjectModel;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.wrapping.IBeanWrapper;

/**
 * Base class for a decorateur that adds {@link EntityPropertyChangedEvent} handling to an entity.
 * While it eases model classes development, it is in no way mandatory to extend this.
 * 
 * @author Administrateur
 * 
 * @param <T>
 */
public class AEventHandlingModelDecorator<T>
        implements IBeanWrapper<T>, IEventSource<EntityPropertyChangedEvent<T>>, IObjectModel<T> {

    /**
     * The decorated entity.
     */
    protected T baseBean;

    /**
     * The listeners.
     */
    protected transient Collection<IListener<EntityPropertyChangedEvent<T>>> listeners;

    /**
     * Constructor. If the base bean is not a config element, please use the
     * AEventHandlingModelDecorator(T baseBean, boolean configElement) constructor.
     * 
     * @param baseBean the decorated entity.
     */
    public AEventHandlingModelDecorator(T baseBean) {
        assert (baseBean != null);
        this.setBaseBean(baseBean);
        this.listeners = new HashSet<>();
    }

    /**
     * The decorated entity.
     */
    @Override
    public T getBaseBean() {
        return baseBean;
    }

    /**
     * The decorated entity. Protected : it is only there for internal mechanisms, such as the
     * constructor.
     */
    protected void setBaseBean(T baseBean) {
        this.baseBean = baseBean;
    }

    /**
     * Adds a listener to be notified when a property of the base bean is changed.
     */
    @Override
    public void addListener(IListener<EntityPropertyChangedEvent<T>> listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener.
     */
    @Override
    public void removeListener(IListener<EntityPropertyChangedEvent<T>> listener) {
        listeners.remove(listener);
    }

    /**
     * Returns the listeners set.
     * 
     * @return
     */
    @Override
    public Collection<IListener<EntityPropertyChangedEvent<T>>> getListeners() {
        return listeners;
    }

    /**
     * Method to be called by concrete implementations of this class when a property is changed.
     * Reminder : the EntityPropertyChangedEvent must be called AFTER the property has been changed
     * in the bean.
     * 
     * @param <V>
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    protected <V> void firePropertyChange(String propertyName, V oldValue, V newValue) {
        if (!ObjectUtils.sameObject(oldValue, newValue)) {
            EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                    castThisToT(this), propertyName, oldValue, newValue);
            SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent, listeners);
        }
    }

    /**
     * Casts the decorator to its decorator. Concrete class extending this class MUST implement the
     * T interface.
     * 
     * @param thisInstance
     * @return
     */
    @SuppressWarnings("unchecked")
    private T castThisToT(AEventHandlingModelDecorator<T> thisInstance) {
        return (T) thisInstance;
    }

    /**
     * Delegates the equals method to the base bean. The base bean must check equality using only
     * their base interface, not their actual implementation class.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        return baseBean.equals(obj);
    }

    /**
     * Delegates the hashCode method to the base bean.
     * 
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return baseBean.hashCode();
    }

    /**
     * Writes the name of the implementing class, then the toString of the base bean wrapped between
     * parentheses.
     * 
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + baseBean.toString() + ")";
    }

    @Override
    public T toImpl() {
        return baseBean;
    }

}
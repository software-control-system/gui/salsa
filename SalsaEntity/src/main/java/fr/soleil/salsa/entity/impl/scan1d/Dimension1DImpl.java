package fr.soleil.salsa.entity.impl.scan1d;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.event.Dimension1DModel;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.entity.util.ComparatorUtil;

/**
 * @author Alike
 * 
 */
public class Dimension1DImpl extends DimensionImpl implements IDimension1D {

    private static final long serialVersionUID = 5186731342346109415L;

    /**
     * The range.
     */
    private List<IRange1D> rangesXList;

    /**
     * Trajectories.
     */
    private List<ITrajectory1D> trajectoriesList;

    /**
     * Constructor.
     */
    public Dimension1DImpl() {
        super();
        this.rangesXList = new ArrayList<IRange1D>();
        this.trajectoriesList = new ArrayList<ITrajectory1D>();
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IDimension1D#getRangesXList()
     */
    @Override
    public List<IRange1D> getRangesXList() {
        return rangesXList;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IDimension1D#getTrajectories()
     */
    @Override
    public List<ITrajectory1D> getTrajectoriesList() {
        return trajectoriesList;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IDimension1D#setRangesXList(java.util.List)
     */
    @Override
    public void setRangesXList(List<IRange1D> rangesX) {
        this.rangesXList = rangesX;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IDimension1D#setTrajectories(java.util.Set)
     */
    @Override
    public void setTrajectoriesList(List<ITrajectory1D> trajectories) {
        this.trajectoriesList = trajectories;
    }

    protected IDimension initModel() {
        return new Dimension1DModel(this);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null && super.isEqualsTo(dimension)) {
            if (dimension instanceof IDimension1D) {
                equals = ComparatorUtil.rangeListEquals(
                        ((IDimension1D) dimension).getRangesXList(), getRangesXList());
            }
        }
        return equals;
    }

}

package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

public interface ISuggestionCategory extends Serializable {

    /**
     * Gets the label.
     * @return
     */
    String getLabel();

    /**
     * Sets the label.
     * @param label
     */
    void setLabel(String label);

    /**
     * Gets the suggestion list.
     * @return
     */
    List<ISuggestionItem> getSuggestionList();

    /**
     * Sets the suggestion list.
     * @param suggestionList
     */
    void setSuggestionList(List<ISuggestionItem> suggestionList);

}

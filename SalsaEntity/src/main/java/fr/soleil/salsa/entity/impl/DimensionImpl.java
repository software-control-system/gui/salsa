package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.util.ComparatorUtil;

/**
 * Implementation of {@link IDimension} for the dimension entity. A dimension
 * describes an actual dimension of a scan as a set of movements to be performed
 * by the actuators during the scan. It is divided in ranges, which are parts of
 * the scan where the movement is linear. The ranges themselves are in turn
 * divided in trajectories, which details the movement of an actuator on this
 * range.
 * 
 * @see fr.soleil.salsa.entity.ITrajectory
 * @see fr.soleil.salsa.entity.IRange
 * @author Administrateur
 */
public class DimensionImpl implements IDimension, IObjectImpl<IDimension> {

    private static final long serialVersionUID = 654704848770464659L;

    private IDimension model = null;

    /**
     * The unique identifier.
     */
    private Integer id = 0;

    /**
     * Actuators list.
     */
    private List<IActuator> actuatorsList;

    /**
     * The range list.
     */
    private List<IRange> rangeList = null;

    /**
     * Constructor.
     */
    public DimensionImpl() {
        super();
        actuatorsList = new ArrayList<IActuator>();
    }

    /**
     * Gets the actuators list.
     * 
     * @return
     */
    @Override
    public List<IActuator> getActuatorsList() {
        return actuatorsList;
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return id;
    }

    /**
     * Sets the actuators list.
     * 
     * @param actuatorsList
     */
    @Override
    public void setActuatorsList(List<IActuator> actuatorsList) {
        this.actuatorsList = actuatorsList;
    }

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj
     * parameter is an instance of the base interface : another implementation
     * of the interface is considered equal to this one if it represents the
     * same entity.
     * 
     * @see Object#equals(Object)
     * @param obj
     *            the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        } else if (obj == null) {
            equals = false;
        } else if (!(obj instanceof IDimension)) {
            equals = false;
        } else if (this.id == null) {
            // Approximation. The equals method of a bean that has not received
            // an identifier yet
            // should not be called.
            equals = false;
        } else {
            equals = this.id.equals(((IDimension) obj).getId());
        }
        return equals;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }

    @Override
    public IDimension toModel() {
        if (model == null) {
            model = initModel();
            List<? extends IRange> rangeList = getRangeList();
            List<IRange> rangeModelList = new ArrayList<IRange>();
            for (IRange range : rangeList) {
                if (range instanceof IObjectImpl<?>) {
                    rangeModelList.add((IRange) ((IObjectImpl<?>) range).toModel());
                }
            }
            setRangeList(rangeModelList);
        }
        return model;
    }

    protected IDimension initModel() {
        return null;
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null) {
            if (ObjectUtils.sameObject(dimension.getId(), getId())
                    && ComparatorUtil.deviceListEquals(dimension.getActuatorsList(), getActuatorsList())) {
                equals = true;
            }
        }
        return equals;
    }

    @Override
    public List<? extends IRange> getRangeList() {
        List<? extends IRange> tmpRangeList = null;

        if (this instanceof IDimension1D) {
            IDimension1D dimension1D = ((IDimension1D) this);
            tmpRangeList = dimension1D.getRangesXList();
        } else if (this instanceof IDimension2DX) {
            IDimension2DX dimension2DX = ((IDimension2DX) this);
            tmpRangeList = dimension2DX.getRangesList();
        } else if (this instanceof IDimension2DY) {
            IDimension2DY dimension2DY = ((IDimension2DY) this);
            tmpRangeList = dimension2DY.getRangesList();
        } else if (this instanceof IDimensionEnergy) {
            IDimensionEnergy dimensionEnergy = ((IDimensionEnergy) this);
            tmpRangeList = dimensionEnergy.getRangesEnergyList();
        } else if (this instanceof IDimensionHCS) {
            IDimensionHCS dimensionHCS = ((IDimensionHCS) this);
            tmpRangeList = dimensionHCS.getRangesXList();
        } else if (this instanceof IDimensionK) {
            IDimensionK dimensionK = ((IDimensionK) this);
            tmpRangeList = dimensionK.getRangesXList();
        } else {
            tmpRangeList = rangeList;
        }
        return tmpRangeList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setRangeList(List<? extends IRange> arangeList) {
        if (this instanceof IDimension1D) {
            IDimension1D dimension1D = ((IDimension1D) this);
            dimension1D.setRangesXList((List<IRange1D>) arangeList);
        } else if (this instanceof IDimension2DX) {
            IDimension2DX dimension2DX = ((IDimension2DX) this);
            dimension2DX.setRangesList((List<IRange2DX>) arangeList);
        } else if (this instanceof IDimension2DY) {
            IDimension2DY dimension2DY = ((IDimension2DY) this);
            dimension2DY.setRangesList((List<IRange2DY>) arangeList);
        } else if (this instanceof IDimensionEnergy) {
            IDimensionEnergy dimensionEnergy = ((IDimensionEnergy) this);
            dimensionEnergy.setRangesEnergyList((List<IRangeEnergy>) arangeList);
        } else if (this instanceof IDimensionHCS) {
            IDimensionHCS dimensionHCS = ((IDimensionHCS) this);
            dimensionHCS.setRangesXList((List<IRangeHCS>) arangeList);
        } else if (this instanceof IDimensionK) {
            IDimensionK dimensionK = ((IDimensionK) this);
            dimensionK.setRangesXList((List<IRangeK>) arangeList);
        } else {
            this.rangeList = (List<IRange>) arangeList;
        }
    }

    @Override
    public void addRange(IRange range) {
        if ((range != null) && (getRangeList() != null)) {
            if ((this instanceof IDimension1D) && (range instanceof IRange1D)) {
                IDimension1D dimension1D = ((IDimension1D) this);
                List<IRange1D> tmpRangeList = dimension1D.getRangesXList();
                tmpRangeList.add((IRange1D) range);
            } else if ((this instanceof IDimension2DX) && (range instanceof IRange2DX)) {
                IDimension2DX dimension2DX = ((IDimension2DX) this);
                List<IRange2DX> tmpRangeList = dimension2DX.getRangesList();
                tmpRangeList.add((IRange2DX) range);
            } else if ((this instanceof IDimension2DY) && (range instanceof IRange2DY)) {
                IDimension2DY dimension2DY = ((IDimension2DY) this);
                List<IRange2DY> tmpRangeList = dimension2DY.getRangesList();
                tmpRangeList.add((IRange2DY) range);
            } else if ((this instanceof IDimensionEnergy) && (range instanceof IRangeEnergy)) {
                IDimensionEnergy dimensionEnergy = ((IDimensionEnergy) this);
                List<IRangeEnergy> tmpRangeList = dimensionEnergy.getRangesEnergyList();
                tmpRangeList.add((IRangeEnergy) range);
            } else if ((this instanceof IDimensionHCS) && (range instanceof IRangeHCS)) {
                IDimensionHCS dimensionHCS = ((IDimensionHCS) this);
                List<IRangeHCS> tmpRangeList = dimensionHCS.getRangesXList();
                tmpRangeList.add((IRangeHCS) range);
            } else if ((this instanceof IDimensionK) && (range instanceof IRangeK)) {
                IDimensionK dimensionK = ((IDimensionK) this);
                List<IRangeK> tmpRangeList = dimensionK.getRangesXList();
                tmpRangeList.add((IRangeK) range);
            }
        }
    }
}

package fr.soleil.salsa.entity.event;

import java.sql.Timestamp;
import java.util.List;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.impl.DirectoryImpl;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Event handling decorator of {@link IDirectory} for the directory entity. A directory contains any
 * number of scan configurations, and any number of subdirectories.
 * 
 * @author Administrateur
 */
public class DirectoryModel extends AEventHandlingModelDecorator<IDirectory> implements IDirectory {

    private static final long serialVersionUID = 144357304829049608L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param directory the decorated base bean.
     */
    public DirectoryModel(IDirectory directory) {
        super(directory);
    }

    /**
     * Default constructor, that creates a new instance of DirectoryImpl and wraps it.
     */
    public DirectoryModel() {
        this(new DirectoryImpl());
    }

    /**
     * Gets the unique identifier.
     * 
     * @see IDirectory#getId()
     * @return id
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @see IDirectory#setId(Integer)
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the directory name.
     * 
     * @see IDirectory#getName()
     * @return name
     */
    @Override
    public String getName() {
        return this.baseBean.getName();
    }

    /**
     * Sets the directory name.
     * 
     * @see IDirectory#setName(String)
     * @param String
     */
    @Override
    public void setName(String name) {
        String oldValue = this.baseBean.getName();
        this.baseBean.setName(name);
        this.firePropertyChange("name", oldValue, name);
    }

    /**
     * Gets the directory containing this directory. This is null for the root directory.
     * 
     * @see IDirectory#getDirectory()
     * @return directory
     */
    @Override
    public IDirectory getDirectory() {
        return baseBean.getDirectory();
    }

    /**
     * Sets the directory containing this directory. This is null for the root directory.
     * 
     * @see IDirectory#setDirectory(IDirectory)
     * @param IDirectory
     */
    @Override
    public void setDirectory(IDirectory directory) {
        IDirectory oldValue = this.baseBean.getDirectory();
        this.baseBean.setDirectory(directory);
        this.firePropertyChange("directory", oldValue, directory);
    }

    /**
     * Gets the position in the diretory, indicating the basic sort order.
     * 
     * @see IDirectory#getPositionInDirectory()
     * @return positionInDirectory
     */
    @Override
    public Integer getPositionInDirectory() {
        return this.baseBean.getPositionInDirectory();
    }

    /**
     * Sets the position in the diretory, indicating the basic sort order.
     * 
     * @see IDirectory#setPositionInDirectory(Integer)
     * @param Integer
     */
    @Override
    public void setPositionInDirectory(Integer positionInDirectory) {
        Integer oldValue = this.baseBean.getPositionInDirectory();
        this.baseBean.setPositionInDirectory(positionInDirectory);
        this.firePropertyChange("positionInDirectory", oldValue, positionInDirectory);
    }

    /**
     * Gets the subdirectories list.
     * 
     * @see IDirectory#getSubDirectoriesList()
     * @return subDirectoriesList
     */
    @Override
    public List<IDirectory> getSubDirectoriesList() {
        return baseBean.getSubDirectoriesList();
    }

    /**
     * Sets the subdirectories list.
     * 
     * @see IDirectory#setSubDirectoriesList(List<IDirectory>)
     * @param List<IDirectory>
     */
    @Override
    public void setSubDirectoriesList(List<IDirectory> subDirectoriesList) {
        List<IDirectory> oldValue = this.baseBean.getSubDirectoriesList();
        this.baseBean.setSubDirectoriesList(subDirectoriesList);
        this.firePropertyChange("subDirectoriesList", oldValue, subDirectoriesList);
    }

    /**
     * Gets the configurations list.
     * 
     * @see IDirectory#getConfigList()
     * @return configList
     */
    @Override
    public List<IConfig<?>> getConfigList() {
        return baseBean.getConfigList();
    }

    /**
     * Sets the configurations list.
     * 
     * @see IDirectory#setConfigList(List<IConfig>)
     * @param List<IConfig>
     */
    @Override
    public void setConfigList(List<IConfig<?>> configList) {
        List<IConfig<?>> oldValue = this.baseBean.getConfigList();
        this.baseBean.setConfigList(configList);
        this.firePropertyChange("configList", oldValue, configList);
    }

    /**
     * 
     */
    @Override
    public Timestamp getTimestamp() {
        return this.baseBean.getTimestamp();
    }

    /**
     * 
     */
    @Override
    public void setTimestamp(Timestamp timestamp) {
        Timestamp oldValue = this.baseBean.getTimestamp();
        this.baseBean.setTimestamp(timestamp);
        this.firePropertyChange("timestamp", oldValue, timestamp);
    }

    @Override
    public String getFullPath() {
        return baseBean.getFullPath();
    }

    @Override
    public String toString() {
        String toStringValue = super.toString();
        String fullPath = getFullPath();
        if (SalsaUtils.isDefined(fullPath)) {
            toStringValue = fullPath;
        }
        return toStringValue;
    }
}

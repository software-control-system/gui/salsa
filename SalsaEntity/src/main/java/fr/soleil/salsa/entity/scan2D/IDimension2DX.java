package fr.soleil.salsa.entity.scan2D;

import java.util.List;

/**
 * @author Alike
 *
 */
public interface IDimension2DX extends IDimension2D {

    /**
     * Get ranges.
     * @return
     */
    public List<IRange2DX> getRangesList();

    /**
     * Set the ranges.
     * @param ranges
     */
    public void setRangesList(List<IRange2DX> rangesList);
    
    
}

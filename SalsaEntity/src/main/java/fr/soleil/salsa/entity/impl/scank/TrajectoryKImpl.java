package fr.soleil.salsa.entity.impl.scank;

import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.TrajectoryKModel;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.TrajectoryUtil;

public class TrajectoryKImpl extends TrajectoryImpl implements ITrajectoryK {

    private static final long serialVersionUID = 1588206331963290420L;

    /**
     * The range.
     */
    private IRangeK range;

    // Trajectory K Parameters
    private double e0, e1, e2;
    private double eDeltaEdge, eDeltaPreEdge, eMin;
    private double kDelta, kMax, kMin;
    private double m, n;
    private double timePostEdge, timeEdge, timePreEdge, deadTime;

    public TrajectoryKImpl() {
        e0 = e1 = e2 = eDeltaEdge = eDeltaPreEdge = eMin = kDelta = kMax = kMin = m = n = timePostEdge = timeEdge = timePreEdge = deadTime = 0d;
    }

    private void computeKMin() {
        kMin = TrajectoryUtil.calculateKmin(this);
    }

    @Override
    public IRangeK getRange() {
        return range;
    }

    @Override
    public void setRange(IRangeK range) {
        this.range = range;
    }

    @Override
    public double getE0() {
        return e0;
    }

    @Override
    public void setE0(double e0) {
        this.e0 = e0;
        computeKMin();
    }

    @Override
    public double getE1() {
        return e1;
    }

    @Override
    public void setE1(double e1) {
        this.e1 = e1;
    }

    @Override
    public double getE2() {
        return e2;
    }

    @Override
    public void setE2(double e2) {
        this.e2 = e2;
        computeKMin();
    }

    @Override
    public double getEDeltaEdge() {
        return eDeltaEdge;
    }

    @Override
    public void setEDeltaEdge(double eDeltaEdge) {
        this.eDeltaEdge = eDeltaEdge;
    }

    @Override
    public double getEDeltaPreEdge() {
        return eDeltaPreEdge;
    }

    @Override
    public void setEDeltaPreEdge(double eDeltaPreEdge) {
        this.eDeltaPreEdge = eDeltaPreEdge;
    }

    @Override
    public double getEMin() {
        return eMin;
    }

    @Override
    public void setEMin(double eMin) {
        this.eMin = eMin;
    }

    @Override
    public double getKDelta() {
        return kDelta;
    }

    @Override
    public void setKDelta(double kDelta) {
        this.kDelta = kDelta;
    }

    @Override
    public double getKMax() {
        return kMax;
    }

    @Override
    public void setKMax(double kMax) {
        this.kMax = kMax;
    }

    @Override
    public double getKMin() {
        return kMin;
    }

    @Override
    public void setKMin(double kMin) {
        this.kMin = kMin;
    }

    @Override
    public double getM() {
        return m;
    }

    @Override
    public void setM(double m) {
        this.m = m;
    }

    @Override
    public double getN() {
        return n;
    }

    @Override
    public void setN(double n) {
        this.n = n;
    }

    @Override
    public double getTimePostEdge() {
        return timePostEdge;
    }

    @Override
    public void setTimePostEdge(double timePostEdge) {
        this.timePostEdge = timePostEdge;
    }

    @Override
    public double getTimeEdge() {
        return timeEdge;
    }

    @Override
    public void setTimeEdge(double timeEdge) {
        this.timeEdge = timeEdge;
    }

    @Override
    public double getTimePreEdge() {
        return timePreEdge;
    }

    @Override
    public void setTimePreEdge(double timePreEdge) {
        this.timePreEdge = timePreEdge;
    }

    @Override
    public double getDeadTime() {
        return deadTime;
    }

    @Override
    public void setDeadTime(double deadTime) {
        this.deadTime = deadTime;
    }

    @Override
    protected ITrajectory initModel() {
        return new TrajectoryKModel(this);
    }

}

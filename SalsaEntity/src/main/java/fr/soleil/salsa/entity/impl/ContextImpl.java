package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IContext;

public class ContextImpl implements IContext {

    private static final long serialVersionUID = -4472099140137184629L;

    private String userLogFile;

    private String scanServerName;

    private Integer maxLineNumber;

    /**
     * @see fr.soleil.salsa.entity.IContext#getUserLogFile(java.lang.String)
     */
    public String getUserLogFile() {
        return userLogFile;
    }

    /**
     * @see fr.soleil.salsa.entity.IContext#setUserLogFile(java.lang.String)
     */
    public void setUserLogFile(String userLogFile) {
        this.userLogFile = userLogFile;
    }

    /**
     * @see fr.soleil.salsa.entity.IContext#getScanServerName(java.lang.String)
     */
    public String getScanServerName() {
        return scanServerName;
    }

    /**
     * @see fr.soleil.salsa.entity.IContext#setScanServerName(java.lang.String)
     */
    public void setScanServerName(String scanServerName) {
        this.scanServerName = scanServerName;
    }

    public Integer getMaxLineNumber() {
        return maxLineNumber;
    }

    public void setMaxLineNumber(Integer maxLineNumber) {
        this.maxLineNumber = maxLineNumber;

    }

}

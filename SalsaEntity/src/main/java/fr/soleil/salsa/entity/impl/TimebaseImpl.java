package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.event.TimebaseModel;

/**
 * Implementation of {@link ITimebase} for the timeBase entity. A timeBase is a detector that
 * measures the scanned values.
 * 
 * @author Administrateur
 */
public class TimebaseImpl extends DeviceImpl implements ITimebase {

    private static final long serialVersionUID = 7673315882274831301L;

    /**
     * Constructor.
     */
    public TimebaseImpl() {
        super();
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj parameter is an
     * instance of the base interface : another implementation of the interface is considered equal
     * to this one if it represents the same entity.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        }
        else if (obj == null) {
            equals = false;
        }
        else if (!(obj instanceof ITimebase)) {
            equals = false;
        }
        else {
            String thatName = ((ITimebase) obj).getName();
            equals = name != null ? name.equals(thatName) : thatName == null;
        }
        return equals;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.name != null ? this.name.hashCode() : 0;
    }

    protected IDevice initModel() {
        return new TimebaseModel(this);
    }

}

package fr.soleil.salsa.entity.event.queue;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;

import javax.swing.SwingUtilities;

import fr.soleil.salsa.entity.event.IEvent;
import fr.soleil.salsa.entity.event.IMergeCapableEvent;
import fr.soleil.salsa.entity.event.listener.IListener;

/**
 * The event queue.<br />
 * This class allows the queuing of events in order to process them in an orderly fashion.<br />
 * The queue can merge new events with existing ones if they implement {@link IMergeCapableEvent}.
 * Merging events result in a single new event that will be processed when its oldest component
 * would have been. This avoids unnecessary overload and reduces risks of infinite loop when
 * triggering events.<br />
 * This is a singleton that should be accessed through its getInstance static method.<br />
 * <br />
 * Typical usage is<br />
 * SalsaEventQueue.getInstance().queueEvent(event);<br />
 * or the shortcut<br />
 * SalsaEventQueue.staticQueueEvent(event);<br />
 * <br />
 * 
 * @see IMergeCapableEvent
 * @see IEvent
 * @see IListener
 * @author Administrateur
 */
public class SalsaEventQueue {

    /**
     * This inner class holds an event and the listeners that will be notified.
     */
    protected class EventAndListeners<E extends IEvent> {
        /**
         * The event.
         */
        protected E event;

        /**
         * The listeners listening to the event.
         */
        protected Set<IListener<E>> listenersSet;

        /**
         * Constructor.
         * 
         * @param event the event.
         * @param listenersList the listeners litening to the event.
         */
        public EventAndListeners(E event, Collection<IListener<E>> listenersList) {
            super();
            this.event = event;
            // The listeners might not have overridden the equals and hashCode methods, but the
            // default ones from Object
            // compares the references and that is fine for our purposes.
            this.listenersSet = new HashSet<IListener<E>>();
            listenersSet.addAll(listenersList);
        }

        /**
         * The event.
         * 
         * @return the event
         */
        public E getEvent() {
            return event;
        }

        /**
         * The listeners litening to the event.
         * 
         * @return the listenersList
         */
        public Set<IListener<E>> getListenersSet() {
            return listenersSet;
        }
    }

    /**
     * The one instance, SalsaEventQueue is a singleton.
     */
    private static SalsaEventQueue instance;

    /**
     * The events queue.
     */
    private LinkedList<EventAndListeners<IEvent>> eventsList;

    /**
     * Indicates whether the queue is currently processing events.
     */
    private Boolean currentlyProcessingEvents;

    /**
     * Initializes the instance.
     */
    static {
        SalsaEventQueue.instance = new SalsaEventQueue();
    }

    /**
     * Constructor.
     */
    private SalsaEventQueue() {
        this.eventsList = new LinkedList<EventAndListeners<IEvent>>();
        this.currentlyProcessingEvents = false;
    }

    /**
     * SalsaEventQueue is a singleton, this returns the instance.
     * 
     * @return
     */
    public static SalsaEventQueue getInstance() {
        // No need to check if instance is null : it is initialized in a static block.
        return SalsaEventQueue.instance;
    }

    /**
     * A convenience method, shortcut for getInstance().queueEvent().
     * 
     * @param <E>
     * @param event
     * @param listenersCollection
     */
    public static <E extends IEvent> void staticQueueEvent(E event,
            Collection<IListener<E>> listenersCollection) {
        SalsaEventQueue.getInstance().queueEvent(event, listenersCollection);
    }

    /**
     * Adds an event to the queue for later processing. The event might be merged with an existing
     * one if possible, so the events may not be processed in the order they were queued.
     * 
     * @param <E>
     * @param event
     * @param listenersCollection
     */
    @SuppressWarnings("unchecked")
    public <E extends IEvent> void queueEvent(E event, Collection<IListener<E>> listenersCollection) {
        synchronized (this.eventsList) {
            // No point in adding an event if it has no listeners.
            if (listenersCollection == null || listenersCollection.size() > 0) {
                // If the event can be merged, attempt to do so.
                if (event instanceof IMergeCapableEvent) {
                    // Search for an existing event that can be merged with the new one using the
                    // canMerge method.
                    IMergeCapableEvent mergeCapableEvent = (IMergeCapableEvent) event;
                    ListIterator<EventAndListeners<IEvent>> eventsIterator = this.eventsList
                            .listIterator();
                    EventAndListeners<IEvent> existingEventAndListeners;
                    boolean merged = false;
                    while (eventsIterator.hasNext()) {
                        existingEventAndListeners = eventsIterator.next();
                        IEvent existingEvent = existingEventAndListeners.getEvent();
                        if (mergeCapableEvent.canMerge(existingEvent)) {
                            // If the new event can be merged with an existing one, do so.
                            IEvent mergedEvent = mergeCapableEvent.merge(existingEvent);
                            Set<IListener<IEvent>> existingListenersSet = existingEventAndListeners
                                    .getListenersSet();
                            Set<IListener<IEvent>> mergedListenersSet = new HashSet<IListener<IEvent>>(
                                    listenersCollection.size() + existingListenersSet.size());
                            mergedListenersSet.addAll(existingListenersSet);
                            try {
                                for (IListener<E> listener : listenersCollection) {
                                    mergedListenersSet.add((IListener<IEvent>) listener);
                                }
                            }
                            // ConcurrentModificationException
                            catch (Exception e) {
                            }
                            EventAndListeners<IEvent> mergedEventAndListeners = new EventAndListeners<IEvent>(
                                    mergedEvent, mergedListenersSet);
                            // Replaces the old event with the new, merged event.
                            eventsIterator.set(mergedEventAndListeners);
                            merged = true;
                            break;
                        }
                    }
                    if (!merged) {
                        // If the new event can be merged, but no event it could merge with was
                        // found,
                        // adds it to the end of the queue.
                        EventAndListeners<E> newEventAndListeners = new EventAndListeners<E>(event,
                                listenersCollection);
                        this.eventsList.offer((EventAndListeners<IEvent>) newEventAndListeners);
                    }
                }
                else {
                    // If the new event can not be merged, adds it to the end of the queue.
                    EventAndListeners<E> newEventAndListeners = new EventAndListeners<E>(event,
                            listenersCollection);
                    this.eventsList.offer((EventAndListeners<IEvent>) newEventAndListeners);
                }
            }
        }
        this.scheduleEventsProcessing();
    }

    /**
     * Schedule the processing of events.<br />
     * The events are processed on the EDT. This is intentional. This ensures that Swing events and
     * Salsa events will not be processed simultaneously. Additionally, events that triggers short
     * treatment should be executed on the EDT to access Swing functions. Long jobs can create their
     * own threads using SalsaSwingWorker. Using the EDT reduces extra thread creation overload.
     */
    protected void scheduleEventsProcessing() {
        synchronized (this.currentlyProcessingEvents) {
            if (!this.currentlyProcessingEvents) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        processEvents();
                    }
                });
            }
            // While the queue is not actually processing events yet, it will as soon as the EDT
            // gets to it.
            // We do not want to schedule more events processing during that time.
            this.currentlyProcessingEvents = true;
        }
    }

    /**
     * Process the events.
     */
    protected synchronized void processEvents() {
        synchronized (this.currentlyProcessingEvents) {
            this.currentlyProcessingEvents = true;
        }
        EventAndListeners<IEvent> eventAndListeners;
        // Sadly, you cannot use synchronized on a while condition without affecting its body, so
        // the loop exits on a break instead.
        while (true) {
            synchronized (this.eventsList) {
                eventAndListeners = this.eventsList.poll();
            }
            if (eventAndListeners == null) {
                break;
            }
            this.fireEvent(eventAndListeners);
        }
        synchronized (this.currentlyProcessingEvents) {
            this.currentlyProcessingEvents = false;
        }
    }

    /**
     * Fire an event.
     * 
     * @param eventAndListeners
     */
    protected void fireEvent(EventAndListeners<IEvent> eventAndListeners) {
        IEvent event = eventAndListeners.getEvent();
        Set<IListener<IEvent>> listenersSet = eventAndListeners.getListenersSet();
        for (IListener<IEvent> listener : listenersSet) {
            listener.notifyEvent(event);
        }
    }
}

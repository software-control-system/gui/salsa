package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.DisplayAxis;
import fr.soleil.salsa.entity.IDisplay;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.DisplayModel;

/**
 * The display manager parameters.
 */
public class DisplayImpl implements IDisplay, IObjectImpl<IDisplay> {

    private static final long serialVersionUID = 1575542119058532111L;

    /**
     * The list of axis, in order.
     */
    private List<DisplayAxis> axisList = new ArrayList<DisplayAxis>();

    private boolean timeBase = false;

    IDisplay model = null;

    /**
     * The list of axis, in order.
     * 
     * @return the axisList
     */
    @Override
    public List<DisplayAxis> getAxisList() {
        return axisList;
    }

    /**
     * The list of axis, in order.
     * 
     * @param axisList
     *            the axisList to set
     */
    @Override
    public void setAxisList(List<DisplayAxis> axisList) {
        this.axisList = axisList;
    }

    @Override
    public void setTimeBase(boolean timeBase) {
        this.timeBase = timeBase;
    }

    @Override
    public boolean isTimeBase() {
        return timeBase;
    }

    @Override
    public IDisplay toModel() {
        if (model == null) {
            model = initModel();
        }
        return model;
    }

    protected IDisplay initModel() {
        DisplayModel displayModel = new DisplayModel(this);
        return displayModel;
    }

    @Override
    public IDisplay toImpl() {
        return this;
    }
}

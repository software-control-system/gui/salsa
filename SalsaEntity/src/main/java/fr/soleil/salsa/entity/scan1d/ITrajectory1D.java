package fr.soleil.salsa.entity.scan1d;

import fr.soleil.salsa.entity.IActuatorTrajectory;
import fr.soleil.salsa.entity.IRangeTrajectory;

/**
 * 
 * @author Alike
 */
public interface ITrajectory1D extends IActuatorTrajectory, IRangeTrajectory<IRange1D> {

    /**
     * True if the delta is constant.
     * 
     * @return
     */
    public Boolean isDeltaConstant();

    /**
     * Set true if the delta is constant.
     * 
     * @param deltaConstant
     */
    public void setDeltaConstant(Boolean deltaConstant);

}

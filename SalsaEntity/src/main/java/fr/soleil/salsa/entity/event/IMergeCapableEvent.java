package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;

/**
 * This is the interface for a merge capable event. The event queue will attempt to merge events
 * whenever possible to avoid overload and chances of inifinite loops in event handling. Events that
 * do not wish to use this functionality should implement {@link IEvent} instead.
 * 
 * @author Administrateur
 * @see SalsaEventQueue
 * @see IEvent
 */
public interface IMergeCapableEvent extends IEvent {

    /**
     * The events should implement this method to indicate whether they can be merged with another
     * event.
     * 
     * @param event
     * @return
     */
    public boolean canMerge(IEvent event);

    /**
     * The events should implement this method to perform merging. This method can only be called if
     * the canMerge method answered true for that instance of event.
     * 
     * @param event
     * @return
     */
    public IEvent merge(IEvent event);
}

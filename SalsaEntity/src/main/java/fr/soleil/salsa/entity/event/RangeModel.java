package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;

/**
 * Event handling decorator of {@link IRange} for the range entity. A range is a part of a dimension
 * where the movements of the actuators are linear. The trajectory list contains the details of the
 * movement of each actuator on this range.
 * 
 * @see fr.soleil.salsa.entity.ITrajectory
 * @see fr.soleil.salsa.entity.IDimension
 * @author Administrateur
 * 
 */
public class RangeModel extends AEventHandlingModelDecorator<IRange> implements IRange {

    private static final long serialVersionUID = -2503765384741655530L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param range the decorated base bean.
     */
    public RangeModel(IRange range) {
        super(range);
    }

    /**
     * Gets the unique identifier.
     * 
     * @see IRange#getId()
     * @return id
     */
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @see IRange#setId(Integer)
     * @param Integer
     */
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the number of steps between the starting and ending position.
     * 
     * @see IRange#getStepsNumber()
     * @return stepsNumber
     */
    public Integer getStepsNumber() {
        return this.baseBean.getStepsNumber();
    }

    /**
     * Sets the number of steps between the starting and ending position.
     * 
     * @see IRange#setStepsNumber(Integer)
     * @param Integer
     */
    public void setStepsNumber(Integer stepsNumber) {
        Integer oldValue = this.baseBean.getStepsNumber();
        this.baseBean.setStepsNumber(stepsNumber);
        this.firePropertyChange("stepsNumber", oldValue, stepsNumber);
    }

    /**
     * Gets the trajectories of the actuators on this range.
     * 
     * @see IRange#getTrajectoriesList()
     * @return trajectoriesList
     */
    public List<ITrajectory> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    /**
     * Sets the trajectories of the actuators on this range.
     * 
     * @see IRange#setTrajectoriesList(List<ITrajectory>)
     * @param List<ITrajectory>
     */
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        List<ITrajectory> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectoriesList);
        this.firePropertyChange("trajectoriesList", oldValue, trajectoriesList);
    }

    /**
     * Gets the dimension this range is a part of.
     * 
     * @see IRange#getDimension()
     * @return dimension
     */
    public IDimension getDimension() {
        return baseBean.getDimension();
    }

    /**
     * Sets the dimension this range is a part of.
     * 
     * @see IRange#setDimension(IDimension)
     * @param IDimension
     */
    public void setDimension(IDimension dimension) {
        IDimension oldValue = this.baseBean.getDimension();
        this.baseBean.setDimension(dimension);
        this.firePropertyChange("dimension", oldValue, dimension);
    }

    @Override
    public ITrajectory createTrajectory(IActuator actuator) {
        return this.baseBean.createTrajectory(actuator);
    }

    @Override
    public void setStepNumberNoRefresh(Integer stepsNumber) {
        this.baseBean.setStepNumberNoRefresh(stepsNumber);
    }
}

package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.flyscan.IConfigFlyScan;
import fr.soleil.salsa.entity.impl.flyscan.ConfigFlyScanImpl;
import fr.soleil.salsa.entity.impl.scan1d.Config1DImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;

/**
 * Event handling decorator of {@link IConfig1D} for the config1D entity.
 */
public class ConfigFlyScanModel extends AConfigModel<IDimension, IConfigFlyScan> implements
IConfigFlyScan {

    private static final long serialVersionUID = -5879540625712520342L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     *
     * @param config1D the decorated base bean.
     */
    public ConfigFlyScanModel(final IConfigFlyScan configFlyScan) {
        super(configFlyScan);
    }

    /**
     * Default constructor, that creates a new instance of {@link Config1DImpl} and wraps it.
     */
    public ConfigFlyScanModel() {
        this(new ConfigFlyScanImpl());
    }

    @Override
    public IDimension addDimension() {
        Object oldValue = getDimensionList();
        IDimension dimension = baseBean.addDimension();
        Object newValue = getDimensionList();
        this.firePropertyChange("dimensionList", oldValue, newValue);
        return dimension;
    }

    @Override
    public void removeDimension() {
        Object oldValue = getDimensionList();
        baseBean.removeDimension();
        Object newValue = getDimensionList();
        this.firePropertyChange("dimensionList", oldValue, newValue);
    }

    @Override
    public List<IActuator> getActuatorList(final IDimension dimension) {
        return baseBean.getActuatorList(FIRST_DIMENSION);
    }

    @Override
    public void addActuator(final IDimension dimension, final IActuator actuator) {
        Object oldValue = getActuatorList(FIRST_DIMENSION);
        baseBean.addActuator(dimension, actuator);
        Object newValue = getActuatorList(FIRST_DIMENSION);
        this.firePropertyChange("actuatorList", oldValue, newValue);
    }

    @Override
    public void removeActuator(final IDimension dimension, final IActuator actuator) {
        Object oldValue = getActuatorList(FIRST_DIMENSION);
        baseBean.removeActuator(dimension, actuator);
        Object newValue = getActuatorList(1);
        this.firePropertyChange("actuatorList", oldValue, newValue);

    }

    @Override
    public void setActuatorName(final IActuator actuator, final String name) {
        Object oldValue = actuator;
        baseBean.setActuatorName(actuator, name);
        Object newValue = actuator;
        this.firePropertyChange("actuator name", oldValue, newValue);
    }

    @Override
    public void setActuatorEnabled(final IActuator actuator, final boolean enabled) {
        Object oldValue = actuator;
        baseBean.setActuatorEnabled(actuator, enabled);
        Object newValue = actuator;
        this.firePropertyChange("actuator enable", oldValue, newValue);
    }

    @Override
    public void setTrajectory(final IActuator actuator, final ITrajectory trajectory) {
        // TODO Auto-generated method stub

    }

    @Override
    public ITrajectory getTrajectory(final IActuator actuator) {
        // TODO Auto-generated method stub
        return null;
    }

}

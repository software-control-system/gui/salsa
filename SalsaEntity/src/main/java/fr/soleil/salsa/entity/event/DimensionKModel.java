package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.impl.scank.DimensionKImpl;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;

public class DimensionKModel extends DimensionModel<IDimensionK> implements IDimensionK {

    private static final long serialVersionUID = 4232772626434426779L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param dimensionK the decorated base bean.
     */
    public DimensionKModel(IDimensionK dimensionK) {
        super(dimensionK);
    }

    /**
     * Default constructor, that creates a new instance of DimensionKImpl and wraps it.
     */
    public DimensionKModel() {
        this(new DimensionKImpl());
    }

    /**
     * 
     */
    public IRangeK getRangeX() {
        return baseBean.getRangeX();
    }

    /**
     * 
     */
    public void setRangeX(IRangeK rangeX) {
        IRangeK oldValue = this.baseBean.getRangeX();
        this.baseBean.setRangeX(rangeX);
        this.firePropertyChange("rangesX", oldValue, rangeX);
    }

    /**
     * 
     */
    public ITrajectoryK getTrajectory() {
        return baseBean.getTrajectory();
    }

    /**
     * 
     */
    public void setTrajectory(ITrajectoryK trajectory) {
        ITrajectoryK oldValue = this.baseBean.getTrajectory();
        this.baseBean.setTrajectory(trajectory);
        this.firePropertyChange("trajectories", oldValue, trajectory);
    }

    @Override
    public List<IRangeK> getRangesXList() {
        return this.baseBean.getRangesXList();
    }

    @Override
    public void setRangesXList(List<IRangeK> rangesX) {
        this.baseBean.setRangesXList(rangesX);
    }

}

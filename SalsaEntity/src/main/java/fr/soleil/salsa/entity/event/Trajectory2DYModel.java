package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scan2d.Trajectory2DYImpl;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;

/**
 * @author Alike
 * 
 */
public class Trajectory2DYModel extends AEventHandlingModelDecorator<ITrajectory2DY> implements ITrajectory2DY {

    private static final long serialVersionUID = 2107663513958675578L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param trajectory2D the decorated base bean.
     */
    public Trajectory2DYModel(ITrajectory2DY trajectory2D) {
        super(trajectory2D);
    }

    /**
     * Default constructor, that creates a new instance of Trajectory2DImpl and wraps it.
     */
    public Trajectory2DYModel() {
        this(new Trajectory2DYImpl());
    }

    /**
     * Gets the unique identifier.
     * 
     * @see ITrajectory#getId()
     * @return id
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @see ITrajectory#setId(Integer)
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the initial position.
     * 
     * @see ITrajectory#getBeginPosition()
     * @return beginPosition
     */
    @Override
    public double getBeginPosition() {
        return this.baseBean.getBeginPosition();
    }

    /**
     * Sets the initial position.
     * 
     * @see ITrajectory#setBeginPosition(double)
     * @param double
     */
    @Override
    public void setBeginPosition(double beginPosition) {
        double oldValue = this.baseBean.getBeginPosition();
        this.baseBean.setBeginPosition(beginPosition);
        this.firePropertyChange("beginPosition", oldValue, beginPosition);
    }

    /**
     * Gets the final position.
     * 
     * @see ITrajectory#getEndPosition()
     * @return endPosition
     */
    @Override
    public double getEndPosition() {
        return this.baseBean.getEndPosition();
    }

    /**
     * Sets the final position.
     * 
     * @see ITrajectory#setEndPosition(double)
     * @param double
     */
    @Override
    public void setEndPosition(double endPosition) {
        double oldValue = this.baseBean.getEndPosition();
        this.baseBean.setEndPosition(endPosition);
        this.firePropertyChange("endPosition", oldValue, endPosition);
    }

    /**
     * Gets the position difference between two consecutive steps. It is always positive. For
     * instance, the delta of trajectory 6, 8, 10, 12 is 2.
     * 
     * @see ITrajectory#getDelta()
     * @return delta
     */
    @Override
    public double getDelta() {
        return this.baseBean.getDelta();
    }

    /**
     * Sets the position difference between two consecutive steps. It is always positive. For
     * instance, the delta of trajectory 6, 8, 10, 12 is 2.
     * 
     * @see ITrajectory#setDelta(double)
     * @param double
     */
    @Override
    public void setDelta(double delta) {
        double oldValue = this.baseBean.getDelta();
        this.baseBean.setDelta(delta);
        this.firePropertyChange("delta", oldValue, delta);
    }

    /**
     * Gets the actuator movement speed. It is always positive.
     * 
     * @see ITrajectory#getSpeed()
     * @return speed
     */
    @Override
    public double getSpeed() {
        return this.baseBean.getSpeed();
    }

    /**
     * Sets the actuator movement speed. It is always positive.
     * 
     * @see ITrajectory#setSpeed(double)
     * @param double
     */
    @Override
    public void setSpeed(double speed) {
        double oldValue = this.baseBean.getSpeed();
        this.baseBean.setSpeed(speed);
        this.firePropertyChange("speed", oldValue, speed);
    }

    /**
     * Gets the flag that marks the trajectory as relative. The reference position is the position
     * before the scan started.
     * 
     * @see ITrajectory#getRelative()
     * @return relative
     */
    @Override
    public Boolean getRelative() {
        return this.baseBean.getRelative();
    }

    /**
     * Sets the flag that marks the trajectory as relative. The reference position is the position
     * before the scan started.
     * 
     * @see ITrajectory#setRelative(Boolean)
     * @param Boolean
     */
    @Override
    public void setRelative(Boolean relative) {
        Boolean oldValue = this.baseBean.getRelative();
        this.baseBean.setRelative(relative);
        this.firePropertyChange("relative", oldValue, relative);
    }

    /**
     * Get the actuator.
     * 
     * @return
     */
    @Override
    public IActuator getActuator() {
        return baseBean.getActuator();
    }

    /**
     * Set the actuator.
     * 
     * @param actuator
     */
    @Override
    public void setActuator(IActuator actuator) {
        IActuator oldValue = baseBean.getActuator();
        baseBean.setActuator(actuator);
        this.firePropertyChange("actuator", oldValue, actuator);
    }

    /**
     * Set the delta constant.
     * 
     * @param deltaConstant
     */
    @Override
    public void setDeltaConstant(Boolean deltaConstant) {
        Boolean oldValue = baseBean.isDeltaConstant();
        baseBean.setDeltaConstant(deltaConstant);
        firePropertyChange("deltaConstant", oldValue, deltaConstant);
    }

    /**
     * Get the range.
     * 
     * @return
     */
    @Override
    public IRange2DY getRange() {
        return baseBean.getRange();
    }

    /**
     * Set the range.
     * 
     * @param range
     */
    @Override
    public void setRange(IRange2DY range) {
        IRange2DY oldValue = baseBean.getRange();
        baseBean.setRange(range);
        firePropertyChange("range", oldValue, range);
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setName(String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setTrajectory(double[] values) {
        if (baseBean != null) {
            double[] oldValue = this.baseBean.getTrajectory();
            this.baseBean.setTrajectory(values);
            this.firePropertyChange("trajectory", oldValue, values);
        }
    }

    @Override
    public double[] getTrajectory() {
        if (baseBean != null) {
            return baseBean.getTrajectory();
        }
        return null;
    }

    @Override
    public void setIRange(IRange range) {
        baseBean.setIRange(range);
    }

    @Override
    public IRange getIRange() {
        return baseBean.getIRange();
    }

    @Override
    public ITrajectory toModel() {
        return this;
    }

    @Override
    public void refreshDelta() {
        baseBean.refreshDelta();
    }

    @Override
    public void refreshStep() {
        baseBean.refreshStep();
    }

    @Override
    public void refreshEndPosition() {
        baseBean.refreshEndPosition();
    }

    @Override
    public Boolean isDeltaConstant() {
        return baseBean.isDeltaConstant();
    }

    @Override
    public Boolean getDeltaConstant() {
        return isDeltaConstant();
    }

    @Override
    public void setCustomTrajectory(boolean custom) {
        boolean oldValue = this.baseBean.isCustomTrajectory();
        this.baseBean.setCustomTrajectory(custom);
        this.firePropertyChange("customTrajectory", oldValue, custom);
    }

    @Override
    public boolean isCustomTrajectory() {
        return baseBean.isCustomTrajectory();
    }
}

package fr.soleil.salsa.entity;

import java.io.Serializable;

public interface IDevice extends Serializable {

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    public Integer getId();

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    public void setId(Integer id);

    /**
     * Gets the device name.
     * 
     * @return name
     */
    public String getName();

    /**
     * Sets the device name.
     * 
     * @param String
     */
    public void setName(String name);

    /**
     * Gets the device attribute name in scan server.
     * 
     * @return name
     */
    public String getScanServerAttributeName();

    /**
     * Set the device attribute name in scan server
     * 
     */
    public void setScanServerAttributeName(String scanServerAttributeName);

    /**
     * Gets true if the device is enabled.
     * 
     * @return
     */
    boolean isEnabled();

    /**
     * Sets true if the device is enabled.
     * 
     * @param enabled
     */
    void setEnabled(boolean enabled);

    boolean isCommon();

    void setCommon(boolean common);

    public IDevice toModel();

    public IDevice toImpl();

}

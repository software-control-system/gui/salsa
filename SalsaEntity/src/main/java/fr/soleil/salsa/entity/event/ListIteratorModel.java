package fr.soleil.salsa.entity.event;

import java.util.ListIterator;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;

public class ListIteratorModel<E, T extends IEventSource<?>> implements ListIterator<E> {

    /**
     * The decorated iterator.
     */
    protected ListIterator<E> baseIterator;

    /**
     * The actual entity that this collection is a property of.
     */
    protected T entity;

    /**
     * The property name.
     */
    protected String propertyName;

    /**
     * The constructor.
     * 
     * @param baseIterator the decorated iterator.
     * @param entity the actual entity that this collection is a property of.
     * @param propertyName the property name.
     */
    public ListIteratorModel(ListIterator<E> baseIterator, T entity, String propertyName) {
        super();
        this.entity = entity;
        this.propertyName = propertyName;
        this.baseIterator = baseIterator;
    }

    /**
     * @param o
     * @see java.util.ListIterator#add(java.lang.Object)
     */
    public void add(E o) {
        baseIterator.add(o);
        this.firePropertyChange();
    }

    /**
     * @return
     * @see java.util.ListIterator#hasNext()
     */
    public boolean hasNext() {
        return baseIterator.hasNext();
    }

    /**
     * @return
     * @see java.util.ListIterator#hasPrevious()
     */
    public boolean hasPrevious() {
        return baseIterator.hasPrevious();
    }

    /**
     * @return
     * @see java.util.ListIterator#next()
     */
    public E next() {
        return baseIterator.next();
    }

    /**
     * @return
     * @see java.util.ListIterator#nextIndex()
     */
    public int nextIndex() {
        return baseIterator.nextIndex();
    }

    /**
     * @return
     * @see java.util.ListIterator#previous()
     */
    public E previous() {
        return baseIterator.previous();
    }

    /**
     * @return
     * @see java.util.ListIterator#previousIndex()
     */
    public int previousIndex() {
        return baseIterator.previousIndex();
    }

    /**
     * 
     * @see java.util.ListIterator#remove()
     */
    public void remove() {
        baseIterator.remove();
        this.firePropertyChange();
    }

    /**
     * @param o
     * @see java.util.ListIterator#set(java.lang.Object)
     */
    public void set(E o) {
        baseIterator.set(o);
        this.firePropertyChange();
    }

    /**
     * Sends the EntityPropertyChangedEvent. Reminder : the EntityPropertyChangedEvent must be
     * called AFTER the property has been changed in the bean.
     * 
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    @SuppressWarnings("unchecked")
    protected void firePropertyChange() {
        EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                this.entity, this.propertyName, null, this);
        SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent,
                ((IEventSource<EntityPropertyChangedEvent<T>>) entity).getListeners());
    }
}

package fr.soleil.salsa.entity.event;

import java.sql.Timestamp;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.util.ComparatorUtil;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Abstract Event handling decorator of {@link IConfig} for the config entity.
 * This is the configuration data for a scan. This adds the "modified" property
 * over the ones already in IConfig.
 *
 * @author Administrateur
 */
public abstract class AConfigModel<T extends IDimension, C extends IConfig<T>> extends AEventHandlingModelDecorator<C>
        implements IConfig<T> {

    private static final long serialVersionUID = -3056443089924478832L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first
     * parameter.
     *
     * @param config
     *            the decorated base bean.
     */
    public AConfigModel(final C config) {
        super(config);
    }

    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    @Override
    public void setId(final Integer id) {
        Integer oldValue = this.baseBean.getId();
        if (oldValue != id) {
            this.baseBean.setId(id);
            this.firePropertyChange("id", oldValue, id);
        }
    }

    @Override
    public String getName() {
        return this.baseBean.getName();
    }

    @Override
    public void setName(final String name) {
        String oldValue = this.baseBean.getName();
        if (!ComparatorUtil.stringEquals(oldValue, name)) {
            this.baseBean.setName(name);
            // When it is a user renaming
            if (oldValue != null) {
                baseBean.setModified(true);
            }
            this.firePropertyChange("name", oldValue, name);
        }
    }

    @Override
    public ScanType getType() {
        return this.baseBean.getType();
    }

    @Override
    public void setType(final ScanType type) {
        ScanType oldValue = this.baseBean.getType();
        if (oldValue != type) {
            this.baseBean.setType(type);
            this.firePropertyChange("type", oldValue, type);
        }
    }

    @Override
    public boolean isLoaded() {
        return baseBean.isLoaded();
    }

    @Override
    public void setLoaded(final boolean loaded) {
        boolean oldValue = this.baseBean.isLoaded();
        if (oldValue != loaded) {
            this.baseBean.setLoaded(loaded);
            this.firePropertyChange("loaded", oldValue, loaded);
        }
    }

    @Override
    public IDirectory getDirectory() {
        return baseBean.getDirectory();
    }

    @Override
    public void setDirectory(final IDirectory directory) {
        IDirectory oldValue = this.baseBean.getDirectory();
        String oldName = null;
        String newName = null;
        if (oldValue != null) {
            oldName = oldValue.getFullPath();
        }
        if ((directory != null) && (oldValue != null)) {
            newName = directory.getFullPath();
        }
        this.baseBean.setDirectory(directory);
        if (!ComparatorUtil.stringEquals(oldName, newName)) {
            this.firePropertyChange("directory", oldName, newName);
            setModified(true);
        }
    }

    @Override
    public Integer getPositionInDirectory() {
        return this.baseBean.getPositionInDirectory();
    }

    @Override
    public void setPositionInDirectory(final Integer positionInDirectory) {
        Integer oldValue = this.baseBean.getPositionInDirectory();
        if (oldValue != positionInDirectory) {
            this.baseBean.setPositionInDirectory(positionInDirectory);
            this.firePropertyChange("positionInDirectory", oldValue, positionInDirectory);
            setModified(true);
        }
    }

    @Override
    public List<ISensor> getSensorsList() {
        return baseBean.getSensorsList();
    }

    @Override
    public List<IActuator> getActuatorList(final int dimension) {
        return baseBean.getActuatorList(dimension);
    }

    @Override
    public void setSensorsList(final List<ISensor> sensorsList) {
        List<ISensor> oldValue = this.baseBean.getSensorsList();
        this.baseBean.setSensorsList(sensorsList);
        this.firePropertyChange("sensorsList", oldValue, sensorsList);
        setModified(true);
    }

    @Override
    public boolean isModified() {
        return baseBean.isModified();
    }

    @Override
    public void setModified(final boolean modified) {
        boolean oldValue = this.baseBean.isModified();
        if (oldValue != modified) {
            baseBean.setModified(modified);
            this.firePropertyChange("modified", oldValue, modified);
        }
    }

    @Override
    public double getActuatorsDelay() {
        return this.baseBean.getActuatorsDelay();
    }

    @Override
    public void setActuatorsDelay(final double actuatorsDelay) {
        double oldValue = this.baseBean.getActuatorsDelay();
        if (actuatorsDelay != oldValue) {
            this.baseBean.setActuatorsDelay(actuatorsDelay);
            this.firePropertyChange("actuatorsDelay", oldValue, actuatorsDelay);
            setModified(true);
        }
    }

    @Override
    public double getTimebasesDelay() {
        return this.baseBean.getTimebasesDelay();
    }

    @Override
    public void setTimebasesDelay(final double timebasesDelay) {
        double oldValue = this.baseBean.getTimebasesDelay();
        if (timebasesDelay != oldValue) {
            this.baseBean.setTimebasesDelay(timebasesDelay);
            this.firePropertyChange("timebasesDelay", oldValue, timebasesDelay);
            setModified(true);
        }
    }

    @Override
    public int getScanNumber() {
        return this.baseBean.getScanNumber();
    }

    @Override
    public void setScanNumber(final int scanNumber) {
        Integer oldValue = this.baseBean.getScanNumber();
        if (scanNumber != oldValue) {
            this.baseBean.setScanNumber(scanNumber);
            this.firePropertyChange("scanNumber", oldValue, scanNumber);
            setModified(true);
        }
    }

    @Override
    public Timestamp getTimestamp() {
        return this.baseBean.getTimestamp();
    }

    @Override
    public long getLastModificationDate() {
        return baseBean.getLastModificationDate();
    }

    @Override
    public void setTimestamp(final Timestamp timestamp) {
        this.baseBean.setTimestamp(timestamp);
    }

    @Override
    public IScanAddOns getScanAddOn() {
        return this.baseBean.getScanAddOn();
    }

    @Override
    public void setScanAddOn(final IScanAddOns scanAddOn) {
        IScanAddOns oldValue = this.baseBean.getScanAddOn();
        if (!ComparatorUtil.scanAddOnsEquals(scanAddOn, oldValue)) {
            this.baseBean.setScanAddOn(scanAddOn);
            this.firePropertyChange("scanAddOn", oldValue, scanAddOn);
            setModified(true);
        }
    }

    @Override
    public String getDataRecorderConfig() {
        return this.baseBean.getDataRecorderConfig();
    }

    @Override
    public void setDataRecorderConfig(final String config) {
        String oldValue = this.baseBean.getDataRecorderConfig();
        if (!ComparatorUtil.stringEquals(oldValue, config)) {
            this.baseBean.setDataRecorderConfig(config);
            this.firePropertyChange("dataRecorderConfig", oldValue, config);
            setModified(true);
        }
    }

    @Override
    public boolean isOnTheFly() {
        return this.baseBean.isOnTheFly();
    }

    @Override
    public void setOnTheFly(final boolean onTheFly) {
        boolean oldValue = this.baseBean.isOnTheFly();
        if (onTheFly != oldValue) {
            this.baseBean.setOnTheFly(onTheFly);
            this.firePropertyChange("onTheFly", oldValue, onTheFly);
            setModified(true);
        }
    }

    @Override
    public boolean isZigzag() {
        return this.baseBean.isZigzag();
    }

    @Override
    public void setZigzag(final boolean zigzag) {
        boolean oldValue = this.baseBean.isZigzag();
        if (zigzag != oldValue) {
            this.baseBean.setZigzag(zigzag);
            this.firePropertyChange("zigzag", oldValue, zigzag);
            setModified(true);
        }
    }

    @Override
    public boolean isEnableScanSpeed() {
        return this.baseBean.isEnableScanSpeed();
    }

    @Override
    public void setEnableScanSpeed(final boolean enableScanSpeed) {
        boolean oldValue = this.baseBean.isEnableScanSpeed();
        if (enableScanSpeed != oldValue) {
            this.baseBean.setEnableScanSpeed(enableScanSpeed);
            this.firePropertyChange("enableScanSpeed", oldValue, enableScanSpeed);
            setModified(true);
        }
    }

    @Override
    public List<ITimebase> getTimebaseList() {
        return baseBean.getTimebaseList();
    }

    @Override
    public void setTimebaseList(final List<ITimebase> timebaseList) {
        List<ITimebase> oldValue = this.baseBean.getTimebaseList();
        this.baseBean.setTimebaseList(timebaseList);
        this.firePropertyChange("timebaseList", oldValue, timebaseList);
        setModified(true);
    }

    @Override
    public T getDimensionX() {
        T dimension = null;
        if (baseBean != null) {
            dimension = baseBean.getDimensionX();
        }
        return dimension;
    }

    @Override
    public List<IDimension> getDimensionList() {
        return baseBean.getDimensionList();
    }

    @Override
    public void setDimensionX(final T dimension) {
        if (baseBean != null) {
            T oldValue = this.baseBean.getDimensionX();
            baseBean.setDimensionX(dimension);
            this.firePropertyChange("dimensionX", oldValue, dimension);
        }
    }

    @Override
    public String getFullPath() {
        String path = null;
        if (baseBean != null) {
            path = baseBean.getFullPath();
        }
        return path;
    }

    @Override
    public boolean isValid() {
        boolean valid = false;
        if (baseBean != null) {
            valid = baseBean.isValid();
        }
        return valid;
    }

    @Override
    public boolean isForcedSaving() {
        return baseBean.isForcedSaving();
    }

    @Override
    public void setForcedSaving(final boolean forcedSaving) {
        if (baseBean != null) {
            baseBean.setForcedSaving(forcedSaving);
        }
    }

    @Override
    public void activateSensors(final String[] sensorList) throws SalsaException {
        if (baseBean != null) {
            List<ISensor> oldSensorList = getSensorsList();
            baseBean.activateSensors(sensorList);
            this.firePropertyChange("activateActuators", oldSensorList, sensorList);
            setModified(true);
        }
    }

    @Override
    public void activateActuators(final String[] actuatorList, final int dimension) throws SalsaException {
        if (baseBean != null) {
            List<IActuator> oldActuatorList = getActuatorList(dimension);
            baseBean.activateActuators(actuatorList, dimension);
            this.firePropertyChange("activateActuators", oldActuatorList, actuatorList);
            setModified(true);
        }
    }

    @Override
    public void activateTimebase(final String[] timebaseList) throws SalsaException {
        if (baseBean != null) {
            List<ITimebase> oldTimeBaseList = getTimebaseList();
            baseBean.activateTimebase(timebaseList);
            this.firePropertyChange("activateTimebase", oldTimeBaseList, timebaseList);
            setModified(true);
        }

    }

    @Override
    public void setSensorEnable(final String sensorName, final boolean enabled) {
        if (baseBean != null) {
            boolean oldValue = this.baseBean.isSensorEnable(sensorName);
            if (oldValue != enabled) {
                List<ISensor> oldSensorList = getSensorsList();
                baseBean.setSensorEnable(sensorName, enabled);
                this.firePropertyChange("setSensorEnable", oldSensorList, sensorName);
                setModified(true);
            }
        }
    }

    @Override
    public boolean isSensorEnable(final String sensorName) {
        if (baseBean != null) {
            return baseBean.isSensorEnable(sensorName);
        }
        return false;
    }

    @Override
    public void setActuatorEnable(final String actuatorName, final boolean enabled, final int dimension) {
        if (baseBean != null) {
            List<IActuator> oldActuatorList = getActuatorList(dimension);
            baseBean.setActuatorEnable(actuatorName, enabled, dimension);
            this.firePropertyChange("setActuatorEnable", oldActuatorList, actuatorName);
            setModified(true);
        }
    }

    @Override
    public boolean isActuatorEnable(final String actuatorName, final int dimension) {
        if (baseBean != null) {
            return baseBean.isActuatorEnable(actuatorName, dimension);
        }
        return false;
    }

    @Override
    public void setTimeBaseEnable(final String timeBaseName, final boolean enabled) {
        if (baseBean != null) {
            boolean oldValue = this.baseBean.isTimeBaseEnable(timeBaseName);
            if (oldValue != enabled) {
                List<ITimebase> oldTimeBaseList = getTimebaseList();
                baseBean.setTimeBaseEnable(timeBaseName, enabled);
                this.firePropertyChange("setTimeBaseEnable", oldTimeBaseList, timeBaseName);
                setModified(true);
            }
        }
    }

    @Override
    public boolean isTimeBaseEnable(final String timeBaseName) {
        if (baseBean != null) {
            return baseBean.isTimeBaseEnable(timeBaseName);
        }
        return false;
    }

    @Override
    public void addActuator(final String actuatorName, final int dimension) {
        if (baseBean != null) {
            List<IActuator> oldActuatorList = getActuatorList(dimension);
            baseBean.addActuator(actuatorName, dimension);
            List<IActuator> newActuatorList = getActuatorList(dimension);
            IDevice newDevice = getDevice(actuatorName, newActuatorList);
            String propertyName = "addActuator";
            if (dimension == SECOND_DIMENSION) {
                propertyName = propertyName + "Y";
            }
            this.firePropertyChange(propertyName, oldActuatorList, newDevice);
            setModified(true);
        }
    }

    @Override
    public void swapActuator(final String actuator1, final String actuator2, final int dimension) {
        if (baseBean != null) {
            baseBean.swapActuator(actuator1, actuator2, dimension);
            List<IActuator> newActuatorList = getActuatorList(dimension);
            String propertyName = "swapActuator";
            if (dimension == SECOND_DIMENSION) {
                propertyName = propertyName + "Y";
            }
            this.firePropertyChange(propertyName, null, newActuatorList);
            setModified(true);
        }
    }

    @Override
    public void swapSensor(final String sensorName1, final String sensorName2) {
        if (baseBean != null) {
            baseBean.swapSensor(sensorName1, sensorName2);
            List<ISensor> newSensorList = getSensorsList();
            String propertyName = "swapSensor";
            this.firePropertyChange(propertyName, null, newSensorList);
            setModified(true);
        }
    }

    @Override
    public void swapTimeBase(final String timeBase1, final String timeBase2) {
        if (baseBean != null) {
            baseBean.swapTimeBase(timeBase1, timeBase2);
            List<ITimebase> newTimebaseList = getTimebaseList();
            String propertyName = "swapTimebase";
            this.firePropertyChange(propertyName, null, newTimebaseList);
            setModified(true);
        }
    }

    @Override
    public void addSensor(final String sensorName) {
        if (baseBean != null) {
            List<ISensor> oldSensorList = getSensorsList();
            baseBean.addSensor(sensorName);
            this.firePropertyChange("addSensor", oldSensorList, sensorName);
            setModified(true);
        }
    }

    @Override
    public void deleteActuator(final String actuatorName, final int dimension) {
        if (baseBean != null) {
            List<IActuator> oldActuatorList = getActuatorList(dimension);
            IDevice deleteDevice = getDevice(actuatorName, oldActuatorList);
            baseBean.deleteActuator(actuatorName, dimension);
            this.firePropertyChange("deleteActuator", oldActuatorList, deleteDevice);
            setModified(true);
        }
    }

    @Override
    public void deleteSensor(final String sensorName) {
        if (baseBean != null) {
            List<ISensor> oldSensorList = getSensorsList();
            baseBean.deleteSensor(sensorName);
            this.firePropertyChange("deleteSensor", oldSensorList, sensorName);
            setModified(true);
        }
    }

    @Override
    public void addTimeBase(final String timebaseName) {
        if (baseBean != null) {
            List<ITimebase> oldTimeBaseList = getTimebaseList();
            baseBean.addTimeBase(timebaseName);
            this.firePropertyChange("addTimeBase", oldTimeBaseList, timebaseName);
            setModified(true);
        }
    }

    @Override
    public void deleteTimeBase(final String timebaseName) {
        if (baseBean != null) {
            List<ITimebase> oldTimeBaseList = getTimebaseList();
            baseBean.deleteTimeBase(timebaseName);
            this.firePropertyChange("deleteTimeBase", oldTimeBaseList, timebaseName);
            setModified(true);
        }
    }

    @Override
    public void renameActuator(final String oldActuatorName, final String newActuatorName, final int dimension) {
        if (baseBean != null) {
            baseBean.renameActuator(oldActuatorName, newActuatorName, dimension);
            this.firePropertyChange("renameActuator", oldActuatorName, newActuatorName);
            setModified(true);
        }
    }

    @Override
    public void renameSensor(final String oldSensorName, final String newSensorName) {
        if (baseBean != null) {
            baseBean.renameSensor(oldSensorName, newSensorName);
            this.firePropertyChange("renameSensor", oldSensorName, newSensorName);
            setModified(true);
        }
    }

    @Override
    public void renameTimeBase(final String oldTimeBaseName, final String newTimeBaseName) {
        if (baseBean != null) {
            baseBean.renameTimeBase(oldTimeBaseName, newTimeBaseName);
            this.firePropertyChange("renameTimeBase", oldTimeBaseName, newTimeBaseName);
            setModified(true);
        }
    }

    @Override
    public List<IDevice> getActivatedActuatorsList() {
        if (baseBean != null) {
            return baseBean.getActivatedActuatorsList();
        }
        return null;
    }

    @Override
    public List<IDevice> getActivatedSensorsList() {
        if (baseBean != null) {
            return baseBean.getActivatedSensorsList();
        }
        return null;
    }

    @Override
    public List<IDevice> getActivatedTimebasesList() {
        if (baseBean != null) {
            return baseBean.getActivatedTimebasesList();
        }
        return null;
    }

    @Override
    public String[] getScanInfo() {
        return baseBean.getScanInfo();
    }

    @Override
    public IDevice getDevice(final String deviceName, final List<? extends IDevice> deviceList) {
        return baseBean.getDevice(deviceName, deviceList);
    }

    @Override
    public String getRunName() {
        return baseBean.getRunName();
    }

    @Override
    public void setRunName(final String runName) {
        baseBean.setRunName(runName);
    }

    @Override
    public String toString() {
        String toStringValue = super.toString();
        String fullPath = getFullPath();
        if (SalsaUtils.isDefined(fullPath)) {
            toStringValue = fullPath;
        }
        return toStringValue;
    }

}

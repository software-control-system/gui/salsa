package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for the range entity. A range is a part of a dimension where the movements of the
 * actuators are linear. The trajectory list contains the details of the movement of each actuator
 * on this range.
 * 
 * @see fr.soleil.salsa.entity.ITrajectory
 * @see fr.soleil.salsa.entity.IDimension
 * @author Administrateur
 * 
 */
public interface IRange extends Serializable {

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    public Integer getId();

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    public void setId(Integer id);

    /**
     * Gets the number of steps between the starting and ending position.
     * 
     * @return stepsNumber
     */
    public Integer getStepsNumber();

    /**
     * Sets the number of steps between the starting and ending position.
     * 
     * @param Integer
     */
    public void setStepsNumber(Integer stepsNumber);

    /**
     * Gets the trajectories of the actuators on this range.
     * 
     * @return trajectoriesList
     */
    public List<ITrajectory> getTrajectoriesList();

    /**
     * Sets the trajectories of the actuators on this range.
     * 
     * @param List<ITrajectory>
     */
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList);

    /**
     * Gets the dimension this range is a part of.
     * 
     * @return dimension
     */
    public IDimension getDimension();

    /**
     * Sets the dimension this range is a part of.
     * 
     * @param IDimension
     */
    public void setDimension(IDimension dimension);

    public ITrajectory createTrajectory(IActuator actuator);

    public IRange toImpl();

    public void setStepNumberNoRefresh(Integer stepsNumber);
}

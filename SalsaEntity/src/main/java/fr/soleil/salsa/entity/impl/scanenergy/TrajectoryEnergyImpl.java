package fr.soleil.salsa.entity.impl.scanenergy;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.TrajectoryEnergyModel;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;

/**
 * @author Alike
 * 
 */
public class TrajectoryEnergyImpl extends TrajectoryImpl implements ITrajectoryEnergy {

    private static final long serialVersionUID = -2700860354694822786L;

    /**
     * The actuator.
     */
    private IActuator actuator = null;

    /**
     * The range.
     */
    private IRangeEnergy range = null;

    /**
     * Get the actuator.
     * 
     * @return
     */
    public IActuator getActuator() {
        return actuator;
    }

    /**
     * Set the actuator.
     * 
     * @param actuator
     */
    public void setActuator(IActuator actuator) {
        this.actuator = actuator;
    }

    /**
     * Get the range.
     * 
     * @return
     */
    public IRangeEnergy getRange() {
        return range;
    }

    /**
     * Set the range.
     * 
     * @param range
     */
    public void setRange(IRangeEnergy range) {
        this.range = range;
    }

    protected ITrajectory initModel() {
        return new TrajectoryEnergyModel(this);
    }
}

package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IReadWriteAccess;

/**
 * @author Alike
 * 
 */
public class ReadWriteAccessImpl implements IReadWriteAccess {

    private static final long serialVersionUID = 7022465697400464799L;

    /**
     * True if readable.
     */
    private boolean readable;

    /**
     * True if writable.
     */
    private boolean writable;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IReadWriteAccess#isReadable()
     */
    public boolean isReadable() {
        return readable;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IReadWriteAccess#setReadable(boolean)
     */
    public void setReadable(boolean readable) {
        this.readable = readable;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IReadWriteAccess#isWritable()
     */
    public boolean isWritable() {
        return writable;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IReadWriteAccess#setWritable(boolean)
     */
    public void setWritable(boolean writable) {
        this.writable = writable;
    }

}

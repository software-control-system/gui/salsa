package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.impl.scanhcs.ConfigHCSImpl;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;

public class ConfigHCSModel extends AConfigModel<IDimensionHCS, IConfigHCS> implements IConfigHCS {

    private static final long serialVersionUID = 4546568846754313581L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param configHCS the decorated base bean.
     */
    public ConfigHCSModel(IConfigHCS configHCS) {
        super(configHCS);
    }

    /**
     * Default constructor, that creates a new instance of {@link ConfigHCSImpl} and wraps it.
     */
    public ConfigHCSModel() {
        this(new ConfigHCSImpl());
    }

}

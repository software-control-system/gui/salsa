package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.tool.SalsaUtils;

public class ScanStatusImpl implements IScanStatus {

    private static final long serialVersionUID = 1116087636570968743L;

    private String state;
    private String status;

    private String runStartDate;
    private String scanStartDate;
    private String scanEndDate;
    private String runEndDate;
    private String scanDuration;
    private String runDuration;
    private String scanRemainingTime;
    private String runRemainingTime;
    private String scanElapsed;
    private String runElapsed;
    private double runCompletion;
    private double scanCompletion;

    private double deadTime;
    private double deadTimePercentage;
    private double deadTimePerPoint;

    @Override
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getRunStartDate() {
        return runStartDate;
    }

    public void setRunStartDate(String runStartDate) {
        this.runStartDate = runStartDate;
    }

    @Override
    public String getScanStartDate() {
        return scanStartDate;
    }

    public void setScanStartDate(String scanStartDate) {
        this.scanStartDate = scanStartDate;
    }

    @Override
    public String getScanEndDate() {
        return scanEndDate;
    }

    public void setScanEndDate(String scanEndDate) {
        this.scanEndDate = scanEndDate;
    }

    @Override
    public String getRunEndDate() {
        return runEndDate;
    }

    public void setRunEndDate(String runEndDate) {
        this.runEndDate = runEndDate;
    }

    @Override
    public String getScanDuration() {
        return scanDuration;
    }

    public void setScanDuration(String scanDuration) {
        this.scanDuration = scanDuration;
    }

    @Override
    public String getRunDuration() {
        return runDuration;
    }

    public void setRunDuration(String runDuration) {
        this.runDuration = runDuration;
    }

    @Override
    public String getScanRemainingTime() {
        return scanRemainingTime;
    }

    public void setScanRemainingTime(String scanRemainingTime) {
        this.scanRemainingTime = scanRemainingTime;
    }

    @Override
    public String getRunRemainingTime() {
        return runRemainingTime;
    }

    public void setRunRemainingTime(String runRemainingTime) {
        this.runRemainingTime = runRemainingTime;
    }

    @Override
    public String getScanElapsed() {
        return scanElapsed;
    }

    public void setScanElapsed(String scanElapsed) {
        this.scanElapsed = scanElapsed;
    }

    @Override
    public String getRunElapsed() {
        return runElapsed;
    }

    public void setRunElapsed(String runElapsed) {
        this.runElapsed = runElapsed;
    }

    @Override
    public double getRunCompletion() {
        return runCompletion;
    }

    public void setRunCompletion(double runCompletion) {
        this.runCompletion = runCompletion;
    }

    @Override
    public double getScanCompletion() {
        return scanCompletion;
    }

    public void setScanCompletion(double scanCompletion) {
        this.scanCompletion = scanCompletion;
    }

    @Override
    public double getDeadTime() {
        return deadTime;
    }

    public void setDeadTime(double deadTime) {
        this.deadTime = deadTime;
    }

    @Override
    public double getDeadTimePercentage() {
        return deadTimePercentage;
    }

    public void setDeadTimePercentage(double deadTimePercentage) {
        this.deadTimePercentage = deadTimePercentage;
    }

    @Override
    public double getDeadTimePerPoint() {
        return deadTimePerPoint;
    }

    public void setDeadTimePerPoint(double deadTimePerPoint) {
        this.deadTimePerPoint = deadTimePerPoint;
    }

    @Override
    public String toString() {
        String toStringValue = super.toString();
        StringBuilder stringBuilder = new StringBuilder();
        if (getState() != null) {
            stringBuilder.append("State=" + getState());
        }
        if (getStatus() != null) {
            stringBuilder.append("Status=" + getStatus());
        }
        String tmp = stringBuilder.toString();
        if (SalsaUtils.isDefined(tmp)) {
            toStringValue = tmp;
        }
        return toStringValue;
    }

}

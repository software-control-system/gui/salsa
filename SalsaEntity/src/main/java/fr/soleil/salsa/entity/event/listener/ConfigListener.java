package fr.soleil.salsa.entity.event.listener;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.source.IEventSource;

public class ConfigListener implements IConfigListener,
        IListener<EntityPropertyChangedEvent<IConfig<?>>> {

    private IConfigListener listener = null;

    @SuppressWarnings("rawtypes")
    private IEventSource config = null;

    public ConfigListener(IConfigListener listener) {
        this.listener = listener;
    }

    @Override
    public void notifyEvent(EntityPropertyChangedEvent<IConfig<?>> event) {
        List<String> propertyNameList = new ArrayList<String>();
        if (event != null) {
            List<EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?>> propertyList = event
                    .getPropertyChangeList();
            for (EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?> pc : propertyList) {
                propertyNameList.add(pc.getPropertyName());
            }
        }
        if (isConfigChanged(propertyNameList)) {
            refresh();
        }
    }

    @SuppressWarnings("unchecked")
    public void startListening(IConfig<?> aconfig) {
        if (aconfig instanceof IEventSource) {
            this.config = (IEventSource) aconfig;
        }
        if (listener != null && config != null) {
            config.addListener(this);
        }
    }

    @SuppressWarnings("unchecked")
    public void stopListening() {
        if (listener != null && config != null) {
            config.removeListener(this);
        }
    }

    @Override
    public void refresh() {
        if (listener != null) {
            listener.refresh();
        }
    }

    @Override
    public boolean isConfigChanged(List<String> propertyNameList) {
        if (listener != null) {
            return listener.isConfigChanged(propertyNameList);
        }
        return false;
    }

}

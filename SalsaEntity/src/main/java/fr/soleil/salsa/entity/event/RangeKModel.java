package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scank.RangeKImpl;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;

public class RangeKModel extends AEventHandlingModelDecorator<IRangeK> implements IRangeK {

    private static final long serialVersionUID = -3502846578064953886L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param rangeK the decorated base bean.
     */
    public RangeKModel(IRangeK rangeK) {
        super(rangeK);
    }

    /**
     * Default constructor, that creates a new instance of RangeKImpl and wraps it.
     */
    public RangeKModel() {
        this(new RangeKImpl());
    }

    @Override
    public ITrajectoryK getTrajectory() {
        return this.baseBean.getTrajectory();
    }

    @Override
    public void setTrajectory(ITrajectoryK trajectory) {
        ITrajectoryK oldValue = this.baseBean.getTrajectory();
        this.baseBean.setTrajectory(trajectory);
        this.firePropertyChange("trajectory", oldValue, trajectory);
    }

    /**
     * 
     */
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * 
     */
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * 
     */
    public Integer getStepsNumber() {
        return this.baseBean.getStepsNumber();
    }

    /**
     * 
     */
    public void setStepsNumber(Integer stepsNumber) {
        Integer oldValue = this.baseBean.getStepsNumber();
        this.baseBean.setStepsNumber(stepsNumber);
        this.firePropertyChange("stepsNumber", oldValue, stepsNumber);
    }

    /**
     * 
     */
    public List<ITrajectory> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    /**
     * 
     */
    public IDimension getDimension() {
        return baseBean.getDimension();
    }

    /**
     * 
     */
    public void setDimension(IDimension dimension) {
        IDimension oldValue = this.baseBean.getDimension();
        this.baseBean.setDimension(dimension);
        this.firePropertyChange("dimension", oldValue, dimension);
    }

    /**
     * 
     */
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        List<ITrajectory> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectoriesList);
        this.firePropertyChange("trajectoriesList", oldValue, trajectoriesList);
    }

    @Override
    public ITrajectory createTrajectory(IActuator actuator) {
        return this.baseBean.createTrajectory(actuator);
    }

    @Override
    public void setStepNumberNoRefresh(Integer stepsNumber) {
        this.baseBean.setStepNumberNoRefresh(stepsNumber);
    }

}

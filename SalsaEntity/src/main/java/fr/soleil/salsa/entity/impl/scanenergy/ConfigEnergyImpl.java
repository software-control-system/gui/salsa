package fr.soleil.salsa.entity.impl.scanenergy;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.ConfigEnergyModel;
import fr.soleil.salsa.entity.impl.ConfigImpl;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;

/**
 * @author Alike
 *
 */
public class ConfigEnergyImpl extends ConfigImpl<IDimensionEnergy> implements IConfigEnergy {

    private static final long serialVersionUID = -1385091108687927227L;

    public ConfigEnergyImpl() {
        super();
        setType(ScanType.SCAN_ENERGY);
    }

    @Override
    protected IConfig<IDimensionEnergy> initModel() {
        ConfigEnergyModel configEnergyDModel = new ConfigEnergyModel(this);

        IDimensionEnergy dimension = getDimensionX();
        if (dimension != null && dimension instanceof IObjectImpl<?>) {
            IDimensionEnergy dimensionModel = (IDimensionEnergy) ((IObjectImpl<?>) dimension)
                    .toModel();
            List<IActuator> actuatorList = dimension.getActuatorsList();
            List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
            dimensionModel.setActuatorsList(actuatorListModel);
            configEnergyDModel.setDimensionX(dimensionModel);
        }
        return configEnergyDModel;
    }



}

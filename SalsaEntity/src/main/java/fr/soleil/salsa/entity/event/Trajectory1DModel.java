package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scan1d.Trajectory1DImpl;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;

/**
 * Event handling decorator of {@link ITrajectory1D} for the trajectory1D entity.
 * 
 */
public class Trajectory1DModel extends AEventHandlingModelDecorator<ITrajectory1D> implements ITrajectory1D {

    private static final long serialVersionUID = -4438172617318703793L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param trajectory1D the decorated base bean.
     */
    public Trajectory1DModel(ITrajectory1D trajectory1D) {
        super(trajectory1D);
    }

    /**
     * Default constructor, that creates a new instance of Trajectory1DImpl and wraps it.
     */
    public Trajectory1DModel() {
        this(new Trajectory1DImpl());
    }

    /**
     * 
     */
    @Override
    public IActuator getActuator() {
        return this.baseBean.getActuator();
    }

    /**
     * 
     */
    @Override
    public void setActuator(IActuator actuator) {
        IActuator oldValue = this.baseBean.getActuator();
        this.baseBean.setActuator(actuator);
        this.firePropertyChange("actuator", oldValue, actuator);
    }

    /**
     * 
     */
    @Override
    public Boolean isDeltaConstant() {
        return this.baseBean.isDeltaConstant();
    }

    /**
     * 
     */
    @Override
    public void setDeltaConstant(Boolean deltaConstant) {
        Boolean oldValue = this.baseBean.isDeltaConstant();
        this.baseBean.setDeltaConstant(deltaConstant);
        this.firePropertyChange("deltaConstant", oldValue, deltaConstant);
    }

    /**
     * 
     */
    @Override
    public IRange1D getRange() {
        return this.baseBean.getRange();
    }

    /**
     * 
     */
    @Override
    public void setRange(IRange1D range) {
        IRange1D oldValue = this.baseBean.getRange();
        this.baseBean.setRange(range);
        this.firePropertyChange("range", oldValue, range);
    }

    /**
     * 
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * 
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * 
     */
    @Override
    public double getBeginPosition() {
        return this.baseBean.getBeginPosition();
    }

    /**
     * 
     */
    @Override
    public void setBeginPosition(double beginPosition) {
        double oldValue = this.baseBean.getBeginPosition();
        this.baseBean.setBeginPosition(beginPosition);
        this.firePropertyChange("beginPosition", oldValue, beginPosition);
    }

    /**
     * 
     */
    @Override
    public double getEndPosition() {
        return this.baseBean.getEndPosition();
    }

    /**
     * 
     */
    @Override
    public void setEndPosition(double endPosition) {
        double oldValue = this.baseBean.getEndPosition();
        this.baseBean.setEndPosition(endPosition);
        this.firePropertyChange("endPosition", oldValue, endPosition);
    }

    /**
     * 
     */
    @Override
    public double getDelta() {
        return this.baseBean.getDelta();
    }

    /**
     * 
     */
    @Override
    public void setDelta(double delta) {
        double oldValue = this.baseBean.getDelta();
        this.baseBean.setDelta(delta);
        this.firePropertyChange("delta", oldValue, delta);
    }

    /**
     * 
     */
    @Override
    public double getSpeed() {
        return this.baseBean.getSpeed();
    }

    /**
     * 
     */
    @Override
    public void setSpeed(double speed) {
        double oldValue = this.baseBean.getSpeed();
        this.baseBean.setSpeed(speed);
        this.firePropertyChange("speed", oldValue, speed);
    }

    /**
     * 
     */
    @Override
    public Boolean getRelative() {
        return this.baseBean.getRelative();
    }

    /**
     * 
     */
    @Override
    public void setRelative(Boolean relative) {
        Boolean oldValue = this.baseBean.getRelative();
        if (oldValue != relative) {
            this.baseBean.setRelative(relative);
            this.firePropertyChange("relative", oldValue, relative);
        }
    }

    /**
     * Delegates the equals method to the base bean. The base bean must check equality using only
     * their base interface, not their actual implementation class.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        return this.baseBean.equals(obj);
    }

    /**
     * Delegates the hashCode method to the base bean.
     * 
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.baseBean.hashCode();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String id) {
    }

    @Override
    public void setTrajectory(double[] values) {
        if (baseBean != null) {
            double[] oldValue = this.baseBean.getTrajectory();
            this.baseBean.setTrajectory(values);
            this.firePropertyChange("trajectory", oldValue, values);
        }
    }

    @Override
    public double[] getTrajectory() {
        if (baseBean != null) {
            return baseBean.getTrajectory();
        }
        return null;
    }

    @Override
    public ITrajectory toModel() {
        return this;
    }

    @Override
    public void setIRange(IRange range) {
        baseBean.setIRange(range);
    }

    @Override
    public IRange getIRange() {
        return baseBean.getIRange();
    }

    @Override
    public void refreshDelta() {
        baseBean.refreshDelta();
    }

    @Override
    public void refreshStep() {
        baseBean.refreshStep();
    }

    @Override
    public void refreshEndPosition() {
        baseBean.refreshEndPosition();
    }

    @Override
    public void setCustomTrajectory(boolean custom) {
        boolean oldValue = this.baseBean.isCustomTrajectory();
        this.baseBean.setCustomTrajectory(custom);
        this.firePropertyChange("customTrajectory", oldValue, custom);
    }

    @Override
    public boolean isCustomTrajectory() {
        return baseBean.isCustomTrajectory();
    }

}

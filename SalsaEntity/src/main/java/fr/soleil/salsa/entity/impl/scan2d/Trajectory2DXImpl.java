package fr.soleil.salsa.entity.impl.scan2d;

import fr.soleil.salsa.entity.IRangeTrajectory;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.Trajectory2DXModel;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;

/**
 * @author Alike
 * 
 */
public class Trajectory2DXImpl extends Trajectory2DImpl implements ITrajectory2DX,
IRangeTrajectory<IRange2DX> {

    private static final long serialVersionUID = -5169655586678012790L;

    /**
     * The range.
     */
    private IRange2DX range;

    /**
     * Get the range.
     * 
     * @return
     */
    public IRange2DX getRange() {
        return range;
    }

    /**
     * Set the range.
     * 
     * @param range
     */
    public void setRange(IRange2DX range) {
        this.range = range;
    }


    @Override
    protected ITrajectory initModel() {
        return new Trajectory2DXModel(this);
    }

    @Override
    public Boolean getDeltaConstant() {
        return isDeltaConstant();
    }
}

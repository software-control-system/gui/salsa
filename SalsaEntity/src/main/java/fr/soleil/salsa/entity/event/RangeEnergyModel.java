package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scanenergy.RangeEnergyImpl;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;

/**
 * Event handling decorator of {@link IRangeEnergy} for the rangeEnergy entity.
 * 
 */
public class RangeEnergyModel extends AEventHandlingModelDecorator<IRangeEnergy> implements IRangeEnergy {

    private static final long serialVersionUID = -5978109073481810300L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param range1D the decorated base bean.
     */
    public RangeEnergyModel(IRangeEnergy rangeEnergy) {
        super(rangeEnergy);
    }

    /**
     * Default constructor, that creates a new instance of Range1DImpl and wraps it.
     */
    public RangeEnergyModel() {
        this(new RangeEnergyImpl());
    }

    @Override
    public double[] getIntegrationTime() {
        return this.baseBean.getIntegrationTime();
    }

    @Override
    public void setIntegrationTime(double[] integrationTime) {
        double[] oldValue = this.baseBean.getIntegrationTime();
        this.baseBean.setIntegrationTime(integrationTime);
        this.firePropertyChange("integrationTime", oldValue, integrationTime);

    }

    @Override
    public IDimension getDimension() {
        return baseBean.getDimension();
    }

    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    @Override
    public Integer getStepsNumber() {
        return this.baseBean.getStepsNumber();
    }

    @Override
    public List<ITrajectory> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    @Override
    public void setDimension(IDimension dimension) {
        IDimension oldValue = this.baseBean.getDimension();
        this.baseBean.setDimension(dimension);
        this.firePropertyChange("dimension", oldValue, dimension);

    }

    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    @Override
    public void setStepsNumber(Integer stepsNumber) {
        Integer oldValue = this.baseBean.getStepsNumber();
        this.baseBean.setStepsNumber(stepsNumber);
        this.firePropertyChange("stepsNumber", oldValue, stepsNumber);
    }

    @Override
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        List<ITrajectory> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectoriesList);
        this.firePropertyChange("trajectoriesList", oldValue, trajectoriesList);

    }

    @Override
    public ITrajectory createTrajectory(IActuator actuator) {
        return this.baseBean.createTrajectory(actuator);
    }

    @Override
    public void setStepNumberNoRefresh(Integer stepsNumber) {
        this.baseBean.setStepNumberNoRefresh(stepsNumber);
    }
}

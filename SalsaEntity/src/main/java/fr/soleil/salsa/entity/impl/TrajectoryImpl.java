package fr.soleil.salsa.entity.impl;

import java.util.Arrays;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IRangeIntegrated;
import fr.soleil.salsa.entity.IRangeTrajectory;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.TrajectoryModel;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.TrajectoryUtil;

/**
 * Implementation of {@link ITrajectory} for the trajectory entity. Describes
 * the movement of an actuator to be performed during a linear part (called
 * range) of a scan.
 *
 * @see fr.soleil.salsa.entity.IRange
 * @see fr.soleil.salsa.entity.IDimension
 * @author Administrateur
 *
 */
public class TrajectoryImpl implements ITrajectory {

    private static final long serialVersionUID = -4231935889346180285L;

    private ITrajectory model = null;

    private double[] trajectory = null;
    private boolean firstCall = true;

    /**
     * The unique identifier.
     */
    private Integer id = 0;

    /**
     * The name.
     */
    private String name = null;

    /**
     * The initial position.
     */
    private double beginPosition = 0;

    /**
     * The final position.
     */
    private double endPosition = 0;

    /**
     * The position difference between two consecutive steps. It is always
     * positive. For instance, the delta of trajectory 6, 8, 10, 12 is 2.
     */
    private double delta = 0;

    /**
     * The actuator movement speed. It is always positive.
     */
    private double speed = 0;

    /**
     * The flag that marks the trajectory as relative. The reference position is
     * the position before the scan started.
     */
    private Boolean relative = false;

    /**
     * The delta constant.
     */
    private Boolean deltaConstant = false;

    /**
     * Custom trajectory
     */
    private boolean customTrajectory = false;

    /**
     * Constructor.
     */
    public TrajectoryImpl() {
        super();
    }

    /**
     * Gets the unique identifier.
     *
     * @return id
     */
    @Override
    public Integer getId() {
        return id;
    }

    /**
     * Sets the unique identifier.
     *
     * @param Integer
     */
    @Override
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     */
    @Override
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the initial position.
     *
     * @return beginPosition
     */
    @Override
    public double getBeginPosition() {
        return beginPosition;
    }

    /**
     * Sets the initial position.
     *
     * @param double
     */
    @Override
    public void setBeginPosition(final double beginPosition) {
        this.beginPosition = beginPosition;
        if (deltaConstant) {
            refreshStep();
        } else {
            refreshDelta();
        }
    }

    @Override
    public void refreshDelta() {
        if (this instanceof IRangeTrajectory<?>) {
            IRange range = ((IRangeTrajectory<?>) this).getRange();
            if (range != null) {
                int nbOfSteps = range.getStepsNumber();
                if (nbOfSteps != 0) {
                    double adelta = TrajectoryUtil.calculDelta(beginPosition, endPosition, nbOfSteps);
                    this.delta = adelta;
                }
            }
        }

    }

    @Override
    public void refreshStep() {
        if (this instanceof IRangeTrajectory<?>) {
            IRange range = ((IRangeTrajectory<?>) this).getRange();
            if (range != null) {
                int nbOfSteps = TrajectoryUtil.calculNbStep(beginPosition, endPosition, delta);
                endPosition = TrajectoryUtil.calculEndPosition(beginPosition, endPosition, nbOfSteps, delta);
                range.setStepsNumber(nbOfSteps);
            }
        }
    }

    public void refreshIntegrationTime() {
        if (this instanceof IRangeTrajectory<?>) {
            IRange range = ((IRangeTrajectory<?>) this).getRange();
            if (range instanceof IRangeIntegrated) {
                IRangeIntegrated rangeIntegrated = (IRangeIntegrated) range;
                double[] integrationTimes = rangeIntegrated.getIntegrationTime();
                if (integrationTimes != null && integrationTimes.length > 0) {
                    double[] newIntegrationTimes = new double[range.getStepsNumber() + 1];
                    Arrays.fill(newIntegrationTimes, integrationTimes[0]);
                    rangeIntegrated.setIntegrationTime(newIntegrationTimes);
                }
            }
        }
    }

    // @Override
    @Override
    public void refreshEndPosition() {
        if (this instanceof IRangeTrajectory<?>) {
            IRange range = ((IRangeTrajectory<?>) this).getRange();
            if (range != null) {
                int nbOfSteps = range.getStepsNumber();
                endPosition = TrajectoryUtil.calculEndPosition(beginPosition, endPosition, nbOfSteps, delta);
            }
        }
    }

    /**
     * Gets the final position.
     *
     * @return endPosition
     */
    @Override
    public double getEndPosition() {
        return endPosition;
    }

    /**
     * Sets the final position.
     *
     * @param double
     */
    @Override
    public void setEndPosition(final double endPosition) {
        this.endPosition = endPosition;
        if (deltaConstant) {
            refreshStep();
        } else {
            refreshDelta();
        }
    }

    /**
     * Gets the position difference between two consecutive steps. It is always
     * positive. For instance, the delta of trajectory 6, 8, 10, 12 is 2.
     *
     * @return delta
     */
    @Override
    public double getDelta() {
        return delta;
    }

    /**
     * Sets the position difference between two consecutive steps. It is always
     * positive. For instance, the delta of trajectory 6, 8, 10, 12 is 2.
     *
     * @param double
     */
    @Override
    public void setDelta(final double delta) {
        this.delta = delta;
        refreshStep();
    }

    /**
     * Gets the actuator movement speed. It is always positive.
     *
     * @return speed
     */
    @Override
    public double getSpeed() {
        if (this instanceof IRangeTrajectory<?>) {
            IRange range = ((IRangeTrajectory<?>) this).getRange();
            if ((range != null) && (range instanceof IRangeIntegrated)) {
                // Speed is calculate Mantis bug 23263
                double totalDistance = Math.abs(endPosition - beginPosition);
                double[] integrationTime = ((IRangeIntegrated) range).getIntegrationTime();
                double totalTime = 0;
                if (integrationTime != null) {
                    totalTime = TrajectoryUtil.getTotalIntegrationTime(integrationTime);
                }

                if (totalTime != 0) {
                    speed = totalDistance / totalTime;
                }
            }
        }

        return speed;
    }

    /**
     * Sets the actuator movement speed. It is always positive.
     *
     * @param double
     */
    @Override
    public void setSpeed(final double speed) {
        this.speed = speed;
    }

    /**
     * Gets the flag that marks the trajectory as relative. The reference
     * position is the position before the scan started.
     *
     * @return relative
     */
    @Override
    public Boolean getRelative() {
        return relative;
    }

    /**
     * Sets the flag that marks the trajectory as relative. The reference
     * position is the position before the scan started.
     *
     * @param Boolean
     */
    @Override
    public void setRelative(final Boolean relative) {
        if (relative != null) {
            this.relative = relative;
        }
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.ITrajectory1D#isDeltaConstant()
     */
    @Override
    public Boolean isDeltaConstant() {
        return deltaConstant;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.ITrajectory1D#setDeltaConstant(java.lang.Boolean)
     */
    @Override
    public void setDeltaConstant(final Boolean deltaConstant) {
        if (deltaConstant != null) {
            this.deltaConstant = deltaConstant;
        }
    }

    @Override
    public void setCustomTrajectory(final boolean custom) {
        customTrajectory = custom;

    }

    @Override
    public boolean isCustomTrajectory() {
        return customTrajectory;
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj
     * parameter is an instance of the base interface : another implementation
     * of the interface is considered equal to this one if it represents the
     * same entity.
     *
     * @see Object#equals(Object)
     * @param obj
     *            the object with which to compare.
     */
    @Override
    public boolean equals(final Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        } else if (obj == null) {
            equals = false;
        } else if (!(obj instanceof ITrajectory)) {
            equals = false;
        } else if (this.id == null) {
            // Approximation. The equals method of a bean that has not received
            // an identifier yet
            // should not be called.
            equals = false;
        } else {
            equals = this.id.equals(((ITrajectory) obj).getId());
        }
        return equals;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }

    @Override
    public void setTrajectory(final double[] values) {
        trajectory = values;
        if ((trajectory != null) && (getIRange() != null)) {
            getIRange().setStepsNumber(trajectory.length - 1);
            refreshIntegrationTime();
        }
    }

    @Override
    public double[] getTrajectory() {
        if (firstCall) {
            firstCall = false;
        } else if ((trajectory == null) && (this instanceof IRangeTrajectory<?>) && customTrajectory) {
            IRange range = ((IRangeTrajectory<?>) this).getRange();
            if (range != null) {
                int nbStep = range.getStepsNumber();
                if (nbStep != 0) {
                    trajectory = TrajectoryUtil.calculateLinearTrajectory(0, beginPosition, endPosition, nbStep, false);
                }
            }
        }
        return trajectory;
    }

    @Override
    public ITrajectory toModel() {
        if (model == null) {
            model = initModel();
        }
        return model;
    }

    protected ITrajectory initModel() {
        return new TrajectoryModel(this);
    }

    @Override
    public ITrajectory toImpl() {
        return this;
    }

    @Override
    public void setIRange(final IRange range) {
        if (range != null) {
            if ((range instanceof IRange1D) && (this instanceof ITrajectory1D)) {
                ((ITrajectory1D) this).setRange((IRange1D) range);
            } else if ((range instanceof IRange2DX) && (this instanceof ITrajectory2DX)) {
                ((ITrajectory2DX) this).setRange((IRange2DX) range);
            } else if ((range instanceof IRange2DY) && (this instanceof ITrajectory2DY)) {
                ((ITrajectory2DY) this).setRange((IRange2DY) range);
            } else if ((range instanceof IRangeEnergy) && (this instanceof ITrajectoryEnergy)) {
                ((ITrajectoryEnergy) this).setRange((IRangeEnergy) range);
            } else if ((range instanceof IRangeHCS) && (this instanceof ITrajectoryHCS)) {
                ((ITrajectoryHCS) this).setRange((IRangeHCS) range);
            } else if ((range instanceof IRangeK) && (this instanceof ITrajectoryK)) {
                ((ITrajectoryK) this).setRange((IRangeK) range);
            }
        }
    }

    @Override
    public IRange getIRange() {
        IRange range = null;
        if (this instanceof IRangeTrajectory<?>) {
            range = ((IRangeTrajectory<?>) this).getRange();
        }
        return range;
    }

    // Default implementation
    @Override
    public IActuator getActuator() {
        return null;
    }

}

package fr.soleil.salsa.entity.impl.scank;

import java.util.List;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.RangeKModel;
import fr.soleil.salsa.entity.impl.RangeImpl;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.tool.SalsaUtils;

public class RangeKImpl extends RangeImpl implements IRangeK {

    private static final long serialVersionUID = -671208287349754851L;

    /**
     * The K trajectory.
     */
    private ITrajectoryK trajectory;

    public RangeKImpl() {
        super();
        trajectory = new TrajectoryKImpl();
        trajectory.setRange(this);
        if (trajectoriesList != null) {
            trajectoriesList.add(trajectory);
        }
    }

    /**
     * Gets the K trajectory.
     */
    @Override
    public ITrajectoryK getTrajectory() {
        return trajectory;
    }

    /**
     * Sets the K trajectory.
     */
    @Override
    public void setTrajectory(ITrajectoryK trajectory) {
        this.trajectory = trajectory;
    }

    @Override
    protected IRange initModel() {
        return new RangeKModel(this);
    }

    @Override
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        super.setTrajectoriesList(trajectoriesList);
        if (SalsaUtils.isFulfilled(trajectoriesList)) {
            ITrajectory atrajectory = trajectoriesList.get(0);
            if (atrajectory instanceof ITrajectoryK) {
                trajectory = (ITrajectoryK) atrajectory;
            }
        }
    }

}

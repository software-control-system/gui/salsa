package fr.soleil.salsa.entity;

/**
 * The scan state.
 */
public enum ScanState {
    STOPPED, RUNNING, PAUSED, ABORT
}

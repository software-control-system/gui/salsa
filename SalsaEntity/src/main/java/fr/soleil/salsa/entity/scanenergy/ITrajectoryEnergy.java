package fr.soleil.salsa.entity.scanenergy;

import fr.soleil.salsa.entity.IActuatorTrajectory;
import fr.soleil.salsa.entity.IRangeTrajectory;

public interface ITrajectoryEnergy extends IActuatorTrajectory, IRangeTrajectory<IRangeEnergy> {

}

package fr.soleil.salsa.entity.event.listener;

import java.util.List;

public interface IConfigListener {

    public void refresh();

    public boolean isConfigChanged(List<String> propertyNameList);

}

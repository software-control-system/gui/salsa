package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Implementation of {@link IScanResult} for the scanResult entity. The result of a scan.
 */
public class ScanResultImpl implements IScanResult {

    private static final long serialVersionUID = 7664673492590273349L;

    private String scanServer = null;

    /**
     * The name of the current scan.
     */
    private String runName = null;

    /**
     * The list of actuators used in the scan on the X dimension.
     */
    private List<IActuator> actuatorsXList = new ArrayList<IActuator>();

    /**
     * The list of sensors used in the scan.
     */
    private List<ISensor> sensorsList = new ArrayList<ISensor>();

    /**
     * The type of scan.
     */
    private ResultType resultType;

    Map<IActuator, double[]> trajectoriesMap = null;

    /**
     * Constructor.
     */
    public ScanResultImpl() {
        super();
    }

    /**
     * The name of the current scan.
     * 
     * @return the runName
     */
    @Override
    public String getRunName() {
        return runName;
    }

    /**
     * The name of the current scan.
     * 
     * @param runName the runName to set
     */
    @Override
    public void setRunName(String runName) {
        this.runName = runName;
    }

    /**
     * Gets the list of actuators used in the scan on the X dimension.
     * 
     * @return actuatorsList
     */
    @Override
    public List<IActuator> getActuatorsXList() {
        return actuatorsXList;
    }

    /**
     * Sets the list of actuators used in the scan on the X dimension.
     * 
     * @param List<IActuator>
     */
    @Override
    public void setActuatorsXList(List<IActuator> actuatorsXList) {
        this.actuatorsXList = actuatorsXList;
    }

    /**
     * Gets the list of sensors used in the scan.
     * 
     * @return sensorsList
     */
    @Override
    public List<ISensor> getSensorsList() {
        return sensorsList;
    }

    /**
     * Sets the list of sensors used in the scan.
     * 
     * @param List<ISensor>
     */
    @Override
    public void setSensorsList(List<ISensor> sensorsList) {
        this.sensorsList = sensorsList;
    }

    /**
     * The type of scan.
     */
    @Override
    public ResultType getResultType() {
        return resultType;
    }

    /**
     * The type of scan.
     */
    @Override
    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    @Override
    public String getScanServer() {
        return scanServer;
    }

    @Override
    public String getSensorsTimeStampsCompleteName() {
        StringBuilder builder = new StringBuilder();
        builder.append(scanServer).append("/");
        builder.append("sensorsTimestamps");
        return builder.toString();
    }

    @Override
    public String getActuatorsTimeStampsCompleteName() {
        StringBuilder builder = new StringBuilder();
        builder.append(scanServer).append("/");
        builder.append("actuatorsTimestamps");
        return builder.toString();
    }

    @Override
    public void setScanServer(String scanServer) {
        this.scanServer = scanServer;
    }

    @Override
    public Map<IActuator, double[]> getTrajectoryMap() {
        return trajectoriesMap;
    }

    @Override
    public void setTrajectoryMap(Map<IActuator, double[]> trajectoryMap) {
        this.trajectoriesMap = trajectoryMap;

    }

    @Override
    public String toString() {
        String toString = super.toString();
        StringBuilder stringBuilder = new StringBuilder();
        if (getScanServer() != null) {
            stringBuilder.append("scanServer=" + getScanServer());
        }
        if (getRunName() != null) {
            stringBuilder.append("runName=" + getRunName());
        }
        if (getResultType() != null) {
            stringBuilder.append("resultType=" + getResultType());
        }
        if (getSensorsTimeStampsCompleteName() != null) {
            stringBuilder.append("sensorsTimeStampsCompleteName=" + getSensorsTimeStampsCompleteName());
        }
        if (SalsaUtils.isFulfilled(getSensorsList())) {
            List<ISensor> sensorList = getSensorsList();
            stringBuilder.append("number of sensor=" + sensorList.size());
            for (ISensor sensor : sensorList) {
                stringBuilder.append(sensor);
            }
        }

        if (SalsaUtils.isFulfilled(getTrajectoryMap())) {
            Map<IActuator, double[]> trajectoryMap = getTrajectoryMap();
            stringBuilder.append("number of actuators=" + trajectoryMap.size());
            Set<Entry<IActuator, double[]>> entrySet = trajectoryMap.entrySet();
            for (Entry<IActuator, double[]> entry : entrySet) {
                stringBuilder.append(entry.getKey() + "=" + Arrays.toString(entry.getValue()));
            }
        } else if (SalsaUtils.isFulfilled(getActuatorsXList())) {
            List<IActuator> actuatorList = getActuatorsXList();
            stringBuilder.append("number of actuators=" + actuatorList.size());
            for (IActuator actuator : actuatorList) {
                stringBuilder.append(actuator);
            }
        }

        String tmp = stringBuilder.toString();
        if (SalsaUtils.isDefined(tmp)) {
            toString = tmp;
        }

        return toString;
    }

}

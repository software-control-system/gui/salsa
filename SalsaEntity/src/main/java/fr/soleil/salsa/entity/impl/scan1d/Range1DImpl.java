package fr.soleil.salsa.entity.impl.scan1d;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.event.Range1DModel;
import fr.soleil.salsa.entity.impl.RangeImpl;
import fr.soleil.salsa.entity.scan1d.IRange1D;

/**
 * @author Alike
 * 
 */
public class Range1DImpl extends RangeImpl implements IRange1D {

    private static final long serialVersionUID = 1918213240122110693L;

    /**
     * The integration time.
     */
    private double[] integrationTime;

    /**
     * @see fr.soleil.salsa.entity.scan1d.IRange1D#getIntegrationTime()
     */
    @Override
    public double[] getIntegrationTime() {
        return integrationTime;
    }

    /**
     * @see fr.soleil.salsa.entity.scan1d.IRange1D#setIntegrationTime(double)
     */
    @Override
    public void setIntegrationTime(double[] integrationTime) {
        this.integrationTime = integrationTime;
    }

    @Override
    protected IRange initModel() {
        return new Range1DModel(this);
    }

}

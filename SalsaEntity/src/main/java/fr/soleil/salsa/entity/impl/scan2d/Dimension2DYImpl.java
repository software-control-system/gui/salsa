package fr.soleil.salsa.entity.impl.scan2d;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.event.Dimension2DYModel;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.util.ComparatorUtil;

/**
 * @author Alike
 * 
 */
public class Dimension2DYImpl extends DimensionImpl implements IDimension2DY {

    private static final long serialVersionUID = -6926208122210506721L;

    /**
     * Ranges.
     */
    private List<IRange2DY> rangesList;

    /**
     * Default constructor.
     */
    public Dimension2DYImpl() {
        rangesList = new ArrayList<IRange2DY>();
    }

    /**
     * Get ranges.
     * 
     * @return
     */
    public List<IRange2DY> getRangesList() {
        return rangesList;
    }

    /**
     * Set ranges.
     * 
     * @param ranges
     */
    public void setRangesList(List<IRange2DY> rangesList) {
        this.rangesList = rangesList;
    }

    protected IDimension initModel() {
        return new Dimension2DYModel(this);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null && super.isEqualsTo(dimension)) {
            if (dimension instanceof IDimension2DY) {
                equals = ComparatorUtil.rangeListEquals(
                        ((IDimension2DY) dimension).getRangesList(), getRangesList());
            }
        }
        return equals;
    }

}

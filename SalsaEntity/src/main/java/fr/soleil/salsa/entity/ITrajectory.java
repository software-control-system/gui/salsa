package fr.soleil.salsa.entity;

import java.io.Serializable;

/**
 * Interface for the trajectory entity. Describes the movement of an actuator to be performed during
 * a linear part (called range) of a scan.
 *
 * @see fr.soleil.salsa.entity.IRange
 * @see fr.soleil.salsa.entity.IDimension
 * @author Administrateur
 *
 */
public interface ITrajectory extends Serializable {

    /**
     * Gets the unique identifier.
     *
     * @return id
     */
    public Integer getId();

    /**
     * Sets the unique identifier.
     *
     * @param Integer
     */
    public void setId(Integer id);

    /**
     * Gets the name.
     *
     * @return name
     */
    public String getName();

    /**
     * Sets the name.
     *
     * @param name
     */
    public void setName(String name);

    /**
     * Gets the initial position.
     *
     * @return beginPosition
     */
    public double getBeginPosition();

    /**
     * Sets the initial position.
     *
     * @param double
     */
    public void setBeginPosition(double beginPosition);

    /**
     * Gets the final position.
     *
     * @return endPosition
     */
    public double getEndPosition();

    /**
     * Sets the final position.
     *
     * @param double
     */
    public void setEndPosition(double endPosition);

    /**
     * Gets the position difference between two consecutive steps. It is always positive. For
     * instance, the delta of trajectory 6, 8, 10, 12 is 2.
     *
     * @return delta
     */
    public double getDelta();

    /**
     * Sets the position difference between two consecutive steps. It is always positive. For
     * instance, the delta of trajectory 6, 8, 10, 12 is 2.
     *
     * @param double
     */
    public void setDelta(double delta);

    /**
     * Gets the actuator movement speed. It is always positive.
     *
     * @return speed
     */
    public double getSpeed();

    /**
     * Sets the actuator movement speed. It is always positive.
     *
     * @param double
     */
    public void setSpeed(double speed);

    /**
     * Gets the flag that marks the trajectory as relative. The reference position is the position
     * before the scan started.
     *
     * @return relative
     */
    public Boolean getRelative();

    /**
     * Sets the flag that marks the trajectory as relative. The reference position is the position
     * before the scan started.
     *
     * @param Boolean
     */
    public void setRelative(Boolean relative);

    public void setTrajectory(double[] values);

    public double[] getTrajectory();

    public ITrajectory toModel();

    public ITrajectory toImpl();

    public void setIRange(IRange range);

    public IRange getIRange();

    public void refreshDelta();

    public void refreshStep();

    public void refreshEndPosition();

    public Boolean isDeltaConstant();

    /**
     * Set true if the delta is constant.
     *
     * @param deltaConstant
     */
    public void setDeltaConstant(Boolean deltaConstant);

    public void setCustomTrajectory(boolean custom);

    public boolean isCustomTrajectory();

    public IActuator getActuator();

}

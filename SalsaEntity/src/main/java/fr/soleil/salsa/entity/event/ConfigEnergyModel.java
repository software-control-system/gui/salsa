package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.impl.scanenergy.ConfigEnergyImpl;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;

public class ConfigEnergyModel extends AConfigModel<IDimensionEnergy, IConfigEnergy> implements
        IConfigEnergy {

    private static final long serialVersionUID = 3329968135601265319L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param configEnergy the decorated base bean.
     */
    public ConfigEnergyModel(IConfigEnergy configEnergy) {
        super(configEnergy);
    }

    /**
     * Default constructor, that creates a new instance of {@link ConfigEnergyImpl} and wraps it.
     */
    public ConfigEnergyModel() {
        this(new ConfigEnergyImpl());
    }

}

package fr.soleil.salsa.entity.impl;

import java.util.Collection;
import java.util.Date;

import fr.soleil.salsa.entity.IHistoricLogLine;
import fr.soleil.salsa.entity.ITrajectory;

/**
 * Historic Log line. Save an event of log scan.
 * 
 */
public class HistoricLogLine implements IHistoricLogLine {

    private Date date;
    private String scanId;
    private String action;
    private String scan;
    private String nexusFile;
    private String nxEntry;
    private Collection<ITrajectory> trajectories;

    public HistoricLogLine() {
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public String getNexusFile() {
        return nexusFile;
    }

    @Override
    public String getNxEntry() {
        return nxEntry;
    }

    @Override
    public void setNxEntry(String nxEntry) {
        this.nxEntry = nxEntry;
    }

    @Override
    public String getScan() {
        return scan;
    }

    @Override
    public Collection<ITrajectory> getTrajectories() {
        return trajectories;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public void setNexusFile(String nexusFile) {
        this.nexusFile = nexusFile;
    }

    @Override
    public void setScan(String scan) {
        this.scan = scan;
    }

    /**
     * Gets the scan id.
     * 
     * @return scan
     */
    @Override
    public String getScanId() {
        return scanId;
    }

    /**
     * Sets the scan id.
     * 
     * @param scan
     */
    @Override
    public void setScanId(String scanid) {
        this.scanId = scanid;
    }

    @Override
    public void setTrajectories(Collection<ITrajectory> trajectories) {
        this.trajectories = trajectories;
    }
}

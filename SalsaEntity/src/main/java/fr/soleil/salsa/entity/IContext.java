package fr.soleil.salsa.entity;

import java.io.Serializable;

/**
 * A class representing a scan context. A scan context contains:
 * <ul>
 * <li>A scan server name</li>
 * <li>A user log file</li>
 * <li>A maximum line number for this log</li>
 * </ul>
 * 
 * @author alikedba
 */
public interface IContext extends Serializable {

    /**
     * Get the userLogfile.
     * 
     * @return userLogFile
     */
    public String getUserLogFile();

    /**
     * Sets the userLogFile.
     * 
     * @param userLogFile
     */
    public void setUserLogFile(String userLogFile);

    /**
     * Get the scanServerName.
     * 
     * @return scanServerName
     */
    public String getScanServerName();

    /**
     * Sets the scanServerName.
     * 
     * @param scanServerName
     */
    public void setScanServerName(String scanServerName);

    /**
     * Get the maxLineNumber.
     * 
     * @return maxLineNumber
     */
    public Integer getMaxLineNumber();

    /**
     * Sets the maxLineNumber.
     * 
     * @param maxLineNumber
     */
    public void setMaxLineNumber(Integer maxLineNumber);

}

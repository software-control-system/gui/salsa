package fr.soleil.salsa.entity.event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;

/**
 * Event fired when a property of an entity has been changed.<br />
 * This event must be fired after the property has been changed. This event can actually contain the
 * changes of several properties of a single entity due to event merging.<br />
 * oldValue might be new when the property is a collection, and giving both oldValue and newValue
 * would require to clone it.<br />
 * 
 * @author Administrateur
 * @param <T> the entity type
 * @see SalsaEventQueue
 */
public class EntityPropertyChangedEvent<T> implements IMergeCapableEvent {

    private static final long serialVersionUID = -108063778163192405L;

    /**
     * A single property change.
     * 
     * @author Administrateur
     */
    public class PropertyChange<V> implements Serializable {

        private static final long serialVersionUID = -6753271211067081272L;

        /**
         * The name of the changed property.
         */
        private String propertyName;

        /**
         * The previous value of the property. oldValue might be new when the property is a
         * collection, and giving both oldValue and newValue would require to clone it.<br />
         */
        private V oldValue;

        /**
         * The new value of the property.
         */
        private V newValue;

        /**
         * Constructor.
         * 
         * @param propertyName
         * @param oldValue, oldValue might be new when the property is a collection, and giving both
         *            oldValue and newValue would require to clone it.<br />
         * @param newValue
         */
        public PropertyChange(String propertyName, V oldValue, V newValue) {
            super();
            this.propertyName = propertyName;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        /**
         * @return the propertyName
         */
        public String getPropertyName() {
            return propertyName;
        }

        /**
         * @return the oldValue oldValue might be new when the property is a collection, and giving
         *         both oldValue and newValue would require to clone it.<br />
         */
        public V getOldValue() {
            return oldValue;
        }

        /**
         * @return the newValue
         */
        public V getNewValue() {
            return newValue;
        }
    }

    /**
     * The entity whose property(ies) has(have) been changed.
     */
    private T entity;

    /**
     * The list of property changes.
     */
    private List<PropertyChange<?>> propertyChangeList;

    /**
     * Constructor for a single property change. EntityPropertyChangedEvent for multiple properties
     * change are created by the event queue using the merge method. oldValue might be new when the
     * property is a collection, and giving both oldValue and newValue would require to clone it.<br />
     * 
     * @param entity the entity whose property(ies) has(have) been changed.
     * @param propertyName
     * @param oldValue
     * @param newValue
     * @param <V> the property type.
     */
    public <V> EntityPropertyChangedEvent(T entity, String propertyName, V oldValue, V newValue) {
        super();
        assert entity != null;
        assert propertyName != null;
        this.entity = entity;
        this.propertyChangeList = new ArrayList<PropertyChange<?>>(1);
        PropertyChange<V> propertyChange = new PropertyChange<V>(propertyName, oldValue, newValue);
        this.propertyChangeList.add(propertyChange);
    }

    /**
     * @return the entity
     */
    public T getEntity() {
        return entity;
    }

    /**
     * @return the propertyChangeList
     */
    public List<PropertyChange<?>> getPropertyChangeList() {
        return propertyChangeList;
    }

    /**
     * @see IEvent#canMerge(IEvent)
     */
    public boolean canMerge(IEvent event) {
        boolean canMerge;
        if (event instanceof EntityPropertyChangedEvent<?>) {
            canMerge = this.getEntity().equals(((EntityPropertyChangedEvent<?>) event).getEntity());
        }
        else {
            canMerge = false;
        }
        return canMerge;
    }

    /**
     * @see IEvent#merge(IEvent)
     */
    @SuppressWarnings("unchecked")
    public IEvent merge(IEvent event) {
        EntityPropertyChangedEvent<T> that = (EntityPropertyChangedEvent<T>) event;
        this.propertyChangeList.addAll(that.propertyChangeList);
        return this;
    }

    /**
     * The event source.
     */
    @SuppressWarnings("unchecked")
    public IEventSource<IEvent> getEventSource() {
        return (IEventSource<IEvent>) this.getEntity();
    }
}

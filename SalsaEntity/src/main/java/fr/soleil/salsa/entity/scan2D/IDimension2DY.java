package fr.soleil.salsa.entity.scan2D;

import java.util.List;

/**
 * @author Alike
 *
 */
public interface IDimension2DY extends IDimension2D {

    /**
     * Get ranges.
     * @return
     */
    public List<IRange2DY> getRangesList();

    /**
     * Set ranges.
     * @param ranges
     */
    public void setRangesList(List<IRange2DY> rangesList);
    
    
    
}

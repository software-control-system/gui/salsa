package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IDisplay;
import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.impl.ScanAddOnImp;

/***
 * Event handling decorator of {@link IScanAddOns} for the ScanAddOn entity. A ScanAddOn is a
 * detector that measures the scanned values.
 * 
 * @author Tarek
 * 
 */
public class ScanAddOnModel extends AEventHandlingModelDecorator<IScanAddOns> implements
        IScanAddOns {

    private static final long serialVersionUID = 7333643292590004404L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param IScanAddOns the decorated base bean.
     */
    public ScanAddOnModel(IScanAddOns scanAddOns) {
        super(scanAddOns);
    }

    public ScanAddOnModel() {
        super(new ScanAddOnImp());
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @param id
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the error strategy
     * 
     * @return errorStrategy
     */
    @Override
    public IErrorStrategy getErrorStrategy() {
        return this.baseBean.getErrorStrategy();
    }

    /**
     * Sets the error strategy
     * 
     * @param errorStrategy
     */
    @Override
    public void setErrorStrategy(IErrorStrategy errorStrategy) {
        IErrorStrategy oldValue = this.baseBean.getErrorStrategy();
        baseBean.setErrorStrategy(errorStrategy);
        this.firePropertyChange("id", oldValue, errorStrategy);
    }

    /**
     * Gets the hooks list
     * 
     * @return
     */
    @Override
    public List<IHook> getHooks() {
        return baseBean.getHooks();
    }

    /**
     * Set the hooks list
     * 
     * @param hooksList
     */
    @Override
    public void setHooks(List<IHook> hooksList) {
        List<IHook> oldValue = baseBean.getHooks();
        baseBean.setHooks(hooksList);
        firePropertyChange("hooksList", oldValue, hooksList);
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IScanAddOns#getPostScanBehaviour()
     */
    @Override
    public IPostScanBehaviour getPostScanBehaviour() {
        return baseBean.getPostScanBehaviour();
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IScanAddOns#setPostScanBehaviour(fr.soleil.salsa.entity.IPostScanBehaviour)
     */
    @Override
    public void setPostScanBehaviour(IPostScanBehaviour postScanBehaviour) {
        IPostScanBehaviour oldValue = baseBean.getPostScanBehaviour();
        baseBean.setPostScanBehaviour(postScanBehaviour);
        firePropertyChange("postScanBehaviour", oldValue, postScanBehaviour);
    }

    /**
     * The display manager parameters.
     * 
     * @return the display
     */
    @Override
    public IDisplay getDisplay() {
        return baseBean.getDisplay();
    }

    /**
     * The display manager parameters.
     * 
     * @param display the display to set
     */
    @Override
    public void setDisplay(IDisplay display) {
        IDisplay oldValue = baseBean.getDisplay();
        baseBean.setDisplay(display);
        firePropertyChange("display", oldValue, display);
    }

    public IScanAddOns toModel() {
        return this;
    }

}

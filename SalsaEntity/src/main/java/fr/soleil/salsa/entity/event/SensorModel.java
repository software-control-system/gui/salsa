package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.impl.SensorImpl;

/***
 * Event handling decorator of {@link ISensor} for the sensor entity. A sensor is a detector that
 * measures the scanned values.
 * 
 * @author Administrateur
 * 
 */
public class SensorModel extends AEventHandlingModelDecorator<ISensor> implements ISensor {

    private static final long serialVersionUID = 1064941551711183417L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param sensor the decorated base bean.
     */
    public SensorModel(ISensor sensor) {
        super(sensor);
    }

    /**
     * Default constructor, that creates a new instance of SensorImpl and wraps it.
     */
    public SensorModel() {
        this(new SensorImpl());
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the sensor name.
     * 
     * @return name
     */
    @Override
    public String getName() {
        return baseBean.getName();
    }

    /**
     * Sets the sensor name.
     * 
     * @param String
     */
    @Override
    public void setName(String name) {
        String oldValue = this.baseBean.getName();
        baseBean.setName(name);
        this.firePropertyChange("name", oldValue, this.baseBean.getName());
    }

    /**
     * Set the sensor attribute name in scan server
     * 
     */
    @Override
    public String getScanServerAttributeName() {
        return baseBean.getScanServerAttributeName();
    }

    /**
     * Set the sensor attribute name in scan server
     * 
     */
    @Override
    public void setScanServerAttributeName(String scanServerAttributeName) {
        String oldValue = this.baseBean.getScanServerAttributeName();
        baseBean.setScanServerAttributeName(scanServerAttributeName);
        this.firePropertyChange("scanServerAttributeName", oldValue, scanServerAttributeName);
    }

    /**
     * True if the sensor is enabled.
     * 
     * @return
     */
    @Override
    public boolean isEnabled() {
        return baseBean.isEnabled();
    }

    /**
     * True if the sensor is enabled.
     * 
     * @param enabled
     */
    @Override
    public void setEnabled(boolean enabled) {
        baseBean.setEnabled(enabled);
    }

    @Override
    public boolean isCommon() {
        return baseBean.isCommon();
    }

    @Override
    public void setCommon(boolean common) {
        baseBean.setCommon(common);
    }

    @Override
    public IDevice toModel() {
        return this;
    }

    @Override
    public String toString() {
        return baseBean.toString();
    }
}

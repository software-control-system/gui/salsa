package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.Map;

/**
 * The interface for the scan point entity.<br />
 * A point in a scan.<br />
 * This contains the values the time, the actuators and the sensors had at a point in a scan.<br />
 * 
 * @author Administrateur
 */
public interface IScanPoint extends Serializable {

    /**
     * The time.
     * 
     * @return the time
     */
    public Double getTime();

    /**
     * The time.
     * 
     * @param time the time to set
     */
    public void setTime(Double time);

    /**
     * The values of the actuators on the X dimension.
     * 
     * @return the actuatorsValuesMap
     */
    public Map<IActuator, Double> getActuatorsXValuesMap();

    /**
     * The values of the actuators on the X dimension.
     * 
     * @param actuatorsValuesMap the actuatorsValuesMap to set
     */
    public void setActuatorsXValuesMap(Map<IActuator, Double> actuatorsXValuesMap);

    /**
     * The values of the actuators on the Y dimension.
     * 
     * @return the actuatorsValuesMap
     */
    public Map<IActuator, Double> getActuatorsYValuesMap();

    /**
     * The values of the actuators on the Y dimension.
     * 
     * @param actuatorsValuesMap the actuatorsValuesMap to set
     */
    public void setActuatorsYValuesMap(Map<IActuator, Double> actuatorsYValuesMap);

    /**
     * The values of the sensors.
     * 
     * @return the sensorsValuesMap
     */
    public Map<ISensor, Double> getSensorsValuesMap();

    /**
     * The values of the sensors.
     * 
     * @param sensorsValuesMap the sensorsValuesMap to set
     */
    public void setSensorsValuesMap(Map<ISensor, Double> sensorsValuesMap);

    /**
     * The value of an actuator.
     * 
     * @param actuator
     * @return
     */
    public Double getValue(IActuator actuator);

    /**
     * The value of a sensor.
     * 
     * @param sensor
     * @return
     */
    public Double getValue(ISensor sensor);
}

package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.event.ErrorStrategyItemModel;

/**
 * @author Alike
 *
 *         Class relative to the error strategy item.
 *
 */
public class ErrorStrategyItemImpl implements IErrorStrategyItem {

    private static final long serialVersionUID = -6348715068288033124L;

    /**
     * Number of retry.
     */
    private int retryCount = 5;

    /**
     * The strategy.
     */
    private ErrorStrategyType strategy = ErrorStrategyType.ABORT;

    /**
     * The time between retries.
     */
    private double timeBetweenRetries = 0.2;

    /**
     * The time out.
     */
    private double timeOut = 5;

    IErrorStrategyItem model = null;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getRetryCount()
     */
    @Override
    public int getRetryCount() {
        return retryCount;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getStrategy()
     */
    @Override
    public ErrorStrategyType getStrategy() {
        return strategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getTimeBetweenRetries()
     */
    @Override
    public double getTimeBetweenRetries() {
        return timeBetweenRetries;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getTimeOut()
     */
    @Override
    public double getTimeOut() {
        return timeOut;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#setRetryCount(int)
     */
    @Override
    public void setRetryCount(final int retryCount) {
        this.retryCount = retryCount;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#setStrategy(fr.soleil.salsa.entity.impl.ErrorStrategyType)
     */
    @Override
    public void setStrategy(final ErrorStrategyType strategy) {
        this.strategy = strategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#setTimeBetweenRetries(double)
     */
    @Override
    public void setTimeBetweenRetries(final double timeBetweenRetries) {
        this.timeBetweenRetries = timeBetweenRetries;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#setTimeOut(double)
     */
    @Override
    public void setTimeOut(final double timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public IErrorStrategyItem toModel() {
        if (model == null) {
            model = initModel();
        }
        return model;
    }

    protected IErrorStrategyItem initModel() {
        ErrorStrategyItemModel strategyModel = new ErrorStrategyItemModel(this);
        return strategyModel;
    }

    @Override
    public IErrorStrategyItem toImpl() {
        return this;
    }

}

package fr.soleil.salsa.entity.event.listener;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDisplay;
import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.ActuatorModel;
import fr.soleil.salsa.entity.event.Config1DModel;
import fr.soleil.salsa.entity.event.Config2DModel;
import fr.soleil.salsa.entity.event.ConfigEnergyModel;
import fr.soleil.salsa.entity.event.ConfigHCSModel;
import fr.soleil.salsa.entity.event.ConfigKModel;
import fr.soleil.salsa.entity.event.Dimension1DModel;
import fr.soleil.salsa.entity.event.Dimension2DXModel;
import fr.soleil.salsa.entity.event.Dimension2DYModel;
import fr.soleil.salsa.entity.event.DimensionEnergyModel;
import fr.soleil.salsa.entity.event.DimensionHCSModel;
import fr.soleil.salsa.entity.event.DimensionKModel;
import fr.soleil.salsa.entity.event.DisplayModel;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.ErrorStrategyItemModel;
import fr.soleil.salsa.entity.event.ErrorStrategyModel;
import fr.soleil.salsa.entity.event.HookCommandModel;
import fr.soleil.salsa.entity.event.HookModel;
import fr.soleil.salsa.entity.event.PostScanBehaviourModel;
import fr.soleil.salsa.entity.event.Range1DModel;
import fr.soleil.salsa.entity.event.Range2DXModel;
import fr.soleil.salsa.entity.event.Range2DYModel;
import fr.soleil.salsa.entity.event.RangeEnergyModel;
import fr.soleil.salsa.entity.event.RangeHCSModel;
import fr.soleil.salsa.entity.event.RangeKModel;
import fr.soleil.salsa.entity.event.ScanAddOnModel;
import fr.soleil.salsa.entity.event.SensorModel;
import fr.soleil.salsa.entity.event.Trajectory1DModel;
import fr.soleil.salsa.entity.event.Trajectory2DXModel;
import fr.soleil.salsa.entity.event.Trajectory2DYModel;
import fr.soleil.salsa.entity.event.TrajectoryEnergyModel;
import fr.soleil.salsa.entity.event.TrajectoryHCSModel;
import fr.soleil.salsa.entity.event.TrajectoryKModel;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DX;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;

/**
 * Monitors a config using the EntityPropertyChangedEvent to update its modified property.
 */
public class ConfigChangeListener {

    /**
     * The monitored config.
     */
    private IConfig<?> config = null;

    /**
     * Listener for 1D config.
     */
    private IListener<EntityPropertyChangedEvent<IConfig1D>> listenerConfig1D = new IListener<EntityPropertyChangedEvent<IConfig1D>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IConfig1D> event) {
            boolean notModifiedProperty = false;
            for (EntityPropertyChangedEvent<IConfig1D>.PropertyChange<?> propertyChange : event
                    .getPropertyChangeList()) {
                if (!propertyChange.getPropertyName().equals("modified")) {
                    notModifiedProperty = true;
                    break;
                }
            }
            if (notModifiedProperty) {
                configChanged();
            }
        }
    };

    /**
     * Listener for energy config.
     */
    private IListener<EntityPropertyChangedEvent<IConfigEnergy>> listenerConfigEnergy = new IListener<EntityPropertyChangedEvent<IConfigEnergy>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IConfigEnergy> event) {
            boolean notModifiedProperty = false;
            for (EntityPropertyChangedEvent<IConfigEnergy>.PropertyChange<?> propertyChange : event
                    .getPropertyChangeList()) {
                if (!propertyChange.getPropertyName().equals("modified")) {
                    notModifiedProperty = true;
                    break;
                }
            }
            if (notModifiedProperty) {
                configChanged();
            }
        }
    };

    /**
     * Listener for 2D config.
     */
    private IListener<EntityPropertyChangedEvent<IConfig2D>> listenerConfig2D = new IListener<EntityPropertyChangedEvent<IConfig2D>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IConfig2D> event) {
            boolean notModifiedProperty = false;
            for (EntityPropertyChangedEvent<IConfig2D>.PropertyChange<?> propertyChange : event
                    .getPropertyChangeList()) {
                if (!propertyChange.getPropertyName().equals("modified")) {
                    notModifiedProperty = true;
                    break;
                }
            }
            if (notModifiedProperty) {
                configChanged();
            }
        }
    };

    /**
     * Listener for HCS config.
     */
    private IListener<EntityPropertyChangedEvent<IConfigHCS>> listenerConfigHCS = new IListener<EntityPropertyChangedEvent<IConfigHCS>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IConfigHCS> event) {
            boolean notModifiedProperty = false;
            for (EntityPropertyChangedEvent<IConfigHCS>.PropertyChange<?> propertyChange : event
                    .getPropertyChangeList()) {
                if (!propertyChange.getPropertyName().equals("modified")) {
                    notModifiedProperty = true;
                    break;
                }
            }
            if (notModifiedProperty) {
                configChanged();
            }
        }
    };

    /**
     * Listener for K config.
     */
    private IListener<EntityPropertyChangedEvent<IConfigK>> listenerConfigK = new IListener<EntityPropertyChangedEvent<IConfigK>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IConfigK> event) {
            boolean notModifiedProperty = false;
            for (EntityPropertyChangedEvent<IConfigK>.PropertyChange<?> propertyChange : event
                    .getPropertyChangeList()) {
                if (!propertyChange.getPropertyName().equals("modified")) {
                    notModifiedProperty = true;
                    break;
                }
            }
            if (notModifiedProperty) {
                configChanged();
            }
        }
    };

    /**
     * Listener for sensors.
     */
    private IListener<EntityPropertyChangedEvent<ISensor>> listenerSensor = new IListener<EntityPropertyChangedEvent<ISensor>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ISensor> event) {
            configChanged();
        }
    };

    /**
     * Listener for dimension1D.
     */
    private IListener<EntityPropertyChangedEvent<IDimension1D>> listenerDimension1D = new IListener<EntityPropertyChangedEvent<IDimension1D>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IDimension1D> event) {
            configChanged();
        }
    };

    /**
     * Listener for dimensionEnergy.
     */
    private IListener<EntityPropertyChangedEvent<IDimensionEnergy>> listenerDimensionEnergy = new IListener<EntityPropertyChangedEvent<IDimensionEnergy>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IDimensionEnergy> event) {
            configChanged();
        }
    };

    /**
     * Listener for dimension2DX.
     */
    private IListener<EntityPropertyChangedEvent<IDimension2DX>> listenerDimension2DX = new IListener<EntityPropertyChangedEvent<IDimension2DX>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IDimension2DX> event) {
            configChanged();
        }
    };

    /**
     * Listener for dimension2DY.
     */
    private IListener<EntityPropertyChangedEvent<IDimension2DY>> listenerDimension2DY = new IListener<EntityPropertyChangedEvent<IDimension2DY>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IDimension2DY> event) {
            configChanged();
        }
    };

    /**
     * Listener for dimensionHCS.
     */
    private IListener<EntityPropertyChangedEvent<IDimensionHCS>> listenerDimensionHCS = new IListener<EntityPropertyChangedEvent<IDimensionHCS>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IDimensionHCS> event) {
            configChanged();
        }
    };

    /**
     * Listener for dimensionK.
     */
    private IListener<EntityPropertyChangedEvent<IDimensionK>> listenerDimensionK = new IListener<EntityPropertyChangedEvent<IDimensionK>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IDimensionK> event) {
            configChanged();
        }
    };

    /**
     * Listener for actuators.
     */
    private IListener<EntityPropertyChangedEvent<IActuator>> listenerActuator = new IListener<EntityPropertyChangedEvent<IActuator>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IActuator> event) {
            configChanged();
        }
    };

    /**
     * Listener for 1D ranges.
     */
    private IListener<EntityPropertyChangedEvent<IRange1D>> listenerRange1D = new IListener<EntityPropertyChangedEvent<IRange1D>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IRange1D> event) {
            configChanged();
        }
    };

    /**
     * Listener for energy ranges.
     */
    private IListener<EntityPropertyChangedEvent<IRangeEnergy>> listenerRangeEnergy = new IListener<EntityPropertyChangedEvent<IRangeEnergy>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IRangeEnergy> event) {
            configChanged();
        }
    };

    /**
     * Listener for 2D X ranges.
     */
    private IListener<EntityPropertyChangedEvent<IRange2DX>> listenerRange2DX = new IListener<EntityPropertyChangedEvent<IRange2DX>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IRange2DX> event) {
            configChanged();
        }
    };

    /**
     * Listener for 2D Y ranges.
     */
    private IListener<EntityPropertyChangedEvent<IRange2DY>> listenerRange2DY = new IListener<EntityPropertyChangedEvent<IRange2DY>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IRange2DY> event) {
            configChanged();
        }
    };

    /**
     * Listener for HCS ranges.
     */
    private IListener<EntityPropertyChangedEvent<IRangeHCS>> listenerRangeHCS = new IListener<EntityPropertyChangedEvent<IRangeHCS>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IRangeHCS> event) {
            configChanged();
        }
    };

    /**
     * Listener for K ranges.
     */
    private IListener<EntityPropertyChangedEvent<IRangeK>> listenerRangeK = new IListener<EntityPropertyChangedEvent<IRangeK>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IRangeK> event) {
            configChanged();
        }
    };

    /**
     * Listener for trajectories.
     */
    private IListener<EntityPropertyChangedEvent<ITrajectory1D>> listenerTrajectory1D = new IListener<EntityPropertyChangedEvent<ITrajectory1D>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ITrajectory1D> event) {
            configChanged();
        }
    };

    /**
     * Listener for trajectories energy.
     */
    private IListener<EntityPropertyChangedEvent<ITrajectoryEnergy>> listenerTrajectoryEnergy = new IListener<EntityPropertyChangedEvent<ITrajectoryEnergy>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ITrajectoryEnergy> event) {
            configChanged();
        }
    };

    /**
     * Listener for X trajectories 2D.
     */
    private IListener<EntityPropertyChangedEvent<ITrajectory2DX>> listenerTrajectory2DX = new IListener<EntityPropertyChangedEvent<ITrajectory2DX>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ITrajectory2DX> event) {
            configChanged();
        }
    };

    /**
     * Listener for Y trajectories 2D.
     */
    private IListener<EntityPropertyChangedEvent<ITrajectory2DY>> listenerTrajectory2DY = new IListener<EntityPropertyChangedEvent<ITrajectory2DY>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ITrajectory2DY> event) {
            configChanged();
        }
    };

    /**
     * Listener for HCS trajectories.
     */
    private IListener<EntityPropertyChangedEvent<ITrajectoryHCS>> listenerTrajectoryHCS = new IListener<EntityPropertyChangedEvent<ITrajectoryHCS>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ITrajectoryHCS> event) {
            configChanged();
        }
    };

    /**
     * Listener for K trajectory.
     */
    private IListener<EntityPropertyChangedEvent<ITrajectoryK>> listenerTrajectoryK = new IListener<EntityPropertyChangedEvent<ITrajectoryK>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<ITrajectoryK> event) {
            configChanged();
        }
    };

    /**
     * Listener for IScanAddOns.
     */
    private IListener<EntityPropertyChangedEvent<IScanAddOns>> listenerScanAddOn = new IListener<EntityPropertyChangedEvent<IScanAddOns>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IScanAddOns> event) {
            configChanged();
        }
    };

    /**
     * Listener for ErrorStrategy.
     */
    private IListener<EntityPropertyChangedEvent<IErrorStrategy>> listenerErrorStrategy = new IListener<EntityPropertyChangedEvent<IErrorStrategy>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IErrorStrategy> event) {
            configChanged();
        }
    };

    /**
     * Listener for ErrorStrategyItem.
     */
    private IListener<EntityPropertyChangedEvent<IErrorStrategyItem>> listenerErrorStrategyItem = new IListener<EntityPropertyChangedEvent<IErrorStrategyItem>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IErrorStrategyItem> event) {
            configChanged();
        }
    };

    /**
     * Listener for PostScanBehaviour.
     */
    private IListener<EntityPropertyChangedEvent<IPostScanBehaviour>> listenerPostScanBehaviour = new IListener<EntityPropertyChangedEvent<IPostScanBehaviour>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IPostScanBehaviour> event) {
            configChanged();
        }
    };

    /**
     * Listener for Hooks.
     */
    private IListener<EntityPropertyChangedEvent<IHook>> listenerHook = new IListener<EntityPropertyChangedEvent<IHook>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IHook> event) {
            configChanged();
        }
    };

    /**
     * Listener for HooksCommand.
     */
    private IListener<EntityPropertyChangedEvent<IHookCommand>> listenerHookCommand = new IListener<EntityPropertyChangedEvent<IHookCommand>>() {
        @Override
        public void notifyEvent(EntityPropertyChangedEvent<IHookCommand> event) {
            configChanged();
        }
    };

    private IListener<EntityPropertyChangedEvent<IDisplay>> displayListener = new IListener<EntityPropertyChangedEvent<IDisplay>>() {
        @Override
        public void notifyEvent(fr.soleil.salsa.entity.event.EntityPropertyChangedEvent<IDisplay> event) {
            configChanged();
        };
    };

    /**
     * The one instance (singleton).
     */
    private static ConfigChangeListener instance;

    /**
     * Initializes the one instance.
     */
    static {
        instance = new ConfigChangeListener();
    }

    /**
     * Private constructor : this is a singleton.
     */
    private ConfigChangeListener() {

    }

    /**
     * Access the instance (singleton).
     * 
     * @return
     */
    public static ConfigChangeListener getInstance() {
        return instance;
    }

    /**
     * Enable/Disable listening to scanAddOn.
     * 
     * @param isao
     * @param listen
     */
    private void enableListening(IScanAddOns isao, boolean listen) {
        if (isao instanceof ScanAddOnModel) { // this test returns false if isao is null
            ScanAddOnModel saom = (ScanAddOnModel) isao;

            if (listen) {
                saom.addListener(listenerScanAddOn);
            } else {
                saom.removeListener(listenerScanAddOn);
            }

            enableListening(saom.getErrorStrategy(), listen);
            enableListening(saom.getPostScanBehaviour(), listen);
            enableListening(saom.getDisplay(), listen);
            if (isao.getHooks() != null) {
                for (IHook oh : isao.getHooks()) {
                    enableListening(oh, listen);
                }
            }
        }
    }

    private void enableListening(IDisplay display, boolean listen) {
        if (display instanceof DisplayModel) { // this test returns false if display is null
            DisplayModel displayModel = (DisplayModel) display;
            if (listen) {
                displayModel.addListener(displayListener);
            } else {
                displayModel.removeListener(displayListener);
            }
        }
    }

    /**
     * Enable/Disable listening to Hook commands.
     * 
     * @param ihc
     * @param listen
     */
    private void enableListening(IHookCommand ihc, boolean listen) {
        if (ihc instanceof HookCommandModel) { // this test returns false if ihc is null
            HookCommandModel hcm = (HookCommandModel) ihc;
            if (listen) {
                hcm.addListener(listenerHookCommand);
            } else {
                hcm.removeListener(listenerHookCommand);
            }
        }
    }

    /**
     * Enable/Disable listening to hook.
     * 
     * @param ih
     * @param listen
     */
    private void enableListening(IHook ih, boolean listen) {
        if (ih instanceof HookModel) { // this test returns false if ih is null
            HookModel hm = (HookModel) ih;
            if (listen) {
                hm.addListener(listenerHook);
            } else {
                hm.removeListener(listenerHook);
            }
            if (ih.getCommandsList() != null) {
                for (IHookCommand ihc : ih.getCommandsList()) {
                    enableListening(ihc, listen);
                }
            }
        }
    }

    /**
     * Enable/Disable listening to post scan behaviour.
     * 
     * @param ipsb
     * @param listen
     */
    private void enableListening(IPostScanBehaviour ipsb, boolean listen) {
        if (ipsb instanceof PostScanBehaviourModel) { // this test returns false if ipsb is null
            PostScanBehaviourModel psbm = (PostScanBehaviourModel) ipsb;

            if (listen) {
                psbm.addListener(listenerPostScanBehaviour);
            } else {
                psbm.removeListener(listenerPostScanBehaviour);
            }
        }
    }

    /**
     * Enable/Disable listening to Error Strategy Item.
     * 
     * @param ies
     * @param listen
     */
    private void enableListening(IErrorStrategyItem ies, boolean listen) {
        if (ies instanceof ErrorStrategyItemModel) { // this test returns false if ies is null
            ErrorStrategyItemModel esim = (ErrorStrategyItemModel) ies;
            if (listen) {
                esim.addListener(listenerErrorStrategyItem);
            } else {
                esim.removeListener(listenerErrorStrategyItem);
            }
        }
    }

    /**
     * Enable/Disable listening to Error Strategy.
     * 
     * @param ies
     * @param listen
     */
    private void enableListening(IErrorStrategy ies, boolean listen) {
        if (ies instanceof ErrorStrategyModel) { // this test returns false if ies is null
            ErrorStrategyModel esm = (ErrorStrategyModel) ies;
            if (listen) {
                esm.addListener(listenerErrorStrategy);
            } else {
                esm.removeListener(listenerErrorStrategy);
            }
            enableListening(esm.getActuatorsErrorStrategy(), listen);
            enableListening(esm.getSensorsErrorStrategy(), listen);
            enableListening(esm.getTimebasesErrorStrategy(), listen);
            enableListening(esm.getHooksErrorStrategy(), listen);
        }
    }

    /**
     * Start listening to a config.
     * 
     * @param config
     */
    public void startListening(IConfig<?> config) {
        if (config != null) {
            assert config instanceof IEventSource<?>;
            stopListening();
            this.config = config;

            enableListening(config.getScanAddOn(), true);

            switch (config.getType()) {
                case SCAN_1D:
                    Config1DModel configModel = (Config1DModel) config;
                    configModel.addListener(listenerConfig1D);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).addListener(listenerSensor);
                    }
                    Dimension1DModel dimension1DModel = (Dimension1DModel) configModel.getDimensionX();
                    dimension1DModel.addListener(listenerDimension1D);
                    for (IActuator actuator : dimension1DModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).addListener(listenerActuator);
                    }
                    for (IRange1D range1D : dimension1DModel.getRangesXList()) {
                        ((Range1DModel) range1D).addListener(listenerRange1D);
                        for (ITrajectory trajectory : range1D.getTrajectoriesList()) {
                            ((Trajectory1DModel) trajectory).addListener(listenerTrajectory1D);
                        }
                    }
                    break;

                case SCAN_2D:
                    Config2DModel config2DModel = (Config2DModel) config;
                    config2DModel.addListener(listenerConfig2D);

                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).addListener(listenerSensor);
                    }

                    Dimension2DXModel dimension2DXModel = (Dimension2DXModel) config2DModel.getDimensionX();
                    dimension2DXModel.addListener(listenerDimension2DX);

                    Dimension2DYModel dimension2DYModel = (Dimension2DYModel) config2DModel.getDimensionY();
                    dimension2DYModel.addListener(listenerDimension2DY);

                    for (IActuator actuator : dimension2DXModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).addListener(listenerActuator);
                    }

                    for (IActuator actuator : dimension2DYModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).addListener(listenerActuator);
                    }

                    for (IRange2DX range2DX : dimension2DXModel.getRangesList()) {
                        ((Range2DXModel) range2DX).addListener(listenerRange2DX);
                        for (ITrajectory trajectory : range2DX.getTrajectoriesList()) {
                            ((Trajectory2DXModel) trajectory).addListener(listenerTrajectory2DX);
                        }
                    }

                    for (IRange2DY range2DY : dimension2DYModel.getRangesList()) {
                        ((Range2DYModel) range2DY).addListener(listenerRange2DY);
                        for (ITrajectory trajectory : range2DY.getTrajectoriesList()) {
                            ((Trajectory2DYModel) trajectory).addListener(listenerTrajectory2DY);
                        }
                    }

                    break;

                case SCAN_HCS:
                    ConfigHCSModel configHCSModel = (ConfigHCSModel) config;
                    configHCSModel.addListener(listenerConfigHCS);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).addListener(listenerSensor);
                    }
                    DimensionHCSModel dimensionHCSModel = (DimensionHCSModel) configHCSModel.getDimensionX();
                    dimensionHCSModel.addListener(listenerDimensionHCS);
                    for (IActuator actuator : dimensionHCSModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).addListener(listenerActuator);
                    }
                    for (IRangeHCS rangeHCS : dimensionHCSModel.getRangesXList()) {
                        ((RangeHCSModel) rangeHCS).addListener(listenerRangeHCS);
                        for (ITrajectory trajectory : rangeHCS.getTrajectoriesList()) {
                            ((TrajectoryHCSModel) trajectory).addListener(listenerTrajectoryHCS);
                        }
                    }
                    break;

                case SCAN_ENERGY:
                    ConfigEnergyModel configEnergyModel = (ConfigEnergyModel) config;
                    configEnergyModel.addListener(listenerConfigEnergy);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).addListener(listenerSensor);
                    }
                    DimensionEnergyModel dimensionEnergyModel = (DimensionEnergyModel) configEnergyModel
                            .getDimensionX();
                    dimensionEnergyModel.addListener(listenerDimensionEnergy);
                    for (IActuator actuator : dimensionEnergyModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).addListener(listenerActuator);
                    }
                    for (IRangeEnergy rangeEnergy : dimensionEnergyModel.getRangesEnergyList()) {
                        ((RangeEnergyModel) rangeEnergy).addListener(listenerRangeEnergy);
                    }
                    for (ITrajectoryEnergy trajectoryEnergy : dimensionEnergyModel.getTrajectoriesEnergyList()) {
                        ((TrajectoryEnergyModel) trajectoryEnergy).addListener(listenerTrajectoryEnergy);
                    }
                    break;

                case SCAN_K:
                    ConfigKModel configKModel = (ConfigKModel) config;
                    configKModel.addListener(listenerConfigK);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).addListener(listenerSensor);
                    }
                    DimensionKModel dimensionKModel = (DimensionKModel) configKModel.getDimensionX();
                    dimensionKModel.addListener(listenerDimensionK);
                    for (IActuator actuator : dimensionKModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).addListener(listenerActuator);
                    }
                    IRangeK rangeK = dimensionKModel.getRangeX();
                    ((RangeKModel) rangeK).addListener(listenerRangeK);

                    ITrajectoryK trajectoryK = dimensionKModel.getTrajectory();
                    ((TrajectoryKModel) trajectoryK).addListener(listenerTrajectoryK);
                    break;

                default:
                    // nothing to do for other cases
                    break;
            }
        }
    }

    /**
     * Start listening to a config.
     * 
     * @param config
     */
    public void stopListening() {
        if (config != null) {

            enableListening(config.getScanAddOn(), false);

            switch (config.getType()) {
                case SCAN_1D:
                    Config1DModel configModel = (Config1DModel) config;
                    configModel.removeListener(listenerConfig1D);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).removeListener(listenerSensor);
                    }
                    Dimension1DModel dimension1DModel = (Dimension1DModel) configModel.getDimensionX();
                    dimension1DModel.removeListener(listenerDimension1D);
                    for (IActuator actuator : dimension1DModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).removeListener(listenerActuator);
                    }
                    for (IRange1D range1D : dimension1DModel.getRangesXList()) {
                        ((Range1DModel) range1D).removeListener(listenerRange1D);
                        for (ITrajectory trajectory : range1D.getTrajectoriesList()) {
                            ((Trajectory1DModel) trajectory).removeListener(listenerTrajectory1D);
                        }
                    }
                    break;

                case SCAN_2D:
                    Config2DModel config2DModel = (Config2DModel) config;
                    config2DModel.removeListener(listenerConfig2D);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).removeListener(listenerSensor);
                    }

                    Dimension2DXModel dimension2DXModel = (Dimension2DXModel) config2DModel.getDimensionX();
                    dimension2DXModel.removeListener(listenerDimension2DX);

                    Dimension2DYModel dimension2DYModel = (Dimension2DYModel) config2DModel.getDimensionY();
                    dimension2DYModel.removeListener(listenerDimension2DY);

                    for (IActuator actuator : dimension2DXModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).removeListener(listenerActuator);
                    }

                    for (IActuator actuator : dimension2DYModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).removeListener(listenerActuator);
                    }

                    for (IRange2DX range2DX : dimension2DXModel.getRangesList()) {
                        ((Range2DXModel) range2DX).removeListener(listenerRange2DX);
                        for (ITrajectory trajectory : range2DX.getTrajectoriesList()) {
                            ((Trajectory2DXModel) trajectory).removeListener(listenerTrajectory2DX);
                        }
                    }

                    for (IRange2DY range2DY : dimension2DYModel.getRangesList()) {
                        ((Range2DYModel) range2DY).removeListener(listenerRange2DY);
                        for (ITrajectory trajectory : range2DY.getTrajectoriesList()) {
                            ((Trajectory2DYModel) trajectory).removeListener(listenerTrajectory2DY);
                        }
                    }
                    break;

                case SCAN_HCS:
                    ConfigHCSModel configHCSModel = (ConfigHCSModel) config;
                    configHCSModel.removeListener(listenerConfigHCS);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).removeListener(listenerSensor);
                    }
                    DimensionHCSModel dimensionHCSModel = (DimensionHCSModel) configHCSModel.getDimensionX();
                    dimensionHCSModel.removeListener(listenerDimensionHCS);
                    for (IActuator actuator : dimensionHCSModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).removeListener(listenerActuator);
                    }
                    for (IRangeHCS rangeHCS : dimensionHCSModel.getRangesXList()) {
                        ((RangeHCSModel) rangeHCS).removeListener(listenerRangeHCS);
                        for (ITrajectory trajectory : rangeHCS.getTrajectoriesList()) {
                            ((TrajectoryHCSModel) trajectory).removeListener(listenerTrajectoryHCS);
                        }
                    }
                    break;

                case SCAN_ENERGY:
                    ConfigEnergyModel configEnergyModel = (ConfigEnergyModel) config;
                    configEnergyModel.removeListener(listenerConfigEnergy);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).removeListener(listenerSensor);
                    }
                    DimensionEnergyModel dimensionEnergyModel = (DimensionEnergyModel) configEnergyModel
                            .getDimensionX();
                    dimensionEnergyModel.removeListener(listenerDimensionEnergy);
                    for (IActuator actuator : dimensionEnergyModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).removeListener(listenerActuator);
                    }
                    for (IRangeEnergy rangeEnergy : dimensionEnergyModel.getRangesEnergyList()) {
                        ((RangeEnergyModel) rangeEnergy).removeListener(listenerRangeEnergy);
                    }
                    for (ITrajectoryEnergy trajectoryEnergy : dimensionEnergyModel.getTrajectoriesEnergyList()) {
                        ((TrajectoryEnergyModel) trajectoryEnergy).removeListener(listenerTrajectoryEnergy);
                    }
                    break;

                case SCAN_K:
                    ConfigKModel configKModel = (ConfigKModel) config;
                    configKModel.removeListener(listenerConfigK);
                    for (ISensor sensor : config.getSensorsList()) {
                        ((SensorModel) sensor).removeListener(listenerSensor);
                    }
                    DimensionKModel dimensionKModel = (DimensionKModel) configKModel.getDimensionX();
                    dimensionKModel.removeListener(listenerDimensionK);
                    for (IActuator actuator : dimensionKModel.getActuatorsList()) {
                        ((ActuatorModel) actuator).removeListener(listenerActuator);
                    }
                    IRangeK rangeK = dimensionKModel.getRangeX();
                    ((RangeKModel) rangeK).removeListener(listenerRangeK);

                    ITrajectoryK trajectoryK = dimensionKModel.getTrajectory();
                    ((TrajectoryKModel) trajectoryK).removeListener(listenerTrajectoryK);
                    break;

                default:
                    // nothing to do for other cases
                    break;
            }
            config = null;
        }
    }

    /**
     * Method called when the config is changed.
     */
    public void configChanged() {
        if (config != null) {
            config.setModified(true);
        }
    }
}

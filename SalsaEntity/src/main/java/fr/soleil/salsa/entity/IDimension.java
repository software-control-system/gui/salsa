package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for the dimension entity. A dimension describes an actual dimension of a scan as a set
 * of movements to be performed by the actuators during the scan. It can be divided in ranges, which
 * are parts of the scan where the movement is linear (not present in interface).
 * 
 * @see fr.soleil.salsa.entity.ITrajectory
 * @see fr.soleil.salsa.entity.IActuator
 * @author Administrateur
 */
public interface IDimension extends Serializable {

    /**
     * Gets the actuators list.
     * 
     * @return
     */
    public List<IActuator> getActuatorsList();

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    public Integer getId();

    /**
     * Sets the actuators list.
     * 
     * @param actuatorsList
     */
    public void setActuatorsList(List<IActuator> actuatorsList);

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    public void setId(Integer id);

    public List<? extends IRange> getRangeList();

    public void setRangeList(List<? extends IRange> rangeList);

    public void addRange(IRange range);

    public boolean isEqualsTo(IDimension dimension);

}

package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IDisplay;
import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.event.ScanAddOnModel;

/**
 * @author Administrateur
 * 
 */
public class ScanAddOnImp implements IScanAddOns, IObjectImpl<IScanAddOns> {

    private static final long serialVersionUID = 5526000752238058804L;

    private IErrorStrategy errorStrategy = new ErrorStrategyImpl();

    private List<IHook> hooksList = new ArrayList<IHook>();

    private Integer id = 0;

    private IScanAddOns model = null;

    /**
     * The display manager parameters.
     */
    private IDisplay display = new DisplayImpl();

    /**
     * The post scan behaviour.
     */
    private IPostScanBehaviour postScanBehaviour = new PostScanBehaviourImpl();

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IScanAddOns#getPostScanBehaviour()
     */
    public IPostScanBehaviour getPostScanBehaviour() {
        return postScanBehaviour;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IScanAddOns#setPostScanBehaviour(fr.soleil.salsa.entity.IPostScanBehaviour)
     */
    public void setPostScanBehaviour(IPostScanBehaviour postScanBehaviour) {
        this.postScanBehaviour = postScanBehaviour;
    }

    public ScanAddOnImp() {
        super();
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj parameter is an
     * instance of the base interface : another implementation of the interface is considered equal
     * to this one if it represents the same entity.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        }
        else if (obj == null) {
            equals = false;
        }
        else if (!(obj instanceof IScanAddOns)) {
            equals = false;
        }
        else if (this.id == null) {
            // Approximation. The equals method of a bean that has not received
            // an identifier yet should not be called.
            equals = false;
        }
        else {
            equals = this.id.equals(((IScanAddOns) obj).getId());
        }
        return equals;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IScanAddOns#getErrorStrategy()
     */
    public IErrorStrategy getErrorStrategy() {
        return errorStrategy;
    }

    public List<IHook> getHooks() {
        return hooksList;
    }

    public Integer getId() {
        return id;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }

    /**
     * @param errorStrategy
     */
    /*
    public void setErrorStrategy(ErrorStrategyImp errorStrategy) {
        this.errorStrategy = errorStrategy;
    }
    */

    public void setHooks(List<IHook> hooksList) {
        this.hooksList = hooksList;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IScanAddOns#setErrorStrategy(fr.soleil.salsa.entity.IErrorStrategy)
     */
    @Override
    public void setErrorStrategy(IErrorStrategy errorStrategy) {
        this.errorStrategy = errorStrategy;
    }

    /**
     * The display manager parameters.
     * 
     * @return the display
     */
    @Override
    public IDisplay getDisplay() {
        return display;
    }

    /**
     * The display manager parameters.
     * 
     * @param display the display to set
     */
    @Override
    public void setDisplay(IDisplay display) {
        this.display = display;
    }

    @Override
    public IScanAddOns toModel() {
        if (model == null) {
            model = initModel();
        }
        return model;
    }

    protected IScanAddOns initModel() {
        ScanAddOnModel scanAddOnModel = new ScanAddOnModel(this);
        IErrorStrategy errorStategyImpl = getErrorStrategy();
        if (errorStategyImpl != null) {
            scanAddOnModel.setErrorStrategy(errorStategyImpl.toModel());
        }
        IDisplay displayImpl = getDisplay();
        if (errorStategyImpl != null) {
            scanAddOnModel.setDisplay(displayImpl.toModel());
        }

        IPostScanBehaviour scanBehaviourImpl = getPostScanBehaviour();
        if (scanBehaviourImpl != null) {
            scanAddOnModel.setPostScanBehaviour(scanBehaviourImpl.toModel());
        }

        List<IHook> hookListImpl = getHooks();
        if (hookListImpl != null) {
            List<IHook> hookListModel = new ArrayList<IHook>();
            for (IHook hookImpl : hookListImpl) {
                hookListModel.add(hookImpl.toModel());
            }
            scanAddOnModel.setHooks(hookListModel);
        }

        return scanAddOnModel;
    }

    @Override
    public IScanAddOns toImpl() {
        return this;
    }

}

package fr.soleil.salsa.entity.scanhcs;

import fr.soleil.salsa.entity.IActuatorTrajectory;
import fr.soleil.salsa.entity.IRangeTrajectory;

public interface ITrajectoryHCS extends IActuatorTrajectory, IRangeTrajectory<IRangeHCS> {

    /**
     * True if the delta is constant.
     * 
     * @return
     */
    public Boolean isDeltaConstant();

    /**
     * Set true if the delta is constant.
     * 
     * @param deltaConstant
     */
    public void setDeltaConstant(Boolean deltaConstant);
}

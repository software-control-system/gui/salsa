package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Interface for the scanResult entity. The result of a scan.
 */
public interface IScanResult extends Serializable {

    /**
     * The type of scan.
     */
    public enum ResultType {
        RESULT_1D, RESULT_2D, RESULT_TIMESCAN
    }

    /**
     * Return the complete name of the attribute that contains the sensors timestamps.
     * 
     * @return A {@link String} value
     */
    public String getSensorsTimeStampsCompleteName();

    /**
     * Return the complete name of the attribute that contains the actuators timestamps.
     * 
     * @return A {@link String} value
     */
    public String getActuatorsTimeStampsCompleteName();

    /**
     * Returns the name of this {@link IScanResult}'s scan server. <big>Expert usage only!</big>
     */
    public String getScanServer();

    /**
     * Sets the name of this {@link IScanResult}'s scan server. <big>Expert usage only!</big>
     * 
     * @param scanServer The scan server name to set
     */
    public void setScanServer(String scanServer);

    /**
     * The name of the current scan.
     * 
     * @return
     */
    public String getRunName();

    /**
     * The name of the current scan.
     * 
     * @return
     */
    public void setRunName(String runName);

    /**
     * Gets the list of actuators used in the scan on the X dimension.
     * 
     * @return actuatorsList
     */
    public List<IActuator> getActuatorsXList();

    /**
     * Sets the list of actuators used in the scan on the X dimension.
     * 
     * @param List<IActuator>
     */
    public void setActuatorsXList(List<IActuator> actuatorsList);

    /**
     * Gets the list of sensors used in the scan.
     * 
     * @return sensorsList
     */
    public List<ISensor> getSensorsList();

    /**
     * Sets the list of sensors used in the scan.
     * 
     * @param List<ISensor>
     */
    public void setSensorsList(List<ISensor> sensorsList);

    /**
     * The type of scan.
     */
    public ResultType getResultType();

    /**
     * The type of scan.
     */
    public void setResultType(ResultType resultType);

    /**
     * Read the real trajectory set on the given actuator
     */
    Map<IActuator, double[]> getTrajectoryMap();

    /**
     * Set the Map of trajectory
     */
    public void setTrajectoryMap(Map<IActuator, double[]> trajectoryMap);

}

package fr.soleil.salsa.entity.event;

import java.util.Map;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IScanPoint;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.impl.ScanPointImpl;

public class ScanPointModel extends AEventHandlingModelDecorator<IScanPoint> implements IScanPoint {

    private static final long serialVersionUID = -3842604100842231840L;

    /**
     * The constructor
     * 
     * @param scanPoint the decorated base bean.
     */
    public ScanPointModel(IScanPoint scanPoint) {
        super(scanPoint);
    }

    /**
     * Default constructor, that creates a new instance of ScanPointImpl and wraps it.
     */
    public ScanPointModel() {
        this(new ScanPointImpl());
    }

    /**
     * @return
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#getActuatorsXValuesMap()
     */
    public Map<IActuator, Double> getActuatorsXValuesMap() {
        return baseBean.getActuatorsXValuesMap();
    }

    /**
     * @return
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#getActuatorsYValuesMap()
     */
    public Map<IActuator, Double> getActuatorsYValuesMap() {
        return baseBean.getActuatorsYValuesMap();
    }

    /**
     * @return
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#getSensorsValuesMap()
     */
    public Map<ISensor, Double> getSensorsValuesMap() {
        return baseBean.getSensorsValuesMap();
    }

    /**
     * @return
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#getTime()
     */
    public Double getTime() {
        return baseBean.getTime();
    }

    /**
     * @param actuator
     * @return
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#getValue(fr.soleil.salsa.entity.IActuator)
     */
    public Double getValue(IActuator actuator) {
        return baseBean.getValue(actuator);
    }

    /**
     * @param sensor
     * @return
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#getValue(fr.soleil.salsa.entity.ISensor)
     */
    public Double getValue(ISensor sensor) {
        return baseBean.getValue(sensor);
    }

    /**
     * @param actuatorsValuesMap
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#setActuatorsXValuesMap(java.util.Map)
     */
    public void setActuatorsXValuesMap(Map<IActuator, Double> actuatorsXValuesMap) {
        Map<IActuator, Double> oldValue = baseBean.getActuatorsXValuesMap();
        baseBean.setActuatorsXValuesMap(actuatorsXValuesMap);
        this.firePropertyChange("actuatorsXValuesMap", oldValue, actuatorsXValuesMap);
    }

    /**
     * @param actuatorsValuesMap
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#setActuatorsYValuesMap(java.util.Map)
     */
    public void setActuatorsYValuesMap(Map<IActuator, Double> actuatorsYValuesMap) {
        Map<IActuator, Double> oldValue = baseBean.getActuatorsYValuesMap();
        baseBean.setActuatorsYValuesMap(actuatorsYValuesMap);
        this.firePropertyChange("actuatorsYValuesMap", oldValue, actuatorsYValuesMap);
    }

    /**
     * @param sensorsValuesMap
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#setSensorsValuesMap(java.util.Map)
     */
    public void setSensorsValuesMap(Map<ISensor, Double> sensorsValuesMap) {
        Map<ISensor, Double> oldValue = baseBean.getSensorsValuesMap();
        baseBean.setSensorsValuesMap(sensorsValuesMap);
        this.firePropertyChange("sensorsValuesMap", oldValue, sensorsValuesMap);
    }

    /**
     * @param time
     * @see fr.soleil.salsa.entity.impl.ScanPointImpl#setTime(java.lang.Double)
     */
    public void setTime(Double time) {
        Double oldValue = baseBean.getTime();
        baseBean.setTime(time);
        this.firePropertyChange("time", oldValue, time);
    }
}

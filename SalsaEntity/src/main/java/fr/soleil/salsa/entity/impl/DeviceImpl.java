package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IDevice;

public class DeviceImpl implements IDevice {

    private static final long serialVersionUID = -1392782382245948255L;

    IDevice model = null;
    /**
     * The unique identifier.
     */
    protected Integer id;

    /**
     * The sensor name.
     */
    protected String name;

    /**
     * The scan server sensors name's
     */
    protected String scanServerAttributeName;

    /**
     * True if the sensor is enabled.
     */
    protected boolean enabled;

    private boolean common = true;

    public DeviceImpl() {
        super();
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (name != null) {
            name = name.trim();
        }
        this.name = name;
    }

    @Override
    public String getScanServerAttributeName() {
        return scanServerAttributeName;
    }

    @Override
    public void setScanServerAttributeName(String scanServerAttributeName) {
        this.scanServerAttributeName = scanServerAttributeName;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public IDevice toModel() {
        if (model == null) {
            model = initModel();
        }
        return model;
    }

    protected IDevice initModel() {
        return null;
    }

    @Override
    public IDevice toImpl() {
        return this;
    }

    @Override
    public boolean isCommon() {
        return common;
    }

    @Override
    public void setCommon(boolean common) {
        this.common = common;
    }

    @Override
    public String toString() {
        String value = super.toString();
        if (name != null) {
            value = name;
        }
        return value;
    }

}

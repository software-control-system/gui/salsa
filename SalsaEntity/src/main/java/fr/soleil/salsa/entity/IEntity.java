package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Common interface for directories and scan configurations
 * 
 * @author girardot
 */
public interface IEntity extends Serializable {

    /**
     * Gets the unique identifier.
     * 
     * @return An {@link Integer}
     */
    public Integer getId();

    /**
     * Sets the unique identifier.
     * 
     * @param id The identifier to set
     */
    public void setId(Integer id);

    /**
     * Gets the entity name.
     * 
     * @return A {@link String}
     */
    public String getName();

    /**
     * Sets the entity name.
     * 
     * @param name The name to set
     */
    public void setName(String name);

    /**
     * Gets the directory containing this entity. May be <code>null</code>
     * 
     * @return An {@link IDirectory}
     */
    public IDirectory getDirectory();

    /**
     * Sets the directory containing this entity.
     * 
     * @param directory The directory to set
     */
    public void setDirectory(IDirectory directory);

    /**
     * Gets the position in the directory, indicating the basic sort order.
     * 
     * @return An {@link Integer}
     */
    public Integer getPositionInDirectory();

    /**
     * Sets the position in the directory, indicating the basic sort order.
     * 
     * @param positionInDirectory The position to set
     */
    public void setPositionInDirectory(Integer positionInDirectory);

    /**
     * Get the timestamp.
     * 
     * @return A {@link Timestamp}
     */
    public Timestamp getTimestamp();

    /**
     * Set the timestamp.
     * 
     * @param timestamp The {@link Timestamp} to set
     */
    public void setTimestamp(Timestamp timestamp);

    /**
     * Returns the full path of this entity
     * 
     * @return A {@link String}
     */
    public String getFullPath();

}

package fr.soleil.salsa.entity.impl.scanenergy;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.event.RangeEnergyModel;
import fr.soleil.salsa.entity.impl.RangeImpl;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;

/**
 * @author Alike
 * 
 */
public class RangeEnergyImpl extends RangeImpl implements IRangeEnergy {

    private static final long serialVersionUID = -2940032437003980319L;

    /**
     * The integration time.
     */
    private double[] integrationTime;

    /**
     * Get the integration time.
     * 
     * @return
     */
    @Override
    public double[] getIntegrationTime() {
        return integrationTime;
    }

    /**
     * Set the integration time.
     * 
     * @param integrationTime
     */
    @Override
    public void setIntegrationTime(double[] integrationTime) {
        this.integrationTime = integrationTime;
    }

    @Override
    protected IRange initModel() {
        return new RangeEnergyModel(this);
    }

}

package fr.soleil.salsa.entity.impl.scanhcs;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.event.DimensionHCSModel;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.util.ComparatorUtil;

public class DimensionHCSImpl extends DimensionImpl implements IDimensionHCS {

    private static final long serialVersionUID = -5972774836785837543L;

    /**
     * The HCS range list.
     */
    private List<IRangeHCS> rangesXList;

    /**
     * The HCS trajectory list.
     */
    private List<ITrajectoryHCS> trajectoriesList;

    /**
     * Constructor.
     */
    public DimensionHCSImpl() {
        super();
        this.rangesXList = new ArrayList<IRangeHCS>();
        this.trajectoriesList = new ArrayList<ITrajectoryHCS>();
    }

    /**
     * Gets the list of HCS ranges.
     */
    public List<IRangeHCS> getRangesXList() {
        return rangesXList;
    }

    /**
     * Gets the list of HCS trajectories.
     */
    public List<ITrajectoryHCS> getTrajectoriesList() {
        return trajectoriesList;
    }

    /**
     * Sets the list of HCS ranges.
     */
    public void setRangesXList(List<IRangeHCS> rangesX) {

        this.rangesXList = rangesX;
    }

    /**
     * Sets the list of HCS trajectories.
     */
    public void setTrajectoriesList(List<ITrajectoryHCS> trajectories) {
        this.trajectoriesList = trajectories;
    }

    protected IDimension initModel() {
        return new DimensionHCSModel(this);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null && super.isEqualsTo(dimension)) {
            if (dimension instanceof IDimensionHCS) {
                equals = ComparatorUtil.rangeListEquals(
                        ((IDimensionHCS) dimension).getRangesXList(), getRangesXList());
            }
        }
        return equals;
    }

}

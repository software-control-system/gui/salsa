package fr.soleil.salsa.entity;

public interface IActuatorTrajectory extends ITrajectory {

    /**
     * Get the actuator.
     * 
     * @return
     */
    public IActuator getActuator();

    /**
     * Set the actuator.
     * 
     * @param actuator
     */
    public void setActuator(IActuator actuator);

}

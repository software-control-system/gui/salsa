package fr.soleil.salsa.entity;

import java.io.Serializable;

import fr.soleil.salsa.entity.impl.ErrorStrategyType;

/**
 * @author Alike
 * 
 *         The interface relative to the error strategy item.
 */
public interface IErrorStrategyItem extends Serializable {

    /**
     * Error strategy for timebases.
     */
    int getRetryCount();

    /**
     * Gets the strategy.
     * 
     * @return
     */
    ErrorStrategyType getStrategy();

    /**
     * Get the time between retries.
     * 
     * @return
     */
    double getTimeBetweenRetries();

    /**
     * Gets the time out.
     * 
     * @return
     */
    double getTimeOut();

    /**
     * Sets the number of retries.
     * 
     * @param retryCount
     */
    void setRetryCount(int retryCount);

    /**
     * Sets the strategy.
     * 
     * @param strategy
     */
    void setStrategy(ErrorStrategyType strategy);

    /**
     * Sets the time between retries.
     * 
     * @param timeBetweenRetries
     */
    void setTimeBetweenRetries(double timeBetweenRetries);

    /**
     * Sets the time out.
     * 
     * @param timeOut
     */
    void setTimeOut(double timeOut);

    public IErrorStrategyItem toModel();

    public IErrorStrategyItem toImpl();

}

package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

public interface IScanAddOns extends Serializable {

    public IErrorStrategy getErrorStrategy();

    public List<IHook> getHooks();

    public Integer getId();

    public void setErrorStrategy(IErrorStrategy errorStrategy);

    public void setHooks(List<IHook> hooksList);

    public void setId(Integer id);

    /**
     * Gets the post scan behaviour.
     * 
     * @return
     */
    IPostScanBehaviour getPostScanBehaviour();

    /**
     * Sets the post scan behaviour.
     * 
     * @param postScanBehaviour
     */
    void setPostScanBehaviour(IPostScanBehaviour postScanBehaviour);

    /**
     * The display manager parameters.
     * 
     * @return the display
     */
    public IDisplay getDisplay();

    /**
     * The display manager parameters.
     * 
     * @param display the display to set
     */
    public void setDisplay(IDisplay display);

    public IScanAddOns toModel();

    public IScanAddOns toImpl();

}

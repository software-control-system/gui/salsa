package fr.soleil.salsa.entity;

import java.io.Serializable;

public interface IScanStatus extends Serializable {

    public String getState();

    public String getStatus();

    public String getRunStartDate();

    public String getScanStartDate();

    public String getScanEndDate();

    public String getRunEndDate();

    public String getScanDuration();

    public String getRunDuration();

    public String getScanRemainingTime();

    public String getRunRemainingTime();

    public String getScanElapsed();

    public String getRunElapsed();

    public double getRunCompletion();

    public double getScanCompletion();

    public double getDeadTime();

    public double getDeadTimePercentage();

    public double getDeadTimePerPoint();

}

package fr.soleil.salsa.entity.impl;

import java.util.ResourceBundle;

import org.slf4j.Logger;

import fr.soleil.salsa.logging.LoggingUtil;
public class Messages {

    public static final Logger LOGGER = LoggingUtil.getLogger(Messages.class);

    private static final String BUNDLE_NAME = "messages"; //$NON-NLS-1$
    private static ResourceBundle RESOURCE_BUNDLE = null;
    static {
        try {
            RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        catch (Exception exc) {
            LOGGER.error("Cannot read ressource file {}", exc.getMessage());
            LOGGER.debug("Stack trace", exc);
        }
    }

    private Messages() {
    }

    public static String getString(String key) {
        try {
            return RESOURCE_BUNDLE.getString(key);
        }
        catch (Exception e) {
            LOGGER.error("Key not found {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
            return '!' + key + '!';
        }
    }
}

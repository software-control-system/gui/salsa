package fr.soleil.salsa.entity.impl;

import java.util.Set;

import fr.soleil.salsa.entity.Behaviour;

/**
 * Class relative to the Behaviour.
 * 
 * @author Alike
 * 
 */
public class BehaviourHookImp extends HookImpl {

    private static final long serialVersionUID = -8269833779796577230L;

    /**
     * Actions.
     */
    private Set<BehaviourActionImp> actions;

    /**
     * Behaviour.
     */
    private Behaviour behaviour;

    /**
     * Get actions.
     * 
     * @return
     */
    public Set<BehaviourActionImp> getActions() {
        return actions;
    }

    /**
     * Get the behaviour.
     * 
     * @return
     */
    public Behaviour getBehaviour() {
        return behaviour;
    }

    /**
     * Set actions.
     * 
     * @param actions
     */
    public void setActions(Set<BehaviourActionImp> actions) {
        this.actions = actions;
    }

    /**
     * Set the behaviour.
     * 
     * @param behaviour
     */
    public void setBehaviour(Behaviour behaviour) {
        this.behaviour = behaviour;
    }

}

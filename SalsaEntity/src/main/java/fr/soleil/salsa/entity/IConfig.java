package fr.soleil.salsa.entity;

import java.util.List;

import fr.soleil.salsa.exception.SalsaException;

/**
 * Interface for the config entity. This is the configuration data for a scan.
 *
 * @author Administrateur
 */
public interface IConfig<T extends IDimension> extends IEntity {

    public enum ScanType {
        SCAN_1D, SCAN_2D, SCAN_HCS, SCAN_K, SCAN_ENERGY, SCAN_FLY
    }

    public int FIRST_DIMENSION = 1;

    public int SECOND_DIMENSION = 2;

    /**
     * Set a custom runName (instead of configuration name by default)
     */
    public void setRunName(String runName);

    /**
     * Get the custom runName
     */
    public String getRunName();

    public IDevice getDevice(final String deviceName, List<? extends IDevice> deviceList);

    /**
     * Get the actuator delay.
     *
     * @return
     */
    public double getActuatorsDelay();

    /**
     * Get the timebases delay.
     *
     * @return
     */
    public double getTimebasesDelay();

    /**
     * Gets the scan number.
     *
     * @return
     */
    public int getScanNumber();

    /**
     * Gets the sensors list.
     *
     * @return sensorsList
     */
    public List<ISensor> getSensorsList();

    /**
     * get the actuator list
     *
     * @return actuatorList
     */
    public List<IActuator> getActuatorList(int dimension);

    /**
     * Gets the timebase list.
     *
     * @return List<ITimebase> : timebaseList
     */
    public List<ITimebase> getTimebaseList();

    /**
     * Returns the last date (in milliseconds) when this config was set
     * modified.
     *
     * @return a long.
     */
    public long getLastModificationDate();

    /**
     * Gets the ScanAddOn
     *
     * @return
     */
    public IScanAddOns getScanAddOn();

    /**
     * Gets the scan type.
     *
     * @return
     */
    public ScanType getType();

    /**
     * Gets true if the scan speed is enabled.
     *
     * @return
     */
    public boolean isEnableScanSpeed();

    /**
     * Gets true if the object data are fully available. (if false, only id,
     * type and timestamp are provided).
     *
     * @return
     */
    public boolean isLoaded();

    /**
     * Get true if the scan is on the fly.
     *
     * @return
     */
    public boolean isOnTheFly();

    /**
     * Gets true if the scan is zig zag.
     *
     * @return
     */
    public boolean isZigzag();

    /**
     * Sets the actuator delay.
     *
     * @param actuatorsDelay
     */
    public void setActuatorsDelay(double actuatorsDelay);

    /**
     * Sets the timebases delay.
     *
     * @param timebasesDelay
     */
    public void setTimebasesDelay(double timebasesDelay);

    /**
     * Sets true if the scan speed is enabled.
     *
     * @param enableScanSpeed
     */
    public void setEnableScanSpeed(boolean enableScanSpeed);

    /**
     * Sets to true if the obejct data are fully available. (if false, only id,
     * type and timestamp are provided).
     *
     * @param loaded
     */
    public void setLoaded(boolean loaded);

    /**
     * Sets true if the scan is on the fly.
     *
     * @param onTheFly
     */
    public void setOnTheFly(boolean onTheFly);

    /**
     * Sets the scan add on.
     *
     * @param scanAddOn
     */
    public void setScanAddOn(IScanAddOns scanAddOn);

    /**
     * Sets the scan number.
     *
     * @param scanNumber
     */
    public void setScanNumber(int scanNumber);

    /**
     * Sets the sensors list.
     *
     * @param List
     *            <ISensor>
     */
    public void setSensorsList(List<ISensor> sensorsList);

    /**
     * Sets the timebase list.
     *
     * @param List
     *            <ITimebase>
     */
    public void setTimebaseList(List<ITimebase> timebaseList);

    /**
     * Sets the scan type.
     *
     * @return
     */
    public void setType(ScanType type);

    /**
     * Sets true if the scan is zig zag.
     *
     * @param zigzag
     */
    public void setZigzag(boolean zigzag);

    /**
     * True if the configuration has been modified since its last change.
     *
     * @return
     */
    public boolean isModified();

    /**
     * True if the configuration has been modified since its last change.
     *
     * @param modified
     */
    public void setModified(boolean modified);

    /**
     * return the script extraction associated to the scan configuration
     *
     * @return
     */
    public String getDataRecorderConfig();

    /**
     * Set the script extraction associated to the scan configuration
     *
     * @param modified
     */
    public void setDataRecorderConfig(String config);

    /**
     * Returns the X dimension
     *
     * @return an {@link IDimension}
     */
    public T getDimensionX();

    /**
     * Sets the X dimension
     *
     * @param dimension
     *            The dimension to set
     */
    public void setDimensionX(T dimension);

    public List<IDimension> getDimensionList();

    /**
     * Check if the configuration is valid
     *
     * @return the validity
     */

    public boolean isValid();

    public boolean isForcedSaving();

    public void setForcedSaving(boolean forcedSaving);

    /**
     * active sensor contain in sensorList and disable others
     *
     * @param sensorList
     *            list of sensor to active
     * @throws SalsaException
     *             if sensorList contain sensor which is not in this
     *             configuration
     */
    public void activateSensors(String[] sensorList) throws SalsaException;

    /**
     * active actuator contain in actuatorList and disable others
     *
     * @param actuatorList
     *            list of actuator to active
     * @throws SalsaException
     *             if actuatorList contain actuator which is not in this
     *             configuration
     */
    public void activateActuators(String[] actuatorList, int dimension) throws SalsaException;

    /**
     * active timeBase contain in timeBaseList and disable others
     *
     * @param timeBaseList
     *            list of timeBase to active
     * @throws SalsaException
     *             if timeBaseList contain timeBase which is not in this
     *             configuration
     */
    public void activateTimebase(String[] timebaseList) throws SalsaException;

    /**
     * enabled or disable sensor contained in sensorList
     *
     * @param sensorName
     *            the name of sensor to enable or disable
     * @param enabled
     *            , true to enable else false
     */
    public void setSensorEnable(final String sensorName, boolean enabled);

    /**
     * get if a sensor is enable or disable
     *
     * @param sensorName
     *            the name of sensor to enable or disable
     * @return true if the sensor is enabled
     */
    public boolean isSensorEnable(final String sensorName);

    /**
     * enabled or disable actuator contained in actuatorList
     *
     * @param actuatorName
     *            the name of actuator to enable or disable
     * @param enabled
     *            , true to enable else false
     */
    public void setActuatorEnable(final String actuatorName, boolean enabled, int dimension);

    /**
     * get if a actuator is enable or disable
     *
     * @param actuatorName
     *            the name of actuator to enable or disable
     * @return true if the actuator is enabled
     */
    public boolean isActuatorEnable(final String actuatorName, int dimension);

    /**
     * enabled or disable timeBase contained in timeBaseList
     *
     * @param timeBaseName
     *            the name of timeBase to enable or disable
     * @param enabled
     *            , true to enable else false
     */
    public void setTimeBaseEnable(final String timeBaseName, boolean enabled);

    /**
     * get if a timeBase is enable or disable
     *
     * @param timeBaseName
     *            the name of timeBase to enable or disable
     * @return true if the timeBase is enabled
     */
    public boolean isTimeBaseEnable(final String timeBaseName);

    public void swapActuator(String actuatorName1, String actuatorName2, int dimension);

    public void addActuator(String actuatorName, int dimension);

    public void deleteActuator(String actuatorName, int dimension);

    public void renameActuator(String oldActuatorName, String newActuatorName, int dimension);

    public void addSensor(String sensorName);

    public void deleteSensor(String sensorName);

    public void renameSensor(String oldSensorName, String newSensorName);

    public void swapSensor(String sensorName1, String sensorName2);

    public void addTimeBase(String timebaseName);

    public void deleteTimeBase(String timebaseName);

    public void renameTimeBase(String oldTimeBaseName, String newTimeBaseName);

    public void swapTimeBase(String timeBaseName1, String timeBaseName2);

    public List<IDevice> getActivatedActuatorsList();

    public List<IDevice> getActivatedSensorsList();

    public List<IDevice> getActivatedTimebasesList();

    /**
     * Get the the Scan information that will be stored in NexusFile
     *
     * @return n according to the following syntax group/child/dataitem:value
     */
    public String[] getScanInfo();

    public IConfig<T> toImpl();

}

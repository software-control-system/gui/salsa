package fr.soleil.salsa.entity.flyscan;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ITrajectory;

/**
 * Class relative to Fly Scan config
 * 
 * @author Saintin
 * 
 */
public interface IConfigFlyScan extends IConfig<IDimension> {

    /**
     * Add a new dimension to the configuration
     */
    public IDimension addDimension();

    /**
     * remove the last dimension of the configuration
     */
    public void removeDimension();

    /**
     * Get the list of actuator for a given dimension
     * 
     * @param dimension
     * @return list of actuator
     */

    public List<IActuator> getActuatorList(IDimension dimension);

    /**
     * Add an actuator to a given dimension
     * 
     * @param dimension
     * @param actuator
     * 
     */
    public void addActuator(IDimension dimension, IActuator actuator);

    /**
     * Remove an actuator to a given dimension
     * 
     * @param dimension
     * @param actuator
     * 
     */
    public void removeActuator(IDimension dimension, IActuator actuator);

    /**
     * Set the actuator name.
     * 
     * @param actuator
     * @param String, the name of the actuator
     */
    public void setActuatorName(IActuator actuator, String name);

    /**
     * Enable or disable an actuator.
     * 
     * @param actuator
     * @param enabled
     */
    void setActuatorEnabled(IActuator actuator, boolean enabled);

    /**
     * Set the trajectory for a given IActuator
     * 
     * @param actuator
     * @param trajectory
     */
    public void setTrajectory(IActuator actuator, ITrajectory trajectory);

    /**
     * Get a trajectory for a given IActuator
     * 
     * @param actuator
     */
    public ITrajectory getTrajectory(IActuator actuator);

}

package fr.soleil.salsa.entity.scanenergy;

import java.util.List;

import fr.soleil.salsa.entity.IDimension;


public interface IDimensionEnergy extends IDimension  {

    /**
     * Get the range.
     * @return
     */
    public List<IRangeEnergy> getRangesEnergyList();

    /**
     * Get trajectories.
     * @return
     */
    public List<ITrajectoryEnergy> getTrajectoriesEnergyList();

    /**
     * Set ranges.
     * @param range
     */
    public void setRangesEnergyList(List<IRangeEnergy> rangeEnergy);

    /**
     * Set trajectories.
     * @param trajectories
     */
    public void setTrajectoriesEnergyList(List<ITrajectoryEnergy> trajectoriesEnergy);
}

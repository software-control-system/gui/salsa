package fr.soleil.salsa.entity;

public enum DisplayAxis {
    X, Y1, Y2, Z, NONE
}

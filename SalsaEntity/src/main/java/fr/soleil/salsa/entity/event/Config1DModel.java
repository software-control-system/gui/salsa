package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.impl.scan1d.Config1DImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;

/**
 * Event handling decorator of {@link IConfig1D} for the config1D entity.
 */
public class Config1DModel extends AConfigModel<IDimension1D, IConfig1D> implements IConfig1D {

    private static final long serialVersionUID = -5879540625712520342L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first
     * parameter.
     * 
     * @param config1D
     *            the decorated base bean.
     */
    public Config1DModel(IConfig1D config1D) {
        super(config1D);
    }

    /**
     * Default constructor, that creates a new instance of {@link Config1DImpl}
     * and wraps it.
     */
    public Config1DModel() {
        this(new Config1DImpl());
    }

}

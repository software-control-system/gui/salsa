package fr.soleil.salsa.entity.scan2D;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IScanResult;

public interface IScanResult2D extends IScanResult {

    /**
     * Gets the list of actuators used in the scan on the Y dimension.
     * 
     * @return actuatorsList
     */
    public List<IActuator> getActuatorsYList();

    /**
     * Sets the list of actuators used in the scan on the Y dimension.
     * 
     * @param List<IActuator>
     */
    public void setActuatorsYList(List<IActuator> actuatorsList);

}

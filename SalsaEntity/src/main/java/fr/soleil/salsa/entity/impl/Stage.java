package fr.soleil.salsa.entity.impl;

/**
 * Class used to represent the Stages which can be associated with a Hook.
 * 
 * @author GIRARDOT
 */
public enum Stage {

    PRE_RUN(Messages.getString("Stage.PRE_RUN")), PRE_SCAN(Messages.getString("Stage.PRE_SCAN")),
    PRE_STEP(Messages.getString("Stage.PRE_STEP")), POST_ACTUATOR_MOVE(Messages.getString("Stage.POST_ACTUATOR_MOVE")),
    POST_INTEGRATION(Messages.getString("Stage.POST_INTEGRATION")), POST_STEP(Messages.getString("Stage.POST_STEP")),
    POST_SCAN(Messages.getString("Stage.POST_SCAN")), POST_RUN(Messages.getString("Stage.POST_RUN"));

    private final String value;

    private Stage(String value) {
        this.value = value;
    }

    /**
     * @return a short description of the stage
     */
    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return getValue();
    }

}

package fr.soleil.salsa.entity.impl;

public enum ErrorStrategyType {
    IGNORE(0),
    PAUSE(1),
    ABORT(2);

    private int value;

    ErrorStrategyType (int value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    public int getValue () {
        return value;
    }
}

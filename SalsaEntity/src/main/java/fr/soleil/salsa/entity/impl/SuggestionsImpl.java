package fr.soleil.salsa.entity.impl;

import java.util.List;

import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestions;

public class SuggestionsImpl implements ISuggestions {

    private static final long serialVersionUID = -5252649169465742263L;

    /**
     * Suggestions for actuators.
     */
    private List<ISuggestionCategory> actuatorSuggestionList;

    /**
     * Suggestions for sensors.
     */
    private List<ISuggestionCategory> sensorSuggestionList;

    /**
     * Suggestions for timebase.
     */
    private List<ISuggestionCategory> timebaseSuggestionList;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestions#getActuatorSuggestionList()
     */
    public List<ISuggestionCategory> getActuatorSuggestionList() {
        return actuatorSuggestionList;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestions#getSensorSuggestionList()
     */
    public List<ISuggestionCategory> getSensorSuggestionList() {
        return sensorSuggestionList;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestions#getTimebaseSuggestionList()
     */
    public List<ISuggestionCategory> getTimebaseSuggestionList() {
        return timebaseSuggestionList;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestions#setActuatorSuggestionList(java.util.List)
     */
    public void setActuatorSuggestionList(List<ISuggestionCategory> actuatorSuggestionList) {
        this.actuatorSuggestionList = actuatorSuggestionList;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestions#setSensorSuggestionList(java.util.List)
     */
    public void setSensorSuggestionList(List<ISuggestionCategory> sensorSuggestionList) {
        this.sensorSuggestionList = sensorSuggestionList;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestions#setTimebaseSuggestionList(java.util.List)
     */
    public void setTimebaseSuggestionList(List<ISuggestionCategory> timebaseSuggestionList) {
        this.timebaseSuggestionList = timebaseSuggestionList;
    }

}

package fr.soleil.salsa.entity.impl.scan1d;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IScanPoint;
import fr.soleil.salsa.entity.ISensor;

/**
 * The implementation for the scan point entity.<br />
 * A point in a scan.<br />
 * This contains the values the time, the actuators and the sensors had at a point in a scan.<br />
 * 
 * @author Administrateur
 */
public class ScanPointImpl implements IScanPoint {

    private static final long serialVersionUID = -4060524312866739037L;

    /**
     * The time.
     */
    private Double time;

    /**
     * The values of the actuators on the X dimension.
     */
    private Map<IActuator, Double> actuatorsXValuesMap;

    /**
     * The values of the actuators on the Y dimension.
     */
    private Map<IActuator, Double> actuatorsYValuesMap;

    /**
     * The values of the sensors.
     */
    private Map<ISensor, Double> sensorsValuesMap;

    /**
     * Constructeur.
     */
    public ScanPointImpl() {
        this.actuatorsXValuesMap = new HashMap<IActuator, Double>();
        this.actuatorsYValuesMap = new HashMap<IActuator, Double>();
        this.sensorsValuesMap = new HashMap<ISensor, Double>();
    }

    /**
     * The time.
     * 
     * @return the time
     */
    @Override
    public Double getTime() {
        return time;
    }

    /**
     * The time.
     * 
     * @param time the time to set
     */
    @Override
    public void setTime(Double time) {
        this.time = time;
    }

    /**
     * The values of the actuators.
     * 
     * @return the actuatorsValuesMap
     */
    @Override
    public Map<IActuator, Double> getActuatorsXValuesMap() {
        return actuatorsXValuesMap;
    }

    /**
     * The values of the actuators on the X dimension.
     * 
     * @param actuatorsValuesMap the actuatorsValuesMap to set
     */
    @Override
    public void setActuatorsXValuesMap(Map<IActuator, Double> actuatorsXValuesMap) {
        this.actuatorsXValuesMap = actuatorsXValuesMap;
    }

    /**
     * The values of the actuators on the Y dimension.
     */
    @Override
    public Map<IActuator, Double> getActuatorsYValuesMap() {
        return actuatorsYValuesMap;
    }

    /**
     * The values of the actuators on the Y dimension.
     */
    @Override
    public void setActuatorsYValuesMap(Map<IActuator, Double> actuatorsYValuesMap) {
        this.actuatorsYValuesMap = actuatorsYValuesMap;
    }

    /**
     * The values of the sensors on the X dimension.
     * 
     * @return the sensorsValuesMap
     */
    @Override
    public Map<ISensor, Double> getSensorsValuesMap() {
        return sensorsValuesMap;
    }

    /**
     * The values of the sensors.
     * 
     * @param sensorsValuesMap the sensorsValuesMap to set
     */
    @Override
    public void setSensorsValuesMap(Map<ISensor, Double> sensorsValuesMap) {
        this.sensorsValuesMap = sensorsValuesMap;
    }

    /**
     * The value of an actuator.
     * 
     * @param actuator
     * @return
     */
    @Override
    public Double getValue(IActuator actuator) {
        return this.actuatorsXValuesMap.get(actuator);
    }

    /**
     * The value of a sensor.
     * 
     * @param sensor
     * @return
     */
    @Override
    public Double getValue(ISensor sensor) {
        return this.sensorsValuesMap.get(sensor);
    }

    /**
     * @param obj
     * @return
     * @see java.lang.Double#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == this) {
            equals = true;
        }
        else if (obj == null) {
            equals = false;
        }
        else if (!(obj instanceof IScanPoint)) {
            equals = false;
        }
        else {
            equals = time != null ? time.equals(((IScanPoint) obj).getTime()) : ((IScanPoint) obj)
                    .getTime() == null;
        }
        return equals;
    }

    /**
     * @return
     * @see java.lang.Double#hashCode()
     */
    @Override
    public int hashCode() {
        return time.hashCode();
    }
}

package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.impl.ConfigImpl;

/**
 * Event handling decorator of {@link IConfig} for the config entity. This is the configuration data
 * for a scan. This adds the "modified" property over the ones already in IConfig.
 * 
 * @author Administrateur
 */
public abstract class ConfigModel<T extends IDimension> extends AConfigModel<T, IConfig<T>>
        implements IConfig<T> {

    private static final long serialVersionUID = 385864780359390422L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param config the decorated base bean.
     */
    public ConfigModel(IConfig<T> config) {
        super(config);
    }

    /**
     * Default constructor, that creates a new instance of ConfigImpl and wraps it.
     */
    public ConfigModel() {
        this(new ConfigImpl<T>());
    }

}

package fr.soleil.salsa.entity;

public interface IObjectImpl<T> {

    public T toModel();

}

package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.event.SensorModel;

/**
 * Implementation of {@link ISensor} for the sensor entity. A sensor is a detector that measures the
 * scanned values.
 * 
 * @author Administrateur
 */
public class SensorImpl extends DeviceImpl implements ISensor {

    private static final long serialVersionUID = 3851923003219924920L;

    /**
     * Constructor.
     */
    public SensorImpl() {
        super();
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj parameter is an
     * instance of the base interface : another implementation of the interface is considered equal
     * to this one if it represents the same entity.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        }
        else if (obj == null) {
            equals = false;
        }
        else if (!(obj instanceof ISensor)) {
            equals = false;
        }
        else {
            String thatName = ((ISensor) obj).getName();
            equals = name != null ? name.equals(thatName) : thatName == null;
        }
        return equals;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.name != null ? this.name.hashCode() : 0;
    }

    protected IDevice initModel() {
        return new SensorModel(this);
    }

}

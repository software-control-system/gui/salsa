package fr.soleil.salsa.entity.scank;

import fr.soleil.salsa.entity.IRangeTrajectory;

public interface ITrajectoryK extends IRangeTrajectory<IRangeK> {

    /**
     * Get E0
     * 
     * @return
     */
    public double getE0();

    /**
     * Set E0
     * 
     * @return
     */
    public void setE0(double e0);

    /**
     * Get E1
     * 
     * @return
     */
    public double getE1();

    /**
     * Set E1
     * 
     * @return
     */
    public void setE1(double e1);

    /**
     * Get E2
     * 
     * @return
     */
    public double getE2();

    /**
     * Set E2
     * 
     * @return
     */
    public void setE2(double e2);

    /**
     * Get eDeltaEdge
     * 
     * @return
     */
    public double getEDeltaEdge();

    /**
     * Set eDeltaEdge
     * 
     * @return
     */
    public void setEDeltaEdge(double eDeltaEdge);

    /**
     * Get eDeltaPreEdge
     * 
     * @return
     */
    public double getEDeltaPreEdge();

    /**
     * Set eDeltaPreEdge
     * 
     * @return
     */
    public void setEDeltaPreEdge(double eDeltaPreEdge);

    /**
     * Get Emin
     * 
     * @return
     */
    public double getEMin();

    /**
     * Set EMin
     * 
     * @return
     */
    public void setEMin(double eMin);

    /**
     * Get Kdelta
     * 
     * @return
     */
    public double getKDelta();

    /**
     * Set KDelta
     * 
     * @return
     */
    public void setKDelta(double kDelta);

    /**
     * Get EMax
     * 
     * @return
     */
    public double getKMax();

    /**
     * Set KMax
     * 
     * @return
     */
    public void setKMax(double kMax);

    /**
     * Get KMin
     * 
     * @return
     */
    public double getKMin();

    /**
     * Set KMin
     * 
     * @return
     */
    public void setKMin(double kMin);

    /**
     * Get M (connectN formerly)
     * 
     * @return
     */
    public double getM();

    /**
     * Get M (connectN formerly)
     * 
     * @return
     */
    public void setM(double m);

    /**
     * Get N (kPower formerly)
     * 
     * @return
     */
    public double getN();

    /**
     * Set N (kPower formerly)
     * 
     * @return
     */
    public void setN(double n);

    /**
     * Get postEdgeTime
     * 
     * @return
     */
    public double getTimePostEdge();

    /**
     * Set TimePostEdge
     * 
     * @return
     */
    public void setTimePostEdge(double postEdge);

    /**
     * Get TimeEdge
     * 
     * @return
     */
    public double getTimeEdge();

    /**
     * Set TimeEdge
     * 
     * @return
     */
    public void setTimeEdge(double edge);

    /**
     * Get TimePreEdge
     * 
     * @return
     */
    public double getTimePreEdge();

    /**
     * Set TimePreEdge
     * 
     * @return
     */
    public void setTimePreEdge(double preEdge);

    /**
     * Returns the dead time.
     * 
     * @return A <code>double</code>.
     */
    public double getDeadTime();

    /**
     * Sets the dead time.
     * 
     * @param deadTime The dead time to set.
     */
    public void setDeadTime(double deadTime);
}

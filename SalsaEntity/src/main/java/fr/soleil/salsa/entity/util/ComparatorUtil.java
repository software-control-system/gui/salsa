package fr.soleil.salsa.entity.util;

import java.util.List;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.entity.DisplayAxis;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IDisplay;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.scank.ITrajectoryK;

/**
 * 
 * @author SAINTIN This class provide static method in order to compare salsa
 *         specifics object in order to detect if the model change in comparison
 *         of database model
 */
public class ComparatorUtil {

    public static boolean stringEquals(String string1, String string2) {
        boolean equals = false;
        if (string1 == null && string2 == null) {
            equals = true;
        } else if (string1 != null && string2 != null) {
            return string1.equals(string2);
        }
        return equals;
    }

    public static boolean deviceEquals(IDevice obj1, IDevice obj2) {
        boolean equals = false;
        if (obj1 == null && obj2 == null) {
            equals = true;
        } else if (obj1 != null && obj2 != null) {
            if ((stringEquals(obj1.getName(), obj2.getName())) && (obj1.isEnabled() == obj2.isEnabled())
                    && (stringEquals(obj1.getScanServerAttributeName(), obj2.getScanServerAttributeName()))) {
                equals = true;
            }
        }
        return equals;
    }

    public static boolean scanAddOnsEquals(IScanAddOns scanAddOns1, IScanAddOns scanAddOns2) {
        boolean equals = false;
        if (scanAddOns1 == null && scanAddOns2 == null) {
            equals = true;
        } else if (scanAddOns1 != null && scanAddOns2 != null) {
            // TODO compare ErrorStrategy , Hook, getPostScanBehaviour
            equals = displayEquals(scanAddOns1.getDisplay(), scanAddOns2.getDisplay());
        }
        return equals;
    }

    public static boolean dimensionEquals(IDimension dimension1, IDimension dimension2) {
        boolean equals = false;
        if (dimension1 == null && dimension2 == null) {
            equals = true;
        } else if (dimension1 != null && dimension2 != null) {
            equals = dimension1.isEqualsTo(dimension2);
        }
        return equals;
    }

    public static boolean directoryEquals(IDirectory directory1, IDirectory directory2) {
        boolean equals = false;
        if (directory1 == null && directory2 == null) {
            equals = true;
        } else if (directory1 != null && directory2 != null) {
            if (!stringEquals(directory1.getFullPath(), directory2.getFullPath())
                    && (stringEquals(directory1.getName(), directory2.getName()))
                    && directoryListEquals(directory1.getSubDirectoriesList(), directory2.getSubDirectoriesList())
                    && configListEquals(directory1.getConfigList(), directory2.getConfigList())) {
                equals = true;
            }
        }
        return equals;
    }

    public static boolean displayEquals(IDisplay display1, IDisplay display2) {
        boolean equals = false;
        if (display1 == null && display2 == null) {
            equals = true;
        } else if (display1 != null && display2 != null) {
            if (axisListEquals(display1.getAxisList(), display2.getAxisList())) {
                equals = true;
            }
        }
        return equals;
    }

    public static boolean hookEquals(IHook obj1, IHook obj2) {
        boolean equals = false;
        if (obj1 == null && obj2 == null) {
            equals = true;
        } else if (obj1 != null && obj2 != null) {
            if ((obj1.getStage() == obj2.getStage())
                    && hookCommandListEquals(obj1.getCommandsList(), obj2.getCommandsList())) {
                equals = true;
            }
        }
        return equals;
    }

    public static boolean hookCommandEquals(IHookCommand obj1, IHookCommand obj2) {
        boolean equals = false;
        if (obj1 == null && obj2 == null) {
            equals = true;
        } else if (obj1 != null && obj2 != null) {
            equals = stringEquals(obj1.getCommand(), obj2.getCommand());
        }
        return equals;
    }

    public static boolean rangeEquals(IRange obj1, IRange obj2) {
        boolean equals = false;
        if (obj1 == null && obj2 == null) {
            equals = true;
        } else if (obj1 != null && obj2 != null) {
            if (ObjectUtils.sameObject(obj1.getStepsNumber(), obj2.getStepsNumber())
                    && (trajectoryListEquals(obj1.getTrajectoriesList(), obj2.getTrajectoriesList()))) {
                equals = true;
            }
        }
        return equals;
    }

    public static boolean trajectoryEquals(ITrajectory obj1, ITrajectory obj2) {
        boolean equals = false;
        if (obj1 == null && obj2 == null) {
            equals = true;
        } else if (obj1 != null && obj2 != null) {
            boolean ktrajectoryEquals = true;
            if ((obj1 instanceof ITrajectoryK) && (obj2 instanceof ITrajectoryK)) {
                ITrajectoryK trajK1 = (ITrajectoryK) obj1;
                ITrajectoryK trajK2 = (ITrajectoryK) obj2;
                if (!ObjectUtils.sameDouble(trajK1.getE0(), trajK2.getE0())
                        || !ObjectUtils.sameDouble(trajK1.getE1(), trajK2.getE1())
                        || !ObjectUtils.sameDouble(trajK1.getE2(), trajK2.getE2())
                        || (trajK1.getEDeltaEdge() != trajK2.getEDeltaEdge())
                        || !ObjectUtils.sameDouble(trajK1.getEDeltaPreEdge(), trajK2.getEDeltaPreEdge())
                        || !ObjectUtils.sameDouble(trajK1.getEMin(), trajK2.getEMin())
                        || !ObjectUtils.sameDouble(trajK1.getKDelta(), trajK2.getKDelta())
                        || !ObjectUtils.sameDouble(trajK1.getKMax(), trajK2.getKMax())
                        || !ObjectUtils.sameDouble(trajK1.getKMin(), trajK2.getKMin())
                        || !ObjectUtils.sameDouble(trajK1.getM(), trajK2.getM())
                        || !ObjectUtils.sameDouble(trajK1.getN(), trajK2.getN())
                        || !ObjectUtils.sameDouble(trajK1.getTimeEdge(), trajK2.getTimeEdge())
                        || !ObjectUtils.sameDouble(trajK1.getTimePostEdge(), trajK2.getTimePostEdge())
                        || !ObjectUtils.sameDouble(trajK1.getTimePreEdge(), trajK2.getTimePreEdge())
                        || !ObjectUtils.sameDouble(trajK1.getDeadTime(), trajK2.getDeadTime())) {
                    ktrajectoryEquals = false;
                }
            }
            if (ktrajectoryEquals && ObjectUtils.sameDouble(obj1.getBeginPosition(), obj2.getBeginPosition())
                    && ObjectUtils.sameDouble(obj1.getBeginPosition(), obj2.getBeginPosition())
                    && ObjectUtils.sameDouble(obj1.getEndPosition(), obj2.getEndPosition())
                    && ObjectUtils.sameObject(obj1.getRelative(), obj2.getRelative())
                    && ObjectUtils.sameDouble(obj1.getDelta(), obj2.getDelta())
                    && (obj1.getSpeed() == obj2.getSpeed())) {
                // && (obj1.getTrajectory() == obj2.getTrajectory())
                equals = true;
            }
        }

        return equals;
    }

    public static boolean deviceListEquals(List<? extends IDevice> list1, List<? extends IDevice> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IDevice device1 = list1.get(i);
                IDevice device2 = list2.get(i);
                if (!deviceEquals(device1, device2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean configPathEquals(IConfig<?> config1, IConfig<?> config2) {
        boolean equals = false;
        if (config1 == null && config2 == null) {
            equals = true;
        } else if ((config1 != null) && (config2 != null)) {
            equals = stringEquals(config1.getFullPath(), config2.getFullPath());
        }
        return equals;
    }

    public static boolean configListEquals(List<IConfig<?>> list1, List<IConfig<?>> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IConfig<?> config1 = list1.get(i);
                IConfig<?> config2 = list2.get(i);
                if (!configPathEquals(config1, config2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean dimensionListEquals(List<IDimension> list1, List<IDimension> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IDimension dimension1 = list1.get(i);
                IDimension dimension2 = list2.get(i);
                if (!dimensionEquals(dimension1, dimension2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean directoryListEquals(List<IDirectory> list1, List<IDirectory> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IDirectory directory1 = list1.get(i);
                IDirectory directory2 = list2.get(i);
                if (!directoryEquals(directory1, directory2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean axisListEquals(List<DisplayAxis> list1, List<DisplayAxis> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                DisplayAxis axis1 = list1.get(i);
                DisplayAxis axis2 = list2.get(i);
                if (axis1 != axis2) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean hookListEquals(List<IHook> list1, List<IHook> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IHook obj1 = list1.get(i);
                IHook obj2 = list2.get(i);
                if (!hookEquals(obj1, obj2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean trajectoryListEquals(List<? extends ITrajectory> list1, List<? extends ITrajectory> list2) {

        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                ITrajectory obj1 = list1.get(i);
                ITrajectory obj2 = list2.get(i);
                if (!trajectoryEquals(obj2, obj1)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean hookCommandListEquals(List<IHookCommand> list1, List<IHookCommand> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IHookCommand obj1 = list1.get(i);
                IHookCommand obj2 = list2.get(i);
                if (!hookCommandEquals(obj1, obj2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

    public static boolean objectEquals(Object obj1, Object obj2) {
        boolean equals = false;
        if (obj1 == null && obj2 == null) {
            equals = true;
        } else if (obj1 != null && obj2 != null) {
            equals = obj1.equals(obj2);
        }
        return equals;
    }

    public static boolean rangeListEquals(List<? extends IRange> list1, List<? extends IRange> list2) {
        boolean equals = false;
        if (list1 == null && list2 == null) {
            equals = true;
        } else if ((list1 != null && list2 != null) && (list1.size() == list2.size())) {
            boolean listEquals = true;
            for (int i = 0; i < list1.size(); i++) {
                IRange obj1 = list1.get(i);
                IRange obj2 = list2.get(i);
                if (!rangeEquals(obj1, obj2)) {
                    listEquals = false;
                    break;
                }
            }
            equals = listEquals;
        }
        return equals;
    }

}

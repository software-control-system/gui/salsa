package fr.soleil.salsa.entity.scank;

import java.util.List;

import fr.soleil.salsa.entity.IDimension;

public interface IDimensionK extends IDimension {

    /**
     * Get the range.
     * 
     * @return
     */
    public IRangeK getRangeX();

    /**
     * Get trajectory.
     * 
     * @return
     */
    public ITrajectoryK getTrajectory();

    /**
     * Set the range.
     * 
     * @param rangesX
     */
    public void setRangeX(IRangeK rangeX);

    /**
     * Set the trajectory.
     * 
     * @param trajectory
     */
    public void setTrajectory(ITrajectoryK trajectory);

    /**
     * Get the range.
     * 
     * @return
     */
    public List<IRangeK> getRangesXList();

    /**
     * Set the range.
     * 
     * @param rangesX
     */
    public void setRangesXList(List<IRangeK> rangesX);

}

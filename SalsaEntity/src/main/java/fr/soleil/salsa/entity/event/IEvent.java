package fr.soleil.salsa.entity.event;

import java.io.Serializable;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;

/**
 * Interface for an event. Events that can merge with another should implements the
 * {@link IMergeCapableEvent} interface, which extends this one.
 * 
 * @see IMergeCapableEvent
 * @see SalsaEventQueue
 * @author Administrateur
 * 
 */
public interface IEvent extends Serializable {

    public IEventSource<IEvent> getEventSource();
}

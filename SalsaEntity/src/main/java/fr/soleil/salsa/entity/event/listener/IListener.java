package fr.soleil.salsa.entity.event.listener;

import fr.soleil.salsa.entity.event.IEvent;

/**
 * The interface for the classes that wish to be notified of an event.
 * When the event is fired, the notifyEvent method will be called.
 * The IListener must have been recorded into the event source for this to work.
 * 
 * @author Administrateur
 * @see IEvent
 * @param <E> the event type
 */
public interface IListener<E> {
    
    /**
     * Method called when the event has been fired.
     * @param event
     */
    public void notifyEvent(E event);
}

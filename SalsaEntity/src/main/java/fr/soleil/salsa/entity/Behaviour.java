package fr.soleil.salsa.entity;

import fr.soleil.salsa.entity.impl.Messages;

/**
 * This class represents the actions which can be associated with a Scan
 * Behaviour. It contains a short description, a long description, the action
 * type represented as an <code>int</code>, and the number of arguments
 * needed by this action.
 * 
 * @author GIRARDOT
 */
public enum Behaviour {
    NOOP(Messages.getString("Behaviour.NOOP_SHORT_DESC"), 0, 0, Messages.getString("Behaviour.NOOP_LONG_DESC")), SCAN_START(
            Messages.getString("Behaviour.SCAN_START_SHORT_DESC"), 1, 0, Messages
            .getString("Behaviour.SCAN_START_LONG_DESC")), BEFORE_SCAN(Messages
                    .getString("Behaviour.BEFORE_SCAN_SHORT_DESC"), 2, 0, Messages
                    .getString("Behaviour.BEFORE_SCAN_LONGT_DESC")), SENSOR_MAX(Messages
                            .getString("Behaviour.SENSOR_MAX_SHORT_DESC"), 3, 1, Messages.getString("Behaviour.SENSOR_MAX_LONG_DESC")), SENSOR_MIN(
                                    Messages.getString("Behaviour.SENSOR_MIN_SHORT_DESC"), 4, 1, Messages
                                    .getString("Behaviour.SENSOR_MIN_LONG_DESC")), SENSOR_MAX_RELATIVE(Messages
                                            .getString("Behaviour.SENSOR_MAX_RELATIVE_SHORT_DESC"), 5, 2, Messages
                                            .getString("Behaviour.SENSOR_MAX_RELATIVE_LONG_DESC")), SENSOR_MIN_RELATIVE(Messages
                                                    .getString("Behaviour.SENSOR_MIN_RELATIVE_SHORT_DESC"), 6, 2, Messages
                                                    .getString("Behaviour.SENSOR_MIN_RELATIVE_LONG_DESC")), SENSOR_CENTER_RELATIVE(Messages
                                                            .getString("Behaviour.SENSOR_CENTER_RELATIVE_SHORT_DESC"), 7, 2, Messages
                                                            .getString("Behaviour.SENSOR_CENTER_RELATIVE_LONG_DESC"));

    private final String shortDescription;
    private final int type;
    private final int argumentCount;
    private final String longDescription;

    private Behaviour(final String shortDescription, final int type, final int argumentCount,
            final String longDescription) {
        this.shortDescription = shortDescription;
        this.type = type;
        this.argumentCount = argumentCount;
        this.longDescription = longDescription;
    }

    /**
     * @return the short description
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @return the number of arguments needed by this action
     */
    public int getArgumentCount() {
        return argumentCount;
    }

    /**
     * @return the long description
     */
    public String getLongDescription() {
        return longDescription;
    }

    public static Behaviour getBehaviourFromShortDescription(final String userDescription) {
        Behaviour result = null;
        Behaviour[] behaviours = Behaviour.values();

        if (userDescription != null) {
            for (Behaviour behaviour : behaviours) {
                // Compare description without spaces
                String description = behaviour.getShortDescription().replaceAll("\\s+", "");
                String userDescriptionModifier = userDescription.replaceAll("\\s+", "");
                if (userDescriptionModifier != null && description != null) {
                    if (userDescriptionModifier.equalsIgnoreCase(description)) {
                        result = behaviour;
                        break;
                    }
                }
            }
        }
        return result;
    }

}

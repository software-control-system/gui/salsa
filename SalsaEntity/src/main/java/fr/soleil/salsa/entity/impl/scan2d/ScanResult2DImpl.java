package fr.soleil.salsa.entity.impl.scan2d;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.impl.ScanResultImpl;
import fr.soleil.salsa.entity.scan2D.IScanResult2D;

/**
 * Implementation of {@link IScanResult} for the scanResult entity. The result of a scan.
 */
public class ScanResult2DImpl extends ScanResultImpl implements IScanResult2D {

    private static final long serialVersionUID = -9166103638929086376L;

    /**
     * The list of actuators used in the scan on the Y dimension.
     */
    private List<IActuator> actuatorsYList;

    /**
     * Constructor.
     */
    public ScanResult2DImpl() {
        super();
        actuatorsYList = new ArrayList<IActuator>();
    }

    /**
     * The list of actuators used in the scan on the Y dimension.
     */
    @Override
    public List<IActuator> getActuatorsYList() {
        return actuatorsYList;
    }

    /**
     * The list of actuators used in the scan on the Y dimension.
     */
    @Override
    public void setActuatorsYList(List<IActuator> actuatorsYList) {
        this.actuatorsYList = actuatorsYList;
    }
}

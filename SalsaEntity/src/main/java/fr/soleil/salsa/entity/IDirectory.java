package fr.soleil.salsa.entity;

import java.util.List;

/**
 * Interface for the directory entity. A directory contains any number of scan configurations, and
 * any number of subdirectories.
 * 
 * @author Administrateur
 */
public interface IDirectory extends IEntity {

    /**
     * Gets the subdirectories list.
     * 
     * @return subDirectoriesList
     */
    public List<IDirectory> getSubDirectoriesList();

    /**
     * Sets the subdirectories list.
     * 
     * @param List<IDirectory>
     */
    public void setSubDirectoriesList(List<IDirectory> subDirectoriesList);

    /**
     * Gets the configurations list.
     * 
     * @return configList
     */
    public List<IConfig<?>> getConfigList();

    /**
     * Sets the configurations list.
     * 
     * @param List<IConfig>
     */
    public void setConfigList(List<IConfig<?>> configList);

}

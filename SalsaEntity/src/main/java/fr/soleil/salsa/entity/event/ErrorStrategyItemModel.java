package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.impl.ErrorStrategyItemImpl;
import fr.soleil.salsa.entity.impl.ErrorStrategyType;

/**
 * @author Alike
 * 
 */
public class ErrorStrategyItemModel extends AEventHandlingModelDecorator<IErrorStrategyItem>
        implements IErrorStrategyItem {

    private static final long serialVersionUID = 2215948720612153897L;

    /**
     * 
     */
    public ErrorStrategyItemModel() {
        super(new ErrorStrategyItemImpl());
    }

    /**
     * @param baseBean
     */
    public ErrorStrategyItemModel(IErrorStrategyItem baseBean) {
        super(baseBean);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getRetryCount()
     */
    @Override
    public int getRetryCount() {
        return baseBean.getRetryCount();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getStrategy()
     */
    @Override
    public ErrorStrategyType getStrategy() {
        return baseBean.getStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getTimeBetweenRetries()
     */
    @Override
    public double getTimeBetweenRetries() {
        return baseBean.getTimeBetweenRetries();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#getTimeOut()
     */
    @Override
    public double getTimeOut() {
        return baseBean.getTimeOut();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#setRetryCount(int)
     */
    @Override
    public void setRetryCount(int retryCount) {
        int oldValue = baseBean.getRetryCount();
        baseBean.setRetryCount(retryCount);
        firePropertyChange("retryCount", oldValue, retryCount);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategyItem#setStrategy(fr.soleil.salsa
     * .entity.impl.ErrorStrategyType)
     */
    @Override
    public void setStrategy(ErrorStrategyType strategy) {
        ErrorStrategyType oldValue = baseBean.getStrategy();
        baseBean.setStrategy(strategy);
        firePropertyChange("strategy", oldValue, strategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategyItem#setTimeBetweenRetries(double)
     */
    @Override
    public void setTimeBetweenRetries(double timeBetweenRetries) {
        double oldValue = baseBean.getTimeBetweenRetries();
        baseBean.setTimeBetweenRetries(timeBetweenRetries);
        firePropertyChange("timeBetweenRetries", oldValue, timeBetweenRetries);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategyItem#setTimeOut(double)
     */
    @Override
    public void setTimeOut(double timeOut) {
        double oldValue = baseBean.getTimeOut();
        baseBean.setTimeOut(timeOut);
        firePropertyChange("timeOut", oldValue, timeOut);
    }

    @Override
    public IErrorStrategyItem toModel() {
        return this;
    }

}

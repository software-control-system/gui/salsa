package fr.soleil.salsa.entity.impl.scan2d;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.ActuatorModel;
import fr.soleil.salsa.entity.event.Config2DModel;
import fr.soleil.salsa.entity.impl.ConfigImpl;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * @author Alike.
 *
 */
public class Config2DImpl extends ConfigImpl<IDimension2DX> implements IConfig2D {

    private static final long serialVersionUID = -703856649833576607L;

    /**
     * The Y dimension.
     */
    private IDimension2DY dimensionY;

    /**
     * Constructor.
     */
    public Config2DImpl() {
        super();
        setType(ScanType.SCAN_2D);
    }

    /**
     * Get the Y dimension.
     *
     * @return
     */
    @Override
    public IDimension2DY getDimensionY() {
        return dimensionY;
    }

    /**
     * Set the Y dimension.
     *
     * @param dimensionY
     */
    @Override
    public void setDimensionY(final IDimension2DY dimensionY) {
        this.dimensionY = dimensionY;
    }

    // @Override
    // public List<IActuator> getActuatorList() {
    // final ArrayList<IActuator> allActuators = new ArrayList<IActuator>();
    // allActuators.addAll(dimensionY.getActuatorsList());
    // allActuators.addAll(super.getDimensionX().getActuatorsList());
    // return allActuators;
    // }

    @Override
    public List<IActuator> getActuatorList(final int dimension) {
        final ArrayList<IActuator> allActuators = new ArrayList<IActuator>();
        if (dimension == SECOND_DIMENSION) {
            allActuators.addAll(dimensionY.getActuatorsList());
        } else {
            allActuators.addAll(super.getDimensionX().getActuatorsList());
        }
        return allActuators;
    }

    @Override
    public void addActuator(final String actuatorName, final int dimension) {
        List<IActuator> actuatorList = getActuatorList(dimension);
        if (getDevice(actuatorName, actuatorList) == null) {
            IActuator device = new ActuatorModel();
            device.setName(actuatorName);
            // JIRA SCAN-375
            device.setEnabled(false);

            IDimension idimension = getDimensionX();
            if (dimension == SECOND_DIMENSION) {
                idimension = ((IConfig2D) this).getDimensionY();
            }

            if (idimension != null) {
                List<IActuator> tmpActuatorList = idimension.getActuatorsList();
                tmpActuatorList.add(device);

                List<? extends IRange> rangeList = idimension.getRangeList();
                if (rangeList != null) {
                    for (IRange range : rangeList) {
                        List<ITrajectory> trajectoryList = range.getTrajectoriesList();
                        ITrajectory trajectory = range.createTrajectory(device);
                        if ((trajectory != null) && (trajectoryList != null)) {
                            trajectoryList.add(trajectory);
                        }
                    }
                }
                setDimension(idimension);
            }
        }
    }

    @Override
    public void deleteActuator(final String actuatorName, final int dimension) {
        List<IDimension> dimensionList = getDimensionList();

        if (SalsaUtils.isFulfilled(dimensionList)) {
            List<IActuator> tmpActuatorList = null;
            IDevice actuator = null;

            IDimension idimension = getDimensionX();
            if (dimension == SECOND_DIMENSION) {
                idimension = ((IConfig2D) this).getDimensionY();
            }
            tmpActuatorList = idimension.getActuatorsList();
            actuator = getDevice(actuatorName, tmpActuatorList);
            if (actuator != null) {
                int index = tmpActuatorList.indexOf(actuator);
                // Remove associated trajectory
                List<? extends IRange> rangeList = idimension.getRangeList();
                if (rangeList != null) {
                    for (IRange range : rangeList) {
                        List<ITrajectory> trajectoryList = range.getTrajectoriesList();
                        if (trajectoryList != null) {
                            if ((index > -1) && (index < trajectoryList.size())) {
                                trajectoryList.remove(index);
                            }
                        }
                    }
                }
                tmpActuatorList.remove(actuator);
                setDimension(idimension);
            }
        }
    }

    @Override
    protected IConfig<IDimension2DX> initModel() {
        Config2DModel config2DModel = new Config2DModel(this);

        IDimension2DX dimension2DX = getDimensionX();
        if (dimension2DX != null && dimension2DX instanceof IObjectImpl<?>) {
            IDimension2DX dimensionModel = (IDimension2DX) ((IObjectImpl<?>) dimension2DX).toModel();
            List<IActuator> actuatorList = dimension2DX.getActuatorsList();
            List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
            dimensionModel.setActuatorsList(actuatorListModel);
            config2DModel.setDimensionX(dimensionModel);

        }

        if (dimensionY != null && dimensionY instanceof IObjectImpl<?>) {
            IDimension2DY dimensionModel = (IDimension2DY) ((IObjectImpl<?>) dimensionY).toModel();
            List<IActuator> actuatorList = dimensionY.getActuatorsList();
            List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
            dimensionModel.setActuatorsList(actuatorListModel);
            config2DModel.setDimensionY(dimensionModel);
        }

        return config2DModel;
    }

}

package fr.soleil.salsa.entity;


/**
 * Interface for the sensor entity. A sensor is a detector that measures the scanned values.
 * 
 * @author Administrateur
 */
public interface ISensor extends IDevice {

}

package fr.soleil.salsa.entity.scan1d;

import fr.soleil.salsa.entity.IConfig;

/**
 * Class relative to Config 1d.
 * 
 * @author Alike
 * 
 */
public interface IConfig1D extends IConfig<IDimension1D> {

}

package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.event.PostScanBehaviourModel;

public class PostScanBehaviourImpl implements IPostScanBehaviour {

    private static final long serialVersionUID = -9074918229274439850L;

    /**
     * The behaviour.
     */
    private Behaviour behaviour = Behaviour.NOOP;

    /**
     * The sensor index.
     */
    private int sensor = 0;

    /**
     * The actuator index.
     */
    private int actuator = 0;

    private IPostScanBehaviour model = null;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#getActuator()
     */
    public int getActuator() {
        return actuator;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#getBehaviour()
     */
    public Behaviour getBehaviour() {
        return behaviour;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#getSensor()
     */
    public int getSensor() {
        return sensor;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#setActuator(int)
     */
    public void setActuator(int actuator) {
        this.actuator = actuator;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#setBehaviour(fr.soleil.salsa.entity.impl.Behaviour)
     */
    public void setBehaviour(Behaviour behaviour) {
        this.behaviour = behaviour;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#setSensor(int)
     */
    public void setSensor(int sensor) {
        this.sensor = sensor;
    }

    @Override
    public IPostScanBehaviour toModel() {
        if (model == null) {
            model = new PostScanBehaviourModel(this);
        }
        return model;
    }

    @Override
    public IPostScanBehaviour toImpl() {
        return this;
    }

}

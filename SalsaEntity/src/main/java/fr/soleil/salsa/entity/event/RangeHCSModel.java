package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scanhcs.RangeHCSImpl;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;

public class RangeHCSModel extends AEventHandlingModelDecorator<IRangeHCS> implements IRangeHCS {

    private static final long serialVersionUID = -2864873358755699150L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param rangeHCS the decorated base bean.
     */
    public RangeHCSModel(IRangeHCS rangeHCS) {
        super(rangeHCS);
    }

    /**
     * Default constructor, that creates a new instance of RangeHCSImpl and wraps it.
     */
    public RangeHCSModel() {
        this(new RangeHCSImpl());
    }

    /**
     * 
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * 
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * 
     */
    @Override
    public Integer getStepsNumber() {
        return this.baseBean.getStepsNumber();
    }

    /**
     * 
     */
    @Override
    public void setStepsNumber(Integer stepsNumber) {
        Integer oldValue = this.baseBean.getStepsNumber();
        this.baseBean.setStepsNumber(stepsNumber);
        this.firePropertyChange("stepsNumber", oldValue, stepsNumber);
    }

    /**
     * 
     */
    @Override
    public List<ITrajectory> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    /**
     * 
     */
    @Override
    public IDimension getDimension() {
        return baseBean.getDimension();
    }

    /**
     * 
     */
    @Override
    public void setDimension(IDimension dimension) {
        IDimension oldValue = this.baseBean.getDimension();
        this.baseBean.setDimension(dimension);
        this.firePropertyChange("dimension", oldValue, dimension);
    }

    /**
     * 
     */
    @Override
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        List<ITrajectory> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectoriesList);
        this.firePropertyChange("trajectoriesList", oldValue, trajectoriesList);
    }

    @Override
    public ITrajectory createTrajectory(IActuator actuator) {
        return this.baseBean.createTrajectory(actuator);
    }

    @Override
    public void setStepNumberNoRefresh(Integer stepsNumber) {
        this.baseBean.setStepNumberNoRefresh(stepsNumber);
    }

    @Override
    public double[] getIntegrationTime() {
        return this.baseBean.getIntegrationTime();
    }

    @Override
    public void setIntegrationTime(double[] integrationTime) {
        this.baseBean.setIntegrationTime(integrationTime);
    }

}

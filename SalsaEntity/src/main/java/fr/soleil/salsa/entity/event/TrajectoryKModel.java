package fr.soleil.salsa.entity.event;

import fr.soleil.lib.project.math.MathConst;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scank.TrajectoryKImpl;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;

public class TrajectoryKModel extends AEventHandlingModelDecorator<ITrajectoryK> implements ITrajectoryK {

    private static final long serialVersionUID = -4679331232362703699L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     *
     * @param trajectoryK the decorated base bean.
     */
    public TrajectoryKModel(final ITrajectoryK trajectoryK) {
        super(trajectoryK);
    }

    /**
     * Default constructor, that creates a new instance of TrajectoryKImpl and wraps it.
     */
    public TrajectoryKModel() {
        this(new TrajectoryKImpl());
    }

    /**
     *
     */
    @Override
    public IRangeK getRange() {
        return baseBean.getRange();
    }

    /**
     *
     */
    @Override
    public void setRange(final IRangeK range) {
        IRangeK oldValue = baseBean.getRange();
        baseBean.setRange(range);
        firePropertyChange("range", oldValue, range);
    }

    /**
     *
     */
    @Override
    public Integer getId() {
        return baseBean.getId();
    }

    /**
     *
     */
    @Override
    public void setId(final Integer id) {
        Integer oldValue = baseBean.getId();
        baseBean.setId(id);
        firePropertyChange("id", oldValue, id);
    }

    /**
     * Delegates the equals method to the base bean. The base bean must check equality using only
     * their base interface, not their actual implementation class.
     *
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(final Object obj) {
        return baseBean.equals(obj);
    }

    /**
     * Delegates the hashCode method to the base bean.
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return baseBean.hashCode();
    }

    @Override
    public double getE0() {
        return baseBean.getE0();
    }

    @Override
    public double getE1() {
        return baseBean.getE1();
    }

    @Override
    public double getE2() {
        return baseBean.getE2();
    }

    @Override
    public double getEDeltaEdge() {
        return baseBean.getEDeltaEdge();
    }

    @Override
    public double getEDeltaPreEdge() {
        return baseBean.getEDeltaPreEdge();
    }

    @Override
    public double getEMin() {
        return baseBean.getEMin();
    }

    @Override
    public double getKDelta() {
        return baseBean.getKDelta();
    }

    @Override
    public double getKMax() {
        return baseBean.getKMax();
    }

    @Override
    public double getKMin() {
        return baseBean.getKMin();
    }

    @Override
    public double getM() {
        return baseBean.getM();
    }

    @Override
    public double getN() {
        return baseBean.getN();
    }

    @Override
    public double getTimePostEdge() {
        return baseBean.getTimePostEdge();
    }

    @Override
    public double getTimeEdge() {
        return baseBean.getTimeEdge();
    }

    @Override
    public double getTimePreEdge() {
        return baseBean.getTimePreEdge();
    }

    @Override
    public void setE0(final double e0) {
        double oldValue = baseBean.getE0();
        baseBean.setE0(e0);
        firePropertyChange("e0", oldValue, e0);
    }

    @Override
    public void setE1(final double e1) {
        double oldValue = baseBean.getE1();
        baseBean.setE1(e1);
        firePropertyChange("e1", oldValue, e1);
    }

    @Override
    public void setE2(final double e2) {
        double oldValue = baseBean.getE2();
        baseBean.setE2(e2);
        firePropertyChange("e2", oldValue, e2);
    }

    @Override
    public void setEDeltaEdge(final double eDeltaEdge) {
        double oldValue = baseBean.getEDeltaEdge();
        baseBean.setEDeltaEdge(eDeltaEdge);
        firePropertyChange("eDeltaEdge", oldValue, eDeltaEdge);
    }

    @Override
    public void setEDeltaPreEdge(final double eDeltaPreEdge) {
        double oldValue = baseBean.getEDeltaPreEdge();
        baseBean.setEDeltaPreEdge(eDeltaPreEdge);
        firePropertyChange("eDeltaPreEdge", oldValue, eDeltaPreEdge);
    }

    @Override
    public void setEMin(final double eMin) {
        double oldValue = baseBean.getEMin();
        baseBean.setEMin(eMin);
        firePropertyChange("eMin", oldValue, eMin);
    }

    @Override
    public void setKDelta(final double kDelta) {
        double oldValue = baseBean.getKDelta();
        baseBean.setKDelta(kDelta);
        firePropertyChange("kDelta", oldValue, kDelta);
    }

    @Override
    public void setKMax(final double kMax) {
        double oldValue = baseBean.getKMax();
        baseBean.setKMax(kMax);
        firePropertyChange("kMax", oldValue, kMax);
    }

    @Override
    public void setKMin(final double kMin) {
        double oldValue = baseBean.getKMin();
        baseBean.setKMin(kMin);
        firePropertyChange("kMin", oldValue, kMin);
    }

    @Override
    public void setM(final double m) {
        double oldValue = baseBean.getM();
        baseBean.setM(m);
        firePropertyChange("m", oldValue, m);
    }

    @Override
    public void setN(final double n) {
        double oldValue = baseBean.getN();
        baseBean.setN(n);
        firePropertyChange("n", oldValue, n);
    }

    @Override
    public void setTimeEdge(final double timeEdge) {
        double oldValue = baseBean.getTimeEdge();
        baseBean.setTimeEdge(timeEdge);
        firePropertyChange("timeEdge", oldValue, timeEdge);
    }

    @Override
    public void setTimePreEdge(final double preEdge) {
        double oldValue = baseBean.getTimePreEdge();
        baseBean.setTimePreEdge(preEdge);
        firePropertyChange("timePreEdge", oldValue, preEdge);
    }

    @Override
    public void setTimePostEdge(final double postEdge) {
        double oldValue = baseBean.getTimePostEdge();
        baseBean.setTimePostEdge(postEdge);
        firePropertyChange("timePostEdge", oldValue, postEdge);
    }

    @Override
    public double getDeadTime() {
        return baseBean.getDeadTime();
    }

    @Override
    public void setDeadTime(double deadTime) {
        double oldValue = baseBean.getDeadTime();
        baseBean.setDeadTime(deadTime);
        firePropertyChange("deadTime", oldValue, deadTime);
    }

    @Override
    public double getBeginPosition() {
        return MathConst.NAN_FOR_NULL;
    }

    @Override
    public double getDelta() {
        return MathConst.NAN_FOR_NULL;
    }

    @Override
    public double getEndPosition() {
        return MathConst.NAN_FOR_NULL;
    }

    @Override
    public Boolean getRelative() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getSpeed() {
        return baseBean.getSpeed();
    }

    @Override
    public void setBeginPosition(final double beginPosition) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDelta(final double delta) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setEndPosition(final double endPosition) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setRelative(final Boolean relative) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setSpeed(final double speed) {
        double oldValue = baseBean.getSpeed();
        baseBean.setSpeed(speed);
        firePropertyChange("speed", oldValue, speed);
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setName(final String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setTrajectory(final double[] values) {
        if (baseBean != null) {
            baseBean.setTrajectory(values);
        }
    }

    @Override
    public double[] getTrajectory() {
        if (baseBean != null) {
            return baseBean.getTrajectory();
        }
        return null;
    }

    @Override
    public void setIRange(final IRange range) {
        baseBean.setIRange(range);
    }

    @Override
    public IRange getIRange() {
        return baseBean.getIRange();
    }

    @Override
    public ITrajectory toModel() {
        return this;
    }

    @Override
    public void refreshDelta() {
        baseBean.refreshDelta();
    }

    @Override
    public void refreshStep() {
        baseBean.refreshStep();
    }

    @Override
    public void refreshEndPosition() {
        baseBean.refreshEndPosition();
    }

    @Override
    public void setDeltaConstant(final Boolean deltaConstant) {
        baseBean.setDeltaConstant(deltaConstant);

    }

    @Override
    public Boolean isDeltaConstant() {
        return baseBean.isDeltaConstant();
    }

    @Override
    public void setCustomTrajectory(final boolean custom) {
        baseBean.setCustomTrajectory(custom);
    }

    @Override
    public boolean isCustomTrajectory() {
        return baseBean.isCustomTrajectory();
    }

    // Default implementation
    @Override
    public IActuator getActuator() {
        return null;
    }

}

package fr.soleil.salsa.entity.impl.scan2d;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.event.Range2DXModel;
import fr.soleil.salsa.entity.scan2D.IRange2DX;

/**
 * @author Administrateur
 * 
 */
public class Range2DXImpl extends Range2DImpl implements IRange2DX {

    private static final long serialVersionUID = 4917870690158714296L;

    @Override
    protected IRange initModel() {
        return new Range2DXModel(this);
    }

}

package fr.soleil.salsa.entity.impl.scanhcs;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.ConfigHCSModel;
import fr.soleil.salsa.entity.impl.ConfigImpl;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.tool.SalsaUtils;

public class ConfigHCSImpl extends ConfigImpl<IDimensionHCS> implements IConfigHCS {

    private static final long serialVersionUID = 7133899354425622854L;

    /**
     * Constructor.
     */
    public ConfigHCSImpl() {
        super();
        setEnableScanSpeed(true);
        setType(ScanType.SCAN_HCS);
    }

    /**
     * Get the unique integration time.
     * 
     * @return
     */
    public double[] getIntegrationTime() {
        double[] integrationTime = null;
        IRangeHCS rangeHCS = getIRangeHCS();
        if (rangeHCS != null) {
            integrationTime = rangeHCS.getIntegrationTime();
        }
        return integrationTime;
    }

    /**
     * Set the unique integration time.
     * 
     * @param dimensionX
     */
    public void setIntegrationTime(double[] integrationTime) {
        IRangeHCS rangeHCS = getIRangeHCS();
        if (rangeHCS != null) {
            rangeHCS.setIntegrationTime(integrationTime);
        }
    }

    /**
     * Get the number of points.
     * 
     * @return
     */
    public Integer getNumberOfPoints() {
        Integer stepNumber = 1;
        IRangeHCS rangeHCS = getIRangeHCS();
        if (rangeHCS != null) {
            stepNumber = rangeHCS.getStepsNumber();
        }
        return stepNumber;
    }

    /**
     * Set the number of points.
     * 
     * @param dimensionX
     */
    public void setNumberOfPoints(Integer numberOfPoints) {
        IRangeHCS rangeHCS = getIRangeHCS();
        if (rangeHCS != null) {
            rangeHCS.setStepsNumber(numberOfPoints);
        }
    }

    private IRangeHCS getIRangeHCS() {
        IRangeHCS rangeHCS = null;
        IDimensionHCS dimension = getDimensionX();
        if (dimension != null) {
            List<IRangeHCS> rangeList = dimension.getRangesXList();
            if (SalsaUtils.isFulfilled(rangeList)) {
                rangeHCS = rangeList.get(0);
            }
        }
        return rangeHCS;
    }

    @Override
    protected IConfig<IDimensionHCS> initModel() {
        ConfigHCSModel configHCSModel = new ConfigHCSModel(this);
        IDimensionHCS dimension = getDimensionX();
        if (dimension != null && dimension instanceof IObjectImpl<?>) {
            IDimensionHCS dimensionModel = (IDimensionHCS) ((IObjectImpl<?>) dimension).toModel();
            List<IActuator> actuatorList = dimension.getActuatorsList();
            List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
            dimensionModel.setActuatorsList(actuatorListModel);
            configHCSModel.setDimensionX(dimensionModel);
        }
        return configHCSModel;
    }

}

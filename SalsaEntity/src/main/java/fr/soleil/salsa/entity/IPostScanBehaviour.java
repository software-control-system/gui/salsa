package fr.soleil.salsa.entity;

import java.io.Serializable;

public interface IPostScanBehaviour extends Serializable {

    /**
     * Gets the actuator index.
     * 
     * @return
     */
    int getActuator();

    /**
     * Gets the behaviour.
     * 
     * @return
     */
    Behaviour getBehaviour();

    /**
     * Gets the sensor index.
     * 
     * @return
     */
    int getSensor();

    /**
     * Sets the actuator index.
     * 
     * @param actuator
     */
    void setActuator(int actuator);

    /**
     * Sets the behaviour.
     * 
     * @param behaviour
     */
    void setBehaviour(Behaviour behaviour);

    /**
     * Sets the sensor index.
     * 
     * @param sensor
     */
    void setSensor(int sensor);

    public IPostScanBehaviour toModel();

    public IPostScanBehaviour toImpl();

}

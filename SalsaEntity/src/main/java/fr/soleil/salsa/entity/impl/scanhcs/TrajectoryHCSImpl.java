package fr.soleil.salsa.entity.impl.scanhcs;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.TrajectoryHCSModel;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;

public class TrajectoryHCSImpl extends TrajectoryImpl implements ITrajectoryHCS {

    private static final long serialVersionUID = -8477917111096549171L;

    /**
     * The actuator.
     */
    private IActuator actuator = null;

    /**
     * The delta constant.
     */
    private Boolean deltaConstant = false;

    /**
     * The range.
     */
    private IRangeHCS range = null;

    /**
     * Gets the actuator.
     */
    public IActuator getActuator() {
        return actuator;
    }

    /**
     * Gets the HCS range.
     */
    public IRangeHCS getRange() {
        return range;
    }

    /**
     * Gets the delta constant.
     */
    public Boolean isDeltaConstant() {
        return deltaConstant;
    }

    /**
     * Sets the actuator.
     */
    public void setActuator(IActuator actuator) {
        this.actuator = actuator;
    }

    /**
     * 
     */
    public void setDeltaConstant(Boolean deltaConstant) {
        this.deltaConstant = deltaConstant;
    }

    /**
     * 
     */
    public void setRange(IRangeHCS range) {
        this.range = range;
    }

    protected ITrajectory initModel() {
        return new TrajectoryHCSModel(this);
    }

}

package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.impl.scan2d.Dimension2DXImpl;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DX;

/**
 * @author Alike
 * 
 */
public class Dimension2DXModel extends DimensionModel<IDimension2DX> implements IDimension2DX {

    private static final long serialVersionUID = 3591409845747307814L;

    public Dimension2DXModel(IDimension2DX dimension2DX) {
        super(dimension2DX);
    }

    public Dimension2DXModel() {
        this(new Dimension2DXImpl());
    }

    /**
     * Get ranges.
     * 
     * @return
     */
    @Override
    public List<IRange2DX> getRangesList() {
        return baseBean.getRangesList();
    }

    /**
     * Set the ranges.
     * 
     * @param ranges
     */
    @Override
    public void setRangesList(List<IRange2DX> rangesList) {
        List<IRange2DX> oldValue = baseBean.getRangesList();
        baseBean.setRangesList(rangesList);
        this.firePropertyChange("rangesList", oldValue, rangesList);
    }

}

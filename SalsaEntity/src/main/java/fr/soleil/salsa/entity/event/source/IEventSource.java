package fr.soleil.salsa.entity.event.source;

import java.util.Collection;

import fr.soleil.salsa.entity.event.listener.IListener;

/**
 * The interface to be implemented by an event source so listeners can register.
 * @author Administrateur
 * @param <E> the event type.
 */
public interface IEventSource<E> {

    /**
     * Adds a listener to be notified if an  event is triggered.
     * @param listener
     */
    public void addListener(IListener<E> listener);
    
    /**
     * Removes a listener.
     * @param listener
     */
    public void removeListener(IListener<E> listener);
    
    /**
     * Returns the listeners collection.
     * @return
     */
    public Collection<IListener<E>> getListeners();
}

package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IActuatorTrajectory;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.RangeModel;
import fr.soleil.salsa.entity.event.Trajectory1DModel;
import fr.soleil.salsa.entity.event.Trajectory2DXModel;
import fr.soleil.salsa.entity.event.Trajectory2DYModel;
import fr.soleil.salsa.entity.event.TrajectoryEnergyModel;
import fr.soleil.salsa.entity.event.TrajectoryHCSModel;
import fr.soleil.salsa.entity.event.TrajectoryKModel;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scank.IRangeK;

/**
 * Implementation of {@link IRange} for the range entity. A range is a part of a
 * dimension where the movements of the actuators are linear. The trajectory
 * list contains the details of the movement of each actuator on this range.
 * 
 * @see fr.soleil.salsa.entity.ITrajectory
 * @see fr.soleil.salsa.entity.IDimension
 * @author Administrateur
 * 
 */
public class RangeImpl implements IRange, IObjectImpl<IRange> {

    private static final long serialVersionUID = 5634129592263712591L;

    private IRange model = null;

    /**
     * The unique identifier.
     */
    private Integer id = 0;

    /**
     * The number of steps between the starting and ending position.
     */
    private Integer stepsNumber = 0;

    /**
     * The trajectories of the actuators on this range.
     */
    protected List<ITrajectory> trajectoriesList;

    /**
     * The dimension this range is a part of.
     */
    private IDimension dimension = null;;

    /**
     * Constructor.
     */
    public RangeImpl() {
        super();
        trajectoriesList = new ArrayList<ITrajectory>();
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return id;
    }

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the number of steps between the starting and ending position.
     * 
     * @return stepsNumber
     */
    @Override
    public Integer getStepsNumber() {
        return stepsNumber;
    }

    /**
     * Sets the number of steps between the starting and ending position.
     * 
     * @param Integer
     */
    @Override
    public void setStepsNumber(Integer stepsNumber) {
        this.stepsNumber = stepsNumber;
        List<ITrajectory> trajectoryList = getTrajectoriesList();
        for (ITrajectory trajectory : trajectoryList) {
            if (!trajectory.isDeltaConstant()) {
                trajectory.refreshDelta();
            } else {
                trajectory.refreshEndPosition();
            }
        }
    }

    @Override
    public void setStepNumberNoRefresh(Integer stepsNumber) {
        this.stepsNumber = stepsNumber;
    }

    /**
     * Gets the trajectories of the actuators on this range.
     * 
     * @return trajectoriesList
     */
    @Override
    public List<ITrajectory> getTrajectoriesList() {
        if (trajectoriesList == null) {
            trajectoriesList = new ArrayList<ITrajectory>();
        }
        return trajectoriesList;
    }

    /**
     * Sets the trajectories of the actuators on this range.
     * 
     * @param List
     *            <ITrajectory>
     */
    @Override
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        this.trajectoriesList = trajectoriesList;
        updateRangeToTrajectory();
    }

    /**
     * Gets the dimension this range is a part of.
     * 
     * @return dimension
     */
    @Override
    public IDimension getDimension() {
        return dimension;
    }

    /**
     * Sets the dimension this range is a part of.
     * 
     * @param IDimension
     */
    @Override
    public void setDimension(IDimension dimension) {
        this.dimension = dimension;
    }

    /**
     * Checks for equality. This equals implementation only checks if the obj
     * parameter is an instance of the base interface : another implementation
     * of the interface is considered equal to this one if it represents the
     * same entity.
     * 
     * @see Object#equals(Object)
     * @param obj
     *            the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        } else if (obj == null) {
            equals = false;
        } else if (!(obj instanceof IRange)) {
            equals = false;
        } else if (this.id == null) {
            // Approximation. The equals method of a bean that has not received
            // an identifier yet
            // should not be called.
            equals = false;
        } else {
            equals = this.id.equals(((IRange) obj).getId());
        }
        return equals;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }

    @Override
    public IRange toModel() {
        if (model == null) {
            model = initModel();

            if (model != null) {
                List<ITrajectory> trajectoryList = getTrajectoriesList();
                if (trajectoryList != null) {
                    List<ITrajectory> trajectoryModelList = new ArrayList<ITrajectory>();
                    for (ITrajectory trajectory : trajectoryList) {
                        ITrajectory trajectoryModel = trajectory.toModel();
                        if (trajectoryModel instanceof IActuatorTrajectory) {
                            IActuator actuator = ((IActuatorTrajectory) trajectory).getActuator();
                            ((IActuatorTrajectory) trajectoryModel).setActuator((IActuator) actuator.toModel());
                        }
                        trajectory.setIRange(this);
                        trajectoryModelList.add(trajectoryModel);
                    }
                    model.setTrajectoriesList(trajectoryModelList);
                }
            }
        }
        return model;
    }

    private void updateRangeToTrajectory() {
        if (trajectoriesList != null) {
            for (ITrajectory trajectory : trajectoriesList) {
                trajectory.setIRange(this);
            }
        }
    }

    @Override
    public ITrajectory createTrajectory(IActuator actuator) {
        ITrajectory trajectory = null;
        if (this instanceof IRange1D) {
            trajectory = new Trajectory1DModel();
        } else if (this instanceof IRange2DX) {
            trajectory = new Trajectory2DXModel();
        } else if (this instanceof IRange2DY) {
            trajectory = new Trajectory2DYModel();
        } else if (this instanceof IRangeHCS) {
            trajectory = new TrajectoryHCSModel();
        } else if (this instanceof IRangeEnergy) {
            trajectory = new TrajectoryEnergyModel();
        } else if (this instanceof IRangeK) {
            trajectory = new TrajectoryKModel();
        }
        initValueTrajectory(trajectory);
        if (trajectory != null) {
            trajectory.setIRange(this);
            if (trajectory instanceof IActuatorTrajectory) {
                ((IActuatorTrajectory) trajectory).setActuator(actuator);
            }
        }
        return trajectory;
    }

    private void initValueTrajectory(ITrajectory trajectory) {
        if (trajectory != null) {
            trajectory.setBeginPosition(0.0);
            trajectory.setEndPosition(0.0);
            trajectory.setDelta(0.0);
            trajectory.setRelative(false);
            trajectory.setSpeed(0.0);
        }
    }

    protected IRange initModel() {
        return new RangeModel(this);
    }

    @Override
    public IRange toImpl() {
        return this;
    }

}

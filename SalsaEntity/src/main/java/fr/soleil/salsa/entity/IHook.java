package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

import fr.soleil.salsa.entity.impl.Stage;

/**
 * @author
 * 
 */
public interface IHook extends Serializable {

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    public Integer getId();

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    public void setId(Integer id);

    /**
     * Sets the stage.
     * 
     * @param stage
     */
    void setStage(Stage stage);

    /**
     * Gets the stage.
     * 
     * @return
     */
    Stage getStage();

    /**
     * Gets the command list.
     * 
     * @return
     */
    List<IHookCommand> getCommandsList();

    /**
     * Sets the command list.
     * 
     * @param commandsList
     */
    void setCommandsList(List<IHookCommand> commandsList);

    public IHook toModel();

    public IHook toImpl();

}

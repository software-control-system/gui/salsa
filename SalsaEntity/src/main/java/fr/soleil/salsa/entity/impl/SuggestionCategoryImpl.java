package fr.soleil.salsa.entity.impl;

import java.util.List;

import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestionItem;

public class SuggestionCategoryImpl implements ISuggestionCategory {

    private static final long serialVersionUID = -1858383819022277551L;

    /**
     * The lable.
     */
    private String label;

    /**
     * Suggestions
     */
    private List<ISuggestionItem> suggestionList;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionCategory#getLabel()
     */
    public String getLabel() {
        return label;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionCategory#setLabel(java.lang.String)
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionCategory#getSuggestionList()
     */
    public List<ISuggestionItem> getSuggestionList() {
        return suggestionList;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionCategory#setSuggestionList(java.util.List)
     */
    public void setSuggestionList(List<ISuggestionItem> suggestionList) {
        this.suggestionList = suggestionList;
    }

}

package fr.soleil.salsa.entity.event;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.wrapping.IBeanWrapper;

/**
 * Model entity are supposed to send an event when one of their property is changed. If the property
 * is a map, we want the event to be fired if the map is modified, even if it is not entirely
 * replaced. Therefore, we need maps that send events when methods like add are called. This is such
 * an implementation of java.util.Map. It works by wrapping a map.
 * 
 * @author Administrateur
 * 
 * @param <E>
 * @param <T>
 */
public class MapModel<K, V, T extends IEventSource<?>> implements Map<K, V> {

    /**
     * Implements Map.Entry.
     * 
     * @param <K>
     * @param <V>
     */
    public class EntryProperty<K2, V2, T2 extends IEventSource<?>> implements Map.Entry<K2, V2>,
            IBeanWrapper<Map.Entry<K2, V2>> {

        /**
         * The base entry.
         */
        protected Entry<K2, V2> baseEntry;

        /**
         * The actual entity that this map is a property of.
         */
        protected T2 entity;

        /**
         * The property name.
         */
        protected String propertyName;

        /**
         * The constructor.
         * 
         * @param baseEntry
         * @param entity
         * @param propertyName
         */
        public EntryProperty(Entry<K2, V2> baseEntry, T2 entity, String propertyName) {
            super();
            this.baseEntry = baseEntry;
            this.entity = entity;
            this.propertyName = propertyName;
        }

        /**
         * Returns the base bean.
         * 
         * @return
         */
        public Entry<K2, V2> getBaseBean() {
            return baseEntry;
        }

        /**
         * @param o
         * @return
         * @see java.util.Map.Entry#equals(java.lang.Object)
         */
        public boolean equals(Object o) {
            return baseEntry.equals(o);
        }

        /**
         * @return
         * @see java.util.Map.Entry#getKey()
         */
        public K2 getKey() {
            return baseEntry.getKey();
        }

        /**
         * @return
         * @see java.util.Map.Entry#getValue()
         */
        public V2 getValue() {
            return baseEntry.getValue();
        }

        /**
         * @return
         * @see java.util.Map.Entry#hashCode()
         */
        public int hashCode() {
            return baseEntry.hashCode();
        }

        /**
         * @param value
         * @return
         * @see java.util.Map.Entry#setValue(java.lang.Object)
         */
        public V2 setValue(V2 value) {
            return baseEntry.setValue(value);
        }
    }

    /**
     * The decorated map.
     */
    protected Map<K, V> baseMap;

    /**
     * The actual entity that this map is a property of.
     */
    protected T entity;

    /**
     * The property name.
     */
    protected String propertyName;

    /**
     * The constructor.
     * 
     * @param baseMap the decorated Map.
     * @param entity the actual entity that this map is a property of.
     * @param propertyName the property name.
     * @param wrapperFactory the element wrapper factory.
     */
    public MapModel(Map<K, V> baseMap, T entity, String propertyName) {
        this.baseMap = baseMap;
        this.entity = entity;
        this.propertyName = propertyName;
    }

    /**
     * Returns the base map.
     */
    public Map<K, V> getBaseBean() {
        return baseMap;
    }

    /**
     * Sends the EntityPropertyChangedEvent. Reminder : the EntityPropertyChangedEvent must be
     * called AFTER the property has been changed in the bean.
     * 
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    @SuppressWarnings("unchecked")
    protected void firePropertyChange() {
        EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                this.entity, this.propertyName, null, this);
        SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent,
                ((IEventSource<EntityPropertyChangedEvent<T>>) entity).getListeners());
    }

    /**
     * 
     * @see java.util.Map#clear()
     */
    public void clear() {
        baseMap.clear();
        this.firePropertyChange();
    }

    /**
     * @param key
     * @return
     * @see java.util.Map#containsKey(java.lang.Object)
     */
    public boolean containsKey(Object key) {
        return baseMap.containsKey(key);
    }

    /**
     * @param value
     * @return
     * @see java.util.Map#containsValue(java.lang.Object)
     */
    public boolean containsValue(Object value) {
        return baseMap.containsValue(value);
    }

    /**
     * @return
     * @see java.util.Map#entrySet()
     */
    public Set<java.util.Map.Entry<K, V>> entrySet() {
        return new SetModel<Entry<K, V>, T>(baseMap.entrySet(), this.entity, this.propertyName);
    }

    /**
     * @param o
     * @return
     * @see java.util.Map#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        return baseMap.equals(o);
    }

    /**
     * @param key
     * @return
     * @see java.util.Map#get(java.lang.Object)
     */
    public V get(Object key) {
        return baseMap.get(key);
    }

    /**
     * @return
     * @see java.util.Map#hashCode()
     */
    public int hashCode() {
        return baseMap.hashCode();
    }

    /**
     * @return
     * @see java.util.Map#isEmpty()
     */
    public boolean isEmpty() {
        return baseMap.isEmpty();
    }

    /**
     * @return
     * @see java.util.Map#keySet()
     */
    public Set<K> keySet() {
        // Since the keys set can change the map, we wrap it into a SetProperty to the same
        // property.
        return new SetModel<K, T>(baseMap.keySet(), this.entity, this.propertyName);
    }

    /**
     * @param key
     * @param value
     * @return
     * @see java.util.Map#put(java.lang.Object, java.lang.Object)
     */
    public V put(K key, V value) {
        V result = baseMap.put(key, value);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param t
     * @see java.util.Map#putAll(java.util.Map)
     */
    public void putAll(Map<? extends K, ? extends V> t) {
        baseMap.putAll(t);
        this.firePropertyChange();
    }

    /**
     * @param key
     * @return
     * @see java.util.Map#remove(java.lang.Object)
     */
    public V remove(Object key) {
        V result = baseMap.remove(key);
        this.firePropertyChange();
        return result;
    }

    /**
     * @return
     * @see java.util.Map#size()
     */
    public int size() {
        return baseMap.size();
    }

    /**
     * @return
     * @see java.util.Map#values()
     */
    public Collection<V> values() {
        return new CollectionModel<V, T>(baseMap.values(), this.entity, this.propertyName);
    }
}
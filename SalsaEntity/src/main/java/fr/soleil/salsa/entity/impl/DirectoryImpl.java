package fr.soleil.salsa.entity.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Implementation of {@link IDirectory} for the directory entity. A directory contains any number of
 * scan configurations, and any number of subdirectories.
 * 
 * @author Administrateur
 */
public class DirectoryImpl implements IDirectory {

    private static final long serialVersionUID = 7177539068610353130L;

    /**
     * The unique identifier.
     */
    private Integer id;

    /**
     * The directory name.
     */
    private String name;

    /**
     * The directory containing this directory. This is null for the root directory.
     */
    private IDirectory directory;

    /**
     * The position in the diretory, indicating the basic sort order.
     */
    private Integer positionInDirectory;

    /**
     * The subdirectories list.
     */
    private List<IDirectory> subDirectoriesList;

    /**
     * The configurations list.
     */
    private List<IConfig<?>> configList;

    /**
     * The timestamp.
     */
    private Timestamp timestamp;

    /**
     * Constructor.
     */
    public DirectoryImpl() {
        super();
        directory = null;
        subDirectoriesList = new ArrayList<IDirectory>();
        configList = new ArrayList<IConfig<?>>();
    }

    /**
     * Gets the configurations list.
     * 
     * @return configList
     */
    @Override
    public List<IConfig<?>> getConfigList() {
        return configList;
    }

    /**
     * Gets the directory containing this directory. This is null for the root directory.
     * 
     * @return directory
     */
    @Override
    public IDirectory getDirectory() {
        return directory;
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return id;
    }

    /**
     * Gets the directory name.
     * 
     * @return name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets the position in the diretory, indicating the basic sort order.
     * 
     * @return positionInDirectory
     */
    @Override
    public Integer getPositionInDirectory() {
        return positionInDirectory;
    }

    /**
     * Gets the subdirectories list.
     * 
     * @return subDirectoriesList
     */
    @Override
    public List<IDirectory> getSubDirectoriesList() {
        return subDirectoriesList;
    }

    /**
     * Get the timestamp.
     * 
     * @return
     */
    @Override
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the configurations list.
     * 
     * @param List<IConfig>
     */
    @Override
    public void setConfigList(List<IConfig<?>> configList) {
        this.configList = configList;
    }

    /**
     * Sets the directory containing this directory. This is null for the root directory.
     * 
     * @param IDirectory
     */
    @Override
    public void setDirectory(IDirectory directory) {
        this.directory = directory;
    }

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Sets the directory name.
     * 
     * @param String
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the position in the diretory, indicating the basic sort order.
     * 
     * @param Integer
     */
    @Override
    public void setPositionInDirectory(Integer positionInDirectory) {
        this.positionInDirectory = positionInDirectory;
    }

    /**
     * Sets the subdirectories list.
     * 
     * @param List<IDirectory>
     */
    @Override
    public void setSubDirectoriesList(List<IDirectory> subDirectoriesList) {
        this.subDirectoriesList = subDirectoriesList;
    }

    /**
     * Set the timestamp.
     * 
     * @param timestamp
     */
    @Override
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @see Object#equals(Object)
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (obj instanceof IDirectory) {
            IDirectory that = (IDirectory) obj;
            if (this.getId() != null) {
                if (that.getId() != null) {
                    return this.getId().equals(that.getId());
                } else {
                    return false;
                }
            } else {
                if (that.getId() != null) {
                    return false;
                } else {
                    return getPath(this).equals(getPath(that));
                }
            }
        } else {
            return false;
        }
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (getId() != null) {
            return getId().hashCode();
        } else {
            return getPath(this).hashCode();
        }
    }

    /**
     * Returns the complete path to a directory..
     * 
     * @param config
     * @return
     */
    private static String getPath(IDirectory directory) {
        if (directory == null) {
            return "";
        }
        if (directory.getName() == null) {
            return "/";
        }
        return getPath(directory.getDirectory()) + "/" + directory.getName();
    }

    @Override
    public String getFullPath() {
        ArrayList<IDirectory> paths = new ArrayList<IDirectory>();
        IDirectory directory = this;
        while (directory != null) {
            if (paths.isEmpty()) {
                paths.add(directory);
            } else {
                paths.add(0, directory);
            }
            directory = directory.getDirectory();
        }
        StringBuilder builder = new StringBuilder();
        for (IDirectory dir : paths) {
            builder.append(dir.getName()).append("/");
        }
        paths.clear();
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    @Override
    public String toString() {
        String toStringValue = super.toString();
        String fullPath = getFullPath();
        if (SalsaUtils.isDefined(fullPath)) {
            toStringValue = fullPath;
        }
        return toStringValue;
    }
}

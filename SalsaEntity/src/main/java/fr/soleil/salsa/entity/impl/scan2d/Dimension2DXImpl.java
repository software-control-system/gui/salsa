package fr.soleil.salsa.entity.impl.scan2d;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.event.Dimension2DXModel;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.util.ComparatorUtil;

/**
 * @author Alike
 * 
 */
public class Dimension2DXImpl extends DimensionImpl implements IDimension2DX {

    private static final long serialVersionUID = 1747185653320113594L;

    /**
     * Ranges.
     */
    private List<IRange2DX> rangesList;

    /**
     * Default constructor.
     */
    public Dimension2DXImpl() {
        rangesList = new ArrayList<IRange2DX>();
    }

    /**
     * Get ranges.
     * 
     * @return
     */
    public List<IRange2DX> getRangesList() {
        return rangesList;
    }

    /**
     * Set the ranges.
     * 
     * @param ranges
     */
    public void setRangesList(List<IRange2DX> rangesList) {
        this.rangesList = rangesList;
    }

    protected IDimension initModel() {
        return new Dimension2DXModel(this);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null && super.isEqualsTo(dimension)) {
            if (dimension instanceof IDimension2DX) {
                equals = ComparatorUtil.rangeListEquals(
                        ((IDimension2DX) dimension).getRangesList(), getRangesList());
            }
        }
        return equals;
    }

}

package fr.soleil.salsa.entity.wrapping;

/**
 * An interface to be implemented by decorators.
 * @author Administrateur
 *
 * @param <T> the decorated class.
 */
public interface IBeanWrapper<T> {
    
    /**
     * Returns the base bean. 
     * @return
     */
    public T getBaseBean();
}

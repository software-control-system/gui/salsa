package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.impl.scan2d.Dimension2DYImpl;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DY;

/**
 * @author Alike
 * 
 */
public class Dimension2DYModel extends DimensionModel<IDimension2DY> implements IDimension2DY {

    private static final long serialVersionUID = 7567951762399942026L;

    public Dimension2DYModel(IDimension2DY dimension2DY) {
        super(dimension2DY);
    }

    public Dimension2DYModel() {
        this(new Dimension2DYImpl());
    }

    /**
     * Get ranges.
     * 
     * @return
     */
    @Override
    public List<IRange2DY> getRangesList() {
        return baseBean.getRangesList();
    }

    /**
     * Set the ranges.
     * 
     * @param ranges
     */
    @Override
    public void setRangesList(List<IRange2DY> rangesList) {
        List<IRange2DY> oldValue = baseBean.getRangesList();
        baseBean.setRangesList(rangesList);
        this.firePropertyChange("rangesList", oldValue, rangesList);
    }

}

package fr.soleil.salsa.entity;

public interface IObjectModel<T> {

    public T toImpl();

}

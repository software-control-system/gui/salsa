package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.impl.scank.ConfigKImpl;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;

public class ConfigKModel extends AConfigModel<IDimensionK, IConfigK> implements IConfigK {

    private static final long serialVersionUID = 230464549932771377L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param configK the decorated base bean.
     */
    public ConfigKModel(IConfigK configK) {
        super(configK);
    }

    /**
     * Default constructor, that creates a new instance of {@link ConfigKImpl} and wraps it.
     */
    public ConfigKModel() {
        this(new ConfigKImpl());
    }

}

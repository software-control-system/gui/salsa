package fr.soleil.salsa.entity.event.status;

import java.util.EventObject;

@Deprecated
public class ScanStatusEvent extends EventObject {

    private static final long serialVersionUID = -6350709010368582510L;

    public ScanStatusEvent(IStatusSource source) {
        super(source);
    }

    @Override
    public IStatusSource getSource() {
        return (IStatusSource) super.getSource();
    }

}

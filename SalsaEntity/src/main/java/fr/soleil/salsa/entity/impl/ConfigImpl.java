package fr.soleil.salsa.entity.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IDirectory;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.ActuatorModel;
import fr.soleil.salsa.entity.event.SensorModel;
import fr.soleil.salsa.entity.event.TimebaseModel;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.ITrajectoryHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.TrajectoryUtil;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Implementation of {@link IConfig} for the config entity. This is the
 * configuration data for a scan.
 *
 * @author Administrateur
 */
public class ConfigImpl<T extends IDimension> implements IConfig<T>, IObjectImpl<IConfig<T>> {

    private static final long serialVersionUID = -6219900741675946119L;

    private IConfig<T> model = null;

    /**
     * The default value for the scan number property.
     */
    public static final int DEFAULT_SCAN_NUMBER = 1;

    /**
     * The scan type;
     */
    private ScanType type = ScanType.SCAN_1D;

    /**
     * The actuator delay.
     */
    private double actuatorsDelay = 0;

    /**
     * The timebases delay.
     */
    private double timebasesDelay = 0;

    /**
     * The directory containing this configuration.
     */
    private IDirectory directory = null;

    /**
     * True if the scan speed option is enabled.
     */
    private boolean enableScanSpeed = false;

    /**
     * The unique identifier.
     */
    private Integer id = 0;

    /**
     * The config name.
     */
    private String name = null;

    /**
     * True if the "on the fly" option is enabled.
     */
    private boolean onTheFly = false;

    /**
     * The position in the diretory, indicating the basic sort order.
     */
    private Integer positionInDirectory = 0;

    /**
     * The scan number.
     */
    private int scanNumber = DEFAULT_SCAN_NUMBER;

    /**
     * The sensors list.
     */
    private List<ISensor> sensorsList = new ArrayList<>();

    /**
     * The time base list.
     */
    private List<ITimebase> timebaseList = new ArrayList<>();

    /**
     * The timestamp.
     */
    private Timestamp timestamp = null;

    /**
     * True if zig zag option is enabled.
     */
    private boolean zigzag = false;

    private String runName = null;

    /**
     * True if the object data are fully available. (except for parent
     * directory).
     */
    private boolean loaded = false;

    /**
     * True if the configuration has been modified since its last change.
     */
    private boolean modified;

    private boolean forcedSaving = false;

    private String dataRecorderConfig = null;

    /**
     * The scan add on
     */
    private IScanAddOns scanAddOn = new ScanAddOnImp();

    protected T dimensionX = null;

    private long lastModificationDate = 0;

    protected List<IDimension> dimensionList = new ArrayList<>();

    /**
     * Constructor.
     */
    public ConfigImpl() {
        super();
        modified = false;
        lastModificationDate = 0;

    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getActuatorsDelay()
     */
    @Override
    public double getActuatorsDelay() {
        return actuatorsDelay;
    }

    @Override
    public double getTimebasesDelay() {
        return timebasesDelay;
    }

    @Override
    public void setTimebasesDelay(final double timebasesDelay) {
        this.timebasesDelay = timebasesDelay;

    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getDirectory()
     */
    @Override
    public IDirectory getDirectory() {
        return directory;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getId()
     */
    @Override
    public Integer getId() {
        return id;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getPositionInDirectory()
     */
    @Override
    public Integer getPositionInDirectory() {
        return positionInDirectory;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getScanNumber()
     */
    @Override
    public int getScanNumber() {
        return scanNumber;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getSensorsList()
     */
    @Override
    public List<ISensor> getSensorsList() {
        return sensorsList;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getTimestamp()
     */
    @Override
    public Timestamp getTimestamp() {
        return timestamp;
    }

    @Override
    public long getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     *
     * @return
     */
    @Override
    public IScanAddOns getScanAddOn() {
        return scanAddOn;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getType()
     */
    @Override
    public ScanType getType() {
        return type;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#isEnableScanSpeed()
     */
    @Override
    public boolean isEnableScanSpeed() {
        return enableScanSpeed;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#isLoaded()
     */
    @Override
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#isOnTheFly()
     */
    @Override
    public boolean isOnTheFly() {
        return onTheFly;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#isZigzag()
     */
    @Override
    public boolean isZigzag() {
        return zigzag;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setActuatorsDelay(double)
     */
    @Override
    public void setActuatorsDelay(final double actuatorsDelay) {
        this.actuatorsDelay = actuatorsDelay;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setDirectory(fr.soleil.salsa.entity.IDirectory)
     */
    @Override
    public void setDirectory(final IDirectory directory) {
        this.directory = directory;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setEnableScanSpeed(boolean)
     */
    @Override
    public void setEnableScanSpeed(final boolean enableScanSpeed) {
        this.enableScanSpeed = enableScanSpeed;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setId(java.lang.Integer)
     */
    @Override
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setLoaded(boolean)
     */
    @Override
    public void setLoaded(final boolean loaded) {
        this.loaded = loaded;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setName(java.lang.String)
     */
    @Override
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setOnTheFly(boolean)
     */
    @Override
    public void setOnTheFly(final boolean onTheFly) {
        this.onTheFly = onTheFly;
        setEnableScanSpeed(onTheFly);
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setPositionInDirectory(java.lang.Integer)
     */
    @Override
    public void setPositionInDirectory(final Integer positionInDirectory) {
        this.positionInDirectory = positionInDirectory;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setScanNumber(int)
     */
    @Override
    public void setScanNumber(final int scanNumber) {
        this.scanNumber = scanNumber;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setSensorsList(java.util.List)
     */
    @Override
    public void setSensorsList(final List<ISensor> sensorsList) {
        this.sensorsList = sensorsList;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setTimestamp(java.sql.Timestamp)
     */
    @Override
    public void setTimestamp(final Timestamp timestamp) {
        this.timestamp = timestamp;
        if (timestamp != null) {
            lastModificationDate = timestamp.getTime();
        }
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setType(fr.soleil.salsa.entity.IConfig.ScanType)
     */
    @Override
    public void setType(final ScanType type) {
        this.type = type;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setZigzag(boolean)
     */
    @Override
    public void setZigzag(final boolean zigzag) {
        this.zigzag = zigzag;
    }

    /**
     * True if the configuration has been modified since its last change.
     *
     * @return
     */
    @Override
    public boolean isModified() {
        return modified;
    }

    /**
     * True if the configuration has been modified since its last change.
     *
     * @param modified
     */
    @Override
    public void setModified(final boolean modified) {
        this.modified = modified;
        if (modified) {
            lastModificationDate = System.currentTimeMillis();
        }
    }

    /**
     * @see Object#equals(Object)
     * @return
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (obj instanceof IConfig<?>) {
            final IConfig<?> that = (IConfig<?>) obj;
            if (this.getId() != null) {
                if (that.getId() != null) {
                    return this.getId().equals(that.getId());
                } else {
                    return false;
                }
            } else {
                if (that.getId() != null) {
                    return false;
                } else {
                    return getPath(this).equals(getPath(that));
                }
            }
        } else {
            return false;
        }
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (getId() != null) {
            return getId().hashCode();
        } else {
            return getPath(this).hashCode();
        }
    }

    /**
     * Returns the complete path to a config.
     *
     * @param config
     * @return
     */
    private static String getPath(final IConfig<?> config) {
        return getPath(config.getDirectory()) + "/" + config.getName();
    }

    /**
     * Returns the complete path to a directory..
     *
     * @param config
     * @return
     */
    private static String getPath(final IDirectory directory) {
        if (directory == null) {
            return "";
        }
        if (directory.getName() == null) {
            return "/";
        }
        return getPath(directory.getDirectory()) + "/" + directory.getName();
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#getTimebaseList()
     */
    @Override
    public List<ITimebase> getTimebaseList() {
        return this.timebaseList;
    }

    /**
     * @see fr.soleil.salsa.entity.IConfig#setSensorsList(java.util.List)
     */
    @Override
    public void setTimebaseList(final List<ITimebase> timebaseList) {
        this.timebaseList = timebaseList;

    }

    /**
     * Sets the ScanAddOn
     *
     * @param scanAddOn
     */
    @Override
    public void setScanAddOn(final IScanAddOns scanAddOn) {
        this.scanAddOn = scanAddOn;
    }

    @Override
    public T getDimensionX() {
        return dimensionX;
    }

    @Override
    public void setDimensionX(final T dimension) {
        this.dimensionX = dimension;
    }

    @Override
    public List<IDimension> getDimensionList() {
        if (dimensionList.isEmpty()) {
            dimensionList.add(getDimensionX());
            if (this instanceof IConfig2D) {
                dimensionList.add(((IConfig2D) this).getDimensionY());
            }
        }
        return dimensionList;
    }

    @Override
    public String getFullPath() {
        final StringBuilder builder = new StringBuilder();
        if (directory != null) {
            builder.append(directory.getFullPath()).append("/");
        }
        builder.append(name);
        return builder.toString();
    }

    @Override
    public boolean isValid() {
        // TODO According to each congig Type, control the validity of the
        // configuration
        // Trajectory, nomber of point
        // step number
        // actuator
        // sensor
        return false;
    }

    @Override
    public boolean isForcedSaving() {
        return forcedSaving;
    }

    @Override
    public void setForcedSaving(final boolean forcedSaving) {
        this.forcedSaving = forcedSaving;

    }

    @Override
    public IDevice getDevice(final String deviceName, final List<? extends IDevice> deviceList) {
        IDevice device = null;
        if ((deviceName != null) && (deviceList != null)) {
            for (IDevice tmpDevice : deviceList) {
                if (tmpDevice.getName().equalsIgnoreCase(deviceName)) {
                    device = tmpDevice;
                    break;
                }
            }
        }
        return device;
    }

    /**
     * Active device(an actuator or a sensor or a timeBase)contain in
     * userDeviceList and disable others. If a device is not contain in this
     * configuration a salsaException is thrown
     *
     * @param userDeviceList
     *            list of device to active
     * @param salsaDevice
     *            list of device contain in this configuration
     * @param name
     *            the name of the device list (actuator, sensor or timebase)
     *            it's use for exception message
     * @throws SalsaException
     *             throws when a userDevice is not contain in this configuration
     */
    protected void setDeviceEnable(final String userDevice, final List<? extends IDevice> deviceList,
            final boolean enable) {
        IDevice device = getDevice(userDevice, deviceList);
        if (device != null) {
            device.setEnabled(enable);
        }
    }

    protected boolean isDeviceEnable(final String userDevice, final List<? extends IDevice> deviceList) {
        boolean enabled = false;
        IDevice device = getDevice(userDevice, deviceList);
        if (device != null) {
            enabled = device.isEnabled();
        }
        return enabled;
    }

    @Override
    public void setActuatorEnable(final String actuatorName, final boolean enabled, final int dimension) {
        setDeviceEnable(actuatorName, getActuatorList(dimension), enabled);
    }

    /**
     * Active device(an actuator or a sensor or a timeBase)contain in
     * userDeviceList and disable others. If a device is not contain in this
     * configuration a salsaException is thrown
     *
     * @param userDeviceList
     *            list of device to active
     * @param salsaDevice
     *            list of device contain in this configuration
     * @param name
     *            the name of the device list (actuator, sensor or timebase)
     *            it's use for exception message
     * @throws SalsaException
     *             throws when a userDevice is not contain in this configuration
     */
    protected void activateDevices(final String[] userDeviceList, final List<? extends IDevice> deviceList,
            final String name) throws SalsaException {

        // check deviceList is correct: for each element in deviceList we check
        // it's in
        // salsaConfiguration
        // Deativate all
        for (final IDevice device : deviceList) {
            device.setEnabled(false);
        }

        if (userDeviceList != null) {
            IDevice device = null;
            for (final String deviceName : userDeviceList) {
                device = getDevice(deviceName, deviceList);
                if (device == null) {
                    throw new SalsaException(name + ": " + deviceName + " not found the current salsa configuration.");
                } else {
                    device.setEnabled(true);
                }
            }
        }
    }

    @Override
    public void activateSensors(final String[] sensorList) throws SalsaException {
        activateDevices(sensorList, this.sensorsList, "sensor");
    }

    @Override
    public void activateActuators(final String[] actuatorList, final int dimension) throws SalsaException {
        activateDevices(actuatorList, getActuatorList(dimension), "actuator");
    }

    @Override
    public void activateTimebase(final String[] timebaseList) throws SalsaException {
        activateDevices(timebaseList, this.timebaseList, "timebase");
    }

    @Override
    public void setSensorEnable(final String sensorName, final boolean enabled) {
        setDeviceEnable(sensorName, this.sensorsList, enabled);
    }

    @Override
    public boolean isSensorEnable(final String sensorName) {
        return isDeviceEnable(sensorName, this.sensorsList);
    }

    @Override
    public void swapSensor(final String sensorName1, final String sensorName2) {
        List<ISensor> tmpSensorList = getSensorsList();
        swapDevice(sensorName1, sensorName2, tmpSensorList);
    }

    private void swapDevice(final String deviceName1, final String deviceName2,
            final List<? extends IDevice> deviceList) {
        IDevice device1 = getDevice(deviceName1, deviceList);
        IDevice device2 = getDevice(deviceName2, deviceList);
        if ((device1 != null) && (device2 != null)) {
            boolean enable1 = device1.isEnabled();
            boolean isCommon1 = device1.isCommon();
            boolean enable2 = device2.isEnabled();
            boolean isCommon2 = device2.isCommon();
            device1.setName(deviceName2);
            device2.setName(deviceName1);
            device1.setEnabled(enable2);
            device2.setEnabled(enable1);
            device1.setCommon(isCommon2);
            device2.setCommon(isCommon1);
        }
    }

    @Override
    public List<IActuator> getActuatorList(final int dimension) {
        return this.dimensionX.getActuatorsList();
    }

    @Override
    public boolean isActuatorEnable(final String actuatorName, final int dimension) {
        return isDeviceEnable(actuatorName, getActuatorList(dimension));
    }

    @Override
    public void setTimeBaseEnable(final String timeBaseName, final boolean enabled) {
        setDeviceEnable(timeBaseName, this.timebaseList, enabled);
    }

    @Override
    public boolean isTimeBaseEnable(final String timeBaseName) {
        return isDeviceEnable(timeBaseName, this.timebaseList);
    }

    @Override
    public void swapTimeBase(final String timeBaseName1, final String timeBaseName2) {
        List<ITimebase> tmpTimebaseList = getTimebaseList();
        swapDevice(timeBaseName1, timeBaseName2, tmpTimebaseList);
    }

    @Override
    public IConfig<T> toModel() {
        if (model == null) {
            model = initModel();
            if (model != null) {
                List<ISensor> sensorList = getSensorsList();
                List<ISensor> sensorListModel = convertSensorListToModel(sensorList);
                model.setSensorsList(sensorListModel);

                List<ITimebase> timeBaseList = getTimebaseList();
                List<ITimebase> timeBaseListModel = convertTimeBaseListToModel(timeBaseList);
                model.setTimebaseList(timeBaseListModel);

                IScanAddOns scanAddOnIm = getScanAddOn();
                if (scanAddOnIm != null) {
                    model.setScanAddOn(scanAddOnIm.toModel());
                }
            }
        }
        return model;
    }

    protected IConfig<T> initModel() {
        return null;
    }

    @Override
    public IConfig<T> toImpl() {
        return this;
    }

    protected List<IActuator> convertActuatorListToModel(final List<IActuator> actuatorList) {
        List<IActuator> actuatorListModel = null;
        if (actuatorList != null) {
            actuatorListModel = new ArrayList<>();
            for (IActuator actuator : actuatorList) {
                actuatorListModel.add((IActuator) actuator.toModel());
            }
        }
        return actuatorListModel;
    }

    protected List<ISensor> convertSensorListToModel(final List<ISensor> sensorList) {
        List<ISensor> sensorListModel = null;
        if (sensorList != null) {
            sensorListModel = new ArrayList<>();
            for (ISensor sensor : sensorList) {
                sensorListModel.add((ISensor) sensor.toModel());
            }
        }
        return sensorListModel;
    }

    protected List<ITimebase> convertTimeBaseListToModel(final List<ITimebase> timeBaseList) {
        List<ITimebase> timebaseListModel = null;
        if (timeBaseList != null) {
            timebaseListModel = new ArrayList<>();
            for (ITimebase timebase : timeBaseList) {
                timebaseListModel.add((ITimebase) timebase.toModel());
            }
        }
        return timebaseListModel;
    }

    @Override
    public void addActuator(final String actuatorName, final int dimension) {
        List<IActuator> actuatorList = getActuatorList(dimension);
        if (getDevice(actuatorName, actuatorList) == null) {
            IActuator device = new ActuatorModel();
            device.setName(actuatorName);
            // JIRA SCAN-375
            device.setEnabled(false);

            IDimension idimension = getDimensionX();

            if (idimension != null) {
                List<IActuator> tmpActuatorList = idimension.getActuatorsList();
                tmpActuatorList.add(device);

                List<? extends IRange> rangeList = idimension.getRangeList();
                if (rangeList != null) {
                    for (IRange range : rangeList) {
                        List<ITrajectory> trajectoryList = range.getTrajectoriesList();
                        ITrajectory trajectory = range.createTrajectory(device);
                        if ((trajectory != null) && (trajectoryList != null)) {
                            trajectoryList.add(trajectory);
                        }
                    }
                }
                setDimension(idimension);
            }
        }
    }

    @Override
    public void deleteActuator(final String actuatorName, final int dimension) {
        List<IDimension> dimensionList = getDimensionList();

        if (SalsaUtils.isFulfilled(dimensionList)) {
            List<IActuator> tmpActuatorList = null;
            IDevice actuator = null;
            for (IDimension idimension : dimensionList) {
                tmpActuatorList = idimension.getActuatorsList();
                actuator = getDevice(actuatorName, tmpActuatorList);
                if (actuator != null) {
                    int index = tmpActuatorList.indexOf(actuator);
                    // Remove associated trajectory
                    List<? extends IRange> rangeList = idimension.getRangeList();
                    if (rangeList != null) {
                        for (IRange range : rangeList) {
                            List<ITrajectory> trajectoryList = range.getTrajectoriesList();
                            if (trajectoryList != null) {
                                if ((index > -1) && (index < trajectoryList.size())) {
                                    trajectoryList.remove(index);
                                }
                            }
                        }
                    }
                    tmpActuatorList.remove(actuator);
                    setDimension(idimension);
                    break;
                }
            }
        }
    }

    private int getIndexForName(final String deviceName, final List<? extends IDevice> deviceList) {
        int index = -1;
        if ((deviceList != null) && (deviceName != null)) {
            Object[] objectArray = deviceList.toArray();
            Object object = null;
            for (int i = 0; i < objectArray.length; i++) {
                object = objectArray[i];
                if (object instanceof IDevice) {
                    if (deviceName.equalsIgnoreCase(((IDevice) object).getName())) {
                        index = i;
                        break;
                    }
                }
            }
        }
        return index;
    }

    @Override
    public void swapActuator(final String actuatorName1, final String actuatorName2, final int dimension) {
        IDimension idimension = getDimensionX();
        if (dimension == 2 && (this instanceof IConfig2D)) {
            idimension = ((IConfig2D) this).getDimensionY();
        }

        if (idimension != null) {
            List<IActuator> tmpActuatorList = idimension.getActuatorsList();
            int actuator1pos = getIndexForName(actuatorName1, tmpActuatorList);
            int actuator2pos = getIndexForName(actuatorName2, tmpActuatorList);
            if ((actuator1pos > -1) && (actuator2pos > -1)) {
                // First rename actuator
                swapDevice(actuatorName1, actuatorName2, tmpActuatorList);
                List<? extends IRange> rangeList = idimension.getRangeList();
                if (rangeList != null) {
                    List<ITrajectory> trajectoryList = null;
                    ITrajectory t1 = null;
                    ITrajectory t2 = null;
                    for (IRange range : rangeList) {
                        trajectoryList = range.getTrajectoriesList();
                        if ((trajectoryList != null) && (actuator1pos < trajectoryList.size())
                                && (actuator2pos < trajectoryList.size())) {
                            t1 = trajectoryList.get(actuator1pos);
                            t2 = trajectoryList.get(actuator2pos);
                            TrajectoryUtil.swapTrajectory(t1, t2);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void addSensor(final String sensorName) {
        if (getDevice(sensorName, sensorsList) == null) {
            ISensor device = new SensorModel();
            device.setName(sensorName);
            device.setEnabled(true);
            sensorsList.add(device);
        }
    }

    @Override
    public void deleteSensor(final String sensorName) {
        IDevice sensor = getDevice(sensorName, sensorsList);
        if (sensor != null) {
            sensorsList.remove(sensor);
        }
    }

    @Override
    public void addTimeBase(final String timebaseName) {
        if (getDevice(timebaseName, timebaseList) == null) {
            ITimebase timeBase = new TimebaseModel();
            timeBase.setName(timebaseName);
            timeBase.setEnabled(true);
            timebaseList.add(timeBase);
        }
    }

    @Override
    public void deleteTimeBase(final String timebaseName) {
        IDevice timebase = getDevice(timebaseName, timebaseList);
        if (timebase != null) {
            timebaseList.remove(timebase);
        }
    }

    @Override
    public void renameActuator(final String oldActuatorName, final String newActuatorName, final int dimension) {
        List<IActuator> tmpActuatorList = getActuatorList(dimension);
        IDevice actuator = getDevice(oldActuatorName, tmpActuatorList);
        if (actuator != null) {
            actuator.setName(newActuatorName);
        }
    }

    @Override
    public void renameSensor(final String oldSensorName, final String newSensorName) {
        IDevice sensor = getDevice(oldSensorName, sensorsList);
        if (sensor != null) {
            sensor.setName(newSensorName);
        }
    }

    @Override
    public void renameTimeBase(final String oldTimeBaseName, final String newTimeBaseName) {
        IDevice timebase = getDevice(oldTimeBaseName, timebaseList);
        if (timebase != null) {
            timebase.setName(newTimeBaseName);
        }
    }

    protected void setDimension(final IDimension dimension) {
        if ((this instanceof IConfig1D) && (dimension instanceof IDimension1D)) {
            ((IConfig1D) this).setDimensionX((IDimension1D) dimension);
        } else if ((this instanceof IConfig2D) && (dimension instanceof IDimension2DX)) {
            ((IConfig2D) this).setDimensionX((IDimension2DX) dimension);
        } else if ((this instanceof IConfig2D) && (dimension instanceof IDimension2DY)) {
            ((IConfig2D) this).setDimensionY((IDimension2DY) dimension);
        } else if ((this instanceof IConfigHCS) && (dimension instanceof IDimensionHCS)) {
            ((IConfigHCS) this).setDimensionX((IDimensionHCS) dimension);
        } else if ((this instanceof IConfigEnergy) && (dimension instanceof IDimensionEnergy)) {
            ((IConfigEnergy) this).setDimensionX((IDimensionEnergy) dimension);
        } else if ((this instanceof IConfigK) && (dimension instanceof IDimensionK)) {
            ((IConfigK) this).setDimensionX((IDimensionK) dimension);
        }
    }

    private List<IDevice> getActivatedDeviceList(final List<? extends IDevice> allList) {
        List<IDevice> newActiveList = new ArrayList<>();
        for (IDevice device : allList) {
            if (device.isEnabled()) {
                newActiveList.add(device);
            }
        }
        return newActiveList;
    }

    @Override
    public List<IDevice> getActivatedActuatorsList() {
        List<IActuator> allActuator = getActuatorList(FIRST_DIMENSION);
        return getActivatedDeviceList(allActuator);
    }

    @Override
    public List<IDevice> getActivatedSensorsList() {
        List<ISensor> allSensors = getSensorsList();
        return getActivatedDeviceList(allSensors);
    }

    @Override
    public List<IDevice> getActivatedTimebasesList() {
        List<ITimebase> allTimebases = getTimebaseList();
        return getActivatedDeviceList(allTimebases);
    }

    @Override
    public String getDataRecorderConfig() {
        return dataRecorderConfig;
    }

    @Override
    public void setDataRecorderConfig(final String config) {
        dataRecorderConfig = config;
    }

    @Override
    public String getRunName() {
        return runName;
    }

    @Override
    public void setRunName(final String runName) {
        this.runName = runName;
    }

    @Override
    public String[] getScanInfo() {

        List<String> scanInfoList = new ArrayList<>();
        List<IDimension> dimensionList = getDimensionList();
        // Get the dimension List
        if (SalsaUtils.isFulfilled(dimensionList)) {
            List<? extends IRange> rangeList = null;
            List<IActuator> actuatorList = null;
            List<ITrajectory> trajectoryList = null;
            int dimensionIndex = 1;
            int rangeIndex = 1;
            String rangeInfo = null;
            String actuatorInfo = null;
            String trajectoryInfo = "trajectory/";
            ITrajectory trajectory = null;
            IActuator actuator = null;

            for (IDimension dimension : dimensionList) {
                // Get the actuator List
                actuatorList = dimension.getActuatorsList();
                if (SalsaUtils.isFulfilled(actuatorList)) {
                    rangeList = dimension.getRangeList();
                    if (SalsaUtils.isFulfilled(rangeList)) {
                        for (int i = 0; i < actuatorList.size(); i++) {
                            rangeIndex = 1;
                            for (IRange range : rangeList) {
                                rangeInfo = "";
                                trajectoryList = range.getTrajectoriesList();
                                if (SalsaUtils.isFulfilled(trajectoryList)
                                        && (actuatorList.size() <= trajectoryList.size())) {
                                    actuator = actuatorList.get(i);
                                    if (actuator.isEnabled()) {
                                        actuatorInfo = trajectoryInfo + "actuator_" + dimensionIndex + "_" + (i + 1)
                                                + "/";

                                        // Display actuator name only for the
                                        // first range
                                        if (rangeIndex == 1) {
                                            scanInfoList.add(actuatorInfo + "name:" + actuator.getName());
                                        }
                                        if (rangeList.size() > 1) {
                                            rangeInfo = "range_" + rangeIndex + "/";
                                        }

                                        trajectory = trajectoryList.get(i);
                                        if (dimension instanceof IDimensionK) {
                                            ITrajectoryK ktraj = ((IDimensionK) dimension).getTrajectory();
                                            if (rangeIndex == 1) {
                                                scanInfoList.add(actuatorInfo + "type:K");
                                            }
                                            scanInfoList.add(actuatorInfo + "emin:" + ktraj.getEMin());
                                            scanInfoList
                                                    .add(actuatorInfo + "pre_edge_delta:" + ktraj.getEDeltaPreEdge());
                                            scanInfoList.add(actuatorInfo + "e0:" + ktraj.getE0());
                                            scanInfoList.add(actuatorInfo + "e1:" + ktraj.getE1());
                                            scanInfoList.add(actuatorInfo + "e2:" + ktraj.getE2());
                                            scanInfoList.add(actuatorInfo + "edge_delta:" + ktraj.getEDeltaEdge());
                                            if (ktraj.getM() > 0) {
                                                scanInfoList.add(actuatorInfo + "m:" + ktraj.getM());
                                            }
                                            scanInfoList.add(actuatorInfo + "kmin:" + ktraj.getKMin());
                                            scanInfoList.add(actuatorInfo + "kmax:" + ktraj.getKMax());
                                            scanInfoList.add(actuatorInfo + "kdelta:" + ktraj.getKDelta());
                                            scanInfoList.add(actuatorInfo + "deadtime:" + ktraj.getDeadTime());
                                        } else {
                                            if (rangeIndex == 1) {
                                                if (trajectory instanceof ITrajectoryHCS) {
                                                    scanInfoList.add(actuatorInfo + "type:HCS");
                                                }
                                                if (trajectory instanceof ITrajectoryEnergy) {
                                                    scanInfoList.add(actuatorInfo + "type:Energy");
                                                }
                                            }

                                            scanInfoList
                                                    .add(actuatorInfo + rangeInfo + "steps:" + range.getStepsNumber());
                                            if (trajectory.getRelative()) {
                                                scanInfoList.add(actuatorInfo + rangeInfo + "relative:true");
                                            }
                                            if (trajectory.isCustomTrajectory()) {
                                                scanInfoList.add(actuatorInfo + rangeInfo + "custom_values:"
                                                        + Arrays.toString(trajectory.getTrajectory()));
                                            } else {
                                                scanInfoList.add(actuatorInfo + rangeInfo + "from:"
                                                        + trajectory.getBeginPosition());
                                                scanInfoList.add(
                                                        actuatorInfo + rangeInfo + "to:" + trajectory.getEndPosition());
                                                scanInfoList.add(
                                                        actuatorInfo + rangeInfo + "delta:" + trajectory.getDelta());
                                            }
                                        }
                                    } // if actuator enable
                                } // end if trajectory list
                                rangeIndex++;
                            } // End for range list
                        } // End for actuator list
                    } // End if range list
                } // End if actuator list
                dimensionIndex++;
            } // End for dimension list
        } // End if dimension list

        return scanInfoList.toArray(new String[scanInfoList.size()]);
    }

    @Override
    public String toString() {
        String toStringValue = super.toString();
        String fullPath = getFullPath();
        if (SalsaUtils.isDefined(fullPath)) {
            toStringValue = fullPath;
        }
        return toStringValue;
    }

}

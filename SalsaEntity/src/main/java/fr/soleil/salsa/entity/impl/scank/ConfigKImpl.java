package fr.soleil.salsa.entity.impl.scank;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.ConfigKModel;
import fr.soleil.salsa.entity.impl.ConfigImpl;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;

public class ConfigKImpl extends ConfigImpl<IDimensionK> implements IConfigK {

    private static final long serialVersionUID = -8540308741218439361L;

    /**
     * Constructor.
     */
    public ConfigKImpl() {
        super();
        setType(ScanType.SCAN_K);
    }

    @Override
    protected IConfig<IDimensionK> initModel() {
        ConfigKModel configKModel = new ConfigKModel(this);
        IDimensionK dimension = (IDimensionK) getDimensionX();
        if (dimension != null && dimension instanceof IObjectImpl<?>) {
            IDimensionK dimensionModel = (IDimensionK) ((IObjectImpl<?>) dimension).toModel();
            List<IActuator> actuatorList = dimension.getActuatorsList();
            List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
            dimensionModel.setActuatorsList(actuatorListModel);
            configKModel.setDimensionX(dimensionModel);
        }
        return configKModel;
    }
}

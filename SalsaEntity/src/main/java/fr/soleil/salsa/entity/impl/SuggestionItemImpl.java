package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.ISuggestionItem;

public class SuggestionItemImpl implements ISuggestionItem {

    private static final long serialVersionUID = -2740084191058016241L;

    /**
     * The label.
     */
    private String label;

    /**
     * The value.
     */
    private String value;

    /**
     * True if available.
     */
    private boolean available;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionItem#getLabel()
     */
    public String getLabel() {
        return label;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionItem#getValue()
     */
    public String getValue() {
        return value;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionItem#isAvailable()
     */
    public boolean isAvailable() {
        return available;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionItem#setAvailable(boolean)
     */
    public void setAvailable(boolean available) {
        this.available = available;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionItem#setLabel(java.lang.String)
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.ISuggestionItem#setValue(java.lang.String)
     */
    public void setValue(String value) {
        this.value = value;
    }

}

package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.event.HookCommandModel;

public class HookCommandImpl implements IHookCommand {

    private static final long serialVersionUID = -4726392386317650013L;

    /**
     * The command.
     */
    private String command = null;

    private boolean enabled = true;

    IHookCommand model = null;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IHookCommand#getCommand()
     */
    public String getCommand() {
        return command;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IHookCommand#setCommand(java.lang.String)
     */
    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public boolean isEnable() {
        return enabled;
    }

    @Override
    public void setEnable(boolean enable) {
        enabled = enable;
    }

    @Override
    public IHookCommand toModel() {
        if (model == null) {
            model = new HookCommandModel(this);
        }
        return model;
    }

    @Override
    public IHookCommand toImpl() {
        return this;
    }

}

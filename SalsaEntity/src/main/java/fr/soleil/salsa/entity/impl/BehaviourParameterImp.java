package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IModelElement;

/**
 * 
 * Class relative to the Behaviour Parameter.
 * @author Alike
 * 
 */
public class BehaviourParameterImp implements IModelElement {

    /**
     * Id.
     */
    private Integer id;

    /**
     * Name.
     */
    private String name;

    /* (non-Javadoc)
     * @see com.alike.impl.ModelElement#getId()
     */
    public Integer getId() {
        return id;
    }

    /**
     * Get the name.
     * @return
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see com.alike.impl.ModelElement#setId(java.lang.Integer)
     */
    public void setId(Integer id) {
        this.id = id;

    }

    /**
     * Set the name.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

}

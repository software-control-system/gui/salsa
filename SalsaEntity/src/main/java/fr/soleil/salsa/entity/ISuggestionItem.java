package fr.soleil.salsa.entity;

import java.io.Serializable;

public interface ISuggestionItem extends Serializable {

    /**
     * True if the suggestion is available.
     * @return
     */
    boolean isAvailable();

    /**
     * Sets true if the suggestion is available.
     * @param available
     */
    void setAvailable(boolean available);

    /**
     * Sets the label.
     * @param label
     */
    void setLabel(String label);

    /**
     * Sets the value.
     * @param value
     */
    void setValue(String value);

    /**
     * Gets the label.
     * @return
     */
    String getLabel();

    /**
     * Gets the value.
     * @return
     */
    String getValue();

}

package fr.soleil.salsa.entity.impl.scan2d;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.Trajectory2DModel;
import fr.soleil.salsa.entity.impl.TrajectoryImpl;
import fr.soleil.salsa.entity.scan2D.ITrajectory2D;

/**
 * @author Alike.
 * 
 */
public class Trajectory2DImpl extends TrajectoryImpl implements ITrajectory2D {

    private static final long serialVersionUID = 6408723614278074242L;

    /**
     * The actuator.
     */
    private IActuator actuator;

    /**
     * Get the actuator.
     * 
     * @return
     */
    public IActuator getActuator() {
        return actuator;
    }

    /**
     * Set the actuator.
     * 
     * @param actuator
     */
    public void setActuator(IActuator actuator) {
        this.actuator = actuator;
    }

    protected ITrajectory initModel() {
        return new Trajectory2DModel(this);
    }

}

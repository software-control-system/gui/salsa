package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

public interface ISuggestions extends Serializable {

    /**
     * Gets suggestions for actuators.
     * @return
     */
    List<ISuggestionCategory> getActuatorSuggestionList();

    /**
     * Gets suggestions for sensors.
     * @return
     */
    List<ISuggestionCategory> getSensorSuggestionList();

    /**
     * Gets suggestions for timebase.
     * @return
     */
    List<ISuggestionCategory> getTimebaseSuggestionList();

    /**
     * Sets suggestions for actuators.
     * @param actuatorSuggestionList
     */
    void setActuatorSuggestionList(
            List<ISuggestionCategory> actuatorSuggestionList);

    /**
     * Sets suggestions for sensors.
     * @param sensorSuggestionList
     */
    void setSensorSuggestionList(
            List<ISuggestionCategory> sensorSuggestionList);

    /**
     * Sets suggestions for timebases.
     * @param timebaseSuggestionList
     */
    void setTimebaseSuggestionList(
            List<ISuggestionCategory> timebaseSuggestionList);

}

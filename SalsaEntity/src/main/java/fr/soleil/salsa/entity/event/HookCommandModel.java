package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.impl.HookCommandImpl;

public class HookCommandModel extends AEventHandlingModelDecorator<IHookCommand> implements
        IHookCommand {

    private static final long serialVersionUID = -1561963528854436307L;

    /**
     * @param baseBean
     */
    public HookCommandModel(IHookCommand baseBean) {
        super(baseBean);
    }

    /**
     * 
     */
    public HookCommandModel() {
        super(new HookCommandImpl());
    }

    @Override
    public String getCommand() {
        return baseBean.getCommand();
    }

    @Override
    public void setCommand(String command) {
        String oldValue = baseBean.getCommand();
        baseBean.setCommand(command);
        this.firePropertyChange("command", oldValue, command);
    }

    @Override
    public boolean isEnable() {
        return baseBean.isEnable();
    }

    @Override
    public void setEnable(boolean enable) {
        boolean oldValue = baseBean.isEnable();
        baseBean.setEnable(enable);
        this.firePropertyChange("enable", oldValue, enable);
    }

    @Override
    public IHookCommand toModel() {
        return this;
    }

}

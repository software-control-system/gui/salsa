package fr.soleil.salsa.entity;


/**
 * Interface for the actuator entity. An actuator is a device that lets the scan progress from one
 * step to the next. For instance, it could be an engine that moves the sample between each mesure.
 * 
 * @author Administrateur
 */
public interface IActuator extends IDevice {

}

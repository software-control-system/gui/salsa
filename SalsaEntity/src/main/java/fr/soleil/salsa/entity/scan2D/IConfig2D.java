package fr.soleil.salsa.entity.scan2D;

import fr.soleil.salsa.entity.IConfig;

/**
 * @author Alike.
 *
 */
public interface IConfig2D extends IConfig<IDimension2DX> {

    /**
     * Get the Y dimension.
     *
     * @return
     */
    public IDimension2DY getDimensionY();

    /**
     * Set the Y dimension.
     *
     * @param dimensionY
     */
    public void setDimensionY(IDimension2DY dimensionY);


}

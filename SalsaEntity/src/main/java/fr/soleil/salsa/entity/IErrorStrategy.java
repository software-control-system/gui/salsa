package fr.soleil.salsa.entity;

import java.io.Serializable;

import fr.soleil.salsa.entity.impl.ErrorStrategyType;

/**
 * @author Alike
 * 
 *         The interface for the error strategy.
 */
public interface IErrorStrategy extends Serializable {

    /**
     * Gets the actuators error strategy.
     * 
     * @return
     */
    IErrorStrategyItem getActuatorsErrorStrategy();

    /**
     * Gets the context validation device.
     * 
     * @return
     */
    String getContextValidationDevice();

    /**
     * Gets the context validation strategy.
     * 
     * @return
     */
    ErrorStrategyType getContextValidationStrategy();

    /**
     * Gets the hooks error strategy.
     * 
     * @return
     */
    IErrorStrategyItem getHooksErrorStrategy();

    /**
     * Gets the sensors error strategy.
     * 
     * @return
     */
    IErrorStrategyItem getSensorsErrorStrategy();

    /**
     * Gets the timebases errors strategy.
     * 
     * @return
     */
    IErrorStrategyItem getTimebasesErrorStrategy();

    /**
     * Sets the actuator error strategy.
     * 
     * @param actuatorsErrorStrategy
     */
    void setActuatorsErrorStrategy(IErrorStrategyItem actuatorsErrorStrategy);

    /**
     * Sets the context validation device.
     * 
     * @param contextValidationDevice
     */
    void setContextValidationDevice(String contextValidationDevice);

    /**
     * Sets the context validation strategy.
     * 
     * @param contextValidationStrategy
     */
    void setContextValidationStrategy(ErrorStrategyType contextValidationStrategy);

    /**
     * Sets the hooks error strategy.
     * 
     * @param hooksErrorStrategy
     */
    void setHooksErrorStrategy(IErrorStrategyItem hooksErrorStrategy);

    /**
     * Sets the sensors error strategy.
     * 
     * @param sensorsErrorStrategy
     */
    void setSensorsErrorStrategy(IErrorStrategyItem sensorsErrorStrategy);

    /**
     * Sets the timebases error strategy.
     * 
     * @param timebasesErrorStrategy
     */
    void setTimebasesErrorStrategy(IErrorStrategyItem timebasesErrorStrategy);

    public IErrorStrategy toModel();

    public IErrorStrategy toImpl();

}

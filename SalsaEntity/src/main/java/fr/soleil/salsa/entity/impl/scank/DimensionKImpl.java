package fr.soleil.salsa.entity.impl.scank;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.event.DimensionKModel;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.entity.util.ComparatorUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class DimensionKImpl extends DimensionImpl implements IDimensionK {

    private static final long serialVersionUID = -1659768505376626544L;

    /**
     * The K range.
     */
    private IRangeK rangeX;

    private List<IRangeK> rangesXList;

    /**
     * Constructor.
     */
    public DimensionKImpl() {
        super();
        rangeX = new RangeKImpl();
        rangesXList = new ArrayList<IRangeK>();
        rangesXList.add(rangeX);
    }

    /**
     * Gets the K range.
     */
    @Override
    public IRangeK getRangeX() {
        return rangeX;
    }

    /**
     * Gets the K trajectory.
     */
    @Override
    public ITrajectoryK getTrajectory() {
        return rangeX.getTrajectory();
    }

    /**
     * Sets the K range.
     */
    @Override
    public void setRangeX(IRangeK rangeX) {
        this.rangeX = rangeX;
        rangesXList.clear();
        rangesXList.add(rangeX);
    }

    /**
     * Sets the K trajectory.
     */
    @Override
    public void setTrajectory(ITrajectoryK trajectory) {
        rangeX.setTrajectory(trajectory);
    }

    @Override
    protected IDimension initModel() {
        return new DimensionKModel(this);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null && super.isEqualsTo(dimension)) {
            if (dimension instanceof IDimensionK) {
                equals = ComparatorUtil.rangeEquals(((IDimensionK) dimension).getRangeX(), getRangeX());
            }
        }
        return equals;
    }

    @Override
    public List<IRangeK> getRangesXList() {
        return rangesXList;
    }

    @Override
    public void setRangesXList(List<IRangeK> rangesX) {
        if (rangesXList == null) {
            rangesXList = new ArrayList<IRangeK>();
        }
        rangesXList.clear();
        if (SalsaUtils.isFulfilled(rangesX)) {
            rangesXList.addAll(rangesX);
            rangeX = rangesX.get(0);
        }
    }

}

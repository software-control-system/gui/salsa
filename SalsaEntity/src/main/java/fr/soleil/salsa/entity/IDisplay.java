package fr.soleil.salsa.entity;

import java.io.Serializable;
import java.util.List;

/**
 * The display manager parameters.
 */
public interface IDisplay extends Serializable {

    public void setTimeBase(boolean timeBase);

    public boolean isTimeBase();

    /**
     * The list of axis, in order.
     * 
     * @return
     */
    public List<DisplayAxis> getAxisList();

    /**
     * The list of axis, in order.
     * 
     * @param axisList
     */
    public void setAxisList(List<DisplayAxis> axisList);

    public IDisplay toModel();

    public IDisplay toImpl();

}

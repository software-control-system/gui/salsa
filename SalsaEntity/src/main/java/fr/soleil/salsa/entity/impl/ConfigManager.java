package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.event.ActuatorModel;
import fr.soleil.salsa.entity.event.SensorModel;
import fr.soleil.salsa.entity.event.TimebaseModel;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class ConfigManager extends ConfigImpl<DimensionImpl> {

    private static final long serialVersionUID = -2295866172608093356L;

    public static final Logger LOGGER = LoggingUtil.getLogger(ConfigManager.class);
    private List<IConfig<?>> configList = new ArrayList<>();
    private List<IActuator> actuatorList = new ArrayList<>();

    public ConfigManager() {
        setName("ConfigManager");
    }

    public void setConfigList(final List<IConfig<?>> aconfigList) {
        configList.clear();
        if (aconfigList != null) {
            for (IConfig<?> config : aconfigList) {
                if (!configList.contains(config)) {
                    configList.add(config);
                }
            }
        }
        updateDevicesList();
    }

    public List<IConfig<?>> getConfigList() {
        return configList;
    }

    @Override
    public void renameActuator(final String oldActuatorName, final String newActuatorName, final int dimension) {
        if (!ObjectUtils.sameObject(oldActuatorName, newActuatorName)) {
            if (configList != null) {
                List<IActuator> tmpActuatorList = null;
                for (IConfig<?> config : configList) {
                    tmpActuatorList = config.getActuatorList(dimension);
                    // Add only if necessary
                    if (super.getDevice(oldActuatorName, tmpActuatorList) != null) {
                        config.renameActuator(oldActuatorName, newActuatorName, dimension);
                    }
                }
                IDevice device = super.getDevice(oldActuatorName, actuatorList);
                if (device != null) {
                    device.setName(newActuatorName);
                }
            }
        }
    }

    @Override
    public void addActuator(final String actuatorName, final int dimension) {
        if (configList != null) {
            List<IActuator> tmpActuatorList = null;
            for (IConfig<?> config : configList) {
                tmpActuatorList = config.getActuatorList(dimension);
                if (super.getDevice(actuatorName, tmpActuatorList) == null) {
                    config.addActuator(actuatorName, dimension);
                }
            }
            IDevice device = super.getDevice(actuatorName, actuatorList);
            if (device != null) {
                device.setCommon(true);
            } else {
                device = new ActuatorModel();
                device.setName(actuatorName);
                actuatorList.add((IActuator) device);
                IDimension tmpDimension = null;
                if (dimension == FIRST_DIMENSION && !dimensionList.isEmpty()) {
                    tmpDimension = dimensionList.get(0);
                } else if (dimension == SECOND_DIMENSION && dimensionList.size() > 1) {
                    tmpDimension = dimensionList.get(1);
                }
                if (tmpDimension != null) {
                    tmpActuatorList = tmpDimension.getActuatorsList();
                    if (tmpActuatorList != null) {
                        tmpActuatorList.add((IActuator) device);
                    }
                }
            }
        }
    }

    @Override
    public void deleteActuator(final String actuatorName, final int dimension) {
        if (configList != null) {
            List<IActuator> tmpActuatorList = null;
            for (IConfig<?> config : configList) {
                // Remove only if necessary
                tmpActuatorList = config.getActuatorList(dimension);
                if (super.getDevice(actuatorName, tmpActuatorList) != null) {
                    config.deleteActuator(actuatorName, dimension);
                }
            }
        }
        updateDevicesList();
    }

    @Override
    public void renameSensor(final String oldSensorName, final String newSensorName) {
        LOGGER.trace("renameSensor {} to {}", oldSensorName, newSensorName);
        if (!ObjectUtils.sameObject(oldSensorName, newSensorName)) {
            if (configList != null) {
                List<ISensor> tmpSensorList = null;
                for (IConfig<?> config : configList) {
                    tmpSensorList = config.getSensorsList();
                    // Add only if necessary
                    if (super.getDevice(oldSensorName, tmpSensorList) != null) {
                        LOGGER.trace("renameSensor {} to {} in {}", oldSensorName, newSensorName, config);
                        config.renameSensor(oldSensorName, newSensorName);
                    }
                }
                IDevice device = super.getDevice(oldSensorName, getSensorsList());
                if (device != null) {
                    device.setName(newSensorName);
                }
            }
        }
    }

    @Override
    public void addSensor(final String sensorName) {
        LOGGER.trace("addSensor {}", sensorName);
        if (configList != null) {
            List<ISensor> tmpSensorList = null;
            for (IConfig<?> config : configList) {
                tmpSensorList = config.getSensorsList();
                // Add only if necessary
                if (super.getDevice(sensorName, tmpSensorList) == null) {
                    LOGGER.trace("addSensor {} in {}", sensorName, config);
                    config.addSensor(sensorName);
                }
            }
            IDevice device = super.getDevice(sensorName, getSensorsList());
            if (device != null) {
                device.setCommon(true);
            } else {
                super.addSensor(sensorName);
            }
        }
    }

    @Override
    public void deleteSensor(final String sensorName) {
        LOGGER.trace("deleteSensor {}", sensorName);
        if (configList != null) {
            List<ISensor> tmpSensorList = null;
            for (IConfig<?> config : configList) {
                tmpSensorList = config.getSensorsList();
                // Remove only if necessary
                if (super.getDevice(sensorName, tmpSensorList) != null) {
                    LOGGER.trace("deleteSensor {} in {}", sensorName, config);
                    config.deleteSensor(sensorName);
                }
            }
        }
        super.deleteSensor(sensorName);
    }

    @Override
    public void renameTimeBase(final String oldTimebaseName, final String newTimebaseName) {
        LOGGER.trace("renameTimeBase {} to {}", oldTimebaseName, newTimebaseName);
        if (!ObjectUtils.sameObject(oldTimebaseName, newTimebaseName)) {
            if (configList != null) {
                List<ITimebase> tmpTimebaseList = null;
                for (IConfig<?> config : configList) {
                    tmpTimebaseList = config.getTimebaseList();
                    // Rename only if necessary
                    if (super.getDevice(oldTimebaseName, tmpTimebaseList) != null) {
                        LOGGER.trace("renameTimeBase {} to {} in {}", oldTimebaseName, newTimebaseName, config);
                        config.renameTimeBase(oldTimebaseName, newTimebaseName);
                    }
                }
                IDevice device = super.getDevice(oldTimebaseName, getTimebaseList());
                if (device != null) {
                    device.setName(newTimebaseName);
                }
            }
        }
    }

    @Override
    public void addTimeBase(final String timebaseName) {
        LOGGER.trace("addTimeBase {}", timebaseName);
        if (configList != null) {
            List<ITimebase> tmpTimebaseList = null;
            for (IConfig<?> config : configList) {
                tmpTimebaseList = config.getTimebaseList();
                // Add only if necessary
                if (super.getDevice(timebaseName, tmpTimebaseList) == null) {
                    LOGGER.trace("addTimeBase {} in {}", timebaseName, config);
                    config.addTimeBase(timebaseName);
                }
            }
            IDevice device = super.getDevice(timebaseName, getTimebaseList());
            if (device != null) {
                device.setCommon(true);
            } else {
                super.addTimeBase(timebaseName);
            }
        }
    }

    @Override
    public void deleteTimeBase(final String timebaseName) {
        LOGGER.trace("deleteTimeBase {}", timebaseName);
        if (configList != null) {
            List<ITimebase> tmpTimebaseList = null;
            for (IConfig<?> config : configList) {
                // Add only if necessary
                tmpTimebaseList = config.getTimebaseList();
                if (super.getDevice(timebaseName, tmpTimebaseList) != null) {
                    LOGGER.trace("deleteTimeBase {} in {}", timebaseName, config);
                    config.deleteTimeBase(timebaseName);
                }
            }
            super.deleteTimeBase(timebaseName);
        }
    }

    @Override
    public void setActuatorEnable(final String actuatorName, final boolean enabled, final int dimension) {
        LOGGER.trace("setActuatorEnable {} = {}", actuatorName, enabled);
        if (configList != null) {
            for (IConfig<?> config : configList) {
                if (config.isActuatorEnable(actuatorName, dimension) != enabled) {
                    LOGGER.trace("setActuatorEnable {} = {} in {}", actuatorName, enabled, config);
                    config.setActuatorEnable(actuatorName, enabled, dimension);
                }
            }
            super.setActuatorEnable(actuatorName, enabled, dimension);
        }
    }

    @Override
    public void setSensorEnable(final String sensorName, final boolean enabled) {
        LOGGER.trace("setSensorEnable {} = {}", sensorName, enabled);
        if (configList != null) {
            for (IConfig<?> config : configList) {
                if (config.isSensorEnable(sensorName) != enabled) {
                    LOGGER.trace("setSensorEnable {}={} in {} ", sensorName, enabled, config);
                    config.setSensorEnable(sensorName, enabled);
                }
            }
            super.setSensorEnable(sensorName, enabled);
        }
    }

    @Override
    public void setTimeBaseEnable(final String timeBaseName, final boolean enabled) {
        LOGGER.trace("setTimeBaseEnable {} = {}", timeBaseName, enabled);
        if (configList != null) {
            for (IConfig<?> config : configList) {
                if (config.isTimeBaseEnable(timeBaseName) != enabled) {
                    LOGGER.trace("setTimeBaseEnable {}={} in {}", timeBaseName, enabled, config);
                    config.setTimeBaseEnable(timeBaseName, enabled);
                }
            }
            super.setSensorEnable(timeBaseName, enabled);
        }
    }

    @Override
    public boolean isActuatorEnable(final String actuatorName, final int dimension) {
        return isDeviceEnable(actuatorName, getActuatorList(dimension));
    }

    @Override
    public List<IActuator> getActuatorList(final int dimension) {
        return actuatorList;
    }

    @Override
    public List<IDimension> getDimensionList() {
        return dimensionList;
    }

    private void updateDevicesList() {
        List<ISensor> sensorList = new ArrayList<ISensor>();
        List<ITimebase> timeBaseList = new ArrayList<ITimebase>();
        dimensionList.clear();
        actuatorList.clear();
        List<? extends IDevice> tmpDeviceList = null;
        for (IConfig<?> config : configList) {
            // Get sensors
            tmpDeviceList = config.getSensorsList();
            if (tmpDeviceList != null) {
                for (IDevice sensor : tmpDeviceList) {
                    LOGGER.trace("sensor={} from {}", sensor, config);
                    updateDeviceList(config, sensor, sensorList);
                }
            }
            // Get timebase
            tmpDeviceList = config.getTimebaseList();
            if (tmpDeviceList != null) {
                for (IDevice timebase : tmpDeviceList) {
                    LOGGER.trace("timebase={} from {}", timebase, config);
                    updateDeviceList(config, timebase, timeBaseList);
                }
            }

            // Get actuators
            List<IDimension> tmpDimensionList = config.getDimensionList();
            IDimension tmpDimension = null;
            IDimension managerDimension = null;
            List<IActuator> managerActuators = null;
            if (SalsaUtils.isFulfilled(tmpDimensionList)) {
                for (int index = 0; index < tmpDimensionList.size(); index++) {
                    tmpDimension = tmpDimensionList.get(index);
                    tmpDeviceList = tmpDimension.getActuatorsList();
                    if (index < dimensionList.size()) {
                        managerDimension = dimensionList.get(index);
                    } else {
                        managerDimension = new DimensionImpl();
                        dimensionList.add(managerDimension);
                    }

                    managerActuators = managerDimension.getActuatorsList();
                    if (managerActuators == null) {
                        managerActuators = new ArrayList<IActuator>();
                        managerDimension.setActuatorsList(managerActuators);
                    }
                    for (IDevice actuator : tmpDeviceList) {
                        LOGGER.trace("actuator={} from {}", actuator, config);
                        IDevice newActuator = updateDeviceList(config, actuator, actuatorList);
                        if (super.getDevice(newActuator.getName(), managerActuators) == null) {
                            managerActuators.add((IActuator) newActuator);
                        }
                    }
                }
            }
        }
        LOGGER.trace("configuration list size ={}", configList.size());
        super.setSensorsList(sensorList);
        super.setTimebaseList(timeBaseList);
        // Update common properties
        updateCommonDevice(sensorList);
        updateCommonDevice(timeBaseList);
        // updateCommonDevice(actuatorList);
        IDimension tmpDimension = null;
        for (int index = 0; index < dimensionList.size(); index++) {
            tmpDimension = dimensionList.get(index);
            tmpDeviceList = tmpDimension.getActuatorsList();
            updateCommonDevice(tmpDeviceList, index);
        }
    }

    @SuppressWarnings("unchecked")
    private IDevice updateDeviceList(final IConfig<?> config, final IDevice device,
            final List<? extends IDevice> deviceList) {
        IDevice newDevice = null;
        if (device != null) {
            // JIRA SCAN-350
            String deviceName = device.getName();
            IDevice tmpDevice = super.getDevice(deviceName, deviceList);
            newDevice = tmpDevice;
            if (tmpDevice == null) {
                LOGGER.trace("add device {} from {}", deviceName, config);
                if (device instanceof ISensor) {
                    ISensor sensor = new SensorModel();
                    sensor.setName(deviceName);
                    sensor.setEnabled(false);
                    ((List<ISensor>) deviceList).add(sensor);
                    newDevice = sensor;
                }
                if (device instanceof IActuator) {
                    IActuator actuator = new ActuatorModel();
                    actuator.setName(deviceName);
                    actuator.setEnabled(false);
                    ((List<IActuator>) deviceList).add(actuator);
                    newDevice = actuator;
                }
                if (device instanceof ITimebase) {
                    ITimebase timebase = new TimebaseModel();
                    timebase.setName(deviceName);
                    timebase.setEnabled(false);
                    ((List<ITimebase>) deviceList).add(timebase);
                    newDevice = timebase;
                }
            } else if (device.isEnabled()) {
                tmpDevice.setEnabled(true);
            }
        }
        return newDevice;
    }

    private void updateCommonDevice(final List<? extends IDevice> deviceList) {
        if (deviceList != null) {
            List<? extends IDevice> configDeviceList = null;
            // Sensors
            for (IDevice device : deviceList) {
                for (IConfig<?> config : configList) {
                    if (device instanceof ISensor) {
                        configDeviceList = config.getSensorsList();
                    }
                    if (device instanceof ITimebase) {
                        configDeviceList = config.getTimebaseList();
                    }
                    if (super.getDevice(device.getName(), configDeviceList) == null) {
                        device.setCommon(false);
                        break;
                    }
                }
            }
        }
    }

    private void updateCommonDevice(final List<? extends IDevice> deviceList, final int dimensionIndex) {
        if (deviceList != null) {
            List<? extends IDevice> configDeviceList = null;
            List<IDimension> configDimensionList = null;
            IDimension configDimension = null;
            // Sensors
            for (IDevice device : deviceList) {
                boolean first = true;
                for (IConfig<?> config : configList) {
                    if (first) {
                        if (device instanceof IActuator) {
                            configDimensionList = config.getDimensionList();
                            if (dimensionIndex < configDimensionList.size()) {
                                configDimension = configDimensionList.get(dimensionIndex);
                            }
                            if (configDimension != null) {
                                configDeviceList = configDimension.getActuatorsList();
                            }
                        }
                        first = false;
                    }
                    if (super.getDevice(device.getName(), configDeviceList) == null) {
                        device.setCommon(false);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void swapActuator(final String actuatorName1, final String actuatorName2, final int dimension) {
        if (configList != null) {
            for (IConfig<?> config : configList) {
                config.swapActuator(actuatorName1, actuatorName2, dimension);
            }
        }
        super.swapActuator(actuatorName1, actuatorName2, dimension);
    }

    @Override
    public void swapSensor(final String sensorName1, final String sensorName2) {
        if (configList != null) {
            for (IConfig<?> config : configList) {
                config.swapSensor(sensorName1, sensorName2);
            }
        }
        super.swapSensor(sensorName1, sensorName2);
    }

    @Override
    public void swapTimeBase(final String timeBaseName1, final String timeBaseName2) {
        if (configList != null) {
            for (IConfig<?> config : configList) {
                config.swapSensor(timeBaseName1, timeBaseName2);
            }
        }
        super.swapTimeBase(timeBaseName1, timeBaseName2);
    }

}

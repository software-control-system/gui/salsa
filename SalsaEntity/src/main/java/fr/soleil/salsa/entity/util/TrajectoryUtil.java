package fr.soleil.salsa.entity.util;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IActuatorTrajectory;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.exception.SalsaTrajectoryException;

/**
 * set of function to calculate a trajectory
 * 
 * @author GRAMER
 * 
 */
public abstract class TrajectoryUtil {

    private final static double CONSTANT = 0.512;
    private final static double CONSTANT2 = CONSTANT * CONSTANT;
    public static final double KEV = 1000;

    private static double nbStepPreEdge = 0;
    private static double nbStepEdge = 0;
    private static double nbStepPostEdge = 0;

    // Calculation of the Delta part from begin position , end position and number of steps
    public static double calculDelta(final double beginPos, final double endPos, final int steps) {
        return Math.abs(TrajectoryUtil.calculateDelta(beginPos, endPos, steps));
    }

    public static double calculEndPosition(final double beginPos, final double endPos, final int steps,
            final double delta) {
        double newEndPos = endPos;
        if (endPos < beginPos) {
            newEndPos = beginPos - (steps * delta);
        } else {
            newEndPos = beginPos + (steps * delta);
        }
        return newEndPos;
    }

    public static int calculNbStep(final double beginPos, final double endPos, final double delta) {
        int step = 1;
        if (delta != 0) {
            step = (int) Math.abs(Math.round((endPos - beginPos) / delta));
        }
        return step;
    }

    public static double calculateDelta(final double beginPos, final double endPos, final int steps) {
        return (endPos - beginPos) / steps;
    }

    public static double getTotalIntegrationTime(final double[] integrationTime) {
        double result = 0.0;
        for (int i = 0; i < integrationTime.length; i++) {
            result += integrationTime[i];

        }
        return result;
    }

    // Calculation of the speed part from delta and integration time
    public static double calculSpeed(final double delta, final int steps, final double[] integrationTime) {
        return (delta * steps) / getTotalIntegrationTime(integrationTime);
    }

    public static double[] calculateLinearTrajectory(final double offset, final double beginPosition,
            final double endPosition, final int steps, final boolean continuousScan) {
        // In the continuous scan case there is only 2 points (begin and end position)
        int stepsNumber = steps;
        if (continuousScan) {
            stepsNumber = 1;
        }

        double delta = calculateDelta(beginPosition, endPosition, steps);

        assert Math.abs(delta) == delta : "Error: the delta for the trajectory is miscalculated. End position is "
                + endPosition + ", begin position is " + beginPosition + ", steps number is " + stepsNumber
                + " and delta is " + delta + ". Delta should be |endPosition - beginPosition| / stepsNumber) = "
                + Math.abs(delta) + ".";

        double[] positionsList = new double[stepsNumber + 1];
        for (int posIndex = 0; posIndex < stepsNumber; posIndex++) {
            positionsList[posIndex] = beginPosition + delta * posIndex + offset;
        }
        positionsList[stepsNumber] = endPosition + offset;
        return positionsList;
    }

    public static double[] calculateKTrajectory(ITrajectoryK trajectoryK) throws SalsaTrajectoryException {
        // The number of step for decrementation of the delta between PreEdget and Edge
        int m = (int) (trajectoryK.getM());

        // The delta for PreEdge Region
        double eDeltaPreEdge = trajectoryK.getEDeltaPreEdge();

        // The start value for PreEdge Region
        double eMin = trajectoryK.getEMin();

        // The End value for PreEdge Region and Start Value for Edge Region
        double e1 = trajectoryK.getE1();

        // The delta for Edge Region
        double eDeltaEdge = trajectoryK.getEDeltaEdge();

        // The End value for Edge Region and Start Value for K Region
        double e2 = trajectoryK.getE2();

        // The delta for K region
        double kDelta = trajectoryK.getKDelta();

        // The Energy absorption value for the formula in K Region
        double e0 = trajectoryK.getE0();

        // The End value for K Region
        double kMax = trajectoryK.getKMax();

        // Check condition
        if (kDelta == 0) {
            throw new SalsaTrajectoryException("Kdelta can't be equals to 0");
        }
        if (eDeltaPreEdge == 0) {
            throw new SalsaTrajectoryException("EDeltaPreEdge can't be equals to 0");
        }
        if (e0 == 0) {
            throw new SalsaTrajectoryException("e0 can't be equals to 0");
        }
        if (eDeltaEdge == 0) {
            throw new SalsaTrajectoryException("EDeltaEdge can't be equals to 0");
        }
        if (eMin > e1) {
            throw new SalsaTrajectoryException("eMin can't be greater than e1");
        }
        if (e1 > e2) {
            throw new SalsaTrajectoryException("e1 can't be greater than e2");
        }
        if (eDeltaEdge > eDeltaPreEdge) {
            throw new SalsaTrajectoryException("EDeltaEdge can't be greater than EDeltaPreEdge");
        }

        List<Double> eGrid = new ArrayList<>();

        // The value of the trajectory
        double point = 0;

        // The index where begin the decrementation of the step
        // If m = 0 there is no decrementation

        double transitionStep = 0;
        if (m > 0) {
            transitionStep = (eDeltaPreEdge - eDeltaEdge) / m;
        }

        point = e1;

        double[] transitionValues = new double[m];
        double newStep = 0;

        for (int i = 0; i < transitionValues.length; i++) {
            if (transitionStep < 1) {
                newStep = (i + 1) * transitionStep;
            } else {
                newStep = (transitionValues.length - i) * transitionStep;
            }
            point = point - newStep;
            transitionValues[transitionValues.length - i - 1] = point;
        }

        // PreEdge region From Emin to E1
        double preEdgeWidth = point - eMin;
        int preEdgeNbStep = (int) (preEdgeWidth / eDeltaPreEdge);
        nbStepPreEdge = preEdgeNbStep + m;

        for (int i = 0; i < preEdgeNbStep; i++) {
            point = eMin + i * eDeltaPreEdge;
            eGrid.add(point);
        }

        for (double transitionValue : transitionValues) {
            eGrid.add(transitionValue);
        }

        // Edge region From E1 to E2
        double edgeWidth = e2 - e1;
        int edgeNbStep = (int) (edgeWidth / eDeltaEdge);
        nbStepEdge = edgeNbStep;
        for (int i = 0; i < edgeNbStep; i++) {
            point = e1 + i * eDeltaEdge;
            eGrid.add(point);
        }

        // Post Edge region E2 to KMAX
        // Edge region From E1 to E2
        double kMin = convertEnergyToK(e0, e2, KEV);
        if (kMin > kMax) {
            throw new SalsaTrajectoryException("kMax can't be smaller than " + kMin);
        }

        double kWidth = kMax - kMin;

        int postEdgeNbStep = (int) (kWidth / kDelta);
        if (postEdgeNbStep == 0) {
            throw new SalsaTrajectoryException("kDelta can't be greater than " + kWidth);
        }

        double kValue = 0;
        nbStepPostEdge = postEdgeNbStep;
        for (int i = 0; i < postEdgeNbStep; i++) {
            kValue = kMin + i * kDelta;
            point = convertKToEnergy(e0, kValue, KEV);

            eGrid.add(point);
        }

        if (kValue < kMax) {
            nbStepPostEdge++;
            point = convertKToEnergy(e0, kMax, KEV);
            eGrid.add(point);
        }

        double[] trajectoryValuesList = new double[eGrid.size()];
        for (int i = 0; i < eGrid.size(); i++) {
            trajectoryValuesList[i] = Double.parseDouble(eGrid.get(i).toString());
        }
        return trajectoryValuesList;
    }

    public static double calculateKmin(ITrajectoryK trajectoryK) {
        double kmin = Double.NaN;
        if (trajectoryK != null) {
            kmin = convertEnergyToK(trajectoryK.getE0(), trajectoryK.getE2(), KEV);
        }
        return kmin;
    }

    public static double calculateKTime(double k, double kMin, double postEdgeTime, double n) {
        return postEdgeTime * Math.pow((k / kMin), n);
    }

    /**
     * Computes the integration times for a scan K.
     * 
     * @return
     */
    public static double[] calculateKIntegrationTimes(ITrajectoryK trajectoryK) {
        double timePreEdge = trajectoryK.getTimePreEdge();
        double timeEdge = trajectoryK.getTimeEdge();
        double postEdgeTime = trajectoryK.getTimePostEdge();
        double n = trajectoryK.getN();
        double kDelta = trajectoryK.getKDelta();
        double kmin = trajectoryK.getKMin();

        // N(Time(k) = time * (k/kmin)

        List<Double> tGrid = new ArrayList<>();

        // PreEdge region From Emin to E0
        for (int i = 0; i < nbStepPreEdge; i++) {
            tGrid.add(timePreEdge);
        }

        // Edge region From E1 to E2
        for (int i = 0; i < nbStepEdge; i++) {
            tGrid.add(timeEdge);
        }

        if (kmin == 0) {
            kmin = calculateKmin(trajectoryK);
        }
        if (kmin == 0) {
            kmin = 1;
        }

        // Post Edge region E2 to KMAX
        // Edge region From E1 to E2
        for (int i = 0; i < nbStepPostEdge; i++) {
            double kValue = kmin + i * kDelta;
            double kTime = calculateKTime(kValue, kmin, postEdgeTime, n);
            tGrid.add(kTime);
            // tGrid.add(postEdgeTime);
        }

        double[] timeValuesList = new double[tGrid.size()];
        for (int i = 0; i < tGrid.size(); i++) {
            timeValuesList[i] = Double.parseDouble(tGrid.get(i).toString());
        }

        return timeValuesList;
    }

    // E0 absorption
    // K value
    // unit 1000 if defined in Kev
    public static double convertKToEnergy(double E0, double K, double unit) {
        if (unit == 0) {
            unit = 1;
        }
        // formula E = (K * K)/constant2 + E0 * unit EV
        // formula E Kev = Ev/unit
        double energyEv = K * K / CONSTANT2 + E0 * unit;
        return energyEv / unit;
    }

    // E0 absortion
    // E value
    // unit 1000 if defined in Kev
    public static double convertEnergyToK(double E0, double E, double unit) {
        if (unit == 0) {
            unit = 1;
        }
        // formula K = 0.512 * sqrt (E - E0) unit EV
        // formula K Kev = 0.512 * sqrt ((E - E0) * unit)
        double energyK = CONSTANT * Math.sqrt((E - E0) * unit);
        return energyK;
    }

    public static void swapTrajectory(ITrajectory t1, ITrajectory t2) {
        if ((t1 != null) && (t2 != null)) {
            double t1BeginPosition = t1.getBeginPosition();
            double t1EndPosition = t1.getEndPosition();
            double t1Delta = t1.getDelta();
            double t1Speed = t1.getSpeed();
            Boolean t1Relative = t1.getRelative();
            Boolean t1DeltaConstant = t1.isDeltaConstant();
            String t1Name = t1.getName();
            double[] t1Trajectory = t1.getTrajectory();

            t1.setBeginPosition(t2.getBeginPosition());
            t1.setEndPosition(t2.getEndPosition());
            t1.setDelta(t2.getDelta());
            t1.setSpeed(t2.getSpeed());
            t1.setRelative(t2.getRelative());
            t1.setName(t2.getName());
            t1.setDeltaConstant(t2.isDeltaConstant());
            t1.setTrajectory(t2.getTrajectory());

            t2.setBeginPosition(t1BeginPosition);
            t2.setEndPosition(t1EndPosition);
            t2.setDelta(t1Delta);
            t2.setSpeed(t1Speed);
            t2.setRelative(t1Relative);
            t2.setName(t1Name);
            t2.setDeltaConstant(t1DeltaConstant);
            t2.setTrajectory(t1Trajectory);

            IRange t1range = t1.getIRange();
            IRange t2range = t2.getIRange();
            t1.setIRange(t2range);
            t2.setIRange(t1range);

            if ((t1 instanceof IActuatorTrajectory) && (t2 instanceof IActuatorTrajectory)) {
                IActuator t1Actuator = ((IActuatorTrajectory) t1).getActuator();
                IActuator t2Actuator = ((IActuatorTrajectory) t2).getActuator();
                ((IActuatorTrajectory) t1).setActuator(t2Actuator);
                ((IActuatorTrajectory) t2).setActuator(t1Actuator);
            }
        }
    }

    public static double calculatePostEdgeSteps(double kMin, double kMax, double kDelta) {
        return (kMax - kMin) / kDelta;
    }

    public static double calculatePreEdgeDuration(double eMin, double e1, double eDelta, double time,
            double actuatorDelay) {
        return ((e1 - eMin) / eDelta) * (time + actuatorDelay);
    }

    public static double calculateEdgeDuration(double e1, double e2, double eDelta, double time, double actuatorDelay) {
        return ((e2 - e1) / eDelta) * (time + actuatorDelay);
    }

    public static double calculatePostEdgeDuration(double tMin, double tMax, double kMin, double kMax, double kDelta,
            double actuatorDelay) {
        return (actuatorDelay + (tMax + tMin) / 2) * calculatePostEdgeSteps(kMin, kMax, kDelta);
    }

}

package fr.soleil.salsa.entity.impl.scanenergy;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.event.DimensionEnergyModel;
import fr.soleil.salsa.entity.impl.DimensionImpl;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;
import fr.soleil.salsa.entity.util.ComparatorUtil;

/**
 * @author Alike
 * 
 */
public class DimensionEnergyImpl extends DimensionImpl implements IDimensionEnergy {

    private static final long serialVersionUID = -2841759594416907754L;

    /**
     * The range.
     */
    private List<IRangeEnergy> rangeEnergyList;

    /**
     * Trajectories.
     */
    private List<ITrajectoryEnergy> trajectoriesEnergyList;

    public DimensionEnergyImpl() {
        super();
        rangeEnergyList = new ArrayList<IRangeEnergy>();
        trajectoriesEnergyList = new ArrayList<ITrajectoryEnergy>();
        List<IActuator> actuatorsList = new ArrayList<IActuator>(1);
        this.setActuatorsList(actuatorsList);
    }

    /**
     * Get the range.
     * 
     * @return
     */
    public List<IRangeEnergy> getRangesEnergyList() {
        return rangeEnergyList;
    }

    /**
     * Get trajectories.
     * 
     * @return
     */
    public List<ITrajectoryEnergy> getTrajectoriesEnergyList() {
        return trajectoriesEnergyList;
    }

    /**
     * Set ranges.
     * 
     * @param range
     */
    public void setRangesEnergyList(List<IRangeEnergy> rangeEnergy) {
        this.rangeEnergyList = rangeEnergy;
    }

    /**
     * Set trajectories.
     * 
     * @param trajectories
     */
    public void setTrajectoriesEnergyList(List<ITrajectoryEnergy> trajectoriesEnergy) {
        this.trajectoriesEnergyList = trajectoriesEnergy;
    }

    protected IDimension initModel() {
        return new DimensionEnergyModel(this);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        boolean equals = false;
        if (dimension != null && super.isEqualsTo(dimension)) {
            if (dimension instanceof IDimensionEnergy) {
                equals = ComparatorUtil
                        .rangeListEquals(((IDimensionEnergy) dimension).getRangesEnergyList(),
                                getRangesEnergyList());
            }
        }
        return equals;
    }

}

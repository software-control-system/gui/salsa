package fr.soleil.salsa.entity.event.status;

import fr.soleil.salsa.entity.IScanStatus;
import fr.soleil.salsa.entity.ScanState;

@Deprecated
public interface IStatusSource {

    public ScanState getScanState();

    public IScanStatus getScanStatus();

    public void addScanStatusListener(IScanStatusListener listener);

    public void removeScanStatusListener(IScanStatusListener listener);

    public void removeAllScanStatusListeners();

}

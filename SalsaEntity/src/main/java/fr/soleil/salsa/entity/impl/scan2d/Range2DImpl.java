package fr.soleil.salsa.entity.impl.scan2d;

import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.event.Range2DModel;
import fr.soleil.salsa.entity.impl.RangeImpl;
import fr.soleil.salsa.entity.scan2D.IRange2D;

/**
 * @author Alike
 * 
 */
public class Range2DImpl extends RangeImpl implements IRange2D {

    private static final long serialVersionUID = -3931947345215435563L;

    /**
     * Number of steps.
     */
    private int numberOfSteps;

    /**
     * Integration time.
     */
    private double[] integrationTime;

    /**
     * Default constructor.
     * 
     * @return
     */
    public int getNumberOfSteps() {
        return numberOfSteps;
    }

    /**
     * Set the number of steps.
     * 
     * @param numberOfSteps
     */
    public void setNumberOfSteps(int numberOfSteps) {
        this.numberOfSteps = numberOfSteps;
    }

    /**
     * Get the integration time.
     * 
     * @return
     */
    @Override
    public double[] getIntegrationTime() {
        return integrationTime;
    }

    /**
     * Set the integration time.
     * 
     * @param integrationTime
     */
    @Override
    public void setIntegrationTime(double[] integrationTime) {
        this.integrationTime = integrationTime;
    }

    @Override
    protected IRange initModel() {
        return new Range2DModel(this);
    }
}

package fr.soleil.salsa.entity;


public interface IRangeTrajectory<T extends IRange> extends ITrajectory {

    /**
     * Get the range.
     * 
     * @return
     */
    public T getRange();

    /**
     * Set the range.
     * 
     * @param range
     */
    public void setRange(T range);

}

package fr.soleil.salsa.entity.impl.scan2d;

import fr.soleil.salsa.entity.IRangeTrajectory;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.event.Trajectory2DYModel;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scan2D.ITrajectory2DY;

/**
 * @author Alike
 * 
 * 
 */
public class Trajectory2DYImpl extends Trajectory2DImpl implements ITrajectory2DY,
        IRangeTrajectory<IRange2DY> {

    private static final long serialVersionUID = -6256618154163540136L;

    /**
     * The range.
     */
    private IRange2DY range = null;

    /**
     * Get the range.
     * 
     * @return
     */
    public IRange2DY getRange() {
        return range;
    }

    /**
     * Set the range.
     * 
     * @param range
     */
    public void setRange(IRange2DY range) {
        this.range = range;
    }

    protected ITrajectory initModel() {
        return new Trajectory2DYModel(this);
    }

    @Override
    public Boolean getDeltaConstant() {
        return isDeltaConstant();
    }

}

package fr.soleil.salsa.entity.event.status;

@Deprecated
public interface IScanStatusListener {

    public void stateChanged(ScanStatusEvent event);

}

package fr.soleil.salsa.entity.event;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.wrapping.IBeanWrapper;

/**
 * Model entity are supposed to send an event when one of their property is changed. If the property
 * is a collection, we want the event to be fired if the collection is modified, even if it is not
 * entirely replaced. Therefore, we need collections that send events when methods like add are
 * called. This is such an implementation of java.util.List. It works by wrapping a list.
 * 
 * @author Administrateur
 * 
 * @param <E> the element type
 * @param <T> the entity type that contains this list as a property.
 */
public class ListModel<E, T extends IEventSource<?>> implements List<E>, IBeanWrapper<List<E>> {

    /**
     * The decorated collection.
     */
    protected List<E> baseCollection;

    /**
     * The actual entity that this collection is a property of.
     */
    protected T entity;

    /**
     * The property name.
     */
    protected String propertyName;

    /**
     * Constructeur
     * 
     * @param baseList the decorated List.
     * @param entity the actual entity that this collection is a property of.
     * @param propertyName the property name.
     */
    public ListModel(List<E> baseList, T entity, String propertyName) {
        this.baseCollection = baseList;
        this.entity = entity;
        this.propertyName = propertyName;
    }

    /**
     * Returns the base collection.
     */
    public List<E> getBaseBean() {
        return baseCollection;
    }

    /**
     * Sends the EntityPropertyChangedEvent. Reminder : the EntityPropertyChangedEvent must be
     * called AFTER the property has been changed in the bean.
     * 
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    @SuppressWarnings("unchecked")
    protected void firePropertyChange() {
        EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                this.entity, this.propertyName, null, this);
        SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent,
                ((IEventSource<EntityPropertyChangedEvent<T>>) entity).getListeners());
    }

    /**
     * @param o
     * @return
     * @see java.util.List#add(java.lang.Object)
     */
    public boolean add(E o) {
        boolean result = baseCollection.add(o);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param index
     * @param element
     * @see java.util.List#add(int, java.lang.Object)
     */
    public void add(int index, E element) {
        baseCollection.add(index, element);
        this.firePropertyChange();
    }

    /**
     * @param c
     * @return
     * @see java.util.List#addAll(java.util.Collection)
     */
    public boolean addAll(Collection<? extends E> c) {
        boolean result = baseCollection.addAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param index
     * @param c
     * @return
     * @see java.util.List#addAll(int, java.util.Collection)
     */
    public boolean addAll(int index, Collection<? extends E> c) {
        boolean result = baseCollection.addAll(index, c);
        this.firePropertyChange();
        return result;
    }

    /**
     * 
     * @see java.util.List#clear()
     */
    public void clear() {
        baseCollection.clear();
        this.firePropertyChange();
    }

    /**
     * @param o
     * @return
     * @see java.util.List#contains(java.lang.Object)
     */
    public boolean contains(Object o) {
        return baseCollection.contains(o);
    }

    /**
     * @param c
     * @return
     * @see java.util.List#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c) {
        return baseCollection.containsAll(c);
    }

    /**
     * @param o
     * @return
     * @see java.util.List#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        return baseCollection.equals(o);
    }

    /**
     * @param index
     * @return
     * @see java.util.List#get(int)
     */
    public E get(int index) {
        return baseCollection.get(index);
    }

    /**
     * @return
     * @see java.util.List#hashCode()
     */
    public int hashCode() {
        return baseCollection.hashCode();
    }

    /**
     * @param o
     * @return
     * @see java.util.List#indexOf(java.lang.Object)
     */
    public int indexOf(Object o) {
        return baseCollection.indexOf(o);
    }

    /**
     * @return
     * @see java.util.List#isEmpty()
     */
    public boolean isEmpty() {
        return baseCollection.isEmpty();
    }

    /**
     * @return
     * @see java.util.List#iterator()
     */
    public Iterator<E> iterator() {
        // Since an iterator can change the list, we wrap it into an IteratorProperty to the same
        // property.
        return new IteratorModel<E, T>(baseCollection.iterator(), this.entity, this.propertyName);
    }

    /**
     * @param o
     * @return
     * @see java.util.List#lastIndexOf(java.lang.Object)
     */
    public int lastIndexOf(Object o) {
        return baseCollection.lastIndexOf(o);
    }

    /**
     * @return
     * @see java.util.List#listIterator()
     */
    public ListIterator<E> listIterator() {
        // Since an listIterator can change the list, we wrap it into a ListIteratorProperty to the
        // same property.
        return new ListIteratorModel<E, T>(baseCollection.listIterator(), this.entity,
                this.propertyName);
    }

    /**
     * @param index
     * @return
     * @see java.util.List#listIterator(int)
     */
    public ListIterator<E> listIterator(int index) {
        // Since an listIterator can change the list, we wrap it into a ListIteratorProperty to the
        // same property.
        return new ListIteratorModel<E, T>(baseCollection.listIterator(index), this.entity,
                this.propertyName);
    }

    /**
     * @param index
     * @return
     * @see java.util.List#remove(int)
     */
    public E remove(int index) {
        E result = baseCollection.remove(index);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param o
     * @return
     * @see java.util.List#remove(java.lang.Object)
     */
    public boolean remove(Object o) {
        boolean result = baseCollection.remove(o);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.List#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection<?> c) {
        boolean result = baseCollection.removeAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.List#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection<?> c) {
        boolean result = baseCollection.retainAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param index
     * @param element
     * @return
     * @see java.util.List#set(int, java.lang.Object)
     */
    public E set(int index, E element) {
        E result = baseCollection.set(index, element);
        this.firePropertyChange();
        return result;
    }

    /**
     * @return
     * @see java.util.List#size()
     */
    public int size() {
        return baseCollection.size();
    }

    /**
     * @param fromIndex
     * @param toIndex
     * @return
     * @see java.util.List#subList(int, int)
     */
    public List<E> subList(int fromIndex, int toIndex) {
        // Since a sublist can change the list, we wrap it into a new ListProperty to the same
        // property.
        return new ListModel<E, T>(baseCollection.subList(fromIndex, toIndex), this.entity,
                this.propertyName);
    }

    /**
     * @return
     * @see java.util.List#toArray()
     */
    public Object[] toArray() {
        Object[] array = baseCollection.toArray();
        return array;
    }

    /**
     * @param <T2>
     * @param a
     * @return
     * @see java.util.List#toArray(T2[])
     */
    public <T2> T2[] toArray(T2[] a) {
        T2[] array = baseCollection.toArray(a);
        return array;
    }
}
package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.impl.ActuatorImpl;

/**
 * Implementation of {@link IActuator} for the actuator entity. An actuator is a device that lets
 * the scan progress from one step to the next. For instance, it could be an engine that moves the
 * sample between each mesure.
 * 
 * @author Administrateur
 */
public class ActuatorModel extends AEventHandlingModelDecorator<IActuator> implements IActuator {

    private static final long serialVersionUID = 8837207214202439012L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param directory the decorated base bean.
     */
    public ActuatorModel(IActuator actuator) {
        super(actuator);
    }

    /**
     * Default constructor, that creates a new instance of ActuatorImpl and wraps it.
     */
    public ActuatorModel() {
        this(new ActuatorImpl());
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the actuator name.
     * 
     * @return name
     */
    @Override
    public String getName() {
        return baseBean.getName();
    }

    /**
     * Sets the actuator name.
     * 
     * @param String
     */
    @Override
    public void setName(String name) {
        String oldValue = this.baseBean.getName();
        baseBean.setName(name);
        this.firePropertyChange("name", oldValue, this.baseBean.getName());
    }

    /**
     * Gets the actuator attribute name in scan server.
     * 
     * @return name
     */
    @Override
    public String getScanServerAttributeName() {
        return baseBean.getScanServerAttributeName();
    }

    /**
     * Set the actuator attribute name in scan server
     * 
     */
    @Override
    public void setScanServerAttributeName(String scanServerAttributeName) {
        String oldValue = this.baseBean.getScanServerAttributeName();
        baseBean.setScanServerAttributeName(scanServerAttributeName);
        this.firePropertyChange("scanServerAttributeName", oldValue, scanServerAttributeName);
    }

    /**
     * 
     */
    public boolean isEnabled() {
        return this.baseBean.isEnabled();
    }

    /**
     * 
     */
    public void setEnabled(boolean enabled) {
        boolean oldValue = this.baseBean.isEnabled();
        this.baseBean.setEnabled(enabled);
        this.firePropertyChange("enabled", oldValue, enabled);
    }

    @Override
    public boolean isCommon() {
        return baseBean.isCommon();
    }

    @Override
    public void setCommon(boolean common) {
        baseBean.setCommon(common);
    }

    @Override
    public IDevice toModel() {
        return this;
    }

    @Override
    public String toString() {
        return baseBean.toString();
    }
}

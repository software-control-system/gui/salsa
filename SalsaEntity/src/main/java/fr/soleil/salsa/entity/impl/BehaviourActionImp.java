package fr.soleil.salsa.entity.impl;

import java.util.List;

import fr.soleil.salsa.entity.Behaviour;

/**
 * Class relative to the Behaviour action
 * @author Alike
 * 
 */
public class BehaviourActionImp extends ActionImp {

    /**
     * The behaviour.
     */
    private Behaviour behaviour;

    /**
     * Action name
     */
    private String name;

    /**
     * Values.
     */
    private List<BehaviourParameterImp> value;

    /**
     * Get the behaviour<
     * @return
     */
    public Behaviour getBehaviour() {
        return behaviour;
    }

    /**
     * Get the name.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returne values.
     * @return
     */
    public List<BehaviourParameterImp> getValue() {
        return value;
    }

    /**
     * Set the behaviour.
     * @param behaviour
     */
    public void setBehaviour(Behaviour behaviour) {
        this.behaviour = behaviour;
    }

    /**
     * Set the name.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set values.
     * @param value
     */
    public void setValue(List<BehaviourParameterImp> value) {
        this.value = value;
    }

}

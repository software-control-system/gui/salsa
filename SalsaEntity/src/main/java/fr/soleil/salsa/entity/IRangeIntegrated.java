package fr.soleil.salsa.entity;

/**
 * @author Alike
 * 
 */
public interface IRangeIntegrated extends IRange {

    /**
     * Gets the integration time.
     * 
     * @return
     */
    public double[] getIntegrationTime();

    /**
     * Sets the integration time.
     * 
     * @param integrationTime
     */
    public void setIntegrationTime(double[] integrationTime);

}

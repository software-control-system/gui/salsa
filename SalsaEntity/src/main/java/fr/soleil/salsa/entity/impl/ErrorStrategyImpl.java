package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.event.ErrorStrategyModel;

/**
 * Class relative to the Error Strategy.
 * 
 * @author Alike
 * 
 */
public class ErrorStrategyImpl implements IErrorStrategy {

    private static final long serialVersionUID = -8108345766413321238L;

    /**
     * The actuators error strategy.
     */
    private IErrorStrategyItem actuatorsErrorStrategy = new ErrorStrategyItemImpl();

    /**
     * The sensors error strategy.
     */
    private IErrorStrategyItem sensorsErrorStrategy = new ErrorStrategyItemImpl();

    /**
     * The timebases erro strategy.
     */
    private IErrorStrategyItem timebasesErrorStrategy = new ErrorStrategyItemImpl();

    /**
     * The hooks error strategy.
     */
    private IErrorStrategyItem hooksErrorStrategy = new ErrorStrategyItemImpl();

    /**
     * Context validation device
     */
    private String contextValidationDevice = null;

    /**
     * The context validation error strategy.
     */
    private ErrorStrategyType contextValidationStrategy = ErrorStrategyType.IGNORE;

    private IErrorStrategy model = null;

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#getActuatorsErrorStrategy()
     */
    public IErrorStrategyItem getActuatorsErrorStrategy() {
        return actuatorsErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#setActuatorsErrorStrategy(fr.soleil.salsa.entity.IErrorStrategyItem)
     */
    public void setActuatorsErrorStrategy(IErrorStrategyItem actuatorsErrorStrategy) {
        this.actuatorsErrorStrategy = actuatorsErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#getSensorsErrorStrategy()
     */
    public IErrorStrategyItem getSensorsErrorStrategy() {
        return sensorsErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#setSensorsErrorStrategy(fr.soleil.salsa.entity.IErrorStrategyItem)
     */
    public void setSensorsErrorStrategy(IErrorStrategyItem sensorsErrorStrategy) {
        this.sensorsErrorStrategy = sensorsErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#getTimebasesErrorStrategy()
     */
    public IErrorStrategyItem getTimebasesErrorStrategy() {
        return timebasesErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#setTimebasesErrorStrategy(fr.soleil.salsa.entity.IErrorStrategyItem)
     */
    public void setTimebasesErrorStrategy(IErrorStrategyItem timebasesErrorStrategy) {
        this.timebasesErrorStrategy = timebasesErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#getHooksErrorStrategy()
     */
    public IErrorStrategyItem getHooksErrorStrategy() {
        return hooksErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#setHooksErrorStrategy(fr.soleil.salsa.entity.IErrorStrategyItem)
     */
    public void setHooksErrorStrategy(IErrorStrategyItem hooksErrorStrategy) {
        this.hooksErrorStrategy = hooksErrorStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#getContextValidationDevice()
     */
    public String getContextValidationDevice() {
        return contextValidationDevice;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#setContextValidationDevice(java.lang.String)
     */
    public void setContextValidationDevice(String contextValidationDevice) {
        this.contextValidationDevice = contextValidationDevice;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#getContextValidationStrategy()
     */
    public ErrorStrategyType getContextValidationStrategy() {
        return contextValidationStrategy;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IErrorStrategy#setContextValidationStrategy(fr.soleil.salsa.entity.impl.ErrorStrategyType)
     */
    public void setContextValidationStrategy(ErrorStrategyType contextValidationStrategy) {
        this.contextValidationStrategy = contextValidationStrategy;
    }

    @Override
    public IErrorStrategy toModel() {
        if (model == null) {
            model = initModel();
        }
        return model;
    }

    protected IErrorStrategy initModel() {
        ErrorStrategyModel strategyModel = new ErrorStrategyModel(this);
        IErrorStrategyItem itemImpl = getHooksErrorStrategy();
        if (itemImpl != null) {
            strategyModel.setHooksErrorStrategy(itemImpl.toModel());
        }
        itemImpl = getActuatorsErrorStrategy();
        if (itemImpl != null) {
            strategyModel.setActuatorsErrorStrategy(itemImpl.toModel());
        }

        itemImpl = getSensorsErrorStrategy();
        if (itemImpl != null) {
            strategyModel.setSensorsErrorStrategy(itemImpl.toModel());
        }

        itemImpl = getTimebasesErrorStrategy();
        if (itemImpl != null) {
            strategyModel.setTimebasesErrorStrategy(itemImpl.toModel());
        }
        return strategyModel;
    }

    @Override
    public IErrorStrategy toImpl() {
        return this;
    }

}

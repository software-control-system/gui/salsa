package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.impl.TimebaseImpl;

/***
 * Event handling decorator of {@link ITimebase} for the timebase entity. A timeBase is a detector
 * that measures the scanned values.
 * 
 * @author Tarek
 * 
 */
public class TimebaseModel extends AEventHandlingModelDecorator<ITimebase> implements ITimebase {

    private static final long serialVersionUID = -638316470839033280L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param timebase the decorated base bean.
     */
    public TimebaseModel(ITimebase timebase) {
        super(timebase);
    }

    /**
     * Default constructor, that creates a new instance of timebaseImpl and wraps it.
     */
    public TimebaseModel() {
        this(new TimebaseImpl());
    }

    /**
     * Gets the unique identifier.
     * 
     * @return id
     */
    @Override
    public Integer getId() {
        return baseBean.getId();
    }

    /**
     * Gets the timebase name.
     * 
     * @return name
     */
    @Override
    public String getName() {
        return baseBean.getName();
    }

    /**
     * Set the timebase attribute name in scan server
     * 
     */
    @Override
    public String getScanServerAttributeName() {
        return baseBean.getScanServerAttributeName();
    }

    /**
     * True if the timebase is enabled.
     * 
     * @return
     */
    @Override
    public boolean isEnabled() {
        return baseBean.isEnabled();
    }

    /**
     * True if the timebase is enabled.
     * 
     * @param enabled
     */
    @Override
    public void setEnabled(boolean enabled) {
        baseBean.setEnabled(enabled);
    }

    /**
     * Sets the unique identifier.
     * 
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Sets the timebase name.
     * 
     * @param String
     */
    @Override
    public void setName(String name) {
        String oldValue = this.baseBean.getName();
        baseBean.setName(name);
        this.firePropertyChange("name", oldValue, name);

    }

    /**
     * Set the timebase attribute name in scan server
     * 
     */
    @Override
    public void setScanServerAttributeName(String attributeName) {
        String oldValue = this.baseBean.getScanServerAttributeName();
        baseBean.setScanServerAttributeName(attributeName);
        this.firePropertyChange("scanServerAttributeName", oldValue, attributeName);
    }

    @Override
    public boolean isCommon() {
        return baseBean.isCommon();
    }

    @Override
    public void setCommon(boolean common) {
        baseBean.setCommon(common);
    }

    @Override
    public IDevice toModel() {
        return this;
    }

    @Override
    public String toString() {
        return baseBean.toString();
    }

}

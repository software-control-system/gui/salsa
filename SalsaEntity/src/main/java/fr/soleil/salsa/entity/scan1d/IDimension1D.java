package fr.soleil.salsa.entity.scan1d;

import java.util.List;

import fr.soleil.salsa.entity.IDimension;

/**
 * @author Alike
 *
 */
public interface IDimension1D extends IDimension {

    /**
     * Get the range.
     * @return
     */
    public List<IRange1D> getRangesXList();

    /**
     * Get trajectories.
     * @return
     */
    public List<ITrajectory1D> getTrajectoriesList();

    /**
     * Set the range.
     * @param rangesX
     */
    public void setRangesXList(List<IRange1D> rangesX);

    /**
     * Set the trajectories.
     * @param trajectories
     */
    public void setTrajectoriesList(List<ITrajectory1D> trajectories);

}

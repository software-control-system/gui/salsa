package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.impl.scan2d.Config2DImpl;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;

/**
 * @author Alike.
 *
 */
public class Config2DModel extends AConfigModel<IDimension2DX, IConfig2D> implements IConfig2D {

    private static final long serialVersionUID = 230928710170406985L;

    public Config2DModel(final IConfig2D config2D) {
        super(config2D);
    }

    public Config2DModel() {
        this(new Config2DImpl());
    }

    @Override
    public IDimension2DY getDimensionY() {
        return baseBean.getDimensionY();
    }

    @Override
    public void setDimensionY(final IDimension2DY dimensionY) {
        IDimension2DY oldValue = baseBean.getDimensionY();
        baseBean.setDimensionY(dimensionY);
        this.firePropertyChange("dimensionY", oldValue, dimensionY);
    }
}

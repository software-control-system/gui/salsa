package fr.soleil.salsa.entity.scank;

import fr.soleil.salsa.entity.IRange;

public interface IRangeK extends IRange{

    /**
     * Get trajectory.
     * @return
     */
    public ITrajectoryK getTrajectory();

    /**
     * Set the trajectory.
     * @param trajectory
     */
    public void setTrajectory(ITrajectoryK trajectory);
}

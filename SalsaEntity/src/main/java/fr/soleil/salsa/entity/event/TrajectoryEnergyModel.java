package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scanenergy.TrajectoryEnergyImpl;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanenergy.ITrajectoryEnergy;

public class TrajectoryEnergyModel extends AEventHandlingModelDecorator<ITrajectoryEnergy>
        implements ITrajectoryEnergy {

    private static final long serialVersionUID = -8142040904238311229L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param trajectory1D the decorated base bean.
     */
    public TrajectoryEnergyModel(ITrajectoryEnergy trajectoryEnergy) {
        super(trajectoryEnergy);
    }

    /**
     * Default constructor, that creates a new instance of Trajectory1DImpl and wraps it.
     */
    public TrajectoryEnergyModel() {
        this(new TrajectoryEnergyImpl());
    }

    @Override
    public IActuator getActuator() {
        return this.baseBean.getActuator();
    }

    @Override
    public IRangeEnergy getRange() {
        return this.baseBean.getRange();
    }

    @Override
    public void setActuator(IActuator actuator) {
        IActuator oldValue = this.baseBean.getActuator();
        this.baseBean.setActuator(actuator);
        this.firePropertyChange("actuator", oldValue, actuator);
    }

    @Override
    public void setRange(IRangeEnergy range) {
        IRangeEnergy oldValue = this.baseBean.getRange();
        this.baseBean.setRange(range);
        this.firePropertyChange("range", oldValue, range);
    }

    @Override
    public double getBeginPosition() {
        return this.baseBean.getBeginPosition();
    }

    @Override
    public double getDelta() {
        return this.baseBean.getDelta();
    }

    @Override
    public double getEndPosition() {
        return this.baseBean.getEndPosition();
    }

    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    @Override
    public Boolean getRelative() {
        return this.baseBean.getRelative();
    }

    @Override
    public double getSpeed() {
        return this.baseBean.getSpeed();
    }

    @Override
    public void setBeginPosition(double beginPosition) {
        double oldValue = this.baseBean.getBeginPosition();
        this.baseBean.setBeginPosition(beginPosition);
        this.firePropertyChange("beginPosition", oldValue, beginPosition);

    }

    @Override
    public void setDelta(double delta) {
        double oldValue = this.baseBean.getDelta();
        this.baseBean.setDelta(delta);
        this.firePropertyChange("delta", oldValue, delta);
    }

    @Override
    public void setEndPosition(double endPosition) {
        double oldValue = this.baseBean.getEndPosition();
        this.baseBean.setEndPosition(endPosition);
        this.firePropertyChange("endPosition", oldValue, endPosition);

    }

    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    @Override
    public void setRelative(Boolean relative) {
        Boolean oldValue = this.baseBean.getRelative();
        this.baseBean.setRelative(relative);
        this.firePropertyChange("relative", oldValue, relative);
    }

    @Override
    public void setSpeed(double speed) {
        double oldValue = this.baseBean.getSpeed();
        this.baseBean.setSpeed(speed);
        this.firePropertyChange("speed", oldValue, speed);
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setName(String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setTrajectory(double[] values) {
        if (baseBean != null) {
            baseBean.setTrajectory(values);
        }
    }

    @Override
    public double[] getTrajectory() {
        if (baseBean != null) {
            return baseBean.getTrajectory();
        }
        return null;
    }

    @Override
    public void setIRange(IRange range) {
        baseBean.setIRange(range);
    }

    @Override
    public IRange getIRange() {
        return baseBean.getIRange();
    }

    @Override
    public ITrajectory toModel() {
        return this;
    }

    @Override
    public void refreshDelta() {
        baseBean.refreshDelta();
    }

    @Override
    public void refreshStep() {
        baseBean.refreshStep();
    }

    @Override
    public void refreshEndPosition() {
        baseBean.refreshEndPosition();
    }

    @Override
    public void setDeltaConstant(Boolean deltaConstant) {
        baseBean.setDeltaConstant(deltaConstant);

    }

    @Override
    public Boolean isDeltaConstant() {
        return baseBean.isDeltaConstant();
    }

    @Override
    public void setCustomTrajectory(boolean custom) {
        baseBean.setCustomTrajectory(custom);
    }

    @Override
    public boolean isCustomTrajectory() {
        return baseBean.isCustomTrajectory();
    }
}

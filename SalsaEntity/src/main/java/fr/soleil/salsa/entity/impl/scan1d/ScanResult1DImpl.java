package fr.soleil.salsa.entity.impl.scan1d;

import fr.soleil.salsa.entity.IScanResult;
import fr.soleil.salsa.entity.impl.ScanResultImpl;

/**
 * Implementation of {@link IScanResult} for the scanResult entity. The result of a scan.
 */
public class ScanResult1DImpl extends ScanResultImpl implements IScanResult {

    private static final long serialVersionUID = 2891739910280664864L;

    /**
     * Constructor.
     */
    public ScanResult1DImpl() {
        super();
    }
}

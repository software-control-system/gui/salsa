package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.impl.scan1d.Dimension1DImpl;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan1d.ITrajectory1D;

/**
 * Event handling decorator of {@link IDimension1D} for the dimension1D entity.
 * 
 */
public class Dimension1DModel extends DimensionModel<IDimension1D> implements IDimension1D {

    private static final long serialVersionUID = 7846508209341793769L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param dimension1D the decorated base bean.
     */
    public Dimension1DModel(IDimension1D dimension1D) {
        super(dimension1D);
    }

    /**
     * Default constructor, that creates a new instance of Dimension1DImpl and wraps it.
     */
    public Dimension1DModel() {
        this(new Dimension1DImpl());
    }

    @Override
    public List<IRange1D> getRangesXList() {
        return baseBean.getRangesXList();
    }

    /**
     * 
     */
    public void setRangesXList(List<IRange1D> rangesX) {
        List<IRange1D> oldValue = this.baseBean.getRangesXList();
        this.baseBean.setRangesXList(rangesX);
        this.firePropertyChange("rangesX", oldValue, rangesX);
    }

    /**
     * 
     */
    public List<ITrajectory1D> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    /**
     * 
     */
    public void setTrajectoriesList(List<ITrajectory1D> trajectories) {
        List<ITrajectory1D> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectories);
        this.firePropertyChange("trajectories", oldValue, trajectories);
    }

}

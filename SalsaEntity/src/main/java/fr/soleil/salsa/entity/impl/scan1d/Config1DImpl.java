package fr.soleil.salsa.entity.impl.scan1d;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IObjectImpl;
import fr.soleil.salsa.entity.event.Config1DModel;
import fr.soleil.salsa.entity.impl.ConfigImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;

/**
 * Class relative to Config 1d.
 *
 * @author Alike
 *
 */
public class Config1DImpl extends ConfigImpl<IDimension1D> implements IConfig1D {

    private static final long serialVersionUID = -5361128113851783031L;

    /**
     * Constructor.
     */
    public Config1DImpl() {
        super();
        setType(ScanType.SCAN_1D);
    }

    @Override
    protected IConfig<IDimension1D> initModel() {
        Config1DModel config1DModel = new Config1DModel(this);
        IDimension1D dimension1D = getDimensionX();
        if (dimension1D != null && dimension1D instanceof IObjectImpl<?>) {
            IDimension1D dimensionModel = (IDimension1D) ((IObjectImpl<?>) dimension1D).toModel();
            List<IActuator> actuatorList = dimension1D.getActuatorsList();
            List<IActuator> actuatorListModel = convertActuatorListToModel(actuatorList);
            dimensionModel.setActuatorsList(actuatorListModel);
            config1DModel.setDimensionX(dimensionModel);
        }

        return config1DModel;
    }

}

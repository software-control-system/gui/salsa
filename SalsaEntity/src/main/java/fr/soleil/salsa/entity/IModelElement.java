package fr.soleil.salsa.entity;

/**
 * Interface for all Model v3 element. 
 * @author Alike
 */
public interface IModelElement {

    /**
     * Get the Id.
     * @return
     */
    public Integer getId();

    /**
     * Set the Id.
     * @param id
     */
    public void setId(Integer id);

}

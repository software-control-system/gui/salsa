package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.impl.PostScanBehaviourImpl;

public class PostScanBehaviourModel extends AEventHandlingModelDecorator<IPostScanBehaviour>
        implements IPostScanBehaviour {

    private static final long serialVersionUID = 6928030618323561123L;

    /**
     * @param baseBean
     */
    public PostScanBehaviourModel(IPostScanBehaviour baseBean) {
        super(baseBean);
    }

    /**
     * 
     */
    public PostScanBehaviourModel() {
        super(new PostScanBehaviourImpl());
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#getActuator()
     */
    @Override
    public int getActuator() {
        return baseBean.getActuator();
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#getBehaviour()
     */
    @Override
    public Behaviour getBehaviour() {
        return baseBean.getBehaviour();
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#getSensor()
     */
    @Override
    public int getSensor() {
        // TODO Auto-generated method stub
        return baseBean.getSensor();
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#setActuator(int)
     */
    @Override
    public void setActuator(int actuator) {
        int oldValue = baseBean.getActuator();
        baseBean.setActuator(actuator);
        this.firePropertyChange("actuator", oldValue, actuator);
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#setBehaviour(fr.soleil.salsa.entity.impl.Behaviour)
     */
    @Override
    public void setBehaviour(Behaviour behaviour) {
        Behaviour oldValue = baseBean.getBehaviour();
        baseBean.setBehaviour(behaviour);
        this.firePropertyChange("behaviour", oldValue, behaviour);

    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IPostScanBehaviour#setSensor(int)
     */
    @Override
    public void setSensor(int sensor) {
        int oldValue = baseBean.getSensor();
        baseBean.setSensor(sensor);
        this.firePropertyChange("sensor", oldValue, sensor);
    }

    @Override
    public IPostScanBehaviour toModel() {
        return this;
    }

}

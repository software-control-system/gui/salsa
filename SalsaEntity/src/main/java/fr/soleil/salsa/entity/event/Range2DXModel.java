package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.scan2d.Range2DXImpl;
import fr.soleil.salsa.entity.scan2D.IRange2DX;

/**
 * @author Administrateur
 * 
 */
public class Range2DXModel extends AEventHandlingModelDecorator<IRange2DX> implements IRange2DX {

    private static final long serialVersionUID = 4872635812328692140L;

    public Range2DXModel(IRange2DX range2DX) {
        super(range2DX);
    }

    public Range2DXModel() {
        this(new Range2DXImpl());
    }

    /**
     * Gets the unique identifier.
     * 
     * @see IRange#getId()
     * @return id
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @see IRange#setId(Integer)
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the number of steps between the starting and ending position.
     * 
     * @see IRange#getStepsNumber()
     * @return stepsNumber
     */
    @Override
    public Integer getStepsNumber() {
        return this.baseBean.getStepsNumber();
    }

    /**
     * Sets the number of steps between the starting and ending position.
     * 
     * @see IRange#setStepsNumber(Integer)
     * @param Integer
     */
    @Override
    public void setStepsNumber(Integer stepsNumber) {
        Integer oldValue = this.baseBean.getStepsNumber();
        this.baseBean.setStepsNumber(stepsNumber);
        this.firePropertyChange("stepsNumber", oldValue, stepsNumber);
    }

    /**
     * Gets the trajectories of the actuators on this range.
     * 
     * @see IRange#getTrajectoriesList()
     * @return trajectoriesList
     */
    @Override
    public List<ITrajectory> getTrajectoriesList() {
        return baseBean.getTrajectoriesList();
    }

    /**
     * Sets the trajectories of the actuators on this range.
     * 
     * @see IRange#setTrajectoriesList(List<ITrajectory>)
     * @param List<ITrajectory>
     */
    @Override
    public void setTrajectoriesList(List<ITrajectory> trajectoriesList) {
        List<ITrajectory> oldValue = this.baseBean.getTrajectoriesList();
        this.baseBean.setTrajectoriesList(trajectoriesList);
        this.firePropertyChange("trajectoriesList", oldValue, trajectoriesList);
    }

    /**
     * Gets the dimension this range is a part of.
     * 
     * @see IRange#getDimension()
     * @return dimension
     */
    @Override
    public IDimension getDimension() {
        return baseBean.getDimension();
    }

    /**
     * Sets the dimension this range is a part of.
     * 
     * @see IRange#setDimension(IDimension)
     * @param IDimension
     */
    @Override
    public void setDimension(IDimension dimension) {
        IDimension oldValue = this.baseBean.getDimension();
        this.baseBean.setDimension(dimension);
        this.firePropertyChange("dimension", oldValue, dimension);
    }

    /**
     * Get the integration time.
     * 
     * @return
     */
    @Override
    public double[] getIntegrationTime() {
        return baseBean.getIntegrationTime();
    }

    /**
     * Set the integration time.
     * 
     * @param integrationTime
     */
    @Override
    public void setIntegrationTime(double[] integrationTime) {
        double[] oldValue = baseBean.getIntegrationTime();
        baseBean.setIntegrationTime(integrationTime);
        this.firePropertyChange("integrationTime", oldValue, integrationTime);
    }

    @Override
    public ITrajectory createTrajectory(IActuator actuator) {
        return this.baseBean.createTrajectory(actuator);
    }

    @Override
    public void setStepNumberNoRefresh(Integer stepsNumber) {
        this.baseBean.setStepNumberNoRefresh(stepsNumber);
    }

}

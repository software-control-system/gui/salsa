package fr.soleil.salsa.entity.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.event.HookModel;

/**
 * Class relative to Hooks.
 * 
 * @author Alike
 * 
 */
public class HookImpl implements IHook {

    private static final long serialVersionUID = -7588074248247450071L;

    /**
     * The unique identifier.
     */
    private Integer id = 0;

    /**
     * Stage.
     */
    private Stage stage = Stage.POST_ACTUATOR_MOVE;

    /**
     * List of commands.
     */
    private List<IHookCommand> commandsList = new ArrayList<IHookCommand>();

    private IHook model = null;

    /**
     * Checks for equality. This equals implementation only checks if the obj parameter is an
     * instance of the base interface : another implementation of the interface is considered equal
     * to this one if it represents the same entity.
     * 
     * @see Object#equals(Object)
     * @param obj the object with which to compare.
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            equals = true;
        }
        else if (obj == null) {
            equals = false;
        }
        else if (!(obj instanceof IHook)) {
            equals = false;
        }
        else if (this.id == null) {
            // Approximation. The equals method of a bean that has not received
            // an identifier yet should not be called.
            equals = false;
        }
        else {
            equals = this.id.equals(((IHook) obj).getId());
        }
        return equals;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IHook#getCommandsList()
     */
    public List<IHookCommand> getCommandsList() {
        return commandsList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.alike.impl.ModelElement#getId()
     */
    public Integer getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IHook#getStage()
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IHook#setCommandsList(java.util.List)
     */
    public void setCommandsList(List<IHookCommand> commands) {
        commandsList.clear();
        commandsList.addAll(commands);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.alike.impl.ModelElement#setId(java.lang.Integer)
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /* (non-Javadoc)
     * @see fr.soleil.salsa.entity.IHook#setStage(fr.soleil.salsa.entity.impl.Stage)
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public IHook toModel() {
        if (model == null) {
            model = new HookModel(this);

            List<IHookCommand> commandListImpl = getCommandsList();
            if (commandListImpl != null) {
                List<IHookCommand> commandListModel = new ArrayList<IHookCommand>();
                for (IHookCommand hookCommandImpl : commandListModel) {
                    commandListModel.add(hookCommandImpl.toModel());
                }
                model.setCommandsList(commandListModel);
            }
        }
        return model;
    }

    @Override
    public IHook toImpl() {
        return this;
    }

}

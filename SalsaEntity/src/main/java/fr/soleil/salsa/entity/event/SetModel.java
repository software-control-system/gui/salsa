package fr.soleil.salsa.entity.event;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;

/**
 * Model entity are supposed to send an event when one of their property is changed. If the property
 * is a collection, we want the event to be fired if the collection is modified, even if it is not
 * entirely replaced. Therefore, we need collections that send events when methods like add are
 * called. This is such an implementation of java.util.Set. It works by wrapping a set.
 * 
 * @author Administrateur
 * 
 * @param <E>
 * @param <T>
 */
public class SetModel<E, T extends IEventSource<?>> implements Set<E> {

    /**
     * The decorated collection.
     */
    protected Set<E> baseCollection;

    /**
     * The actual entity that this collection is a property of.
     */
    protected T entity;

    /**
     * The property name.
     */
    protected String propertyName;

    /**
     * Constructeur
     * 
     * @param baseSet the decorated set.
     * @param entity the actual entity that this collection is a property of.
     * @param propertyName the property name.
     */
    public SetModel(Set<E> baseSet, T entity, String propertyName) {
        this.baseCollection = baseSet;
        this.entity = entity;
        this.propertyName = propertyName;
    }

    /**
     * Returns the base collection.
     */
    public Set<E> getBaseBean() {
        return baseCollection;
    }

    /**
     * Sends the EntityPropertyChangedEvent. Reminder : the EntityPropertyChangedEvent must be
     * called AFTER the property has been changed in the bean.
     * 
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    @SuppressWarnings("unchecked")
    protected void firePropertyChange() {
        EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                this.entity, this.propertyName, null, this);
        SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent,
                ((IEventSource<EntityPropertyChangedEvent<T>>) entity).getListeners());
    }

    /**
     * @param o
     * @return
     * @see java.util.Set#add(java.lang.Object)
     */
    public boolean add(E o) {
        boolean result = baseCollection.add(o);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.Set#addAll(java.util.Collection)
     */
    public boolean addAll(Collection<? extends E> c) {
        boolean result = baseCollection.addAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * 
     * @see java.util.Set#clear()
     */
    public void clear() {
        baseCollection.clear();
        this.firePropertyChange();
    }

    /**
     * @param o
     * @return
     * @see java.util.Set#contains(java.lang.Object)
     */
    public boolean contains(Object o) {
        return baseCollection.contains(o);
    }

    /**
     * @param c
     * @return
     * @see java.util.Set#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c) {
        return baseCollection.containsAll(c);
    }

    /**
     * @param o
     * @return
     * @see java.util.Set#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        return baseCollection.equals(o);
    }

    /**
     * @return
     * @see java.util.Set#hashCode()
     */
    public int hashCode() {
        return baseCollection.hashCode();
    }

    /**
     * @return
     * @see java.util.Set#isEmpty()
     */
    public boolean isEmpty() {
        return baseCollection.isEmpty();
    }

    /**
     * @return
     * @see java.util.Set#iterator()
     */
    public Iterator<E> iterator() {
        // Since an iterator can change the set, we wrap it into an IteratorProperty to the same
        // property.
        return new IteratorModel<E, T>(baseCollection.iterator(), this.entity, this.propertyName);
    }

    /**
     * @param o
     * @return
     * @see java.util.Set#remove(java.lang.Object)
     */
    public boolean remove(Object o) {
        boolean result = baseCollection.remove(o);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.Set#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection<?> c) {
        boolean result = baseCollection.removeAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.Set#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection<?> c) {
        boolean result = baseCollection.retainAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @return
     * @see java.util.Set#size()
     */
    public int size() {
        return baseCollection.size();
    }

    /**
     * @return
     * @see java.util.Set#toArray()
     */
    public Object[] toArray() {
        Object[] array = baseCollection.toArray();
        return array;
    }

    /**
     * @param <T2>
     * @param a
     * @return
     * @see java.util.Set#toArray(T2[])
     */
    public <T2> T2[] toArray(T2[] a) {
        T2[] array = baseCollection.toArray(a);
        return array;
    }
}
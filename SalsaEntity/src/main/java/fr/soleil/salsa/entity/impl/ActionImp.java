package fr.soleil.salsa.entity.impl;

import fr.soleil.salsa.entity.IModelElement;

/**
 * Class relative to Action object.
 * 
 * @author Alike
 * 
 */
public class ActionImp implements IModelElement {

    /**
     * Id.
     */
    private Integer id;

    /**
     * Default Constructor
     * 
     */
    public ActionImp() {
        super();
    }

    /* (non-Javadoc)
     * @see com.alike.impl.ModelElement#getId()
     */
    public Integer getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see com.alike.impl.ModelElement#setId(java.lang.Integer)
     */
    public void setId(Integer id) {
        this.id = id;
    }

}

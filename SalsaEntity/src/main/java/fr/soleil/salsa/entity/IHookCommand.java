package fr.soleil.salsa.entity;

import java.io.Serializable;

public interface IHookCommand extends Serializable {

    /**
     * Gets the command.
     * 
     * @return
     */
    String getCommand();

    /**
     * Sets the command/
     * 
     * @param command
     */
    void setCommand(String command);

    boolean isEnable();

    void setEnable(boolean enable);

    public IHookCommand toModel();

    public IHookCommand toImpl();

}

package fr.soleil.salsa.entity.event;

import java.util.Collection;
import java.util.Iterator;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.wrapping.IBeanWrapper;

/**
 * Model entity are supposed to send an event when one of their property is changed. If the property
 * is a collection, we want the event to be fired if the collection is modified, even if it is not
 * entirely replaced. Therefore, we need collections that send events when methods like add are
 * called. This is such an implementation of java.util.Collection. It works by wrapping a
 * collection.
 * 
 * @author Administrateur
 * 
 * @param <E>
 * @param <T>
 */
public class CollectionModel<E, T extends IEventSource<?>> implements Collection<E>,
        IBeanWrapper<Collection<E>> {

    /**
     * The decorated collection.
     */
    protected Collection<E> baseCollection;

    /**
     * The actual entity that this collection is a property of.
     */
    protected T entity;

    /**
     * The property name.
     */
    protected String propertyName;

    /**
     * Constructeur
     * 
     * @param baseCollection the decorated Collection.
     * @param entity the actual entity that this collection is a property of.
     * @param propertyName the property name.
     */
    public CollectionModel(Collection<E> baseCollection, T entity, String propertyName) {
        this.baseCollection = baseCollection;
        this.entity = entity;
        this.propertyName = propertyName;
    }

    /**
     * Returns the base collection.
     */
    public Collection<E> getBaseBean() {
        return baseCollection;
    }

    /**
     * Sends the EntityPropertyChangedEvent. Reminder : the EntityPropertyChangedEvent must be
     * called AFTER the property has been changed in the bean.
     * 
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    @SuppressWarnings("unchecked")
    protected void firePropertyChange() {
        EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                this.entity, this.propertyName, null, this);
        SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent,
                ((IEventSource<EntityPropertyChangedEvent<T>>) entity).getListeners());
    }

    /**
     * @param o
     * @return
     * @see java.util.Collection#add(java.lang.Object)
     */
    public boolean add(E o) {
        boolean result = baseCollection.add(o);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.Collection#addAll(java.util.Collection)
     */
    public boolean addAll(Collection<? extends E> c) {
        boolean result = baseCollection.addAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * 
     * @see java.util.Collection#clear()
     */
    public void clear() {
        baseCollection.clear();
        this.firePropertyChange();
    }

    /**
     * @param o
     * @return
     * @see java.util.Collection#contains(java.lang.Object)
     */
    public boolean contains(Object o) {
        return baseCollection.contains(o);
    }

    /**
     * @param c
     * @return
     * @see java.util.Collection#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c) {
        return baseCollection.containsAll(c);
    }

    /**
     * @param o
     * @return
     * @see java.util.Collection#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        return baseCollection.equals(o);
    }

    /**
     * @return
     * @see java.util.Collection#hashCode()
     */
    public int hashCode() {
        return baseCollection.hashCode();
    }

    /**
     * @return
     * @see java.util.Collection#isEmpty()
     */
    public boolean isEmpty() {
        return baseCollection.isEmpty();
    }

    /**
     * @return
     * @see java.util.Collection#iterator()
     */
    public Iterator<E> iterator() {
        // Since an iterator can change the list, we wrap it into an IteratorProperty to the same
        // property.
        return new IteratorModel<E, T>(baseCollection.iterator(), this.entity, this.propertyName);
    }

    /**
     * @param o
     * @return
     * @see java.util.Collection#remove(java.lang.Object)
     */
    public boolean remove(Object o) {
        boolean result = baseCollection.remove(o);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.Collection#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection<?> c) {
        boolean result = baseCollection.removeAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @param c
     * @return
     * @see java.util.Collection#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection<?> c) {
        boolean result = baseCollection.retainAll(c);
        this.firePropertyChange();
        return result;
    }

    /**
     * @return
     * @see java.util.Collection#size()
     */
    public int size() {
        return baseCollection.size();
    }

    /**
     * @param <T2>
     * @param a
     * @return
     * @see java.util.Collection#toArray(T2[])
     */
    public Object[] toArray() {
        Object[] array = baseCollection.toArray();
        return array;
    }

    /**
     * @param <T2>
     * @param a
     * @return
     * @see java.util.List#toArray(T2[])
     */
    public <T2> T2[] toArray(T2[] a) {
        T2[] array = baseCollection.toArray(a);
        return array;
    }
}
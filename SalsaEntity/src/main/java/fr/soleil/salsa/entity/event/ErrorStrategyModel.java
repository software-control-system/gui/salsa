package fr.soleil.salsa.entity.event;

import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.impl.ErrorStrategyImpl;
import fr.soleil.salsa.entity.impl.ErrorStrategyType;

/***
 * Event handling decorator of {@link IErrorStrategy} for the errorStrategy entity. A errorStrategy
 * is a detector that measures the scanned values.
 * 
 * @author Alike
 * 
 */
public class ErrorStrategyModel extends AEventHandlingModelDecorator<IErrorStrategy> implements
        IErrorStrategy {

    private static final long serialVersionUID = 4039287061348173749L;

    /**
     * Constructor.
     */
    public ErrorStrategyModel() {
        super(new ErrorStrategyImpl());
    }

    /**
     * Constructor.
     * 
     * @param baseBean
     */
    public ErrorStrategyModel(IErrorStrategy baseBean) {
        super(baseBean);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategy#getActuatorsErrorStrategy()
     */
    @Override
    public IErrorStrategyItem getActuatorsErrorStrategy() {
        return baseBean.getActuatorsErrorStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategy#getContextValidationDevice()
     */
    @Override
    public String getContextValidationDevice() {
        return baseBean.getContextValidationDevice();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategy#getContextValidationStrategy()
     */
    @Override
    public ErrorStrategyType getContextValidationStrategy() {
        return baseBean.getContextValidationStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategy#getHooksErrorStrategy()
     */
    @Override
    public IErrorStrategyItem getHooksErrorStrategy() {
        return baseBean.getHooksErrorStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategy#getSensorsErrorStrategy()
     */
    @Override
    public IErrorStrategyItem getSensorsErrorStrategy() {
        return baseBean.getSensorsErrorStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.salsa.entity.IErrorStrategy#getTimebasesErrorStrategy()
     */
    @Override
    public IErrorStrategyItem getTimebasesErrorStrategy() {
        return baseBean.getTimebasesErrorStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategy#setActuatorsErrorStrategy(fr.soleil
     * .salsa.entity.IErrorStrategyItem)
     */
    @Override
    public void setActuatorsErrorStrategy(IErrorStrategyItem actuatorsErrorStrategy) {
        IErrorStrategyItem oldValue = baseBean.getActuatorsErrorStrategy();
        baseBean.setActuatorsErrorStrategy(actuatorsErrorStrategy);
        firePropertyChange("actuatorsErrorStrategy", oldValue, actuatorsErrorStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategy#setContextValidationDevice(java
     * .lang.String)
     */
    @Override
    public void setContextValidationDevice(String contextValidationDevice) {
        String oldValue = baseBean.getContextValidationDevice();
        baseBean.setContextValidationDevice(contextValidationDevice);
        firePropertyChange("contextValidationDevice", oldValue, contextValidationDevice);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategy#setContextValidationStrategy(fr
     * .soleil.salsa.entity.impl.ErrorStrategyType)
     */
    @Override
    public void setContextValidationStrategy(ErrorStrategyType contextValidationStrategy) {
        ErrorStrategyType oldValue = baseBean.getContextValidationStrategy();
        baseBean.setContextValidationStrategy(contextValidationStrategy);
        firePropertyChange("contextValidationStrategy", oldValue, contextValidationStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategy#setHooksErrorStrategy(fr.soleil
     * .salsa.entity.IErrorStrategyItem)
     */
    @Override
    public void setHooksErrorStrategy(IErrorStrategyItem hooksErrorStrategy) {
        IErrorStrategyItem oldValue = baseBean.getHooksErrorStrategy();
        baseBean.setHooksErrorStrategy(hooksErrorStrategy);
        firePropertyChange("hooksErrorStrategy", oldValue, hooksErrorStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategy#setSensorsErrorStrategy(fr.soleil
     * .salsa.entity.IErrorStrategyItem)
     */
    @Override
    public void setSensorsErrorStrategy(IErrorStrategyItem sensorsErrorStrategy) {
        IErrorStrategyItem oldValue = baseBean.getSensorsErrorStrategy();
        baseBean.setSensorsErrorStrategy(sensorsErrorStrategy);
        firePropertyChange("sensorsErrorStrategy", oldValue, sensorsErrorStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.salsa.entity.IErrorStrategy#setTimebasesErrorStrategy(fr.soleil
     * .salsa.entity.IErrorStrategyItem)
     */
    @Override
    public void setTimebasesErrorStrategy(IErrorStrategyItem timebasesErrorStrategy) {
        IErrorStrategyItem oldValue = baseBean.getTimebasesErrorStrategy();
        baseBean.setTimebasesErrorStrategy(timebasesErrorStrategy);
        firePropertyChange("timebasesErrorStrategy", oldValue, timebasesErrorStrategy);
    }

    @Override
    public IErrorStrategy toModel() {
        return this;
    }

}

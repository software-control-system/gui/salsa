package fr.soleil.salsa.entity;


/**
 * Interface for the timeBase entity. A timeBase is a detector that measures the
 * scanned values.
 * 
 * @author Administrateur
 */
public interface ITimebase extends IDevice {

}

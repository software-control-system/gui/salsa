package fr.soleil.salsa.entity.event;

import java.util.List;

import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IRange;

/**
 * Event handling decorator of {@link IDimension} for the dimension entity. A dimension describes an
 * actual dimension of a scan as a set of movements to be performed by the actuators during the
 * scan. It is divided in ranges, which are parts of the scan where the movement is linear. The
 * ranges themselves are in turn divided in trajectories, which details the movement of an actuator
 * on this range.
 * 
 * @see fr.soleil.salsa.entity.ITrajectory
 * @see fr.soleil.salsa.entity.IRange
 * @author Administrateur
 */
public abstract class DimensionModel<T extends IDimension> extends AEventHandlingModelDecorator<T>
implements IDimension {

    private static final long serialVersionUID = 57175041731218158L;

    /**
     * Constructor for a wrapper that decorates the bean passed as first parameter.
     * 
     * @param dimension the decorated base bean.
     */
    public DimensionModel(T dimension) {
        super(dimension);
    }

    /**
     * Gets the unique identifier.
     * 
     * @see IDimension#getId()
     * @return id
     */
    @Override
    public Integer getId() {
        return this.baseBean.getId();
    }

    /**
     * Sets the unique identifier.
     * 
     * @see IDimension#setId(Integer)
     * @param Integer
     */
    @Override
    public void setId(Integer id) {
        Integer oldValue = this.baseBean.getId();
        this.baseBean.setId(id);
        this.firePropertyChange("id", oldValue, id);
    }

    /**
     * Gets the list of actuators.
     * 
     * @see IRange#getActuatorsList()
     * @return actuatorsList
     */
    public List<IActuator> getActuatorsList() {
        return baseBean.getActuatorsList();
    }

    /**
     * Sets the list of actuators.
     * 
     * @see IRange#setActuatorsList(List<IActuator>)
     * @param List<IActuator>
     */
    public void setActuatorsList(List<IActuator> actuatorsList) {
        List<IActuator> oldValue = this.baseBean.getActuatorsList();
        this.baseBean.setActuatorsList(actuatorsList);
        this.firePropertyChange("actuatorsList", oldValue, actuatorsList);
    }

    @Override
    public boolean isEqualsTo(IDimension dimension) {
        return baseBean.isEqualsTo(dimension);
    }

    @Override
    public List<? extends IRange> getRangeList() {
        return baseBean.getRangeList();
    }

    @Override
    public void setRangeList(List<? extends IRange> rangeList) {
        baseBean.setRangeList(rangeList);
    }

    @Override
    public void addRange(IRange range) {
        baseBean.addRange(range);
    }
}

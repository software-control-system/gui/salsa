package fr.soleil.salsa.entity.event;

import java.util.Iterator;

import fr.soleil.salsa.entity.event.queue.SalsaEventQueue;
import fr.soleil.salsa.entity.event.source.IEventSource;

/**
 * An iterator for a ListProperty. Model entity are supposed to send an event when one of their
 * property is changed. If the property is a collection, we want the event to be fired if the
 * collection is modified, even if it is not entirely replaced. Therefore, we need collections that
 * send events when methods like add are called. This is an iterator for such a collection, it works
 * by decorating the base iterator.
 * 
 * @author Administrateur
 * @see ListModel
 */
public class IteratorModel<E, T extends IEventSource<?>> implements Iterator<E> {

    /**
     * The decorated iterator.
     */
    protected Iterator<E> baseIterator;

    /**
     * The actual entity that this collection is a property of.
     */
    protected T entity;

    /**
     * The property name.
     */
    protected String propertyName;

    /**
     * The constructor.
     * 
     * @param baseIterator the decorated iterator.
     * @param entity the actual entity that this collection is a property of.
     * @param propertyName the property name.
     */
    public IteratorModel(Iterator<E> baseIterator, T entity, String propertyName) {
        super();
        this.entity = entity;
        this.propertyName = propertyName;
        this.baseIterator = baseIterator;
    }

    /**
     * @return
     * @see java.util.Iterator#hasNext()
     */
    public boolean hasNext() {
        return baseIterator.hasNext();
    }

    /**
     * @return
     * @see java.util.Iterator#next()
     */
    public E next() {
        return baseIterator.next();
    }

    /**
     * 
     * @see java.util.Iterator#remove()
     */
    public void remove() {
        baseIterator.remove();
        this.firePropertyChange();
    }

    /**
     * Sends the EntityPropertyChangedEvent. Reminder : the EntityPropertyChangedEvent must be
     * called AFTER the property has been changed in the bean.
     * 
     * @param propertyName
     * @param oldValue
     * @param newValue
     */
    @SuppressWarnings("unchecked")
    protected void firePropertyChange() {
        EntityPropertyChangedEvent<T> entityPropertyChangedEvent = new EntityPropertyChangedEvent<T>(
                this.entity, this.propertyName, null, this);
        SalsaEventQueue.staticQueueEvent(entityPropertyChangedEvent,
                ((IEventSource<EntityPropertyChangedEvent<T>>) entity).getListeners());
    }
}

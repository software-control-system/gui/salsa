package fr.soleil.salsa.preferences;

import org.slf4j.Logger;

import com.google.common.eventbus.EventBus;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferencesEvent.Type;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Salsa's device preferences
 * 
 * @author Tarek
 * @author madela
 */
public class DevicePreferences {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(DevicePreferences.class);

    /** The preference file */
    private final String preferenceFile;

    /** The application bus event */
    private final EventBus eventBus;

    /** Scanserver device */
    private String scanServer = ObjectUtils.EMPTY_STRING;

    /** Datafitter device */
    private String dataFitter = ObjectUtils.EMPTY_STRING;

    /** Datarecorder enable or disable */
    private boolean dataRecorder = true;

    /** Publisher device */
    private String publisher = ObjectUtils.EMPTY_STRING;

    /** Recording manager device */
    private String recordingManager = ObjectUtils.EMPTY_STRING;

    /** Recording manager profil to update recording view depending of profil */
    private String recordingManagerProfil = ObjectUtils.EMPTY_STRING;

    /** The User Logfile in bean preferences */
    private String userLogFile;

    /** The max line number in the scan log */
    private Integer maxLineNumber;

    /**
     * Create a new instance
     * 
     * @param preferenceFile the preference file
     * @param eventBus the eventBus
     */
    public DevicePreferences(String preferenceFile, EventBus eventBus) {
        this.preferenceFile = preferenceFile;
        this.eventBus = eventBus;
        eventBus.register(new DevicePreferencesChangeLogger());
    }

    /**
     * Get preference file
     * 
     * @return preference file
     */
    public String getPreferenceFile() {
        return preferenceFile;
    }

    /**
     * Get Scanserver device
     * 
     * @return Scanserver device
     */
    public String getScanServer() {
        return scanServer;
    }

    /**
     * Set Scanserver device
     * 
     * @param scanServer Scanserver device
     */
    public void setScanServer(String scanServer) {
        this.scanServer = scanServer;
        fireChange(Type.SCANSERVER_CHANGED);
    }

    /**
     * Get Datafitter device
     * 
     * @return Datafitter device
     */
    public String getDataFitter() {
        return dataFitter;
    }

    /**
     * Set Datafitter device
     * 
     * @param dataFitter Datafitter device
     */
    public void setDataFitter(String dataFitter) {
        this.dataFitter = dataFitter;
        fireChange(Type.DATAFITTER_CHANGED);
    }

    /**
     * Return true if datarecorder enable
     * 
     * @return true if datarecorder enable
     */
    public boolean isDataRecorder() {
        return dataRecorder;
    }

    /**
     * Set if dataRecorder is enable
     * 
     * @param dataRecorderEnabled enable state of dataRecorder
     */
    public void setDataRecorder(boolean dataRecorderEnabled) {
        this.dataRecorder = dataRecorderEnabled;
        fireChange(Type.DATARECORDER_CHANGED);
    }

    /**
     * Get Publisher device
     * 
     * @return Publisher device
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Set Publisher device
     * 
     * @param publisher Publisher device
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
        fireChange(Type.PUBLISHER_CHANGED);
    }

    /**
     * Get recording manager device
     * 
     * return recording manager device
     */
    public String getRecordingManager() {
        return recordingManager;
    }

    /**
     * Set recording manager device
     * 
     * @param recordingManager recording manager device
     */
    public void setRecordingManager(String recordingManager) {
        this.recordingManager = recordingManager;
        fireChange(Type.RECORDINGMANAGER_CHANGED);
    }

    /**
     * Get recording manager profil, ex: expert
     */
    public String getRecordingManagerProfil() {
        return recordingManagerProfil;
    }

    /**
     * Set recording manager profil, ex: expert
     * 
     * @param recordingManagerProfil the recording manager profil
     */
    public void setRecordingManagerProfil(String recordingManagerProfil) {
        this.recordingManagerProfil = recordingManagerProfil;
        fireChange(Type.RECORDINGMANAGER_PROFIL_CHANGED);
    }

    /**
     * Get User LogFile
     * 
     * @return User LogFile
     */
    public String getUserLogFile() {
        return userLogFile;
    }

    /**
     * Set User LogFile
     * 
     * @param userLogFile User LogFile
     */
    public void setUserLogFile(String userLogFile) {
        this.userLogFile = userLogFile;
        fireChange(Type.USERLOGFILE_CHANGED);

    }

    /**
     * Get max line number in scan log
     * 
     * @return max line number in scan log
     */
    public Integer getMaxLineNumber() {
        return maxLineNumber;
    }

    /**
     * Set max line number in scan log
     * 
     * @param maxLineNumber max line number in scan log
     */
    public void setMaxLineNumber(int maxLineNumber) {
        this.maxLineNumber = maxLineNumber;
        fireChange(Type.MAXLINENUMBER_CHANGED);

    }

    /**
     * Returns true if scan server is defined
     * 
     * @return true if scan server is defined
     */
    public boolean isScanServerDefined() {
        return SalsaUtils.isDefined(getScanServer());
    }

    /**
     * Returns true if scan server is defined
     * 
     * @return true if scan server is defined
     */
    public boolean isRecordingManagerDefined() {
        return SalsaUtils.isDefined(getRecordingManager());
    }

    private void fireChange(Type eventType) {
        fireChange(eventType, false);
    }

    /**
     * Notifies all {@link PerspectiveListener}s for some changes and notifies with another done event after the
     * expected notification if isDone is false
     * 
     * @param type type of event
     * @param perspective perspective of event
     * @param isDone notify with another done event after the expected notification when false
     */
    private void fireChange(Type type, boolean isDone) {
        DevicePreferencesEvent devicePreferencesEvent = new DevicePreferencesEvent(this, type);
        LOGGER.debug("Post {}", devicePreferencesEvent);
        eventBus.post(devicePreferencesEvent);
        if (!isDone) {
            fireChange(Type.DONE, true);
        }
    }

    @Override
    public String toString() {
        return "DevicePreferences [preferenceFile=" + preferenceFile + "]";
    }
}

package fr.soleil.salsa.preferences;

import java.util.EventObject;

/***
 * Event for device preferences
 * 
 * @author Tarek
 * @author madela
 */
public class DevicePreferencesEvent extends EventObject {
    /** Serialization id */
    private static final long serialVersionUID = 4703631168095295609L;

    /** Types of event */
    public enum Type {
        /** Undefined */
        UNDEFINED,
        /** When loading */
        LOADING,
        /** When scanserver preference changed */
        SCANSERVER_CHANGED,
        /** When datafitter preference changed */
        DATAFITTER_CHANGED,
        /** When datarecorder preference changed */
        DATARECORDER_CHANGED,
        /** When publisher preference changed */
        PUBLISHER_CHANGED,
        /** When recording manager preference changed */
        RECORDINGMANAGER_CHANGED,
        /** When recording manager profil preference changed */
        RECORDINGMANAGER_PROFIL_CHANGED,
        /** When user log file preference changed */
        USERLOGFILE_CHANGED,
        /** When max line number preference changed */
        MAXLINENUMBER_CHANGED,
        /** When all listener already noticed about a change */
        DONE
    }

    /** Type of event */
    private Type type;

    /**
     * Constructs a DevicePreferencesEvent object.
     * 
     * @param source the Object that is the source of the event(typically this)
     */
    public DevicePreferencesEvent(DevicePreferences source) {
        super(source);
        this.type = Type.UNDEFINED;
    }

    /**
     * Constructs a DevicePreferencesEvent object.
     * 
     * @param source the Object that is the source of the event(typically this)
     * @param type the type of event
     */
    public DevicePreferencesEvent(DevicePreferences source, Type type) {
        super(source);
        this.type = type;
    }

    @Override
    public DevicePreferences getSource() {
        return (DevicePreferences) super.getSource();
    }

    /**
     * @return Returns the type.
     */
    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DevicePreferencesEvent [type=" + type + ", source=" + source + "]";
    }

}

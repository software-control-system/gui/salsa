package fr.soleil.salsa.preferences;

import org.slf4j.Logger;

import com.google.common.eventbus.Subscribe;

import fr.soleil.salsa.logging.LoggingUtil;

/**
 * Add event listener logger for debugging on device preference change
 * 
 * @author Patrick Madela
 */
public class DevicePreferencesChangeLogger {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(DevicePreferencesChangeLogger.class);

    /**
     * Log device preferences change event
     * 
     * @param event the device preferences change event
     */
    @Subscribe
    public void log(DevicePreferencesEvent event) {
        DevicePreferences devicePreferences = event.getSource();
        switch (event.getType()) {
            case SCANSERVER_CHANGED:
                log("ScanServer", devicePreferences.getScanServer());
                break;
            case DATAFITTER_CHANGED:
                log("DataFitter", devicePreferences.getDataFitter());
                break;
            case PUBLISHER_CHANGED:
                log("Publisher", devicePreferences.getPublisher());
                break;
            case RECORDINGMANAGER_CHANGED:
                log("RecordingManager", devicePreferences.getRecordingManager());
                break;
            case RECORDINGMANAGER_PROFIL_CHANGED:
                log("RecordingManagerProfil", devicePreferences.getRecordingManagerProfil());
                break;
            case USERLOGFILE_CHANGED:
                log("UserLogFile", devicePreferences.getUserLogFile());
                break;
            case MAXLINENUMBER_CHANGED:
                log("MaxLineNumber", devicePreferences.getMaxLineNumber());
                break;
            default:
                break;
        }
    }

    private void log(String name, Object value) {
        LOGGER.debug("Device preferences value of \"{}\" changed to \"{}\"", name, value);
    }
}

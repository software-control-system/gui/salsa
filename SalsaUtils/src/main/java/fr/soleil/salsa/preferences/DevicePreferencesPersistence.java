package fr.soleil.salsa.preferences;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Collator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.EventBusHandler;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Class relative to the persistence of preferences datas. It does not save
 * every properties, only "interesting properties".
 * 
 * @author Alike
 */
public class DevicePreferencesPersistence {
    /** The logger for messages */
    private static final Logger LOGGER = LoggingUtil.getLogger(DevicePreferencesPersistence.class);

    /** Preference file property name */
    public static final String PREFERENCE_FILE_PROPERTY = "SALSA_DEVICE_PREFERENCES";

    /** scanserver device property name */
    public static final String SCANSERVER_PROPERTY = "SCANSERVER";

    private static DevicePreferences devicePreferences = null;
    private static final Object DEVICE_PREFERENCES_LOCK = new Object();

    public static DevicePreferences getSystemPreferences() {
        if (devicePreferences == null) {
            synchronized (DEVICE_PREFERENCES_LOCK) {
                if (devicePreferences == null) {
                    String preferencePath = SystemUtils.getSystemProperty(PREFERENCE_FILE_PROPERTY);
                    if (SalsaUtils.isDefined(preferencePath)) {
                        String scanServer = getDefaultScanServer();
                        devicePreferences = load(preferencePath, scanServer);
                    } else {
                        LOGGER.error("Missing property {}", PREFERENCE_FILE_PROPERTY);
                    }
                }
            }
        }
        return devicePreferences;
    }

    private static String getDefaultScanServer() {
        return SystemUtils.getSystemProperty(SCANSERVER_PROPERTY);
    }

    /**
     * Loads preferences from file system.
     * 
     * @param preferenceFilePath The path to the preference file
     * @param defaultScanServer The scan server to use if the preference file does not exist.
     * @return The corresponding {@link DevicePreferences}
     */
    public static DevicePreferences load(String preferenceFilePath, String defaultScanServer) {
        DevicePreferences devicePreferences = new DevicePreferences(preferenceFilePath, EventBusHandler.getEventBus());
        devicePreferences.setScanServer(defaultScanServer);
        try {
            DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
            fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ObjectUtils.EMPTY_STRING); // Compliant
            fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ObjectUtils.EMPTY_STRING); // compliant
            DocumentBuilder constructeur = fabrique.newDocumentBuilder();
            LOGGER.debug("Load devices preferences from \"{}\"", preferenceFilePath);
            File f = new File(preferenceFilePath);
            Document d = constructeur.parse(f);
            NodeList elements = d.getElementsByTagName("add");
            for (int i = 0; i < elements.getLength(); i++) {
                if (elements.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) elements.item(i);
                String k = e.getAttribute("key");
                String v = e.getTextContent();
                LOGGER.debug("Set devices preferences value of \"{}\" to \"{}\"", k, v);

                if (PreferenceItem.DataFitter.name().equals(k)) {
                    devicePreferences.setDataFitter(v);
                } else if (PreferenceItem.DataRecorder.name().equals(k)) {
                    devicePreferences.setDataRecorder(Boolean.parseBoolean(v));
                } else if (PreferenceItem.ScanServer.name().equals(k)) {
                    devicePreferences.setScanServer(v);
                } else if (PreferenceItem.UserLogFile.name().equals(k)) {
                    devicePreferences.setUserLogFile(v);
                } else if (PreferenceItem.Publisher.name().equals(k)) {
                    devicePreferences.setPublisher(v);
                } else if (PreferenceItem.MaxLineNumber.name().equals(k)) {
                    setMaxLineNumber(devicePreferences, v);
                } else if (PreferenceItem.RecordingManager.name().equals(k)) {
                    devicePreferences.setRecordingManager(v);
                } else if (PreferenceItem.RecordingManagerProfil.name().equals(k)) {
                    devicePreferences.setRecordingManagerProfil(v);
                }
            }
        } catch (ParserConfigurationException | SAXException e) {
            LOGGER.error("Invalid format of device preference file: {}", e.getMessage());
            LOGGER.debug("Invalid format of device preference file", e);
        } catch (FileNotFoundException fnfe) {
            LOGGER.error("Device Preference file not found {}", fnfe.getMessage());
            LOGGER.debug("Device Preference file not found", fnfe);
            LOGGER.info("Device preference file will be created (if allowed)");
            if (!SalsaUtils.isDefined(devicePreferences.getScanServer())) {
                LOGGER.error(
                        "{} property for default scan server device is not defined, please set scan server device in device preference",
                        SCANSERVER_PROPERTY);
            }
            save(devicePreferences);
        } catch (IOException e) {
            LOGGER.error("Unable to load device preference file {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
        return devicePreferences;
    }

    private static void setMaxLineNumber(DevicePreferences devicePreferences, String v) {
        try {
            Integer mln = Integer.parseInt(v);
            devicePreferences.setMaxLineNumber(mln);
        } catch (NumberFormatException exc) {
            LOGGER.debug("Cannot parse Max Line Number in preference file: {}", exc.getMessage());
        }
    }

    /**
     * Save preferences on file system.
     * 
     * @param devicePreferences The preferences to save.
     */
    public static void save(DevicePreferences devicePreferences) {
        if (devicePreferences != null) {
            try {
                DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
                fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ObjectUtils.EMPTY_STRING); // Compliant
                fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ObjectUtils.EMPTY_STRING); // compliant
                DocumentBuilder constructeur = fabrique.newDocumentBuilder();

                Document document = constructeur.newDocument();
                Element root = document.createElement("settings");

                document.appendChild(root);

                Map<String, String> preferenceMap = new TreeMap<>(Collator.getInstance());
                preferenceMap.put(PreferenceItem.DataFitter.name(), devicePreferences.getDataFitter());
                preferenceMap.put(PreferenceItem.DataRecorder.name(),
                        String.valueOf(devicePreferences.isDataRecorder()));
                preferenceMap.put(PreferenceItem.ScanServer.name(), devicePreferences.getScanServer());
                preferenceMap.put(PreferenceItem.UserLogFile.name(), devicePreferences.getUserLogFile());
                preferenceMap.put(PreferenceItem.Publisher.name(), devicePreferences.getPublisher());
                Integer maxLineNumber = devicePreferences.getMaxLineNumber();
                preferenceMap.put(PreferenceItem.MaxLineNumber.name(),
                        maxLineNumber != null ? maxLineNumber.toString() : ObjectUtils.EMPTY_STRING);
                preferenceMap.put(PreferenceItem.RecordingManager.name(), devicePreferences.getRecordingManager());
                preferenceMap.put(PreferenceItem.RecordingManagerProfil.name(),
                        devicePreferences.getRecordingManagerProfil());
                for (Entry<String, String> entry : preferenceMap.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    Element e = document.createElement("add");
                    e.setAttribute("key", key);
                    e.setTextContent(value);
                    root.appendChild(e);
                }
                preferenceMap.clear();

                String str = XMLUtils.xmlToString(document);
                File f = new File(devicePreferences.getPreferenceFile());
                write(str, f);
            } catch (TransformerException | ParserConfigurationException e) {
                LOGGER.error("Unable to save device preference {}", e.getMessage());
                LOGGER.debug("Unable to save device preference", e);
            }
        }
    }

    private static void write(String str, File f) {
        try (FileWriter fw = new FileWriter(f); BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(str);
            bw.flush();
        } catch (IOException e) {
            LOGGER.error("Unable to write device preference file {}", e.getMessage());
            LOGGER.debug("Unable to write device preference file", e);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /** Preference items */
    private enum PreferenceItem {
        ScanServer, DataFitter, DataRecorder, Publisher, RecordingManager, RecordingManagerProfil, UserLogFile,
        MaxLineNumber
    }

}

package fr.soleil.salsa.exception;

/**
 * An exception that can be thrown when there is some problem with preferences
 * 
 * @author girardot
 * 
 */
public class SalsaPreferencesException extends SalsaException {

    private static final long serialVersionUID = 81451750916587561L;

    public SalsaPreferencesException() {
        super();
    }

    public SalsaPreferencesException(String message) {
        super(message);
    }

    public SalsaPreferencesException(Throwable cause) {
        super(cause);
    }

    public SalsaPreferencesException(String message, Throwable cause) {
        super(message, cause);
    }

}

package fr.soleil.salsa.exception;

/**
 * This class is a parent class of all the {@link Exception}s expected to be thrown in Salsa
 * 
 * @author girardot
 */
public class SalsaException extends Exception {

    private static final long serialVersionUID = -1522368116078138872L;

    public SalsaException() {
        super();
    }

    public SalsaException(String message) {
        super(message);
    }

    public SalsaException(Throwable cause) {
        super(cause);
    }

    public SalsaException(String message, Throwable cause) {
        super(message, cause);
    }

}

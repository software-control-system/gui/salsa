package fr.soleil.salsa.exception;

/**
 * A {@link SalsaPreferencesException} used for missing system properties
 * 
 * @author girardot
 */
public class SalsaMissingPropertyException extends SalsaPreferencesException {
    /**
     * 
     */
    private static final long serialVersionUID = -1975202712820318668L;

    private String missingProperty;
    private final static String MISSING_PROPERTY_MESSAGE = "Invalid or missing property: ";

    public SalsaMissingPropertyException(String property) {
        super(MISSING_PROPERTY_MESSAGE + property);
    }

    public String getMissingProperty() {
        return missingProperty;
    }
}

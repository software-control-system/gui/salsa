package fr.soleil.salsa.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.salsa.tool.SalsaUtils;

public class LoggingUtil {

    private static final String HOME = "HOME";
    private static final String SALSA_PERSPECTIVES_DIRECTORY = "SALSA_PERSPECTIVES_DIRECTORY";
    private static final String USER_DIR = "user.dir";
    private static final String USER_HOME = "user.home";

    public static Logger getLogger(Class<?> clazz) {
        // This should not be done in a getLogger method...
        String homeProp = SystemUtils.getSystemProperty(HOME);
        if (!SalsaUtils.isDefined(homeProp)) {
            String salsaPerspectiveDir = SystemUtils.getSystemProperty(SALSA_PERSPECTIVES_DIRECTORY);
            if (salsaPerspectiveDir == null) {
                salsaPerspectiveDir = System.getProperty(USER_DIR);
            }
            if (salsaPerspectiveDir == null) {
                salsaPerspectiveDir = System.getProperty(USER_HOME, ObjectUtils.EMPTY_STRING);
            }
            System.setProperty(HOME, salsaPerspectiveDir);
        }
        return LoggerFactory.getLogger(clazz);
    }
}

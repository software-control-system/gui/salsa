package fr.soleil.salsa.tool;

import com.google.common.eventbus.EventBus;

/**
 * Allows to get a single instance of the application event bus awaiting a dependency injection engine as Guice or
 * Spring
 * 
 * @author madela
 */
public class EventBusHandler {
    /**
     * The single instance of event bus
     */
    private static EventBus eventBus = new EventBus();

    /** Utility classes, which can be extended, should not have public constructors */
    private EventBusHandler() {
        throw new IllegalStateException("Utility class should not have public constructors");
    }

    /**
     * Get the single instance of the event bus
     * 
     * @return the event bus
     */
    public static EventBus getEventBus() {
        return eventBus;
    }
}
package fr.soleil.salsa.tool;

import java.util.Collection;
import java.util.Map;

public class SalsaUtils {

    private SalsaUtils() {
        // hide constructor
    }

    /**
     * Returns true if given value is considered as defined
     * 
     * @param value The value
     * @return true if given value is considered as defined
     */
    public static boolean isDefined(String value) {
        return value != null && !value.trim().isEmpty();
    }

    /**
     * Returns true if given collection is considered as fulfilled
     * 
     * @param collection The collection
     * @return true if given collection is considered as fulfilled
     */
    public static boolean isFulfilled(Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    /**
     * Returns true if given map is considered as fulfilled
     * 
     * @param map The map
     * @return true if given map is considered as fulfilled
     */
    public static boolean isFulfilled(Map<?, ?> map) {
        return map != null && !map.isEmpty();
    }

}

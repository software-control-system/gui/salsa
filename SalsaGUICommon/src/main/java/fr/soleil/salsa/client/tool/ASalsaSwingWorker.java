package fr.soleil.salsa.client.tool;

import javax.swing.SwingUtilities;

/**
 * Allows an application to perform tasks in a background thread, then to display the results in the
 * EDT.<br />
 * <br />
 * javax.swing.SwingWorker is not included by default in Java SE in versions prior to Java SE 6. We
 * chose to provide our own rather than adding one more dependency.<br />
 * <br />
 * The user must override doInBackground : the task to be completed in another thread.<br />
 * The user must override done : called in the EDT when the task is completed.<br />
 * <br />
 * Additionally, the user can have doInBackground return a value. It can later be accessed from
 * done() using the get() method. The generic type parameter is the type of this value.<br />
 * The setProgress(double) method can be called form doInBackground to indicate the completion rate.<br />
 * The getProgress() can then be called from any other thread to get the completion rate.<br />
 * Note that the completion is indicated as a fraction of 1. So the completion percentage would be
 * 100d * getProgress(). <br />
 * Finally, the execute method actually launches the thread.<br />
 * <br />
 * The schedule private method might be rewritten to allow more sophisticated thread management.
 * 
 * @param <T> the background task result if any.
 */
public abstract class ASalsaSwingWorker<T> {

    /**
     * Lock preventing the task result to be read before the task has completed.
     */
    private final Object taskResultLock = new Object();

    /**
     * The background task result.
     */
    private T taskResult;

    /**
     * Lock preventing the completion rate to be read and wrote at the same time.
     */
    private final Object progressLock = new Object();

    /**
     * The completion rate, ranging from 0d to 1d. null indicates that the background task did not
     * compute the completion rate.
     */
    private Double progress = null;

    /**
     * The thread used for the background task.
     */
    private Thread backgroundThread = null;

    /**
     * Lock preventing the background thread to be read and wrote at the same time.
     */
    private final Object backgroundThreadLock = new Object();

    /**
     * The task name, given to the thread for debugging purposes.
     */
    private final String name;

    /**
     * The constructor. The name parameter is given to the thread for debugging purposes.
     * 
     * @param name
     */
    public ASalsaSwingWorker(String name) {
        this.name = name;
    }

    /**
     * The user must override doInBackground : the task to be completed in anopther thread.
     * 
     * @return the task result, which will be made available in done through the get method.
     */
    public abstract T doInBackground();

    /**
     * The user must override done : called in the EDT when the task is completed.
     */
    public abstract void done();

    /**
     * The done method can call this to get the task result.
     * 
     * @return
     */
    public T get() {
        synchronized (this.taskResultLock) {
            return this.taskResult;
        }
    }

    /**
     * The completion rate, ranging from 0d to 1d. null indicates that the background task did not
     * compute the completion rate. To be called from the outside.
     */
    public Double getProgress() {
        synchronized (this.progressLock) {
            return progress;
        }
    }

    /**
     * The completion rate, ranging from 0d to 1d. null indicates that the background task did not
     * compute the completion rate. To be called from doInBackground.
     */
    public void setProgress(Double progress) {
        synchronized (this.progressLock) {
            this.progress = progress;
        }
    }

    /**
     * Returns the thread used for the background task, or null if it has not been created yet. The
     * thread is created when the execute method is called.
     * 
     * @return
     */
    public Thread getBackgroundThread() {
        synchronized (this.backgroundThreadLock) {
            return this.backgroundThread;
        }
    }

    /**
     * Launches the background task in another thread.
     */
    public void execute() {
        // The task to be performed in the background.
        Runnable backgroundTask = new Runnable() {
            public void run() {
                // Synchronization on taskResultLock because the get method must not be completed
                // before the doInBackground method has completed.
                synchronized (taskResultLock) {
                    // Call to doInBackground, the job the user asked for.
                    taskResult = doInBackground();
                }
                // When the background job has completed, the done method must be called from the
                // EDT.
                // So we use invokeLater to make the EDT executes a Runnable that calls done.
                SwingUtilities.invokeLater(new Runnable() {
                    // The EDT has to call the done method.
                    public void run() {
                        done();
                    }
                });
            }
        };
        // Schedule the task. It might be performed immediately or after other job have completed.
        this.schedule(backgroundTask);
    }

    /**
     * Schedules the background task execution. Called from execute, this method takes care of all
     * the details of job execution. This might be modified in the future for more advanced thread
     * management.
     * 
     * @param backgroundTask
     */
    private void schedule(Runnable backgroundTask) {
        synchronized (backgroundThreadLock) {
            this.backgroundThread = new Thread(backgroundTask, this.name);
        }
        backgroundThread.start();
    }
}

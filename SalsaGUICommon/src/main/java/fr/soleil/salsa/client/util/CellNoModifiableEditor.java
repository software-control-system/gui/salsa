package fr.soleil.salsa.client.util;

import java.awt.Color;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;

public class CellNoModifiableEditor extends DefaultCellEditor {

    private static final long serialVersionUID = -3267344941957636182L;
    Color backgroundColor = null;
    Color selectionColor = null;

    public CellNoModifiableEditor(JTextField textField) {
        super(textField);
        textField.setBackground(backgroundColor);
        textField.setEditable(false);
        textField.setOpaque(true);
    }

}

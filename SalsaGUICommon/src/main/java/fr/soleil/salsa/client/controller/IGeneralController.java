package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.GeneralView;

public interface IGeneralController extends IController<GeneralView> {

    /**
     * The actuator delay has been changed.
     * 
     * @param actuatorDelay
     */
    public void notifyActuatorDelay(String actuatorDelay);

    /**
     * The actuator delay has been changed.
     * 
     * @param actuatorDelay
     */
    public void notifyTimebaseDelay(String timebaseDelay);

    /**
     * The dataRecorder configuration.
     * 
     * @param dataRecorder
     */
    public void notifyDataRecorderConfig(String config);

    /**
     * The selected actuator has been changed.
     * 
     * @param index
     */
    public void notifyActuatorSelected(int index);

    /**
     * the enable-actuator-speed has been changed.
     * 
     * @param enableActuatorSpeed
     */
    public void notifyEnableActuatorSpeed(boolean enableActuatorSpeed);

    /**
     * the on-the-fly has been changed.
     * 
     * @param onTheFly
     */
    public void notifyOntheFly(boolean onTheFly);

    /**
     * The post scan behaviour has been changed.
     * 
     * @param code
     */
    public void notifyPostScanBehaviour(String code);

    /**
     * The scan number has been changed.
     * 
     * @param sScanNumber
     */
    public void notifyScanNumberChanged(String sScanNumber);

    /**
     * The selected sensor has been changed.
     * 
     * @param index
     */
    public void notifySensorSelected(int index);

    /**
     * The zig zag has been changed.
     * 
     * @param zigzag
     */
    public void notifyZigZag(boolean zigzag);

    public String[] getConfigList();

}

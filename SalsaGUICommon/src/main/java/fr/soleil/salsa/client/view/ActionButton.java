package fr.soleil.salsa.client.view;

import javax.swing.Icon;
import javax.swing.JButton;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.service.ILogService;

public class ActionButton extends JButton {

    private static final long serialVersionUID = 2322592899401815809L;

    public static enum ACTION_TYPE {
        START, STOP, PAUSE, RESUME, NONE
    }

    public static final Icon START_ICON = Icons.getIcon("salsa.scanserver.start");
    public static final Icon STOP_ICON = Icons.getIcon("salsa.scanserver.stop");
    public static final Icon PAUSE_ICON = Icons.getIcon("salsa.scanserver.pause");
    public static final Icon RESUME_ICON = Icons.getIcon("salsa.scanserver.resume");

    private ACTION_TYPE actionType = null;

    public ActionButton(ACTION_TYPE action) {
        setActionType(action);
    }

    public ACTION_TYPE getActionType() {
        return actionType;
    }

    public void setActionType(ACTION_TYPE action) {
        this.actionType = action;
        // Must be done in EDT (SCAN-976, PROBLEM-2582)
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            switch (actionType) {
                case START:
                    setIcon(START_ICON);
                    break;
                case STOP:
                    setIcon(STOP_ICON);
                    break;
                case PAUSE:
                    setIcon(PAUSE_ICON);
                    break;
                case RESUME:
                    setIcon(RESUME_ICON);
                    break;
                default:
                    break;
            }
        });
    }

    public void setAction(String action) {
        if (action != null) {
            if (action.equalsIgnoreCase(ILogService.START_ACTION)) {
                setActionType(ACTION_TYPE.START);
            }
            if (action.equalsIgnoreCase(ILogService.STOP_ACTION)) {
                setActionType(ACTION_TYPE.STOP);
            }
            if (action.equalsIgnoreCase(ILogService.PAUSE_ACTION)) {
                setActionType(ACTION_TYPE.PAUSE);
            }
            if (action.equalsIgnoreCase(ILogService.RESUME_ACTION)) {
                setActionType(ACTION_TYPE.RESUME);
            }
        }
    }

}

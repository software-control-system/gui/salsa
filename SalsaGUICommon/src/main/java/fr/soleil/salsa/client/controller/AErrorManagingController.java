package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.IView;

/**
 * An {@link AController} that is able to handle its errors
 * 
 * @author girardot
 * 
 * @param <T> The type of {@link IView}
 */
public abstract class AErrorManagingController<T extends IView<?>> extends AController<T> {

    protected IErrorController errorController;

    public AErrorManagingController(T view, IErrorController errorController) {
        this(view, errorController, false);
    }

    public AErrorManagingController(T view, IErrorController errorController, boolean readOnly) {
        super(view, readOnly);
        setErrorController(errorController);
    }

    public IErrorController getErrorController() {
        return errorController;
    }

    public void setErrorController(IErrorController errorController) {
        this.errorController = errorController;
    }

    /**
     * Displays an error message.
     * 
     * @param message
     */
    protected void errorMessage(String message) {
        if (errorController != null) {
            this.errorController.addErrorMessage(message);
        }
    }

}

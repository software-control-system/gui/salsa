package fr.soleil.salsa.client.util;

/**
 * 
 * can edit the cell type Double
 * 
 * @author Administrateur
 * 
 */
public class CellDoubleEditor extends CellNumberEditor<Double> {

    private static final long serialVersionUID = -9198969222646336949L;

    public CellDoubleEditor() {
        super();
    }

    @Override
    protected Double parseNumber(String value) {
        return Double.valueOf(value);
    }

}

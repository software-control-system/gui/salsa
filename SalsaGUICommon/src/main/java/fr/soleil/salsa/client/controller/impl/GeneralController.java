package fr.soleil.salsa.client.controller.impl;

import java.util.List;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IGeneralController;
import fr.soleil.salsa.client.view.GeneralView;
import fr.soleil.salsa.entity.Behaviour;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.IPostScanBehaviour;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.PostScanBehaviourModel;
import fr.soleil.salsa.entity.event.ScanAddOnModel;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.util.ComparatorUtil;
import fr.soleil.salsa.exception.ScanNotFoundException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * @author Alike
 * 
 *         Class relative to the controller of the General view.
 * 
 */
public class GeneralController extends AController<GeneralView> implements IGeneralController {

    public static final Logger LOGGER = LoggingUtil.getLogger(GeneralController.class);

    /**
     * The configuration.
     */
    private IConfig<?> config;
    private IConfig<?> dbconfig;

    /**
     * The config listener.
     */
    private final GCListener<IConfig<?>> configListener = new GCListener<IConfig<?>>();

    /**
     * the constructor.
     */
    public GeneralController() {
        this(null);
    }

    /**
     * the constructor.
     */
    public GeneralController(GeneralView view) {
        super(view);
    }

    @Override
    protected GeneralView generateView() {
        return new GeneralView(this);
    }

    @Override
    public void notifyActuatorDelay(String actuatorDelay) {
        if (config != null) {
            double oldValue = config.getActuatorsDelay();
            try {
                double d = Double.parseDouble(actuatorDelay);
                if (oldValue != d) {
                    this.config.setActuatorsDelay(d);
                    config.setModified(true);
                }
            } catch (NumberFormatException fe) {
                if (view != null) {
                    view.setActuatorDelay(oldValue, false);
                }
            }
        }
    }

    @Override
    public void notifyDataRecorderConfig(String aconfig) {
        if (config != null) {
            String oldValue = config.getDataRecorderConfig();
            if (!ComparatorUtil.stringEquals(oldValue, aconfig)) {
                config.setDataRecorderConfig(aconfig);
                config.setModified(true);
            }
        }
    }

    @Override
    public void notifyActuatorSelected(int index) {
        IPostScanBehaviour postScanBehaviour = getPostScanBehaviour(config);
        if (postScanBehaviour != null) {
            postScanBehaviour.setActuator(index);
            config.setModified(true);
        }
    }

    private IPostScanBehaviour getPostScanBehaviour(IConfig<?> aconfig) {
        IPostScanBehaviour postScanBehaviour = null;
        IScanAddOns addOn = getScanAddOn(aconfig);
        if (addOn != null) {
            postScanBehaviour = addOn.getPostScanBehaviour();
        }
        return postScanBehaviour;
    }

    private IScanAddOns getScanAddOn(IConfig<?> aconfig) {
        IScanAddOns addOn = null;
        if (aconfig != null) {
            addOn = aconfig.getScanAddOn();
        }
        return addOn;
    }

    @Override
    public void notifyEnableActuatorSpeed(boolean enableActuatorSpeed) {
        if (config != null) {
            config.setEnableScanSpeed(enableActuatorSpeed);
            config.setModified(true);
        }
    }

    @Override
    public void notifyOntheFly(boolean onTheFly) {
        if (config != null) {
            this.config.setOnTheFly(onTheFly);
            this.config.setEnableScanSpeed(onTheFly);
            config.setModified(true);
        }
    }

    @Override
    public void notifyPostScanBehaviour(String code) {
        if (config != null) {
            Behaviour b = Behaviour.valueOf(Behaviour.class, code);
            if (getPostScanBehaviour(config) != null) {
                getPostScanBehaviour(config).setBehaviour(b);
                if (view != null) {
                    int nbArgs = b.getArgumentCount();
                    view.setSensorsComboEnabled(nbArgs > 0);
                    view.setActuatorsComboEnabled(nbArgs == 2);
                }
                config.setModified(true);
            }
        }
    }

    @Override
    public void notifyScanNumberChanged(String sScanNumber) {
        if (config != null) {
            int oldValue = config.getScanNumber();
            try {
                int scanNumber = Integer.parseInt(sScanNumber);
                if (oldValue != scanNumber) {
                    this.config.setScanNumber(scanNumber);
                    view.setScanNumber(scanNumber, false);
                    config.setModified(true);
                }
            } catch (NumberFormatException fe) {
                if (view != null) {
                    view.setScanNumber(oldValue, false);
                }
            }
        }
    }

    @Override
    public void notifySensorSelected(int index) {
        if (getPostScanBehaviour(config) != null) {
            getPostScanBehaviour(config).setSensor(index);
            config.setModified(true);
        }
    }

    @Override
    public void notifyZigZag(boolean zigzag) {
        if (config != null) {
            config.setZigzag(zigzag);
            config.setModified(true);
        }
    }

    private void refresh() {
        // set the current configuration value
        refresh(false);
        // set the database configuration value
        refresh(true);
    }

    /**
     * Refresh the view, with model data.
     */
    private void refresh(boolean saved) {
        IConfig<?> currentConfig = config;
        if (saved) {
            currentConfig = dbconfig;
        }

        if ((view != null) && (config != null)) { // We disable listener
            view.disableListener();
            view.disableControls();
            if (currentConfig != null) {
                view.enableControls();
                view.setScanNumber(currentConfig.getScanNumber(), saved);
                view.setActuatorDelay(currentConfig.getActuatorsDelay(), saved);
                view.setTimebaseDelay(currentConfig.getTimebasesDelay(), saved);
                if (!saved) {
                    // Various value on the view
                    view.setZigzag(currentConfig.isZigzag());
                    view.setOnTheFly(currentConfig.isOnTheFly());
                    view.setEnableActuatorSpeed(currentConfig.isEnableScanSpeed());
                    view.setDataRecorderConfig(currentConfig.getDataRecorderConfig());

                    // Behaviour
                    IPostScanBehaviour postScanBehaviour = getPostScanBehaviour(currentConfig);
                    if ((postScanBehaviour != null) && (postScanBehaviour.getBehaviour() != null)) {
                        Behaviour selectedBehaviour = postScanBehaviour.getBehaviour();
                        int selectedIndex = 0;
                        view.clearPostScanBehaviour();
                        Behaviour[] behaviours = Behaviour.values();
                        for (int i = 0; i < behaviours.length; i++) {
                            Behaviour b = behaviours[i];
                            view.addPostScanBehaviour(b.name(), b.getShortDescription());
                            if (selectedBehaviour == b) {
                                selectedIndex = i;
                                view.setDescription(b.getLongDescription());
                            }
                        }
                        view.setPostScanSelected(selectedIndex);

                        // We enable or not the sensor and actuator combo box.
                        int nbArgs = selectedBehaviour.getArgumentCount();
                        view.setSensorsComboEnabled(nbArgs > 0);
                        view.setActuatorsComboEnabled(nbArgs == 2);

                        // Sensors
                        List<ISensor> sensors = config.getSensorsList();
                        view.clearSensors();
                        for (int i = 0; i < sensors.size(); i++) {
                            ISensor s = sensors.get(i);
                            if (s.isEnabled()) {
                                view.addSensor(s.getName(), s.getName());
                                if (i == postScanBehaviour.getSensor()) {
                                    view.setSensorSelected(i);
                                }
                            }
                        }
                        if (sensors.size() == 0) {
                            view.addSensor(null, "Not defined");
                        }

                        // Actuators
                        IDimension dimension = currentConfig.getDimensionX();
                        if (dimension != null) {
                            List<IActuator> actuators = dimension.getActuatorsList();
                            view.clearActuators();
                            if (actuators != null) {
                                for (int i = 0; i < actuators.size(); i++) {
                                    IActuator a = actuators.get(i);
                                    if (a.isEnabled()) {
                                        view.addActuator(a.getName(), a.getName());
                                        if (i == postScanBehaviour.getActuator()) {
                                            view.setActuatorSelected(i);
                                        }
                                    }
                                }

                                if (actuators.size() == 0) {
                                    view.addActuator(null, "Not defined");
                                }
                            }
                        }
                    }

                    // Check box enable/Disable

                    view.setZigZagEnabled(currentConfig instanceof IConfig2D);

                    view.setOnTheFlyEnabled(
                            !((currentConfig instanceof IConfigHCS) && (currentConfig instanceof IConfigK)));

                    view.setEnableActuatorSpeedControlEnabled(
                            currentConfig.isOnTheFly() || (config instanceof IConfigHCS));

                    // Disable onthefly and setEnableScanSpeed
                    if (currentConfig instanceof IConfigK) {
                        currentConfig.setOnTheFly(false);
                        currentConfig.setEnableScanSpeed(false);
                        view.setOnTheFlyEnabled(false);
                        view.setOnTheFly(false);
                        view.setEnableActuatorSpeed(false);
                        view.setEnableActuatorSpeedControlEnabled(false);
                    }
                }
            }
            view.enableListener();
        }
    }

    /**
     * Sets the config.
     * 
     * @param config
     */
    @SuppressWarnings("unchecked")
    public void setConfig(IConfig<?> aconfig) {
        if ((config != null) && (config instanceof IEventSource)) {
            ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) config).removeListener(configListener);
        }

        if (view != null) {
            view.clearValue();
            view.setEnabled(false);
        }

        config = aconfig;
        dbconfig = null;
        if (config != null) {
            try {
                dbconfig = SalsaAPI.getConfigById(config.getId(), true);
                if (dbconfig.getScanAddOn() == null) {
                    dbconfig.setScanAddOn(new ScanAddOnModel());
                }

                if (getPostScanBehaviour(dbconfig) == null) {
                    getScanAddOn(dbconfig).setPostScanBehaviour(new PostScanBehaviourModel());
                }
            } catch (ScanNotFoundException e) {
            }

            if (config.getScanAddOn() == null) {
                config.setScanAddOn(new ScanAddOnModel());
            }

            if (getPostScanBehaviour(config) == null) {
                getScanAddOn(config).setPostScanBehaviour(new PostScanBehaviourModel());
            }

            if (config instanceof IEventSource) {
                ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) this.config).addListener(configListener);
            }
        }

        refresh();

        if (view != null) {
            view.setEnabled(config != null);
        }
    }

    @Override
    public String[] getConfigList() {
        String[] configList = null;
        if (getDevicePreferences() != null) {
            String scanServer = getDevicePreferences().getScanServer();
            // Read the property of DataRecorder =
            // //storage/recorder/datarecorder.1/
            String dataRecorderDeviceName = "storage/recorder/datarecorder.1";
            Database database = TangoDeviceHelper.getDatabase();
            if ((database != null) && SalsaUtils.isDefined(scanServer)) {
                try {
                    DbDatum dbDatum = database.get_device_property(scanServer, "DataRecorder");
                    if (dbDatum != null) {
                        dataRecorderDeviceName = dbDatum.extractString();
                    }

                    if (SalsaUtils.isDefined(dataRecorderDeviceName)) {
                        DeviceProxy dataRecorderProxy = TangoDeviceHelper.getDeviceProxy(dataRecorderDeviceName);
                        if (dataRecorderProxy == null) {
                            LOGGER.error("Could not get DeviceProxy for " + dataRecorderDeviceName);
                        } else if (TangoCommandHelper.doesCommandExist(dataRecorderDeviceName, "GetConfigList")) {
                            DeviceData deviceData = dataRecorderProxy.command_inout("GetConfigList");
                            if (deviceData != null) {
                                configList = deviceData.extractStringArray();
                            }
                        } else {
                            LOGGER.error("GetConfigList cmmand does not exist on device " + dataRecorderDeviceName);
                        }
                    }
                } catch (DevFailed e) {
                    LOGGER.error("Cannot execute command {}/GetConfigList {}", dataRecorderDeviceName,
                            DevFailedUtils.toString(e));
                    LOGGER.debug("Stack trace", e);
                }

            } else {
                LOGGER.error("ScanServer is not defined in devicePreferences file");
            }

        }
        if ((configList == null) || (configList.length == 0)) {
            configList = new String[] { " " };
        }

        return configList;
    }

    @Override
    public void notifyTimebaseDelay(String timebaseDelay) {
        if (config != null) {
            double oldValue = config.getTimebasesDelay();
            try {
                double d = Double.parseDouble(timebaseDelay);
                if (oldValue != d) {
                    this.config.setTimebasesDelay(d);
                    config.setModified(true);
                }
            } catch (NumberFormatException fe) {
                if (view != null) {
                    view.setTimebaseDelay(oldValue, false);
                }
            }
        }

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * 
     * A multi-events listener.
     * 
     * @param <T>
     */
    private class GCListener<T> implements IListener<EntityPropertyChangedEvent<T>> {

        @Override
        public void notifyEvent(EntityPropertyChangedEvent<T> event) {
            refresh();
        }
    }

}

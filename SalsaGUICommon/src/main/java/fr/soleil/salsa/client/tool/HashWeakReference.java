package fr.soleil.salsa.client.tool;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * Those weak references implements the hashCode and equals methods by delegating them to the object
 * they reference. This is mostly useful to create hash map buffers.
 * 
 * @param <T>
 */
public class HashWeakReference<T> extends WeakReference<T> {

    public HashWeakReference(T referent) {
        super(referent);
        assert referent != null : "The referent object cannot be null.";
    }

    public HashWeakReference(T referent, ReferenceQueue<? super T> q) {
        super(referent, q);
        assert referent != null : "The referent object cannot be null.";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (obj == null) {
            return false;
        }
        else if (obj instanceof HashWeakReference<?>) {
            HashWeakReference<?> that = (HashWeakReference<?>) obj;
            return this.get().equals(that.get());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.get() != null ? this.get().hashCode() : 0;
    }
}

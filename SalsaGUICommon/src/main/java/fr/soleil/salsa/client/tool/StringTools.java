package fr.soleil.salsa.client.tool;

import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Tools for handling strings.
 */
public class StringTools {

    /**
     * The {@link String#replaceAll(String, String)} method cannot handle a replacement string with
     * "\".
     * 
     * @param string
     * @param oldString
     * @param newString
     * @return
     */
    public static String replaceAll(String string, String oldString, String newString) {
        String result = null;
        if ((string != null) && (newString != null) && SalsaUtils.isDefined(oldString)) {
            StringBuffer resultBuffer = new StringBuffer();
            int startIndex = 0;
            int indexOld = 0;
            while ((indexOld = string.indexOf(oldString, startIndex)) >= 0) {
                resultBuffer.append(string.substring(startIndex, indexOld));
                resultBuffer.append(newString);
                startIndex = indexOld + oldString.length();
            }
            resultBuffer.append(string.substring(startIndex));
            result = resultBuffer.toString();
        }
        return result;
    }
}

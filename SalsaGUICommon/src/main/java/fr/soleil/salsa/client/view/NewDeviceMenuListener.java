package fr.soleil.salsa.client.view;

import java.util.List;

public interface NewDeviceMenuListener {

    public void deviceAdded(String newDeviceName);

    public void devicesAdded(List<String> deviceList);

}

package fr.soleil.salsa.client.view.component;

import javax.swing.JMenuItem;

/**
 * @author Alike MenuItem with value.
 */
public class ValueMenuItem extends JMenuItem {

    private static final long serialVersionUID = -295391355780874674L;

    /**
     * The value.
     */
    private String value;

    /**
     * Gets the value.
     * 
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

}

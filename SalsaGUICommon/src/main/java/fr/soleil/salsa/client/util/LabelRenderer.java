package fr.soleil.salsa.client.util;

import javax.swing.JLabel;

public class LabelRenderer extends JLabel {

    private static final long serialVersionUID = -6328209822177824694L;
    private String dbValue = null;

    public LabelRenderer() {
        setOpaque(true);
    }

    @Override
    public void setText(String arg0) {
        super.setText(arg0);
        BackgroundRenderer.setBackgroundCellRenderer(this, dbValue);
    }

    public void setDbValue(String dbValue) {
        this.dbValue = dbValue;
        BackgroundRenderer.setBackgroundCellRenderer(this, dbValue);
    }

}

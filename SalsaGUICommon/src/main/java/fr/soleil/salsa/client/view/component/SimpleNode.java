package fr.soleil.salsa.client.view.component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alike Class relative to the representation of simple arborescent
 *         structure.
 */
public class SimpleNode {

    /**
     * The value.
     */
    private String value;
    
    /**
     * The label.
     */
    private String label;

    /**
     * The parentNode
     */
    private SimpleNode parent;

    /**
     * Children.
     */
    private List<SimpleNode> children;
    
    /**
     * Constructor.
     * @param value
     * @param label
     */
    public SimpleNode(String value, String label) {
        this.value = value;
        this.label = label;
        children = new ArrayList<SimpleNode>();    
    }
    
    /**
     * Gets children.
     * @return
     */
    public List<SimpleNode> getChildren() {
        return children;
    }

    /**
     * Gets the label.
     * 
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     * Gets the parent.
     * 
     * @return
     */
    public SimpleNode getParent() {
        return parent;
    }

    /**
     * Gets the value.
     * 
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets children.
     * @param children
     */
    public void setChildren(List<SimpleNode> children) {
        this.children = children;
    }

    /**
     * Sets the label.
     * 
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Sets the parent.
     * 
     * @param parent
     */
    public void setParent(SimpleNode parent) {
        this.parent = parent;
    }

    /**
     * Sets the value.
     * 
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

}

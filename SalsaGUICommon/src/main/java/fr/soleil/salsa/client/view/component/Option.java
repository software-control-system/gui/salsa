package fr.soleil.salsa.client.view.component;

/**
 * An option in a combo box. Used so the controller can communicate the
 * available options without knowing anything about Swing.
 */
public class Option {
    /**
     * The value to be returned if the user choose this option..
     */
    public Object value;

    /**
     * The text to be displayed in the combo box.
     */
    public String text;

    /**
     * Constructor.
     * Sets both value and text at null.
     */
    public Option() {
        super();
    }

    /**
     * Constructor.
     * @param value
     * @param text
     */
    public Option(Object value, String text) {
        super();
        this.value = value;
        this.text = text;
    }

    /**
     * The value to be returned if the user choose this option..
     */
    public Object getValue() {
        return value;
    }

    /**
     * The value to be returned if the user choose this option..
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * The text to be displayed in the combo box.
     */
    public String getText() {
        return text;
    }

    /**
     * The text to be displayed in the combo box.
     */
    public void setText(String text) {
        this.text = text;
    }
    
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        else if(obj == null) {
            return false;
        }
        else if(obj instanceof Option) {
            Option that = (Option) obj;
            boolean valuesEqual = this.value != null ? this.value.equals(that.value) : that.value == null;
            boolean textsEqual = this.text != null ? this.text.equals(that.text) : that.text == null;;
            return valuesEqual && textsEqual;
        }
        else {
            return false;
        }
    }
}
package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.soleil.salsa.client.controller.IGeneralController;
import fr.soleil.salsa.client.util.BackgroundRenderer;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * @author Alike
 * 
 *         Class relative to the view of the "General" dock item.
 */
public class GeneralView extends JPanel implements IView<IGeneralController> {

    private static final long serialVersionUID = -6026501670234675090L;

    private String scanNumberValue = null;
    private String dbScanNumberValue = null;

    private String actuatorDelay = null;
    private String dbActuatorDelay = null;

    private String timebaseDelay = null;
    private String dbTimebaseDelay = null;

    private final JButton refreshButton = new JButton();
    /**
     * The scan number text field.
     */
    private JTextField txtScanNumber;

    /**
     * The DataRecorderConfig text field.
     */
    private JComboBox<String> cbDataRecorderConfig;

    /**
     * The actuator delay text field.
     */
    private JTextField txtActuatorDelay;

    /**
     * The timebase delay text field.
     */
    private JTextField txtTimebaseDelay;

    /**
     * The zig zag check box.
     */
    private JCheckBox cbZigZag;

    /**
     * The on the fly check box.
     */
    private JCheckBox cbOnTheFly;

    /**
     * The enable actuator speed check box.
     */
    private JCheckBox cbEnableActuatorSpeed;

    /**
     * The controller who's in charge to manage this view.
     */
    private IGeneralController controller;

    /**
     * The post scan combo box.
     */
    private JComboBox<Item> cbPostScan;

    private JComboBox<Item> cbActuators;

    /**
     * The sensors combo box.
     */
    private JComboBox<Item> cbSensors;

    /**
     * The description of the selected behaviour.
     */
    private JLabel lblDescription;

    /**
     * The actuator delay label.
     */
    private JLabel lblActuatorDelay;

    /**
     * The timebase delay label.
     */
    private JLabel lblTimebaseDelay;

    /**
     * The scan number label.
     */
    private JLabel lblScanNumber;

    /**
     * The ontheFly label.
     */
    private JLabel lblOnTheFly;

    /**
     * The scanSpeed label.
     */
    private JLabel lblScanSpeed;

    /**
     * The scanSpeed label.
     */
    private JLabel labelDataRecorderConfig;

    /**
     * A panel that contains the combo box fopr the post scan behaviour.
     */
    private JPanel combosPanel;

    private boolean editable = true;

    /**
     * Class relative to binding with combo box items.
     */
    private class Item {

        /**
         * The value (i.e the identifier key) for the combo box item.
         */
        private final String value;

        /**
         * The text to display.
         */
        private final String text;

        Item(String value, String text) {
            this.value = value;
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    /**
     * The listener relative to focus action of components.
     */
    private final FocusListener focusListener = new FocusListener() {

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
         */
        @Override
        public void focusGained(FocusEvent e) {
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
         */
        @Override
        public void focusLost(FocusEvent e) {
            Object s = e.getSource();
            if (s == txtScanNumber) {
                scanNumberChanged();
                setBackgroundScanNumber();
            } else if (s == txtActuatorDelay) {
                actuatorDelayChanged();
                setBackgroundActuatorDelay();
            } else if (s == txtTimebaseDelay) {
                timebaseDelayChanged();
                setBackgroundTimebaseDelay();
            }
        }

    };

    private final KeyListener keyListener = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            Object s = e.getSource();
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                if (s == txtScanNumber) {
                    scanNumberChanged();
                } else if (s == txtActuatorDelay) {
                    actuatorDelayChanged();
                } else if (s == txtTimebaseDelay) {
                    timebaseDelayChanged();
                }
            }
            if (s == txtScanNumber) {
                setBackgroundScanNumber();
            } else if (s == txtActuatorDelay) {
                setBackgroundActuatorDelay();
            } else if (s == txtTimebaseDelay) {
                setBackgroundTimebaseDelay();
            }
        }
    };

    /**
     * The listener relative to value changed action on components. Only one for
     * every components.
     */
    private final ActionListener actionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            Object s = e.getSource();

            if (s == refreshButton) {
                refreshScriptList();
            } else {
                String actionCommand = e.getActionCommand();
                if ((e.getModifiers() == InputEvent.BUTTON1_MASK) || !actionCommand.equals("comboBoxChanged")) {
                    if (s == cbZigZag) {
                        zigZagChanged();
                    } else if (s == cbOnTheFly) {
                        onTheFlyChanged();
                    } else if (s == cbEnableActuatorSpeed) {
                        enabledActuatorSpeedChanged();
                    } else if (s == cbPostScan) {
                        postScanBehaviourChanged();
                    } else if (s == cbSensors) {
                        sensorSelectedChanged();
                    } else if (s == cbActuators) {
                        actuatorSelectedChanged();
                    } else if (s == cbDataRecorderConfig) {
                        dataRecorderScriptChanged();
                    }
                }
            }

        }

    };

    public GeneralView() {
        this(null);
    }

    public void setLightView(boolean light) {
        lblOnTheFly.setVisible(!light);
        lblScanSpeed.setVisible(!light);
        cbOnTheFly.setVisible(!light);
        cbEnableActuatorSpeed.setVisible(!light);
    }

    private void refreshScriptList() {
        cbDataRecorderConfig.removeAllItems();
        if (controller != null) {
            String[] configList = controller.getConfigList();
            if (configList != null) {
                for (String config : configList) {
                    cbDataRecorderConfig.addItem(config);
                }
            }
        }
    }

    /**
     * The constructor.
     * 
     * @param controller
     */
    public GeneralView(IGeneralController controller) {
        this.controller = controller;
        initialize();
        disableControls();
        cbActuators.setEnabled(false);
        cbSensors.setEnabled(false);
        refreshScriptList();
        setEnabled(false);
        clearValue();
    }

    /**
     * Actuator delay has been changed.
     */
    private void actuatorDelayChanged() {
        actuatorDelay = txtActuatorDelay.getText();
        if (controller != null) {
            controller.notifyActuatorDelay(actuatorDelay);
        }
    }

    /**
     * Timebase delay has been changed.
     */
    private void timebaseDelayChanged() {
        timebaseDelay = txtTimebaseDelay.getText();
        if (controller != null) {
            controller.notifyTimebaseDelay(timebaseDelay);
        }
    }

    /**
     * The actuator has been changed.
     */
    private void actuatorSelectedChanged() {
        Item i = (Item) cbActuators.getSelectedItem();
        int index = findItem(cbActuators, i);
        if (controller != null) {
            controller.notifyActuatorSelected(index);
        }
    }

    /**
     * The dataRecorderScriptChanged
     */
    private void dataRecorderScriptChanged() {
        String config = (String) cbDataRecorderConfig.getSelectedItem();
        if ((config == null) || config.equals("(unnamed)")) {
            config = "";
        }
        if (controller != null) {
            controller.notifyDataRecorderConfig(config);
        }
    }

    /**
     * Add an actuator to the actuators combo box.
     * 
     * @param value
     * @param text
     */
    public void addActuator(String value, String text) {
        cbActuators.addItem(new Item(value, text));
    }

    /**
     * Add a post scan behaviour to the post scan behaviour combo box.
     * 
     * @param value
     * @param text
     */
    public void addPostScanBehaviour(String value, String text) {
        cbPostScan.addItem(new Item(value, text));
    }

    /**
     * Add a sensor to the sensors combo box.
     * 
     * @param value
     * @param text
     */
    public void addSensor(String value, String text) {
        cbSensors.addItem(new Item(value, text));
    }

    /**
     * Clear the actuator combo box items.
     */
    public void clearActuators() {
        cbActuators.removeAllItems();
    }

    /**
     * Clear the post scan behaviour combo box.
     */
    public void clearPostScanBehaviour() {
        cbPostScan.removeAllItems();
    }

    /**
     * Clear the sensors combo box.
     */
    public void clearSensors() {
        cbSensors.removeAllItems();
    }

    /**
     * Util method to arrange the placement of components.
     * 
     * @param x
     * @param y
     * @return
     */
    private GridBagConstraints createGBC(int x, int y, int fill) {
        GridBagConstraints r = new GridBagConstraints();

        r.fill = fill;
        r.insets = new Insets(2, 8, 2, 8);
        r.gridx = x;
        r.gridy = y;

        return r;
    }

    /**
     * Disable all listener.
     */
    public void disableListener() {
        txtScanNumber.removeKeyListener(keyListener);
        txtActuatorDelay.removeKeyListener(keyListener);
        txtTimebaseDelay.removeKeyListener(keyListener);
        cbZigZag.removeActionListener(actionListener);
        cbOnTheFly.removeActionListener(actionListener);
        cbEnableActuatorSpeed.removeActionListener(actionListener);
        cbPostScan.removeActionListener(actionListener);
        cbSensors.removeActionListener(actionListener);
        cbActuators.removeActionListener(actionListener);
        cbDataRecorderConfig.removeActionListener(actionListener);
        txtScanNumber.removeFocusListener(focusListener);
        txtActuatorDelay.removeFocusListener(focusListener);
        txtTimebaseDelay.removeFocusListener(focusListener);
    }

    /**
     * The boolean enable actuator speed has been changed.
     */
    private void enabledActuatorSpeedChanged() {
        if (controller != null) {
            controller.notifyEnableActuatorSpeed(cbEnableActuatorSpeed.isSelected());
        }
    }

    /**
     * Enable all listener.
     */
    public void enableListener() {
        txtScanNumber.addKeyListener(keyListener);
        txtActuatorDelay.addKeyListener(keyListener);
        txtTimebaseDelay.addKeyListener(keyListener);
        cbZigZag.addActionListener(actionListener);
        cbOnTheFly.addActionListener(actionListener);
        cbEnableActuatorSpeed.addActionListener(actionListener);
        cbPostScan.addActionListener(actionListener);
        cbSensors.addActionListener(actionListener);
        cbActuators.addActionListener(actionListener);
        cbDataRecorderConfig.addActionListener(actionListener);
        txtScanNumber.addFocusListener(focusListener);
        txtActuatorDelay.addFocusListener(focusListener);
        txtTimebaseDelay.addFocusListener(focusListener);
    }

    /**
     * Look for a specific item in a combo box.
     * 
     * @param combo
     * @param toFind
     * @return
     */
    private int findItem(JComboBox<Item> combo, Item toFind) {
        int index = -1;
        for (int i = 0; i < combo.getItemCount(); i++) {
            Item item = combo.getItemAt(i);
            if (item == toFind) {
                index = i;
                break;
            }
        }
        return index;
    }

    /*
     * public double getActuatorDelay() { return actuatorDelay; }
     */

    @Override
    public IGeneralController getController() {
        return controller;
    }

    @Override
    public void setController(IGeneralController controller) {
        this.controller = controller;
        refreshScriptList();
    }

    /**
     * Initialize the layout.
     */
    private void initialize() {
        JPanel mainPanel = new JPanel();
        this.setLayout(new BorderLayout());

        GridBagLayout l = new GridBagLayout();
        l.columnWidths = new int[] { 150, 150, 150, 150 };

        mainPanel.setLayout(l);

        lblScanNumber = new JLabel("ScanNumber");
        mainPanel.add(lblScanNumber, createGBC(0, 0, GridBagConstraints.HORIZONTAL));

        txtScanNumber = new JTextField();
        mainPanel.add(txtScanNumber, createGBC(1, 0, GridBagConstraints.HORIZONTAL));
        setBackgroundScanNumber();

        lblActuatorDelay = new JLabel("Actuator delay (s)");
        mainPanel.add(lblActuatorDelay, createGBC(0, 1, GridBagConstraints.HORIZONTAL));

        txtActuatorDelay = new JTextField();
        mainPanel.add(txtActuatorDelay, createGBC(1, 1, GridBagConstraints.HORIZONTAL));
        setBackgroundActuatorDelay();

        lblTimebaseDelay = new JLabel("Timebase delay (s)");
        mainPanel.add(lblTimebaseDelay, createGBC(0, 2, GridBagConstraints.HORIZONTAL));

        txtTimebaseDelay = new JTextField();
        mainPanel.add(txtTimebaseDelay, createGBC(1, 2, GridBagConstraints.HORIZONTAL));
        setBackgroundTimebaseDelay();

        labelDataRecorderConfig = new JLabel("DataRecorder configuration");
        mainPanel.add(labelDataRecorderConfig, createGBC(0, 3, GridBagConstraints.HORIZONTAL));
        cbDataRecorderConfig = new JComboBox<>();
        mainPanel.add(cbDataRecorderConfig, createGBC(1, 3, GridBagConstraints.HORIZONTAL));
        refreshButton.setToolTipText("Refresh the list of data recorder configuration");
        refreshButton.setIcon(Icons.getIcon("salsa.layout.reset"));
        refreshButton.setSize(22, 22);
        refreshButton.setPreferredSize(refreshButton.getSize());

        GridBagConstraints refreshConstraints = new GridBagConstraints();
        refreshConstraints.insets = new Insets(2, 2, 2, 2);
        refreshConstraints.gridx = 2;
        refreshConstraints.gridy = 3;
        refreshConstraints.anchor = GridBagConstraints.WEST;
        mainPanel.add(refreshButton, refreshConstraints);

        mainPanel.add(new JLabel("Zigzag"), createGBC(0, 4, GridBagConstraints.HORIZONTAL));
        cbZigZag = new JCheckBox();
        cbZigZag.getMinimumSize();
        mainPanel.add(cbZigZag, createGBC(1, 4, GridBagConstraints.VERTICAL));

        lblOnTheFly = new JLabel("On the fly");
        mainPanel.add(lblOnTheFly, createGBC(0, 5, GridBagConstraints.HORIZONTAL));
        cbOnTheFly = new JCheckBox();
        mainPanel.add(cbOnTheFly, createGBC(1, 5, GridBagConstraints.VERTICAL));

        lblScanSpeed = new JLabel("Enable scan speed");
        mainPanel.add(lblScanSpeed, createGBC(0, 6, GridBagConstraints.HORIZONTAL));
        cbEnableActuatorSpeed = new JCheckBox();
        mainPanel.add(cbEnableActuatorSpeed, createGBC(1, 6, GridBagConstraints.VERTICAL));

        GridBagLayout l2 = new GridBagLayout();

        combosPanel = new JPanel();
        TitledBorder tb = new TitledBorder("Post Scan Behaviour");
        combosPanel.setBorder(tb);

        combosPanel.setLayout(l2);
        l2.columnWidths = new int[] { 200, 200, 200 };
        GridBagConstraints gbc = createGBC(0, 7, GridBagConstraints.HORIZONTAL);
        gbc.gridwidth = 4;
        mainPanel.add(combosPanel, gbc);

        cbPostScan = new JComboBox<>();
        combosPanel.add(cbPostScan, createGBC(0, 0, GridBagConstraints.VERTICAL));

        cbSensors = new JComboBox<>();
        cbSensors.setEnabled(false);
        combosPanel.add(cbSensors, createGBC(1, 0, GridBagConstraints.VERTICAL));

        cbActuators = new JComboBox<>();
        cbActuators.setEnabled(false);
        combosPanel.add(cbActuators, createGBC(2, 0, GridBagConstraints.VERTICAL));

        lblDescription = new JLabel();
        GridBagConstraints gbcDesc = createGBC(0, 2, GridBagConstraints.VERTICAL);
        gbcDesc.gridwidth = 3;
        gbcDesc.gridheight = 2;
        combosPanel.add(lblDescription, gbcDesc);

        JScrollPane scrollPane = new JScrollPane(mainPanel);
        scrollPane.setPreferredSize(new Dimension(400, 200));

        this.add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * The on-the-fly checkbox value has been changed.
     */
    private void onTheFlyChanged() {
        if (controller != null) {
            controller.notifyOntheFly(cbOnTheFly.isSelected());
        }
    }

    /**
     * Sets the description of the selected behaviour.
     * 
     * @param description
     */
    public void setDescription(String description) {
        lblDescription.setText(description);
    }

    /**
     * The selected item in the post scan behaviour combo box has been changed.
     */
    private void postScanBehaviourChanged() {
        Item item = (Item) cbPostScan.getSelectedItem();
        if (item == null) {
            return;
        }
        if (controller != null) {
            controller.notifyPostScanBehaviour(item.value);
        }
    }

    /**
     * The scan number value has been changed.
     */
    private void scanNumberChanged() {
        scanNumberValue = txtScanNumber.getText();
        if (controller != null) {
            controller.notifyScanNumberChanged(scanNumberValue);
        }
    }

    /**
     * The selected value of the sensors combo box has been changed.
     */
    private void sensorSelectedChanged() {
        int index = cbSensors.getSelectedIndex();
        if (controller != null) {
            controller.notifySensorSelected(index);
        }
    }

    /**
     * Sets the actuator delay.
     * 
     * @param actuatorDelay
     */
    public void setActuatorDelay(double anactuatorDelay, boolean savedValue) {
        if (savedValue) {
            this.dbActuatorDelay = String.valueOf(anactuatorDelay);
        } else {
            this.actuatorDelay = String.valueOf(anactuatorDelay);
            txtActuatorDelay.setText(actuatorDelay);
        }
        setBackgroundActuatorDelay();

    }

    private void setBackgroundActuatorDelay() {
        BackgroundRenderer.setBackgroundField(this.txtActuatorDelay, actuatorDelay, dbActuatorDelay);
    }

    /**
     * Sets the actuator delay.
     * 
     * @param actuatorDelay
     */
    public void setTimebaseDelay(double atimebaseDelay, boolean savedValue) {
        if (savedValue) {
            this.dbTimebaseDelay = String.valueOf(atimebaseDelay);
        } else {
            this.timebaseDelay = String.valueOf(atimebaseDelay);
            txtTimebaseDelay.setText(timebaseDelay);
        }
        setBackgroundActuatorDelay();

    }

    private void setBackgroundTimebaseDelay() {
        BackgroundRenderer.setBackgroundField(this.txtTimebaseDelay, timebaseDelay, dbTimebaseDelay);
    }

    /**
     * Enable the actuators combo box.
     * 
     * @param b
     */
    public void setActuatorsComboEnabled(boolean b) {
        cbActuators.setEnabled(b);
    }

    /**
     * Sets the selected value of the actuators combo box.
     * 
     * @param index
     */
    public void setActuatorSelected(int index) {
        cbActuators.setSelectedIndex(index);
    }

    /**
     * (Un)/Check the enable actuator speed check box.
     * 
     * @param enableActuatorSpeed
     */
    public void setEnableActuatorSpeed(boolean enableActuatorSpeed) {
        cbEnableActuatorSpeed.setSelected(enableActuatorSpeed);
    }

    /**
     * Sets the on the fly value.
     * 
     * @param onTheFly
     */
    public void setOnTheFly(boolean onTheFly) {
        cbOnTheFly.setSelected(onTheFly);
    }

    /**
     * Sets the selected value of the post scan combo box.
     * 
     * @param index
     */
    public void setPostScanSelected(int index) {
        cbPostScan.setSelectedIndex(index);
    }

    /**
     * Sets the scan number.
     * 
     * @param scanNumber
     */
    public void setScanNumber(int scanNumber, boolean savedValue) {

        if (savedValue) {
            this.dbScanNumberValue = String.valueOf(scanNumber);
        } else {
            this.scanNumberValue = String.valueOf(scanNumber);
            txtScanNumber.setText(scanNumberValue);
        }
        setBackgroundScanNumber();
    }

    private void setBackgroundScanNumber() {
        BackgroundRenderer.setBackgroundField(this.txtScanNumber, scanNumberValue, dbScanNumberValue);
    }

    /**
     * Sets the dataRecorderConfig
     * 
     * @param scanNumber
     */
    public void setDataRecorderConfig(String dataRecorderConfig) {
        if (cbDataRecorderConfig.getItemCount() > 0) {
            if (SalsaUtils.isDefined(dataRecorderConfig)) {
                try {
                    cbDataRecorderConfig.setSelectedItem(dataRecorderConfig);
                } catch (Exception e) {
                    cbDataRecorderConfig.setSelectedIndex(0);
                }
            } else {
                cbDataRecorderConfig.setSelectedIndex(0);
            }
        }
    }

    /**
     * Enable/disable the sensors combo box.
     * 
     * @param b
     */
    public void setSensorsComboEnabled(boolean b) {
        cbSensors.setEnabled(b);
    }

    /**
     * Sets the selected value of the sensors combo box.
     * 
     * @param index
     */
    public void setSensorSelected(int index) {
        cbSensors.setSelectedIndex(index);
    }

    /**
     * Check/Uncheck the zig zag check box.
     * 
     * @param zigzag
     */
    public void setZigzag(boolean zigzag) {
        cbZigZag.setSelected(zigzag);
    }

    /**
     * Enable/Disable the zig zag check box.
     * 
     * @param enabled
     */
    public void setZigZagEnabled(boolean enabled) {
        cbZigZag.setEnabled(enabled);
    }

    /**
     * Enable/Disable the On the fly check box.
     * 
     * @param enabled
     */
    public void setOnTheFlyEnabled(boolean enabled) {
        cbOnTheFly.setEnabled(enabled);
        setEnableActuatorSpeedControlEnabled(enabled);
    }

    /**
     * Enable/Disable the Enable actuator speed controle checkbox.
     * 
     * @param enabled
     */
    public void setEnableActuatorSpeedControlEnabled(boolean enabled) {
        cbEnableActuatorSpeed.setEnabled(enabled);
    }

    /**
     * The zig zag value has been changed.
     */
    private void zigZagChanged() {
        if (controller != null) {
            controller.notifyZigZag(cbZigZag.isSelected());
        }
    }

    /**
     * Used to enable the controls when a config is selected.
     */
    public void enableControls() {
        txtScanNumber.setEditable(true);
        txtActuatorDelay.setEditable(true);
        txtTimebaseDelay.setEditable(true);
        cbDataRecorderConfig.setEnabled(true);
        cbZigZag.setEnabled(true);
        cbOnTheFly.setEnabled(true);
        cbEnableActuatorSpeed.setEnabled(true);
        cbPostScan.setEnabled(true);
        ((TitledBorder) combosPanel.getBorder()).setTitleColor(lblActuatorDelay.getForeground());
    }

    /**
     * Used to disable the controls when a config is selected.
     */
    public void disableControls() {
        txtScanNumber.setEditable(false);
        txtActuatorDelay.setEditable(false);
        txtTimebaseDelay.setEditable(false);
        cbDataRecorderConfig.setEnabled(false);
        cbZigZag.setEnabled(false);
        cbOnTheFly.setEnabled(false);
        cbEnableActuatorSpeed.setEnabled(false);
        cbPostScan.setEnabled(false);
        ((TitledBorder) combosPanel.getBorder()).setTitleColor(Color.LIGHT_GRAY);
    }

    public void clearValue() {
        txtScanNumber.setText("");
        txtActuatorDelay.setText("");
        cbZigZag.setSelected(false);
        cbOnTheFly.setSelected(false);
        cbEnableActuatorSpeed.setSelected(false);
        if (cbDataRecorderConfig.getItemCount() > 0) {
            cbDataRecorderConfig.setSelectedIndex(0);
        }

        txtScanNumber.setBackground(Color.WHITE);
        txtActuatorDelay.setBackground(Color.WHITE);

        clearActuators();
        clearPostScanBehaviour();
        clearSensors();

    }

    @Override
    public void setEnabled(boolean arg0) {
        txtScanNumber.setEditable(editable && arg0);
        txtActuatorDelay.setEditable(editable && arg0);
        txtTimebaseDelay.setEditable(editable && arg0);
        cbDataRecorderConfig.setEnabled(editable && arg0);
        cbZigZag.setEnabled(editable && arg0);
        cbOnTheFly.setEnabled(editable && arg0);
        cbEnableActuatorSpeed.setEnabled(editable && arg0);
        cbPostScan.setEnabled(editable && arg0);
        super.setEnabled(editable && arg0);
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        setEnabled(editable);
    }

    public boolean isEditable() {
        return editable;
    }

}
package fr.soleil.salsa.client.tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A second level cache using a hashmap for its implementation.
 * The objects contained in the cache are cleared when no other references to them exist in the JVM.
 * @param <K> the class of the keys of the cache. Must implements the hashCode and equals methods.
 * @param <E> the element contained in the cache.
 */
public class HashMapCache<K, E> {

    /**
     * An entry in the cache.
     */
    public class CacheEntry {
        
        private K key;
        
        private E element;

        /**
         * @return the key
         */
        public K getKey() {
            return key;
        }

        /**
         * @param key the key to set
         */
        public void setKey(K key) {
            this.key = key;
        }

        /**
         * @return the element
         */
        public E getElement() {
            return element;
        }

        /**
         * @param element the element to set
         */
        public void setElement(E element) {
            this.element = element;
        }
    }
    
    /**
     * The hash map that contains the objects.
     */
    private final Map<HashWeakReference<K>, HashWeakReference<E>> contentMap
            = new HashMap<HashWeakReference<K>, HashWeakReference<E>>();

    /**
     * Adds an element to the cache.
     * @param abstractKey
     * @param abstractDAO
     */
    public void register(K key, E element) {
        if (key != null && element != null) {
            HashWeakReference<K> keyReference = new HashWeakReference<K>(key);
            HashWeakReference<E> elementReference = new HashWeakReference<E>(element);
            contentMap.put(keyReference, elementReference);
        }
    }
    
    /**
     * Search for an element in the buffer. If there is no element, returns null.
     * @param abstractKey
     * @return
     */
    public E lookup(K key) {
        if(key == null) {
            return null;
        }
        HashWeakReference<E> reference = contentMap.get(new HashWeakReference<K>(key));
        return reference != null ? reference.get() : null;
    }
    
    /**
     * Returns the whole content.
     */
    public List<CacheEntry> getContent() {
        List<CacheEntry> content = new ArrayList<CacheEntry>();
        Set<Map.Entry<HashWeakReference<K>, HashWeakReference<E>>> entrySet = contentMap.entrySet();
        CacheEntry cacheEntry;
        K key;
        E element;
        for(Map.Entry<HashWeakReference<K>, HashWeakReference<E>> mapEntry : entrySet) {
            key = mapEntry.getKey().get();
            element = mapEntry.getValue().get();
            if(key != null && element != null) {
                cacheEntry = new CacheEntry();
                cacheEntry.setKey(key);
                cacheEntry.setElement(element);
                content.add(cacheEntry);
            }
        }
        return content;
    }
}

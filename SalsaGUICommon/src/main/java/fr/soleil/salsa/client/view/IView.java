package fr.soleil.salsa.client.view;

import fr.soleil.salsa.client.controller.IController;

/**
 * Views must implement this interface.
 * 
 * @author Administrateur
 * 
 * @param <C> the controller class.
 */
public interface IView<C extends IController<?>> {

    /**
     * Returns the controller of the view.
     * 
     * @return
     */
    public C getController();

    /**
     * Sets the controller of the view
     * 
     * @param controller The controller to set
     */
    public void setController(C controller);

    /**
     * Shows or hides the view.
     * 
     * @param visible
     */
    public void setVisible(boolean visible);
}

package fr.soleil.salsa.client.util;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * 
 * @author Administrateur
 * 
 */
public class CellNumberRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

    private static final long serialVersionUID = -6229588420280797550L;

    public CellNumberRenderer() {
        super();
    }

    public JComponent getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
        // JLabel component = (JLabel) super.getTableCellRendererComponent(table, value, isSelected,
        // hasFocus, row, column);
        String doubleValue = (value == null ? "" : value.toString());
        LabelRenderer renderer = new LabelRenderer();
        renderer.setText(doubleValue);
        renderer.setHorizontalAlignment(SwingConstants.RIGHT);
        if (value != null && value instanceof NumberValue) {
            renderer.setDbValue(((NumberValue) value).getDbValue());
        }

        return renderer;
    }

}
package fr.soleil.salsa.client.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import fr.soleil.salsa.client.controller.IErrorController;

/**
 * View that shows the error messages. The error window is a non model dialog box with multi thread
 * support. If error messages are added before the user closed the window, the new messages are
 * displayed on the same window after the previous one.
 */
public class ErrorView implements IView<IErrorController> {

    /**
     * The maximum height in pixels.
     */
    private static final int MAX_HEIGHT = 300;

    /**
     * The maximum width in pixels.
     */
    private static final int MAX_WIDTH = 600;

    /**
     * Used to display the error on the edt.
     */
    private class ErrorDisplayTask implements Runnable {
        private final String errorText;

        public ErrorDisplayTask(String errorText) {
            this.errorText = errorText;
        }

        @Override
        public void run() {
            setText(errorText);
            setVisible(true);
        }
    }

    /**
     * The controller for this view.
     */
    private IErrorController controller;

    /**
     * The scroll pane.
     */
    private JScrollPane scrollPane;

    /**
     * The main window panel.
     */
    private JPanel mainPanel;

    /**
     * The content displayed in the error view.
     */
    private JTextPane textPane;

    /**
     * The error window.
     */
    private JDialog dialog;

    /**
     * The closing button.
     */
    private JButton closeButton;

    /**
     * The error icon.
     */
    private Icon errorIcon;

    /**
     * The label that contains the error icon.
     */
    private JLabel errorIconLabel;

    /**
     * The top panel that contains the error icon and the text.
     */
    private JPanel topPanel;

    /**
     * The parent window, normally the main application window.
     */
    private JFrame parentWindow = null;

    public ErrorView() {
        this(null);
    }

    /**
     * Constructor.
     */
    public ErrorView(IErrorController controller) {
        super();
        this.controller = controller;
        // Error View has the particularity that its initialize method is not called in the
        // constructor.
        // It is called in setParentWindow, because the JDialog component needs the parent window to
        // be constructed.
    }

    /**
     * Performs initialization. Error View has the particularity that its initialize method is not
     * called in the constructor. It is called in setParentWindow, because the JDialog component
     * needs the parent window to be constructed.
     */
    private void initialize() {
        if (parentWindow != null) {
            dialog = new JDialog(parentWindow, "Error");
            dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we) {
                    if (controller != null) {
                        controller.notifyClose();
                    }
                }
            });

            errorIcon = UIManager.getIcon("OptionPane.errorIcon");
            errorIconLabel = new JLabel(errorIcon);

            textPane = new JTextPane();
            textPane.setEditable(false);
            textPane.setBackground(errorIconLabel.getBackground());
            Border textBorder = BorderFactory.createEmptyBorder();
            textPane.setBorder(textBorder);

            scrollPane = new JScrollPane(textPane);
            scrollPane.setBackground(errorIconLabel.getBackground());
            scrollPane.setBorder(null);
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

            closeButton = new JButton("Ok");
            closeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (controller != null) {
                        controller.notifyClose();
                    }
                }
            });

            // Yet another workaround around a Swing Bug : in Swing 1.1, mixing GridBagLayouts,
            // scrollpanes and resizing is a bad idea.
            int padding = 10;
            topPanel = new JPanel();
            topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
            topPanel.add(Box.createRigidArea(new Dimension(padding, 0)));
            topPanel.add(errorIconLabel);
            topPanel.add(Box.createRigidArea(new Dimension(2 * padding, 0)));
            topPanel.add(scrollPane);
            topPanel.add(Box.createRigidArea(new Dimension(padding, 0)));

            mainPanel = new JPanel();
            mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
            mainPanel.add(Box.createRigidArea(new Dimension(0, padding)));
            mainPanel.add(topPanel);
            mainPanel.add(Box.createRigidArea(new Dimension(0, 2 * padding)));
            mainPanel.add(closeButton);
            mainPanel.add(Box.createRigidArea(new Dimension(0, padding)));
            closeButton.setAlignmentX(Component.CENTER_ALIGNMENT);

            dialog.setBackground(errorIconLabel.getBackground());
            dialog.setContentPane(mainPanel);
            dialog.setLocationRelativeTo(parentWindow);
        }
    }

    /**
     * Returns the controller.
     */
    @Override
    public IErrorController getController() {
        return controller;
    }

    @Override
    public void setController(IErrorController controller) {
        this.controller = controller;
    }

    /**
     * Gets the text of the view.
     * 
     * @return
     */
    public String getText() {
        return textPane.getText();
    }

    /**
     * Sets the text of the view.
     * 
     * @param text
     */
    public void setText(String text) {
        int oldWidth = dialog.getWidth();
        int oldHeight = dialog.getHeight();
        int oldX = dialog.getX();
        int oldY = dialog.getY();
        boolean oldVisible = dialog.isVisible();
        JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();
        int oldPosition = verticalScrollBar.getValue();
        int oldCaretPosition = textPane.getCaretPosition();

        StyledDocument document = new DefaultStyledDocument();
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setBold(attributeSet, true);
        StyleConstants.setAlignment(attributeSet, StyleConstants.ALIGN_JUSTIFIED);
        try {
            document.insertString(document.getLength(), text, attributeSet);
        }
        catch (BadLocationException e) {
            // Normally cannot happen.
            textPane.setText(text);
        }

        textPane = new JTextPane();
        textPane.setEditable(false);
        textPane.setBackground(errorIconLabel.getBackground());
        textPane.setStyledDocument(document);

        scrollPane.setViewportView(textPane);

        dialog.pack();

        // Workaround around a Swing bug : dialog maximum size is ignored.
        int width = dialog.getWidth();
        if (width > MAX_WIDTH) {
            width = MAX_WIDTH;
            dialog.setSize(width, MAX_HEIGHT);
        }
        else {
            int height = dialog.getHeight();
            if (height > MAX_HEIGHT) {
                height = MAX_HEIGHT;
                dialog.setSize(width, height);
            }
        }

        if (oldVisible && (oldWidth != 0) && (oldHeight != 0)) {
            int newWidth = dialog.getWidth();
            int newHeight = dialog.getHeight();
            int newX = oldX + (oldWidth - newWidth) / 2;
            int newY = oldY + (oldHeight - newHeight) / 2;
            dialog.setLocation(newX, newY);
        }
        else {
            dialog.setLocationRelativeTo(parentWindow);
        }

        if (oldVisible) {
            try {
                textPane.setCaretPosition(oldCaretPosition);
            }
            catch (IllegalArgumentException e) {
                textPane.setCaretPosition(textPane.getSelectionEnd());
            }
            verticalScrollBar.setValue(oldPosition);
        }
    }

    /**
     * Displays the error message.
     * 
     * @param errorText
     */
    public void display(String errorText) {
        SwingUtilities.invokeLater(new ErrorDisplayTask(errorText));
    }

    /**
     * Closes the error window.
     */
    public void close() {
        textPane.setText("");
        setVisible(false);
    }

    /**
     * The parent window, normally the main application window.
     */
    public JFrame getParentWindow() {
        return parentWindow;
    }

    /**
     * The parent window, normally the main application window.
     */
    // Error View has the particularity that its initialize method is not called in the constructor.
    // It is called in setParentWindow, because the JDialog component needs the parent window to be
    // constructed.
    public void setParentWindow(JFrame parentWindow) {
        this.parentWindow = parentWindow;
        initialize();
    }

    /**
     * Shows or hides the view.
     */
    @Override
    public void setVisible(boolean visible) {
        dialog.setVisible(visible);
    }
}

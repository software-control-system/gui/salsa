package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.ErrorView;

/**
 * The interface of the controller of the view that shows error messages. The error window is a non
 * model dialog box with multi thread support. If error messages are added before the user closed
 * the window, the new messages are displayed on the same window after the previous one.
 */
public interface IErrorController extends IController<ErrorView> {

    /**
     * Adds an error message.
     * 
     * @param errorMessage
     */
    public void addErrorMessage(String errorMessage);

    /**
     * Adds a preference error message.
     * 
     * @param preferenceErrorMessage
     */
    public void addErrorPreferenceMessage(String preferenceErrorMessage);

    /**
     * Called by the view when the user asks for the view tio be closed.
     */
    public void notifyClose();
}

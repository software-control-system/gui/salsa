package fr.soleil.salsa.client.controller.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.client.controller.event.ControllerEvent;
import fr.soleil.salsa.client.view.ErrorView;

/**
 * Controller for the view that displays error messages. The error window is a non model dialog box
 * with multi thread support. If error messages are added before the user closed the window, the new
 * messages are displayed on the same window after the previous one.
 */
public class ErrorController extends AController<ErrorView> implements IErrorController {

    /**
     * Separator between two errors.
     */
    private static final String SEPARATOR = "  ----------\n";

    /**
     * The list of errors.
     */
    private final List<String> errorsList = new ArrayList<String>();

    /**
     * Is the view visible. The error window is displayed on two conditions : - the viewVisible
     * property is true. - there is an error message to be displayed.
     */
    private boolean viewVisible = false;

    /**
     * Index of the last message that was displayed, so that a message may never be shown twice. -1
     * means that no messages were displayed.
     */
    private int messageShownIndex = -1;

    /**
     * Constructor.
     */
    public ErrorController() {
        this(null);
    }

    /**
     * Constructor.
     */
    public ErrorController(ErrorView view) {
        super(view);
    }

    @Override
    protected ErrorView generateView() {
        return new ErrorView(this);
    }

    @Override
    public synchronized void addErrorMessage(String errorMessage) {
        if (errorMessage != null && !"".equals(errorMessage.trim())) {
            if (!errorMessage.endsWith(SEPARATOR)) {
                errorMessage += SEPARATOR;
            }
            if (!errorsList.contains(errorMessage)) {
                errorsList.add(errorMessage);
            }
            if (viewVisible) {
                if (view != null) {
                    view.display(errorMessage);
                }
            }
        }
    }

    @Override
    public synchronized void addErrorPreferenceMessage(String preferenceErrorMessage) {
        if (preferenceErrorMessage != null && !"".equals(preferenceErrorMessage.trim())) {
            if (!preferenceErrorMessage.endsWith(SEPARATOR)) {
                preferenceErrorMessage += SEPARATOR;
            }
            if (!errorsList.contains(preferenceErrorMessage)) {
                errorsList.add(preferenceErrorMessage);
            }
            if (viewVisible) {
                if (view != null) {
                    view.display(preferenceErrorMessage);
                }
            }
        }
    }

    /**
     * Called by the view when the user asks for the view tio be closed.
     */
    public void notifyClose() {
        messageShownIndex = errorsList.size() - 1;
        if (view != null) {
            view.close();
        }
    }

    /**
     * Sets the view visible or invisible.
     */
    @Override
    public void setViewVisible(boolean visible) {
        fireEvent(visible ? ControllerEvent.VIEW_WILL_BE_DISPLAYED
                : ControllerEvent.VIEW_WILL_BE_HIDDEN);
        viewVisible = visible;
        if (view != null) {
            view.setVisible(visible && messageShownIndex < errorsList.size() - 1);
            if (visible && !errorsList.isEmpty()) {
                view.display(getErrorsText());
            }
        }
        fireEvent(visible ? ControllerEvent.VIEW_IS_DISPLAYED : ControllerEvent.VIEW_IS_HIDDEN);
    }

    /**
     * Builds the error text.
     * 
     * @return
     */
    public String getErrorsText() {
        StringBuilder errorsTextBuilder = new StringBuilder();
        Iterator<String> errorsIterator = errorsList.iterator();
        for (int skipIndex = 0; skipIndex <= messageShownIndex; ++skipIndex) {
            errorsIterator.next();
        }
        if (errorsIterator.hasNext()) {
            errorsTextBuilder.append(errorsIterator.next());
        }
        while (errorsIterator.hasNext()) {
            errorsTextBuilder.append(SEPARATOR);
            errorsTextBuilder.append(errorsIterator.next());
        }
        return errorsTextBuilder.toString();
    }
}

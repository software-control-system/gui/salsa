package fr.soleil.salsa.client.controller.event;

import java.util.EventObject;

import fr.soleil.salsa.client.controller.IController;

public class ControllerEvent extends EventObject {

    private static final long serialVersionUID = -4271103506354466088L;

    /**
     * Represents the fact that the {@link IController}'s view is about to be displayed
     */
    public final static int VIEW_WILL_BE_DISPLAYED = 0;
    /**
     * Represents the fact that the {@link IController}'s view is displayed
     */
    public final static int VIEW_IS_DISPLAYED = 1;
    /**
     * Represents the fact that the {@link IController}'s view is about to be hidden
     */
    public final static int VIEW_WILL_BE_HIDDEN = 2;
    /**
     * Represents the fact that the {@link IController}'s view is hidden
     */
    public final static int VIEW_IS_HIDDEN = 3;

    private int eventType;

    public ControllerEvent(IController<?> source, int eventType) {
        super(source);
        this.eventType = eventType;
    }

    @Override
    public IController<?> getSource() {
        return (IController<?>) super.getSource();
    }

    /**
     * Returns the type of this event
     * 
     * @return An int
     * @see #VIEW_WILL_BE_DISPLAYED
     * @see #VIEW_IS_DISPLAYED
     * @see #VIEW_WILL_BE_HIDDEN
     * @see #VIEW_IS_HIDDEN
     */
    public int getEventType() {
        return eventType;
    }

}

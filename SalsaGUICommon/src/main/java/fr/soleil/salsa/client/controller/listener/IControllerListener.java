package fr.soleil.salsa.client.controller.listener;

import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.client.controller.event.ControllerEvent;

/**
 * An {@link IController} listens to an {@link IController} to know when it does a change in its
 * view visibility
 * 
 * @author girardot
 */
public interface IControllerListener {

    /**
     * Notifies this listener that an {@link IController} is made a change in its view visibility
     * 
     * @param event The associated {@link ControllerEvent}
     */
    public void controllerViewChanged(ControllerEvent event);

}

package fr.soleil.salsa.client.util;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 * A {@link TableCellEditor} that can edit cells containing {@link Number}s with
 * one click
 * 
 * @author girardot
 * 
 * @param <T>
 *            The type of {@link Number}
 */
public abstract class CellNumberEditor<T extends Number> extends DefaultCellEditor implements TableCellEditor {

    private static final long serialVersionUID = -9069647733598493746L;
    private String currentValue = null;

    public CellNumberEditor() {
        super(new JTextField());
        clickCountToStart = 1;
        getComponent().setHorizontalAlignment(JTextField.RIGHT);
        getComponent().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent arg0) {
                BackgroundRenderer.setBackgroundCellEditor(((JTextField) arg0.getSource()), currentValue);
            }

        });

        getComponent().addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent e) {
                BackgroundRenderer.setBackgroundCellEditor(((JTextField) e.getSource()), currentValue);
            }

            @Override
            public void focusGained(FocusEvent e) {
                getComponent().selectAll();
            }
        });
    }

    @Override
    public JTextField getComponent() {
        return (JTextField) super.getComponent();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getCellEditorValue() {
        Object object = super.getCellEditorValue();
        T value = null;
        if (object != null) {
            currentValue = object.toString();
            if (object instanceof NumberValue) {
                ((NumberValue) object).setCurrentValue(getComponent().getText());
                value = (T) object;
            } else {
                value = parseNumber(getComponent().getText());
            }
        }
        return value;
    }

    protected abstract T parseNumber(String value);

}

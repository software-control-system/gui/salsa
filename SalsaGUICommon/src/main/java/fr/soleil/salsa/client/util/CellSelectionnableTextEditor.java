package fr.soleil.salsa.client.util;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;

import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.impl.DeviceImpl;

public class CellSelectionnableTextEditor extends DefaultCellEditor {

    private static final long serialVersionUID = -3267344941957636182L;
    private Object currentValue = null;

    public CellSelectionnableTextEditor() {
        super(new JTextField());
        final JTextField textField = (JTextField) editorComponent;
        textField.setEditable(true);
        textField.setOpaque(true);

        textField.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent arg0) {
            }

            @Override
            public void focusGained(FocusEvent arg0) {
                JTextField editor = (JTextField) arg0.getSource();
                editor.selectAll();
            }
        });
    }


    @Override
    public Object getCellEditorValue() {
        Object superValue = super.getCellEditorValue();
        DeviceImpl newValue = null;
        if ((superValue != null) && (currentValue instanceof IDevice)) {
            newValue = new DeviceImpl();
            newValue.setName(superValue.toString());
            newValue.setCommon(((IDevice) currentValue).isCommon());
            newValue.setEnabled(((IDevice) currentValue).isEnabled());
        }
        return newValue;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        currentValue = value;
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

}

package fr.soleil.salsa.client.view;

import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import fr.soleil.salsa.client.view.component.SimpleNode;
import fr.soleil.salsa.client.view.component.ValueMenuItem;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestionItem;
import fr.soleil.salsa.tool.SalsaUtils;

public class NewDeviceMenu extends JPanel {

    private static final long serialVersionUID = 1190917499915414606L;

    private JButton newButton = null;
    private JButton suggestionToggleButton = null;
    private JPopupMenu suggestionPopup = null;
    private JMenuItem newMultiple = null;
    private NewDeviceMenuListener listener = null;

    /**
     * Action listener for the popup menu.
     */
    private final ActionListener menuActionListener = e -> {
        ValueMenuItem smi = (ValueMenuItem) e.getSource();
        String value = smi.getValue();
        if (listener != null) {
            listener.deviceAdded(value);
        }
    };

    public NewDeviceMenu(final NewDeviceMenuListener listener) {
        FlowLayout layout = new FlowLayout();
        layout.setHgap(0);
        setLayout(layout);
        this.listener = listener;
        // setMaximumSize(new Dimension(60, 40));
        // setFloatable(false);
        add(getNewButton());
        add(getNewSuggestionButton());
    }

    /***
     * Return the button "new" for create a new element
     * 
     * @return
     */
    public JButton getNewButton() {
        if (newButton == null) {
            newButton = new JButton();
            newButton.setText("New");
            newButton.setText("New device");
            newButton.setEnabled(false);
            newButton.addActionListener(e -> {
                if (listener != null) {
                    listener.deviceAdded("");
                }
            });
        }
        return newButton;
    }

    @Override
    public void setEnabled(boolean enabled) {
        getNewButton().setEnabled(enabled);
        getNewSuggestionButton().setEnabled(enabled);
        super.setEnabled(enabled);
    }

    public JButton getNewSuggestionButton() {
        if (suggestionToggleButton == null) {
            suggestionToggleButton = new JButton(Icons.getIcon("salsa.scanconfig.down.small"));
            suggestionToggleButton.setEnabled(false);
            Insets margin = suggestionToggleButton.getMargin();
            margin.left = 0;
            margin.right = 0;
            suggestionToggleButton.setMargin(margin);
            suggestionToggleButton.addActionListener(e -> {
                getPopupSuggestion().show(suggestionToggleButton, 0, suggestionToggleButton.getHeight());
            });
        }
        return suggestionToggleButton;
    }

    public JPopupMenu getPopupSuggestion() {
        if (suggestionPopup == null) {
            suggestionPopup = new JPopupMenu();
            suggestionPopup.add(getNewMultipleDevice());
        }
        return suggestionPopup;
    }

    public JMenuItem getNewMultipleDevice() {
        if (newMultiple == null) {
            newMultiple = new JMenuItem("New ...");
            newMultiple.setToolTipText("Add multiple device");
            newMultiple.addActionListener(e -> {
                showDeviceListTextArea();
            });
        }
        return newMultiple;
    }

    private void showDeviceListTextArea() {
        DeviceListTexteArea.showFrame(this);
        List<String> deviceList = DeviceListTexteArea.getDeviceList();
        if (SalsaUtils.isFulfilled(deviceList) && (listener != null)) {
            listener.devicesAdded(deviceList);
        }
    }

    private void setSuggestions(SimpleNode menuRoot) {
        getPopupSuggestion().removeAll();
        // Note : Only a 2 level of depth.
        // So, we don't use a recursive method.
        getPopupSuggestion().add(getNewMultipleDevice());
        List<SimpleNode> firstLevelMenus = menuRoot.getChildren();
        for (SimpleNode sn : firstLevelMenus) {
            JMenu category = new JMenu(sn.getLabel());
            List<SimpleNode> secondLevelMenus = sn.getChildren();
            for (SimpleNode subMenuNode : secondLevelMenus) {
                ValueMenuItem smi = new ValueMenuItem();
                smi.setValue(subMenuNode.getValue());
                smi.setText(subMenuNode.getLabel());
                smi.addActionListener(menuActionListener);
                category.add(smi);
            }
            getPopupSuggestion().add(category);
        }
    }

    public void setSuggestionList(List<ISuggestionCategory> list) {
        SimpleNode rootNode = convertSuggestionsToTree(list);
        setSuggestions(rootNode);
    }

    private SimpleNode convertSuggestionsToTree(List<ISuggestionCategory> list) {
        SimpleNode root = new SimpleNode(null, "");
        if (list != null) {
            for (ISuggestionCategory c : list) {
                SimpleNode cNode = new SimpleNode(null, c.getLabel());
                if (c.getSuggestionList() != null) {
                    for (ISuggestionItem a : c.getSuggestionList()) {
                        SimpleNode aNode = new SimpleNode(a.getValue(), a.getLabel());
                        cNode.getChildren().add(aNode);
                    }
                    root.getChildren().add(cNode);
                }
            }
        }
        return root;
    }

}

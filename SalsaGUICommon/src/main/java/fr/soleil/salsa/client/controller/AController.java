package fr.soleil.salsa.client.controller;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.client.controller.event.ControllerEvent;
import fr.soleil.salsa.client.controller.listener.IControllerListener;
import fr.soleil.salsa.client.view.IView;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesPersistence;

/**
 * This class gives some common implementation of {@link IController}
 * 
 * @author girardot
 * 
 * @param <T>
 *            The type of {@link IView}
 */
public abstract class AController<T extends IView<?>> implements IController<T> {

    protected final List<WeakReference<IControllerListener>> listenerList = new ArrayList<WeakReference<IControllerListener>>();

    protected T view;
    protected DevicePreferences devicePreferences;
    protected boolean readOnly = false;

    public AController(T view) {
        this(view, false);
    }

    public AController(T view, boolean ro) {
        this.readOnly = ro;
        this.view = null;
        devicePreferences = null;
        initBeforeSetView();
        setView(view == null ? generateView() : view);
    }

    protected void initBeforeSetView() {
        // nothing to do by default
    }

    /**
     * Creates a view. Used in initialization
     * 
     * @return An {@link IView}
     */
    protected abstract T generateView();

    @Override
    public void addControllerListener(IControllerListener listener) {
        ArrayList<WeakReference<IControllerListener>> toRemove = new ArrayList<WeakReference<IControllerListener>>();
        boolean canAdd = (listener == null ? false : true);
        synchronized (listenerList) {
            for (WeakReference<IControllerListener> ref : listenerList) {
                IControllerListener temp = ref.get();
                if (temp == null) {
                    toRemove.add(ref);
                } else if (canAdd && (temp.equals(listener))) {
                    canAdd = false;
                }
            }
            listenerList.removeAll(toRemove);
            if (canAdd) {
                listenerList.add(new WeakReference<IControllerListener>(listener));
            }
        }
        toRemove.clear();
    }

    @Override
    public void removeControllerListener(IControllerListener listener) {
        ArrayList<WeakReference<IControllerListener>> toRemove = new ArrayList<WeakReference<IControllerListener>>();
        synchronized (listenerList) {
            for (WeakReference<IControllerListener> ref : listenerList) {
                IControllerListener temp = ref.get();
                if ((temp == null) || (temp.equals(listener))) {
                    toRemove.add(ref);
                }
            }
            listenerList.removeAll(toRemove);
        }
        toRemove.clear();
    }

    /**
     * Notifies all this {@link IController}'s {@link IControllerListener}s
     * about an event
     * 
     * @param eventType
     *            The type of {@link ControllerEvent}
     */
    protected void fireEvent(int eventType) {
        ArrayList<WeakReference<IControllerListener>> toRemove = new ArrayList<WeakReference<IControllerListener>>();
        ControllerEvent event = new ControllerEvent(this, eventType);
        synchronized (listenerList) {
            for (WeakReference<IControllerListener> ref : listenerList) {
                IControllerListener temp = ref.get();
                if (temp == null) {
                    toRemove.add(ref);
                } else {
                    temp.controllerViewChanged(event);
                }
            }
            listenerList.removeAll(toRemove);
        }
        toRemove.clear();
    }

    @Override
    public void setViewVisible(boolean visible) {
        fireEvent(visible ? ControllerEvent.VIEW_WILL_BE_DISPLAYED : ControllerEvent.VIEW_WILL_BE_HIDDEN);
        if (view != null) {
            view.setVisible(visible);
        }
        fireEvent(visible ? ControllerEvent.VIEW_IS_DISPLAYED : ControllerEvent.VIEW_IS_HIDDEN);
    }

    @Override
    public T getView() {
        return view;
    }

    @Override
    public void setView(T view) {
        if (this.view != null) {
            this.view.setController(null);
        }
        this.view = view;
    }

    @Override
    public DevicePreferences getDevicePreferences() {
        if (devicePreferences == null) {
            devicePreferences = DevicePreferencesPersistence.getSystemPreferences();
        }
        return devicePreferences;
    }

    @Override
    public void setDevicePreferences(DevicePreferences preferences) {
        this.devicePreferences = preferences;
    }

    protected String getScanServer() {
        // use getDevicePreferences() to recover from system if necessary
        return (getDevicePreferences() == null ? null : devicePreferences.getScanServer());
    }

}

package fr.soleil.salsa.client.util;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

public class TrajectoryCellRenderer extends JTextArea implements TableCellRenderer {
    private static final long serialVersionUID = 6636417464247322266L;

    public TrajectoryCellRenderer() {
        setLineWrap(true);
        setWrapStyleWord(true);
        setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }
        setFont(table.getFont());
        if (hasFocus) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if (table.isCellEditable(row, column)) {
                setForeground(UIManager.getColor("Table.focusCellForeground"));
                setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        }
        else {
            setBorder(new EmptyBorder(1, 2, 1, 2));
        }
        if (value != null) {
            String[] nbLine = value.toString().split("\n");
            table.setRowHeight(row, nbLine.length * table.getRowHeight());
            String trajectory = "";
            for (String line : nbLine) {
                String[] actuator = line.split("/");
                if (actuator.length == 1) {
                    trajectory = trajectory + actuator[0] + "\n";
                }
                else if (actuator.length == 4) {
                    trajectory = trajectory + actuator[3] + "\n";
                }
            }

            setText(trajectory);

            setToolTipText(value.toString());
        }
        else {
            setText("");
        }

        return this;
    }
}
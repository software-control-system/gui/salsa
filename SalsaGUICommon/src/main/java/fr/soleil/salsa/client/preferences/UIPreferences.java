package fr.soleil.salsa.client.preferences;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.ScanProperties;
import fr.soleil.salsa.client.preferences.UIPreferencesEvent.UIPreferenceEventType;
import fr.soleil.salsa.exception.SalsaPreferencesException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * A class that represents Salsa's preferences
 * 
 * @author Tarek, Rapha&euml;l GIRARDOT
 */
public class UIPreferences {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(UIPreferences.class);

    /** Control panel property (to recover control panel when not set */
    private static final String CONTROL_PANEL_PROPERTY = "ControlPanel";
    /** Control panel property, with first letter to lower case, (to recover control panel when not set */
    private static final String ALTERNATE_CONTROL_PANEL_PROPERTY = "controlPanel";

    /** Control panel recovered from properties */
    public static final String DEFAULT_CONTROL_PANEL;
    /** The singleton instance of preferences */
    private static final UIPreferences PREFERENCES;
    static {
        String controlPanel;
        try {
            controlPanel = System.getProperty(CONTROL_PANEL_PROPERTY,
                    System.getProperty(ALTERNATE_CONTROL_PANEL_PROPERTY));
        } catch (Exception e) {
            LOGGER.error("Failed to recover device displaying application", e);
            controlPanel = null;
        }
        DEFAULT_CONTROL_PANEL = controlPanel;
        UIPreferences preferences = null;
        try {
            preferences = UIPreferencesPersistence.loadSystemPreferences();
        } catch (SalsaPreferencesException e) {
            LOGGER.error("Unable to get instance of UI preferences: " + e.getMessage(), e);
        }
        if (preferences == null) {
            preferences = new UIPreferences(ObjectUtils.EMPTY_STRING);
        }
        PREFERENCES = preferences;
    }

    /** Path to preferences file */
    private final String preferencesFile;

    /** Refresh time */
    private Integer refreshTime;

    /** True if enable confirmation */
    private boolean enableConfirmation;

    /** Name of the last used perspective (it's used for loading the last perspective). */
    private String lastPerspectiveName;

    /** True if docking window is locked */
    private boolean isDockingWindowLocked = false;

    /** X position of the window */
    private String xWindowPosition;

    /** Y position of the window */
    private String yWindowPosition;

    /** Width of the window */
    private String windowWidth;

    /** Height of the window */
    private String windowHeight;

    /** The divider location for scan result bean */
    private int dividerLocation = 600;

    /** The command line for the control panel. */
    private String controlPanel;

    /** The command line for the databrowser. */
    private String dataBrowser = "databrowser";

    /** True if allow display image as spectrum */
    private String showImageAsSpectrumStack = "false";

    /** True if allow send values to sensors */
    private String isSendSensor = "false";

    private final Map<String, ScanProperties> currentScanPropertiesMap = new HashMap<>();
    private final Map<String, ChartProperties> chartPropertiesMap = new HashMap<>();
    private final Map<String, PlotProperties> plotPropertiesMap = new HashMap<>();
    private final Map<String, ImageProperties> imagePropertiesMap = new HashMap<>();
    private final Map<String, Map<String, String>> bookmarkMap = new HashMap<>();

    private Collection<WeakReference<UIPreferencesListener>> listeners = new ArrayList<>();

    public static UIPreferences getInstance() {
        return PREFERENCES;
    }

    protected UIPreferences(String preferenceFile) {
        lastPerspectiveName = ObjectUtils.EMPTY_STRING;
        refreshTime = null;
        this.preferencesFile = preferenceFile;
    }

    public String getPreferencesFile() {
        return preferencesFile;
    }

    public Map<String, Map<String, String>> getBookmarks() {
        return bookmarkMap;
    }

    public Map<String, String> addBookmarks(String bookmark) {
        Map<String, String> configMap = bookmarkMap.get(bookmark);
        if (configMap == null) {
            configMap = new HashMap<>();
            bookmarkMap.put(bookmark, configMap);
        }
        return configMap;
    }

    public void removeBookmark(String bookmark) {
        Map<String, String> configMap = bookmarkMap.get(bookmark);
        if (configMap != null) {
            configMap.clear();
            bookmarkMap.remove(bookmark);
        }
    }

    public void addEntryToBookmarks(String bookmark, String configPath, String customLabel) {
        Map<String, String> configMap = addBookmarks(bookmark);
        if (configMap != null) {
            if (!configMap.containsKey(configPath)) {
                configMap.put(configPath, customLabel);
            }
        }
    }

    public void replaceEntryInBookmarks(String configPath, String newConfigPath) {
        Set<String> bookmarkKeySet = bookmarkMap.keySet();
        for (String bookmarkKey : bookmarkKeySet) {
            Map<String, String> configMap = bookmarkMap.get(bookmarkKey);
            if ((configMap != null) && configMap.containsKey(configPath)) {
                String value = configMap.get(configPath);
                configMap.remove(configPath);
                configMap.put(newConfigPath, value);
            }
        }
    }

    public void removeEntryFromBookmark(String bookmark, String configPath) {
        Map<String, String> configMap = bookmarkMap.get(bookmark);
        if (configMap != null) {
            configMap.remove(configPath);
        }
    }

    public void removeAllEntryFromBookmark(String configPath) {
        Set<String> bookmarkKeySet = bookmarkMap.keySet();
        for (String bookmarkKey : bookmarkKeySet) {
            removeEntryFromBookmark(bookmarkKey, configPath);
        }
    }

    /**
     * Gets true if enable confirmation is enabled.
     * 
     * @return
     */
    public boolean isEnableConfirmation() {
        return enableConfirmation;
    }

    /**
     * Sets true if enable confirmation is enabled.
     * 
     * @param enableConfirmation
     */
    public void setEnableConfirmation(boolean enableConfirmation) {
        this.enableConfirmation = enableConfirmation;
        notifyListeners(UIPreferenceEventType.ENABLECONFIRMATION_CHANGED);
    }

    public Integer getRefreshTime() {
        return refreshTime;
    }

    /**
     * Notifies all {@link UIPreferencesListener}s for some changes
     */
    private void notifyListeners(UIPreferenceEventType eventType) {
        ArrayList<WeakReference<UIPreferencesListener>> toRemove = new ArrayList<>();
        UIPreferencesEvent event = new UIPreferencesEvent(this, eventType);
        synchronized (listeners) {
            for (WeakReference<UIPreferencesListener> reference : listeners) {
                UIPreferencesListener tempListener = reference.get();
                if (tempListener == null) {
                    toRemove.add(reference);
                } else {
                    tempListener.preferenceChanged(event);
                }
            }
            listeners.removeAll(toRemove);
        }
        toRemove.clear();
    }

    public void addPreferenceListener(UIPreferencesListener listener) {
        if (listener != null) {
            boolean canAdd = true;
            ArrayList<WeakReference<UIPreferencesListener>> toRemove = new ArrayList<>();
            synchronized (listeners) {
                for (WeakReference<UIPreferencesListener> reference : listeners) {
                    UIPreferencesListener tempListener = reference.get();
                    if (tempListener == null) {
                        toRemove.add(reference);
                    } else if (tempListener.equals(listener)) {
                        canAdd = false;
                    }
                }
                if (canAdd) {
                    listeners.add(new WeakReference<UIPreferencesListener>(listener));
                }
                listeners.removeAll(toRemove);
            }
            toRemove.clear();
        }
    }

    public void removePreferenceListener(UIPreferencesListener listener) {
        if (listener != null) {
            ArrayList<WeakReference<UIPreferencesListener>> toRemove = new ArrayList<>();
            synchronized (listeners) {
                for (WeakReference<UIPreferencesListener> reference : listeners) {
                    UIPreferencesListener tempListener = reference.get();
                    if ((tempListener == null) || (tempListener.equals(listener))) {
                        toRemove.add(reference);
                    }
                }
                listeners.removeAll(toRemove);
            }
            toRemove.clear();
        }
    }

    public void removeAllPreferenceListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    public void setRefreshTime(int refreshTime) {
        this.refreshTime = refreshTime;
        IDataSourceProducer producer = DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        // Do not supporte the refreshing service under 1000 ms
        if (refreshTime > 1000) {
            if (producer instanceof AbstractRefreshingManager<?>) {
                ((AbstractRefreshingManager<?>) producer)
                        .setDefaultRefreshingStrategy(new PolledRefreshingStrategy(refreshTime));
            }
        }
        notifyListeners(UIPreferenceEventType.REFRESHTIME_CHANGED);

    }

    /**
     * Get the last perspective name.
     * 
     * @return lastPerspectiveName last perspective name
     */
    public String getLastPerspectiveName() {
        return lastPerspectiveName;
    }

    /**
     * Set the last perspective name.
     */
    public void setLastPerspectiveName(String lastPerspectiveName) {
        this.lastPerspectiveName = lastPerspectiveName;
        notifyListeners(UIPreferenceEventType.LAST_PERSPECTIVE_CHANGED);
    }

    /**
     * Return true if docking window locked
     * 
     * @return true if docking window locked
     */
    public boolean isDockingWindowLocked() {
        return isDockingWindowLocked;
    }

    /**
     * Set true if docking window locked
     * 
     * @param isDockingWindowLocked true if docking window locked
     */
    public void setDockingWindowLocked(boolean isDockingWindowLocked) {
        this.isDockingWindowLocked = isDockingWindowLocked;
        notifyListeners(UIPreferenceEventType.IS_DOCKING_WINDOW_LOCKED_CHANGED);
    }

    /**
     * Get X position of the window.
     * 
     * @return
     */
    public String getXWindowPosition() {
        return xWindowPosition;
    }

    /**
     * Set X position of the window.
     * 
     * @param windowPosition
     */
    public void setXWindowPosition(String windowPosition) {
        xWindowPosition = windowPosition;
    }

    /**
     * Get Y position of the window.
     * 
     * @return
     */
    public String getYWindowPosition() {
        return yWindowPosition;
    }

    /**
     * Set X position of the window.
     * 
     * @param windowPosition
     */
    public void setYWindowPosition(String windowPosition) {
        yWindowPosition = windowPosition;
    }

    /**
     * Get width of the window.
     * 
     * @return
     */
    public String getWindowWidth() {
        return windowWidth;
    }

    /**
     * Set width of the window.
     * 
     * @param windowWidth
     */
    public void setWindowWidth(String windowWidth) {
        this.windowWidth = windowWidth;
    }

    /**
     * Get height of the window.
     * 
     * @return
     */
    public String getWindowHeight() {
        return windowHeight;
    }

    /**
     * Set height of the window.
     * 
     * @param windowHeight
     */
    public void setWindowHeight(String windowHeight) {
        this.windowHeight = windowHeight;
    }

    /**
     * The command line for the control panel.
     * 
     * @return
     */
    public String getControlPanel() {
        return controlPanel;
    }

    /**
     * The command line for the control panel.
     * 
     * @param controlPanel
     */
    public void setControlPanel(String controlPanel) {
        this.controlPanel = SalsaUtils.isDefined(controlPanel) ? controlPanel : DEFAULT_CONTROL_PANEL;
        notifyListeners(UIPreferenceEventType.CONTROL_PANEL_CHANGED);
    }

    public String getDataBrowser() {
        return dataBrowser;
    }

    public void setDataBrowser(String dataBrowser) {
        this.dataBrowser = dataBrowser;
        notifyListeners(UIPreferenceEventType.DATABROWSER_CHANGED);
    }

    public void showImageAsSpectrumStack(String v) {
        this.showImageAsSpectrumStack = v;
    }

    public String isShowImageAsSpectrumStack() {
        return this.showImageAsSpectrumStack;
    }

    public String isSendSensor() {
        return isSendSensor;
    }

    public void sendSensor(String isSendSensor) {
        this.isSendSensor = isSendSensor;
    }

    public Map<String, ChartProperties> getChartPropertiesMap() {
        return chartPropertiesMap;
    }

    public void setChartPropertiesMap(Map<String, ChartProperties> propMap) {
        if (SalsaUtils.isFulfilled(propMap)) {
            Set<Entry<String, ChartProperties>> entrySet = propMap.entrySet();
            for (Entry<String, ChartProperties> entry : entrySet) {
                addChartPropertiesMap(entry.getKey(), entry.getValue());
            }
        }
    }

    public void addChartPropertiesMap(String dataId, ChartProperties chartProperties) {
        if (SalsaUtils.isDefined(dataId) && (chartProperties != null)) {
            chartPropertiesMap.put(dataId.toLowerCase(), chartProperties);
        }
    }

    public Map<String, ImageProperties> getImagePropertiesMap() {
        return imagePropertiesMap;
    }

    public void setImagePropertiesMap(Map<String, ImageProperties> propMap) {
        if (propMap != null) {
            imagePropertiesMap.putAll(propMap);
        }
    }

    public void addImagePropertiesMap(String dataId, ImageProperties imageProperties) {
        if (SalsaUtils.isDefined(dataId) && (imageProperties != null)) {
            imagePropertiesMap.put(dataId.toLowerCase(), imageProperties);
        }
    }

    public Map<String, ScanProperties> getCurrentScanPropertiesMap() {
        return currentScanPropertiesMap;
    }

    public void setCurrentScanPropertiesMap(Map<String, ScanProperties> currentScanPropertiesMap) {
        if (currentScanPropertiesMap != null) {
            this.currentScanPropertiesMap.putAll(currentScanPropertiesMap);
        }
    }

    public void addCurrentScanProperties(ScanProperties currentScanProperties) {
        if (currentScanProperties != null) {
            this.currentScanPropertiesMap.put(currentScanProperties.getRunName(), currentScanProperties);
        }
    }

    public Map<String, PlotProperties> getPlotPropertiesMap() {
        return plotPropertiesMap;
    }

    public void setPlotPropertiesMap(Map<String, PlotProperties> propMap) {
        if (SalsaUtils.isFulfilled(propMap)) {
            Set<Entry<String, PlotProperties>> entrySet = propMap.entrySet();
            for (Entry<String, PlotProperties> entry : entrySet) {
                addPlotPropertiesMap(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * The Id is the label of scan server attribute
     * 
     * @param dataViewId
     * @param plotProperties
     */
    public void addPlotPropertiesMap(String dataViewId, PlotProperties plotProperties) {
        if (SalsaUtils.isDefined(dataViewId)) {
            PlotProperties oldProp = getPlotProperties(dataViewId);
            if (plotProperties == null) {
                plotProperties = oldProp;
            } else {
                // Do not change the axis information
                if (oldProp != null) {
                    int oldAxis = oldProp.getAxisChoice();
                    plotProperties.setAxisChoice(oldAxis);
                }
            }
            plotProperties.getMarker().setLegendVisible(true);
            plotPropertiesMap.put(dataViewId.toLowerCase(), plotProperties);
        }
    }

    private PlotProperties getPlotProperties(String dataViewId) {
        PlotProperties properties = null;
        if (SalsaUtils.isDefined(dataViewId)) {
            properties = plotPropertiesMap.get(dataViewId.toLowerCase());
            if (properties == null) {
                properties = new PlotProperties();
                properties.getMarker().setStyle(IChartViewer.MARKER_DOT);
                // XXX check why this axis choice
                properties.setAxisChoice(-1);
            }
        }
        return properties;
    }

    public int getDividerLocation() {
        return dividerLocation;
    }

    public void setDividerLocation(int dividerLocation) {
        this.dividerLocation = dividerLocation;
    }
}

package fr.soleil.salsa.client.util;

/**
 * 
 * can edit the cell type Integer
 * 
 * @author Administrateur
 * 
 */
public class CellIntegerEditor extends CellNumberEditor<Integer> {

    private static final long serialVersionUID = 2690168925414399434L;

    public CellIntegerEditor() {
        super();
    }

    @Override
    protected Integer parseNumber(String value) {
        return Integer.valueOf(value);
    }

}

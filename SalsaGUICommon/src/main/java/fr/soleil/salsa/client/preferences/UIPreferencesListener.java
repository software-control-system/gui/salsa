package fr.soleil.salsa.client.preferences;

import java.util.EventListener;

/**
 * A listener that listens to {@link UIPreferences}
 * 
 * @author alike
 */
public interface UIPreferencesListener extends EventListener {

    /**
     * Notifies this listener for a change in {@link UIPreferences}
     * 
     * @param event The {@link UIPreferencesEvent} that identifies the change
     */
    public void preferenceChanged(UIPreferencesEvent event);

}

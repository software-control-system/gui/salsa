package fr.soleil.salsa.client.view.component;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Locale;

import javax.swing.text.DefaultCaret;

import fr.soleil.comete.swing.TextField;
import fr.soleil.lib.project.Format;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Simple field for Double values. Based on Comete TextField.
 */
public class DoubleField extends TextField {

    private static final long serialVersionUID = -4053630171120068042L;

    public static final String DEFAULT_FORMAT = "%6.5f";

    /**
     * The current value.
     */
    private Double value;

    /**
     * The format used to display and read the value.
     */
    private String format = null;

    /**
     * Constructor.
     * 
     * @param value
     * @param format
     */
    public DoubleField(Double value, String format) {
        // State
        this.format = format == null ? DEFAULT_FORMAT : format;
        this.value = value;

        this.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                formatText();
            }

        });

        // Layout
        this.setHorizontalAlignment(DoubleField.LEADING);
    }

    /**
     * Constructor. If nullAllowed is true, value is null by default. If nullAllowed is false, value
     * is new Double(0d).
     * 
     * @param nullAllowed
     */
    public DoubleField(String format) {
        this(null, format);
    }

    /**
     * Constructor. Value is null and nullAllowed is true by default.
     */
    public DoubleField() {
        this(DEFAULT_FORMAT);
    }

    public void setFormat(String format) {
        if (SalsaUtils.isDefined(format)) {
            this.format = format;
        } else {
            this.format = DEFAULT_FORMAT;
        }
        formatText();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        formatText();
        super.actionPerformed(e);
    }

    /**
     * Ensures that the text is ALWAYS a number.
     */
    private void formatText() {
        String text = "";
        if ((value != null) && !value.isNaN()) {
            text = value.toString();
            if (Format.isNumberFormatOK(format) && !format.equals(DEFAULT_FORMAT)) {
                text = Format.format(Locale.US, format, value);
            } else if (text.length() > 5) {
                text = Format.format(Locale.US, DEFAULT_FORMAT, value);
            }
        }
        super.setText(text);
    }

    /**
     * Sets the text of this <code>TextComponent</code> to the specified text. If the text is <code>null</code> or
     * empty, has the effect of simply deleting the old text. When text has
     * been inserted, the resulting caret location is determined by the implementation of the caret
     * class.
     * <p>
     * This method is thread safe, although most Swing methods are not. Please see <A
     * HREF="http://java.sun.com/docs/books/tutorial/uiswing/misc/threads.html">How to Use Threads</A> for more
     * information.
     * 
     * Note that text is not a bound property, so no <code>PropertyChangeEvent
     * </code> is fired when it changes. To listen for changes to the text, use <code>DocumentListener</code>.
     * 
     * If the parameter is null, the value will be null or 0d depending on nullAllowed. If the parameter cannot be
     * converted into a number using the current format, the value will be unchanged.
     * 
     * @param text the new text to be set
     * @see #getText
     * @see DefaultCaret
     * @beaninfo description: the text of this component
     */
    @Override
    public void setText(String text) {
        if (text != null) {
            try {
                // value = format.parse(text).doubleValue();
                value = Double.valueOf(text);
            } catch (Exception exception) {
                value = null;
            }
        } else {
            setValue(null);
        }
        formatText();
    }

    /**
     * Returns the value.
     * 
     * @return
     */
    public Double getValue() {
        return value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     */
    public void setValue(Double value) {
        this.value = value;
        formatText();
    }

}

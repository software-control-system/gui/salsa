package fr.soleil.salsa.client.util;

public class NumberValue extends Number {

    private static final long serialVersionUID = -3446360361455044685L;

    private String dbValue = null;

    private String currentValue = null;

    private Integer integerValue = null;

    private Double doubleValue = null;

    private Float floatValue = null;

    private Long longValue = null;

    public void setIntegerValue(Integer integerValue) {
        this.integerValue = integerValue;
        currentValue = String.valueOf(integerValue);
    }

    public void setDoubleValue(Double doubleValue) {
        this.doubleValue = doubleValue;
        currentValue = String.valueOf(doubleValue);
    }

    public void setFloatValue(Float floatValue) {
        this.floatValue = floatValue;
        currentValue = String.valueOf(floatValue);
    }

    public void setLongValue(Long longValue) {
        this.longValue = longValue;
        currentValue = String.valueOf(longValue);
    }

    public double getDoubleDbValue() {
        if (dbValue != null) {
            try {
                return Double.parseDouble(dbValue);
            }
            catch (Exception e) {
                return Double.NaN;
            }
        }
        return Double.NaN;
    }

    public String getDbValue() {
        return dbValue;
    }

    public void setDbValue(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
        if (integerValue != null) {
            try {
                integerValue = intValue();
            }
            catch (Exception e) {
            }
        }
        else if (doubleValue != null) {
            setDoubleValue(doubleValue());
        }
    }

    @Override
    public String toString() {
        return currentValue;
    }

    @Override
    public double doubleValue() {
        if (doubleValue != null) {
            return doubleValue;
        }
        else if (currentValue != null) {
            try {
                return Double.parseDouble(currentValue);
            }
            catch (Exception e) {
                return Double.NaN;
            }
        }
        return Double.NaN;
    }

    @Override
    public float floatValue() {
        if (floatValue != null) {
            return floatValue;
        }
        else if (currentValue != null) {
            try {
                return Float.parseFloat(currentValue);
            }
            catch (Exception e) {
                return Float.NaN;
            }
        }
        return Float.NaN;
    }

    @Override
    public int intValue() {
        if (integerValue != null) {
            return integerValue;
        }
        else if (currentValue != null) {
            try {
                return Integer.parseInt(currentValue);
            }
            catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    @Override
    public long longValue() {
        if (longValue != null) {
            return longValue;
        }
        else if (currentValue != null) {
            try {
                return Long.parseLong(currentValue);
            }
            catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

}

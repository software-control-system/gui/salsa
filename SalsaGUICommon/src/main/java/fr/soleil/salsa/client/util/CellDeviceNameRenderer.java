package fr.soleil.salsa.client.util;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;

import org.jdesktop.swingx.renderer.DefaultTableRenderer;

import fr.soleil.salsa.entity.IDevice;

/**
 * 
 * @author Administrateur
 * 
 */
public class CellDeviceNameRenderer extends DefaultTableRenderer {

    private static final long serialVersionUID = 3344579826535109234L;

    public CellDeviceNameRenderer() {
        super();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                column);
        boolean redColor = false;
        if ((value == null) || value.toString().trim().isEmpty()) {
            redColor = true;
            if (rendererComponent instanceof JLabel) {
                ((JLabel) rendererComponent).setText("Undefined device name !!");
            }
        }

        if ((value instanceof IDevice) && !redColor) {
            redColor = !((IDevice) value).isCommon();
        }

        if (redColor) {
            rendererComponent.setForeground(Color.RED);
        }
        return rendererComponent;
    }

}
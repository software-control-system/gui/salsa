package fr.soleil.salsa.client.util;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

/**
 * Custom renderer for the cells of the table.
 */
public class CellToolTypeRenderer extends JTextField implements TableCellRenderer {

    private static final long serialVersionUID = -611727321027951879L;

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {

        setBorder(null);
        if (isSelected) {
            setBackground(jTable.getSelectionBackground());
        }
        else {
            setBackground(jTable.getBackground());
        }
        setToolTipText(value.toString());
        setText(value == null ? "" : value.toString());
        return this;
    }
}

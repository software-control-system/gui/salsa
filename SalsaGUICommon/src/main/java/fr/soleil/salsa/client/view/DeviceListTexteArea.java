package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.AbstractTableModel;

public class DeviceListTexteArea extends JDialog {

    private static final long serialVersionUID = -6868701340686743289L;
    private static DeviceListTexteArea instance = null;
    private static JPanel mainPanel = new JPanel();
    private static JPanel textAreaPanel = new JPanel();
    private static JTextArea textArea = new JTextArea();
    private static JButton okButton = new JButton("Apply");
    private static JButton clearButton = new JButton("Cancel");
    private static JButton closeButton = new JButton("OK");
    private static JTable lineNumber = new JTable();
    private static List<String> deviceList = new ArrayList<String>();

    private static AbstractTableModel tableModel = new AbstractTableModel() {

        private static final long serialVersionUID = 4807669709436264035L;

        @Override
        public Object getValueAt(int row, int col) {
            return row;
        }

        @Override
        public int getRowCount() {
            return deviceList.size();
        }

        @Override
        public int getColumnCount() {
            return 1;
        }
    };

    private DeviceListTexteArea() {
        setTitle("Device list editor");
    }

    private static void initGUI() {
        textAreaPanel.setLayout(new BorderLayout());
        textAreaPanel.add(textArea, BorderLayout.CENTER);
        textAreaPanel.add(lineNumber, BorderLayout.WEST);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setSize(500, 600);
        JScrollPane scrollPane = new JScrollPane(textAreaPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(clearButton);
        buttonPanel.add(closeButton);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                parseValue();
            }
        });

        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                clearInfo();
                instance.setVisible(false);
            }
        });

        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                parseValue();
                instance.setVisible(false);
            }
        });

        lineNumber.setEnabled(false);
        lineNumber.setBackground(Color.LIGHT_GRAY);
        lineNumber.setModel(tableModel);
    }

    private static void clearInfo() {
        deviceList.clear();
        tableModel.fireTableStructureChanged();
        textArea.setText("");
    }

    private static void setText() {
        StringBuilder builder = new StringBuilder();
        for (String val : deviceList) {
            builder.append(val);
            builder.append("\n");
        }
        textArea.setText(builder.toString());
        tableModel.fireTableStructureChanged();
    }

    public static List<String> getDeviceList() {
        return deviceList;
    }

    private static void parseValue() {
        String text = textArea.getText();
        clearInfo();
        // FIRST REPLACE ALL SEPARATOR WITH \n
        if (text != null) {
            // FIRST REPLACE ALL SEPARATOR WITH \n
            text = text.replaceAll(";", "\n");
            text = text.replaceAll(",", "\n");
            text = text.replaceAll("\t", "\n");
            text = text.replaceAll(" ", "\n");

            // Then replace double \n by one \n
            text = text.replaceAll("\n\n", "\n");

            // Split the text with \n
            String[] stringValue = text.split("\n");

            if (stringValue != null) {
                for (String aString : stringValue) {
                    deviceList.add(aString);
                }
            }
        }

        setText();
    }

    public static void showFrame(Component parent) {
        if (instance == null) {
            instance = new DeviceListTexteArea();
            initGUI();
            instance.setContentPane(mainPanel);
            instance.setSize(mainPanel.getSize());
            instance.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            instance.setLocationRelativeTo(parent);
            instance.setAlwaysOnTop(true);
            instance.setModal(true);
        }

        deviceList.clear();
        tableModel.fireTableStructureChanged();
        textArea.setText("");
        instance.setVisible(true);
        instance.toFront();

    }

    public static void main(String[] args) {
        DeviceListTexteArea.showFrame(null);
    }
}

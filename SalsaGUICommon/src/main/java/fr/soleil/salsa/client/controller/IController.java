package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.controller.listener.IControllerListener;
import fr.soleil.salsa.client.view.IView;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * Base interface for the controllers. Controllers creates and manipulates
 * views. They are responsible for events affecting them.
 * 
 * @author Administrateur
 * 
 */
public interface IController<T extends IView<?>> {

    /**
     * Shows or hides the view.
     * 
     * @param visible
     */
    public void setViewVisible(boolean visible);

    /**
     * Returns the view.
     * 
     * @return an {@link IView}
     */
    public T getView();

    /**
     * Changes this controller's view
     * 
     * @param view
     *            The {@link IView} to set
     */
    public void setView(T view);

    /**
     * Adds an {@link IControllerListener} to this controller
     * 
     * @param listener
     *            The {@link IControllerListener} to add
     */
    public void addControllerListener(IControllerListener listener);

    /**
     * Removes an {@link IControllerListener} from this controller
     * 
     * @param listener
     *            The {@link IControllerListener} to remove
     */
    public void removeControllerListener(IControllerListener listener);

    /**
     * Returns the {@link DevicePreferences} associated with this
     * {@link IController}
     * 
     * @return Some {@link DevicePreferences}
     */
    public DevicePreferences getDevicePreferences();

    /**
     * Sets the {@link DevicePreferences} this {@link IController} should use
     * 
     * @param preferences
     *            The {@link DevicePreferences} to set
     */
    public void setDevicePreferences(DevicePreferences preferences);

}

package fr.soleil.salsa.client.util;

import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

/***
 * The model table of list
 * 
 * @author Tarek
 * 
 */
public class ModelTableList extends DefaultTableModel {

    private static final long serialVersionUID = 539445954433096952L;
    private final List<Class<?>> columnClass;
    private boolean editable = true;

    /***
     * The number of row
     */
    private static final int ROW = 0;

    /***
     * Creates a new instance of ModelTableList
     * 
     * @param Vector: the names of columns
     */
    public ModelTableList(Vector<String> columnNames, List<Class<?>> columnClass) {
        super(columnNames, ROW);
        this.columnClass = columnClass;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass.get(columnIndex);
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {

        if (aValue instanceof String) {
            aValue = ((String) aValue).trim();
        }
        super.setValueAt(aValue, row, column);
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object value = null;
        if ((row > -1) && (row < this.getRowCount())) {
            value = super.getValueAt(row, column);
        }
        return value;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return ((column == 0) || editable);
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

}

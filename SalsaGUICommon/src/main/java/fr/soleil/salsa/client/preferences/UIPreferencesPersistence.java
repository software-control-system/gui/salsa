package fr.soleil.salsa.client.preferences;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.ImagePropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.PlotPropertiesXmlManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;
import fr.soleil.model.scanserver.ScanProperties;
import fr.soleil.salsa.exception.SalsaMissingPropertyException;
import fr.soleil.salsa.exception.SalsaPreferencesException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Class relative to the persistence of preferences data. It does not save every properties, only "interesting
 * properties".
 * 
 * @author Alike
 */
public class UIPreferencesPersistence {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(UIPreferencesPersistence.class);

    public static final String PREFERENCE_FILE_PROPERTY = "SALSA_UI_PREFERENCES";

    private enum PreferenceItem {
        RefreshTime, EnableConfirmation, DividerLocation, Monochrome, LastPerspective, IsDockingWindowLocked,
        ControlPanel, DataBrowser, ShowImageAsSpectrumStack, SendSensor, XWindowPosition, YWindowPosition, WindowWidth,
        WindowHeight
    }

    protected static UIPreferences loadSystemPreferences() throws SalsaPreferencesException {
        UIPreferences preferences;
        String preferencePath = SystemUtils.getSystemProperty(PREFERENCE_FILE_PROPERTY);
        if (SalsaUtils.isDefined(preferencePath)) {
            preferences = load(preferencePath);
        } else {
            throw new SalsaMissingPropertyException(PREFERENCE_FILE_PROPERTY);
        }
        return preferences;
    }

    /**
     * Loads preferences from file system.
     * 
     * @param preferenceFilePath The path to the preference file
     * @return The corresponding {@link UIPreferences}
     */
    public static UIPreferences load(String preferenceFilePath) {
        UIPreferences uiPreferences = new UIPreferences(preferenceFilePath);
        try {
            DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
            fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ObjectUtils.EMPTY_STRING);
            fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ObjectUtils.EMPTY_STRING);
            DocumentBuilder constructeur = fabrique.newDocumentBuilder();
            File f = new File(preferenceFilePath);
            Document d = constructeur.parse(f);
            NodeList elements = d.getElementsByTagName("add");
            for (int i = 0; i < elements.getLength(); i++) {
                if (elements.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) elements.item(i);
                String k = e.getAttribute("key");
                String v = e.getTextContent();
                if (PreferenceItem.RefreshTime.name().equals(k)) {
                    parseInteger(k, v).ifPresent(uiPreferences::setRefreshTime);
                } else if (PreferenceItem.EnableConfirmation.name().equals(k)) {
                    uiPreferences.setEnableConfirmation(Boolean.parseBoolean(v));
                } else if (PreferenceItem.DividerLocation.name().equals(k)) {
                    parseInteger(k, v).ifPresent(uiPreferences::setDividerLocation);
                } else if (PreferenceItem.LastPerspective.name().equals(k)) {
                    uiPreferences.setLastPerspectiveName(v);
                } else if (PreferenceItem.IsDockingWindowLocked.name().equals(k)) {
                    uiPreferences.setDockingWindowLocked(Boolean.parseBoolean(v));
                } else if (PreferenceItem.ControlPanel.name().equals(k)) {
                    uiPreferences.setControlPanel(v);
                } else if (PreferenceItem.DataBrowser.name().equals(k)) {
                    uiPreferences.setDataBrowser(v);
                } else if (PreferenceItem.ShowImageAsSpectrumStack.name().equals(k)) {
                    uiPreferences.showImageAsSpectrumStack(v);
                } else if (PreferenceItem.SendSensor.name().equals(k)) {
                    uiPreferences.sendSensor(v);
                } else if (PreferenceItem.XWindowPosition.name().equals(k)) {
                    uiPreferences.setXWindowPosition(v);
                } else if (PreferenceItem.YWindowPosition.name().equals(k)) {
                    uiPreferences.setYWindowPosition(v);
                } else if (PreferenceItem.WindowWidth.name().equals(k)) {
                    uiPreferences.setWindowWidth(v);
                } else if (PreferenceItem.WindowHeight.name().equals(k)) {
                    uiPreferences.setWindowHeight(v);
                }
            }
            loadBookmarks(uiPreferences, d);
            loadPlotProperties(uiPreferences, d);
            loadImageProperties(uiPreferences, d);
            loadChartProperties(uiPreferences, d);
            loadScanConfigurations(uiPreferences, d);
        }

        catch (ParserConfigurationException | SAXException e) {
            LOGGER.error("Invalid UI preference file: {}", e.getMessage());
            LOGGER.trace("Invalid UI preference file", e);
        } catch (FileNotFoundException e) {
            LOGGER.error("UI Preference file not found {}", e.getMessage());
            LOGGER.debug("UI Preference file not found", e);
            LOGGER.info("This file will be created (if allowed).");
            save(uiPreferences);
        } catch (IOException e) {
            LOGGER.error("Unable to load UI preference file {}", e.getMessage());
            LOGGER.trace("Unable to load UI preference file", e);
        }
        return uiPreferences;
    }

    private static void loadBookmarks(UIPreferences uiPreferences, Document d) {
        NodeList bookmarks = d.getElementsByTagName("list");
        for (int i = 0; i < bookmarks.getLength(); i++) {
            Element e = (Element) bookmarks.item(i);
            String bookmark = e.getAttribute("name");
            uiPreferences.addBookmarks(bookmark);
            NodeList childNodes = e.getElementsByTagName("item");
            for (int j = 0; j < childNodes.getLength(); j++) {
                Node child = childNodes.item(j);
                String configPath = ((Element) child).getAttribute("key");
                String value = child.getTextContent();
                uiPreferences.addEntryToBookmarks(bookmark, configPath, value);
            }
        }
    }

    private static void loadPlotProperties(UIPreferences uiPreferences, Document d) {
        try {
            NodeList plotPropertiesNodeList = d.getElementsByTagName("plotproperties");
            if ((plotPropertiesNodeList != null) && (plotPropertiesNodeList.getLength() > 0)) {
                Element plotPropertiesMap = (Element) plotPropertiesNodeList.item(0);
                NodeList childNodes = plotPropertiesMap.getElementsByTagName("item");
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node child = childNodes.item(j);
                    String itemKey = ((Element) child).getAttribute("key");
                    String value = child.getTextContent();
                    Node node = XMLUtils.getRootNodeFromFileContent(value);
                    uiPreferences.addPlotPropertiesMap(itemKey, PlotPropertiesXmlManager.loadPlotProperties(node));
                }
            }
        } catch (XMLWarning e) {
            LOGGER.debug("Unable to load plot properties from UI preferences: {}", e.getMessage());
            LOGGER.trace("Unable to load plot properties from UI preferences", e);
        }
    }

    private static void loadImageProperties(UIPreferences uiPreferences, Document d) {
        try {
            NodeList impagePropertiesNodeList = d.getElementsByTagName("imageproperties");
            if ((impagePropertiesNodeList != null) && (impagePropertiesNodeList.getLength() > 0)) {
                Element imagePropertiesMap = (Element) impagePropertiesNodeList.item(0);
                NodeList childNodes = imagePropertiesMap.getElementsByTagName("item");
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node child = childNodes.item(j);
                    String itemKey = ((Element) child).getAttribute("key");
                    String value = child.getTextContent();
                    Node node = XMLUtils.getRootNodeFromFileContent(value);
                    uiPreferences.addImagePropertiesMap(itemKey, ImagePropertiesXmlManager.loadImageProperties(node));
                }
            }
        } catch (XMLWarning e) {
            LOGGER.debug("Unable to load image properties from UI preferences: {}", e.getMessage());
            LOGGER.trace("Unable to load image properties from UI preferences", e);
        }
    }

    private static void loadChartProperties(UIPreferences uiPreferences, Document d) {
        try {
            NodeList chartPropertiesNodeList = d.getElementsByTagName("chartpropertiesmap");
            if ((chartPropertiesNodeList != null) && (chartPropertiesNodeList.getLength() > 0)) {
                Element chartElement = (Element) chartPropertiesNodeList.item(0);
                NodeList childNodes = chartElement.getElementsByTagName("item");
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node child = childNodes.item(j);
                    String itemKey = ((Element) child).getAttribute("key");
                    String value = child.getTextContent();
                    Node node = XMLUtils.getRootNodeFromFileContent(value);
                    uiPreferences.addChartPropertiesMap(itemKey, ChartPropertiesXmlManager.loadChartProperties(node));
                }
            }
        } catch (XMLWarning e) {
            LOGGER.debug("Unable to load chart properties from UI preferences: {}", e.getMessage());
            LOGGER.trace("Unable to load chart properties from UI preferences", e);
        }
    }

    private static void loadScanConfigurations(UIPreferences uiPreferences, Document d) {
        try {
            NodeList scanNodeList = d.getElementsByTagName("scanconfiglist");
            String runName = null;
            if ((scanNodeList != null) && (scanNodeList.getLength() > 0)) {
                Element scanElements = (Element) scanNodeList.item(0);
                NodeList runNameNodes = scanElements.getElementsByTagName("item");
                ScanProperties prop = null;
                if ((runNameNodes != null) && (runNameNodes.getLength() > 0)) {
                    for (int j = 0; j < runNameNodes.getLength(); j++) {
                        Node child = runNameNodes.item(j);
                        Element propertyElement = ((Element) child);
                        runName = propertyElement.getAttribute("key");
                        prop = new ScanProperties(runName);

                        NodeList chartNode = propertyElement.getElementsByTagName("chart");
                        if ((chartNode != null) && (chartNode.getLength() > 0)) {
                            Element chartElement = (Element) chartNode.item(0);
                            String xmlChart = chartElement.getTextContent();
                            Node node = XMLUtils.getRootNodeFromFileContent(xmlChart);
                            prop.setChartProperties(ChartPropertiesXmlManager.loadChartProperties(node));
                        }

                        NodeList y1NodeList = propertyElement.getElementsByTagName("y1List");
                        if ((y1NodeList != null) && (y1NodeList.getLength() > 0)) {
                            prop.setY1AttributeList(getAttributeList((Element) y1NodeList.item(0)));
                        }

                        NodeList y2NodeList = propertyElement.getElementsByTagName("y2List");
                        if ((y2NodeList != null) && (y2NodeList.getLength() > 0)) {
                            prop.setY2AttributeList(getAttributeList((Element) y2NodeList.item(0)));
                        }

                        NodeList zNodeList = propertyElement.getElementsByTagName("zList");
                        if ((zNodeList != null) && (zNodeList.getLength() > 0)) {
                            prop.setZAttributeList(getAttributeList((Element) zNodeList.item(0)));
                        }

                        NodeList xNode = propertyElement.getElementsByTagName("xAttribute");
                        if ((xNode != null) && (xNode.getLength() > 0)) {
                            prop.setXAttribute(((Element) xNode.item(0)).getAttribute("name"));
                        }
                        uiPreferences.addCurrentScanProperties(prop);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Unable to load scan configurations from UI preferences: {}", e.getMessage());
            LOGGER.trace("Unable to load scan configurations from UI preferences", e);
        }
    }

    private static Optional<Integer> parseInteger(String k, String v) {
        if (SalsaUtils.isDefined(v)) {
            try {
                return Optional.of(Integer.parseInt(v));
            } catch (NumberFormatException e) {
                LOGGER.debug("Cannot convert \"{}\" to integer for preference key {}: {}", v, k, e.getMessage());
                LOGGER.trace("Cannot convert \"{}\" to integer for preference key {}", v, k, e);
            }
        } else {
            LOGGER.debug("The value is empty for preference key {}", k);
        }
        return Optional.empty();
    }

    private static List<String> getAttributeList(Element element) {
        List<String> attributeList = null;
        if (element != null) {
            NodeList nodeList = element.getElementsByTagName("attribute");
            Element attributeElement = null;
            if ((nodeList != null) && (nodeList.getLength() > 0)) {
                attributeList = new ArrayList<>();
                for (int j = 0; j < nodeList.getLength(); j++) {
                    attributeElement = ((Element) nodeList.item(j));
                    attributeList.add(attributeElement.getAttribute("name"));
                }
            }
        }

        return attributeList;
    }

    /**
     * Save {@link UIPreferences} on file system.
     * 
     * @param uiPreferences The preferences to save
     */
    public static void save(UIPreferences uiPreferences) {
        if (uiPreferences != null) {
            try {
                DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
                fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ObjectUtils.EMPTY_STRING);
                fabrique.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ObjectUtils.EMPTY_STRING);
                DocumentBuilder constructeur = fabrique.newDocumentBuilder();

                Document document = constructeur.newDocument();
                Element root = document.createElement("settings");

                document.appendChild(root);

                HashMap<String, String> hashMap = new HashMap<>();

                hashMap.put(PreferenceItem.EnableConfirmation.name(),
                        Boolean.toString(uiPreferences.isEnableConfirmation()));
                hashMap.put(PreferenceItem.DividerLocation.name(), String.valueOf(uiPreferences.getDividerLocation()));
                Integer refreshTime = uiPreferences.getRefreshTime();
                hashMap.put(PreferenceItem.RefreshTime.name(),
                        refreshTime != null ? refreshTime.toString() : ObjectUtils.EMPTY_STRING);
                hashMap.put(PreferenceItem.LastPerspective.name(), uiPreferences.getLastPerspectiveName());
                hashMap.put(PreferenceItem.IsDockingWindowLocked.name(),
                        Boolean.toString(uiPreferences.isDockingWindowLocked()));
                hashMap.put(PreferenceItem.ControlPanel.name(), uiPreferences.getControlPanel());
                hashMap.put(PreferenceItem.DataBrowser.name(), uiPreferences.getDataBrowser());
                hashMap.put(PreferenceItem.XWindowPosition.name(), uiPreferences.getXWindowPosition());
                hashMap.put(PreferenceItem.ShowImageAsSpectrumStack.name(), uiPreferences.isShowImageAsSpectrumStack());
                hashMap.put(PreferenceItem.SendSensor.name(), uiPreferences.isSendSensor());
                hashMap.put(PreferenceItem.YWindowPosition.name(), uiPreferences.getYWindowPosition());
                hashMap.put(PreferenceItem.WindowWidth.name(), uiPreferences.getWindowWidth());
                hashMap.put(PreferenceItem.WindowHeight.name(), uiPreferences.getWindowHeight());

                List<String> keys = new ArrayList<>(hashMap.keySet());
                Collections.sort(keys, null);
                for (String key : keys) {
                    String value = hashMap.get(key);
                    Element e = document.createElement("add");
                    e.setAttribute("key", key);
                    e.setTextContent(value);
                    root.appendChild(e);
                }
                hashMap.clear();

                Map<String, Map<String, String>> bookmarksMap = uiPreferences.getBookmarks();
                if (SalsaUtils.isFulfilled(bookmarksMap)) {
                    Element bookmarks = document.createElement("bookmarks");
                    root.appendChild(bookmarks);
                    Set<Entry<String, Map<String, String>>> entrySet = bookmarksMap.entrySet();
                    String key = null;
                    Map<String, String> bookmark = null;
                    Set<Entry<String, String>> bookmarkEntrySet = null;
                    String label = null;
                    String itemKey = null;
                    for (Entry<String, Map<String, String>> entry : entrySet) {
                        key = entry.getKey();
                        bookmark = entry.getValue();
                        Element e = document.createElement("list");
                        e.setAttribute("name", key);
                        bookmarkEntrySet = bookmark.entrySet();
                        for (Entry<String, String> bookmarkEntry : bookmarkEntrySet) {
                            itemKey = bookmarkEntry.getKey();
                            label = bookmarkEntry.getValue();
                            Element itemElement = document.createElement("item");
                            itemElement.setAttribute("key", itemKey);
                            itemElement.setTextContent(label);
                            e.appendChild(itemElement);
                        }
                        bookmarks.appendChild(e);
                    }
                }

                Map<String, PlotProperties> plotPropertiesMap = uiPreferences.getPlotPropertiesMap();
                if (SalsaUtils.isFulfilled(plotPropertiesMap)) {
                    Element plotPropertiesElement = document.createElement("plotproperties");
                    root.appendChild(plotPropertiesElement);
                    String plotXml = null;
                    String key = null;
                    for (Map.Entry<String, PlotProperties> entry : plotPropertiesMap.entrySet()) {
                        key = entry.getKey();
                        Element itemElement = document.createElement("item");
                        itemElement.setAttribute("key", key);
                        plotXml = PlotPropertiesXmlManager.toXmlString(entry.getValue());
                        itemElement.setTextContent(plotXml);
                        plotPropertiesElement.appendChild(itemElement);
                    }
                }

                Map<String, ImageProperties> imagePropertiesMap = uiPreferences.getImagePropertiesMap();
                if (SalsaUtils.isFulfilled(imagePropertiesMap)) {
                    Element imagePropertiesElement = document.createElement("imageproperties");
                    root.appendChild(imagePropertiesElement);
                    String key = null;
                    String imageXml = null;
                    ImageProperties prop = null;
                    for (Map.Entry<String, ImageProperties> entry : imagePropertiesMap.entrySet()) {
                        key = entry.getKey();
                        prop = entry.getValue();
                        if (prop != null) {
                            Element itemElement = document.createElement("item");
                            itemElement.setAttribute("key", key);
                            imageXml = ImagePropertiesXmlManager.toXmlString(prop);
                            itemElement.setTextContent(imageXml);
                            imagePropertiesElement.appendChild(itemElement);
                        }
                    }
                }

                Map<String, ChartProperties> chartPropertiesMap = uiPreferences.getChartPropertiesMap();
                if (SalsaUtils.isFulfilled(chartPropertiesMap)) {
                    Element chartPropertiesElement = document.createElement("chartpropertiesmap");
                    root.appendChild(chartPropertiesElement);
                    String chartXml = null;
                    String key = null;
                    ChartProperties prop = null;
                    for (Map.Entry<String, ChartProperties> entry : chartPropertiesMap.entrySet()) {
                        key = entry.getKey();
                        prop = entry.getValue();
                        if (prop != null) {
                            Element itemElement = document.createElement("item");
                            itemElement.setAttribute("key", key);
                            chartXml = ChartPropertiesXmlManager.toXmlString(prop);
                            itemElement.setTextContent(chartXml);
                            chartPropertiesElement.appendChild(itemElement);
                        }
                    }
                }

                Map<String, ScanProperties> scanPropertiesMap = uiPreferences.getCurrentScanPropertiesMap();
                if (SalsaUtils.isFulfilled(scanPropertiesMap)) {
                    Element scanConfigElement = document.createElement("scanconfiglist");
                    root.appendChild(scanConfigElement);
                    ScanProperties prop = null;
                    String key = null;
                    String chartXml = null;
                    for (Map.Entry<String, ScanProperties> entry : scanPropertiesMap.entrySet()) {
                        key = entry.getKey();
                        prop = entry.getValue();
                        if (prop != null) {
                            Element itemElement = document.createElement("item");
                            itemElement.setAttribute("key", key);

                            if (prop.getChartProperties() != null) {
                                Element chartElement = document.createElement("chart");
                                chartXml = ChartPropertiesXmlManager.toXmlString(prop.getChartProperties());
                                chartElement.setTextContent(chartXml);
                                itemElement.appendChild(chartElement);
                            }

                            Element y1Element = createAxisElement(document, prop.getY1AttributeList(), "y1List");
                            if (y1Element != null) {
                                itemElement.appendChild(y1Element);
                            }

                            Element y2Element = createAxisElement(document, prop.getY2AttributeList(), "y2List");
                            if (y2Element != null) {
                                itemElement.appendChild(y2Element);
                            }

                            Element zElement = createAxisElement(document, prop.getZAttributeList(), "zList");
                            if (zElement != null) {
                                itemElement.appendChild(zElement);
                            }

                            String xAttribute = prop.getXAttribute();
                            if (SalsaUtils.isDefined(xAttribute)) {
                                Element xElement = document.createElement("xAttribute");
                                xElement.setAttribute("name", xAttribute);
                                itemElement.appendChild(xElement);
                            }

                            scanConfigElement.appendChild(itemElement);
                        }
                    }
                }

                write(uiPreferences, xmlToString(document));
            } catch (IOException | TransformerException | ParserConfigurationException e) {
                LOGGER.error("Unable to write UI preference file {}", e.getMessage());
                LOGGER.trace("Unable to write UI preference file", e);
            }
        }
    }

    private static void write(UIPreferences uiPreferences, String xmlString) throws IOException {
        File f = new File(uiPreferences.getPreferencesFile());
        try (FileWriter fw = new FileWriter(f); BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(xmlString);
            bw.flush();
        }
    }

    private static Element createAxisElement(Document document, List<String> attributeNameList, String elementName) {
        Element element = null;
        if (SalsaUtils.isFulfilled(attributeNameList)) {
            element = document.createElement(elementName);
            for (String attributeName : attributeNameList) {
                Element attributeElement = document.createElement("attribute");
                attributeElement.setAttribute("name", attributeName);
                element.appendChild(attributeElement);
            }
        }
        return element;
    }

    private static String xmlToString(Document document) throws TransformerException {
        DOMSource domSource = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult sr = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ObjectUtils.EMPTY_STRING);
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, ObjectUtils.EMPTY_STRING);
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, sr);
        return writer.toString();
    }

}

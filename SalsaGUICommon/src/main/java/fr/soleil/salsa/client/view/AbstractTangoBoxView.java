package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.client.controller.impl.AbstractTangoBoxController;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Tango box view
 */
public class AbstractTangoBoxView extends JPanel implements IView<AbstractTangoBoxController> {
    /** Serialization id */
    private static final long serialVersionUID = 3088652927409397421L;

    /** The controller */
    protected transient AbstractTangoBoxController controller;

    /** The bean panel */
    protected JPanel bean;

    /** The bean model */
    private String beanModel = null;

    /**
     * Create a new instance
     */
    public AbstractTangoBoxView() {
        this(null);
    }

    /**
     * Create a new instance with controller
     * 
     * @param controller the controller
     */
    public AbstractTangoBoxView(AbstractTangoBoxController controller) {
        super(new BorderLayout());
        this.controller = controller;
        bean = null;
    }

    /**
     * Set the bean panel
     * 
     * @param bean the bena panel
     */
    public void setBean(JPanel bean) {
        if (this.bean != null) {
            remove(this.bean);
        }
        this.bean = bean;
        addBean();
        updateBeanVisibility(isVisible());
    }

    private void addBean() {
        if (bean != null) {
            add(bean, BorderLayout.CENTER);
        }
    }

    @Override
    public AbstractTangoBoxController getController() {
        return controller;
    }

    @Override
    public void setController(AbstractTangoBoxController controller) {
        this.controller = controller;
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        updateBeanVisibility(visible);
    }

    public void updateBeanVisibility(boolean visible) {
        if (bean instanceof AbstractTangoBox) {
            bean.setVisible(visible);
            if (visible) {
                if (SalsaUtils.isDefined(beanModel) && TangoDeviceHelper.isDeviceRunning(beanModel)) {
                    ((AbstractTangoBox) bean).setModel(beanModel);
                    ((AbstractTangoBox) bean).start();
                }
            } else {
                ((AbstractTangoBox) bean).setModel(null);
                ((AbstractTangoBox) bean).stop();
            }
        }
    }

    /**
     * Set bean model
     * 
     * @param model the model
     */
    public void setBeanModel(String model) {
        if (SalsaUtils.isDefined(model)) {
            beanModel = model.trim();
            updateBeanVisibility(true);
        } else {
            beanModel = null;
            updateBeanVisibility(false);
        }
    }

    /**
     * Return true if bean model is set
     * 
     * @return true if bean model is set
     */
    public boolean isSetBeanModel() {
        return beanModel != null;
    }
}

package fr.soleil.salsa.client.util;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JTextField;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.bean.ISalsaBean;
import fr.soleil.salsa.tool.SalsaUtils;

public class BackgroundRenderer {

    public static void setBackgroundField(JTextField field, String actualValue, String dbValue) {
        if (field != null) {
            String text = field.getText();
            field.setBackground(Color.WHITE);
            if (SalsaUtils.isDefined(text) && SalsaUtils.isDefined(dbValue) && SalsaUtils.isDefined(actualValue)) {
                try {
                    double fieldValue = Double.valueOf(text);
                    double actualDoubleValue = Double.valueOf(actualValue);
                    double dbDoubleValue = Double.valueOf(dbValue);
                    if (fieldValue != actualDoubleValue) {
                        field.setBackground(ISalsaBean.NOTAPPLY_COLOR);
                    } else if (actualDoubleValue != dbDoubleValue) {
                        field.setBackground(ISalsaBean.MODIFY_COLOR);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        }
    }

    public static void setBackgroundTextField(JTextField field, String actualValue, String dbValue) {
        if (field != null) {
            String text = field.getText();
            if (text == null) {
                text = ObjectUtils.EMPTY_STRING;
            }
            if (actualValue == null) {
                actualValue = ObjectUtils.EMPTY_STRING;
            }
            if (dbValue == null) {
                dbValue = ObjectUtils.EMPTY_STRING;
            }
            field.setBackground(Color.WHITE);
            if (!text.trim().equalsIgnoreCase(actualValue.trim())) {
                field.setBackground(ISalsaBean.NOTAPPLY_COLOR);
            } else if (!actualValue.trim().equalsIgnoreCase(dbValue.trim())) {
                field.setBackground(ISalsaBean.MODIFY_COLOR);
            }

        }
    }

    public static void setBackgroundCellEditor(JTextField field, String actualValue) {
        if (field != null) {
            String text = field.getText();
            field.setBackground(Color.WHITE);
            if (SalsaUtils.isDefined(text) && SalsaUtils.isDefined(actualValue)) {
                double fieldValue = Double.valueOf(text);
                double actualDoubleValue = Double.valueOf(actualValue);
                if (fieldValue != actualDoubleValue) {
                    field.setBackground(ISalsaBean.NOTAPPLY_COLOR);
                }
            } else {
                field.setBackground(ISalsaBean.NOTAPPLY_COLOR);
            }
        }
    }

    public static void setBackgroundCellRenderer(JLabel field, String actualValue) {
        if (field != null) {
            String text = field.getText();
            field.setBackground(Color.WHITE);
            if (SalsaUtils.isDefined(text) && SalsaUtils.isDefined(actualValue)) {
                double fieldValue = Double.valueOf(text);
                double actualDoubleValue = Double.valueOf(actualValue);
                if (fieldValue != actualDoubleValue) {
                    field.setBackground(ISalsaBean.MODIFY_COLOR);
                }
            } else {
                field.setBackground(ISalsaBean.MODIFY_COLOR);
            }
        }

    }

}

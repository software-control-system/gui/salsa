package fr.soleil.salsa.client.util;



public class RowObject {

    private String deviceName;
    
    private Boolean booDevice;

    public RowObject(){
        this.deviceName = "";
        this.booDevice = new Boolean(false);
    }
    
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Boolean getBooDevice() {
        return booDevice;
    }

    public void setBooDevice(Boolean booDevice) {
        this.booDevice = booDevice;
    }
}

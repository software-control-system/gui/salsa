package fr.soleil.salsa.client.preferences;

import java.util.EventObject;

/***
 * @author Tarek
 */
public class UIPreferencesEvent extends EventObject {

    private static final long serialVersionUID = 4703631168095295609L;

    public enum UIPreferenceEventType {
        REFRESHTIME_CHANGED, ENABLECONFIRMATION_CHANGED, DATABROWSER_CHANGED, LAST_PERSPECTIVE_CHANGED, IS_DOCKING_WINDOW_LOCKED_CHANGED, X_WINDOW_POSITION_CHANGED, Y_WINDOW_POSITION_CHANGED, WINDOW_WIDTH_CHANGED, WINDOW_HEIGHT_CHANGED, CONTROL_PANEL_CHANGED
    }

    protected UIPreferenceEventType type = UIPreferenceEventType.REFRESHTIME_CHANGED;

    public UIPreferencesEvent(UIPreferences source, UIPreferenceEventType type) {
        super(source);
        this.type = type;
    }

    @Override
    public UIPreferences getSource() {
        return (UIPreferences) super.getSource();
    }

    /**
     * @return Returns the type.
     */
    public UIPreferenceEventType getType() {
        return type;
    }
}

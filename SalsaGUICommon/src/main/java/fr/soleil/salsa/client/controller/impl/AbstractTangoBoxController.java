package fr.soleil.salsa.client.controller.impl;

import javax.swing.JPanel;

import com.google.common.eventbus.Subscribe;

import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.client.view.AbstractTangoBoxView;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.preferences.DevicePreferencesEvent.Type;
import fr.soleil.salsa.tool.EventBusHandler;

/**
 * Abstract Controller for TangoBox
 */
public abstract class AbstractTangoBoxController extends AController<AbstractTangoBoxView>
        implements IController<AbstractTangoBoxView> {

    /**
     * Create a new instance
     * 
     * @param bean the comete bean
     * @param view the TangoBox view
     */
    AbstractTangoBoxController(JPanel bean, AbstractTangoBoxView view) {
        super(view);
        if (this.view != null) {
            this.view.setBean(bean);
        }
        EventBusHandler.getEventBus().register(this);
    }

    /**
     * Create a new instance
     * 
     * @param bean the comete bean
     */
    AbstractTangoBoxController(JPanel bean) {
        this(bean, null);
    }

    @Override
    protected AbstractTangoBoxView generateView() {
        return new AbstractTangoBoxView(this);
    }

    @Override
    public void setDevicePreferences(DevicePreferences preferences) {
        super.setDevicePreferences(preferences);
        stateChanged(new DevicePreferencesEvent(preferences, Type.LOADING));
    }

    /**
     * Invoked when a device preference event occurs in the event bus
     * 
     * @param devicePreferencesEvent the device preferences events
     */
    @Subscribe
    public abstract void stateChanged(DevicePreferencesEvent devicePreferencesEvent);
}

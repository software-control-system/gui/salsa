package fr.soleil.salsa.client.util;

/**
 * 
 * can edit the cell type Double
 * 
 * @author Administrateur
 * 
 */
public class CellNumberValueEditor extends CellNumberEditor<NumberValue> {

    private static final long serialVersionUID = -9198969222646336949L;

    public CellNumberValueEditor() {
        super();
    }

    @Override
    protected NumberValue parseNumber(String value) {
        NumberValue newValue = new NumberValue();
        newValue.setCurrentValue(value);
        return newValue;
    }

}

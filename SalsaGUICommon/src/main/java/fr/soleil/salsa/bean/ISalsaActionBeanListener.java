package fr.soleil.salsa.bean;

import fr.soleil.salsa.exception.SalsaException;

public interface ISalsaActionBeanListener extends ISalsaBeanListener {

    public static enum Action {
        START, STOP, PAUSE, RESUME, NONE
    }

    public void actionPerformed(Action action);

    public void executedActionSucceed(Action action);

    public void executedActionFailed(Action action, SalsaException exception);

}

package fr.soleil.salsa.bean;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.preferences.UIPreferences;

public class BookmarkComboConfigList extends JComboBox<ConfigItem> implements IConfigSelectionComponent {

    private static final long serialVersionUID = 6439475750670469651L;

    private IConfigSelectionListener listener = null;
    private String bookmark = null;
    private Map<String, Map<String, String>> bookmarks = null;
    private boolean load = false;

    @SuppressWarnings("unchecked")
    public BookmarkComboConfigList() {
        setEditable(false);
        bookmarks = UIPreferences.getInstance().getBookmarks();
        setRenderer(new ConfigItemRenderer());
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (arg0.getModifiers() != 0) {
                    notifyListener();
                }
            }
        });
    }

    private void notifyListener() {
        if ((listener != null)) {
            Object object = getSelectedItem();
            if ((object != null) && (object instanceof ConfigItem)) {
                listener.configPathChanged(((ConfigItem) object).getKey());
            }
        }
    }

    public boolean isLoad() {
        return load;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
        if (load) {
            unloadBookmark();
            loadBookmark();
        }
    }

    @Override
    public void addConfigSelectionListener(IConfigSelectionListener listener) {
        this.listener = listener;
        // notifyListener();
    }

    @Override
    public void removeConfigSelectionListener(IConfigSelectionListener listener) {
        listener = null;
    }

    public void loadBookmark() {
        load = true;
        if ((bookmark != null) && (bookmarks != null)) {
            Map<String, String> configList = bookmarks.get(bookmark);
            if (configList != null) {
                Set<Entry<String, String>> entrySet = configList.entrySet();
                for (Entry<String, String> entry : entrySet) {
                    addItem(new ConfigItem(entry.getKey(), entry.getValue()));
                }
            }
            // notifyListener();
        }
    }

    public void unloadBookmark() {
        load = false;
        removeAllItems();
    }

    private class ConfigItemRenderer extends BasicComboBoxRenderer {

        private static final long serialVersionUID = 8666621346246438932L;

        @Override
        public Component getListCellRendererComponent(@SuppressWarnings("unchecked") JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
                if (-1 < index) {
                    if ((value != null) && (value instanceof ConfigItem)) {
                        list.setToolTipText(((ConfigItem) value).getKey());
                    }
                }
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(list.getFont());
            setText((value == null) ? ObjectUtils.EMPTY_STRING : value.toString());
            return this;
        }
    }

    public static void main(String[] args) {
        BookmarkComboConfigList combo = new BookmarkComboConfigList();
        JFrame frame = new JFrame();
        frame.setContentPane(combo);
        combo.setBookmark("Liste1");
        combo.addConfigSelectionListener(new IConfigSelectionListener() {
            @Override
            public void configPathChanged(String configPath) {
                System.out.println("configPath=" + configPath);
            }
        });
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        combo.loadBookmark();
    }

}

class ConfigItem {
    private String value;
    private String key;

    public ConfigItem(String key, String value) {
        super();
        this.value = value;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return value;
    }

}

package fr.soleil.salsa.bean;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.exception.SalsaException;

public interface ISalsaBeanListener {

    public void configPathChanged(String configPath);

    public void loadingConfigSucceed(IConfig<?> config);

    public void loadingConfigFailed(String configPath, SalsaException exception);
}

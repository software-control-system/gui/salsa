package fr.soleil.salsa.bean;


public interface IConfigSelectionComponent {

	public void addConfigSelectionListener(IConfigSelectionListener listener);
	
	public void removeConfigSelectionListener(IConfigSelectionListener listener);
}

package fr.soleil.salsa.bean;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.salsa.bean.ISalsaActionBeanListener.Action;
import fr.soleil.salsa.client.view.ActionButton;
import fr.soleil.salsa.client.view.ActionButton.ACTION_TYPE;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.exception.SalsaException;

public abstract class AbstractActionSalsaBean extends AbstractSalsaBean implements ISalsaActionBean, ActionListener {

    private static final long serialVersionUID = -8194178302687784123L;

    private boolean confirmation = false;
    private boolean configReloadedBeforeExecute = false;
    private ActionButton button = null;
    private boolean userEnabled = true;

    public AbstractActionSalsaBean(ACTION_TYPE action) {
        button = new ActionButton(action);
        button.setEnabled(false);
        setLayout(new BorderLayout());
        add(button, BorderLayout.CENTER);
        button.addActionListener(this);
        button.setToolTipText(action.toString());
    }

    public void setTextButton(String text) {
        button.setText(text);
    }

    public void setBackgroundButton(Color background) {
        button.setBackground(background);
    }

    public void setForegroundButton(Color foreground) {
        button.setForeground(foreground);
    }

    @Override
    public void loadConfig() {
        super.loadConfig();
        startListening();
    }

    public boolean isUserEnabled() {
        return userEnabled;
    }

    public void setUserEnabled(boolean userEnabled) {
        this.userEnabled = userEnabled;
        if (!userEnabled) {
            setEnabled(userEnabled);
        }
    }

    @Override
    public Action getAction() {
        Action action = Action.NONE;
        if (button != null) {
            ACTION_TYPE actionType = button.getActionType();
            switch (actionType) {
                case START:
                    action = Action.START;
                    break;
                case STOP:
                    action = Action.STOP;
                    break;
                case RESUME:
                    action = Action.RESUME;
                    break;
                case PAUSE:
                    action = Action.PAUSE;
                    break;
                default:
                    break;
            }
        }
        return action;
    }

    @Override
    public boolean isConfigReloadedBeforeExecute() {
        return configReloadedBeforeExecute;
    }

    @Override
    public void setConfigReloadedBeforeExecute(boolean reload) {
        configReloadedBeforeExecute = reload;
    }

    @Override
    public boolean isConfirmation() {
        return confirmation;
    }

    @Override
    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;

    }

    @Override
    public void setEnabled(boolean enabled) {
        // Must be done in EDT (SCAN-976, PROBLEM-2582)
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            boolean currentEnabled = button.isEnabled();
            boolean newEnabled = true;
            if (userEnabled) {
                newEnabled = enabled;
            } else {
                newEnabled = false;
            }
            if (currentEnabled != newEnabled) {
                button.setEnabled(newEnabled);
            }
        });
    }

    @Override
    public void setBackground(Color arg0) {
        if (button != null) {
            // Must be done in EDT (SCAN-976, PROBLEM-2582)
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                button.setBackground(arg0);
            });
        }
    }

    @Override
    public void setToolTipText(String arg0) {
        if (button != null) {
            // Must be done in EDT (SCAN-976, PROBLEM-2582)
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                button.setToolTipText(arg0);
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        Action action = getAction();
        notifyActionPerformed(action);

        int result = JOptionPane.OK_OPTION;
        if (confirmation) {
            result = JOptionPane.showConfirmDialog(this, getActionMessage(), "Confirmation", JOptionPane.YES_NO_OPTION);
        }
        if (result == JOptionPane.OK_OPTION) {
            try {
                executeAction(getConfig());
                notifyExecutedActionSucceed(action);
            } catch (SalsaException e) {
                notifyExecutedActionFailed(action, e);
            }
        }

    }

    private void notifyActionPerformed(Action action) {
        for (ISalsaBeanListener listener : beanListeners) {
            if (listener instanceof ISalsaActionBeanListener) {
                ((ISalsaActionBeanListener) listener).actionPerformed(action);
            }
        }
    }

    private void notifyExecutedActionSucceed(Action action) {
        for (ISalsaBeanListener listener : beanListeners) {
            if (listener instanceof ISalsaActionBeanListener) {
                ((ISalsaActionBeanListener) listener).executedActionSucceed(action);
            }
        }
    }

    private void notifyExecutedActionFailed(Action action, SalsaException exception) {
        for (ISalsaBeanListener listener : beanListeners) {
            if (listener instanceof ISalsaActionBeanListener) {
                ((ISalsaActionBeanListener) listener).executedActionFailed(action, exception);
            }
        }
    }

    public void setAction(ACTION_TYPE action) {
        if (button != null) {
            button.setActionType(action);
        }
    }

    @Override
    public void stateChanged(String state) {
        super.stateChanged(state);
        setMovingState(CurrentScanDataModel.isMoving(state));
    }

    protected abstract void setMovingState(boolean moving);

    protected abstract void executeAction(IConfig<?> config) throws SalsaException;

    protected abstract String getActionMessage();

}

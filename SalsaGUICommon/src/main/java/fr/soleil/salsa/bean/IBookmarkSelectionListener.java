package fr.soleil.salsa.bean;

public interface IBookmarkSelectionListener {

    public void bookmarksChanged(String bookmarks);
}

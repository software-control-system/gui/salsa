package fr.soleil.salsa.bean;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.api.ScanApi;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class RecordDataPanel extends JPanel {

    private static final long serialVersionUID = -3208357879796965820L;

    public static final Logger LOGGER = LoggingUtil.getLogger(RecordDataPanel.class);
    private JCheckBox recordCheckBox = null;
    private JLabel recordDataLabel = null;
    private static final String ENABLE_DATA_RECORDER_MSG = "Enable Data Recording";

    public RecordDataPanel() {
        super();
        init();
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
    }

    private void init() {
        recordCheckBox = new JCheckBox();
        recordCheckBox.addItemListener(createDataRecorderEnableItemListener());
        recordCheckBox.setOpaque(false);
        recordDataLabel = new JLabel(ENABLE_DATA_RECORDER_MSG);
        updateFromPreferences(SalsaAPI.getDevicePreferences());
        add(recordDataLabel, BorderLayout.EAST);
        add(recordCheckBox, BorderLayout.WEST);
    }

    public void updateFromPreferences(DevicePreferences devicePreferences) {
        try {
            Color colorToUse;
            if (isDataRecorderAvailable()) {
                boolean userWantToUseDataRecorder = devicePreferences.isDataRecorder();
                boolean enabledFromDevice = ScanApi.isDataRecorderEnable();
                boolean enabled = enabledFromDevice && userWantToUseDataRecorder;
                recordCheckBox.setSelected(enabled);
                recordCheckBox.setEnabled(enabled);
                colorToUse = (enabled) ? Color.GREEN : Color.RED;
            } else {
                colorToUse = Color.GRAY;
                recordCheckBox.setSelected(false);
                recordCheckBox.setEnabled(false);
            }
            setBackground(colorToUse);
        } catch (SalsaDeviceException e) {
            LOGGER.error("Cannot setDataRecorderEnable " + e.getMessage(), e);
        }
    }

    private boolean isDataRecorderAvailable() {
        boolean result = false;

        String scanServer = SalsaAPI.getDevicePreferences().getScanServer();

        Database database = TangoDeviceHelper.getDatabase();
        if ((database != null) && SalsaUtils.isDefined(scanServer)) {
            try {
                DbDatum dbDatum = database.get_device_property(scanServer, "DataRecorder");
                if (dbDatum != null) {
                    String dataRecorderDeviceName = dbDatum.extractString();
                    result = TangoDeviceHelper.isDeviceRunning(dataRecorderDeviceName);
                }
            } catch (DevFailed e) {
                LOGGER.error("Cannot read {}/DataRecorder property {}", scanServer, DevFailedUtils.toString(e));
                LOGGER.trace("Stack trace", e);
            }
        }

        return result;
    }

    private ItemListener createDataRecorderEnableItemListener() {
        return e -> {
            boolean enabled = (e.getStateChange() == ItemEvent.SELECTED);
            setSelected(enabled);
        };
    }

    public void setSelected(boolean enable) {
        try {
            Color colorToUse = (enable) ? Color.GREEN : Color.RED;

            ScanApi.setDataRecorderEnable(enable);

            setBackground(colorToUse);
        } catch (Exception ex) {
            LOGGER.error("Cannot setDataRecorderEnable {}", ex.getMessage());
            LOGGER.debug("Stack trace", ex);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        RecordDataPanel button = new RecordDataPanel();
        frame.setContentPane(button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}

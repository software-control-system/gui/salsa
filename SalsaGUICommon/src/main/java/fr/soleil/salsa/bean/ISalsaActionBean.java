package fr.soleil.salsa.bean;

import fr.soleil.salsa.bean.ISalsaActionBeanListener.Action;

public interface ISalsaActionBean extends ISalsaBean {

    public boolean isConfigReloadedBeforeExecute();

    public void setConfigReloadedBeforeExecute(boolean reload);

    public boolean isConfirmation();

    public void setConfirmation(boolean confirmation);

    public Action getAction();

}

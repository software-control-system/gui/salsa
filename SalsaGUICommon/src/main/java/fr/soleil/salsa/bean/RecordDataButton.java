package fr.soleil.salsa.bean;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.slf4j.Logger;

import fr.soleil.salsa.api.ScanApi;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.logging.LoggingUtil;

public class RecordDataButton extends JButton implements ActionListener {

    private static final long serialVersionUID = -7910575560446411154L;

    public static final Logger LOGGER = LoggingUtil.getLogger(RecordDataButton.class);

    private boolean selected = true;

    private static final String DATA_RECORDER_ON = "Data recorder ON";
    private static final String DATA_RECORDER_OFF = "Data recorder OFF";

    public RecordDataButton() {
        super();
        setText(DATA_RECORDER_ON);
        setBackground(Color.GREEN);
        addActionListener(this);
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) {
            setText(DATA_RECORDER_ON);
            setBackground(Color.GREEN);
        }
        else {
            setText(DATA_RECORDER_OFF);
            setBackground(Color.RED);
        }
        try {
            ScanApi.setDataRecorderEnable(selected);
        }
        catch (SalsaDeviceException e) {
            LOGGER.error("Cannot setDataRecorderEnable {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        selected = !selected;
        setSelected(selected);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        RecordDataButton button = new RecordDataButton();
        frame.setContentPane(button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}

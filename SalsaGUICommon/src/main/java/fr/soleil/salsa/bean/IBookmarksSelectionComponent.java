package fr.soleil.salsa.bean;

public interface IBookmarksSelectionComponent {

    public void addBookmarkSelectionListener(IBookmarkSelectionListener listener);

    public void removeBookmarkSelectionListener(IBookmarkSelectionListener listener);

    public void refreshBookmarks();

    public void loadBookmarks();

    public void unloadBookmarks();
}

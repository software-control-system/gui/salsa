package fr.soleil.salsa.bean;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.slf4j.Logger;

import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public abstract class AbstractSalsaBean extends JPanel implements ISalsaBean, IScanServerListener {

    private static final long serialVersionUID = 2704243892663658651L;

    public static final Logger LOGGER = LoggingUtil.getLogger(AbstractSalsaBean.class);

    private String configPath = null;
    private String newConfigPath = null;
    private boolean configLoaded = false;
    private boolean currentConfigAtLoad = false;
    private boolean currentConfigAtNewScan = false;
    private boolean reloadConfigAtRead = false;
    private IConfig<?> config = null;
    protected List<ISalsaBeanListener> beanListeners = new ArrayList<ISalsaBeanListener>();
    private DevicePreferences devicePreferences = null;
    private String lastStartDate = null;
    private boolean listening = false;
    private boolean load = false;

    protected AbstractSalsaBean() {
        super();
    }

    public boolean isLoad() {
        return load;
    }

    @Override
    public void setConfigPath(String aconfigPath) {
        this.configPath = aconfigPath;
        setNewConfigPath(configPath);
        if (load) {
            loadConfig();
        }
    }

    private void setNewConfigPath(String aConfigPath) {
        if ((aConfigPath != null) && ((newConfigPath == null) || !aConfigPath.equals(newConfigPath))) {
            this.newConfigPath = aConfigPath;
            notifyConfigPathChanged(newConfigPath);
        }
    }

    @Override
    public String getConfigPath() {
        return configPath;
    }

    public IConfig<?> getConfig() {
        return config;
    }

    public void setConfig(IConfig<?> aconfig) {
        config = aconfig;
        if (config != null) {
            configPath = config.getFullPath();
            configLoaded = true;
            notifyLoadingConfigSucceed(config);
        } else {
            configLoaded = false;
            notifyLoadingConfigFailed("no config", new SalsaException("configPath is null or empty"));
        }

    }

    protected void reloadConfig() {
        if (config != null) {
            try {
                IConfig<?> newconfig = SalsaAPI.reloadConfigById(config.getId());
                if (newconfig != null) {
                    setConfig(newconfig);
                }
            } catch (SalsaException e) {
                config = null;
                configLoaded = false;
                notifyLoadingConfigFailed(configPath, e);
            }
        }

    }

    @Override
    public void loadConfig() {
        load = true;
        IConfig<?> newConfig = null;
        SalsaException salsaException = null;

        if (currentConfigAtLoad || currentConfigAtNewScan) {
            String currentConfigPath = getCurrentConfigPath();
            if (SalsaUtils.isDefined(currentConfigPath) && (currentConfigPath.indexOf("/") > -1)) {
                setNewConfigPath(currentConfigPath);
            } else if (SalsaUtils.isDefined(configPath) && (configPath.indexOf("/") > -1)) {
                setNewConfigPath(configPath);
            }
        }

        if (SalsaUtils.isDefined(newConfigPath)) {
            LOGGER.trace("newConfigPath={}", newConfigPath);
            LOGGER.trace("currentConfig={}", config);
            // Do not reload the config if the actual config is the same
            if ((config == null) || !config.getFullPath().equalsIgnoreCase(newConfigPath)) {
                try {
                    newConfig = SalsaAPI.getConfigByPath(newConfigPath, reloadConfigAtRead);
                    if (newConfig != null) {
                        config = newConfig;
                    } else {
                        salsaException = new SalsaException("unknown error for loading " + newConfigPath);
                    }

                    if (config == null) {
                        configLoaded = false;
                        notifyLoadingConfigFailed(configPath, salsaException);
                    } else {
                        configLoaded = true;
                        notifyLoadingConfigSucceed(config);
                    }
                } catch (SalsaException e) {
                    salsaException = e;
                    LOGGER.debug(e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            }
        } else {
            String errorMessage = "configPath is null or empty";
            LOGGER.debug(errorMessage);
            salsaException = new SalsaException(errorMessage);
        }

        if (newConfig == null) {
            configLoaded = false;
        }

        // Add StateListener
        if (currentConfigAtNewScan) {
            startListening();
        }

    }

    protected void startListening() {
        if (!listening && (getDevicePreferences() != null)) {
            String scanServerName = getDevicePreferences().getScanServer();
            listening = true;
            CurrentScanDataModel.addScanServerListener(scanServerName, this);
        }
    }

    protected void stopListening() {
        if (listening && (getDevicePreferences() != null)) {
            String scanServerName = getDevicePreferences().getScanServer();
            listening = false;
            CurrentScanDataModel.removeScanServerListener(scanServerName, this);
        }
    }

    @Override
    public void unloadConfig() {
        load = false;
        setNewConfigPath(null);
        config = null;
        configLoaded = false;
        stopListening();
    }

    protected DevicePreferences getDevicePreferences() {
        if (devicePreferences == null) {
            devicePreferences = SalsaAPI.getDevicePreferences();
        }
        return devicePreferences;
    }

    private String getCurrentConfigPath() {
        String currentConfigPath = null;
        if (getDevicePreferences() != null) {
            String scanServerName = getDevicePreferences().getScanServer();
            String newCurrentConfig = CurrentScanDataModel.readRunName(scanServerName);
            if (SalsaUtils.isDefined(newCurrentConfig)) {
                currentConfigPath = newCurrentConfig.replaceAll("\\.", "/");
            }
        }
        return currentConfigPath;
    }

    @Override
    public boolean isConfigLoaded() {
        return configLoaded;
    }

    @Override
    public void setCurrentConfigAtLoad(boolean loadedAtStart) {
        currentConfigAtLoad = loadedAtStart;

    }

    @Override
    public void setCurrentConfigAtNewScan(boolean loadedAtNewScan) {
        currentConfigAtNewScan = loadedAtNewScan;
    }

    @Override
    public boolean isCurrentConfigAtNewScan() {
        return currentConfigAtNewScan;
    }

    @Override
    public boolean isCurrentConfigAtLoad() {
        return currentConfigAtLoad;
    }

    @Override
    public void setReloadConfigAtRead(boolean reload) {
        reloadConfigAtRead = reload;
    }

    @Override
    public boolean isReloadConfigAtRead() {
        return reloadConfigAtRead;
    }

    @Override
    public void addSalsaBeanListener(ISalsaBeanListener listener) {
        if ((listener != null) && !beanListeners.contains(listener)) {
            beanListeners.add(listener);
        }
    }

    @Override
    public void removeSalsaBeanListener(ISalsaBeanListener listener) {
        if ((listener != null) && beanListeners.contains(listener)) {
            synchronized (beanListeners) {
                beanListeners.remove(listener);
            }
        }
    }

    protected void notifyConfigPathChanged(String configPath) {
        for (ISalsaBeanListener listener : beanListeners) {
            listener.configPathChanged(configPath);
        }
    }

    protected void notifyLoadingConfigSucceed(IConfig<?> config) {
        for (ISalsaBeanListener listener : beanListeners) {
            listener.loadingConfigSucceed(config);
        }
    }

    protected void notifyLoadingConfigFailed(String configPath, SalsaException exception) {
        for (ISalsaBeanListener listener : beanListeners) {
            listener.loadingConfigFailed(configPath, exception);
        }
    }

    @Override
    public void stateChanged(String state) {
        if (currentConfigAtNewScan) {
            if (getDevicePreferences() != null) {
                String scanServerName = getDevicePreferences().getScanServer();
                if (lastStartDate == null) {
                    lastStartDate = CurrentScanDataModel.getStartDate(scanServerName);
                }
                // New scan detected
                if (CurrentScanDataModel.dateChanged(scanServerName, lastStartDate)) {
                    lastStartDate = CurrentScanDataModel.getStartDate(scanServerName);
                    loadConfig();
                }
            }
        }
    }
}

package fr.soleil.salsa.bean;

public interface IConfigSelectionListener {

	public void configPathChanged(String configPath);
}

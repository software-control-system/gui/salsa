package fr.soleil.salsa.bean;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import javax.swing.JComboBox;

import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.tool.SalsaUtils;

public class BookmarksCombo extends JComboBox<String> implements IBookmarksSelectionComponent {

    private static final long serialVersionUID = 6006364820424936384L;

    private IBookmarkSelectionListener listener = null;

    public BookmarksCombo() {
        super();
        addActionListener(e -> {
            if (e.getModifiers() != 0) {
                notifyListener();
            }
        });
    }

    @Override
    public void loadBookmarks() {
        buildBookmarks();
    }

    @Override
    public void unloadBookmarks() {
        removeAllItems();
    }

    @Override
    public void addBookmarkSelectionListener(IBookmarkSelectionListener listener) {
        this.listener = listener;
        notifyListener();
    }

    private void notifyListener() {
        if ((listener != null) && (getSelectedItem() != null)) {
            Object item = getSelectedItem();
            if (item != null) {
                String bookmark = item.toString();
                if (SalsaUtils.isDefined(bookmark)) {
                    if (listener != null) {
                        listener.bookmarksChanged(bookmark);
                    }
                }
            }
        }
    }

    @Override
    public void removeBookmarkSelectionListener(IBookmarkSelectionListener listener) {
        this.listener = null;

    }

    @Override
    public void refreshBookmarks() {
        loadBookmarks();
    }

    private void buildBookmarks() {
        // Load the scanServerName
        Map<String, Map<String, String>> bookmarks = UIPreferences.getInstance().getBookmarks();
        Set<String> bookmarkKeySet = bookmarks.keySet();
        String[] keyArray = bookmarkKeySet.toArray(new String[bookmarkKeySet.size()]);
        Arrays.sort(keyArray);
        removeAllItems();
        for (String bookmarkKey : keyArray) {
            addItem(bookmarkKey);
        }
        notifyListener();
    }
}

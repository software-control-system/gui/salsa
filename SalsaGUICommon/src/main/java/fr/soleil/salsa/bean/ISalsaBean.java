package fr.soleil.salsa.bean;

import java.awt.Color;

public interface ISalsaBean {

    public final Color MODIFY_COLOR = new Color(128, 160, 255);
    public final Color NOTAPPLY_COLOR = new Color(255, 200, 0);

    /**
     * Set the scan configuration path e.g : root/scan1D
     * 
     * @param configPath
     */
    public void setConfigPath(String configPath);

    /**
     * Get the scan configuration path
     * 
     * @return configuration path
     */
    public String getConfigPath();

    /**
     * Load the scan configuration specified in configPath and apply value to the bean
     */
    public void loadConfig();

    /**
     * Unload the scan configuration and stop listening the scan server state
     */
    public void unloadConfig();

    /**
     * @return true if the config is well loaded
     */
    public boolean isConfigLoaded();

    /**
     * If configPath not defined read the current scan from the scanServer and set configPath to the
     * currentConfig
     * 
     * @param true to read the current scan
     */
    public void setCurrentConfigAtLoad(boolean currentConfigAtLoad);

    /**
     * @return currentConfigAtLoad
     */
    public boolean isCurrentConfigAtLoad();

    /**
     * If configPath not defined read the current scan from the scanServer at each detected new scan
     * and set configPath to the currentConfig and apply the value to the bean
     * 
     * @param true to read the current scan
     */
    public void setCurrentConfigAtNewScan(boolean loadedAtNewScan);

    /**
     * @return currentConfigAtNewScan
     */
    public boolean isCurrentConfigAtNewScan();

    /**
     * Reload values from the database at load
     * 
     * @param true for reload the value
     */
    public void setReloadConfigAtRead(boolean reload);

    /**
     * @return reloadConfigAtRead
     */
    public boolean isReloadConfigAtRead();

    /**
     * add listener on the bean
     * 
     * @see ISalsaBeanListener
     */
    public void addSalsaBeanListener(ISalsaBeanListener listener);

    /**
     * remove listener on the bean
     * 
     * @see ISalsaBeanListener
     */
    public void removeSalsaBeanListener(ISalsaBeanListener listener);

}

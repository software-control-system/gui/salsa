package fr.soleil.salsa.client.controller.impl;

import java.util.List;

import org.slf4j.Logger;

import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.ITimebase;
import fr.soleil.salsa.entity.event.TimebaseModel;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class GenericTimebaseListController extends GenericDeviceListController {

    private static final Logger LOGGER = LoggingUtil.getLogger(GenericTimebaseListController.class);
    private final static String ACTUATORS_ATTRIBUTE = "actuatorProxies";
    private final static String SENSORS_ATTRIBUTE = "sensor";

    public GenericTimebaseListController() {
        this(null, null);
    }

    public GenericTimebaseListController(final IConfig<?> config, final IErrorController errorController) {
        super(null, config, errorController);
    }

    @Override
    protected IDevice generateDevice() {
        return new TimebaseModel();
    }

    @Override
    protected List<ISuggestionCategory> getCategories(final ISuggestions suggestions) {
        return suggestions.getTimebaseSuggestionList();
    }

    @Override
    protected boolean isPropertyChange(final String propertyName) {
        boolean change = false;
        if ((propertyName != null) && propertyName.toLowerCase().contains("timebase")) {
            change = true;
        }
        return change;
    }

    @Override
    protected void initDeviceList() {
        if (config != null) {
            this.deviceList = config.getTimebaseList();
        }
    }

    @Override
    public void notifyNewAction(final String timebasename) {
        if (listener != null) {
            listener.deviceAdded(timebasename);
        }
        if (getDeviceForName(timebasename) != null) {
            deviceErrorMessage(true, timebasename);
        } else if (timebasename != null) {
            if ((config != null)) {
                config.addTimeBase(timebasename);

                // BUG 20660 add sensor and actuators on X dimension
                if (SalsaUtils.isDefined(timebasename)) {
                    String deviceName = timebasename.trim();

                    // Add Actuator list
                    if (TangoAttributeHelper.isAttributeRunning(deviceName, ACTUATORS_ATTRIBUTE)) {
                        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
                        if (proxy == null) {
                            LOGGER.error("Could not get DeviceProxy for " + deviceName);
                        } else {
                            try {
                                DeviceAttribute deviceAttribute = proxy.read_attribute(ACTUATORS_ATTRIBUTE);
                                String[] attributeList = deviceAttribute.extractStringArray();
                                IDimension dimension = config.getDimensionX();
                                if (dimension != null) {
                                    List<IActuator> actuatorList = dimension.getActuatorsList();
                                    // Check if the actuators are not already defined
                                    // Then add
                                    for (String actuatorName : attributeList) {
                                        if (config.getDevice(deviceName, actuatorList) == null) {
                                            config.addActuator(actuatorName, IConfig.FIRST_DIMENSION);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                            }
                        }
                    }

                    // Add Sensor list
                    if (TangoAttributeHelper.isAttributeRunning(deviceName, SENSORS_ATTRIBUTE)) {
                        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
                        if (proxy == null) {
                            LOGGER.error("Could not get DeviceProxy for " + deviceName);
                        } else {
                            try {
                                DeviceAttribute deviceAttribute = proxy.read_attribute(SENSORS_ATTRIBUTE);
                                String[] attributeList = deviceAttribute.extractStringArray();
                                List<ISensor> sensorList = config.getSensorsList();
                                String completeSensorName = null;
                                for (String sensorName : attributeList) {
                                    completeSensorName = deviceName + "/" + sensorName;
                                    if (config.getDevice(completeSensorName, sensorList) == null) {
                                        config.addSensor(completeSensorName);
                                    }
                                }
                            } catch (Exception e) {
                                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                            }
                        }
                    }
                }
                if (config != null) {
                    config.setModified(true);
                }
            }
        }

    }

    @Override
    public void notifyDeviceEnabled(final String deviceName, final boolean selected) {
        if (config != null) {
            config.setTimeBaseEnable(deviceName, selected);
        }
        super.notifyDeviceEnabled(deviceName, selected);
    }

    @Override
    public void notifyDeviceRenamed(final int deviceIndex, final String newDeviceName) {
        IDevice oldTimebase = getDeviceName(deviceIndex);
        if ((oldTimebase != null) && !ObjectUtils.sameObject(oldTimebase.getName(), newDeviceName)) {
            String oldTimebaseName = oldTimebase.getName();
            if (listener != null) {
                listener.deviceRenamed(oldTimebaseName, newDeviceName);
            }
            if (config != null) {
                config.renameTimeBase(oldTimebaseName, newDeviceName);
            }
        }
    }

    @Override
    protected void swapDevice(final String deviceName1, final String deviceName2) {
        if (this.config != null) {
            config.swapTimeBase(deviceName1, deviceName2);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected boolean addDevice(final IDevice device) {
        boolean result = false;
        if ((deviceList != null) && (device instanceof ITimebase)) {
            result = ((List<ITimebase>) this.deviceList).add((ITimebase) device);
        }
        return result;
    }

}

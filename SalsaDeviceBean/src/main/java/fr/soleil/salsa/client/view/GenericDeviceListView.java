package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jdesktop.swingx.JXTable;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.controller.IGenericDeviceListController;
import fr.soleil.salsa.client.util.CellDeviceNameRenderer;
import fr.soleil.salsa.client.util.CellSelectionnableTextEditor;
import fr.soleil.salsa.client.util.ModelTableList;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.impl.DeviceImpl;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * View of the list of device class
 */
public class GenericDeviceListView extends JPanel implements IView<IGenericDeviceListController> {

    private static final long serialVersionUID = -217556439500881527L;

    private JSplitPane jSplitPane;
    private JPanel listElementPanel;
    private JPanel elementPanel;
    private JButton addToAllButton;
    private final GenericDeviceView deviceView;
    private IGenericDeviceListController deviceListController;
    private boolean managerMode;
    private boolean adminMode;
    private boolean editable;

    private JXTable elementTable;

    private NewDeviceMenu newDeviceButton;

    private JButton deleteButton;
    private JButton upButton, downButton, topButton, bottomButton, reverseButton;
    private JCheckBox selectAll;
    private JPanel jToolBar;
    private ModelTableList model;
    private boolean up;
    private boolean down;

    private final TableModelListener tableModelListener;

    /**
     * Constructor
     */
    public GenericDeviceListView() {
        this(null);
    }

    /***
     * The constructor
     * 
     * @param listController
     * @param deviceView
     */
    public GenericDeviceListView(IGenericDeviceListController listController) {
        this.deviceListController = listController;
        deviceView = new GenericDeviceView();
        managerMode = false;
        adminMode = true;
        editable = true;
        up = false;
        down = false;
        tableModelListener = (tme) -> {
            int selectedRow = tme.getFirstRow();
            int selectedCol = tme.getColumn();
            // Case when we modify a cell
            Object deviceValue = model.getValueAt(selectedRow, 1);
            if ((deviceListController != null) && (deviceValue != null)) {
                String deviceName = deviceValue.toString();
                deviceView.setDeviceName(deviceName);
                if (tme.getType() == TableModelEvent.UPDATE) {
                    if (!isUp() && !isDown()) {
                        if (selectedCol == 0) {
                            boolean selected = Boolean.valueOf(model.getValueAt(selectedRow, 0).toString());
                            deviceListController.notifyDeviceEnabled(deviceName, selected);
                        } else if (selectedCol == 1) {
                            deviceListController.notifyDeviceRenamed(selectedRow, deviceName);
                        }
                    }
                }
            }
        };
        initialize();
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        if (getModel() != null) {
            getModel().setEditable(editable);
        }
    }

    public boolean isEditable() {
        return editable;
    }

    public void setAdminMode(boolean adminMode) {
        this.adminMode = adminMode;
        getNewDeviceButton().setVisible(this.adminMode);
        getDeleteButton().setVisible(this.adminMode);
        getDownButton().setVisible(adminMode && !managerMode);
        getUpButton().setVisible(adminMode && !managerMode);
    }

    public void setManagerMode(boolean managerMode) {
        this.managerMode = managerMode;
        getAddAllButton().setVisible(this.managerMode);
        if (managerMode) {
            getDeleteButton().setText("Delete from all");
            getDeleteButton().setToolTipText("Deleted device from all configuration");
        } else {
            getDeleteButton().setText("Delete");
            getDeleteButton().setToolTipText("Delete device");
        }
        getDownButton().setVisible(adminMode && !managerMode);
        getUpButton().setVisible(adminMode && !managerMode);
    }

    private void initialize() {
        JPanel firstPanel = new JPanel();
        firstPanel.setSize(579, 321);
        firstPanel.setLayout(new BorderLayout());
        firstPanel.add(getButtonsPanel(), BorderLayout.NORTH);
        firstPanel.add(getFirstPanel(), BorderLayout.CENTER);
        setLayout(new BorderLayout());
        add(firstPanel, BorderLayout.CENTER);
        addListSelectionListener();
    }

    private void addListSelectionListener() {
        ListSelectionModel listSelectionModel = elementTable.getSelectionModel();
        listSelectionModel.addListSelectionListener((e) -> {
            // Ignore extra messages.
            if (!e.getValueIsAdjusting()) {
                ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                getDeleteButton().setEnabled(!lsm.isSelectionEmpty());
                if (lsm.isSelectionEmpty()) {
                    getUpButton().setEnabled(false);
                    getDownButton().setEnabled(false);
                    getTopButton().setEnabled(false);
                    getBottomButton().setEnabled(false);
                    getReverseButton().setEnabled(false);
                    getAddAllButton().setEnabled(false);
                } else {
                    int[] selectedRows = getSelectedRows();
                    int selectedRow = lsm.getMinSelectionIndex();
                    boolean up = true, down = true, top = true, bottom = true, reverse, addAll = false;
                    for (int row : selectedRows) {
                        if (row < 1) {
                            up = false;
                        }
                        if (row > model.getRowCount() - 2) {
                            down = false;
                        }
                        if (!addAll) {
                            Object value = getModel().getValueAt(row, 1);
                            if (value instanceof IDevice) {
                                if (!((IDevice) value).isCommon()) {
                                    addAll = true;
                                }
                            }
                        }
                    }
                    if (selectedRows.length == 1) {
                        top = up;
                        bottom = down;
                        reverse = false;
                    } else {
                        Arrays.sort(selectedRows);
                        reverse = true;
                        for (int i = 1; i < selectedRows.length && reverse; i++) {
                            if (selectedRows[i] != selectedRows[i - 1] + 1) {
                                reverse = false;
                            }
                        }
                        if (reverse) {
                            if (selectedRows[0] == 0) {
                                top = false;
                            }
                            if (selectedRows[selectedRows.length - 1] == elementTable.getRowCount() - 1) {
                                bottom = false;
                            }
                        }
                    }
                    Object deviceValue = model.getValueAt(selectedRow, 1);
                    if ((deviceListController != null) && (deviceValue != null)) {
                        deviceView.setDeviceName(deviceValue.toString());
                    }
                    // Update the Down and And button state
                    getUpButton().setEnabled(up);
                    getDownButton().setEnabled(down);
                    getTopButton().setEnabled(top);
                    getBottomButton().setEnabled(bottom);
                    getReverseButton().setEnabled(reverse);
                    getAddAllButton().setEnabled(addAll);
                }
            }
        });
    }

    /***
     * 
     * @return JToolBar
     */
    protected JPanel getButtonsPanel() {
        if (jToolBar == null) {
            jToolBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
            jToolBar.add(getSelectAll());
            jToolBar.add(getNewDeviceButton());
            jToolBar.add(getDeleteButton());
            jToolBar.add(getUpButton());
            jToolBar.add(getDownButton());
            jToolBar.add(getTopButton());
            jToolBar.add(getBottomButton());
            jToolBar.add(getReverseButton());
            jToolBar.setVisible(true);
            jToolBar.add(getAddAllButton());
        }
        return jToolBar;
    }

    private JButton getAddAllButton() {
        if (addToAllButton == null) {
            addToAllButton = new JButton("Add selected to all");
            addToAllButton.setVisible(false);
            addToAllButton.setToolTipText("Add selected devices to all configuration");
            addToAllButton.setEnabled(false);
            addToAllButton.addActionListener((e) -> {
                int[] rows = getElementList().getSelectedRows();
                if ((rows != null) && (rows.length > 0)) {
                    for (int row : rows) {
                        Object value = getModel().getValueAt(row, 1);
                        if (value instanceof IDevice) {
                            ((IDevice) value).setCommon(true);
                            if (deviceListController != null) {
                                deviceListController.notifyNewAction(value.toString());
                            }
                        }
                    }
                    getElementList().repaint();
                }
            });
        }
        return addToAllButton;
    }

    /***
     * 
     * @return JSplitPane
     */
    private JSplitPane getFirstPanel() {
        if (jSplitPane == null) {
            jSplitPane = new JSplitPane();
            jSplitPane.setDividerLocation(230);
            jSplitPane.setDividerSize(5);
            jSplitPane.setOneTouchExpandable(false);
            jSplitPane.setLeftComponent(getListElementPanel());
            jSplitPane.setRightComponent(getElementPanel());
        }
        return jSplitPane;
    }

    public void setControlEnable(boolean enable) {
        getDeleteButton().setEnabled(enable);
        // getUpButton().setEnabled(enable);
        // getDownButton().setEnabled(enable);
        getSelectAll().setEnabled(enable);
        getNewDeviceButton().setEnabled(enable);
    }

    /***
     * Return the panel containing the list of device name
     * 
     * @return JPanel
     */
    private JPanel getListElementPanel() {
        if (listElementPanel == null) {
            listElementPanel = new JPanel();
            listElementPanel.setLayout(new BorderLayout());
            listElementPanel.add(new JScrollPane(getElementList()), BorderLayout.CENTER);
        }
        return listElementPanel;
    }

    /**
     * This method initializes elementList
     * 
     * @return javax.swing.JList
     */
    private JXTable getElementList() {
        if (elementTable == null) {
            DefaultListSelectionModel defaultListSelectionModel1 = new DefaultListSelectionModel();
            elementTable = new JXTable();
            elementTable.setBorder(new LineBorder(Color.GRAY, 2));
            // Editor
            elementTable.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            elementTable.setColumnSelectionAllowed(false);
            elementTable.setRowSelectionAllowed(true);
            elementTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            elementTable.setSelectionModel(defaultListSelectionModel1);
            elementTable.setDefaultEditor(IDevice.class, new CellSelectionnableTextEditor());
            elementTable.setDefaultRenderer(IDevice.class, new CellDeviceNameRenderer());
            elementTable.setSortable(false);
            defaultListSelectionModel1.setAnchorSelectionIndex(-1);
            defaultListSelectionModel1.setLeadSelectionIndex(-1);
        }
        return elementTable;
    }

    /***
     * Get the model
     * 
     * @return ModelTableList
     */
    public ModelTableList getModel() {
        if (model == null) {
            Vector<String> colVector = new Vector<String>();
            colVector.add(ObjectUtils.EMPTY_STRING);
            colVector.add(ObjectUtils.EMPTY_STRING);
            List<Class<?>> columnClass = new ArrayList<Class<?>>();
            columnClass.add(Boolean.class);
            columnClass.add(IDevice.class);
            model = new ModelTableList(colVector, columnClass);

            Vector<Object> rowData = new Vector<Object>();
            rowData.add(new Boolean(false));
            rowData.add(new DeviceImpl());
            model.addRow(rowData);
        }
        if (model != null) {
            model.setEditable(editable);
        }
        return model;
    }

    public boolean isNullModel() {
        return (model == null);
    }

    /***
     * Return the panel containing the device view
     * 
     * @return JPanel
     */
    private JPanel getElementPanel() {
        if (elementPanel == null) {
            elementPanel = new JPanel();
            elementPanel.setLayout(new BorderLayout());
            updateDeviceView();
        }
        return elementPanel;
    }

    protected void updateDeviceView() {
        if (deviceView != null) {
            elementPanel.add(deviceView, BorderLayout.CENTER);
        }
    }

    private JCheckBox getSelectAll() {
        if (selectAll == null) {
            selectAll = new JCheckBox("Check/Uncheck all");
            selectAll.addActionListener((e) -> {
                if (deviceListController != null) {
                    deviceListController.notifyDeviceSelectedAll(selectAll.isSelected());
                }
            });
        }
        return selectAll;
    }

    /**
     * Sets suggestions.
     * 
     * @param menuRoot
     */
    public void setSuggestions(List<ISuggestionCategory> suggestionList) {
        getNewDeviceButton().setSuggestionList(suggestionList);
    }

    private NewDeviceMenu getNewDeviceButton() {
        if (newDeviceButton == null) {
            NewDeviceMenuListener listener = new NewDeviceMenuListener() {

                @Override
                public void devicesAdded(List<String> deviceList) {
                    if (deviceListController != null) {
                        deviceListController.notifyNewAction(deviceList);
                    }
                }

                @Override
                public void deviceAdded(String newDeviceName) {
                    if (deviceListController != null) {
                        deviceListController.notifyNewAction(newDeviceName);
                    }
                }
            };
            newDeviceButton = new NewDeviceMenu(listener);
        }
        return newDeviceButton;
    }

    /***
     * Return the button "delete" for remove of the list
     * 
     * @return JButton
     */
    private JButton getDeleteButton() {
        if (deleteButton == null) {
            deleteButton = new JButton();
            deleteButton.setEnabled(false);
            deleteButton.setText("Delete");
            deleteButton.addActionListener((e) -> {
                if (deviceListController != null) {
                    deviceListController.notifyDeleteAction();
                }
            });
        }
        return deleteButton;
    }

    /***
     * Return the button "up"
     * 
     * @return JButton
     */
    private JButton getUpButton() {
        if (upButton == null) {
            upButton = new JButton();
            upButton.setEnabled(false);
            upButton.setText("Up");
            upButton.addActionListener((e) -> {
                int[] devicesToUp = getSelectedRows();
                setUp(true);
                if (deviceListController != null) {
                    deviceListController.changeDevicePositions(true, devicesToUp);
                }
            });
        }
        return upButton;
    }

    /***
     * Return the button down
     * 
     * @return JButton
     */
    private JButton getDownButton() {
        if (downButton == null) {
            downButton = new JButton();
            downButton.setEnabled(false);
            downButton.setText("Down");
            downButton.addActionListener((e) -> {
                int[] devicesToDown = getSelectedRows();
                setDown(true);
                if (deviceListController != null) {
                    deviceListController.changeDevicePositions(false, devicesToDown);
                }
            });
        }
        return downButton;
    }

    public JButton getTopButton() {
        if (topButton == null) {
            topButton = new JButton();
            topButton.setEnabled(false);
            topButton.setText("Top");
            topButton.addActionListener((e) -> {
                int[] devicesToUp = getSelectedRows();
                setUp(true);
                if (deviceListController != null) {
                    deviceListController.changeDevicePositionsToExtreme(true, devicesToUp);
                }
            });
        }
        return topButton;
    }

    public JButton getBottomButton() {
        if (bottomButton == null) {
            bottomButton = new JButton();
            bottomButton.setEnabled(false);
            bottomButton.setText("Bottom");
            bottomButton.addActionListener((e) -> {
                int[] devicesToDown = getSelectedRows();
                setDown(true);
                if (deviceListController != null) {
                    deviceListController.changeDevicePositionsToExtreme(false, devicesToDown);
                }
            });
        }
        return bottomButton;
    }

    public JButton getReverseButton() {
        if (reverseButton == null) {
            reverseButton = new JButton();
            reverseButton.setEnabled(false);
            reverseButton.setText("Reverse");
            reverseButton.addActionListener((e) -> {
                int[] devicesToReverse = getSelectedRows();
                setDown(true);
                if (deviceListController != null) {
                    deviceListController.reversePositions(devicesToReverse);
                }
            });
        }
        return reverseButton;
    }

    /***
     * Get the deviceController
     */
    @Override
    public IGenericDeviceListController getController() {
        return deviceListController;
    }

    @Override
    public void setController(IGenericDeviceListController controller) {
        this.deviceListController = controller;
    }

    /***
     * They first add data to model
     * 
     * @param listDevices
     */
    public String[] addRowInModel(List<? extends IDevice> listDevices) {
        if (!SalsaUtils.isFulfilled(listDevices)) {
            clear();
        }
        String[] failure = null;
        removeModelListenerToCurrentModel();
        Vector<String> colVector = new Vector<String>();
        colVector.add(ObjectUtils.EMPTY_STRING);
        colVector.add(ObjectUtils.EMPTY_STRING);
        List<Class<?>> columnClass = new ArrayList<Class<?>>();
        columnClass.add(Boolean.class);
        columnClass.add(IDevice.class);
        model = new ModelTableList(colVector, columnClass);
        if ((listDevices != null) && (listDevices.size() > 0)) {
            List<IDevice> toRemove = new ArrayList<IDevice>();
            for (IDevice device : listDevices) {
                // Add element to model
                if (!addRow(device)) {
                    toRemove.add(device);
                }
            }
            if (toRemove.size() > 0) {
                failure = new String[toRemove.size()];
                for (int i = 0; i < failure.length; i++) {
                    failure[i] = toRemove.get(i).getName();
                }
                listDevices.removeAll(toRemove);
            }
            toRemove.clear();
        }
        // Add a model listener to model
        addModelListenerToCurrentModel();
        // add the model to the table
        elementTable.setModel(model);
        setEditable(editable);
        setDefaultColumnWidth();
        return failure;
    }

    protected boolean isDeviceNameAllowed(String name) {
        boolean allowed;
        if (name == null) {
            name = ObjectUtils.EMPTY_STRING;
        }
        if (model == null) {
            allowed = false;
        } else {
            allowed = true;
            for (int i = 0; (i < model.getRowCount()) && allowed; i++) {
                if (name.equalsIgnoreCase(model.getValueAt(i, 1).toString())) {
                    allowed = false;
                }
            }
        }
        return allowed;
    }

    protected void setDefaultColumnWidth() {
        if (elementTable.getColumnModel().getColumnCount() > 0) {
            elementTable.getColumnModel().getColumn(0).setMaxWidth(30);
        }
    }

    /***
     * The method add a row in the model
     * 
     * @param device
     * @return boolean
     */
    public boolean addRow(IDevice device) {
        boolean result;
        if ((model != null) && isDeviceNameAllowed(device.getName())) {
            Vector<Object> newRowData = new Vector<Object>();
            newRowData.add(device.isEnabled());
            newRowData.add(device);
            model.addRow(newRowData);
            result = true;
            if (!deviceView.isVisible()) {
                deviceView.setVisible(true);
            }
        } else {
            result = false;
        }
        return result;
    }

    /***
     * The method remove a row in the model
     * 
     * @param deviceToRemove
     * @return boolean
     */
    public boolean removeRow(int deviceToRemove) {
        boolean result;
        if ((model != null) && (deviceToRemove < model.getRowCount())) {
            model.removeRow(deviceToRemove);
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    /***
     * Add a model table listener to the current model
     */
    private void addModelListenerToCurrentModel() {
        if (model != null) {
            model.addTableModelListener(tableModelListener);
        }
    }

    /***
     * Remove a model table listener to the current model
     */
    private void removeModelListenerToCurrentModel() {
        if (model != null) {
            model.removeTableModelListener(tableModelListener);
        }
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    private boolean isDown() {
        return down;
    }

    private void setDown(boolean down) {
        this.down = down;
    }

    public int[] getSelectedRows() {
        return elementTable.getSelectedRows();
    }

    public void setSelectedDevices(String... devices) {
        List<Integer> rows = new ArrayList<>();
        if ((devices != null) && (devices.length > 0)) {
            for (String device : devices) {
                if (device != null) {
                    for (int row = 0; row < model.getRowCount(); row++) {
                        IDevice tmp = (IDevice) model.getValueAt(row, 1);
                        if (tmp != null && device.equals(tmp.getName())) {
                            rows.add(Integer.valueOf(row));
                        }
                    }
                }
            }
        }
        int[] selectedRows = new int[rows.size()];
        int index = 0;
        for (Integer row : rows) {
            selectedRows[index++] = row.intValue();
        }
        rows.clear();
        setSelectedRows(selectedRows);
    }

    public void setSelectedRows(int... selectedRows) {
        elementTable.getSelectionModel().clearSelection();
        if (isUp()) {
            setUp(false);
        }
        if (isDown()) {
            setDown(false);
        }
        if ((selectedRows != null) && selectedRows.length > 0) {
            boolean up = true, down = true;
            for (int row : selectedRows) {
                if (row < 1) {
                    up = false;
                }
                if (row > model.getRowCount() - 2) {
                    down = false;
                }
                elementTable.getSelectionModel().addSelectionInterval(row, row);
            }
            // Update the Down and Up button state
            getUpButton().setEnabled(up);
            getDownButton().setEnabled(down);
        }
    }

    public void clear() {
        if (model != null) {
            int row = model.getRowCount();
            for (int i = 0; i < row; i++) {
                model.removeRow(0);
            }
        }
        deviceView.setDeviceName(null);
        deviceView.setVisible(false);
    }

    public void refreshUIPreferences() {
        deviceView.refreshUIPreferences();
    }

}

package fr.soleil.salsa.client.controller;

import java.util.List;

public interface IGenericDeviceListListener {

    public void deviceRenamed(String oldName, String newName);

    public void deviceAdded(String devices);

    public void devicesAdded(List<String> devices);

    public void deviceRemoved(String devices);

    public void deviceEnabled(String device, boolean enabled);

    public void deviceSwap(String device1, String device2);

}

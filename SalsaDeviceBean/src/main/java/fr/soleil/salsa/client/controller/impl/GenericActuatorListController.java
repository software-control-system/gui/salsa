package fr.soleil.salsa.client.controller.impl;

import java.util.List;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.event.ActuatorModel;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.tool.SalsaUtils;

public class GenericActuatorListController extends GenericDeviceListController {

    public GenericActuatorListController() {
        this(null, null);
    }

    public GenericActuatorListController(final IConfig<?> config, final IErrorController errorController) {
        super(null, config, errorController);
    }

    @Override
    protected IDevice generateDevice() {
        return new ActuatorModel();
    }

    @Override
    protected List<ISuggestionCategory> getCategories(final ISuggestions suggestions) {
        return suggestions.getActuatorSuggestionList();
    }

    @Override
    protected boolean isPropertyChange(final String propertyName) {
        boolean change = false;
        if ((propertyName != null) && propertyName.toLowerCase().contains("actuator")) {
            change = true;
        }
        return change;
    }

    @Override
    protected void initDeviceList() {
        if (config != null) {
            IDimension dimension = null;
            // All type of config have an X dimension
            if ((config instanceof IConfig2D) && this.yActuator) {
                // For 2D config there is the YDimension to add
                IConfig2D config2D = (IConfig2D) this.config;
                dimension = config2D.getDimensionY();
            } else {
                dimension = config.getDimensionX();
            }
            if (dimension != null) {
                deviceList = dimension.getActuatorsList();
            }
        }
    }

    @Override
    protected boolean canAdd() {
        boolean canAdd = false;
        if (config != null) {
            canAdd = !(((config instanceof IConfigEnergy) || (config instanceof IConfigK))
                    && SalsaUtils.isFulfilled(deviceList));
        }
        return canAdd;
    }

    @Override
    public void notifyNewAction(final String actuatorName) {
        if (listener != null) {
            listener.deviceAdded(actuatorName);
        }
        if (getDeviceForName(actuatorName) != null) {
            deviceErrorMessage(true, actuatorName);
        } else if (canAdd() && (config != null) && (actuatorName != null)) {
            config.addActuator(actuatorName, (yActuator) ? IConfig.SECOND_DIMENSION : IConfig.FIRST_DIMENSION);
        }
    }

    @Override
    protected void notifyDeleteAction(final List<IDevice> deviceToRemove) {
        if (deviceToRemove != null) {
            for (IDevice dev : deviceToRemove) {
                if (config != null) {
                    config.deleteActuator(dev.getName(),
                            (yActuator) ? IConfig.SECOND_DIMENSION : IConfig.FIRST_DIMENSION);
                }
                if ((listener != null)) {
                    listener.deviceRemoved(dev.getName());
                }
            }
        }
    }

    @Override
    public void notifyDeviceEnabled(final String deviceName, final boolean selected) {
        if (config != null) {
            config.setActuatorEnable(deviceName, selected,
                    (yActuator) ? IConfig.SECOND_DIMENSION : IConfig.FIRST_DIMENSION);
        }
        super.notifyDeviceEnabled(deviceName, selected);
    }

    @Override
    public void notifyDeviceRenamed(final int deviceIndex, final String newDeviceName) {
        IDevice oldActuator = getDeviceName(deviceIndex);
        if ((oldActuator != null) && !ObjectUtils.sameObject(oldActuator.getName(), newDeviceName)) {
            String oldActuatorName = oldActuator.getName();
            if (config != null) {
                config.renameActuator(oldActuatorName, newDeviceName,
                        (yActuator) ? IConfig.SECOND_DIMENSION : IConfig.FIRST_DIMENSION);
            }
            if (listener != null) {
                listener.deviceRenamed(oldActuatorName, newDeviceName);
            }
        }
    }

    @Override
    protected void swapDevice(final String deviceName1, final String deviceName2) {
        if (this.config != null) {
            config.swapActuator(deviceName1, deviceName2,
                    (yActuator) ? IConfig.SECOND_DIMENSION : IConfig.FIRST_DIMENSION);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected boolean addDevice(final IDevice device) {
        boolean result = false;
        if ((deviceList != null) && (device instanceof IActuator)) {
            result = ((List<IActuator>) this.deviceList).add((IActuator) device);
        }
        return result;
    }

}

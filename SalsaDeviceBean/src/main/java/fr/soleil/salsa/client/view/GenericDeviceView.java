package fr.soleil.salsa.client.view;

import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import fr.soleil.bean.generic.CommonDeviceAttributeBean;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.preferences.UIPreferencesEvent;
import fr.soleil.salsa.client.preferences.UIPreferencesListener;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * View of the device class
 * 
 * @author Administrateur
 * 
 */
public class GenericDeviceView extends JScrollPane implements UIPreferencesListener {

    private static final long serialVersionUID = 8153560563532261465L;

    // GUI Elements.

    /**
     * Used for scalar devices.
     */
    private CommonDeviceAttributeBean commonBean;

    private String deviceName = null;

    /**
     * Constructor
     * 
     */
    public GenericDeviceView() {
        this.initialize();
    }

    private void initialize() {
        if (commonBean == null) {
            commonBean = new CommonDeviceAttributeBean();
        }
        setBatchFile(UIPreferences.getInstance().getControlPanel());
        setViewportView(commonBean);
        setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        UIPreferences.getInstance().addPreferenceListener(this);
    }

    private void setBatchFile(String batch) {
        if (commonBean != null) {
            commonBean.setExecutedBatchFile(batch);
        }
    }

    public void setImageProperties(ImageProperties properties) {
        commonBean.setImageProperties(properties);
    }

    public void setChartProperties(ChartProperties properties) {
        commonBean.setChartProperties(properties);
    }

    public void setPlotPropertiesMap(Map<String, PlotProperties> plotProperties) {
        commonBean.setPlotPropertiesMap(plotProperties);
    }

    public ImageProperties getImageProperties() {
        return commonBean.getImageProperties();
    }

    public ChartProperties getChartProperties() {
        return commonBean.getChartProperties();
    }

    public Map<String, PlotProperties> getPlotPropertiesMap() {
        return commonBean.getPlotPropertiesMap();
    }

    /**
     * Gets the name of the device.
     * 
     * @return String : name of the device
     */
    public String getDeviceName() {
        return this.deviceName;
    }

    /**
     * Sets the name of the device.
     * 
     * @param deviceName
     */
    public void setDeviceName(String newDeviceName) {
        refreshUIPreferences();
        if (commonBean != null) {
            if (SalsaUtils.isDefined(newDeviceName)) {
                commonBean.setVisible(true);
                if ((this.deviceName == null) || !this.deviceName.equalsIgnoreCase(newDeviceName)) {
                    commonBean.stop();
                    commonBean.setModel(null);
                    this.deviceName = newDeviceName;
                    // Thread car peux prendre tres longtemps
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            setCompleteAttributeName();
                            if (getDeviceName() != null) {
                                String deviceName = getDeviceName().trim().toLowerCase();
                                setChartProperties(UIPreferences.getInstance().getChartPropertiesMap().get(deviceName));
                                setImageProperties(UIPreferences.getInstance().getImagePropertiesMap().get(deviceName));
                                setPlotPropertiesMap(UIPreferences.getInstance().getPlotPropertiesMap());
                                commonBean.start();
                            }
                        }
                    });
                }
            } else {
                commonBean.setVisible(false);
                commonBean.stop();
                this.deviceName = null;
                commonBean.setModel(null);
            }
        }
    }

    public void refreshUIPreferences() {
        if (SalsaUtils.isDefined(this.deviceName)) {
            UIPreferences.getInstance().addChartPropertiesMap(this.deviceName.toLowerCase(), getChartProperties());
            UIPreferences.getInstance().addImagePropertiesMap(this.deviceName.toLowerCase(), getImageProperties());
            UIPreferences.getInstance().setPlotPropertiesMap(getPlotPropertiesMap());
        }
    }

    private void setCompleteAttributeName() {
        if (commonBean != null) {
            commonBean.setCompleteAttributeName(deviceName);
        }
    }

    /**
     * Use of confirmation when changing the device value.
     * 
     * @param confirmation
     */
    public void setConfirmation(boolean confirmation) {
        if (commonBean != null) {
            commonBean.setConfirmation(confirmation);
        }
    }

    /**
     * Is the device read only.
     * 
     * @param readOnly
     *            the readOnly to set
     */
    public void setReadOnly(boolean readOnly) {
        if (commonBean != null) {
            commonBean.setEnabled(!readOnly);
        }
    }

    @Override
    public void preferenceChanged(UIPreferencesEvent event) {
        if (event != null) {
            switch (event.getType()) {
                case ENABLECONFIRMATION_CHANGED:
                    setConfirmation(UIPreferences.getInstance().isEnableConfirmation());
                    break;
                case CONTROL_PANEL_CHANGED:
                    setBatchFile(UIPreferences.getInstance().getControlPanel());
                    break;
                default:
                    break;
            }

        }
    }

}
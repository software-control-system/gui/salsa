package fr.soleil.salsa.client.controller.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.api.ScanApi;
import fr.soleil.salsa.client.controller.AErrorManagingController;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.client.controller.IGenericDeviceListController;
import fr.soleil.salsa.client.controller.IGenericDeviceListListener;
import fr.soleil.salsa.client.view.GenericDeviceListView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.event.source.IEventSource;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.tool.EventBusHandler;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * The list of controller of the device
 *
 * @author Tarek
 */
public abstract class GenericDeviceListController extends AErrorManagingController<GenericDeviceListView>
        implements IGenericDeviceListController {

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(GenericDeviceListController.class);

    protected static final String DEFAULT_SENSOR_NAME = ObjectUtils.EMPTY_STRING;

    /**
     * Redirects EntityPropertyChangedEvent on the config to DeviceListController.notifyConfigChangedEvent.
     */
    protected final IListener<EntityPropertyChangedEvent<IConfig<?>>> configListener;

    protected IGenericDeviceListListener listener = null;

    /**
     * The list of model
     */
    protected List<? extends IDevice> deviceList;

    /**
     * The config model
     */
    protected IConfig<?> config;

    /**
     * The device currently selected.
     */
    protected String[] selectedDevices;

    /**
     * The name of the scan server.
     */
    protected String scanServerName;

    protected boolean yActuator = false;

    /**
     * Create a new instance
     * 
     * @param view the view
     * @param config the config
     * @param errorController the error controller
     */
    protected GenericDeviceListController(final GenericDeviceListView view, final IConfig<?> config,
            final IErrorController errorController) {
        super(view, errorController);
        configListener = new ConfigListener();
        deviceList = null;
        scanServerName = null;
        this.config = null;
        setConfig(config);
        if (config == null) {
            // setDevice calls refresh if the device changes. Before setDevice is called, this.device is null.
            // So if the device is not null, setDevice has called refresh already.
            refresh();
        }

        updateSuggestionsList();
        EventBusHandler.getEventBus().register(this);
    }

    public void setAdminMode(final boolean adminMode) {
        if (view != null) {
            view.setAdminMode(adminMode);
        }
    }

    public void setManagerMode(final boolean managerMode) {
        if (view != null) {
            view.setManagerMode(managerMode);
        }
    }

    public void setConfig(final IConfig<?> config) {
        if (this.config != config) {
            removeListenersTo();
            this.config = config;
            addListenersTo();
            if (this.config == null) {
                deviceList = new ArrayList<>();
                if (view != null) {
                    view.clear();
                }
            } else {
                initDeviceList();
            }
            refresh();
        }
        setViewVisible(config != null);
        if (view != null) {
            view.setControlEnable(config != null);
        }
    }

    @Override
    public void notifyDeviceEnabled(final String deviceName, final boolean selected) {
        if (config != null) {
            IDevice device = config.getDevice(deviceName, deviceList);
            if (device != null) {
                device.setEnabled(selected);
                selectedDevices = new String[] { device.getName() };
            }
        }

        if (listener != null) {
            listener.deviceEnabled(deviceName, selected);
        }
    }

    protected IDevice getDeviceName(final int deviceIndex) {
        IDevice device = null;
        if (deviceList != null) {
            Object[] deviceArray = deviceList.toArray();
            Object object = null;
            if ((deviceIndex > -1) && (deviceIndex < deviceArray.length)) {
                object = deviceArray[deviceIndex];
                if (object instanceof IDevice) {
                    device = (IDevice) object;
                }
            }
        }
        return device;
    }

    protected String[] getDevices(int... positions) {
        String[] devices = new String[positions.length];
        for (int i = 0; i < positions.length; i++) {
            devices[i] = getDeviceName(getDeviceName(positions[i]));
        }
        return devices;
    }

    protected void changeDevicePositionWithoutSelection(int position, boolean up) {
        IDevice device1 = getDeviceName(position);
        IDevice device2 = getDeviceName(position + (up ? -1 : 1));
        if ((device1 != null) && (device2 != null)) {
            if (listener != null) {
                listener.deviceSwap(device1.getName(), device2.getName());
            }
            swapDevice(device1.getName(), device2.getName());
        }
    }

    @Override
    public void changeDevicePositions(boolean up, int... positions) {
        if ((positions != null) && positions.length > 0) {
            String[] selected = getDevices(positions);
            Arrays.sort(positions);
            int refPos;
            if (up) {
                refPos = -1;
            } else {
                refPos = 1;
                for (int i = 0; i < positions.length / 2; i++) {
                    int temp = positions[i];
                    positions[i] = positions[positions.length - i - 1];
                    positions[positions.length - i - 1] = temp;
                }
            }
            int[] devices = new int[positions.length * 2];
            int index = 0;
            for (int position : positions) {
                devices[2 * index] = position;
                devices[2 * index + 1] = position + refPos;
                index++;
            }
            selectedDevices = selected;
            for (int i = 0; i < devices.length / 2; i++) {
                IDevice device1 = getDeviceName(devices[2 * i]);
                IDevice device2 = getDeviceName(devices[2 * i + 1]);
                if ((device1 != null) && (device2 != null)) {
                    if (listener != null) {
                        listener.deviceSwap(device1.getName(), device2.getName());
                    }
                    swapDevice(device1.getName(), device2.getName());
                }
            }
        }
    }

    @Override
    public void changeDevicePositionsToExtreme(boolean top, int... positions) {
        if ((positions != null) && positions.length > 0) {
            String[] selected = getDevices(positions);
            Arrays.sort(positions);
            int[] refPositions = new int[positions.length];
            int refPos;
            if (top) {
                refPos = -1;
                for (int i = 0; i < positions.length; i++) {
                    refPositions[i] = i;
                }
            } else {
                refPos = 1;
                for (int i = 0; i < positions.length / 2; i++) {
                    int temp = positions[i];
                    positions[i] = positions[positions.length - i - 1];
                    positions[positions.length - i - 1] = temp;
                }
                for (int i = 0; i < positions.length; i++) {
                    refPositions[i] = deviceList.size() - (i + 1);
                }
            }
            selectedDevices = selected;
            int index = 0;
            String[] devices = new String[positions.length];
            for (int position : positions) {
                IDevice device1 = getDeviceName(position);
                devices[index++] = getDeviceName(device1);
            }
            index = 0;
            for (String device : devices) {
                int goal = refPositions[index];
                int tmp = positions[index++];
                if (device != null) {
                    List<String> devicesToSwap = new LinkedList<>();
                    while (tmp != goal) {
                        tmp += refPos;
                        IDevice device2 = getDeviceName(tmp);
                        if (device2 != null) {
                            devicesToSwap.add(device2.getName());
                        }
                    }
                    while (!devicesToSwap.isEmpty()) {
                        String swap = devicesToSwap.remove(0);
                        if (listener != null) {
                            listener.deviceSwap(device, swap);
                        }
                        swapDevice(device, swap);
                    }
                }
            }
        }
    }

    @Override
    public void switchDevicePositions(int... positions) {
        if ((positions != null) && positions.length > 0) {
            String[] selected = new String[positions.length];
            for (int i = 0; i < positions.length; i++) {
                selected[i] = getDeviceName(getDeviceName(positions[i]));
            }
            selectedDevices = selected;
            for (int i = 0; i < positions.length / 2; i++) {
                IDevice device1 = getDeviceName(positions[2 * i]);
                IDevice device2 = getDeviceName(positions[2 * i + 1]);
                if ((device1 != null) && (device2 != null)) {
                    if (listener != null) {
                        listener.deviceSwap(device1.getName(), device2.getName());
                    }
                    swapDevice(device1.getName(), device2.getName());
                }
            }
        }
    }

    @Override
    public void reversePositions(int... positions) {
        if ((positions != null) && positions.length > 0) {
            String[] selected = new String[positions.length];
            for (int i = 0; i < positions.length; i++) {
                selected[i] = getDeviceName(getDeviceName(positions[i]));
            }
            Arrays.sort(positions);
            boolean contiguous = true;
            for (int i = 1; i < positions.length && contiguous; i++) {
                if (positions[i] != positions[i - 1] + 1) {
                    contiguous = false;
                }
            }
            if (contiguous) {
                selectedDevices = selected;
                for (int i = 0; i < positions.length / 2; i++) {
                    IDevice device1 = getDeviceName(positions[i]);
                    IDevice device2 = getDeviceName(positions[positions.length - i - 1]);
                    if ((device1 != null) && (device2 != null)) {
                        if (listener != null) {
                            listener.deviceSwap(device1.getName(), device2.getName());
                        }
                        swapDevice(device1.getName(), device2.getName());
                    }
                }
            }
        }
    }

    protected abstract void swapDevice(String deviceName1, String deviceName2);

    protected abstract void initDeviceList();

    @Override
    protected GenericDeviceListView generateView() {
        return new GenericDeviceListView(this);
    }

    public void setDeviceList(final List<? extends IDevice> newDeviceList) {
        deviceList = newDeviceList;
        setViewVisible(true);
        updateSuggestionsList();
        refresh();
    }

    protected String getDeviceName(IDevice device) {
        return device == null ? null : device.getName();
    }

    /**
     * Refreshes the view.
     */
    protected void refresh() {
        // Add the list devices for the model list
        if (deviceList != null && view != null) {
            view.setControlEnable(true);
            deviceErrorMessage(true, view.addRowInModel(deviceList));
            boolean stillThere = false;
            if (SalsaUtils.isFulfilled(deviceList)) {
                List<String> devices = new ArrayList<>();
                if (selectedDevices != null) {
                    for (IDevice deviceSearch : deviceList) {
                        for (String device : selectedDevices) {
                            if ((device != null) && (deviceSearch != null) && device.equals(deviceSearch.getName())) {
                                stillThere = true;
                                devices.add(device);
                                break;
                            }
                        }
                    }
                }
                if (stillThere) {
                    selectedDevices = devices.toArray(new String[devices.size()]);
                    view.setSelectedDevices(selectedDevices);
                } else {
                    selectedDevices = new String[] { getDeviceName(deviceList.get(0)) };
                    view.setSelectedRows(0);
                }
            } else {
                selectedDevices = null;
            }
        } else {
            refreshWhenConfigIsNull();
        }
    }

    protected void refreshWhenConfigIsNull() {
        // nothing to do by default
    }

    public void notifyConfigChangedEvent(final EntityPropertyChangedEvent<IConfig<?>> event) {
        boolean changeOccurred = false;
        for (EntityPropertyChangedEvent<IConfig<?>>.PropertyChange<?> propertyChange : event.getPropertyChangeList()) {
            if (isPropertyChange(propertyChange.getPropertyName())) {
                changeOccurred = true;
                break;
            }
        }
        if (changeOccurred) {
            refresh();
        }
    }

    protected abstract boolean isPropertyChange(String propertyName);

    /**
     * Return the button "new" for create a new element
     */
    @Override
    public void notifyNewAction(final String value) {
        if (listener != null) {
            listener.deviceAdded(value);
        }
        if (config != null) {
            updateDeviceList(false);
            if (canAdd()) {
                newDevice(value);
                config.setModified(true);
            }
            refresh();
        }
    }

    private String newDevice(final String value) {
        String name = value;

        IDevice device = generateDevice();
        device.setName(name);
        // JIRA SCAN-375
        device.setEnabled(false);

        boolean addSucces = view.addRow(device);
        if (addSucces) {
            addDevice(device);
        } else if (!view.isNullModel()) {
            deviceErrorMessage(true, device.getName());
        }

        return name;
    }

    protected abstract boolean addDevice(IDevice device);

    @Override
    public void notifyNewAction(final List<String> deviceNameList) {
        if (listener != null) {
            listener.devicesAdded(deviceNameList);
        }
        if ((config != null) && (deviceNameList != null)) {
            for (String deviceName : deviceNameList) {
                notifyNewAction(deviceName);
            }
        }
    }

    protected boolean canAdd() {
        // true by default
        return true;
    }

    /**
     * Return the button "delete" for remove of the list
     */
    @Override
    public void notifyDeleteAction() {
        if (deviceList != null) {
            updateDeviceList(false);
            int[] indexToRemove = view.getSelectedRows();
            if ((indexToRemove != null) && (indexToRemove.length > 0)) {
                List<IDevice> deviceToRemove = new ArrayList<>();
                for (int deviceIndex : indexToRemove) {
                    deviceToRemove.add(deviceList.get(deviceIndex));
                }

                notifyDeleteAction(deviceToRemove);

                for (int deviceIndex : indexToRemove) {
                    view.removeRow(deviceIndex);
                }
                if (config != null) {
                    config.setModified(true);
                }
                refresh();
            }
        }
    }

    protected void notifyDeleteAction(final List<IDevice> deviceToRemove) {
        if ((deviceList != null) && (deviceToRemove != null)) {
            for (IDevice dev : deviceToRemove) {
                deviceList.remove(dev);
                if (listener != null) {
                    listener.deviceRemoved(dev.getName());
                }
            }
        }
    }

    /**
     * update the device list from the model
     *
     * @param checkForDuplicates
     *            A boolean to check or not for duplicates. If <code>true</code> and a duplicate device name is found,
     *            this controller will try
     *            to cancel this duplication. This is mainly used for device
     *            renaming. If <code>false</code>, this controller won't check
     *            for duplicates.
     */
    protected void updateDeviceList(final boolean checkForDuplicates) {
        if ((deviceList != null) && (view.getModel().getRowCount() > 0)) {
            ArrayList<String> canceledNames = new ArrayList<>();
            IDevice deviceInModel = null;
            IDevice deviceInView = null;
            String deviceNameInModel = null;
            String deviceNameInView = null;
            Boolean isEnabledInView = false;
            boolean isCommonInView = false;
            boolean addDevice = false;
            for (int i = 0; i < view.getModel().getRowCount(); i++) {
                addDevice = false;
                if (i < deviceList.size()) {
                    deviceInModel = deviceList.get(i);
                    deviceNameInModel = deviceInModel.getName();
                } else {
                    addDevice = true;
                    deviceInModel = generateDevice();
                }

                deviceInView = (IDevice) view.getModel().getValueAt(i, 1);
                deviceNameInView = deviceInView.getName();
                isEnabledInView = (Boolean) view.getModel().getValueAt(i, 0);
                isCommonInView = deviceInView.isCommon();

                deviceInModel.setName(deviceNameInView);
                deviceInModel.setEnabled(isEnabledInView);
                deviceInModel.setCommon(isCommonInView);

                if (addDevice) {
                    addDevice(deviceInModel);
                } else if (checkForDuplicates && (!isDeviceNameAllowed(deviceInModel, deviceList))) {
                    canceledNames.add(deviceNameInView);
                    deviceList.get(i).setName(deviceNameInModel);
                    view.getModel().setValueAt(deviceInModel, i, 1);
                }
            }
            deviceErrorMessage(false, canceledNames.toArray(new String[canceledNames.size()]));
            canceledNames.clear();
        }
    }

    protected abstract IDevice generateDevice();

    protected boolean isDeviceNameAllowed(final IDevice device, final List<? extends IDevice> devicesList) {
        boolean allowed = true;
        String name = device.getName();
        for (IDevice deviceSearch : devicesList) {
            if (deviceSearch.getName().equalsIgnoreCase(name) && (device != deviceSearch)) {
                allowed = false;
                break;
            }
        }
        return allowed;
    }

    /**
     * Loads the suggestions from the scan server when the scan server changes
     * and at initialization.
     */
    protected void updateSuggestionsList() {
        String newScanServerName = getScanServer();
        if (SalsaUtils.isDefined(newScanServerName) && (!newScanServerName.equals(this.scanServerName))) {
            this.scanServerName = newScanServerName;
            try {
                ISuggestions suggestions = ScanApi.getDevicesSuggestions(scanServerName);
                List<ISuggestionCategory> categories = getCategories(suggestions);
                view.setSuggestions(categories);
            } catch (SalsaDeviceException exception) {
                errorMessage(exception.getMessage());
            }
        }
    }

    protected abstract List<ISuggestionCategory> getCategories(ISuggestions suggestions);

    /**
     * Invoked when a device preference event occurs in the event bus
     * 
     * @param devicePreferencesEvent the device preferences events
     */
    @Subscribe
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        if (devicePreferences.equals(devicePreferencesEvent.getSource())
                && devicePreferencesEvent.getType().equals(DevicePreferencesEvent.Type.SCANSERVER_CHANGED)) {
            updateSuggestionsList();
        }
    }

    protected void deviceErrorMessage(final boolean removed, final String... deviceNames) {
        if ((deviceNames != null) && (deviceNames.length > 0)) {
            StringBuilder builder = new StringBuilder();
            boolean oneDevice = (deviceNames.length == 1);
            builder.append("Following device");
            if (!oneDevice) {
                builder.append("s");
            }
            builder.append(" already exist");
            if (oneDevice) {
                builder.append("s");
            }
            builder.append(" in list. Therefore, ");
            if (oneDevice) {
                builder.append("its double was");
            } else {
                builder.append("their doubles were");
            }
            if (removed) {
                builder.append(" removed");
            } else {
                builder.append(" canceled");
            }
            builder.append(":\n");
            for (String name : deviceNames) {
                builder.append("'").append(name).append("', ");
            }
            builder.delete(builder.length() - 2, builder.length());
            errorMessage(builder.toString());
        }
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Redirects EntityPropertyChangedEvent on the config to
     * DeviceListController.notifyConfigChangedEvent.
     */
    protected class ConfigListener implements IListener<EntityPropertyChangedEvent<IConfig<?>>> {
        @Override
        public void notifyEvent(final EntityPropertyChangedEvent<IConfig<?>> event) {
            notifyConfigChangedEvent(event);
        }
    }

    @Override
    public void notifyDeviceSelectedAll(final boolean selected) {
        if (deviceList != null) {
            for (IDevice device : deviceList) {
                device.setEnabled(selected);
                if (listener != null) {
                    listener.deviceEnabled(device.getName(), selected);
                }
            }
            refresh();
            if (config != null) {
                config.setModified(true);
            }
        }
    }

    protected IDevice getDeviceForName(final String deviceName) {
        IDevice device = null;
        if ((deviceList != null) && (deviceName != null)) {
            for (IDevice tmpDevice : deviceList) {
                if (tmpDevice.getName().equalsIgnoreCase(deviceName)) {
                    device = tmpDevice;
                    break;
                }
            }
        }
        return device;
    }

    /**
     * Gets the type of the actuator. <code>true</code> = Y actuator.
     * <code>false</code> = X actuator.
     *
     * @return a boolean value
     */
    public boolean isYActuator() {
        return yActuator;
    }

    /**
     * Sets the actuator to Y one.
     *
     * @param actuator
     *            <code>true</code> = Y actuator. <code>false</code> = X
     *            actuator.
     */
    public void setYActuator(final boolean actuator) {
        yActuator = actuator;
    }

    /**
     * Registers this controller as listener to the config and its devices.
     *
     * @param config
     */
    @SuppressWarnings("unchecked")
    private void addListenersTo() {
        if (config instanceof IEventSource) {
            ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) config).addListener(configListener);
            deviceList = config.getSensorsList();
        }
    }

    /**
     * Remove this controller as listener to the config and its devices.
     *
     * @param config
     */
    @SuppressWarnings("unchecked")
    private void removeListenersTo() {
        if (config instanceof IEventSource) {
            ((IEventSource<EntityPropertyChangedEvent<IConfig<?>>>) config).removeListener(configListener);
        }
    }

    public IConfig<?> getConfig() {
        return config;
    }

    @Override
    public void addGenericDeviceListListener(final IGenericDeviceListListener listener) {
        this.listener = listener;

    }

    @Override
    public void removeGenericDeviceListListener(final IGenericDeviceListListener listener) {
        this.listener = null;
    }

}

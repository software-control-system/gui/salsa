package fr.soleil.salsa.client.controller;

import java.util.List;

import fr.soleil.salsa.client.view.GenericDeviceListView;

public interface IGenericDeviceListController extends IController<GenericDeviceListView> {

    public void addGenericDeviceListListener(IGenericDeviceListListener listener);

    public void removeGenericDeviceListListener(IGenericDeviceListListener listener);

    public void notifyNewAction(String value);

    public void notifyNewAction(List<String> deviceNameList);

    public void notifyDeleteAction();

    public void changeDevicePositions(boolean up, int... positions);

    public void changeDevicePositionsToExtreme(boolean top, int... positions);

    public void switchDevicePositions(int... positions);

    public void reversePositions(int... positions);

    public void notifyDeviceSelectedAll(boolean selected);

    public void notifyDeviceEnabled(String deviceName, boolean selected);

    public void notifyDeviceRenamed(int deviceIndex, String newDeviceName);

}

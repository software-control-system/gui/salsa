package fr.soleil.salsa.client.controller.impl;

import java.util.List;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDevice;
import fr.soleil.salsa.entity.ISensor;
import fr.soleil.salsa.entity.ISuggestionCategory;
import fr.soleil.salsa.entity.ISuggestions;
import fr.soleil.salsa.entity.event.SensorModel;

public class GenericSensorListController extends GenericDeviceListController {

    public GenericSensorListController() {
        this(null, null);
    }

    public GenericSensorListController(IConfig<?> config, IErrorController errorController) {
        super(null, config, errorController);
    }

    @Override
    protected IDevice generateDevice() {
        return new SensorModel();
    }

    @Override
    protected List<ISuggestionCategory> getCategories(ISuggestions suggestions) {
        return suggestions.getSensorSuggestionList();
    }

    @Override
    protected boolean isPropertyChange(String propertyName) {
        boolean change = false;
        if ((propertyName != null) && propertyName.toLowerCase().contains("sensor")) {
            change = true;
        }
        return change;
    }

    @Override
    protected void initDeviceList() {
        if (config != null) {
            this.deviceList = config.getSensorsList();
        }
    }

    @Override
    public void notifyDeviceEnabled(String deviceName, boolean selected) {
        if (config != null) {
            config.setSensorEnable(deviceName, selected);
        }
        super.notifyDeviceEnabled(deviceName, selected);
    }

    @Override
    public void notifyDeviceRenamed(int deviceIndex, String newDeviceName) {
        IDevice oldSensor = getDeviceName(deviceIndex);
        if ((oldSensor != null) && !ObjectUtils.sameObject(oldSensor.getName(), newDeviceName)) {
            String oldSensorName = oldSensor.getName();
            if (listener != null) {
                listener.deviceRenamed(oldSensorName, newDeviceName);
            }
            if (config != null) {
                config.renameSensor(oldSensorName, newDeviceName);
            }
        }
    }

    @Override
    protected void swapDevice(String deviceName1, String deviceName2) {
        if (this.config != null) {
            config.swapSensor(deviceName1, deviceName2);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected boolean addDevice(IDevice device) {
        boolean result = false;
        if ((deviceList != null) && (device instanceof ISensor)) {
            result = ((List<ISensor>) this.deviceList).add((ISensor) device);
        }
        return result;
    }


}

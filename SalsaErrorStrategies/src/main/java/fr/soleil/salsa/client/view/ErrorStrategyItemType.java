package fr.soleil.salsa.client.view;

public enum ErrorStrategyItemType {
    actuators, hooks, sensors, timebases
}

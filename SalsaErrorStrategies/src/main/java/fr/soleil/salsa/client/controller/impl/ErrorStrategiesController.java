package fr.soleil.salsa.client.controller.impl;

import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IErrorStrategiesController;
import fr.soleil.salsa.client.view.ErrorStrategiesView;
import fr.soleil.salsa.client.view.ErrorStrategyItemType;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IErrorStrategy;
import fr.soleil.salsa.entity.IErrorStrategyItem;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.event.ErrorStrategyItemModel;
import fr.soleil.salsa.entity.event.ErrorStrategyModel;
import fr.soleil.salsa.entity.event.ScanAddOnModel;
import fr.soleil.salsa.entity.impl.ErrorStrategyType;
import fr.soleil.salsa.exception.ScanNotFoundException;

public class ErrorStrategiesController extends AController<ErrorStrategiesView> implements IErrorStrategiesController {

    public final static double TIME_OUT_DEFAULT = 5;
    public final static double TIME_BETWEEN_RETRIES_DEFAULT = 0.2;
    public final static int RETRY_COUNT_DEFAULT = 5;
    public final static String CONTEXT_VALIDATION_ATTRIBUTE_DEFAULT = "";// de/vi/ce/attribute";
    private IErrorStrategy errorStrategy = null;
    private IErrorStrategy dberrorStrategy = null;
    private IConfig<?> config = null;

    public ErrorStrategiesController() {
        this(null);
    }

    public ErrorStrategiesController(final ErrorStrategiesView view) {
        this(view, false);
    }

    public ErrorStrategiesController(final ErrorStrategiesView view, final boolean ro) {
        super(view, ro);
    }

    @Override
    public void setView(final ErrorStrategiesView view) {
        if (view != null) {
            super.setView(view);
        }
    }

    @Override
    protected ErrorStrategiesView generateView() {
        ErrorStrategiesView aView = new ErrorStrategiesView(this);
        if (readOnly) {
            aView.setEditable(false);
        }
        return aView;
    }

    public void setConfig(final IConfig<?> aconfig) {
        if (view != null) {
            view.setEnabled(false);
            view.enableListener(false);
            view.clearValue();
        }

        this.config = aconfig;
        errorStrategy = null;
        dberrorStrategy = null;
        if (config != null) {
            try {
                IConfig<?> dbconfig = SalsaAPI.getConfigById(config.getId(), true);
                IScanAddOns scanAddOns = dbconfig.getScanAddOn();
                if (scanAddOns != null) {
                    dberrorStrategy = scanAddOns.getErrorStrategy();
                }
            } catch (ScanNotFoundException e) {
            }

            IScanAddOns scanAddOns = config.getScanAddOn();
            if (scanAddOns == null) {
                scanAddOns = new ScanAddOnModel();
                config.setScanAddOn(scanAddOns);
            }

            errorStrategy = scanAddOns.getErrorStrategy();
            if (errorStrategy == null) {
                errorStrategy = new ErrorStrategyModel();
                scanAddOns.setErrorStrategy(errorStrategy);
                errorStrategy.setContextValidationStrategy(ErrorStrategyType.IGNORE);
                errorStrategy.setContextValidationDevice(CONTEXT_VALIDATION_ATTRIBUTE_DEFAULT);
            }

            if (errorStrategy.getActuatorsErrorStrategy() == null) {
                errorStrategy.setActuatorsErrorStrategy(new ErrorStrategyItemModel());
                setDefaultValues(errorStrategy.getActuatorsErrorStrategy());
            }
            if (errorStrategy.getSensorsErrorStrategy() == null) {
                errorStrategy.setSensorsErrorStrategy(new ErrorStrategyItemModel());
                setDefaultValues(errorStrategy.getSensorsErrorStrategy());
            }
            if (errorStrategy.getHooksErrorStrategy() == null) {
                errorStrategy.setHooksErrorStrategy(new ErrorStrategyItemModel());
                setDefaultValues(errorStrategy.getHooksErrorStrategy());
            }

            if (errorStrategy.getTimebasesErrorStrategy() == null) {
                errorStrategy.setTimebasesErrorStrategy(new ErrorStrategyItemModel());
                setDefaultValues(errorStrategy.getTimebasesErrorStrategy());
            }
            refresh();
        }

        if (view != null) {
            view.setEnabled(config != null);
            view.enableListener(config != null);
        }
    }

    private IErrorStrategyItem findErrorStrategyItem(final ErrorStrategyItemType esType) {
        IErrorStrategyItem result = null;
        if (errorStrategy != null) {
            if (esType == ErrorStrategyItemType.sensors) {
                result = errorStrategy.getSensorsErrorStrategy();
            } else if (esType == ErrorStrategyItemType.actuators) {
                result = errorStrategy.getActuatorsErrorStrategy();
            } else if (esType == ErrorStrategyItemType.hooks) {
                result = errorStrategy.getHooksErrorStrategy();
            } else if (esType == ErrorStrategyItemType.timebases) {
                result = errorStrategy.getTimebasesErrorStrategy();
            }
        }
        return result;
    }

    @Override
    public void notifyStrategySelectedChanged(final ErrorStrategyItemType esType, final String value) {
        IErrorStrategyItem esItem = findErrorStrategyItem(esType);
        if (esItem != null) {
            ErrorStrategyType oldType = esItem.getStrategy();
            ErrorStrategyType newType = ErrorStrategyType.valueOf(ErrorStrategyType.class, value);
            if (oldType != newType) {
                esItem.setStrategy(newType);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyTimeBetweenRetriesChanged(final ErrorStrategyItemType esType, final String value) {
        IErrorStrategyItem esItem = findErrorStrategyItem(esType);
        if (esItem != null) {
            double oldValue = esItem.getTimeBetweenRetries();
            try {
                double d = Double.parseDouble(value);
                if (d != oldValue) {
                    esItem.setTimeBetweenRetries(d);
                    if (config != null) {
                        config.setModified(true);
                    }
                }
            } catch (NumberFormatException nfe) {
                view.setTimeBetweenRetries(esType, oldValue, false);
            }
        }
        refresh();
    }

    @Override
    public void notifyRetryCountChanged(final ErrorStrategyItemType esType, final String value) {
        IErrorStrategyItem esItem = findErrorStrategyItem(esType);
        if (esItem != null) {
            int oldValue = esItem.getRetryCount();
            try {
                int n = Integer.parseInt(value);
                if (oldValue != n) {
                    esItem.setRetryCount(n);
                    if (config != null) {
                        config.setModified(true);
                    }
                }
            } catch (NumberFormatException nfe) {
                view.setRetryCount(esType, oldValue, false);
            }
        }
        refresh();
    }

    @Override
    public void notifyTimeOutChanged(final ErrorStrategyItemType esType, final String value) {
        IErrorStrategyItem esItem = findErrorStrategyItem(esType);
        if (esItem != null) {
            double oldValue = esItem.getTimeOut();
            try {
                double d = Double.parseDouble(value);
                if (d != oldValue) {
                    esItem.setTimeOut(d);
                    if (config != null) {
                        config.setModified(true);
                    }
                }
            } catch (NumberFormatException nfe) {
                view.setTimeOut(esType, oldValue, false);
            }
        }
        refresh();
    }

    @Override
    public void notifyContextValidationAttributeChanged(String value) {
        if (errorStrategy != null) {
            String oldValue = errorStrategy.getContextValidationDevice();
            if (oldValue == null) {
                oldValue = "";
            }
            if (value == null) {
                value = "";
            }

            if (!oldValue.equalsIgnoreCase(value)) {
                errorStrategy.setContextValidationDevice(value);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    @Override
    public void notifyContextValidationStrategyChanged(final String value) {
        if ((value != null) && (errorStrategy != null)) {
            ErrorStrategyType oldValue = errorStrategy.getContextValidationStrategy();
            ErrorStrategyType est = ErrorStrategyType.valueOf(value);
            if (oldValue != est) {
                errorStrategy.setContextValidationStrategy(est);
                if (config != null) {
                    config.setModified(true);
                }
            }
        }
        refresh();
    }

    private void refresh() {
        // set the current configuration value
        refresh(false);
        // set the database configuration value
        refresh(true);
    }

    private void refresh(final boolean saved) {

        IErrorStrategy currentErrorStrategy = errorStrategy;
        if (saved) {
            currentErrorStrategy = dberrorStrategy;
        }

        if ((currentErrorStrategy != null) && (view != null) && (config != null)) {
            view.enableListener(false);
            refreshErrorStrategyItem(ErrorStrategyItemType.sensors, currentErrorStrategy.getSensorsErrorStrategy(),
                    saved);
            refreshErrorStrategyItem(ErrorStrategyItemType.actuators, currentErrorStrategy.getActuatorsErrorStrategy(),
                    saved);
            refreshErrorStrategyItem(ErrorStrategyItemType.hooks, currentErrorStrategy.getHooksErrorStrategy(), saved);
            refreshErrorStrategyItem(ErrorStrategyItemType.timebases, currentErrorStrategy.getTimebasesErrorStrategy(),
                    saved);

            /* Context validation */
            String contextValidation = currentErrorStrategy.getContextValidationDevice();
            view.setContextValidationAttribute(contextValidation, saved);

            if (!saved) {
                ErrorStrategyType errorStrategy = currentErrorStrategy.getContextValidationStrategy();
                if (errorStrategy != null) {
                    view.setContextValidationStrategy(errorStrategy.name());
                }
            }
            view.enableListener(true);
        }
    }

    private void refreshErrorStrategyItem(final ErrorStrategyItemType itemType, final IErrorStrategyItem item, final boolean saved) {
        if ((itemType != null) && (item != null)) {
            view.setTimeOut(itemType, item.getTimeOut(), saved);
            view.setRetryCount(itemType, item.getRetryCount(), saved);
            view.setTimeBetweenRetries(itemType, item.getTimeBetweenRetries(), saved);
            if (!saved) {
                if (item.getStrategy() != null) {
                    view.setStrategy(itemType, item.getStrategy().name());
                }
            }
        }
    }

    private void setDefaultValues(final IErrorStrategyItem est) {
        est.setRetryCount(RETRY_COUNT_DEFAULT);
        est.setTimeOut(TIME_OUT_DEFAULT);
        est.setTimeBetweenRetries(TIME_BETWEEN_RETRIES_DEFAULT);
        est.setStrategy(ErrorStrategyType.ABORT);
    }

}

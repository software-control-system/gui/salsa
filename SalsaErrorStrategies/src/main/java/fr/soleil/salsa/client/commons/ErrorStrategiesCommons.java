package fr.soleil.salsa.client.commons;

public class ErrorStrategiesCommons {

    /**
     * Text to show as actuator name when there is no sensor to show.
     */
    public static final String SENSORS_TITLE = "sensors";
    public static final String ACTUATORS_TITLE = "actuators";
    public static final String TIMEBASES_TITLE  = "timebases";
    public static final String HOOKS_TITLE  = "hooks";
    public static final String CONTEXT_VALIDATION_TITLE  = "context validation";

}

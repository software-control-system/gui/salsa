package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.ErrorStrategiesView;
import fr.soleil.salsa.client.view.ErrorStrategyItemType;

public interface IErrorStrategiesController extends IController<ErrorStrategiesView> {

    void notifyStrategySelectedChanged(ErrorStrategyItemType esType, String value);

    void notifyTimeBetweenRetriesChanged(ErrorStrategyItemType esType, String value);

    void notifyRetryCountChanged(ErrorStrategyItemType esType, String value);

    void notifyTimeOutChanged(ErrorStrategyItemType esType, String value);

    void notifyContextValidationStrategyChanged(String value);

    void notifyContextValidationAttributeChanged(String value);

}

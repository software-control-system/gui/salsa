package fr.soleil.salsa.client.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ITextTransferable;
import fr.soleil.lib.project.swing.TextTansfertHandler;
import fr.soleil.salsa.client.controller.IErrorStrategiesController;
import fr.soleil.salsa.client.util.BackgroundRenderer;

public class ContextValidationView extends JPanel implements ITextTransferable {

    private static final long serialVersionUID = 684987596512310643L;

    private JComboBox<String> cbStrategy = null;
    private JLabel lblAttribute = null;
    private JLabel lblStrategy = null;
    private JTextField txtAttribute = null;

    private String attributeValue = null;
    private String dbAttributeValue = null;
    protected IErrorStrategiesController controller;

    private final ActionListener alistener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getModifiers() != 0) {
                handleStrategySelectedChanged();
            }
        }
    };

    private final KeyListener keyListener = new KeyAdapter() {

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                handleAttributeChanged();
            }
            setBackgroundAttribute();
        }

    };

    private final FocusListener flistener = new FocusAdapter() {

        @Override
        public void focusLost(FocusEvent e) {
            handleAttributeChanged();
            setBackgroundAttribute();
        }

    };

    public ContextValidationView(IErrorStrategiesController controller) {
        super();
        this.controller = controller;
        initComponents();
    }

    public IErrorStrategiesController getController() {
        return controller;
    }

    public void setController(IErrorStrategiesController controller) {
        this.controller = controller;
    }

    public void enableListener(boolean enabled) {
        if (enabled) {
            txtAttribute.addKeyListener(keyListener);
            cbStrategy.addActionListener(alistener);
            txtAttribute.addFocusListener(flistener);
        } else {
            txtAttribute.removeKeyListener(keyListener);
            cbStrategy.removeActionListener(alistener);
            txtAttribute.removeFocusListener(flistener);
        }
    }

    private void handleAttributeChanged() {
        if (controller != null) {
            attributeValue = txtAttribute.getText();
            controller.notifyContextValidationAttributeChanged(attributeValue);
        }
    }

    private void handleStrategySelectedChanged() {
        Object item = cbStrategy.getSelectedItem();
        String value = null;
        if (item != null) {
            value = item.toString();
        }
        if (controller != null) {
            controller.notifyContextValidationStrategyChanged(value);
        }
    }

    private void initComponents() {

        // Initialization components
        lblAttribute = new JLabel("Attribute", JLabel.RIGHT);
        txtAttribute = new JTextField();
        TextTansfertHandler.registerTransferHandler(this);

        lblStrategy = new JLabel("Strategy", JLabel.RIGHT);
        cbStrategy = new JComboBox<>();
        cbStrategy.addItem("IGNORE");
        cbStrategy.addItem("PAUSE");
        cbStrategy.addItem("ABORT");

        //
        setLayout(new GridBagLayout());

        // First line
        GridBagConstraints errorDeviceLabelConstraints = new GridBagConstraints();
        errorDeviceLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorDeviceLabelConstraints.gridx = 0;
        errorDeviceLabelConstraints.gridy = 0;
        errorDeviceLabelConstraints.weightx = 0;
        errorDeviceLabelConstraints.weighty = 0;
        errorDeviceLabelConstraints.insets = new Insets(5, 5, 5, 2);
        this.add(lblAttribute, errorDeviceLabelConstraints);

        GridBagConstraints errorDeviceEditorConstraints = new GridBagConstraints();
        errorDeviceEditorConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorDeviceEditorConstraints.gridx = 1;
        errorDeviceEditorConstraints.gridy = 0;
        errorDeviceEditorConstraints.weightx = 1;
        errorDeviceEditorConstraints.weighty = 0;
        errorDeviceEditorConstraints.gridwidth = GridBagConstraints.REMAINDER;
        errorDeviceEditorConstraints.insets = new Insets(5, 10, 0, 400);
        this.add(txtAttribute, errorDeviceEditorConstraints);
        setBackgroundAttribute();

        // Second line
        GridBagConstraints errorStrategyLabelConstraints = new GridBagConstraints();
        errorStrategyLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorStrategyLabelConstraints.gridx = 0;
        errorStrategyLabelConstraints.gridy = 1;
        errorStrategyLabelConstraints.weightx = 0;
        errorStrategyLabelConstraints.weighty = 1;
        errorStrategyLabelConstraints.insets = new Insets(5, 5, 5, 2);
        this.add(lblStrategy, errorStrategyLabelConstraints);

        GridBagConstraints errorStrategyEditorConstraints = new GridBagConstraints();
        errorStrategyEditorConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorStrategyEditorConstraints.gridx = 1;
        errorStrategyEditorConstraints.gridy = 1;
        errorStrategyEditorConstraints.weightx = 1;
        errorStrategyEditorConstraints.weighty = 1;
        errorStrategyEditorConstraints.insets = new Insets(5, 10, 2, 400);
        this.add(cbStrategy, errorStrategyEditorConstraints);

    }

    public void setAttribute(String value, boolean savedValue) {
        if (value == null) {
            value = "";
        }
        if (savedValue) {
            this.dbAttributeValue = value;
        } else {
            this.attributeValue = value;
            txtAttribute.setText(attributeValue);
        }
        setBackgroundAttribute();
    }

    private void setBackgroundAttribute() {
        BackgroundRenderer.setBackgroundTextField(this.txtAttribute, attributeValue, dbAttributeValue);
    }

    public void setStrategy(String value) {
        if (value != null) {
            cbStrategy.setSelectedItem(value);
        }
    }

    public void setEditable(boolean arg0) {
        txtAttribute.setEditable(arg0);
        cbStrategy.setEnabled(arg0);
    }

    public void clearValue() {
        txtAttribute.setText(ObjectUtils.EMPTY_STRING);
        txtAttribute.setBackground(Color.WHITE);
    }

    @Override
    public String getTextToTransfert() {
        String textToTransfert = null;
        if (txtAttribute != null) {
            attributeValue = txtAttribute.getText().trim();
            textToTransfert = TangoDeviceHelper.getDeviceName(attributeValue);
        }
        return textToTransfert;
    }

}

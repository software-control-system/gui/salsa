package fr.soleil.salsa.client.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.soleil.salsa.client.controller.IErrorStrategiesController;
import fr.soleil.salsa.client.util.BackgroundRenderer;
import fr.soleil.salsa.entity.impl.ErrorStrategyType;

public class StrategiesTypeView extends JPanel {

    private static final long serialVersionUID = -1708148362115618063L;

    private JComboBox<String> cbStrategy = null;
    private IErrorStrategiesController controller = null;
    private final ErrorStrategyItemType esType;
    private JLabel retryCountLabel = null;
    private JLabel retryTimeOutLabel = null;
    private JLabel retryTimeOutUnitLabel = null;
    private JLabel strategyLabel = null;
    private JLabel timeOutLabel = null;
    private JLabel timeOutUnitLabel = null;
    private JTextField txtRetryCount = null;
    private JTextField txtTimeBetweenRetries = null;
    private JTextField txtTimeOut = null;

    private String retryCount = null;
    private String timeBetweenRetries = null;
    private String timeOut = null;

    private String dbretryCount = null;
    private String dbtimeBetweenRetries = null;
    private String dbtimeOut = null;

    private ActionListener alistener = null;
    private KeyListener keyListener = null;
    private FocusListener flistener = null;

    /**
     * The construct
     * 
     * @param errorStrategiesController
     */
    public StrategiesTypeView(IErrorStrategiesController controller, ErrorStrategyItemType esType) {
        this.controller = controller;
        this.esType = esType;
        alistener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getModifiers() != 0) {
                    handleStrategySelectedChanged();
                }
            }
        };

        keyListener = new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                    if (e.getSource() == txtTimeOut) {
                        handleTimeOutChanged();
                    } else if (e.getSource() == txtRetryCount) {
                        handleRetryCountChanged();
                    } else if (e.getSource() == txtTimeBetweenRetries) {
                        handleTimeBetweenRetriesChanged();
                    }
                }
                if (e.getSource() == txtTimeOut) {
                    setBackgroundTimeOut();
                } else if (e.getSource() == txtRetryCount) {
                    setBackgroundRetryCount();
                } else if (e.getSource() == txtTimeBetweenRetries) {
                    setBackgroundTimeBetweenRetries();
                }
            }

        };

        flistener = new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
                if (e.getSource() == txtTimeOut) {
                    handleTimeOutChanged();
                    setBackgroundTimeOut();
                } else if (e.getSource() == txtRetryCount) {
                    handleRetryCountChanged();
                    setBackgroundRetryCount();
                } else if (e.getSource() == txtTimeBetweenRetries) {
                    handleTimeBetweenRetriesChanged();
                    setBackgroundTimeBetweenRetries();
                }
            }

        };

        initializeLayout();
        clearValue();
    }

    public void enableListener(boolean enabled) {
        if (enabled) {
            txtTimeOut.addKeyListener(keyListener);
            txtRetryCount.addKeyListener(keyListener);
            txtTimeBetweenRetries.addKeyListener(keyListener);
            cbStrategy.addActionListener(alistener);

            txtTimeOut.addFocusListener(flistener);
            txtRetryCount.addFocusListener(flistener);
            txtTimeBetweenRetries.addFocusListener(flistener);
        } else {
            txtTimeOut.removeKeyListener(keyListener);
            txtRetryCount.removeKeyListener(keyListener);
            txtTimeBetweenRetries.removeKeyListener(keyListener);
            cbStrategy.removeActionListener(alistener);
            txtTimeOut.removeFocusListener(flistener);
            txtRetryCount.removeFocusListener(flistener);
            txtTimeBetweenRetries.removeFocusListener(flistener);
        }
    }

    /**
     * Gets the strategy type
     * 
     * @return ErrorStrategyType
     */
    public ErrorStrategyType getStrategyCombo() {
        return (ErrorStrategyType) cbStrategy.getSelectedItem();
    }

    private void handleRetryCountChanged() {
        retryCount = txtRetryCount.getText();
        if (controller != null) {
            controller.notifyRetryCountChanged(esType, retryCount);
        }
    }

    private void handleStrategySelectedChanged() {
        String i = (String) cbStrategy.getSelectedItem();
        if ((controller != null) && (i != null)) {
            controller.notifyStrategySelectedChanged(esType, i);
        }
    }

    private void handleTimeBetweenRetriesChanged() {
        timeBetweenRetries = txtTimeBetweenRetries.getText();
        if (controller != null) {
            controller.notifyTimeBetweenRetriesChanged(esType, timeBetweenRetries);
        }
    }

    private void handleTimeOutChanged() {
        timeOut = txtTimeOut.getText();
        if (controller != null) {
            controller.notifyTimeOutChanged(esType, timeOut);
        }
    }

    public IErrorStrategiesController getController() {
        return controller;
    }

    public void setController(IErrorStrategiesController controller) {
        this.controller = controller;
    }

    /**
     * Initialization components
     */
    protected void initializeLayout() {
        timeOutLabel = new JLabel("Time Out", JLabel.RIGHT);
        timeOutUnitLabel = new JLabel("s", JLabel.LEFT);
        txtTimeOut = new JTextField("", 0);
        setBackgroundTimeOut();

        retryCountLabel = new JLabel("Retry Count", JLabel.RIGHT);
        txtRetryCount = new JTextField("", 0);
        setBackgroundRetryCount();

        retryTimeOutLabel = new JLabel("Time Between Retries", JLabel.RIGHT);
        retryTimeOutUnitLabel = new JLabel("s", JLabel.LEFT);
        txtTimeBetweenRetries = new JTextField("", 0);
        setBackgroundTimeBetweenRetries();

        strategyLabel = new JLabel("Strategy", JLabel.RIGHT);
        cbStrategy = new JComboBox<>();
        cbStrategy.addItem("IGNORE");
        cbStrategy.addItem("PAUSE");
        cbStrategy.addItem("ABORT");

        setLayout(new GridBagLayout());

        // First line
        GridBagConstraints timeOutLabelConstraints = new GridBagConstraints();
        timeOutLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        timeOutLabelConstraints.gridx = 0;
        timeOutLabelConstraints.gridy = 0;
        timeOutLabelConstraints.weightx = 0;
        timeOutLabelConstraints.weighty = 0;
        timeOutLabelConstraints.insets = new Insets(5, 5, 0, 2);
        add(timeOutLabel, timeOutLabelConstraints);

        GridBagConstraints timeOutTextConstraints = new GridBagConstraints();
        timeOutTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        timeOutTextConstraints.gridx = 1;
        timeOutTextConstraints.gridy = 0;
        timeOutTextConstraints.weightx = 1;
        timeOutTextConstraints.weighty = 0;
        timeOutTextConstraints.insets = new Insets(5, 0, 0, 2);
        add(txtTimeOut, timeOutTextConstraints);

        GridBagConstraints timeOutUnitLabelConstraints = new GridBagConstraints();
        timeOutUnitLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        timeOutUnitLabelConstraints.gridx = 2;
        timeOutUnitLabelConstraints.gridy = 0;
        timeOutUnitLabelConstraints.weightx = 0;
        timeOutUnitLabelConstraints.weighty = 0;
        timeOutUnitLabelConstraints.insets = new Insets(5, 0, 0, 5);
        add(timeOutUnitLabel, timeOutUnitLabelConstraints);

        // Second line
        GridBagConstraints retryCountLabelConstraints = new GridBagConstraints();
        retryCountLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        retryCountLabelConstraints.gridx = 0;
        retryCountLabelConstraints.gridy = 1;
        retryCountLabelConstraints.weightx = 0;
        retryCountLabelConstraints.weighty = 0;
        retryCountLabelConstraints.insets = new Insets(5, 5, 0, 2);
        add(retryCountLabel, retryCountLabelConstraints);

        GridBagConstraints retryCountTextConstraints = new GridBagConstraints();
        retryCountTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        retryCountTextConstraints.gridx = 1;
        retryCountTextConstraints.gridy = 1;
        retryCountTextConstraints.weightx = 1;
        retryCountTextConstraints.weighty = 0;
        retryCountTextConstraints.gridwidth = GridBagConstraints.REMAINDER;
        retryCountTextConstraints.insets = new Insets(5, 0, 0, 5);
        add(txtRetryCount, retryCountTextConstraints);

        // Third line
        GridBagConstraints retryTimeOutLabelConstraints = new GridBagConstraints();
        retryTimeOutLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        retryTimeOutLabelConstraints.gridx = 0;
        retryTimeOutLabelConstraints.gridy = 2;
        retryTimeOutLabelConstraints.weightx = 0;
        retryTimeOutLabelConstraints.weighty = 0;
        retryTimeOutLabelConstraints.insets = new Insets(5, 5, 0, 2);
        add(retryTimeOutLabel, retryTimeOutLabelConstraints);

        GridBagConstraints retryTimeOutTextConstraints = new GridBagConstraints();
        retryTimeOutTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        retryTimeOutTextConstraints.gridx = 1;
        retryTimeOutTextConstraints.gridy = 2;
        retryTimeOutTextConstraints.weightx = 1;
        retryTimeOutTextConstraints.weighty = 0;
        retryTimeOutTextConstraints.insets = new Insets(5, 0, 0, 2);
        this.add(txtTimeBetweenRetries, retryTimeOutTextConstraints);

        GridBagConstraints retryTimeOutUnitLabelConstraints = new GridBagConstraints();
        retryTimeOutUnitLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        retryTimeOutUnitLabelConstraints.gridx = 2;
        retryTimeOutUnitLabelConstraints.gridy = 2;
        retryTimeOutUnitLabelConstraints.weightx = 0;
        retryTimeOutUnitLabelConstraints.weighty = 0;
        retryTimeOutUnitLabelConstraints.insets = new Insets(5, 0, 0, 5);
        this.add(retryTimeOutUnitLabel, retryTimeOutUnitLabelConstraints);

        // Last line
        GridBagConstraints errorStrategyLabelConstraints = new GridBagConstraints();
        errorStrategyLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorStrategyLabelConstraints.gridx = 0;
        errorStrategyLabelConstraints.gridy = 3;
        errorStrategyLabelConstraints.weightx = 0;
        errorStrategyLabelConstraints.weighty = 0;
        errorStrategyLabelConstraints.insets = new Insets(5, 5, 5, 2);
        this.add(strategyLabel, errorStrategyLabelConstraints);

        GridBagConstraints errorStrategyTextConstraints = new GridBagConstraints();
        errorStrategyTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorStrategyTextConstraints.gridx = 1;
        errorStrategyTextConstraints.gridy = 3;
        errorStrategyTextConstraints.weightx = 1;
        errorStrategyTextConstraints.weighty = 0;
        errorStrategyTextConstraints.insets = new Insets(5, 0, 5, 5);
        errorStrategyTextConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.add(cbStrategy, errorStrategyTextConstraints);

        // Add the component panel in mouse listener
        // addMouseListener(this);

    }

    public void setStrategy(String value) {
        if (value != null) {
            cbStrategy.setSelectedItem(value);
        }
    }

    public void setRetryCount(int value, boolean savedValue) {
        if (savedValue) {
            this.dbretryCount = String.valueOf(value);
        } else {
            this.retryCount = String.valueOf(value);
            txtRetryCount.setText(retryCount);
        }
        setBackgroundRetryCount();
    }

    private void setBackgroundRetryCount() {
        BackgroundRenderer.setBackgroundField(this.txtRetryCount, retryCount, dbretryCount);
    }

    public void setTimeBetweenRetries(double value, boolean savedValue) {
        if (savedValue) {
            this.dbtimeBetweenRetries = String.valueOf(value);
        } else {
            this.timeBetweenRetries = String.valueOf(value);
            txtTimeBetweenRetries.setText(timeBetweenRetries);
        }
        setBackgroundTimeBetweenRetries();

    }

    private void setBackgroundTimeBetweenRetries() {
        BackgroundRenderer.setBackgroundField(this.txtTimeBetweenRetries, timeBetweenRetries, dbtimeBetweenRetries);
    }

    public void setTimeOut(double value, boolean savedValue) {

        if (savedValue) {
            this.dbtimeOut = String.valueOf(value);
        } else {
            this.timeOut = String.valueOf(value);
            txtTimeOut.setText(timeOut);
        }
        setBackgroundTimeOut();
    }

    private void setBackgroundTimeOut() {
        BackgroundRenderer.setBackgroundField(this.txtTimeOut, timeOut, dbtimeOut);
    }

    public void setEditable(boolean arg0) {
        txtRetryCount.setEditable(arg0);
        txtTimeBetweenRetries.setEditable(arg0);
        txtTimeOut.setEditable(arg0);
        cbStrategy.setEnabled(arg0);
    }

    public void clearValue() {
        txtTimeOut.setText("");
        txtTimeBetweenRetries.setText("");
        txtRetryCount.setText("");
        txtTimeOut.setBackground(Color.WHITE);
        txtTimeBetweenRetries.setBackground(Color.WHITE);
        txtRetryCount.setBackground(Color.WHITE);
        cbStrategy.setSelectedItem("IGNORE");
    }

}

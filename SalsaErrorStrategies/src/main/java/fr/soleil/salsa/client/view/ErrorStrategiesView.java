package fr.soleil.salsa.client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import fr.soleil.salsa.client.commons.ErrorStrategiesCommons;
import fr.soleil.salsa.client.controller.IErrorStrategiesController;

public class ErrorStrategiesView extends JPanel implements IView<IErrorStrategiesController> {

    private static final long serialVersionUID = 7784439880430717837L;

    /**
     * The actuator view.
     */
    private StrategiesTypeView actuatorsView = null;

    /**
     * The context validation view.
     */
    private ContextValidationView contextView = null;

    /**
     * The error strategies controller
     */
    private IErrorStrategiesController controller;

    /**
     * The hook view.
     */
    private StrategiesTypeView hooksView = null;

    private JPanel mainPanel;

    private JScrollPane scrollPane;

    /**
     * The sensor view.
     */
    private StrategiesTypeView sensorsView = null;
    /**
     * The timebase view.
     */
    private StrategiesTypeView timebasesView = null;

    private boolean editable = true;

    public ErrorStrategiesView() {
        this(null);
    }

    /**
     * Constructor
     * 
     * @param timebaseController
     */
    public ErrorStrategiesView(IErrorStrategiesController errorStrategiesController) {
        this.controller = errorStrategiesController;
        sensorsView = new StrategiesTypeView(controller, ErrorStrategyItemType.sensors);
        actuatorsView = new StrategiesTypeView(controller, ErrorStrategyItemType.actuators);
        timebasesView = new StrategiesTypeView(controller, ErrorStrategyItemType.timebases);
        hooksView = new StrategiesTypeView(controller, ErrorStrategyItemType.hooks);
        contextView = new ContextValidationView(controller);
        this.initialize();
        setEnabled(false);
    }

    public void enableListener(boolean enabled) {
        sensorsView.enableListener(enabled);
        actuatorsView.enableListener(enabled);
        hooksView.enableListener(enabled);
        timebasesView.enableListener(enabled);
        contextView.enableListener(enabled);
    }

    @Override
    public IErrorStrategiesController getController() {
        return controller;
    }

    @Override
    public void setController(IErrorStrategiesController controller) {
        this.controller = controller;
        sensorsView.setController(controller);
        actuatorsView.setController(controller);
        timebasesView.setController(controller);
        hooksView.setController(controller);
        contextView.setController(controller);
    }

    private StrategiesTypeView getErrorStrategyItemView(ErrorStrategyItemType itemType) {
        if (itemType == ErrorStrategyItemType.actuators) {
            return actuatorsView;
        } else if (itemType == ErrorStrategyItemType.sensors) {
            return sensorsView;
        } else if (itemType == ErrorStrategyItemType.hooks) {
            return hooksView;
        } else if (itemType == ErrorStrategyItemType.timebases) {
            return timebasesView;
        }
        return null;
    }

    private void initialize() {

        this.setLayout(new GridBagLayout());

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        // sensors
        GridBagConstraints sensorsbagConstraints = new GridBagConstraints();
        sensorsbagConstraints.fill = GridBagConstraints.BOTH;
        sensorsbagConstraints.gridx = 0;
        sensorsbagConstraints.gridy = 0;
        sensorsbagConstraints.weightx = 0.5;
        sensorsbagConstraints.weighty = 0.2;
        // sensorsView.setStrategyName(ErrorStrategiesCommons.SENSORS_TITLE);
        sensorsView.setBorder(new TitledBorder(ErrorStrategiesCommons.SENSORS_TITLE));
        mainPanel.add(sensorsView, sensorsbagConstraints);

        // actuators
        GridBagConstraints actuatorsbagConstraints = new GridBagConstraints();
        actuatorsbagConstraints.fill = GridBagConstraints.BOTH;
        actuatorsbagConstraints.gridx = 1;
        actuatorsbagConstraints.gridy = 0;
        actuatorsbagConstraints.weightx = 0.5;
        actuatorsbagConstraints.weighty = 0.2;
        // actuatorsView.setStrategyName(ErrorStrategiesCommons.ACTUATORS_TITLE);
        actuatorsView.setBorder(new TitledBorder(ErrorStrategiesCommons.ACTUATORS_TITLE));
        mainPanel.add(actuatorsView, actuatorsbagConstraints);

        // timebases
        GridBagConstraints timebasesbagConstraints = new GridBagConstraints();
        timebasesbagConstraints.fill = GridBagConstraints.BOTH;
        timebasesbagConstraints.gridx = 0;
        timebasesbagConstraints.gridy = 1;
        timebasesbagConstraints.weightx = 0.5;
        timebasesbagConstraints.weighty = 0.2;
        // timebasesView.setStrategyName(ErrorStrategiesCommons.TIMEBASES_TITLE);
        timebasesView.setBorder(new TitledBorder(ErrorStrategiesCommons.TIMEBASES_TITLE));
        mainPanel.add(timebasesView, timebasesbagConstraints);

        // Hooks
        GridBagConstraints hooksbagConstraints = new GridBagConstraints();
        hooksbagConstraints.fill = GridBagConstraints.BOTH;
        hooksbagConstraints.gridx = 1;
        hooksbagConstraints.gridy = 1;
        hooksbagConstraints.weightx = 0.5;
        hooksbagConstraints.weighty = 0.2;
        // hooksView.setStrategyName(ErrorStrategiesCommons.HOOKS_TITLE);
        hooksView.setBorder(new TitledBorder(ErrorStrategiesCommons.HOOKS_TITLE));
        mainPanel.add(hooksView, hooksbagConstraints);

        // Context validation
        GridBagConstraints contextValidationConstraints = new GridBagConstraints();
        contextValidationConstraints.fill = GridBagConstraints.BOTH;
        contextValidationConstraints.gridx = 0;
        contextValidationConstraints.gridy = 2;
        contextValidationConstraints.weightx = 1;
        contextValidationConstraints.weighty = 0.2;
        contextValidationConstraints.gridwidth = GridBagConstraints.REMAINDER;

        contextView.setBorder(new TitledBorder(ErrorStrategiesCommons.CONTEXT_VALIDATION_TITLE));
        mainPanel.add(contextView, contextValidationConstraints);

        scrollPane = new JScrollPane(mainPanel);
        GridBagConstraints scrollConstraints = new GridBagConstraints();
        scrollConstraints.fill = GridBagConstraints.BOTH;
        scrollConstraints.gridx = 0;
        scrollConstraints.gridy = 0;
        scrollConstraints.weightx = 1;
        scrollConstraints.weighty = 1;
        this.add(scrollPane, scrollConstraints);

        setEnabled(false);

    }

    public void setContextValidationAttribute(String value, boolean savedValue) {
        contextView.setAttribute(value, savedValue);
    }

    public void setContextValidationStrategy(String value) {
        contextView.setStrategy(value);
    }

    public void setRetryCount(ErrorStrategyItemType itemType, int value, boolean savedValue) {
        StrategiesTypeView itemView = getErrorStrategyItemView(itemType);
        itemView.setRetryCount(value, savedValue);
    }

    public void setStrategy(ErrorStrategyItemType itemType, String value) {
        StrategiesTypeView itemView = getErrorStrategyItemView(itemType);
        itemView.setStrategy(value);
    }

    public void setTimeBetweenRetries(ErrorStrategyItemType itemType, double value, boolean savedValue) {
        StrategiesTypeView itemView = getErrorStrategyItemView(itemType);
        itemView.setTimeBetweenRetries(value, savedValue);
    }

    public void setTimeOut(ErrorStrategyItemType itemType, double value, boolean savedValue) {
        StrategiesTypeView itemView = getErrorStrategyItemView(itemType);
        itemView.setTimeOut(value, savedValue);
    }

    @Override
    public void setEnabled(boolean enabled) {
        sensorsView.setEditable(editable && enabled);
        actuatorsView.setEditable(editable && enabled);
        timebasesView.setEditable(editable && enabled);
        hooksView.setEditable(editable && enabled);
        contextView.setEditable(editable && enabled);
        super.setEnabled(editable && enabled);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean arg0) {
        editable = arg0;
        setEnabled(editable);
    }

    public void clearValue() {
        sensorsView.clearValue();
        actuatorsView.clearValue();
        timebasesView.clearValue();
        hooksView.clearValue();
        contextView.clearValue();
    }

}

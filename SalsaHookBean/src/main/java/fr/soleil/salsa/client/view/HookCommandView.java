package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.soleil.salsa.client.controller.IHooksController;

/**
 * @author Alike
 * 
 *         Class relative to the view of ONE hook command.
 * 
 */
public class HookCommandView extends JPanel {

    private static final long serialVersionUID = -814330352067874572L;

    /**
     * The listener charged with the listening of every action that could occur on this view.
     */
    private ActionListener actionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == txtCommand) {
                handleCommandChanged();
            }
            else if (e.getSource() == chkCommandSelected) {
                handleSelectionChanged();
            }
        }
    };

    /**
     * The check box to select the command.
     */
    private JCheckBox chkCommandSelected;

    /**
     * The controller.
     */
    private IHooksController controller;

    /**
     * The listening which is in charge of listening focus events.
     */
    private FocusListener focusListener = new FocusListener() {

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            handleCommandChanged();
        }
    };

    /**
     * The hook id (index) which contains this command.
     */
    private int hookId;

    /**
     * The command id (index).
     */
    private int id;

    /**
     * The main panel.
     */
    private JPanel mainPanel;

    /**
     * The textbox which represents the command value.
     */
    private JTextField txtCommand;

    public HookCommandView() {
        this(null, 0, 0);
    }

    /**
     * The constructor.
     * 
     * @param controller The controller
     * @param hookId The hook id (index)
     * @param commandId The command id (index)
     */
    public HookCommandView(IHooksController controller, int hookId, int commandId) {
        this.hookId = hookId;
        this.id = commandId;
        this.controller = controller;
        initializeLayout();
    }

    public IHooksController getController() {
        return controller;
    }

    public void setController(IHooksController controller) {
        this.controller = controller;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHookId() {
        return hookId;
    }

    public void setHookId(int hookId) {
        this.hookId = hookId;
    }

    /**
     * Utility method to create grid bag constraints with frequents values.
     * 
     * @param x
     * @param y
     * @return
     */
    private GridBagConstraints createGBC(int x, int y) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        return gbc;
    }

    /**
     * Enable/Disable all listener on this view.
     * 
     * @param enable
     */
    public void enableListeners(boolean enable) {
        if (enable) {
            txtCommand.addActionListener(actionListener);
            txtCommand.addFocusListener(focusListener);
            chkCommandSelected.addActionListener(actionListener);
        }
        else {
            txtCommand.removeActionListener(actionListener);
            txtCommand.removeFocusListener(focusListener);
            chkCommandSelected.removeActionListener(actionListener);
        }
    }

    /**
     * Handle a change value of a command.
     */
    private void handleCommandChanged() {
        String value = txtCommand.getText();
        if (controller != null) {
            controller.notifyHookCommandChanged(hookId, id, value);
        }
    }

    /**
     * Handle a change value of the selection
     */
    private void handleSelectionChanged() {
        if (controller != null) {
            controller.notifyCommandSelected(hookId, id, chkCommandSelected.isSelected());
        }
    }

    /**
     * Initialize the layou.
     */
    private void initializeLayout() {
        mainPanel = new JPanel();

        this.setLayout(new BorderLayout());
        this.add(mainPanel, BorderLayout.NORTH);

        GridBagLayout gbl = new GridBagLayout();
        gbl.columnWidths = new int[] { 1, 10, 100 };
        gbl.columnWeights = new double[] { 1, 10, 100 };
        mainPanel.setLayout(gbl);
        chkCommandSelected = new JCheckBox();
        mainPanel.add(chkCommandSelected, createGBC(0, 0));

        JLabel lblCommandName = new JLabel("Command name");
        mainPanel.add(lblCommandName, createGBC(1, 0));

        txtCommand = new JTextField();

        Dimension d = txtCommand.getPreferredSize();
        d.width = 400;
        txtCommand.setPreferredSize(d);
        GridBagConstraints gbcCommand = createGBC(2, 0);
        gbcCommand.gridwidth = 3;
        mainPanel.add(txtCommand, gbcCommand);
    }

    /**
     * Returns true if the command is selected (reprensented by the check box)
     * 
     * @return
     */
    public boolean isSelected() {
        return chkCommandSelected.isSelected();
    }

    /**
     * Sets the command value.
     * 
     * @param command
     */
    public void setCommandText(String command) {
        txtCommand.setText(command);
    }

    public void setSelected(boolean selected) {
        chkCommandSelected.setSelected(selected);
    }

}

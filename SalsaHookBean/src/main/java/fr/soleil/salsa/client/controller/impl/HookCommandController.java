package fr.soleil.salsa.client.controller.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IHookCommandController;
import fr.soleil.salsa.client.view.HookTableListView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.event.HookCommandModel;
import fr.soleil.salsa.entity.event.HookModel;
import fr.soleil.salsa.entity.event.ScanAddOnModel;
import fr.soleil.salsa.entity.impl.Stage;

public class HookCommandController extends AController<HookTableListView> implements
        IHookCommandController {

    public final static String DEFAULT_COMMAND = "Define a Tango DevVoid Command ...";

    private IConfig<?> config = null;

    public HookCommandController() {
        this(null);
    }

    public HookCommandController(HookTableListView view) {
        this(view, false);
    }

    public HookCommandController(HookTableListView view, boolean readOnly) {
        super(view, readOnly);
        if (view != null) {
            view.setReadOnly(readOnly);
        }
    }

    public void setConfig(IConfig<?> config) {
        this.config = config;

        // Clean Hook
        if (view != null) {
            view.clearHooks();
        }

        if ((config != null) && (view != null)) {
            IScanAddOns scanAddOns = config.getScanAddOn();
            if (scanAddOns != null) {
                List<IHook> hookList = scanAddOns.getHooks();
                if (hookList != null) {
                    Stage stage = null;
                    for (IHook hook : hookList) {
                        stage = hook.getStage();
                        List<IHookCommand> commandList = hook.getCommandsList();
                        view.setCommandList(stage, commandList);
                    }

                }
            }

        }

        if (view != null) {
            view.setButtonEnable(config != null);
        }

    }

    @Override
    public void hookListChanged(Stage stage, List<IHookCommand> commandList) {
        if (config != null) {
            IScanAddOns scanAddOns = config.getScanAddOn();
            if (scanAddOns == null) {
                scanAddOns = new ScanAddOnModel();
                config.setScanAddOn(scanAddOns);
            }
            if (scanAddOns != null) {
                List<IHook> hookList = scanAddOns.getHooks();
                if (hookList == null) {
                    hookList = new ArrayList<IHook>();
                    scanAddOns.setHooks(hookList);
                }
                if (hookList != null) {
                    IHook hook = null;
                    for (IHook ahook : hookList) {
                        if (ahook.getStage() == stage) {
                            hook = ahook;
                        }
                    }
                    if (hook == null) {
                        hook = new HookModel();
                        hook.setStage(stage);
                        hookList.add(hook);
                    }

                    hook.setCommandsList(commandList);
                }
                config.setScanAddOn(scanAddOns);
            }
            config.setModified(true);
        }

    }

    @Override
    public void addHookCommand(Stage stage) {
        if ((config != null) && !readOnly) {
            IHookCommand command = new HookCommandModel();
            command.setCommand("");
            if (view != null) {
                view.addHookLine(stage, command);
            }
            config.setModified(true);
        }

    }

    @Override
    protected HookTableListView generateView() {
        return new HookTableListView();
    }

    @Override
    public void selectedHookCommandChanged() {
        if (view != null) {
            view.refreshDeleteButtonEnable();
        }
    }

}

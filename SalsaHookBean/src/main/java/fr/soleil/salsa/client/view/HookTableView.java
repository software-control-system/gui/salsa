package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.jdesktop.swingx.JXTable;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.swing.ITextTransferable;
import fr.soleil.lib.project.swing.TextTansfertHandler;
import fr.soleil.salsa.client.controller.IHookCommandController;
import fr.soleil.salsa.client.controller.impl.HookCommandController;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.impl.Stage;
import fr.soleil.salsa.tool.SalsaUtils;

public class HookTableView extends JPanel implements ITextTransferable {

    private static final long serialVersionUID = -4391282942510221903L;

    private Stage hookId = Stage.PRE_RUN;

    /**
     * The controller.
     */
    private IHookCommandController controller;

    private final List<IHookCommand> commandList = new ArrayList<IHookCommand>();
    private final JScrollPane centerPanel = new JScrollPane();
    private final JXTable table = new JXTable();
    private final JPanel buttonPanel = new JPanel();
    private final JButton deselectButton = new JButton("Empty selection");
    private final JCheckBox checkAll = new JCheckBox("Check/Uncheck all");

    private boolean editable = true;

    private boolean readOnly = false;

    private final static HookTableCellRenderer renderer = new HookTableCellRenderer();

    public HookTableView(Stage stage) {
        setLayout(new BorderLayout());
        centerPanel.setViewportView(table);
        add(centerPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
        deselectButton.setToolTipText("Unselect all the selected lines");
        deselectButton.setEnabled(false);
        buttonPanel.add(deselectButton);
        buttonPanel.add(checkAll);

        deselectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                table.removeRowSelectionInterval(0, commandList.size() - 1);
                deselectButton.setEnabled(false);
                if (controller != null) {
                    controller.selectedHookCommandChanged();
                }
            }
        });

        checkAll.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                boolean check = checkAll.isSelected();
                int size = table.getRowCount();
                for (int i = 0; i < size; i++) {
                    table.setValueAt(check, i, 1);
                }
                table.repaint();
            }
        });

        this.hookId = stage;
        table.setAutoStartEditOnKeyStroke(true);
        table.setSortable(false);
        table.setModel(tableModel);
        TextTansfertHandler.registerTransferHandler(this, table);
        // TableColumn colModel = table.getColumnModel().getColumn(0);
        // colModel.setCellRenderer(renderer);
        table.setDefaultRenderer(String.class, renderer);

        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                deselectButton.setEnabled(table.getSelectedRowCount() != 0);
                if (controller != null) {
                    controller.selectedHookCommandChanged();
                }
                int row = table.getSelectedRow();
                if ((row > -1) && (row < commandList.size())) {
                    int col = table.getSelectedColumn();
                    if (col == 1) {
                        IHookCommand command = commandList.get(row);
                        table.setValueAt(!command.isEnable(), row, col);
                    }
                }
            }
        });
        table.packAll();
    }

    @Override
    public String getTextToTransfert() {
        String textToTransfert = null;
        if (table != null) {
            int row = table.getSelectedRow();
            int nbColumn = table.getColumnCount();
            if ((row > -1) && (nbColumn > 0)) {
                Object value = table.getValueAt(row, 0);
                if (value != null) {
                    textToTransfert = TangoDeviceHelper.getDeviceName(value.toString());
                }
            }
        }
        return textToTransfert;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        deselectButton.setVisible(!readOnly);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public Stage getHookId() {
        return hookId;
    }

    public void setHookId(Stage hookId) {
        this.hookId = hookId;
    }

    public IHookCommandController getController() {
        return controller;
    }

    public void setController(IHookCommandController controller) {
        this.controller = controller;
    }

    private final AbstractTableModel tableModel = new AbstractTableModel() {

        private static final long serialVersionUID = 6588744249416925083L;

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            IHookCommand command = commandList.get(rowIndex);
            if (columnIndex == 0) {
                return command.getCommand();
            } else {
                return command.isEnable();
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if ((aValue != null) && !aValue.toString().trim().isEmpty()) {
                String stringValue = aValue.toString().trim();
                IHookCommand command = commandList.get(rowIndex);
                if (columnIndex == 0) {
                    command.setCommand(stringValue);
                } else {
                    command.setEnable((Boolean) aValue);
                }
                notifyHookListChanged();
            }
        }

        @Override
        public int getRowCount() {
            int rowCount = 0;
            if (commandList != null) {
                rowCount = commandList.size();
            }
            return rowCount;
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (columnIndex == 0) {
                return String.class;
            } else {
                return Boolean.class;
            }
        }

        @Override
        public String getColumnName(int column) {
            if (column == 0) {
                return hookId.getValue() + " Command Name";
            } else {
                return "Enable";
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                return !readOnly;
            } else {
                return editable;
            }

        }
    };

    public void deleteSelectedRows() {
        if (!selectionEmpty()) {
            int[] rows = table.getSelectedRows();
            for (int index : rows) {
                if (index < commandList.size()) {
                    commandList.remove(index);
                }
            }
            notifyHookListChanged();
            tableModel.fireTableStructureChanged();
        }
    }

    public boolean selectionEmpty() {
        boolean empty = true;
        int[] rows = table.getSelectedRows();
        if ((rows != null) && (rows.length > 0)) {
            empty = false;
        }
        return empty;
    }

    private void notifyHookListChanged() {
        if (controller != null) {
            controller.hookListChanged(hookId, commandList);
        }
    }

    public void upSelectedRows() {
        int[] rows = table.getSelectedRows();
        if ((rows != null) && (rows.length > 0)) {
            int beforeIndex = 0;
            for (int index : rows) {
                if (index > 0) {
                    beforeIndex = index - 1;
                    IHookCommand beforeCommand = commandList.get(beforeIndex);
                    IHookCommand currentCommand = commandList.get(index);
                    commandList.set(beforeIndex, currentCommand);
                    commandList.set(index, beforeCommand);
                }
            }
            tableModel.fireTableDataChanged();
            notifyHookListChanged();
        }
    }

    public void downSelectedRows() {
        int[] rows = table.getSelectedRows();
        if ((rows != null) && (rows.length > 0)) {
            int afterIndex = 0;
            for (int index : rows) {
                if (index < commandList.size() - 1) {
                    afterIndex = index + 1;
                    IHookCommand afterCommand = commandList.get(afterIndex);
                    IHookCommand currentCommand = commandList.get(index);
                    commandList.set(afterIndex, currentCommand);
                    commandList.set(index, afterCommand);
                }
            }
            tableModel.fireTableDataChanged();
            notifyHookListChanged();
        }
    }

    public void addHookLine(IHookCommand command) {
        commandList.add(command);
        tableModel.fireTableStructureChanged();
        notifyHookListChanged();

        table.editCellAt(0, 0);
        table.changeSelection(0, 0, true, true);
        // table.transferFocus();
        // table.getSelectionModel().setSelectionInterval(0, 1);
    }

    public void setCommandList(List<IHookCommand> command) {
        commandList.clear();
        commandList.addAll(command);
        tableModel.fireTableStructureChanged();
    }

    public void clearHook() {
        commandList.clear();
        tableModel.fireTableStructureChanged();
    }

    public boolean isEmpty() {
        return commandList.isEmpty();
    }

    public boolean isOnError() {
        boolean onError = false;
        String commandName = null;
        for (IHookCommand command : commandList) {
            commandName = command.getCommand();
            if ((!SalsaUtils.isDefined(commandName))
                    || commandName.trim().equalsIgnoreCase(HookCommandController.DEFAULT_COMMAND)) {
                onError = true;
                break;
            }
        }
        return onError;
    }

}

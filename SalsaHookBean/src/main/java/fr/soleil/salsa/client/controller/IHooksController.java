package fr.soleil.salsa.client.controller;

import java.util.List;

import fr.soleil.salsa.client.view.HooksListView;

/**
 * @author Alike
 * 
 *         The interface for the hooks controller.
 */
public interface IHooksController extends IController<HooksListView> {

    /**
     * Notify a delete action on commands.
     * 
     * @param id The hook id (index)
     * @param commandToDel The list of index of commands that should be deleted
     */
    void notifyDeleteCommandAction(int id, List<Integer> commandToDel);

    /**
     * Notify a delete action on hooks.
     * 
     * @param toDel The list of index of hooks that should be deleted.
     */
    void notifyDeleteHookAction(List<Integer> toDel);

    /**
     * Notify an modification on the name of an existing command.
     * 
     * @param hookId the hook id (index)
     * @param id The command id (index)
     * @param value The new value
     */
    void notifyHookCommandChanged(int hookId, int id, String value);

    /**
     * Notify a new command
     * 
     * @param id The hook id (index)
     */
    void notifyNewCommand(int id);

    /**
     * Notify a new hook.
     */
    void notifyNewHookAction();

    /**
     * Notify a modification of stage on an existing hook
     * 
     * @param id The hook id (index)
     * @param value The new value of the stage
     */
    void notifyStageAction(int id, String value);

    /**
     * Notify the selection of a hook.
     * 
     * @param hookId
     */
    void notifyHookSelected(int hookId, boolean b);

    void notifyCommandSelected(int hookId, int commandId, boolean selected);

}

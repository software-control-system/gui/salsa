package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.soleil.salsa.client.controller.IHookCommandController;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.impl.Stage;
import fr.soleil.salsa.tool.SalsaUtils;

public class HookTableListView extends JPanel implements IView<IHookCommandController> {

    private static final long serialVersionUID = -5335758558944474870L;

    private Map<Stage, HookTableView> hookMap = new HashMap<>();
    private Map<Stage, Integer> stageId = new HashMap<>();
    private JTabbedPane mainPanel = new JTabbedPane();
    private boolean editable = true;
    private boolean readOnly = false;

    private JPanel buttonPanel = new JPanel();
    private JButton addButton = new JButton("Add Hook");
    private JComboBox<Stage> stageCombo = new JComboBox<>(Stage.values());
    private JButton deleteButton = new JButton("Delete Hook");
    private JButton upButton = new JButton("Up");
    private JButton downButton = new JButton("Down");

    /**
     * The controller.
     */
    private IHookCommandController controller;

    public HookTableListView() {
        setLayout(new BorderLayout());
        add(mainPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.NORTH);

        buttonPanel.add(addButton);
        buttonPanel.add(stageCombo);
        buttonPanel.add(deleteButton);
        deleteButton.setToolTipText("Select lines to delete");
        buttonPanel.add(upButton);
        buttonPanel.add(downButton);
        setButtonEnable(false);
        stageCombo.setEditable(false);

        stageCombo.addActionListener(e -> {
            Stage stage = (Stage) stageCombo.getSelectedItem();
            Integer intStage = stageId.get(stage);
            mainPanel.setSelectedIndex(intStage);
        });

        addButton.addActionListener(e -> {
            Stage stage = (Stage) stageCombo.getSelectedItem();
            if (controller != null) {
                controller.addHookCommand(stage);
            }
        });

        deleteButton.addActionListener(e -> {
            Collection<HookTableView> collection = hookMap.values();
            for (HookTableView table : collection) {
                table.deleteSelectedRows();
                if (table.isEmpty()) {
                    Stage stage = table.getHookId();
                    int stageIndex = stageId.get(stage);
                    mainPanel.setEnabledAt(stageIndex, false);
                }
            }
        });

        upButton.addActionListener(e -> {
            Collection<HookTableView> collection = hookMap.values();
            for (HookTableView table : collection) {
                table.upSelectedRows();
            }
        });

        downButton.addActionListener(e -> {
            Collection<HookTableView> collection = hookMap.values();
            for (HookTableView table : collection) {
                table.downSelectedRows();
            }
        });

        for (Stage tmpStage : Stage.values()) {
            addHookTable(tmpStage);
        }

        setReadOnly(readOnly);
        setReadOnly(editable);
    }

    private void addHookTable(Stage stage) {
        HookTableView table = new HookTableView(stage);
        table.setController(controller);
        int stageIndex = hookMap.size();
        stageId.put(stage, stageIndex);
        hookMap.put(stage, table);
        mainPanel.add(stage.name(), table);
        mainPanel.setEnabledAt(stageIndex, false);
        refreshColorStagePanel(stage);
    }

    public void clearHooks() {
        Set<Stage> stageList = hookMap.keySet();
        for (Stage stage : stageList) {
            hookMap.get(stage).clearHook();
            int intStage = stageId.get(stage);
            mainPanel.setEnabledAt(intStage, false);
        }
        refreshColorTabbedPanel();
    }

    @Override
    public IHookCommandController getController() {
        return controller;
    }

    @Override
    public void setController(IHookCommandController controller) {
        this.controller = controller;
        Collection<HookTableView> collection = hookMap.values();
        for (HookTableView table : collection) {
            table.setController(controller);
        }

    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        buttonPanel.setVisible(!readOnly);
        Collection<HookTableView> collection = hookMap.values();
        for (HookTableView table : collection) {
            table.setReadOnly(readOnly);
        }
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        Collection<HookTableView> collection = hookMap.values();
        for (HookTableView table : collection) {
            table.setEditable(editable);
        }
    }

    public void setCommandList(Stage stage, List<IHookCommand> commandList) {
        HookTableView table = hookMap.get(stage);
        table.setCommandList(commandList);
        mainPanel.setEnabledAt(stageId.get(stage), SalsaUtils.isFulfilled(commandList));
        refreshColorStagePanel(stage);
    }

    private void refreshColorTabbedPanel() {
        Set<Stage> stageList = hookMap.keySet();
        for (Stage stage : stageList) {
            refreshColorStagePanel(stage);
        }
    }

    private void refreshColorStagePanel(Stage stage) {
        Integer intStage = stageId.get(stage);
        HookTableView table = hookMap.get(stage);
        if (table != null && table.isOnError()) {
            mainPanel.setForegroundAt(intStage, Color.RED);
        } else {
            mainPanel.setForegroundAt(intStage, Color.BLACK);
        }

    }

    public void addHookLine(Stage stage, IHookCommand command) {
        HookTableView table = hookMap.get(stage);
        table.addHookLine(command);
        Integer intStage = stageId.get(stage);
        mainPanel.setEnabledAt(intStage, true);
        refreshColorStagePanel(stage);
    }

    public void setButtonEnable(boolean enable) {
        addButton.setEnabled(enable);
        stageCombo.setEnabled(enable);

        upButton.setEnabled(enable);
        downButton.setEnabled(enable);
        if (enable) {
            refreshDeleteButtonEnable();
        } else {
            deleteButton.setEnabled(enable);
        }
    }

    public void refreshDeleteButtonEnable() {
        Collection<HookTableView> collection = hookMap.values();
        boolean empty = true;
        for (HookTableView table : collection) {
            if (!table.selectionEmpty()) {
                empty = false;
                break;
            }
        }
        deleteButton.setEnabled(!empty);
    }

}

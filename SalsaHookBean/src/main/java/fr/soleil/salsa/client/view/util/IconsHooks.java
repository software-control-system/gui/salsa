package fr.soleil.salsa.client.view.util;

import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.Icon;
import javax.swing.ImageIcon;


public class IconsHooks {
    
    private static final String BUNDLE_NAME = "fr.soleil.salsa.client.view.icons.iconsHooks"; //$NON-NLS-1$

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(BUNDLE_NAME);

    
    public static Icon getIconHook(String key) {
        ImageIcon icon = null;
        
        try {
            URL url = ClassLoader.getSystemResource(RESOURCE_BUNDLE.getString(key));
            if(url != null)
                icon = new ImageIcon(url);
        } catch (MissingResourceException e) {
        }
        return icon;
    }

}

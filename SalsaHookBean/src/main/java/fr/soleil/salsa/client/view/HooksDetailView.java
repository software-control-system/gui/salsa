package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import fr.soleil.salsa.client.controller.IHooksController;

/**
 * The class relative to the view of ONE hook.
 * 
 * @author Alike
 */
public class HooksDetailView extends JPanel {

    private static final long serialVersionUID = 2970167230093212717L;

    /**
     * The listener which is charged to listen of every action on this view.
     */
    private ActionListener actionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == cbStage) {
                stageChanged();
            } else if (e.getSource() == chkHookSelected) {
                handleHookSelectionChanged();
            }
        }

    };

    /**
     * The button to add a command.
     */
    private JButton btnAddCommand;

    /**
     * The button to del a command.
     */
    private JButton btnDelCommand;

    /**
     * The combo box for the stages.
     */
    private JComboBox<Item> cbStage;

    /**
     * The checkbox to select the hook.
     */
    private JCheckBox chkHookSelected;

    /**
     * Regroups the views for command elements of the hook.
     */
    private List<HookCommandView> commandViews = new ArrayList<HookCommandView>();

    /**
     * The controller.
     */
    private IHooksController controller;

    /**
     * The hook id for this view.
     */
    private int id;

    /**
     * The main panel.
     */
    private JPanel mainPanel;

    /**
     * The panel which contains all commands of this hook.
     */
    private JPanel pnlCommands;

    public HooksDetailView() {
        this(0, null);
    }

    /**
     * The constructor.
     * 
     * @param id The hook id (index).
     * @param c The controller
     */
    public HooksDetailView(int id, IHooksController c) {
        this.id = id;
        this.controller = c;
        initializeLayout();
    }

    public IHooksController getController() {
        return controller;
    }

    public void setController(IHooksController controller) {
        this.controller = controller;
        for (HookCommandView commandView : commandViews) {
            commandView.setController(controller);
        }
    }

    /**
     * Add a command to this hook.
     * 
     * @param commandIndex The command id (index)
     * @param command The command value
     */
    public void addCommand(int commandIndex, String command) {
        HookCommandView hcv = new HookCommandView(controller, id, commandIndex);
        hcv.setCommandText(command);
        commandViews.add(hcv);
        pnlCommands.add(hcv, createGBC(0, commandIndex));
    }

    /**
     * Add a stage to the combo box.
     * 
     * @param value The value of the item
     * @param text The text of the item
     */
    public void addStage(String value, String text) {
        cbStage.addItem(new Item(value, text));
    }

    /**
     * Clear all stages of the combo box.
     */
    public void clearStages() {
        commandViews.clear();
        commandViews = new ArrayList<HookCommandView>();
        cbStage.removeAllItems();
    }

    /**
     * Utility method to create grid bag constraint with frequent values.
     * 
     * @param x
     * @param y
     * @return
     */
    private GridBagConstraints createGBC(int x, int y) {
        GridBagConstraints r = new GridBagConstraints();
        r.fill = GridBagConstraints.HORIZONTAL;
        r.anchor = GridBagConstraints.FIRST_LINE_START;
        r.insets = new Insets(2, 8, 2, 8);
        r.gridx = x;
        r.gridy = y;
        // r.gridwidth = 300;
        return r;
    }

    /**
     * Enable all listeners of this view (and for child views).
     * 
     * @param enable
     */
    public void enableListeners(boolean enable) {
        if (enable) {
            chkHookSelected.addActionListener(actionListener);
            cbStage.addActionListener(actionListener);
        } else {
            chkHookSelected.removeActionListener(actionListener);

            cbStage.removeActionListener(actionListener);
        }

        for (HookCommandView hcv : commandViews) {
            hcv.enableListeners(enable);
        }
    }

    /**
     * Returns the index of the selected commands.
     * 
     * @return The index list of the selected commands.
     */
    private List<Integer> getCommandsSelected() {
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < commandViews.size(); i++) {
            HookCommandView hcv = commandViews.get(i);
            if (hcv.isSelected()) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * Get the hook id associated to this view.
     * 
     * @return
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private void handleHookSelectionChanged() {
        if (controller != null) {
            controller.notifyHookSelected(id, chkHookSelected.isSelected());
        }
    }

    private JPanel embed(Component c, int width) {
        JPanel container = new JPanel();
        container.setBackground(Color.yellow);
        // c.setBackground(Color.red);
        container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));
        container.add(c);
        Dimension original = c.getPreferredSize();
        container.setPreferredSize(new Dimension(200, original.height));

        return container;
    }

    /**
     * Initialize the layout.
     */
    private void initializeLayout() {
        this.setLayout(new BorderLayout());
        mainPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        gbl.columnWeights = new double[] { 1, 10, 100 };// , 70 };
        mainPanel.setLayout(gbl);
        chkHookSelected = new JCheckBox();

        mainPanel.add(chkHookSelected, createGBC(0, 0));

        cbStage = new JComboBox<>();

        mainPanel.add(embed(cbStage, 650), createGBC(1, 0));

        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new BorderLayout());

        btnAddCommand = new JButton("+");
        btnAddCommand.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyNewCommand(id);
                }
            }
        });
        btnAddCommand.setEnabled(false);
        btnPanel.add(btnAddCommand, BorderLayout.WEST); // createGBC(3, 0));

        btnDelCommand = new JButton("-");
        btnDelCommand.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<Integer> commandToDel = getCommandsSelected();
                if (controller != null) {
                    controller.notifyDeleteCommandAction(id, commandToDel);
                }
            }
        });
        btnDelCommand.setEnabled(false);
        btnPanel.add(btnDelCommand, BorderLayout.EAST); // createGBC(4, 0));

        mainPanel.add(btnPanel, createGBC(2, 0));

        pnlCommands = new JPanel();
        GridBagLayout commandsGBL = new GridBagLayout();
        commandsGBL.columnWeights = new double[] { 100 };
        pnlCommands.setLayout(commandsGBL);

        GridBagConstraints gbcCommands = createGBC(2, 1);
        gbcCommands.gridwidth = 3;
        mainPanel.add(pnlCommands, gbcCommands);

        this.add(mainPanel, BorderLayout.NORTH);
    }

    /**
     * Return true if the hook is selected.
     * 
     * @return
     */
    public boolean isSelected() {
        return chkHookSelected.isSelected();
    }

    /**
     * Enable/Disable the "Add command" button.
     * 
     * @param enable
     */
    public void setEnableAddCommand(boolean enable) {
        btnAddCommand.setEnabled(enable);
    }

    /**
     * Enable/Disable the "Delete command" button.
     * 
     * @param enable
     */
    public void setEnableDelCommand(boolean enable) {
        btnDelCommand.setEnabled(enable);
    }

    /**
     * Sets the selected stage of the combo box.
     * 
     * @param stageIndex
     */
    public void setSelectedStage(int stageIndex) {
        cbStage.setSelectedIndex(stageIndex);
    }

    /**
     * Select the hook.
     */
    public void setSelected(boolean selected) {
        chkHookSelected.setSelected(selected);
    }

    public void setSelectedCommand(int commandId, boolean selected) {
        if (commandId >= commandViews.size())
            return;

        commandViews.get(commandId).setSelected(selected);
    }

    /**
     * Handle witch the modification of the selected stage.
     */
    private void stageChanged() {
        Item i = (Item) cbStage.getSelectedItem();
        if (controller != null) {
            controller.notifyStageAction(id, i.value);
        }
    }

    /**
     * Utility class to populate Combo box. Should ideally be moved to another project.
     */
    private class Item {
        private String text;
        private String value;

        Item(String value, String text) {
            this.text = text;
            this.value = value;
        }

        @Override
        public String toString() {
            return text;
        }
    }

}

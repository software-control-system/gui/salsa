package fr.soleil.salsa.client.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import fr.soleil.salsa.client.controller.impl.HookCommandController;

/**
 * Custom renderer for the cells of the table. Identical to the default renderer except : - it
 * doesn't display which cell has the focus, - also, it displays the content of the cell in a
 * tooltip, - rows with type "ERROR" are displayed in red.
 */
public class HookTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -8291018729074231746L;
    private final JLabel errorComponent = new JLabel();

    public HookTableCellRenderer() {
        super();
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {

        JComponent component = (JComponent) super.getTableCellRendererComponent(jTable, value,
                isSelected, hasFocus, row, col);

        // Find ERROR label in line
        boolean errorFound = false;
        if ((value == null) || value.toString().trim().isEmpty()
                || (value.toString().equals(HookCommandController.DEFAULT_COMMAND))) {
            errorFound = true;
        }

        if (errorFound) {
            if (isSelected) {
                component.setForeground(Color.RED);
            }
            else {
                errorComponent.setText(HookCommandController.DEFAULT_COMMAND);
                errorComponent.setForeground(Color.RED);
                errorComponent.setHorizontalAlignment(SwingConstants.LEFT);
                component = errorComponent;
            }
        }
        else {
            component.setForeground(Color.BLACK);
        }
        return component;
    }
}

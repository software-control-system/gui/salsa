package fr.soleil.salsa.client.controller.impl;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IHooksController;
import fr.soleil.salsa.client.view.HooksListView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IHook;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.IScanAddOns;
import fr.soleil.salsa.entity.event.EntityPropertyChangedEvent;
import fr.soleil.salsa.entity.event.HookCommandModel;
import fr.soleil.salsa.entity.event.HookModel;
import fr.soleil.salsa.entity.event.ListModel;
import fr.soleil.salsa.entity.event.ScanAddOnModel;
import fr.soleil.salsa.entity.event.listener.IListener;
import fr.soleil.salsa.entity.impl.Stage;

/**
 * @author Alike
 * 
 *         Class relative to the controller of the Hooks fonctionnality.
 * 
 */
public class HooksController extends AController<HooksListView> implements IHooksController {

    /**
     * Class relative to the listening of actions that could occur on the model and which concerns
     * the hooks fonctionnality.
     * 
     * @param <T>
     */
    private class MultiActionListener<T> implements IListener<EntityPropertyChangedEvent<T>> {

        @Override
        public void notifyEvent(EntityPropertyChangedEvent<T> event) {
            refresh();
        }
    }

    /**
     * The maximum number of commands that are allowed for a Hook.
     */
    private static final int MAXIMUM_LIMIT_COMMAND = 10;

    /**
     * The current config.
     */
    private IConfig<?> config;

    /**
     * The listener on modifications which could occur on the command model.
     */
    private MultiActionListener<IHookCommand> hookCommandsActionListener;

    /**
     * Hooks.
     */
    private List<IHook> hooks;

    /**
     * The listener on modifications which could occur on the hook model.
     */
    private MultiActionListener<IHook> hooksActionListener;

    /**
     * The listener on modifications which could occur on the scan add on model.
     */
    private MultiActionListener<IScanAddOns> scanAddOnActionListener;

    protected List<IHookCommand> commandsSelected = new ArrayList<IHookCommand>();
    protected List<IHook> hooksSelected = new ArrayList<IHook>();

    /**
     * Default constructor.
     */
    public HooksController() {
        this(null);
    }

    /**
     * Constructor.
     */
    public HooksController(HooksListView view) {
        super(view);
    }

    @Override
    public void setView(HooksListView view) {
        if (this.view != null) {
            this.view.enableListeners(false);
            this.view.clearHooks();
        }
        super.setView(view);
        refresh();
    }

    @Override
    protected HooksListView generateView() {
        return new HooksListView(this);
    }

    /**
     * Returns the next "free" stage that could be used.
     * 
     * @return
     */
    private Stage getFirstStageUnreserved() {
        List<Stage> stageReserved = getStageReserved();
        Stage[] stages = Stage.values();
        for (int i = 0; i < stages.length; i++) {
            Stage s = stages[i];
            if (!stageReserved.contains(s)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Returns the stages that are already used by an existing hook.
     * 
     * @return
     */
    private List<Stage> getStageReserved() {
        List<Stage> stageReserved = new ArrayList<Stage>();
        for (IHook h : hooks) {
            stageReserved.add(h.getStage());
        }
        return stageReserved;
    }

    @Override
    public void notifyDeleteCommandAction(int id, List<Integer> commandToDel) {
        IHook h = hooks.get(id);
        List<IHookCommand> commandsList = h.getCommandsList();

        for (int i = 0; i < commandToDel.size(); i++) {

            int index = commandToDel.get(i) - i;
            commandsList.remove(index);
        }
        commandsSelected.clear();
        if (config != null) {
            config.setModified(true);
        }
    }

    @Override
    public void notifyDeleteHookAction(List<Integer> toDel) {
        for (int i = 0; i < toDel.size(); i++) {
            int index = toDel.get(i) - i;
            hooks.remove(index);
        }
        hooksSelected.clear();
        if (config != null) {
            config.setModified(true);
        }
    }

    @Override
    public void notifyHookCommandChanged(int hookId, int id, String value) {
        IHook h = hooks.get(hookId);
        List<IHookCommand> commandsList = h.getCommandsList();
        IHookCommand hc = commandsList.get(id);
        hc.setCommand(value);
        if (config != null) {
            config.setModified(true);
        }

    }

    @Override
    public void notifyNewCommand(int id) {
        IHook h = hooks.get(id);
        if (h.getCommandsList() == null) {
            h.setCommandsList(new ListModel<IHookCommand, HookModel>(new ArrayList<IHookCommand>(),
                    ((HookModel) h), "commandsList"));
        }
        List<IHookCommand> commands = h.getCommandsList();
        commands.add(new HookCommandModel());
        if (config != null) {
            config.setModified(true);
        }
    }

    @Override
    public void notifyNewHookAction() {
        IHook h = new HookModel();
        Stage s = getFirstStageUnreserved();
        h.setStage(s);
        hooks.add(h);
        if (config != null) {
            config.setModified(true);
        }
    }

    @Override
    public void notifyStageAction(int id, String value) {
        IHook h = hooks.get(id);
        Stage s = Stage.valueOf(Stage.class, value);
        h.setStage(s);
        if (config != null) {
            config.setModified(true);
        }
    }

    /**
     * Refresh the view data from the model.
     */
    public void refresh() {
        if (hooks != null) {
            view.enableListeners(false);
            List<Stage> stageReserved = getStageReserved();

            int i = 0;
            Stage[] stages = Stage.values();
            view.clearHooks();
            for (IHook h : hooks) {

                view.addHooks(i);
                view.clearStages(i);

                boolean isSelected = myContains(hooksSelected, h);
                view.setSelectedHook(i, isSelected);

                // view.clearCommand(i);
                int iStage = 0;
                for (int j = 0; j < stages.length; j++) {
                    Stage s = stages[j];

                    if ((!stageReserved.contains(s)) || s == h.getStage()) {
                        view.addStage(i, s.name(), s.getValue());
                        if (s == h.getStage()) {
                            view.setSelectedStage(i, iStage);
                        }
                        iStage++;
                    }
                }

                /* Commands */
                List<IHookCommand> commandsList = h.getCommandsList();
                int nbCommands = 0;
                if (commandsList != null) {
                    nbCommands = commandsList.size();
                    for (int k = 0; k < nbCommands; k++) {
                        IHookCommand hc = commandsList.get(k);
                        boolean selected = (myContains(commandsSelected, hc));
                        view.addCommand(i, k, hc.getCommand());
                        ((HookCommandModel) hc).addListener(hookCommandsActionListener);

                        view.setSelectedCommand(i, k, selected);
                    }
                }
                view.setEnableAddCommand(i, nbCommands < MAXIMUM_LIMIT_COMMAND);
                view.setEnableDelCommand(i, nbCommands > 0);

                ((HookModel) h).addListener(hooksActionListener);
                i++;
            }
            boolean hooksLimitReached = (hooks.size() == stages.length);
            view.setEnableAddHook(!hooksLimitReached);
            view.setEnableDelHook(hooks.size() != 0);
            view.enableListeners(true);
        }
    }

    private boolean myContains(List<?> l, Object o) {
        for (int i = 0; i < l.size(); i++) {
            Object o2 = l.get(i);
            if (o2 == o)
                return true;
        }
        return false;
    }

    /**
     * The current config.
     */
    public IConfig<?> getConfig() {
        return config;
    }

    /**
     * Sets the config.
     * 
     * @param config
     */
    public void setConfig(IConfig<?> config) {

        if (config == null) {
            clear();
            return;
        }

        this.config = config;
        if (config.getScanAddOn() == null) {
            config.setScanAddOn(new ScanAddOnModel());
        }
        IScanAddOns scanAddOns = config.getScanAddOn();

        if (scanAddOns.getHooks() == null) {
            scanAddOns.setHooks(new ListModel<IHook, ScanAddOnModel>(new ArrayList<IHook>(),
                    ((ScanAddOnModel) scanAddOns), "hooksList"));
        }
        this.hooks = scanAddOns.getHooks();

        hooksActionListener = new MultiActionListener<IHook>();
        hookCommandsActionListener = new MultiActionListener<IHookCommand>();
        scanAddOnActionListener = new MultiActionListener<IScanAddOns>();

        ((ScanAddOnModel) scanAddOns).addListener(scanAddOnActionListener);

        refresh();
    }

    private void clear() {
        view.enableListeners(false);
        view.clearHooks();
    }

    @Override
    public void notifyHookSelected(int hookId, boolean selected) {
        IHook h = hooks.get(hookId);
        if (selected) {
            hooksSelected.add(h);
        }
        else {
            MyRemove(hooksSelected, h);
        }
    }

    private void MyRemove(List<IHook> list, IHook hook) {
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            IHook h = list.get(i);
            if (hook == h) {
                index = i;
            }
        }
        if (index != -1) {
            list.remove(index);
        }
    }

    public void notifyCommandSelected(int hookId, int commandId, boolean selected) {
        IHook h = hooks.get(hookId);
        if (h.getCommandsList() != null && h.getCommandsList().size() > commandId) {
            IHookCommand c = h.getCommandsList().get(commandId);

            if (selected) {
                commandsSelected.add(c);
            }
            else {
                commandsSelected.remove(c);
            }
        }
    }
}

package fr.soleil.salsa.client.controller;

import java.util.List;

import fr.soleil.salsa.client.view.HookTableListView;
import fr.soleil.salsa.entity.IHookCommand;
import fr.soleil.salsa.entity.impl.Stage;

public interface IHookCommandController extends IController<HookTableListView> {

    public void hookListChanged(Stage stage, List<IHookCommand> commandList);

    public void addHookCommand(Stage stage);

    public void selectedHookCommandChanged();
}

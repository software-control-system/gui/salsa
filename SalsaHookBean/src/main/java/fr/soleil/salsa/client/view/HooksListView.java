package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.soleil.salsa.client.controller.IHooksController;

/**
 * @author Alike
 * 
 *         Class relative to the list view of hooks.
 */
public class HooksListView extends JPanel implements IView<IHooksController> {

    private static final long serialVersionUID = 1380316063045408758L;

    /**
     * The listener on every action that could occur on the list view.
     */
    private ActionListener actionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btnAddHook) {
                handleNewHookAction();
            }
            else if (e.getSource() == btnDelHook) {
                handleDelHookAction();
            }

        }
    };

    /**
     * The button to add an hook.
     */
    private JButton btnAddHook;

    /**
     * The button to delete an hook.
     */
    private JButton btnDelHook;

    /**
     * The controller.
     */
    private IHooksController controller;

    /**
     * The main panel.
     */
    private JPanel mainPanel;

    /**
     * The panel which contains hooks "list".
     */
    private JPanel stagesPanel;

    /**
     * The views that represents an hook detail.
     */
    private List<HooksDetailView> stageViews = new ArrayList<HooksDetailView>();

    /**
     * Constructor
     */
    public HooksListView() {
        this(null);
    }

    /**
     * The constructor
     * 
     * @param controller
     */
    public HooksListView(IHooksController controller) {
        this.controller = controller;
        initializeLayout();
    }

    /**
     * Returns the controller of this view.
     */
    @Override
    public IHooksController getController() {
        return controller;
    }

    @Override
    public void setController(IHooksController controller) {
        this.controller = controller;
        for (HooksDetailView detailView : stageViews) {
            detailView.setController(controller);
        }
    }

    /**
     * Add a command on the hook (Only visually)
     * 
     * @param hookIndex The hook id (index) on which the command must be added
     * @param commandIndex The command id (index)
     * @param command The command to add
     */
    public void addCommand(int hookIndex, int commandIndex, String command) {
        HooksDetailView h = stageViews.get(hookIndex);
        h.addCommand(commandIndex, command);
    }

    /**
     * Add an hook to the end of the view.
     * 
     * @param index The view id (index)
     */
    public void addHooks(int index) {
        HooksDetailView h = new HooksDetailView(index, controller);
        stageViews.add(h);
        stagesPanel.add(h);// , createGBC(0,index));
    }

    /**
     * Add a stage item to the combobox of an existing hook.
     * 
     * @param hookIndex The hood id (index)
     * @param value The item id
     * @param text The item text
     */
    public void addStage(int hookIndex, String value, String text) {
        HooksDetailView h = stageViews.get(hookIndex);
        h.addStage(value, text);
    }

    /**
     * Clear all the hooks.
     */
    public void clearHooks() {
        for (HooksDetailView hookDetailView : stageViews) {
            hookDetailView.clearStages();
        }
        stageViews.clear();
        stageViews = new ArrayList<HooksDetailView>();
        stagesPanel.removeAll();
    }

    /**
     * Clear all the stages of the combobox.
     * 
     * @param index
     */
    public void clearStages(int index) {
        HooksDetailView h = stageViews.get(index);
        h.clearStages();
    }

    /**
     * Provide with a gridbagconstraint with defaults values. Utility method. Should ideally be
     * moved to another projet.
     * 
     * @param x
     * @param y
     * @return
     */
    private GridBagConstraints createGBC(int x, int y) {
        GridBagConstraints r = new GridBagConstraints();
        r.fill = GridBagConstraints.BOTH;
        r.insets = new Insets(2, 8, 2, 8);
        r.gridx = x;
        r.gridy = y;
        return r;
    }

    /**
     * Provide with a gridbagconstraint with defaults values. Utility method. Should ideally be
     * moved to another projet.
     * 
     * @param x
     * @param y
     * @param colspan
     * @return
     */
    private GridBagConstraints createGBC(int x, int y, int colspan) {
        GridBagConstraints r = createGBC(x, y);
        r.gridwidth = colspan;
        return r;
    }

    /**
     * Enable all listeners of this view (and child views).
     * 
     * @param enable
     */
    public void enableListeners(boolean enable) {
        if (enable) {
            btnAddHook.addActionListener(actionListener);
            btnDelHook.addActionListener(actionListener);
        }
        else {
            btnAddHook.removeActionListener(actionListener);
            btnDelHook.removeActionListener(actionListener);
        }

        for (int i = 0; i < stageViews.size(); i++) {
            HooksDetailView hdv = stageViews.get(i);
            hdv.enableListeners(enable);
        }
    }

    /**
     * Return the selected hooks of the view.
     * 
     * @return The list of index of selected hooks
     */
    public List<Integer> getSelectedIndex() {
        List<Integer> result = new ArrayList<Integer>();
        for (HooksDetailView s : stageViews) {
            if (s.isSelected()) {
                result.add(s.getId());
            }
        }
        return result;
    }

    /**
     * Handle with the action "Delete an hook".
     */
    private void handleDelHookAction() {
        List<Integer> toDel = getSelectedIndex();
        if (controller != null) {
            controller.notifyDeleteHookAction(toDel);
        }
    }

    /**
     * Handle with the action "New hook".
     */
    private void handleNewHookAction() {
        if (controller != null) {
            controller.notifyNewHookAction();
        }
    }

    /**
     * Initialize the layout.
     */
    private void initializeLayout() {
        this.setLayout(new BorderLayout());

        mainPanel = new JPanel();
        GridBagLayout mainGBL = new GridBagLayout();
        mainGBL.columnWeights = new double[] { 1, 50, 1 };
        mainPanel.setLayout(mainGBL);

        btnAddHook = new JButton("Add");
        btnAddHook.setText("+");
        btnAddHook.setEnabled(false);
        mainPanel.add(btnAddHook, createGBC(0, 0));

        btnDelHook = new JButton("-");
        btnDelHook.setEnabled(false);
        mainPanel.add(btnDelHook, createGBC(2, 0));

        stagesPanel = new JPanel();
        BoxLayout stagesGBL = new BoxLayout(stagesPanel, BoxLayout.Y_AXIS);
        stagesPanel.setLayout(stagesGBL);

        mainPanel.add(stagesPanel, createGBC(0, 1, 3));

        JScrollPane jsp = new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        this.add(jsp, BorderLayout.CENTER);
    }

    /**
     * Enable/Disable the "Add command" button for the given hook
     * 
     * @param index The hook id (index)
     * @param b True if enabled
     */
    public void setEnableAddCommand(int index, boolean b) {
        HooksDetailView h = stageViews.get(index);
        h.setEnableAddCommand(b);
    }

    /**
     * Enable/Disable the "Add hook" button.
     * 
     * @param enable
     */
    public void setEnableAddHook(boolean enable) {
        btnAddHook.setEnabled(enable);
    }

    /**
     * Enable/Disable the "Delete command" button for the given hook.
     * 
     * @param index The hook id (index)
     * @param b
     */
    public void setEnableDelCommand(int index, boolean b) {
        HooksDetailView h = stageViews.get(index);
        h.setEnableDelCommand(b);
    }

    /**
     * Enable/Disable the "Delete hook" button.
     * 
     * @param enable
     */
    public void setEnableDelHook(boolean enable) {
        btnDelHook.setEnabled(enable);
    }

    /**
     * @param hookIndex
     * @param stageIndex
     */
    public void setSelectedStage(int hookIndex, int stageIndex) {
        HooksDetailView h = stageViews.get(hookIndex);
        h.setSelectedStage(stageIndex);
    }

    public void setSelectedHook(int hookIndex, boolean selected) {
        HooksDetailView h = stageViews.get(hookIndex);
        h.setSelected(selected);
    }

    public void setSelectedCommand(int hookId, int commandId, boolean selected) {
        HooksDetailView h = stageViews.get(hookId);
        h.setSelectedCommand(commandId, selected);
    }

}

package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;

import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.dialog.JDialogUtils;
import fr.soleil.salsa.client.Salsa;
import fr.soleil.salsa.logging.LoggingUtil;

public class AboutDialog extends JDialog {

    private static final long serialVersionUID = 7089227249167130045L;

    public static final Logger LOGGER = LoggingUtil.getLogger(AboutDialog.class);
    private static final String ABOUT_FILENAME = "About.html";

    private JEditorPane htmlPanel;

    // this tells if the url is loaded or not
    private final AtomicBoolean urlIsLoaded = new AtomicBoolean(false);
    // swingworker that will wait until the url is loaded
    private WaitingWorker waitingWorker;

    /**
     * This class is only here to delay the dialog visibility until the url is
     * loaded!! Once the url is loaded, the JEditorPane knows its size, and
     * pack() on the dialog will be accurate.
     * 
     * Either propertyChange or doInBackground can be called first, so we need
     * to check if we have to wait, in doInBackground.
     * 
     * @author mainguy
     */
    private class WaitingWorker extends SwingWorker<Void, Void> implements PropertyChangeListener {

        @Override
        public void propertyChange(final PropertyChangeEvent evt) {
            // the page has changed, meaning it is loaded
            synchronized (this) {
                urlIsLoaded.set(true);
                notify();
            }
        }

        @Override
        protected Void doInBackground() throws Exception {
            synchronized (this) {
                if (!urlIsLoaded.get()) {
                    wait(100);
                }
            }
            return null;
        }

        @Override
        protected void done() {
            reallySetVisible();
        }
    }

    public AboutDialog(final Frame owner) {
        super(owner);

        setModal(true);
        setTitle("About Salsa scan application");

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        waitingWorker = new WaitingWorker();

        htmlPanel = new JEditorPane();
        htmlPanel.setEditorKit(new HTMLEditorKit());
        htmlPanel.setEditable(false);
        // listen for the "page" property, useful for worker to know when load
        // is completed
        htmlPanel.addPropertyChangeListener("page", waitingWorker);

        // let's mimic panel background color
        htmlPanel.setBackground(UIManager.getColor("Panel.background"));

        htmlPanel.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    // if uri is a file, get a temp version extracted from the
                    // jar and ask OS to open it,
                    // else ask OS to browse the link
                    if (Desktop.isDesktopSupported()) {
                        try {
                            URI uri = e.getURL().toURI();
                            String scheme = uri.getScheme();
                            String file = e.getURL().getFile();

                            if (scheme.equalsIgnoreCase("jar")) {
                                String[] pathInfo = file.split("!/");
                                String jarFileName = pathInfo[0].replaceFirst("file:", "");
                                String entryName = pathInfo[1];

                                String extension = FileUtils.getExtension(file);
                                String fileName = file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf("."));
                                openFileInJar(jarFileName, entryName, fileName, extension);
                            } else if (scheme.equalsIgnoreCase("file")) {
                                Desktop.getDesktop().browse(uri);
                            }
                        } catch (Exception ex) {
                            LOGGER.error("Cannot open file {}", ex.getMessage());
                            LOGGER.trace("Stack trace", ex);
                        }
                    }
                }
            }
        });

        URL aboutURL = Salsa.class.getResource(ABOUT_FILENAME);
        try {
            htmlPanel.setPage(aboutURL);
            new Thread("Update version later") {
                @Override
                public void run() {
                    try {
                        sleep(200);
                        final StringBuilder versionBuilder = new StringBuilder();
                        ResourceBundle bundle = ResourceBundle
                                .getBundle(ApplicationView.class.getPackage().getName() + ".application");
                        versionBuilder.append(bundle.getString("project.version")).append(" (");
                        versionBuilder.append(bundle.getString("build.date")).append(")");
                        SwingUtilities.invokeLater(() -> {
                            htmlPanel.setText(htmlPanel.getText().replace("X.X.X", versionBuilder.toString()));
                        });
                    } catch (InterruptedException e) {
                        // nothing to do;
                    }
                };
            }.start();
        } catch (IOException e) {
            LOGGER.error("Cannot open file {}", e.getMessage());
            LOGGER.trace("Stack trace", e);
        }

        // allow to close the dialog with Esc
        JDialogUtils.installEscapeCloseOperation(this);
    }

    private static void openFileInJar(final String jarFileName, final String entryName, final String fileName,
            final String extension) {

        LOGGER.trace("jarFileName={}", jarFileName);
        LOGGER.trace("entryName={}", entryName);
        LOGGER.trace("fileName={}", fileName);
        LOGGER.trace("extension={}", extension);

        try (JarFile jarFile = new JarFile(jarFileName);) {
            JarEntry entry = jarFile.getJarEntry(entryName);
            InputStream is = jarFile.getInputStream(entry);

            // create a temp file named after the one from
            // the jar, with the same extension
            File tempFile = File.createTempFile(fileName, "." + extension);
            FileUtils.duplicateFile(is, tempFile, true);
            tempFile.deleteOnExit();
            LOGGER.trace("Extracting file to {}", tempFile.getAbsolutePath());

            Desktop.getDesktop().open(tempFile);

        } catch (IOException e) {
            LOGGER.error("Cannot open file {}", e.getMessage());
            LOGGER.trace("Stack trace", e);
        }
    }

    private void layoutComponents() {
        JScrollPane scrollPane = new JScrollPane(htmlPanel);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(scrollPane, BorderLayout.CENTER);
        setContentPane(contentPane);
    }

    private void reallySetVisible() {
        // adjust to real size
        // pack();
        // limit to picture size
        setSize(345, 600);
        // center the dialog
        setLocationRelativeTo(getOwner());
        // now set it visible
        super.setVisible(true);
    }

    /**
     * Overridden to ensure the url is loaded before showing dialog
     * 
     * @see java.awt.Dialog#setVisible(boolean)
     */
    @Override
    public void setVisible(final boolean b) {
        if (b) {
            if (urlIsLoaded.get()) {
                // skip waiting worker
                reallySetVisible();
            } else {
                waitingWorker.execute();
            }
        } else {
            super.setVisible(b);
        }
    }

    public static void main(final String[] args) {
        AboutDialog aboutDialog = new AboutDialog(null);

        aboutDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        aboutDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        aboutDialog.setLocationRelativeTo(null);
        aboutDialog.setVisible(true);
    }
}

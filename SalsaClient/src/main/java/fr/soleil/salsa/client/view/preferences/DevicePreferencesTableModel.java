package fr.soleil.salsa.client.view.preferences;

import java.util.Optional;

import javax.swing.table.AbstractTableModel;

import fr.soleil.salsa.client.view.preferences.devices.DeviceCell;
import fr.soleil.salsa.client.view.preferences.devices.DeviceCellFactory;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * Device preferences table model
 * 
 * @author Patrick Madela
 *
 */
public class DevicePreferencesTableModel extends AbstractTableModel {
    /** Serialization id */
    private static final long serialVersionUID = 6463559316270554307L;

    /** Property name column */
    public static final int PROPERTY_NAME_COLUMN = 0;

    /** Property value column */
    public static final int PROPERTY_VALUE_COLUMN = 1;

    /** The column names */
    private static final String[] COLUMN_NAMES = { "Property", "Value" };

    /** The device preferences */
    private transient DevicePreferences devicePreferences;

    /** The table values */
    private transient Object[][] values;

    /**
     * Create a new instance
     * 
     * @param devicePreferences the device preferences
     */
    public DevicePreferencesTableModel(DevicePreferences devicePreferences) {
        super();
        setPreferences(devicePreferences);
    }

    /**
     * Set device preferences
     * 
     * @param devicePreferences the device preferences
     */
    public void setPreferences(DevicePreferences devicePreferences) {
        this.devicePreferences = devicePreferences;
        updateValues();
    }

    private void updateValues() {
        values = new Object[][] {
                DeviceCellFactory.of("ScanServer")
                        .withValue(getDevicePreferences().map(DevicePreferences::getScanServer).orElse("")).get(),
                DeviceCellFactory.of("DataFitter")
                        .withValue(getDevicePreferences().map(DevicePreferences::getDataFitter).orElse("")).get(),
                { "DataRecorder", getDevicePreferences().map(DevicePreferences::isDataRecorder).orElse(Boolean.FALSE) },
                DeviceCellFactory.of("Publisher")
                        .withValue(getDevicePreferences().map(DevicePreferences::getPublisher).orElse("")).get(),
                DeviceCellFactory.of("RecordingManager")
                        .withValue(getDevicePreferences().map(DevicePreferences::getRecordingManager).orElse("")).get(),
                { "RecordingManager profil",
                        getDevicePreferences().map(DevicePreferences::getRecordingManagerProfil).orElse("") } };
        fireTableDataChanged();
    }

    private Optional<DevicePreferences> getDevicePreferences() {
        return Optional.ofNullable(devicePreferences);
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public int getRowCount() {
        return values.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = values[rowIndex][columnIndex];
        if (rowIndex == 2 && columnIndex == 1) {
            return Boolean.valueOf(value.toString());
        }
        return values[rowIndex][columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null && columnIndex == 1) {
            String name = (String) getValueAt(rowIndex, 0);
            Object oldValue = values[rowIndex][columnIndex];
            if (oldValue instanceof DeviceCell) {
                ((DeviceCell) oldValue).setValue(aValue.toString().trim());
            } else {
                values[rowIndex][columnIndex] = aValue;
            }
            setDevicePreferencesValue(name, aValue);
        }
    }

    private void setDevicePreferencesValue(String name, Object value) {
        if (name.equalsIgnoreCase("ScanServer")) {
            devicePreferences.setScanServer(value.toString().trim());
        } else if (name.equalsIgnoreCase("DataFitter")) {
            devicePreferences.setDataFitter(value.toString().trim());
        } else if (name.equalsIgnoreCase("DataRecorder")) {
            devicePreferences.setDataRecorder(Boolean.parseBoolean(value.toString().trim()));
        } else if (name.equalsIgnoreCase("Publisher")) {
            devicePreferences.setPublisher(value.toString().trim());
        } else if (name.equalsIgnoreCase("RecordingManager")) {
            devicePreferences.setRecordingManager(value.toString().trim());
        } else if (name.equalsIgnoreCase("RecordingManager profil")) {
            devicePreferences.setRecordingManagerProfil(value.toString().trim());
        }
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public boolean isCellEditable(int r, int c) {
        return c == PROPERTY_VALUE_COLUMN;
    }
}
package fr.soleil.salsa.client.exception;

import fr.soleil.salsa.exception.SalsaException;

/**
 * An exception that can happen when starting/stopping a scan
 * 
 * @author girardot
 */
public class StartStopException extends SalsaException {

    private static final long serialVersionUID = 6801965172285714921L;

    public StartStopException() {
        super();
    }

    public StartStopException(String message) {
        super(message);
    }

    public StartStopException(Throwable cause) {
        super(cause);
    }

    public StartStopException(String message, Throwable cause) {
        super(message, cause);
    }

}

package fr.soleil.salsa.client.view.preferences.devices;

import java.util.Optional;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * Abstract Device cell
 * 
 * @author Patrick Madela
 */
public abstract class AbstractDeviceCell implements DeviceCell {
    /** The logger for messages */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDeviceCell.class);

    /** The expected device class */
    private final String expectedDeviceClass;

    /** The combobox model */
    private final ComboBoxModel<String> comboBoxModel;

    /** The current cell value */
    private String value;

    /**
     * Create a instance but is hidden. Use method of
     * 
     * @param expectedDeviceClass the expected device class
     */
    protected AbstractDeviceCell(String expectedDeviceClass) {
        this.expectedDeviceClass = expectedDeviceClass;
        String[] devices = ArrayUtils.add(getDevicesOfExpectedDeviceClass(), 0, "");
        comboBoxModel = new DefaultComboBoxModel<>(devices);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
        comboBoxModel.setSelectedItem(value);
    }

    @Override
    public AbstractDeviceCell withValue(String value) {
        setValue(value);
        return this;
    }

    @Override
    public String toString() {
        return (String) comboBoxModel.getSelectedItem();
    }

    @Override
    public Object[] get() {
        return new Object[] { expectedDeviceClass, this };
    }

    @Override
    public TableCellEditor getCellEditor() {
        return new DefaultCellEditor(getComboBox());
    }

    private JComboBox<String> getComboBox() {
        return new JComboBox<>(comboBoxModel);
    }

    private String[] getDevicesOfExpectedDeviceClass() {
        Optional<Database> optionalDatabase = Optional.ofNullable(TangoDeviceHelper.getDatabase());
        if (optionalDatabase.isPresent()) {
            try {
                return getDevicesOfExpectedDeviceClass(optionalDatabase.get());
            } catch (IllegalArgumentException e) {
                LOGGER.warn("Unable to get list of devices for expeted device class {}: {}", expectedDeviceClass,
                        e.getMessage());
                LOGGER.trace("Unable to get list of devices for expeted device class {}", expectedDeviceClass, e);
            }
        }
        return new String[0];
    }

    private String[] getDevicesOfExpectedDeviceClass(Database database) {
        try {
            return database.get_device_exported_for_class(expectedDeviceClass);
        } catch (DevFailed e) {
            throw new IllegalArgumentException("An error occurred when get list of devices for expected device class \""
                    + expectedDeviceClass + "\"", e);
        }
    }

}

package fr.soleil.salsa.client.view.tool;

import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.util.List;

/**
 * Warns the listener of a drop operation.
 * 
 * @see DragAndDropJTree
 */
public interface IDropListener {

    /**
     * Called when the user completes a drag and drop operation.
     * 
     * @param droppedSelection
     * @param target
     */
    public void dropped(List<?> droppedSelection, Object target);

    /**
     * Called when the user starts a drag.
     */
    public void startDragAndDrop(DragGestureEvent dge, List<?> selection);

    /**
     * Called when the user ends a drag & drop, regardless of success.
     */
    public void endDragAndDrop(DragSourceDropEvent dsde);
}

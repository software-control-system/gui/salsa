package fr.soleil.salsa.client.view.preferences.devices;

/**
 * ScanServer Device preferences cell
 * 
 * @author Patrick Madela
 */
public class ScanServerDevicePreferencesCell extends AbstractDeviceCell {
    /** Class of ScanServer */
    private static final String DEVICE_CLASS = "ScanServer";

    /**
     * Create a new Instance
     * 
     * @param value the value
     */
    public ScanServerDevicePreferencesCell() {
        super(DEVICE_CLASS);
    }
}

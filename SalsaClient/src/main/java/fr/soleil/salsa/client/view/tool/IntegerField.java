package fr.soleil.salsa.client.view.tool;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JTextField;

/**
 * 
 * @author Administrateur
 */
public class IntegerField extends JTextField {

    private static final long serialVersionUID = 7564494676243674709L;

    /**
     * The number format.
     */
    private NumberFormat format;

    /**
     * The locale to be used to format the number.
     */
    private Locale locale;

    /**
     * Construtor. Create a new DoubleField using the default locales and format.
     */
    public IntegerField() {
        this(null, Locale.getDefault(), NumberFormat.getIntegerInstance());
    }

    /**
     * Constructor.
     * 
     * @param value the value.
     * @param locale the locale to be used.
     * @param format the number format to be used.
     */
    public IntegerField(Integer value, Locale locale, NumberFormat format) {
        assert (locale != null);
        assert (format != null);
        this.format = format;
        this.locale = locale;
        this.setValue(value);
        this.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent focusEvent) {
                setValue(getValue());
            }
        });
    }

    /**
     * Returns the value as a double.
     * 
     * @return
     */
    public Integer getValue() {
        Number number;
        Integer value;
        String stringValue;
        try {
            stringValue = this.getText();
            number = format.parse(stringValue);
            value = number.intValue();
        }
        catch (ParseException e) {
            // The user input is not a number, maybe blank.
            value = null;
        }
        return value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     */
    public void setValue(Integer value) {
        String stringValue = value != null ? format.format(value) : "";
        this.setText(stringValue);
    }

    /**
     * The number format.
     * 
     * @return
     */
    public NumberFormat getFormat() {
        return format;
    }

    /**
     * The number format.
     */
    public void setFormat(NumberFormat format) {
        this.format = format;
    }

    /**
     * The locale to be used to format the number.
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * The locale to be used to format the number.
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}

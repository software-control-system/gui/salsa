package fr.soleil.salsa.client.view.tool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import fr.soleil.salsa.client.exception.SalsaPerspectiveException;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.view.AbstractTangoBoxView;
import fr.soleil.salsa.client.view.DefaultView;
import fr.soleil.salsa.logging.LoggingUtil;
import net.infonode.docking.RootWindow;
import net.infonode.docking.View;
import net.infonode.docking.util.ViewMap;

/**
 * Class to manage window layout
 * 
 * @author madela
 */
public class WindowLayoutManager implements Serializable {
    /** Serialization id */
    private static final long serialVersionUID = 3065815691214846852L;

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(WindowLayoutManager.class);

    /** The main window. */
    private final RootWindow rootWindow;

    /** The view map */
    private final transient ViewMap viewMap;

    /** To lock and unlock window layout */
    private final DockingWindowLocker dockingWindowLocker;

    /**
     * Create a new instance
     * 
     * @param rootWindow the main window
     * @param viewMap the map of views
     */
    public WindowLayoutManager(RootWindow rootWindow, ViewMap viewMap) {
        this.rootWindow = rootWindow;
        this.viewMap = viewMap;
        this.dockingWindowLocker = DockingWindowLocker.instance(rootWindow, viewMap);
        updateDockingWindowLock();
    }

    /**
     * Gets the current window layout. The window layout is the position and
     * state of the views in the application. Note : do not call those methods
     * directly from outside the controller. Use the controller instead.
     * 
     * @return the current window layout
     * @throws SalsaPerspectiveException
     */
    public WindowLayout getWindowLayout() throws SalsaPerspectiveException {
        try {
            WindowLayout windowLayout = new WindowLayout();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            rootWindow.write(objectOutputStream);
            objectOutputStream.close();
            byteArrayOutputStream.close();
            byte[] layoutContent = byteArrayOutputStream.toByteArray();
            windowLayout.setContentArray(layoutContent);
            return windowLayout;
        } catch (IOException e) {
            throw new SalsaPerspectiveException(e.getMessage(), e);
        }
    }

    /**
     * Sets the current window layout. The window layout is the position and
     * state of the views in the application. Note : do not call those methods
     * directly from outside the controller. Use the controller instead.
     * 
     * @param windowLayout
     * @throws SalsaPerspectiveException
     */
    public void setWindowLayout(WindowLayout windowLayout) throws SalsaPerspectiveException {
        try {
            byte[] layoutContent = windowLayout.getContentArray();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(layoutContent);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            rootWindow.read(objectInputStream);
            objectInputStream.close();
            byteArrayInputStream.close();
        } catch (IOException | NullPointerException e) {
            throw new SalsaPerspectiveException(e.getMessage(), e);
        }
        updateDockingWindowLock();
        updateVisibility();
    }

    private void updateVisibility() {
        getVisibleDefaultView().map(View::getComponent).filter(AbstractTangoBoxView.class::isInstance)
                .map(AbstractTangoBoxView.class::cast).forEach(v -> v.updateBeanVisibility(true));
        getNotVisibleDefaultView().map(View::getComponent).filter(AbstractTangoBoxView.class::isInstance)
                .map(AbstractTangoBoxView.class::cast).forEach(v -> v.updateBeanVisibility(false));
    }

    private Stream<DefaultView> getVisibleDefaultView() {
        return DockingWindowWalker.walk(rootWindow).filter(DefaultView.class::isInstance).map(DefaultView.class::cast)
                .distinct();

    }

    private Stream<DefaultView> getNotVisibleDefaultView() {
        Set<DefaultView> visibleDefaultView = getVisibleDefaultView().collect(Collectors.toSet());
        return DockingWindowWalker.walk(viewMap).filter(DefaultView.class::isInstance).map(DefaultView.class::cast)
                .distinct().filter(v -> !visibleDefaultView.contains(v));
    }

    /**
     * Lock layout
     */
    public void lock() {
        LOGGER.debug("Lock layout");
        dockingWindowLocker.lock();
    }

    /**
     * Unlock layout
     */
    public void unlock() {
        LOGGER.debug("Unlock layout");
        dockingWindowLocker.unlock();
    }

    /**
     * Update docking window lock from UI preferences, because docking window locking state is persisted in UI
     * preferences
     */
    private void updateDockingWindowLock() {
        dockingWindowLocker.setLock(UIPreferences.getInstance().isDockingWindowLocked());
    }

}

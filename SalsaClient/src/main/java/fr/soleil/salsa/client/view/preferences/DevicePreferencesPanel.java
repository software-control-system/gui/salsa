package fr.soleil.salsa.client.view.preferences;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * Device preferences panel
 * 
 * @author Tarek
 * @author Patrick Madela
 */
public class DevicePreferencesPanel extends JPanel implements Preference {
    /** Serialization id */
    private static final long serialVersionUID = 5449675279584477694L;

    /** The device preferences */
    private transient DevicePreferences devicePreferences;

    /** The device preferences table */
    private DevicePreferencesTable devicePreferencesTable;

    /** The device preferences table model */
    private final DevicePreferencesTableModel tableModel;

    /**
     * Create a new instance
     * 
     * @param devicePreferences the device preferences
     */
    public DevicePreferencesPanel(DevicePreferences devicePreferences) {
        super();
        this.devicePreferences = devicePreferences;
        setLayout(new BorderLayout());
        tableModel = new DevicePreferencesTableModel(devicePreferences);
        devicePreferencesTable = new DevicePreferencesTable(tableModel);
        devicePreferencesTable.setRowSelectionAllowed(false);
        devicePreferencesTable.setColumnSelectionAllowed(false);
        JScrollPane jsp = new JScrollPane(devicePreferencesTable);
        add(jsp, BorderLayout.CENTER);

    }

    /**
     * Validate table
     */
    public void validateTable() {
        if (devicePreferencesTable != null && devicePreferencesTable.getCellEditor() != null) {
            devicePreferencesTable.getCellEditor().stopCellEditing();
        }
    }

    /**
     * Get device preferences
     * 
     * @return Returns the device preferences.
     */
    public DevicePreferences getDevicePreferences() {
        return devicePreferences;
    }

    /**
     * Set device preferences
     * 
     * @param devicePreferences the device preferences
     */
    public void setPreferences(DevicePreferences devicePreferences) {
        this.devicePreferences = devicePreferences;
        tableModel.setPreferences(devicePreferences);
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public String getIconTitle() {
        return "Devices";
    }

    @Override
    public String getIconUrl() {
        return "icons/devices32x32.png";
    }

    @Override
    public String getTitle() {
        return "Devices Preferences";
    }

}

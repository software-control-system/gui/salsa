package fr.soleil.salsa.client.controller.impl;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;

/**
 * Controller for DataFitter bean
 */
public class DataFitterController extends AbstractTangoBoxController {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(DataFitterController.class);

    /**
     * Create a new instance
     * 
     * @param bean the DataFitter bean
     */
    public DataFitterController(AbstractTangoBox bean) {
        super(bean);
    }

    @Override
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        if (devicePreferencesEvent.getSource().equals(devicePreferences)) {
            switch (devicePreferencesEvent.getType()) {
                case DATAFITTER_CHANGED:
                case LOADING:
                    view.setBeanModel(devicePreferencesEvent.getSource().getDataFitter());
                    break;
                default:
                    break;
            }
        }
    }
}

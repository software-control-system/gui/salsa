package fr.soleil.salsa.client.view.tool;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;

import fr.soleil.salsa.client.controller.impl.tool.MenuBarControllerTool;
import fr.soleil.salsa.client.tool.Perspective;
import fr.soleil.salsa.client.tool.PerspectiveEventAdd;
import fr.soleil.salsa.client.tool.PerspectiveEventRemove;
import fr.soleil.salsa.client.tool.PerspectiveEventSelect;
import fr.soleil.salsa.client.util.IconsEnergy;
import fr.soleil.salsa.client.util.IconsScan;
import fr.soleil.salsa.client.util.IconsScanK;
import fr.soleil.salsa.client.util.IconsTrajectories;
import fr.soleil.salsa.client.view.AboutDialog;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.client.view.util.IconsHistoric;
import fr.soleil.salsa.client.view.util.IconsHooks;
import fr.soleil.salsa.client.view.util.IconsStrategies;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.tool.EventBusHandler;

/**
 * The menu bar is part of the main window, so it is displayed by the
 * application view. In order to make the code easier to maintain, the code
 * pertaining to the menu bar is isolated in this class. It is NOT an
 * independent view, but a part of the application view.
 */
public class MenuBarViewTool extends JMenuBar {
    /** Serialization id */
    private static final long serialVersionUID = -7385575877154950295L;

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(MenuBarViewTool.class);

    private static final String TEXT_SCAN_MANAGER = "Scan Manager";
    private static final String TEXT_SCAN_RESULT = "Scan Result";
    private static final String TEXT_SCAN_HISTORIC = "Scan Historic";
    private static final String TEXT_GENERAL = "General";
    private static final String TEXT_SENSORS = "Sensors";
    private static final String TEXT_ACTUATORS_X = "Actuators X";
    private static final String TEXT_ACTUATORS_Y = "Actuators Y";
    private static final String TEXT_TRAJECTORY_X = "Trajectory X";
    private static final String TEXT_TRAJECTORY_Y = "Trajectory Y";
    private static final String TEXT_TIMEBASE = "Timebase";
    private static final String TEXT_HOOKS = "Hooks";
    private static final String TEXT_ERROR_SRATEGIES = "Error Strategies";
    private static final String TEXT_SCAN_K = "Scan K";
    private static final String TEXT_SCAN_ENERGY = "Scan Energy";
    private static final String TEXT_DISPLAY_MANAGER = "Display Manager";
    private static final String TEXT_SCAN_FUNCTIONS = "Scan Functions";
    private static final String TEXT_HISTORIC_LOG = "Historic Log";
    private static final String TEXT_DATA_RECORDER = "Data Recorder";
    private static final String TEXT_DATA_FITTER = "Data Fitter";
    private static final String TEXT_BOOKMARKS = "Bookmarks";
    private static final String TEXT_CONFIGURATION_MANAGER = "Configuration Manager";
    private static final String TEXT_RECORDING_CONTROL = "Recording Control";
    private static final String TEXT_RECORDING_PROJECT = "Recording Project";
    private static final String TEXT_RECORDING_COUNTERS = "Recording Counters";
    private static final String TEXT_RECORDING_EXTENSION = "Recording Extension";

    /** scan result perspective if true */
    private final boolean scanResult;

    /** The controller tool. @see MenuBarControllerTool */
    private transient MenuBarControllerTool controllerTool;

    /** About dialog */
    private AboutDialog about;

    private JMenu fileMenu;
    private JMenu editMenu;
    private JMenu windowMenu;
    private JMenu showViewMenu;
    private JMenu resultViewsMenu;
    private JMenu configurationViewsMenu;
    private JMenu perspectiveMenu;
    private JMenu menuHelp;

    private JMenuItem saveScanConfigMenuItem;
    private JMenuItem new1DMenuItem;
    private JMenuItem new2DMenuItem;
    private JMenuItem newDirectoryDMenuItem;
    private JMenuItem newScanEnergyMenuItem;
    private JMenuItem newKMenuItem;
    private JMenuItem newScanHCSMenuItem;
    private JMenuItem exitMenuItem;
    private JMenuItem cutMenuItem;
    private JMenuItem copyMenuItem;
    private JMenuItem pasteJMenuItem;
    private JMenuItem preferencesMenuItem;
    private JMenuItem lockUnlockLayoutMenuItem;
    private JMenuItem resetLayoutMenuItem;
    private JMenuItem configTreeMenuItem;
    private JMenuItem scanResultMenuItem;
    private JMenuItem historicMenuItem;
    private JMenuItem generalMenuItem;
    private JMenuItem sensorsMenuItem;
    private JMenuItem actuatorXMenuItem;
    private JMenuItem trajectoryXMenuItem;
    private JMenuItem actuatorYMenuItem;
    private JMenuItem trajectoryYMenuItem;
    private JMenuItem timebaseMenuItem;
    private JMenuItem hooksMenuItem;
    private JMenuItem errorStrategyMenuItem;
    private JMenuItem scanKMenuItem;
    private JMenuItem scanEnergyMenuItem;
    private JMenuItem configurationManagerMenuItem;
    private JMenuItem displayManagerMenuItem;
    private JMenuItem scanFunctionsMenuItem;
    private JMenuItem historicLogMenuItem;
    private JMenuItem aboutMenuItem;
    private JMenuItem bookmarksMenuItem;
    private JMenuItem dataFitterMenuItem;
    private JMenuItem dataRecorderMenuItem;
    private JMenuItem recordingControlMenuItem;
    private JMenuItem recordingProjectMenuItem;
    private JMenuItem recordingCountersMenuItem;
    private JMenuItem recordingExtensionMenuItem;

    private JMenuItem newPerspectiveMenuItem;
    private JMenuItem removePerspectiveMenuItem;
    private JMenuItem renamePerspectiveMenuItem;

    private ButtonGroup perspectiveButtonGroup;

    private transient DevicePreferences devicePreferences;

    /** True if layout is locked */
    private boolean isLayoutLocked;

    /**
     * Create a new instance
     */
    public MenuBarViewTool() {
        this(null, false);
    }

    /**
     * Create a new instance
     * 
     * @param ascanResult enable scan result if true
     * @param useRecordingManager Use recording manager instead of datarecorder if true
     */
    public MenuBarViewTool(boolean ascanResult) {
        this(null, ascanResult);
    }

    /**
     * Create a new instance
     * 
     * @param controllerTool the control tool controlling this view tool.
     * @param ascanResult enable scan result if true
     */
    public MenuBarViewTool(MenuBarControllerTool controllerTool, boolean ascanResult) {
        super();
        this.scanResult = ascanResult;
        this.controllerTool = controllerTool;
        this.initialize();
        EventBusHandler.getEventBus().register(this);
    }

    /***
     * Returns menu bar controller tool.
     */
    public Optional<MenuBarControllerTool> getControllerTool() {
        return Optional.ofNullable(controllerTool);
    }

    /**
     * Set controller for menu
     * 
     * @param controllerTool the controller for menu
     */
    public void setControllerTool(MenuBarControllerTool controllerTool) {
        this.controllerTool = controllerTool;
    }

    /**
     * Get device preferences
     * 
     * @return device preferences
     */
    public DevicePreferences getDevicePreferences() {
        return devicePreferences;
    }

    /**
     * Set device preferences
     * 
     * @param devicePreferences the device preference
     */
    public void setDevicePreferences(DevicePreferences devicePreferences) {
        this.devicePreferences = devicePreferences;
        stateChanged(new DevicePreferencesEvent(devicePreferences, DevicePreferencesEvent.Type.LOADING));
    }

    /**
     * Enable config 1D menu
     */
    public void enableConfig1DMenuWindow() {
        getActuatorYMenuItem().setVisible(false);
        getTrajectoryYmMenuItem().setVisible(false);
        getScanKMenuItem().setVisible(false);
        getScanEnergyMenuItem().setVisible(false);
        getTrajectoryXMenuItem().setVisible(true);
        getTimebaseMenuItem().setVisible(true);
    }

    /**
     * Enable config 2D menu
     */
    public void enableConfig2DMenuWindow() {
        getScanEnergyMenuItem().setVisible(false);
        getScanKMenuItem().setVisible(false);
        getTrajectoryXMenuItem().setVisible(true);
        getTrajectoryYmMenuItem().setVisible(true);
        getActuatorYMenuItem().setVisible(true);
        getTimebaseMenuItem().setVisible(true);
    }

    /**
     * Enable config HCS menu
     */
    public void enableConfigHCSMenuWindow() {
        getActuatorYMenuItem().setVisible(false);
        getTrajectoryYmMenuItem().setVisible(false);
        getTimebaseMenuItem().setVisible(true);
        getScanEnergyMenuItem().setVisible(false);
        getScanKMenuItem().setVisible(false);
        getTrajectoryXMenuItem().setVisible(true);
    }

    /**
     * Enable config K menu
     */
    public void enableConfigKMenuWindow() {
        getTrajectoryXMenuItem().setVisible(false);
        getActuatorYMenuItem().setVisible(false);
        getTrajectoryYmMenuItem().setVisible(false);
        getTimebaseMenuItem().setVisible(true);
        getScanEnergyMenuItem().setVisible(false);
        getScanKMenuItem().setVisible(true);
    }

    /**
     * Enable config Energy menu
     */
    public void enableConfigEnergyMenuWindow() {
        getTrajectoryXMenuItem().setVisible(false);
        getActuatorYMenuItem().setVisible(false);
        getTrajectoryYmMenuItem().setVisible(false);
        getTimebaseMenuItem().setVisible(true);
        getScanKMenuItem().setVisible(false);
        getScanEnergyMenuItem().setVisible(true);
    }

    /**
     * Show recording manager and hide data recorder in menu if true
     * 
     * @param isRecordingManagerEnabled enable recording manager if true
     */
    public void enableRecordingManager(boolean isRecordingManagerEnabled) {
        getRecordingControlMenuItem().setVisible(isRecordingManagerEnabled);
        getRecordingProjectMenuItem().setVisible(isRecordingManagerEnabled);
        getRecordingCountersMenuItem().setVisible(isRecordingManagerEnabled);
        getRecordingExtensionMenuItem().setVisible(isRecordingManagerEnabled);
        getDataRecorderMenuItem().setVisible(!isRecordingManagerEnabled);
    }

    /**
     * Initialization.
     */
    private void initialize() {
        add(getFileMenu());
        add(getEditMenu());
        add(getWindowMenu());
        if (!scanResult) {
            add(getPerspectiveMenu());
        }
        add(getHelpMenu());
    }

    private JMenu getFileMenu() {
        if (fileMenu == null) {
            fileMenu = new JMenu("File");
            fileMenu.add(getSaveScanConfigMenuItem());
            if (!scanResult) {
                fileMenu.addSeparator();
                fileMenu.add(getNew1DMenuItem());
                fileMenu.add(getNew2DItem());
                fileMenu.add(getNewDirectoryDMenuItem());
                fileMenu.add(getNewScanEnergyMenuItem());
                fileMenu.add(getNewKMenuItem());
                fileMenu.add(getNewScanHCSMenuItem());
                fileMenu.addSeparator();
            }
            fileMenu.add(getExitMenuItem());
        }
        return fileMenu;
    }

    private JMenu getEditMenu() {
        if (editMenu == null) {
            editMenu = new JMenu("Edit");
            if (!scanResult) {
                editMenu.add(getCutMenuItem());
                editMenu.add(getCopyMenuItem());
                editMenu.add(getPasteMenuItem());
                editMenu.addSeparator();
            }
            editMenu.add(getPreferencesMenuItem());
        }
        return editMenu;
    }

    private JMenu getWindowMenu() {
        if (windowMenu == null) {
            windowMenu = new JMenu("Window");
            windowMenu.add(getLockUnlockLayoutMenuItem());
            if (!scanResult) {
                windowMenu.add(getResetLayoutMenuItem());
            }
            windowMenu.add(getShowViewMenu());
        }
        return windowMenu;
    }

    private JMenu getShowViewMenu() {
        if (showViewMenu == null) {
            showViewMenu = new JMenu("Show View");
            JMenu subMenu = showViewMenu;
            if (!scanResult) {
                subMenu = getResultViewsMenu();
                showViewMenu.add(subMenu);
                showViewMenu.add(getConfigurationViewsMenu());
            }
            subMenu.add(getScanResultMenuItem());
            subMenu.add(getHistoricMenuItem());
            subMenu.add(getDisplayManagerMenuItem());
            subMenu.add(getScanFunctionsMenuItem());
            subMenu.add(getHistoricLogMenuItem());
            subMenu.add(getDataFitterMenuItem());
            subMenu.add(getDataRecorderMenuItem());
            subMenu.add(getRecordingControlMenuItem());
            if (!scanResult) {
                subMenu.add(getRecordingProjectMenuItem());
                subMenu.add(getRecordingCountersMenuItem());
                subMenu.add(getRecordingExtensionMenuItem());
            }
        }
        return showViewMenu;
    }

    private JMenu getResultViewsMenu() {
        if (resultViewsMenu == null) {
            resultViewsMenu = new JMenu("Result Views");
        }
        return resultViewsMenu;
    }

    private JMenu getConfigurationViewsMenu() {
        if (configurationViewsMenu == null) {
            configurationViewsMenu = new JMenu("Configuration Views");
            configurationViewsMenu.add(getConfigTreeMenuItem());
            configurationViewsMenu.add(getGeneralMenuItem());
            configurationViewsMenu.add(getSensorsMenuItem());
            configurationViewsMenu.add(getActuatorXMenuItem());
            configurationViewsMenu.add(getTrajectoryXMenuItem());
            configurationViewsMenu.add(getActuatorYMenuItem());
            configurationViewsMenu.add(getTrajectoryYmMenuItem());
            configurationViewsMenu.add(getTimebaseMenuItem());
            configurationViewsMenu.add(getHooksMenuItem());
            configurationViewsMenu.add(getErrorStrategyMenuItem());
            configurationViewsMenu.add(getScanKMenuItem());
            configurationViewsMenu.add(getScanEnergyMenuItem());
            configurationViewsMenu.add(getConfigurationManagerMenuItem());
            configurationViewsMenu.add(getBookmarksMenuItem());

        }
        return configurationViewsMenu;
    }

    private JMenu getPerspectiveMenu() {
        if (perspectiveMenu == null) {
            perspectiveMenu = new JMenu("Perspective");
            perspectiveMenu.addSeparator();
            perspectiveMenu.add(getNewPerpectiveMenuItem());
            perspectiveMenu.add(getRenamePerspectiveMenuItem());
            perspectiveMenu.add(getRemovePerspectiveMenuItem());
        }
        return perspectiveMenu;
    }

    @Override
    public JMenu getHelpMenu() {
        if (menuHelp == null) {
            menuHelp = new JMenu("Help");
            menuHelp.add(getAboutMenuItem());
        }
        return menuHelp;
    }

    private JMenuItem getSaveScanConfigMenuItem() {
        if (saveScanConfigMenuItem == null) {
            saveScanConfigMenuItem = new JMenuItem("Save", Icons.getIcon("salsa.scanconfig.save"));
            saveScanConfigMenuItem.setToolTipText(getSaveScanConfigToolTipText());
            saveScanConfigMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
            saveScanConfigMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifySavingAction));
        }
        return saveScanConfigMenuItem;
    }

    private String getSaveScanConfigToolTipText() {
        String toolType = "Save user settings";
        if (!scanResult) {
            toolType = toolType + " and scan configurations";
        }
        return toolType;
    }

    private JMenuItem getNew1DMenuItem() {
        if (new1DMenuItem == null) {
            new1DMenuItem = new JMenuItem("New 1D", Icons.getIcon("salsa.scanconfig.1d.big"));
            new1DMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
            new1DMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewScan1d));
        }
        return new1DMenuItem;
    }

    private JMenuItem getNew2DItem() {
        if (new2DMenuItem == null) {
            new2DMenuItem = new JMenuItem("New 2D", Icons.getIcon("salsa.scanconfig.2d.big"));
            new2DMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
            new2DMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewScan2d));
        }
        return new2DMenuItem;
    }

    private JMenuItem getNewDirectoryDMenuItem() {
        if (newDirectoryDMenuItem == null) {
            newDirectoryDMenuItem = new JMenuItem("New Directory", Icons.getIcon("salsa.scanconfig.folder-new.big"));
            newDirectoryDMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
            newDirectoryDMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewDirectory));
        }
        return newDirectoryDMenuItem;
    }

    private JMenuItem getNewScanEnergyMenuItem() {
        if (newScanEnergyMenuItem == null) {
            newScanEnergyMenuItem = new JMenuItem("New Scan Energy", Icons.getIcon("salsa.scanconfig.energy.big"));
            newScanEnergyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
            newScanEnergyMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewScanEnergy));
        }
        return newScanEnergyMenuItem;
    }

    private JMenuItem getNewKMenuItem() {
        if (newKMenuItem == null) {
            newKMenuItem = new JMenuItem("New Scan K", Icons.getIcon("salsa.scanconfig.k.big"));
            newKMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewScanK));
        }
        return newKMenuItem;
    }

    private JMenuItem getNewScanHCSMenuItem() {
        if (newScanHCSMenuItem == null) {
            newScanHCSMenuItem = new JMenuItem("New Scan HCS", Icons.getIcon("salsa.scanconfig.hcs.big"));
            newScanHCSMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewScanHCS));
        }
        return newScanHCSMenuItem;
    }

    private JMenuItem getExitMenuItem() {
        if (exitMenuItem == null) {
            exitMenuItem = new JMenuItem("Exit", Icons.getIcon("salsa.quit"));
            exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
            exitMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyExitAction));
        }
        return exitMenuItem;
    }

    private JMenuItem getCutMenuItem() {
        if (cutMenuItem == null) {
            cutMenuItem = new JMenuItem("Cut", Icons.getIcon("salsa.cut"));
            cutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
            cutMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyCutAction));
        }
        return cutMenuItem;
    }

    private JMenuItem getCopyMenuItem() {
        if (copyMenuItem == null) {
            copyMenuItem = new JMenuItem("Copy", Icons.getIcon("salsa.copy"));
            copyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
            copyMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyCopyAction));
        }
        return copyMenuItem;
    }

    private JMenuItem getPasteMenuItem() {
        if (pasteJMenuItem == null) {
            pasteJMenuItem = new JMenuItem("Paste", Icons.getIcon("salsa.paste"));
            pasteJMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
            pasteJMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyPasteAction));
        }
        return pasteJMenuItem;
    }

    private JMenuItem getPreferencesMenuItem() {
        if (preferencesMenuItem == null) {
            preferencesMenuItem = new JMenuItem("Preferences", Icons.getIcon("salsa.preferences"));
            preferencesMenuItem.setToolTipText("Show Preferences");
            preferencesMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyPreferencesView));
        }
        return preferencesMenuItem;
    }

    private JMenuItem getLockUnlockLayoutMenuItem() {
        if (lockUnlockLayoutMenuItem == null) {
            lockUnlockLayoutMenuItem = new JMenuItem("Lock Layout", Icons.getIcon("salsa.layout.lock"));
            lockUnlockLayoutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
            lockUnlockLayoutMenuItem.addActionListener(e -> notifyLockUnlock());
        }
        return lockUnlockLayoutMenuItem;
    }

    private JMenuItem getResetLayoutMenuItem() {
        if (resetLayoutMenuItem == null) {
            resetLayoutMenuItem = new JMenuItem("Reset Layout...", Icons.getIcon("salsa.layout.reset"));
            resetLayoutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
            resetLayoutMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyResetLayout));
        }
        return resetLayoutMenuItem;
    }

    private JMenuItem getConfigTreeMenuItem() {
        if (configTreeMenuItem == null) {
            configTreeMenuItem = new JMenuItem(TEXT_SCAN_MANAGER, Icons.getIcon("salsa.scanmanagement.tree"));
            configTreeMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyConfigTreeView));
        }
        return configTreeMenuItem;
    }

    private JMenuItem getScanResultMenuItem() {
        if (scanResultMenuItem == null) {
            scanResultMenuItem = new JMenuItem(TEXT_SCAN_RESULT, IconsScan.getIconScan("salsa.scanserver.graph"));
            scanResultMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyScanResultView));
        }
        return scanResultMenuItem;
    }

    private JMenuItem getHistoricMenuItem() {
        if (historicMenuItem == null) {
            historicMenuItem = new JMenuItem(TEXT_SCAN_HISTORIC,
                    IconsHistoric.getIconHistoric("salsa.scanserver.history"));
            historicMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyHistoricView));
        }
        return historicMenuItem;
    }

    private JMenuItem getGeneralMenuItem() {
        if (generalMenuItem == null) {
            generalMenuItem = new JMenuItem(TEXT_GENERAL, Icons.getIcon("salsa.scanconfig.general"));
            generalMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyGeneralView));
        }
        return generalMenuItem;
    }

    private JMenuItem getSensorsMenuItem() {
        if (sensorsMenuItem == null) {
            sensorsMenuItem = new JMenuItem(TEXT_SENSORS, Icons.getIcon("salsa.scanconfig.sensor"));
            sensorsMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifySensorView));
        }
        return sensorsMenuItem;
    }

    private JMenuItem getActuatorXMenuItem() {
        if (actuatorXMenuItem == null) {
            actuatorXMenuItem = new JMenuItem(TEXT_ACTUATORS_X, Icons.getIcon("salsa.scanconfig.actuatorx"));
            actuatorXMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyActuatorView));
        }
        return actuatorXMenuItem;
    }

    private JMenuItem getTrajectoryXMenuItem() {
        if (trajectoryXMenuItem == null) {
            trajectoryXMenuItem = new JMenuItem(TEXT_TRAJECTORY_X,
                    IconsTrajectories.getIconTrajectory("salsa.scanconfig.trajectoryx"));
            trajectoryXMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyXTrajectoriesView));
        }
        return trajectoryXMenuItem;
    }

    private JMenuItem getActuatorYMenuItem() {
        if (actuatorYMenuItem == null) {
            actuatorYMenuItem = new JMenuItem(TEXT_ACTUATORS_Y, Icons.getIcon("salsa.scanconfig.actuatory"));
            actuatorYMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyYActuatorView));
        }
        return actuatorYMenuItem;
    }

    private JMenuItem getTrajectoryYmMenuItem() {
        if (trajectoryYMenuItem == null) {
            trajectoryYMenuItem = new JMenuItem(TEXT_TRAJECTORY_Y,
                    IconsTrajectories.getIconTrajectory("salsa.scanconfig.trajectoryy"));
            trajectoryYMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyYTrajectories2DView));
        }
        return trajectoryYMenuItem;
    }

    private JMenuItem getTimebaseMenuItem() {
        if (timebaseMenuItem == null) {
            timebaseMenuItem = new JMenuItem(TEXT_TIMEBASE, Icons.getIcon("salsa.scanconfig.timebase"));
            timebaseMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyTimebasesView));
        }
        return timebaseMenuItem;
    }

    private JMenuItem getHooksMenuItem() {
        if (hooksMenuItem == null) {
            hooksMenuItem = new JMenuItem(TEXT_HOOKS, IconsHooks.getIconHook("salsa.scanconfig.hook"));
            hooksMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyHooksView));
        }
        return hooksMenuItem;
    }

    private JMenuItem getErrorStrategyMenuItem() {
        if (errorStrategyMenuItem == null) {
            errorStrategyMenuItem = new JMenuItem(TEXT_ERROR_SRATEGIES,
                    IconsStrategies.getIconStrategy("salsa.scanconfig.errorstrategy"));
            errorStrategyMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyErrorStrategiesView));
        }
        return errorStrategyMenuItem;
    }

    private JMenuItem getScanKMenuItem() {
        if (scanKMenuItem == null) {
            scanKMenuItem = new JMenuItem(TEXT_SCAN_K, IconsScanK.getIconScanK("salsa.scanconfig.k.small"));
            scanKMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyScanKView));
        }
        return scanKMenuItem;
    }

    private JMenuItem getScanEnergyMenuItem() {
        if (scanEnergyMenuItem == null) {
            scanEnergyMenuItem = new JMenuItem(TEXT_SCAN_ENERGY,
                    IconsEnergy.getIconEnergy("salsa.scanconfig.energy.small"));
            scanEnergyMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyScanEnergyView));
        }
        return scanEnergyMenuItem;
    }

    private JMenuItem getConfigurationManagerMenuItem() {
        if (configurationManagerMenuItem == null) {
            configurationManagerMenuItem = new JMenuItem(TEXT_CONFIGURATION_MANAGER,
                    Icons.getIcon("salsa.configuration_manager"));
            configurationManagerMenuItem
                    .addActionListener(e -> notifyView(MenuBarControllerTool::notifyConfigurationManagerView));
        }
        return configurationManagerMenuItem;
    }

    private JMenuItem getDisplayManagerMenuItem() {
        if (displayManagerMenuItem == null) {
            displayManagerMenuItem = new JMenuItem(TEXT_DISPLAY_MANAGER, Icons.getIcon("salsa.display_manager"));
            displayManagerMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyDisplayManagerView));
        }
        return displayManagerMenuItem;
    }

    private JMenuItem getScanFunctionsMenuItem() {
        if (scanFunctionsMenuItem == null) {
            scanFunctionsMenuItem = new JMenuItem(TEXT_SCAN_FUNCTIONS, Icons.getIcon("salsa.scan_functions"));
            scanFunctionsMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyScanFuntionsView));
        }
        return scanFunctionsMenuItem;
    }

    private JMenuItem getHistoricLogMenuItem() {
        if (historicLogMenuItem == null) {
            historicLogMenuItem = new JMenuItem(TEXT_HISTORIC_LOG, Icons.getIcon("salsa.historic_log"));
            historicLogMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyHistoricLogView));
        }
        return historicLogMenuItem;
    }

    private JMenuItem getDataRecorderMenuItem() {
        if (dataRecorderMenuItem == null) {
            dataRecorderMenuItem = new JMenuItem(TEXT_DATA_RECORDER, Icons.getIcon("salsa.recording.control"));
            dataRecorderMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyRecordingView));
        }
        return dataRecorderMenuItem;
    }

    private JMenuItem getRecordingControlMenuItem() {
        if (recordingControlMenuItem == null) {
            recordingControlMenuItem = new JMenuItem(TEXT_RECORDING_CONTROL, Icons.getIcon("salsa.recording.control"));
            recordingControlMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyRecordingView));
        }
        return recordingControlMenuItem;
    }

    private JMenuItem getRecordingProjectMenuItem() {
        if (recordingProjectMenuItem == null) {
            recordingProjectMenuItem = new JMenuItem(TEXT_RECORDING_PROJECT, Icons.getIcon("salsa.recording.project"));
            recordingProjectMenuItem
                    .addActionListener(e -> notifyView(MenuBarControllerTool::notifyRecordingProjectView));
        }
        return recordingProjectMenuItem;
    }

    private JMenuItem getRecordingCountersMenuItem() {
        if (recordingCountersMenuItem == null) {
            recordingCountersMenuItem = new JMenuItem(TEXT_RECORDING_COUNTERS,
                    Icons.getIcon("salsa.recording.counters"));
            recordingCountersMenuItem
                    .addActionListener(e -> notifyView(MenuBarControllerTool::notifyRecordingCountersControlView));
        }
        return recordingCountersMenuItem;
    }

    private JMenuItem getRecordingExtensionMenuItem() {
        if (recordingExtensionMenuItem == null) {
            recordingExtensionMenuItem = new JMenuItem(TEXT_RECORDING_EXTENSION,
                    Icons.getIcon("salsa.recording.extension"));
            recordingExtensionMenuItem
                    .addActionListener(e -> notifyView(MenuBarControllerTool::notifyRecordingExtensionView));
        }
        return recordingExtensionMenuItem;
    }

    private JMenuItem getAboutMenuItem() {
        if (aboutMenuItem == null) {
            aboutMenuItem = new JMenuItem("About...");
            aboutMenuItem.addActionListener(e -> showAboutDialog());
        }
        return aboutMenuItem;
    }

    private void showAboutDialog() {
        if ((controllerTool != null) && (controllerTool.getApplicationController() != null)) {
            getAboutDialog().setLocationRelativeTo(controllerTool.getApplicationController().getView());
        }
        getAboutDialog().setVisible(true);
    }

    private AboutDialog getAboutDialog() {
        if (about == null) {
            about = new AboutDialog(controllerTool.getApplicationController().getView());
        }
        return about;
    }

    private JMenuItem getNewPerpectiveMenuItem() {
        if (newPerspectiveMenuItem == null) {
            newPerspectiveMenuItem = new JMenuItem("New...");
            newPerspectiveMenuItem.setToolTipText("New Perspective");
            newPerspectiveMenuItem.setAccelerator(
                    KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
            newPerspectiveMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyNewPerspectiveView));
        }
        return newPerspectiveMenuItem;
    }

    private JMenuItem getRenamePerspectiveMenuItem() {
        if (renamePerspectiveMenuItem == null) {
            renamePerspectiveMenuItem = new JMenuItem("Rename...");
            renamePerspectiveMenuItem.setToolTipText("Rename Perspective");
            renamePerspectiveMenuItem
                    .addActionListener(e -> notifyView(MenuBarControllerTool::notifyRenamePerspectiveView));
        }
        return renamePerspectiveMenuItem;
    }

    private JMenuItem getRemovePerspectiveMenuItem() {
        if (removePerspectiveMenuItem == null) {
            removePerspectiveMenuItem = new JMenuItem("Remove...");
            removePerspectiveMenuItem.setToolTipText("Remove Perspective");
            removePerspectiveMenuItem.setAccelerator(
                    KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
            removePerspectiveMenuItem
                    .addActionListener(e -> notifyView(MenuBarControllerTool::notifyRemovePerspectiveView));
        }
        return removePerspectiveMenuItem;
    }

    private JMenuItem getBookmarksMenuItem() {
        if (bookmarksMenuItem == null) {
            bookmarksMenuItem = new JMenuItem(TEXT_BOOKMARKS, Icons.getIcon("salsa.scantree.bookmarks"));
            bookmarksMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyBookmarksView));
        }
        return bookmarksMenuItem;
    }

    private JMenuItem getDataFitterMenuItem() {
        if (dataFitterMenuItem == null) {
            dataFitterMenuItem = new JMenuItem(TEXT_DATA_FITTER, Icons.getIcon("salsa.datafitter"));
            dataFitterMenuItem.addActionListener(e -> notifyView(MenuBarControllerTool::notifyDataFitterView));
        }
        return dataFitterMenuItem;
    }

    private void notifyView(Consumer<? super MenuBarControllerTool> viewNotifier) {
        getControllerTool().ifPresent(viewNotifier);
    }

    private void notifyLockUnlock() {
        if (isLayoutLocked) {
            notifyView(MenuBarControllerTool::notifyUnlockLayout);
        } else {
            notifyView(MenuBarControllerTool::notifyLockLayout);
        }
    }

    /**
     * Invoked when a device preference event occurs in the event bus
     * 
     * @param devicePreferencesEvent the device preferences events
     */
    @Subscribe
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        switch (devicePreferencesEvent.getType()) {
            case RECORDINGMANAGER_CHANGED:
            case LOADING:
                enableRecordingManager(devicePreferencesEvent.getSource().isRecordingManagerDefined());
                break;
            default:
                break;
        }
    }

    /**
     * Invoked when a docking window locker event occurs in the event bus
     * 
     * @param dockingWindowLockerEvent the docking window locker event
     */
    @Subscribe
    public void dockingWindowLockerChanged(DockingWindowLockerEvent dockingWindowLockerEvent) {
        Preconditions.checkNotNull(dockingWindowLockerEvent, "The docking window locker event cannot be null");
        Preconditions.checkNotNull(dockingWindowLockerEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Receive docking window locker event: {}", dockingWindowLockerEvent);
        isLayoutLocked = dockingWindowLockerEvent.isLocked();
        if (isLayoutLocked) {
            getLockUnlockLayoutMenuItem().setText("Unlock Layout");
            getLockUnlockLayoutMenuItem().setIcon(Icons.getIcon("salsa.layout.unlock"));
        } else {
            getLockUnlockLayoutMenuItem().setText("Lock Layout");
            getLockUnlockLayoutMenuItem().setIcon(Icons.getIcon("salsa.layout.lock"));
        }
    }

    /**
     * Invoked when an add perspective event occurs in the event bus
     * 
     * @param perspectiveEventAdd the perspective event
     */
    @Subscribe
    public void addPerspective(PerspectiveEventAdd perspectiveEventAdd) {
        Preconditions.checkNotNull(perspectiveEventAdd, "The add perspective event cannot be null");
        Preconditions.checkNotNull(perspectiveEventAdd.getSource(),
                "The source of add perspective event cannot be null");

        LOGGER.debug("Receive add perspective event: {}", perspectiveEventAdd);
        Perspective perspective = perspectiveEventAdd.getPerspective();
        Preconditions.checkNotNull(perspectiveEventAdd, "The perspective to add cannot be null");

        String name = perspective.getName();
        Preconditions.checkNotNull(name, "The name of perspective to add cannot be null");
        Preconditions.checkState(!name.trim().isEmpty(), "The name of perspective to add cannot be empty");

        LOGGER.debug("Add the perspective \"{}\" to the perspective menu", name);
        JRadioButtonMenuItem perspectiveItem = new JRadioButtonMenuItem(name);
        perspectiveItem.setToolTipText("Select " + name + " Perspective");
        perspectiveItem.addActionListener(l -> getControllerTool().ifPresent(t -> t.notifySelectPerspective(name)));
        getPerspectiveMenu().add(perspectiveItem, getLastPerspectiveMenuItemPosition());
        getPerspectivesButtonGroup().add(perspectiveItem);
    }

    /**
     * Invoked when a select perspective event occurs in the event bus
     * 
     * @param perspectiveEventSelect the perspective event
     */
    @Subscribe
    public void selectPerspective(PerspectiveEventSelect perspectiveEventSelect) {
        Preconditions.checkNotNull(perspectiveEventSelect, "The select perspective event cannot be null");
        Preconditions.checkNotNull(perspectiveEventSelect.getSource(),
                "The source of select perspective event cannot be null");

        LOGGER.debug("Receive select perspective event: {}", perspectiveEventSelect);
        Perspective perspective = perspectiveEventSelect.getPerspective();
        Preconditions.checkNotNull(perspective, "The perspective to select cannot be null");

        String name = perspective.getName();
        Preconditions.checkNotNull(name, "The name of perspective to select cannot be null");
        Preconditions.checkState(!name.trim().isEmpty(), "The name of perspective to select cannot be empty");

        // Select perspective in menu only if present and is not yet selected
        getPerspectiveMenuItem(name).ifPresent(i -> i.setSelected(!i.isSelected()));
        getRenamePerspectiveMenuItem().setEnabled(!perspectiveEventSelect.isDefaultPerspective());
        getRemovePerspectiveMenuItem().setEnabled(!perspectiveEventSelect.isDefaultPerspective());
    }

    /**
     * Invoked when a remove perspective event occurs in the event bus
     * 
     * @param perspectiveEventRemove the perspective event
     */
    @Subscribe
    public void removePerspective(PerspectiveEventRemove perspectiveEventRemove) {
        Preconditions.checkNotNull(perspectiveEventRemove, "The remove perspective event cannot be null");
        Preconditions.checkNotNull(perspectiveEventRemove.getSource(),
                "The source of remove perspective event cannot be null");

        LOGGER.debug("Receive remove perspective event: {}", perspectiveEventRemove);
        Perspective perspective = perspectiveEventRemove.getPerspective();
        Preconditions.checkNotNull(perspectiveEventRemove, "The perspective to remove cannot be null");

        String name = perspective.getName();
        Preconditions.checkNotNull(name, "The name of perspective to remove cannot be null");
        Preconditions.checkState(!name.trim().isEmpty(), "The name of perspective to remove cannot be empty");

        LOGGER.debug("Remove the perspective \"{}\" in the perspective menu", name);
        getPerspectiveMenuItem(name).ifPresent(i -> getPerspectiveMenu().remove(i));
    }

    private Optional<JMenuItem> getPerspectiveMenuItem(String name) {
        return getPerspectiveMenuItems().stream().filter(m -> m.getText().equals(name)).findFirst();
    }

    private List<JMenuItem> getPerspectiveMenuItems() {
        List<JMenuItem> perspectiveMenuItems = new ArrayList<>();
        for (int i = 0; i < getLastPerspectiveMenuItemPosition(); i++) {
            perspectiveMenuItems.add(getPerspectiveMenu().getItem(i));
        }
        return perspectiveMenuItems;
    }

    private int getLastPerspectiveMenuItemPosition() {
        return getPerspectiveMenu().getItemCount() - 4;
    }

    private ButtonGroup getPerspectivesButtonGroup() {
        if (perspectiveButtonGroup == null) {
            perspectiveButtonGroup = new ButtonGroup();
        }
        return perspectiveButtonGroup;
    }

}

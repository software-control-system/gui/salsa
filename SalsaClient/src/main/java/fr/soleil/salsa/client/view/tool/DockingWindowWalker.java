package fr.soleil.salsa.client.view.tool;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.slf4j.Logger;

import fr.soleil.salsa.logging.LoggingUtil;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.util.ViewMap;

/**
 * Allow to walk in docking window tree or view map
 * 
 * @author madela
 */
public class DockingWindowWalker implements Serializable {
    /** Serialization id */
    private static final long serialVersionUID = 3970017698392715167L;

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(DockingWindowWalker.class);

    /**
     * Get docking window for the specified vew map
     * 
     * @param viewMap the view map
     * @return Stream of docking window
     */
    public static synchronized Stream<DockingWindow> walk(ViewMap viewMap) {
        Set<DockingWindow> dockingWindows = new LinkedHashSet<>();
        for (int i = 0; i < viewMap.getViewCount(); i++) {
            dockingWindows.add(viewMap.getViewAtIndex(i));
        }
        return dockingWindows.stream();
    }

    /**
     * Get child docking window for the specified parent docking window
     * 
     * @param window parent docking window
     * @return Stream of docking windows
     */
    public static synchronized Stream<DockingWindow> walk(DockingWindow window) {
        Set<DockingWindow> dockingWindows = new LinkedHashSet<>();
        walkDockingWindows(window, dockingWindows);
        return dockingWindows.stream();
    }

    private static void walkDockingWindows(DockingWindow window, Set<DockingWindow> dockingWindows) {
        for (int i = 0; i < window.getChildWindowCount(); i++) {
            DockingWindow child = window.getChildWindow(i);
            if (!dockingWindows.contains(child)) {
                walkDockingWindows(child, dockingWindows);
            }
        }
        dockingWindows.add(window);
    }
}

package fr.soleil.salsa.client.controller.impl;

import java.util.Optional;

import org.slf4j.Logger;

import fr.soleil.comete.bean.datastorage.helper.Parameters;
import fr.soleil.comete.bean.recordingmanagercountersbean.RecordingManagerCountersBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * The controller for recording counters bean
 * 
 * @author madela
 */
public class RecordingCountersController extends AbstractRecordingController {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(RecordingCountersController.class);

    /** Recording manager counters bean */
    private RecordingManagerCountersBean recordingManagerCountersBean;

    @Override
    protected Optional<AbstractTangoBox> getBean() {
        if (devicePreferences.isRecordingManagerDefined()) {
            return Optional.of(getRecordingManagerCountersBean());
        }
        return Optional.empty();
    }

    private RecordingManagerCountersBean getRecordingManagerCountersBean() {
        String recordingManager = devicePreferences.getRecordingManager();
        String recordingManagerProfil = devicePreferences.getRecordingManagerProfil();
        Parameters.getInstance().setDevice(Optional.ofNullable(recordingManager));
        Parameters.getInstance().setProfile(Optional.ofNullable(recordingManagerProfil));
        if (recordingManagerCountersBean == null) {
            recordingManagerCountersBean = RecordingManagerCountersBean.instance();
            recordingManagerCountersBean.setTitleVisible(false);
        }
        return recordingManagerCountersBean;
    }

    @Override
    protected Optional<String> getBeanModel() {
        return getRecordingManagerDevice();
    }
}

package fr.soleil.salsa.client.controller.impl;

import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.client.view.ShortcutsView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IConfig.ScanType;

public class ShortcutsController extends AController<ShortcutsView> implements IController<ShortcutsView> {

    /**
     * The application controller.
     */
    private ApplicationController applicationController;

    /**
     * Constructor
     */
    public ShortcutsController(ShortcutsView view) {
        this(view, null);
    }

    /**
     * Constructor
     */
    public ShortcutsController(ApplicationController applicationController) {
        this(null, applicationController);
    }

    /**
     * Constructor
     */
    public ShortcutsController(ShortcutsView view, ApplicationController applicationController) {
        super(view);
        this.applicationController = applicationController;
    }

    @Override
    protected ShortcutsView generateView() {
        return new ShortcutsView(this);
    }

    /**
     * Called to create new 1D config.
     */
    public void notifyNewConfig(ScanType scanType) {
        if (applicationController != null) {
            applicationController.notifyNewScan(scanType);
        }
    }

    /**
     * Called to save selected config.
     */
    public void notifySaveConfig() {
        if (applicationController != null) {
            applicationController.notifySavingAction();
        }
    }

    /**
     * Called to delete selected config.
     */
    public void notifyDeleteConfig() {
        if (applicationController != null) {
            applicationController.notifyDeleteConfig();
        }
    }

    /**
     * Called to save perspectives.
     */
    public void notifySavePerspectives() {
        if (applicationController != null) {
            applicationController.getMenuBarControllerTool().notifySavePerspectivesView();
        }
    }

    /**
     * Called to create new directory.
     */
    public void notifyNewDirectory() {
        if (applicationController != null) {
            applicationController.notifyNewDirectory();
        }
    }

    public void setConfig(IConfig<?> config) {
        if (view != null) {
            view.setDeleteButtonEnable(config != null);
        }

    }

}

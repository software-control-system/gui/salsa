package fr.soleil.salsa.client.tool;

/**
 * Perspective event when action (add, remove, select ...) is done
 * 
 * @author madela
 */
public class PerspectiveEventDone extends PerspectiveEvent {
    /** Serialization id */
    private static final long serialVersionUID = 4275192185161006224L;

    /**
     * Constructor
     * 
     * @param source the source of event
     * @param type the type of event
     * @param perspective the perspective
     * @param isDefaultPerspective True if it is event for a default perspective
     */
    public PerspectiveEventDone(Object source, Perspective perspective, boolean isDefaultPerspective) {
        super(source, PerspectiveEvent.Type.DONE, perspective, isDefaultPerspective);
    }

    @Override
    public String toString() {
        return "PerspectiveEventDone [getType()=" + getType() + ", getPerspective()=" + getPerspective()
                + ", isDefaultPerspective()=" + isDefaultPerspective() + "]";
    }
}

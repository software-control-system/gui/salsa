package fr.soleil.salsa.client.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.soleil.salsa.client.controller.impl.ApplicationController;

public class SendMessageAction extends AbstractAction {

    private static final long serialVersionUID = -613888445275481086L;

    protected ApplicationController controller;

    public SendMessageAction(ApplicationController controller, String name) {
        super(name);
        this.controller = controller;
    }

    public void actionPerformed(ActionEvent e) {
        // controller.notifySendMessage();
    }
}

package fr.soleil.salsa.client.view.preferences;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * 
 * @author Tarek
 * 
 */
public class BeanPreference extends JPanel implements Preference {

    private static final long serialVersionUID = -6452680257666171637L;

    private DevicePreferences devicePreferences;
    private final JTable table;
    private final UiTableModel tableModel;

    public BeanPreference(DevicePreferences modelPreferences) {
        super();
        this.devicePreferences = modelPreferences;
        setLayout(new BorderLayout());
        tableModel = new UiTableModel(modelPreferences);
        table = new JTable(tableModel);
        JScrollPane jsp = new JScrollPane(table);
        add(jsp, BorderLayout.CENTER);
    }

    /**
     * @return Returns the model.
     */
    public DevicePreferences getDevicePreferences() {
        return devicePreferences;
    }

    /**
     * @param model
     *            The model to set.
     */
    public void setPreferences(DevicePreferences devicePreferences) {
        this.devicePreferences = devicePreferences;
        tableModel.setPreferences(devicePreferences);
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public String getIconTitle() {
        return "Bean";
    }

    @Override
    public String getIconUrl() {
        return "icons/plugin.png";
    }

    @Override
    public String getTitle() {
        return "Bean Preferences";
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class UiTableModel extends AbstractTableModel {

        private static final long serialVersionUID = -8041638129379208113L;

        private DevicePreferences devicePreferences;
        public final static String[] COLUMN_NAMES = { "Display", "Your choice" };
        public static final int YOUR_CHOICE = 1;

        private Object[][] values;

        public UiTableModel(DevicePreferences devicePreferences) {
            values = null;
            setPreferences(devicePreferences);
        }

        private void updateValues() {
            values = new Object[][] { { "Enable confirmation", UIPreferences.getInstance().isEnableConfirmation() },
                    { "Data browser", UIPreferences.getInstance().getDataBrowser() },
                    { "Allow display image as spectrums", UIPreferences.getInstance().isShowImageAsSpectrumStack() },
                    { "Allow send values to sensors", UIPreferences.getInstance().isSendSensor() },
                    { "Control Panel", UIPreferences.getInstance().getControlPanel() },
                    { "User LogFile", (devicePreferences == null ? ObjectUtils.EMPTY_STRING
                            : devicePreferences.getUserLogFile()) } };
            fireTableDataChanged();
        }

        @Override
        public boolean isCellEditable(int r, int c) {
            return c == YOUR_CHOICE;
        }

        @Override
        public int getColumnCount() {
            return COLUMN_NAMES.length;
        }

        @Override
        public int getRowCount() {
            return values.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return values[rowIndex][columnIndex];
        }

        @Override
        public void setValueAt(Object aValue, int r, int c) {
            try {
                if ((aValue != null) && (c == 1)) {
                    String name = (String) getValueAt(r, 0);
                    if (name.equalsIgnoreCase("Enable confirmation")) {
                        String stringValue = aValue.toString().toLowerCase();
                        UIPreferences.getInstance()
                                .setEnableConfirmation("true".equals(stringValue) || "yes".equals(stringValue));
                        aValue = UIPreferences.getInstance().isEnableConfirmation();
                    } else if (name.equalsIgnoreCase("Data browser")) {
                        String value = aValue.toString();
                        UIPreferences.getInstance().setDataBrowser(value);
                        aValue = UIPreferences.getInstance().getDataBrowser();
                    } else if (name.equalsIgnoreCase("Allow display image as spectrums")) {
                        String value = aValue.toString();
                        UIPreferences.getInstance().showImageAsSpectrumStack(value);
                        aValue = UIPreferences.getInstance().isShowImageAsSpectrumStack();
                    } else if (name.equalsIgnoreCase("Allow send values to sensors")) {
                        String value = aValue.toString();
                        UIPreferences.getInstance().sendSensor(value);
                        aValue = UIPreferences.getInstance().isSendSensor();
                    } else if (name.equalsIgnoreCase("User LogFile")) {
                        String value = aValue.toString();
                        getDevicePreferences().setUserLogFile(value);
                        aValue = getDevicePreferences().getUserLogFile();
                    } else if (name.equalsIgnoreCase("Control Panel")) {
                        UIPreferences.getInstance().setControlPanel(aValue.toString().trim());
                        aValue = UIPreferences.getInstance().getControlPanel();
                    }
                }
            } finally {
                values[r][c] = aValue;
            }
        }

        @Override
        public String getColumnName(int column) {
            return COLUMN_NAMES[column];
        }

        public DevicePreferences getDevicePreferences() {
            return devicePreferences;
        }

        public void setPreferences(DevicePreferences preferences) {
            this.devicePreferences = preferences;
            updateValues();
        }

    }

}

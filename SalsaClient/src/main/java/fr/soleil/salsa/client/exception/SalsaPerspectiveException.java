package fr.soleil.salsa.client.exception;

import fr.soleil.salsa.exception.SalsaException;

/**
 * An exception that can be thrown in case of problem with perspectives
 * 
 * @author girardot
 */
public class SalsaPerspectiveException extends SalsaException {

    private static final long serialVersionUID = 8432136179444282229L;

    public SalsaPerspectiveException() {
        super();
    }

    public SalsaPerspectiveException(String message) {
        super(message);
    }

    public SalsaPerspectiveException(Throwable cause) {
        super(cause);
    }

    public SalsaPerspectiveException(String message, Throwable cause) {
        super(message, cause);
    }

}

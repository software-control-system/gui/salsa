package fr.soleil.salsa.client.view.tool;

import fr.soleil.salsa.entity.IConfig;

/**
 * Node for a configuration in the configuration tree.
 * 
 * @author Administrateur
 * 
 */
public class ConfigTreeNode extends ATreeNode implements Comparable<ConfigTreeNode> {

    private static final long serialVersionUID = -5375665622498127059L;

    /**
     * The wrapped config.
     */
    private IConfig<?> config;

    /**
     * Constructor.
     * 
     * @param config the wrapped config.
     * @param parentDirectory the directory wrapper containing this wrapper.
     */
    public ConfigTreeNode(IConfig<?> config, DirectoryTreeNode parentDirectory) {
        super(parentDirectory);
        assert config != null;
        this.config = config;
    }

    /**
     * The wrapped config.
     * 
     * @return the config
     */
    public IConfig<?> getConfig() {
        return config;
    }

    /**
     * The wrapped config.
     * 
     * @param config
     */
    public void setConfig(IConfig<?> config) {
        assert config != null;
        this.config = config;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return config.getName() + (config.isModified() ? " *" : "");
    }

    /**
     * @see Comparable#compareTo(Object)
     * @param o
     * @return
     */
    @Override
    public int compareTo(ConfigTreeNode o) {
        // Normally, positionInConfiguration is never null.
        int result;
        Integer thisPos = this.getConfig().getPositionInDirectory();
        Integer thatPos = o.getConfig().getPositionInDirectory();
        if (thisPos != null) {
            if (thatPos != null) {
                result = thisPos.compareTo(thatPos);
            }
            else {
                result = -1;
            }
        }
        else {
            if (thatPos != null) {
                result = 1;
            }
            else {
                result = 0;
            }
        }
        return result;
    }

    /**
     * @see Object#equals(Object)
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (obj == null) {
            return false;
        }
        else if (this.config == null) {
            return false;
        }
        else if (obj instanceof ConfigTreeNode) {
            ConfigTreeNode that = (ConfigTreeNode) obj;
            return this.config.equals(that.config);
        }
        else {
            return false;
        }
    }

    /**
     * @see Object#hashCode()
     */
    public int hashCode() {
        return this.config.hashCode();
    }
}

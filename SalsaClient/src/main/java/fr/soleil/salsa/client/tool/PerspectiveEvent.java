package fr.soleil.salsa.client.tool;

import java.util.EventObject;

import com.google.common.base.Preconditions;

/**
 * Perspective event when adding, removing or selecting a perspective
 * with factory using enum with lamba without switch case
 * 
 * @author madela
 */
public class PerspectiveEvent extends EventObject {
    /** Serialization id */
    private static final long serialVersionUID = 5842112290096981734L;

    /** Perspective of this event */
    private final Perspective perspective;

    /** The type of this event */
    private final Type type;

    /** True if it is default perspective */
    private final boolean isDefaultPerspective;

    /** Types of perspective event **/
    public enum Type {
        /** When adding a perspective */
        ADD(PerspectiveEventAdd::new),
        /** When removing a perspective */
        REMOVE(PerspectiveEventRemove::new),
        /** When selecting a perspective */
        SELECT(PerspectiveEventSelect::new),
        /** When action (add, remove, select ...) is done */
        DONE(PerspectiveEventDone::new);

        /** The constructor */
        private final Constructor constructor;

        Type(Constructor constructor) {
            this.constructor = constructor;
        }

        private Constructor getConstructor() {
            return constructor;
        }
    }

    /** Constructor supplier */
    @FunctionalInterface
    private interface Constructor {
        PerspectiveEvent create(Object source, Perspective perspective, boolean isDefaultPerspective);
    }

    /**
     * Create a new instance for specified type, label and attribute name
     * 
     * @param source the source of event
     * @param type the type of event
     * @param perspective the perspective
     * @param isDefaultPerspective True if it is event for a default perspective
     * @return the perspective event
     */
    public static PerspectiveEvent get(Object source, Type type, Perspective perspective,
            boolean isDefaultPerspective) {
        return type.getConstructor().create(source, perspective, isDefaultPerspective);
    }

    /**
     * Constructor
     * 
     * @param source the source of event
     * @param type the type of event
     * @param perspective the perspective
     * @param isDefaultPerspective True if it is event for a default perspective
     */
    protected PerspectiveEvent(Object source, Type type, Perspective perspective, boolean isDefaultPerspective) {
        super(source);
        Preconditions.checkNotNull(type, "Type of perspective event cannot be null");
        Preconditions.checkNotNull(perspective, "Perspective of perspective event cannot be null");
        this.perspective = perspective;
        this.type = type;
        this.isDefaultPerspective = isDefaultPerspective;
    }

    /**
     * Get type of perspective event
     * 
     * @return type of perspective event
     */
    public Type getType() {
        return type;
    }

    /**
     * Get perspective
     * 
     * @return the perspective
     */
    public Perspective getPerspective() {
        return perspective;
    }

    /**
     * Return true if it event from a default perspective
     * 
     * @return true if it event from a default perspective
     */
    public boolean isDefaultPerspective() {
        return isDefaultPerspective;
    }

    @Override
    public String toString() {
        return "PerpectiveEvent [type=" + type + ", perspective=\"" + perspective.getName()
                + "\", isDefaultPerspective=" + isDefaultPerspective + ", source=" + source + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (isDefaultPerspective ? 1231 : 1237);
        result = prime * result + ((perspective == null) ? 0 : perspective.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PerspectiveEvent other = (PerspectiveEvent) obj;
        if (isDefaultPerspective != other.isDefaultPerspective)
            return false;
        if (perspective == null) {
            if (other.perspective != null)
                return false;
        } else if (!perspective.equals(other.perspective))
            return false;
        if (type != other.type)
            return false;
        return true;
    }
}

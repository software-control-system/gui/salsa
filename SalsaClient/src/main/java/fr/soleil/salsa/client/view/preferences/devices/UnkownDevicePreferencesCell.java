package fr.soleil.salsa.client.view.preferences.devices;

/**
 * RecordingManager Device preferences cell
 * 
 * @author Patrick Madela
 */
public class UnkownDevicePreferencesCell extends AbstractDeviceCell {
    /** Class of RecordingManager */
    private static final String DEVICE_CLASS = "Unknown";

    /**
     * Create a new Instance
     */
    public UnkownDevicePreferencesCell() {
        super(DEVICE_CLASS);
    }
}

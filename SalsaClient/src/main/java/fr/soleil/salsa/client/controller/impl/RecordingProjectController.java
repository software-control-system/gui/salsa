package fr.soleil.salsa.client.controller.impl;

import java.util.Optional;

import org.slf4j.Logger;

import fr.soleil.comete.bean.datastorage.helper.Parameters;
import fr.soleil.comete.bean.recordingmanagerprojectbean.RecordingManagerProjectBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * The controller for recording project bean
 * 
 * @author madela
 */
public class RecordingProjectController extends AbstractRecordingController {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(RecordingProjectController.class);

    /** Recording manager add-ons bean */
    private RecordingManagerProjectBean recordingManagerProjectBean;

    @Override
    protected Optional<AbstractTangoBox> getBean() {
        if (devicePreferences.isRecordingManagerDefined()) {
            return Optional.of(getRecordingManagerProjectBean());
        }
        return Optional.empty();
    }

    private RecordingManagerProjectBean getRecordingManagerProjectBean() {
        String recordingManager = devicePreferences.getRecordingManager();
        String recordingManagerProfil = devicePreferences.getRecordingManagerProfil();
        Parameters.getInstance().setDevice(Optional.ofNullable(recordingManager));
        Parameters.getInstance().setProfile(Optional.ofNullable(recordingManagerProfil));
        if (recordingManagerProjectBean == null) {
            recordingManagerProjectBean = RecordingManagerProjectBean.instance();
        }
        return recordingManagerProjectBean;
    }

    @Override
    protected Optional<String> getBeanModel() {
        return getRecordingManagerDevice();
    }
}

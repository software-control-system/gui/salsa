package fr.soleil.salsa.client.view.preferences.devices;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Device cell factory
 * 
 * @author Patrick Madela
 */
public class DeviceCellFactory {
    /** The logger for messages */
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceCellFactory.class);

    /**
     * Utility classes should not have public constructors
     */
    private DeviceCellFactory() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Device class to get constructor
     */
    private enum DeviceClass {
        RECORDING_MANAGER("RecordingManager", RecordingManagerDevicePreferencesCell::new),

        SCAN_SERVER("ScanServer", ScanServerDevicePreferencesCell::new),

        DATA_FITTER("DataFitter", DataFitterDevicePreferencesCell::new),

        PUBLISHER("Publisher", PublisherDevicePreferencesCell::new);

        /** The class name */
        private String name;

        /** The constructor */
        private final Constructor constructor;

        DeviceClass(String name, Constructor constructor) {
            this.name = name;
            this.constructor = constructor;
        }

        private Constructor getConstructor() {
            return constructor;
        }

        /**
         * Insensitive case valueOf
         * 
         * @param name the device class name
         * @return the device class
         */
        public static Optional<DeviceClass> valueOfIgnoreCase(String name) {
            for (DeviceClass deviceClass : DeviceClass.values()) {
                if (deviceClass.name.equalsIgnoreCase(name)) {
                    return Optional.of(deviceClass);
                }
            }
            return Optional.empty();
        }
    }

    /** Constructor supplier */
    @FunctionalInterface
    private interface Constructor {
        AbstractDeviceCell create();
    }

    /**
     * Create a new instance
     * 
     * @param name the device class
     * @return optional instance of device cell
     */
    public static AbstractDeviceCell of(String name) {
        try {
            if (name != null) {
                return DeviceClass.valueOfIgnoreCase(name).map(d -> d.getConstructor().create())
                        .orElse(new UnkownDevicePreferencesCell());
            } else {
                LOGGER.error("device class is mandatory to create device cell");
            }
        } catch (IllegalArgumentException e) {
            LOGGER.error("Unkown device class {} ", name);
        }
        return new UnkownDevicePreferencesCell();
    }

}

package fr.soleil.salsa.client.tool;

import java.io.File;
import java.io.Serializable;

import fr.soleil.salsa.client.view.tool.WindowLayout;

/**
 * Contains the name and the window layout of the perspective. It contains also a File object used
 * for storing.
 * 
 * @author Administrateur
 * 
 */
public class Perspective implements Serializable {

    private static final long serialVersionUID = -931473887596716137L;

    /**
     * Name of the perspective
     */
    private String name;

    /**
     * Window layout of the perspective.
     */
    private WindowLayout windowLayout;

    /**
     * The file containing the perspective.
     */
    private File perspectiveFile;

    /**
     * @param name
     * @param windowLayout
     */
    public Perspective(String name, WindowLayout windowLayout) {
        this.name = name;
        this.windowLayout = windowLayout;
        this.perspectiveFile = new File(name);
    }

    /**
     * Default Constructor
     */
    public Perspective() {

    }

    /**
     * Returns the name of the perspective.
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the perspective.
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the window layout of the perspective.
     */
    public WindowLayout getWindowLayout() {
        return windowLayout;
    }

    /**
     * Gets the window layout of the perspective.
     */
    public void setWindowLayout(WindowLayout windowLayout) {
        this.windowLayout = windowLayout;
    }

    /**
     * Gets the file containing the perspective
     * 
     * @return
     */
    public File getPerspectiveFile() {
        return perspectiveFile;
    }

    /**
     * Gets the file containing the perspective
     * 
     * @return
     */
    public void setPerspectiveFile(File perspectiveFile) {
        this.perspectiveFile = perspectiveFile;
    }

}

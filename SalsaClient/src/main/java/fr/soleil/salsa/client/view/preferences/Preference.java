/**
 * 
 */
package fr.soleil.salsa.client.view.preferences;

import java.awt.Component;

/**
 * Preference interface
 * 
 * @author Tarek
 *
 */
public interface Preference {

    public String getTitle();
    public String getIconUrl();
    public Component getComponent();
    public String getIconTitle();
}

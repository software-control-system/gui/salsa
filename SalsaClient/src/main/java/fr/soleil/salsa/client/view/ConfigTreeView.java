package fr.soleil.salsa.client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.soleil.salsa.client.controller.IConfigTreeController;
import fr.soleil.salsa.client.view.tool.ATreeNode;
import fr.soleil.salsa.client.view.tool.ConfigTreeNode;
import fr.soleil.salsa.client.view.tool.DirectoryTreeNode;
import fr.soleil.salsa.client.view.tool.DragAndDropJTree;
import fr.soleil.salsa.client.view.tool.IDropListener;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;

/**
 * View showing the configuration tree.
 */
@Deprecated
public class ConfigTreeView extends JPanel implements IView<IConfigTreeController> {

    /**
     * Version number for serialization.
     */
    private static final long serialVersionUID = 7016476237879527966L;

    /**
     * Constant returned by confirmDeleteConfig if the user chooses to delete
     * the config.
     */
    public final int DELETE_CONFIG = 0;

    /**
     * Constant returned by confirmDeleteConfig if the user chooses to delete
     * the directory.
     */
    public final int DELETE_DIRECTORY = 0;

    /**
     * The controller.
     */
    private IConfigTreeController controller;

    /**
     * The tree containing the configurations.
     */
    private DragAndDropJTree configTree;

    /**
     * The Swing tree model.
     */
    private TreeModel treeModel;

    /**
     * The scrolling panel containing the tree.
     */
    private JScrollPane scrollPane;

    /**
     * The popup menu that appears on a right click.
     */
    private JPopupMenu popupMenu;

    private JMenuItem newDirectoryItem;

    private JMenuItem newConfig1DItem;

    private JMenuItem newConfig2DItem;

    private JMenuItem newConfigHCSItem;

    private JMenuItem newConfigScanKItem;

    private JMenuItem newConfigScanEnergyItem;

    private JMenuItem saveItem;

    private JMenuItem cutItem;

    private JMenuItem copyItem;

    private JMenuItem pasteItem;

    private JMenuItem deleteItem;

    private JMenuItem renameItem;

    private JMenuItem refreshItem;

    private JButton scanResultButton;

    /**
     * Flag that indicates that the selection is manipulated as part of an
     * internal mechanism and that the controller does not need to be notified.
     */
    private boolean innerSelection = false;

    /**
     * The current selection.
     */
    private ATreeNode selection = null;

    /**
     * True when the user is doing a drag and drop.
     */
    private boolean draggingAndDropping = false;

    public ConfigTreeView() {
        this(null);
    }

    /**
     * Constructor.
     * 
     * @param controller
     */
    public ConfigTreeView(IConfigTreeController controller) {
        this.controller = controller;
        this.initialize();
    }

    /**
     * Initialization.
     * 
     * @return void
     */
    private void initialize() {
        this.setLayout(new GridBagLayout());

        configTree = new DragAndDropJTree();
        configTree.addListener(new IDropListener() {
            @Override
            public void dropped(List<?> droppedSelection, Object target) {
                if (controller != null) {
                    controller.notifyDrop(castToATreeNodesList(droppedSelection), (ATreeNode) target);
                }
            }

            @Override
            public void endDragAndDrop(DragSourceDropEvent dsde) {
                draggingAndDropping = false;
            }

            @Override
            public void startDragAndDrop(DragGestureEvent dge, List<?> selection) {
                draggingAndDropping = true;
            }
        });

        configTree.setModel(null);
        configTree.setCellRenderer(new ConfigTreeCellRenderer());
        configTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        configTree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                if ((!innerSelection) && (!draggingAndDropping)) {
                    TreePath newLeadSelectionPath = e.getNewLeadSelectionPath();
                    if (newLeadSelectionPath != null) {
                        DefaultMutableTreeNode jTreeNode = (DefaultMutableTreeNode) newLeadSelectionPath
                                .getLastPathComponent();
                        selection = (ATreeNode) jTreeNode.getUserObject();
                        if (isSelectionConfig()) {
                            if (controller != null) {
                                controller.notifyConfigSelected((ConfigTreeNode) ConfigTreeView.this.getSelection());
                            }
                        } else {
                            if (controller != null) {
                                controller.notifyDirectorySelected((DirectoryTreeNode) ConfigTreeView.this
                                        .getSelection());
                            }
                        }
                    }
                }
            }
        });

        scanResultButton = new JButton("Select ScanServer configuration");
        scanResultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.selectScanResult();
                }
            }
        });

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(configTree);
        scrollPane.setColumnHeaderView(scanResultButton);
        GridBagConstraints scrollPaneConstraints = new GridBagConstraints();
        scrollPaneConstraints.fill = GridBagConstraints.BOTH;
        scrollPaneConstraints.gridx = 0;
        scrollPaneConstraints.gridy = 0;
        scrollPaneConstraints.weightx = 1.0;
        scrollPaneConstraints.weighty = 1.0;
        this.add(scrollPane, scrollPaneConstraints);

        popupMenu = new JPopupMenu();
        newDirectoryItem = new JMenuItem();
        newDirectoryItem.setText("New Directory");
        newDirectoryItem.setIcon(Icons.getIcon("salsa.scanconfig.folder-new"));
        newDirectoryItem.setMnemonic(KeyEvent.VK_R);
        newDirectoryItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
        newDirectoryItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String directoryName = askNewDirectoryName();
                if (directoryName != null) {
                    if (controller != null) {
                        controller.notifyNewDirectory(directoryName);
                    }
                }
            }
        });
        popupMenu.add(newDirectoryItem);

        newConfig1DItem = new JMenuItem();
        newConfig1DItem.setText("New 1D");
        newConfig1DItem.setIcon(Icons.getIcon("salsa.scanconfig.1d.small"));
        newConfig1DItem.setMnemonic(KeyEvent.VK_N);
        newConfig1DItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        newConfig1DItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newScan(IConfig.ScanType.SCAN_1D);
            }
        });
        popupMenu.add(newConfig1DItem);

        newConfig2DItem = new JMenuItem();
        newConfig2DItem.setText("New 2D");
        newConfig2DItem.setIcon(Icons.getIcon("salsa.scanconfig.2d.small"));
        newConfig2DItem.setMnemonic(KeyEvent.VK_E);
        newConfig2DItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        newConfig2DItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String configName = askNewConfigName();
                if (configName != null) {
                    if (controller != null) {
                        controller.notifyNewConfig(configName, IConfig.ScanType.SCAN_2D);
                    }
                }
            }
        });
        popupMenu.add(newConfig2DItem);

        newConfigHCSItem = new JMenuItem();
        newConfigHCSItem.setText("New HCS");
        newConfigHCSItem.setIcon(Icons.getIcon("salsa.scanconfig.hcs.small"));
        newConfigHCSItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String configName = askNewConfigName();
                if (controller != null) {
                    controller.notifyNewConfig(configName, IConfig.ScanType.SCAN_HCS);
                }
            }
        });
        popupMenu.add(newConfigHCSItem);

        newConfigScanKItem = new JMenuItem();
        newConfigScanKItem.setText("New scan K");
        newConfigScanKItem.setIcon(Icons.getIcon("salsa.scanconfig.k.small"));
        newConfigScanKItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String configName = askNewConfigName();
                if (controller != null) {
                    controller.notifyNewConfig(configName, IConfig.ScanType.SCAN_K);
                }
            }
        });
        popupMenu.add(newConfigScanKItem);

        newConfigScanEnergyItem = new JMenuItem();
        newConfigScanEnergyItem.setText("New scan energy");
        newConfigScanEnergyItem.setIcon(Icons.getIcon("salsa.scanconfig.energy.small"));
        newConfigScanEnergyItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String configName = askNewConfigName();
                if (controller != null) {
                    controller.notifyNewConfig(configName, IConfig.ScanType.SCAN_ENERGY);
                }
            }
        });
        popupMenu.add(newConfigScanEnergyItem);

        popupMenu.addSeparator();

        saveItem = new JMenuItem();
        saveItem.setText("Save");
        saveItem.setIcon(Icons.getIcon("salsa.scanconfig.save.small"));
        saveItem.setMnemonic(KeyEvent.VK_S);
        saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifySave();
                }
            }
        });
        popupMenu.add(saveItem);

        popupMenu.addSeparator();

        cutItem = new JMenuItem();
        cutItem.setText("Cut");
        cutItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        cutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyCut();
                }
            }
        });
        popupMenu.add(cutItem);

        copyItem = new JMenuItem();
        copyItem.setText("Copy");
        copyItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        copyItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyCopy();
                }
            }
        });
        popupMenu.add(copyItem);

        pasteItem = new JMenuItem();
        pasteItem.setText("Paste");
        pasteItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
        pasteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyPaste();
                }
            }
        });
        popupMenu.add(pasteItem);

        deleteItem = new JMenuItem();
        deleteItem.setText("Delete");
        deleteItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        deleteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyDelete();
                }
            }
        });
        popupMenu.add(deleteItem);

        renameItem = new JMenuItem();
        renameItem.setText("Rename");
        renameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ConfigTreeView.this.isSelectionConfig()) {
                    String configName = askNewConfigName();
                    if (controller != null) {
                        controller.notifyRenameConfig(configName);
                    }
                } else {
                    String directoryName = askNewDirectoryName();
                    if (controller != null) {
                        controller.notifyRenameDirectory(directoryName);
                    }
                }
            }
        });
        popupMenu.add(renameItem);

        popupMenu.addSeparator();

        refreshItem = new JMenuItem();
        refreshItem.setText("Refresh");
        refreshItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        refreshItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.notifyRefresh();
                }
            }
        });
        popupMenu.add(refreshItem);

        configTree.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                processPopup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // Elements should be selected on a right click, but this is not
                // the default
                // behavior of a jTree.
                if (SwingUtilities.isRightMouseButton(e)) {
                    int line = configTree.getClosestRowForLocation(e.getX(), e.getY());
                    if (!configTree.isRowSelected(line)) {
                        configTree.setSelectionRow(line);
                    }
                }
                processPopup(e);
            }

            private void processPopup(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });

        // Due to a bug in Swing, accelerators do not work for jPopupMenu. This
        // is a workaround.
        configTree.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent(e);
                JMenuItem menuItem;
                ActionListener[] listeners;
                for (MenuElement menuElement : popupMenu.getSubElements()) {
                    if (menuElement instanceof JMenuItem) {
                        menuItem = (JMenuItem) menuElement;
                        if (keyStroke.equals(menuItem.getAccelerator())) {
                            listeners = menuItem.getActionListeners();
                            if (listeners.length > 0) {
                                listeners[0].actionPerformed(null);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public IConfigTreeController getController() {
        return this.controller;
    }

    @Override
    public void setController(IConfigTreeController controller) {
        this.controller = controller;
    }

    /**
     * Defines the content of the tree.
     * 
     * @param rootDirectoryTreeNode
     *            the root directory of the tree structure.
     */
    public void setTreeContent(DirectoryTreeNode rootDirectoryTreeNode) {
        innerSelection = true;
        ATreeNode selectedNode = this.getSelection();

        int nbRows = configTree.getRowCount();
        Set<ATreeNode> expandedNodesSet = new HashSet<ATreeNode>();
        TreePath treePath;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            if (configTree.isExpanded(rowIndex)) {
                treePath = configTree.getPathForRow(rowIndex);
                Object[] pathElementsArray = treePath.getPath();
                List<ATreeNode> pathElementsList = new ArrayList<ATreeNode>(pathElementsArray.length);
                for (Object element : pathElementsArray) {
                    pathElementsList.add((ATreeNode) ((DefaultMutableTreeNode) element).getUserObject());
                }
                expandedNodesSet.addAll(pathElementsList);
            }
        }

        DefaultMutableTreeNode rootNode = buildJTreeNodeFromWrapper(rootDirectoryTreeNode);

        treeModel = new DefaultTreeModel(rootNode);
        configTree.setModel(treeModel);

        nbRows = configTree.getRowCount();
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            treePath = configTree.getPathForRow(rowIndex);
            ATreeNode treeNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent()).getUserObject();
            if (expandedNodesSet.contains(treeNode)) {
                configTree.expandRow(rowIndex);
                nbRows = configTree.getRowCount();
            }
        }

        ATreeNode treeNode;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            treePath = configTree.getPathForRow(rowIndex);
            treeNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent()).getUserObject();
            if (treeNode.equals(selectedNode)) {
                configTree.addSelectionRow(rowIndex);
            }
        }
        innerSelection = false;
    }

    /**
     * Sets the selection.
     */
    public void setSelection(ATreeNode treeNode) {
        innerSelection = true;
        int nbRows = configTree.getRowCount();
        TreePath treePath;
        boolean select;
        IConfig<?> config1;
        IConfig<?> config2;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            treePath = configTree.getPathForRow(rowIndex);
            ATreeNode searchNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent())
                    .getUserObject();
            select = treeNode.equals(searchNode);
            if (!select) {
                if (treeNode instanceof ConfigTreeNode && searchNode instanceof ConfigTreeNode) {
                    config1 = ((ConfigTreeNode) treeNode).getConfig();
                    config2 = ((ConfigTreeNode) searchNode).getConfig();
                    select = getPath(config1).equals(getPath(config2));
                }
            }
            // Not an else of the previous if : select might have been modified.
            if (select) {
                configTree.addSelectionRow(rowIndex);
                selection = searchNode;
                configTree.expandRow(rowIndex);
                nbRows = configTree.getRowCount();
            } else if (configTree.isRowSelected(rowIndex)) {
                configTree.removeSelectionRow(rowIndex);
            }
        }
        innerSelection = false;
    }

    /**
     * Selects a configuration through its path, expanding upper levels nodes
     * 
     * @param path
     *            The selection path
     */
    public void setSelectedConfig(String path) {
        if (path != null) {
            int nbRows = configTree.getRowCount();
            TreePath treePath = null;
            ATreeNode treeNode = null;
            boolean readyToSelect = false;
            for (int rowIndex = 0; (rowIndex < nbRows) && (!readyToSelect); ++rowIndex) {
                treePath = configTree.getPathForRow(rowIndex);
                treeNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent()).getUserObject();
                if (treeNode instanceof ConfigTreeNode) {
                    String configPath = getPath(((ConfigTreeNode) treeNode).getConfig());
                    if (configPath.indexOf('/') == 0) {
                        configPath = configPath.substring(1);
                    }
                    if (path.equals(configPath)) {
                        readyToSelect = true;
                    }
                } else if (treeNode instanceof DirectoryTreeNode) {
                    // expand directory node if necessary
                    String directoryPath = getPath(((DirectoryTreeNode) treeNode).getDirectory());
                    if (directoryPath.indexOf('/') == 0) {
                        directoryPath = directoryPath.substring(1);
                    }
                    if (path.startsWith(directoryPath)) {
                        configTree.expandPath(treePath);
                    }
                }
            }
            if (readyToSelect) {
                configTree.setSelectionPath(treePath);
            }
        }
    }

    /**
     * Adds a node to the selection.
     * 
     * @param treeNode
     */
    public void addSelection(ATreeNode treeNode) {
        innerSelection = true;
        int nbRows = configTree.getRowCount();
        TreePath treePath;
        boolean select;
        IConfig<?> config1;
        IConfig<?> config2;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            treePath = configTree.getPathForRow(rowIndex);
            ATreeNode searchNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent())
                    .getUserObject();
            select = treeNode.equals(searchNode);
            if (!select) {
                if (treeNode instanceof ConfigTreeNode && searchNode instanceof ConfigTreeNode) {
                    config1 = ((ConfigTreeNode) treeNode).getConfig();
                    config2 = ((ConfigTreeNode) searchNode).getConfig();
                    select = getPath(config1).equals(getPath(config2));
                }
            }
            // Not an else of the previous if : select might have been modified.
            if (select) {
                configTree.addSelectionRow(rowIndex);
                selection = searchNode;
                configTree.expandRow(rowIndex);
                nbRows = configTree.getRowCount();
            }
        }
        innerSelection = false;
    }

    /**
     * Sets the selection.
     */
    public void setMultipleSelection(List<ATreeNode> multipleSelection) {
        innerSelection = true;
        int nbRows = configTree.getRowCount();
        TreePath treePath;
        boolean select;
        IConfig<?> config1;
        IConfig<?> config2;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            treePath = configTree.getPathForRow(rowIndex);
            ATreeNode searchNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent())
                    .getUserObject();
            select = false;
            for (ATreeNode treeNode : multipleSelection) {
                select = treeNode.equals(searchNode);
                if (!select) {
                    if (treeNode instanceof ConfigTreeNode && searchNode instanceof ConfigTreeNode) {
                        config1 = ((ConfigTreeNode) treeNode).getConfig();
                        config2 = ((ConfigTreeNode) searchNode).getConfig();
                        select = getPath(config1).equals(getPath(config2));
                    }
                }
                // Not an else of the previous if : select might have been
                // modified.
                if (select) {
                    break;
                }
            }
            if (select) {
                configTree.addSelectionRow(rowIndex);
                selection = searchNode;
                configTree.expandRow(rowIndex);
                nbRows = configTree.getRowCount();
            } else if (configTree.isRowSelected(rowIndex)) {
                configTree.removeSelectionRow(rowIndex);
            }
        }
        innerSelection = false;
    }

    /**
     * Returns the complete path to a config.
     * 
     * @param config
     * @return
     */
    private static String getPath(IConfig<?> config) {
        return getPath(config.getDirectory()) + "/" + config.getName();
    }

    /**
     * Returns the complete path to a directory..
     * 
     * @param config
     * @return
     */
    private static String getPath(IDirectory directory) {
        if (directory == null) {
            return "";
        }
        if (directory.getName() == null) {
            return "/";
        }
        return getPath(directory.getDirectory()) + "/" + directory.getName();
    }

    /**
     * Creates a JTree node from a DirectoryTreeNode.
     * 
     * @param directory
     */
    private DefaultMutableTreeNode buildJTreeNodeFromWrapper(DirectoryTreeNode directoryTreeNode) {
        if (directoryTreeNode != null && directoryTreeNode.getSubDirectoriesList() != null) {
            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(directoryTreeNode);
            for (DirectoryTreeNode subDirectoryTreeNode : directoryTreeNode.getSubDirectoriesList()) {
                treeNode.add(buildJTreeNodeFromWrapper(subDirectoryTreeNode));
                TreePath treeNodePath = new TreePath(treeNode.getPath());
                this.configTree.scrollPathToVisible(treeNodePath);
                this.configTree.setShowsRootHandles(true);

            }
            for (ConfigTreeNode configTreeNode : directoryTreeNode.getConfigsList()) {
                treeNode.add(buildJTreeNodeFromWrapper(configTreeNode));
            }
            return treeNode;
        } else {
            return null;
        }

    }

    /**
     * Creates a JTree node from a ConfigTreeNode.
     * 
     * @param config
     * @return
     */
    private DefaultMutableTreeNode buildJTreeNodeFromWrapper(ConfigTreeNode configTreeNode) {
        DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(configTreeNode);
        return treeNode;
    }

    /**
     * Returns the current selection. This returns a single item : the last one
     * that was clicked. To get all the selected elements, use
     * getMultipleSelection instead. If no item was clicked, returns null.
     * 
     * @return
     */
    public ATreeNode getSelection() {
        return selection;
    }

    /**
     * Returns the current selection. Returns a list of the selected nodes. If
     * you only need the last selected, use getSelection instead.
     * 
     * @return
     */
    public List<ATreeNode> getMultipleSelection() {
        List<ATreeNode> multipleSelection = new ArrayList<ATreeNode>();
        int nbRows = configTree.getRowCount();
        TreePath treePath;
        ATreeNode treeNode;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            if (configTree.isRowSelected(rowIndex)) {
                treePath = configTree.getPathForRow(rowIndex);
                treeNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent()).getUserObject();
                multipleSelection.add(treeNode);
            }
        }
        return multipleSelection;
    }

    /**
     * Returns true if the current selection is a configuration, false if it is
     * a directory.
     * 
     * @return
     */
    public boolean isSelectionConfig() {
        ATreeNode treeNode = this.getSelection();
        return treeNode instanceof ConfigTreeNode;
    }

    /**
     * Expands a node.
     * 
     * @param treeNode
     */
    public void expandNode(ATreeNode treeNode) {
        int nbRows = configTree.getRowCount();
        TreePath treePath;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            treePath = configTree.getPathForRow(rowIndex);
            ATreeNode searchTreeNode = (ATreeNode) ((DefaultMutableTreeNode) treePath.getLastPathComponent())
                    .getUserObject();
            if (searchTreeNode.equals(treeNode)) {
                configTree.expandRow(rowIndex);
            }
        }
    }

    /**
     * Popup that asks a configuration name.
     * 
     * @return
     */
    private String askNewConfigName() {
        String result = JOptionPane.showInputDialog(this, "Configuration name :", "New configuration",
                JOptionPane.PLAIN_MESSAGE);
        if (result != null && result.contains("/")) {
            JOptionPane.showMessageDialog(this, "The caracter '/' is not allowed");
            return askNewConfigName();
        }
        return result;
    }

    /**
     * Popup that asks a directory name.
     * 
     * @return
     */
    private String askNewDirectoryName() {
        String result = JOptionPane.showInputDialog(this, "Directory name :", "New directory",
                JOptionPane.PLAIN_MESSAGE);
        if (result != null && result.contains("/")) {
            JOptionPane.showMessageDialog(this, "The caracter '/' is not allowed");
            return askNewDirectoryName();
        }
        return result;
    }

    /**
     * New Scan 1d process.
     */
    public void newScan(IConfig.ScanType scanType) {
        String configName = askNewConfigName();
        if (controller != null) {
            controller.notifyNewConfig(configName, scanType);
        }
    }

    /**
     * The new directory process.
     * 
     * @param scanType
     */
    public void newDirectory() {
        String configName = askNewDirectoryName();
        if (controller != null) {
            controller.notifyNewDirectory(configName);
        }
    }

    /**
     * Asks the user for confirmation when during a refresh, Salsa notices that
     * a modified config was deleted.
     * 
     * @param configName
     * @return
     */
    public boolean askRefreshConfigDeleted(String configName) {
        return JOptionPane.showConfirmDialog(this, "The config " + configName
                + " that you modified has been deleted. Do you want to delete it ?", "Conflict detected",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION;
    }

    /**
     * Asks the user for confirmation when during a refresh, Salsa notices that
     * a modified config was modified by another user.
     * 
     * @param configName
     * @return
     */
    public boolean askRefreshConfigModified(String configName) {
        return JOptionPane.showConfirmDialog(this, "The config " + configName
                + " has been modified. Do you want to discard your changes and update it ?", "Conflict detected",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION;
    }

    /**
     * Casts to a list of ATreeNode.
     * 
     * @param object
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<ATreeNode> castToATreeNodesList(Object object) {
        return (List<ATreeNode>) object;
    }

    /**
     * Asks the user a confirmation for deleting config.
     * 
     * @return
     */
    public int confirmDeleteConfig(IConfig<?> config) {
        String message;
        message = "Are you sure you want to delete the " + config.getName() + " config ?";
        String options[] = { "Yes", "No", };
        int choice = JOptionPane.showOptionDialog(this, message, "Delete", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        return choice;
    }

    /**
     * Asks the user a confirmation for deleting config.
     * 
     * @return
     */
    public int confirmDeleteDirectory(IDirectory directory) {
        String message;
        message = "Are you sure you want to delete the " + directory.getName() + " directory ?";
        String options[] = { "Yes", "No", };
        int choice = JOptionPane.showOptionDialog(this, message, "Delete", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        return choice;
    }

    public JTree getConfigTree() {
        return configTree;
    }

}
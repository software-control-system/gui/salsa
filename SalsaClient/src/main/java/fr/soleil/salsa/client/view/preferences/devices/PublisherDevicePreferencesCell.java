package fr.soleil.salsa.client.view.preferences.devices;

/**
 * Publisher Device preferences cell
 * 
 * @author Patrick Madela
 */
public class PublisherDevicePreferencesCell extends AbstractDeviceCell {
    /** Class of ScanServer */
    private static final String DEVICE_CLASS = "Publisher";

    /**
     * Create a new Instance
     */
    public PublisherDevicePreferencesCell() {
        super(DEVICE_CLASS);
    }
}

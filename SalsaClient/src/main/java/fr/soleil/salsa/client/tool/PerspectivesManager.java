package fr.soleil.salsa.client.tool;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;

import com.google.common.eventbus.EventBus;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.salsa.client.exception.SalsaPerspectiveException;
import fr.soleil.salsa.client.tool.PerspectiveEvent.Type;
import fr.soleil.salsa.exception.SalsaMissingPropertyException;
import fr.soleil.salsa.exception.SalsaPreferencesException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.EventBusHandler;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Manages all the operations concerning the manipulation of the perspectives.
 * 
 * @author madela
 */
public class PerspectivesManager {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(PerspectivesManager.class);

    /** The property name for Salsa perspectives path */
    public static final String SALSA_PERSPECTIVES_DIRECTORY = "SALSA_PERSPECTIVES_DIRECTORY";

    /** The default list of perspectives */
    private static final String[] DEFAULT_PERSPECTIVES = { "Default", "Configuration", "Scan" };

    /** The perspective file extension */
    private static final String PERSPECTIVE_FILE_EXTENSION = ".dat";

    /** The application bus event */
    private final EventBus eventBus;

    /** The perspective directory */
    private String perspectivesDirectory;

    /** List of perspectives. */
    private List<Perspective> perspectives = new ArrayList<>();

    /** List of perspective name of restored default perspective since the last init call */
    private Collection<String> restoredDefaultPerspectiveNames = new LinkedHashSet<>();

    /** Map of wrong perspective file by perspective name since the last init call */
    private Map<String, String> wrongPerspectiveFiles = new LinkedHashMap<>();

    /** Current selected perspective */
    private Perspective currentPerspective;

    /**
     * Constructor
     * 
     * @param eventBus the eventBus
     */
    PerspectivesManager(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    /**
     * Create a new instance of {@link PerspectivesManager}
     * 
     * @return new instance of {@link PerspectivesManager}
     */
    public static PerspectivesManager instance() {
        return new PerspectivesManager(EventBusHandler.getEventBus());
    }

    /**
     * Notifies all {@link PerspectiveListener}s for some changes
     * 
     * @param type type of event
     * @param perspective perspective of event
     */
    private void fireChange(Type type, Perspective perspective) {
        fireChange(type, perspective, false);
    }

    /**
     * Notifies all {@link PerspectiveListener}s for some changes and notifies with another done event after the
     * expected notification if isDone is false
     * 
     * @param type type of event
     * @param perspective perspective of event
     * @param isDone notify with another done event after the expected notification when false
     */
    private void fireChange(Type type, Perspective perspective, boolean isDone) {
        PerspectiveEvent perspectiveEvent = PerspectiveEvent.get(perspective, type, perspective,
                isDefaultPerspective(perspective));
        LOGGER.debug("Post {}", perspectiveEvent);
        eventBus.post(perspectiveEvent);
        if (!isDone) {
            fireChange(Type.DONE, perspective, true);
        }
    }

    /**
     * Gets the an array containing the perspectives, required for input dialog.
     * 
     * @return array of perspective names
     */
    public String[] getPerspectiveNames() {
        return perspectives.stream().map(Perspective::getName).toArray(String[]::new);
    }

    /**
     * Get name of first default perspective
     * 
     * @return name of first default perspective
     */
    public String getDefaultPerspectiveName() {
        return DEFAULT_PERSPECTIVES[0];
    }

    /**
     * Gets the an array containing the default perspectives, required for input dialog.
     * 
     * @return array of default perspective names
     */
    public String[] getDefaultPerspectiveNames() {
        return DEFAULT_PERSPECTIVES.clone();
    }

    /**
     * Gets the perspective specified by name.
     * 
     * @param name the perspective name
     * @return the perspective with specified name or null
     */
    public Perspective getPerspectiveByName(String name) {
        return perspectives.stream().filter(p -> p.getName().equals(name)).findFirst().orElse(null);
    }

    /**
     * List of perspective name of wrong perspective file on last init call
     * 
     * @return List of perspective name of wrong perspective file
     */
    public List<String> getWrongPerspectiveFiles() {
        return Collections.unmodifiableList(new ArrayList<>(wrongPerspectiveFiles.keySet()));
    }

    /**
     * Remove wrong perspective file of last init call
     * 
     * @return the list that was unable to delete
     */
    public List<String> deleteWrongPerspectiveFiles() {
        List<String> perspectiveFilesFailedToDelete = new ArrayList<>();
        for (Iterator<Entry<String, String>> iterator = wrongPerspectiveFiles.entrySet().iterator(); iterator
                .hasNext();) {
            Entry<String, String> wrongPerspective = iterator.next();
            String perspectiveName = wrongPerspective.getKey();
            Path perspectivePath = Paths.get(wrongPerspective.getValue());
            try {
                Files.delete(perspectivePath);
                iterator.remove();
            } catch (IOException e) {
                perspectiveFilesFailedToDelete.add(perspectivePath.toString());
                LOGGER.warn("Unable delete file {} for wrong perspective {}: {}", perspectivePath, perspectiveName,
                        e.getMessage());
                LOGGER.trace("Unable delete file {} for wrong perspective {}", perspectivePath, perspectiveName, e);
            }
        }
        return perspectiveFilesFailedToDelete;
    }

    /**
     * List of perspective name of restored default perspective on last init call
     * 
     * @return List of perspective name of restored default perspective
     */
    public Collection<String> getRestoredDefaultPerspectiveNames() {
        return Collections.unmodifiableCollection(restoredDefaultPerspectiveNames);
    }

    /**
     * Delete perspective
     * 
     * @param perspective the {@link Perspective} to delete
     */
    public void deletePerspective(Perspective perspective) {
        if (perspective != null) {
            Path perspectivePath = getPerspectiveFilesLocation()
                    .resolve(perspective.getName() + PERSPECTIVE_FILE_EXTENSION);
            try {
                Files.delete(perspectivePath);
            } catch (IOException e) {
                LOGGER.warn("Cannot delete file {} for perspective {}: {}", perspectivePath, perspective.getName(),
                        e.getMessage());
                LOGGER.trace("Cannot delete file {} for perspective {}", perspectivePath, perspective.getName(), e);
            }
            perspectives.remove(perspective);
            fireChange(Type.REMOVE, perspective);
        }
    }

    /**
     * Select the perspective
     * 
     * @param perspectiveName the perspective name
     */
    public void selectCurrentPerspective(String perspectiveName) {
        Optional.ofNullable(getPerspectiveByName(perspectiveName)).ifPresent(this::setCurrentPerspective);
    }

    /**
     * Initialize perspective manager, remove all perspectives and reload it from perspective directory
     * 
     * @throws SalsaPreferencesException if unable to get perspective directory
     */
    public void init() throws SalsaPreferencesException {
        perspectivesDirectory = getPerspectivesDirectory();
        perspectives.stream().forEach(perspective -> fireChange(Type.REMOVE, perspective));
        perspectives.clear();
        restoredDefaultPerspectiveNames.clear();
        wrongPerspectiveFiles.clear();
        loadPerspectives(getPerpectiveNames()).stream().forEach(this::addPerspective);
        if (SalsaUtils.isFulfilled(perspectives)) {
            selectCurrentPerspective(perspectives.get(0).getName());
        }
    }

    /**
     * Get new {@link Perspective}
     * 
     * @return the new perspective
     */
    public Perspective createPerspective() {
        return createPerspective(null);
    }

    /**
     * Get new {@link Perspective}
     * 
     * @param perspectiveName name of perspective
     * 
     * @return the new perspective
     */
    public Perspective createPerspective(String perspectiveName) {
        Perspective newPerspective = new Perspective(getNewPerspectiveName(perspectiveName), null);
        addPerspective(newPerspective);
        return newPerspective;
    }

    /**
     * Get current perspective
     * 
     * @return the current perspective
     */
    public Perspective getCurrentPerspective() {
        return currentPerspective;
    }

    private void setCurrentPerspective(Perspective perspective) {
        this.currentPerspective = perspective;
        fireChange(Type.SELECT, perspective);
    }

    /**
     * Reset window layout of current perspective with layout of reference perspective
     * 
     * @param referencePerspectiveName the reference perspective name
     * @throws SalsaPerspectiveException if unable to load default perspective
     */
    public void resetCurrentPerspectiveLayout(String referencePerspectiveName) throws SalsaPerspectiveException {
        try (InputStream is = getClass().getResourceAsStream(getPerspectiveResource(referencePerspectiveName));
                BufferedInputStream bis = new BufferedInputStream(is)) {
            Perspective referencePerspective = loadPerspective(bis);
            getCurrentPerspective().setWindowLayout(referencePerspective.getWindowLayout());
            fireChange(Type.SELECT, getCurrentPerspective());
        } catch (SalsaPerspectiveException e) {
            throw e;
        } catch (Exception e) {
            throw new SalsaPerspectiveException("Unable to reset layout with layout of perspective \""
                    + referencePerspectiveName + "\": " + e.getMessage(), e);
        }
    }

    /**
     * Save all perspectives into files.
     */
    public void savePerspectives() {
        if (SalsaUtils.isFulfilled(perspectives)) {
            for (Perspective perspective : perspectives) {
                Path perspectivePath = getPerspectivePath(perspective);
                savePerspective(perspective, perspectivePath);
            }
        }
    }

    private void addPerspective(Perspective perspective) {
        if (perspective != null) {
            perspectives.add(perspective);
            fireChange(Type.ADD, perspective);
        }
    }

    private String getNewPerspectiveName(String perspectiveName) {
        String rootPerspectiveName = SalsaUtils.isDefined(perspectiveName) ? perspectiveName : "New perspective";
        int version = 2;
        String newPerspectiveName = rootPerspectiveName.trim();
        while (isAlreadyUsedPerspectiveName(newPerspectiveName)) {
            newPerspectiveName = rootPerspectiveName + " " + version;
            version++;
        }
        return newPerspectiveName;
    }

    private boolean isAlreadyUsedPerspectiveName(String perspectiveName) {
        return perspectives.stream().sorted((p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName()))
                .anyMatch(p -> p.getName().equalsIgnoreCase(perspectiveName));
    }

    private static String getPerspectivesDirectory() throws SalsaMissingPropertyException {
        String preferenceDirectory = SystemUtils.getSystemProperty(SALSA_PERSPECTIVES_DIRECTORY);
        if (!SalsaUtils.isDefined(preferenceDirectory)) {
            throw new SalsaMissingPropertyException(SALSA_PERSPECTIVES_DIRECTORY);
        }
        return preferenceDirectory;
    }

    private List<Perspective> loadPerspectives(Collection<String> perspectiveNames) {
        return perspectiveNames.stream().map(this::loadPerspective).filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
    }

    private Optional<Perspective> loadPerspective(String perspectiveName) {
        Optional<Perspective> optional;
        Path perspectivePath = getPerspectivePath(perspectiveName);
        try {
            Perspective p;
            if (Files.exists(perspectivePath)) {
                p = loadPerspective(perspectivePath);
            } else {
                p = overwriteDefaultPerspective(perspectiveName);
            }
            Perspective current = getCurrentPerspective();
            if (current != null && ObjectUtils.sameObject(current.getName(), p.getName())) {
                current.setWindowLayout(p.getWindowLayout());
                fireChange(Type.SELECT, getCurrentPerspective());
            }
            optional = Optional.of(p);
        } catch (SalsaPerspectiveException e) {
            LOGGER.error("Unable to load perspective {} from file {}: {}", perspectiveName, perspectivePath,
                    e.getMessage());
            LOGGER.trace("Unable to load perspective {} from file {}", perspectiveName, perspectivePath, e);
            if (isDefaultPerspective(perspectiveName)) {
                try {
                    optional = restoreDefaultPerspective(perspectiveName);
                } catch (Exception e2) {
                    optional = Optional.empty();
                    LOGGER.error("Unable to restore default perspective " + perspectiveName, e2);
                }
            } else {
                optional = Optional.empty();
                wrongPerspectiveFiles.put(perspectiveName, perspectivePath.toAbsolutePath().toString());
            }
        }
        return optional;
    }

    private Optional<Perspective> restoreDefaultPerspective(String perspectiveName) {
        Optional<Perspective> optional;
        Path perspectivePath = getPerspectivePath(perspectiveName);
        try {
            Perspective restoredDefaultPerfective = overwriteDefaultPerspective(perspectiveName);
            restoredDefaultPerspectiveNames.add(perspectiveName);
            optional = Optional.of(restoredDefaultPerfective);
        } catch (SalsaPerspectiveException e) {
            optional = Optional.empty();
            LOGGER.error("Unable to restore default perspective {} to {}: {}", perspectiveName, perspectivePath,
                    e.getMessage());
            LOGGER.trace("Unable to restore default perspective {} to {}", perspectiveName, perspectivePath, e);
        }
        return optional;
    }

    private Perspective overwriteDefaultPerspective(String perspectiveName) throws SalsaPerspectiveException {
        Path perspectivePath = getPerspectivePath(perspectiveName);
        try (InputStream is = getClass().getResourceAsStream(getPerspectiveResource(perspectiveName));
                BufferedInputStream bis = new BufferedInputStream(is)) {
            Files.copy(bis, perspectivePath, StandardCopyOption.REPLACE_EXISTING);
            return loadPerspective(perspectivePath);
        } catch (SalsaPerspectiveException e) {
            throw e;
        } catch (Exception e) {
            throw new SalsaPerspectiveException("Unable to overwrite default perspective: " + e.getMessage(), e);
        }
    }

    private Perspective loadPerspective(Path perspectivePath) throws SalsaPerspectiveException {
        try (FileInputStream fis = new FileInputStream(perspectivePath.toFile());
                BufferedInputStream bis = new BufferedInputStream(fis)) {
            return loadPerspective(bis);
        } catch (SalsaPerspectiveException e) {
            throw e;
        } catch (EOFException e) {
            throw new SalsaPerspectiveException("Unable to load perspective: file is empty", e);
        } catch (Exception e) {
            throw new SalsaPerspectiveException("Unable to load perspective: " + e.getMessage(), e);
        }
    }

    private Perspective loadPerspective(InputStream istream) throws SalsaPerspectiveException {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(istream)) {
            return (Perspective) objectInputStream.readObject();
        } catch (Exception e) {
            throw new SalsaPerspectiveException(e.getMessage(), e);
        }
    }

    private Path getPerspectivePath(String perspectiveName) {
        return getPerspectiveFilesLocation().resolve(perspectiveName + PERSPECTIVE_FILE_EXTENSION).toAbsolutePath();
    }

    private String getPerspectiveResource(String perspectiveName) {
        return "/fr/soleil/salsa/view/" + perspectiveName + PERSPECTIVE_FILE_EXTENSION;
    }

    private Path getPerspectivePath(Perspective perspective) {
        return getPerspectivePath(perspective.getName());
    }

    private Set<String> getPerpectiveNames() {
        Set<String> perspectiveNames = new LinkedHashSet<>();
        for (String perspective : DEFAULT_PERSPECTIVES) {
            perspectiveNames.add(perspective);
        }
        perspectiveNames.addAll(getPerspectiveNamesFromFiles());
        return perspectiveNames;
    }

    private Path getPerspectiveFilesLocation() {
        Path perpectivesPath = Paths.get(perspectivesDirectory);
        if (!Files.isDirectory(perpectivesPath)) {
            perpectivesPath = perpectivesPath.getParent();
        }
        return perpectivesPath;
    }

    private List<String> getPerspectiveNamesFromFiles() {
        List<String> names;
        Path perspectiveFilesLocation = getPerspectiveFilesLocation();
        try (Stream<Path> paths = Files.list(perspectiveFilesLocation)) {
            names = paths.map(Path::getFileName).map(Path::toString).filter(f -> f.endsWith(PERSPECTIVE_FILE_EXTENSION))
                    .map(f -> f.substring(0, f.lastIndexOf("."))).collect(Collectors.toList());
        } catch (Exception e) {
            names = Collections.emptyList();
            LOGGER.error("Unable to get perspectives names from files in {}: {}", perspectiveFilesLocation,
                    e.getMessage());
            LOGGER.trace("Unable to get perspectives names from files in {}", perspectiveFilesLocation, e);
        }
        return names;
    }

    private void savePerspective(Perspective perspective, Path perspectivePath) {
        File perspectiveFile = perspectivePath.toFile();
        perspective.setPerspectiveFile(perspectiveFile);
        try (FileOutputStream os = new FileOutputStream(perspectiveFile);
                BufferedOutputStream bos = new BufferedOutputStream(os);
                ObjectOutputStream objectOutPutStream = new ObjectOutputStream(bos);) {
            objectOutPutStream.writeObject(perspective);
            objectOutPutStream.flush();
        } catch (Exception e) {
            LOGGER.error("Unable to save perspective {} to {}: {}", perspective.getName(), perspectivePath,
                    e.getMessage());
            LOGGER.trace("Unable to save perspective {} to {}", perspective.getName(), perspectivePath, e);
        }
    }

    private boolean isDefaultPerspective(String perspectiveName) {
        boolean found = false;
        for (String perspective : DEFAULT_PERSPECTIVES) {
            if (perspective.equals(perspectiveName)) {
                found = true;
                break;
            }
        }
        return found;
    }

    private boolean isDefaultPerspective(Perspective perspective) {
        return isDefaultPerspective(perspective.getName());
    }
}

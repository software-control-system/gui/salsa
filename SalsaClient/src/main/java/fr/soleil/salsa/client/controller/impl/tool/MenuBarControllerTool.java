package fr.soleil.salsa.client.controller.impl.tool;

import fr.soleil.salsa.client.controller.impl.ApplicationController;
import fr.soleil.salsa.entity.IConfig;

/**
 * The menu bar is part of the main window, so it is controlled by the application controller. In
 * order to make the code easier to maintain, the code pertaining to the menu bar is isolated in
 * this class. It is NOT an independent controller, but a part of the application controller.
 * 
 * @see ApplicationController
 */
public class MenuBarControllerTool {

    /**
     * The application controller delegating management of the menu bar to this tool.
     */
    private final ApplicationController applicationController;

    /**
     * Constructor.
     * 
     * @param applicationController the application controller delegating management of the menu bar
     *            to this tool.
     */
    public MenuBarControllerTool(ApplicationController applicationController) {
        super();
        this.applicationController = applicationController;
    }

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    /**
     * Actuators view selected in the menu.
     */
    public void notifyActuatorView() {
        applicationController.showActuatorListView(true);
    }

    /**
     * Actuators Y view selected in the menu.
     */
    public void notifyYActuatorView() {
        applicationController.showYActuatorListView(true);
    }

    /**
     * Sensors view selected in the menu.
     */
    public void notifySensorView() {
        applicationController.showSensorListView(true);
    }

    /**
     * Trajectories X 2D view selected in the menu.
     */
    public void notifyXTrajectories2DView() {
        applicationController.showXTrajectories2DView(true);
    }

    /**
     * Trajectories X 2D view selected in the menu.
     */
    public void notifyYTrajectories2DView() {
        applicationController.showYTrajectories2DView(true);
    }

    /**
     * Trajectories X view selected in the menu.
     */
    public void notifyXTrajectoriesView() {
        applicationController.showXTrajectoriesView(true);
    }

    /**
     * Timebases view selected in the menu.
     */
    public void notifyTimebasesView() {
        applicationController.showTimebasesView(true);
    }

    /**
     * Timebases view selected in the menu.
     */
    public void notifyErrorStrategiesView() {
        applicationController.showErrorStrategiesView(true);
    }

    /**
     * ScanK view selected in the menu.
     */
    public void notifyScanKView() {
        applicationController.showScanKTrajectoryView(true);
    }

    /**
     * Configurations tree view selected in the menu.
     */
    public void notifyConfigTreeView() {
        applicationController.showConfigTreeView(true);
    }

    /**
     * Scan result view selected in the menu.
     */
    public void notifyScanResultView() {
        applicationController.showScanResultView(true);
    }

    /**
     * Historic View selected in the menu.
     */
    public void notifyHistoricView() {
        applicationController.showHistoricView(true);
    }

    /**
     * Historic Log View selected in the menu.
     */
    public void notifyHistoricLogView() {
        applicationController.showHistoricLogView(true);
    }

    /**
     * DataFitter View selected in the menu
     */
    public void notifyDataFitterView() {
        applicationController.showDataFitterView(true);
    }

    /**
     * General View selected in the menu.
     */
    public void notifyGeneralView() {
        applicationController.showGeneralView(true);
    }

    /**
     * Select a perspective in the menu
     * 
     * @param perspectiveName the name of perspective
     */
    public void notifySelectPerspective(String perspectiveName) {
        applicationController.selectPerspective(perspectiveName);
    }

    /**
     * New perspective view selected in the menu.
     */
    public void notifyNewPerspectiveView() {
        applicationController.showNewPerspectiveView();
    }

    /**
     * Rename perspective view selected in the menu.
     */
    public void notifyRenamePerspectiveView() {
        applicationController.showRenamePerspectiveView();
    }

    /**
     * Deleting a perspective view selected in the menu.
     */
    public void notifyRemovePerspectiveView() {
        applicationController.removePerspectiveView();
    }

    /**
     * New scan1d.
     */
    public void notifyNewScan1d() {
        applicationController.notifyNewScan(IConfig.ScanType.SCAN_1D);
    }

    /**
     * New scan2d.
     */
    public void notifyNewScan2d() {
        applicationController.notifyNewScan(IConfig.ScanType.SCAN_2D);
    }

    /**
     * New scanHCS.
     */
    public void notifyNewScanHCS() {
        applicationController.notifyNewScan(IConfig.ScanType.SCAN_HCS);
    }

    /**
     * New scanHCS.
     */
    public void notifyNewScanK() {
        applicationController.notifyNewScan(IConfig.ScanType.SCAN_K);
    }

    /**
     * New scan energy
     */
    public void notifyNewScanEnergy() {
        applicationController.notifyNewScan(IConfig.ScanType.SCAN_ENERGY);
    }

    /**
     * Scan energy selected in the menu.
     */
    public void notifyScanEnergyView() {
        applicationController.showScanEnergyView(true);
    }

    /**
     * New directory selected in the menu.
     */
    public void notifyNewDirectory() {
        applicationController.notifyNewDirectory();
    }

    /**
     * Copy selected in the menu.
     */

    public void notifyCopyAction() {
        applicationController.notifyCopyAction();
    }

    /**
     * Copy selected in the menu.
     */

    public void notifyCutAction() {
        applicationController.notifyCutAction();
    }

    /**
     * Paste selected in the menu.
     */

    public void notifyPasteAction() {
        applicationController.notifyPasteAction();
    }

    /**
     * Save action selected in the menu.
     */
    public void notifySavingAction() {
        applicationController.notifySavingAction();
    }

    /**
     * Save action selected in the menu.
     */
    public void notifyExitAction() {
        applicationController.notifyExiting();
    }

    /**
     * Shows the preferences view
     */
    public void notifyPreferencesView() {
        applicationController.showPreferencesView();
    }

    /**
     * Lock layout
     */
    public void notifyLockLayout() {
        applicationController.notifyLockLayout();
    }

    /**
     * Unlock layout
     */
    public void notifyUnlockLayout() {
        applicationController.notifyUnlockLayout();
    }

    /**
     * Reset layout
     */
    public void notifyResetLayout() {
        applicationController.notifyResetLayout();
    }

    /**
     * "Save perspectives" button clicked
     */
    public void notifySavePerspectivesView() {
        applicationController.savePerspectives();
    }

    /**
     * Scan functions view selected in the menu.
     */
    public void notifyScanFuntionsView() {
        applicationController.showScanFunctionsView(true);
    }

    /**
     * Hooks view selected in the menu.
     */
    public void notifyHooksView() {
        applicationController.showHooksView(true);
    }

    /**
     * Display Manager selected in the menu
     */
    public void notifyDisplayManagerView() {
        applicationController.showDisplayManagerView(true);
    }

    /**
     * Configuration Manager selected in the menu
     */
    public void notifyConfigurationManagerView() {
        applicationController.showConfigurationManagerView(true);
    }

    /**
     * Bookmarks selected in the menu
     */
    public void notifyBookmarksView() {
        applicationController.showBookmarksView(true);
    }

    /**
     * Recording view selected in the menu
     */
    public void notifyRecordingView() {
        applicationController.showRecordingControlView(true);
    }

    /**
     * Recording project view selected in the menu
     */
    public void notifyRecordingProjectView() {
        applicationController.showRecordingProjectView(true);
    }

    /**
     * Recording counters view selected in the menu
     */
    public void notifyRecordingCountersControlView() {
        applicationController.showRecordingCountersView(true);
    }

    /**
     * Recording extension view selected in the menu
     */
    public void notifyRecordingExtensionView() {
        applicationController.showRecordingExtensionView(true);
    }
}

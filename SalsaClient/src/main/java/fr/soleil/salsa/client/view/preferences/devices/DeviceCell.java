package fr.soleil.salsa.client.view.preferences.devices;

import javax.swing.table.TableCellEditor;

/**
 * Device preferences cell
 * 
 * @author Patrick Madela
 *
 */
public interface DeviceCell {
    /**
     * Get table row object
     * 
     * @return table row object
     */
    Object[] get();

    /**
     * Get device cell value
     * 
     * @return device cell value
     */
    String getValue();

    /**
     * Set device cell value
     * 
     * @param value device cell value
     */
    void setValue(String value);

    /**
     * Set device cell value and return this
     * 
     * @param value device cell value
     * @return this
     */
    DeviceCell withValue(String value);

    /**
     * Get cell editor
     * 
     * @return cell editor
     */
    TableCellEditor getCellEditor();
}

package fr.soleil.salsa.client.controller.impl;

import java.util.Optional;

import org.slf4j.Logger;

import fr.soleil.comete.bean.datastorage.helper.Parameters;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.recordingmanageraddons.RecordingManagerAddonsBean;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * The controller for recording extensions bean
 * 
 * @author madela
 */
public class RecordingExtensionController extends AbstractRecordingController {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(RecordingExtensionController.class);

    /** Recording manager add-ons bean */
    private RecordingManagerAddonsBean recordingManagerAddonsBean;

    @Override
    protected Optional<AbstractTangoBox> getBean() {
        if (devicePreferences.isRecordingManagerDefined()) {
            return Optional.of(getRecordingManagerAddonsBean());
        }
        return Optional.empty();
    }

    private RecordingManagerAddonsBean getRecordingManagerAddonsBean() {
        String recordingManager = devicePreferences.getRecordingManager();
        String recordingManagerProfil = devicePreferences.getRecordingManagerProfil();
        Parameters.getInstance().setDevice(Optional.ofNullable(recordingManager));
        Parameters.getInstance().setProfile(Optional.ofNullable(recordingManagerProfil));
        if (recordingManagerAddonsBean == null) {
            recordingManagerAddonsBean = RecordingManagerAddonsBean.instance();
        }
        return recordingManagerAddonsBean;
    }

    @Override
    protected Optional<String> getBeanModel() {
        return getRecordingManagerDevice();
    }
}

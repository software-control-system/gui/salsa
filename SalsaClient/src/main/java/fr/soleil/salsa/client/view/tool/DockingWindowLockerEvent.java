package fr.soleil.salsa.client.view.tool;

import java.util.EventObject;

/**
 * Event for docking window locker
 * 
 * @author madela
 */
public class DockingWindowLockerEvent extends EventObject {
    /** Serialization id */
    private static final long serialVersionUID = 3819379558586328058L;

    /** True if it is locked */
    private final boolean isLocked;

    /**
     * Constructs a DockingWindowLockerEvent object.
     * 
     * @param source the Object that is the source of the event(typically this)
     * @param isLocked true if it is locked
     */
    public DockingWindowLockerEvent(Object source, boolean isLocked) {
        super(source);
        this.isLocked = isLocked;
    }

    /**
     * Return true if it is locked
     * 
     * @return true if it is locked
     */
    public boolean isLocked() {
        return isLocked;
    }

    @Override
    public String toString() {
        return "DockingWindowLockerEvent [isLocked=" + isLocked + "]";
    }
}

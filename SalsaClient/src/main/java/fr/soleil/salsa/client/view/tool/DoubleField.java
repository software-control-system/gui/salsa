package fr.soleil.salsa.client.view.tool;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JTextField;

/**
 * 
 * @author Administrateur
 */
public class DoubleField extends JTextField {

    private static final long serialVersionUID = -7824261454572776062L;

    /**
     * The number format.
     */
    private NumberFormat format;

    /**
     * The locale to be used to format the number.
     */
    private Locale locale;

    /**
     * Construtor. Create a new DoubleField using the default locales and format.
     */
    public DoubleField() {
        this(null, Locale.getDefault(), NumberFormat.getNumberInstance());
    }

    /**
     * Construtor. Create a new DoubleField using the default locales and format and the indicated
     * number of fraction digits.
     */
    public DoubleField(int minimumFractionDigits, int maximumFractionDigits) {
        this();
        this.format.setMinimumFractionDigits(minimumFractionDigits);
        this.format.setMaximumFractionDigits(maximumFractionDigits);
    }

    /**
     * Constructor.
     * 
     * @param value the value.
     * @param locale the locale to be used.
     * @param format the number format to be used.
     */
    public DoubleField(Double value, Locale locale, NumberFormat format) {
        assert (locale != null);
        assert (format != null);
        this.format = format;
        this.locale = locale;
        this.setValue(value);
        this.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent focusEvent) {
                setValue(getValue());
            }
        });
    }

    /**
     * Returns the value as a double.
     * 
     * @return
     */
    public Double getValue() {
        Number number;
        Double value;
        String stringValue;
        try {
            stringValue = this.getText();
            stringValue = convertDecimalSymbol(stringValue);
            number = format.parse(stringValue);
            value = number.doubleValue();
        }
        catch (ParseException e) {
            // The user input is not a number, maybe blank.
            value = null;
        }
        return value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     */
    public void setValue(Double value) {
        String stringValue = value != null ? format.format(value) : "";
        this.setText(stringValue);
    }

    /**
     * Depending on country, the decimal separator might change. However, the decimal separator on
     * the numeric keypad is always a dot. So the user can be expected to use either the locale
     * decimal separator or a dot. The java DecimalFormatSymbols class only allow one decimal
     * symbol, though. This class converts the dot to the normal local decimal symbol, so the user
     * can use whichever he wants.
     * 
     * @param numberAsString
     * @return
     */
    private String convertDecimalSymbol(String numberAsString) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(locale);
        if (decimalFormatSymbols.getDecimalSeparator() != '.'
                && decimalFormatSymbols.getGroupingSeparator() != '.') {
            numberAsString = numberAsString
                    .replace('.', decimalFormatSymbols.getDecimalSeparator());
        }
        return numberAsString;
    }

    /**
     * The number format.
     * 
     * @return
     */
    public NumberFormat getFormat() {
        return format;
    }

    /**
     * The number format.
     */
    public void setFormat(NumberFormat format) {
        this.format = format;
    }

    /**
     * The locale to be used to format the number.
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * The locale to be used to format the number.
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}

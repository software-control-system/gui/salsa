package fr.soleil.salsa.client.view.tool;

import java.io.Serializable;

/**
 * A node in the configuration tree. It can be a configuration or a directory depending on the
 * concrete implementation.
 */
public abstract class ATreeNode implements Serializable {

    private static final long serialVersionUID = -145368371994617132L;

    /**
     * The directory wrapper containing this wrapper.
     */
    private DirectoryTreeNode parentDirectory;

    /**
     * Constructor.
     * 
     * @param parentDirectory the directory wrapper containing this wrapper.
     */
    public ATreeNode(DirectoryTreeNode parentDirectory) {
        super();
        this.parentDirectory = parentDirectory;
    }

    /**
     * The directory wrapper containing this wrapper.
     * 
     * @return the parentDirectory
     */
    public DirectoryTreeNode getParentDirectory() {
        return parentDirectory;
    }

}

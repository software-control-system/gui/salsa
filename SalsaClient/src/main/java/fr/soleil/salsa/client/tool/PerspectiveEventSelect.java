package fr.soleil.salsa.client.tool;

/**
 * Perspective event when selecting a perspective
 * 
 * @author madela
 */
public class PerspectiveEventSelect extends PerspectiveEvent {
    /** Serialization id */
    private static final long serialVersionUID = 3166012635739374489L;

    /**
     * Constructor
     * 
     * @param source the source of event
     * @param type the type of event
     * @param perspective the perspective
     * @param isDefaultPerspective True if it is event for a default perspective
     */
    public PerspectiveEventSelect(Object source, Perspective perspective, boolean isDefaultPerspective) {
        super(source, PerspectiveEvent.Type.SELECT, perspective, isDefaultPerspective);
    }

    @Override
    public String toString() {
        return "PerspectiveEventSelect [getType()=" + getType() + ", getPerspective()=" + getPerspective()
                + ", isDefaultPerspective()=" + isDefaultPerspective() + "]";
    }
}

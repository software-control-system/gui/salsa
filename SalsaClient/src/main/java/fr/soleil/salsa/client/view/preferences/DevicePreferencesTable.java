package fr.soleil.salsa.client.view.preferences;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import fr.soleil.salsa.client.view.preferences.devices.DeviceCell;

/**
 * Device preferences table
 * 
 * @author Patrick Madela
 */
public class DevicePreferencesTable extends JTable {
    /** Serialization id */
    private static final long serialVersionUID = 4324548274728176218L;

    /** Editing class */
    private Class<?> editingClass;

    /**
     * Create a new instance
     * 
     * @param tableModel the table model
     */
    public DevicePreferencesTable(TableModel tableModel) {
        super(tableModel);

    }

    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);

        if (modelColumn == 1) {
            Class<? extends Object> rowClass = getModel().getValueAt(row, modelColumn).getClass();
            return getDefaultRenderer(rowClass);
        }
        return super.getCellRenderer(row, column);
    }

    @Override
    public TableCellEditor getCellEditor(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);

        if (modelColumn == 1) {
            Object object = getModel().getValueAt(row, modelColumn);
            if (object instanceof DeviceCell) {
                return ((DeviceCell) object).getCellEditor();
            }
            editingClass = object.getClass();
            return getDefaultEditor(editingClass);
        }
        return super.getCellEditor(row, column);
    }

    // This method is also invoked by the editor when the value in the editor
    // component is saved in the TableModel. The class was saved when the
    // editor was invoked so the proper class can be created.

    @Override
    public Class<?> getColumnClass(int column) {
        return editingClass != null ? editingClass : super.getColumnClass(column);
    }
}
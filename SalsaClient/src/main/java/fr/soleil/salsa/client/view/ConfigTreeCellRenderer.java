package fr.soleil.salsa.client.view;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.soleil.salsa.client.view.tool.ConfigTreeNode;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IConfig.ScanType;

/**
 * A renderer for configuration tree
 * 
 * @author Alike
 */
public class ConfigTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -7968651492657265889L;

    /**
     * Configures the renderer based on the passed in components. The value is set from messaging
     * the tree with <code>convertValueToText</code>, which ultimately invokes <code>toString</code>
     * on <code>value</code>. The foreground color is set based on the selection and the icon is set
     * based on the <code>leaf</code> and <code>expanded</code> parameters.
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
            boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JLabel component = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded,
                leaf, row, hasFocus);
        if (value instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            String iconKey = "";
            if (node.getUserObject() != null && node.getUserObject() instanceof ConfigTreeNode) {
                IConfig<?> c = ((ConfigTreeNode) node.getUserObject()).getConfig();
                ScanType st = c.getType();
                if (st == ScanType.SCAN_1D) {
                    iconKey = "salsa.scanconfig.1d.big";
                }
                else if (st == ScanType.SCAN_2D) {
                    iconKey = "salsa.scanconfig.2d.big";
                }
                else if (st == ScanType.SCAN_K) {
                    iconKey = "salsa.scanconfig.k.big";
                }
                else if (st == ScanType.SCAN_ENERGY) {
                    iconKey = "salsa.scanconfig.energy.big";
                }
                else if (st == ScanType.SCAN_HCS) {
                    iconKey = "salsa.scanconfig.hcs.big";
                }
            }
            else {
                if (expanded) {
                    iconKey = "salsa.scanconfig.folder-open";
                }
                else {
                    iconKey = "salsa.scanconfig.folder";
                }
            }
            component.setIcon(Icons.getIcon(iconKey));
        }
        // if (value instanceof ATreeNode) {
        // String text = value.toString();
        // int textWidth = component.getFontMetrics(component.getFont()).stringWidth(text);
        // Dimension dimension = component.getPreferredSize();
        // dimension.setSize(textWidth, dimension.getHeight());
        // component.setPreferredSize(dimension);
        // }
        return component;
    }
}

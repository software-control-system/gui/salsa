package fr.soleil.salsa.client.controller.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.soleil.comete.bean.datastorage.helper.Parameters;
import fr.soleil.comete.bean.recordingmanagercontrolbean.RecordingManagerControlBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * The controller for recording control bean
 * 
 * @author madela
 */
public class RecordingControlController extends AbstractRecordingController {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(RecordingControlController.class);

    /** Recording manager control bean */
    private RecordingManagerControlBean recordingManagerControlBean;

    @Override
    protected Optional<AbstractTangoBox> getBean() {
        return Optional.of(getRecordingManagerControlBean());
    }

    private RecordingManagerControlBean getRecordingManagerControlBean() {
        String recordingManager = devicePreferences.getRecordingManager();
        String recordingManagerProfil = devicePreferences.getRecordingManagerProfil();
        Parameters.getInstance().setDevice(Optional.ofNullable(recordingManager));
        Parameters.getInstance().setProfile(Optional.ofNullable(recordingManagerProfil));
        if (recordingManagerControlBean == null) {
            recordingManagerControlBean = RecordingManagerControlBean.instance();
        }
        return recordingManagerControlBean;
    }

    @Override
    protected Optional<String> getBeanModel() {
        if (devicePreferences.isRecordingManagerDefined()) {
            return getRecordingManagerDevice();
        } else if (devicePreferences.isScanServerDefined()) {
            return getDataRecorderDevice();
        }
        return Optional.empty();
    }

    private Optional<String> getScanServerDevice() {
        String scanServerDevice = devicePreferences.getScanServer();
        return SalsaUtils.isDefined(scanServerDevice) ? Optional.of(scanServerDevice.trim()) : Optional.empty();
    }

    private Optional<String> getDataRecorderDevice() {
        Optional<String> optionalScanServerDevice = getScanServerDevice();
        if (optionalScanServerDevice.isPresent()) {
            String scanServerDevice = optionalScanServerDevice.get();
            return getDataRecorderProperty(scanServerDevice);
        }
        LOGGER.warn("Cannot get data recorder because scan server device is not defined in preference");
        return Optional.empty();
    }

    private Optional<String> getDataRecorderProperty(String scanServerDevice) {
        Optional<String> optional = null;
        try {
            Database database = TangoDeviceHelper.getDatabase();
            if (database != null) {
                DbDatum dbDatum = database.get_device_property(scanServerDevice, "DataRecorder");
                if (dbDatum != null) {
                    String value = dbDatum.extractString();
                    if (SalsaUtils.isDefined(value)) {
                        optional = Optional.of(value);
                    } else {
                        LOGGER.warn("Cannot get {}/DataRecorder property because is not defined", scanServerDevice);
                    }
                } else {
                    LOGGER.warn("Cannot get {}/DataRecorder property", scanServerDevice);
                }
            } else {
                LOGGER.warn("Cannot get {}/DataRecorder property because database is not defined", scanServerDevice);
            }
        } catch (DevFailed e) {
            LOGGER.error("Cannot get {}/DataRecorder property: {}", scanServerDevice, DevFailedUtils.toString(e));
            LOGGER.trace("Cannot get {}/DataRecorder property", scanServerDevice, e);
        }
        if (optional == null) {
            optional = Optional.empty();
        }
        return optional;
    }
}

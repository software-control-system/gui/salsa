package fr.soleil.salsa.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.AbstractCometeBox.LoggingMode;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.salsa.bean.BookmarksBean;
import fr.soleil.salsa.bean.SalsaTrajectoryBean;
import fr.soleil.salsa.client.controller.impl.ApplicationController;
import fr.soleil.salsa.client.preferences.UIPreferencesPersistence;
import fr.soleil.salsa.client.splash.SalsaSplash;
import fr.soleil.salsa.client.tool.PerspectivesManager;
import fr.soleil.salsa.client.view.ApplicationView;
import fr.soleil.salsa.exception.SalsaMissingPropertyException;
import fr.soleil.salsa.exception.SalsaPreferencesException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferencesPersistence;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Main class for Salsa Client.
 */
public class Salsa {
    /** The logger for messages */
    private static final Logger LOGGER = LoggingUtil.getLogger(Salsa.class);

    /** Salsa server database url property name */
    private static final String SALSA_SERVER_URL = "SALSA_SERVER_URL";

    /** Salsa server database login property name */
    private static final String SALSA_LOGIN = "SALSA_LOGIN";

    /** Salsa executable location */
    private static final String SALSA_LOCATION = "SALSA_LOCATION";

    /** Icons resources bundle of package */
    private static final Icons ICONS = new Icons("fr.soleil.salsa.client.view.icons.icons");

    /** List of application icons */
    private static final List<Image> ICONS_LIST = Collections.unmodifiableList(Arrays.asList(
            ICONS.getIcon("salsa.application.24").getImage(), ICONS.getIcon("salsa.application.32").getImage(),
            ICONS.getIcon("salsa.application.40").getImage(), ICONS.getIcon("salsa.application.48").getImage(),
            ICONS.getIcon("salsa.application.64").getImage(), ICONS.getIcon("salsa.application.72").getImage(),
            ICONS.getIcon("salsa.application.96").getImage()));

    /** Max value for splash progress */
    private static final int SPLASH_MAX_PROGRESS = 58;

    /** Splash initialized here in order to make it quickly visible */
    public static final SalsaSplash SPLASH;

    static {
        // SCAN-824
        SPLASH = new SalsaSplash(new ImageIcon(Salsa.class.getResource("splash/Salsa.jpg")), Color.WHITE);
        SPLASH.setMaxProgress(SPLASH_MAX_PROGRESS);
        SPLASH.progress(0);
        SPLASH.setTitle("SALSA");
        SPLASH.setCopyright("(c) Synchrotron Soleil 2011");
        SPLASH.addProgress("Starting application ...");
        SPLASH.setAlwaysOnTop(true);
    }

    private static JFrame salsaFrame;

    public static JFrame getSalsaFrame() {
        return salsaFrame;
    }

    private static void printCorrectUsageAndExit(final SalsaPreferencesException salsaPreferencesException) {
        StringBuilder builder = new StringBuilder("\n----- Wrong Salsa Parameters! -----\n");
        if (salsaPreferencesException != null) {
            builder.append("###############################\n");
            builder.append(salsaPreferencesException.getMessage());
            builder.append("\n###############################\n");
        }
        builder.append("Invalid Salsa Usage. Correct usage:\n");
        builder.append("java ");
        builder.append("-DSALSA_SERVER_URL=YourDataBaseAccess ");
        builder.append("-DSALSA_LOGIN=YourDataBaseUser ");
        builder.append("-D");
        builder.append(DevicePreferencesPersistence.PREFERENCE_FILE_PROPERTY);
        builder.append("=PathToDevicePreferencesFile ");
        builder.append("-D");
        builder.append(DevicePreferencesPersistence.SCANSERVER_PROPERTY);
        builder.append("=TheScanServerToUseIfDevicePreferencesFileDoesNotExist ");
        builder.append("-D");
        builder.append(UIPreferencesPersistence.PREFERENCE_FILE_PROPERTY);
        builder.append("=PathToGraphicalPreferencesFile ");
        builder.append("-D");
        builder.append(PerspectivesManager.SALSA_PERSPECTIVES_DIRECTORY);
        builder.append("=PathToPerspectivesDirectory ");
        builder.append(Salsa.class.getName());
        builder.append("\n-------------------------------\n");
        LOGGER.error("{}", builder);
        System.exit(1);
    }

    /**
     * Main method launches the application.
     *
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        String firstArgin = null;
        String secondArgin = null;
        String perspectiveName = null;
        String configName = null;
        String bookmark = null;
        boolean roMode = false;
        boolean resultMode = false;

        AbstractCometeBox.setLoggingMode(LoggingMode.CONSOLE);

        if (args != null) {
            if (args.length > 0) {
                firstArgin = args[0];
            }
            if (args.length > 1) {
                secondArgin = args[1];
            }
        }

        if (SalsaUtils.isDefined(firstArgin)) {
            // Launch Salsa read only
            if (firstArgin.trim().equalsIgnoreCase("ro")) {
                roMode = true;
                LOGGER.info("Salsa is launched in read only mode");
            }

            // Launch Salsa result
            else if (firstArgin.trim().equalsIgnoreCase("result")) {
                perspectiveName = "Scan";
                resultMode = true;
                LOGGER.info("Salsa is launched in result mode");
            }
            // Launch Salsa
            else {
                perspectiveName = firstArgin.trim();
            }
        }

        if (SalsaUtils.isDefined(secondArgin)) {
            if (roMode) {
                // it is a configuration name
                if (secondArgin.contains(File.separator)) {
                    configName = secondArgin.trim();
                } else {
                    bookmark = secondArgin.trim();
                }
            } else {
                configName = secondArgin.trim();
            }
        }

        LOGGER.trace("bookmark={}", bookmark);
        LOGGER.trace("configName={}", configName);

        checkMandatoryProperties();

        if (roMode) {
            launchSalsaReadOnly(configName, bookmark);
        } else {
            launchSalsa(configName, perspectiveName, resultMode);
        }
    }

    private static void launchSalsaReadOnly(String configName, String bookmark) {
        SPLASH.addProgress("Build Salsa read only panel");

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        SalsaTrajectoryBean bean = new SalsaTrajectoryBean();
        panel.add(bean, BorderLayout.CENTER);

        String salsaExecute = SystemUtils.getSystemProperty(SALSA_LOCATION);
        if (!SalsaUtils.isDefined(salsaExecute)) {
            salsaExecute = "salsa";
        }

        BatchExecutor salsaResult = new BatchExecutor();
        salsaResult.setBatch(salsaExecute);

        List<String> arguments = Arrays.asList("result");
        salsaResult.setBatchParameters(arguments);

        JButton showResult = new JButton("Scan result");
        showResult.setToolTipText("show current scan result");
        showResult.addActionListener(e -> salsaResult.execute());
        panel.add(showResult, BorderLayout.SOUTH);

        if (SalsaUtils.isDefined(configName)) {
            SPLASH.addProgress("Set configuration " + configName);
            LOGGER.info("Load configuration {}", configName);
            bean.setConfigPath(configName);
        } else {
            // display configuration combo
            SPLASH.addProgress("Create Bookmarks combo");
            BookmarksBean combo = new BookmarksBean();
            panel.add(combo, BorderLayout.NORTH);
            if (SalsaUtils.isDefined(bookmark)) {
                combo.setBookmark(bookmark);
                combo.setBookmaksComboVisible(false);
            }
            combo.addConfigSelectionListener(bean::setConfigPath);
        }
        bean.loadConfig();
        SPLASH.addProgress("Frame construction");
        StringBuilder titleBuilder = new StringBuilder("Salsa ");
        try {
            ResourceBundle bundle = ResourceBundle
                    .getBundle(ApplicationView.class.getPackage().getName() + ".application");
            titleBuilder.append(bundle.getString("project.version")).append(" (");
            titleBuilder.append(bundle.getString("build.date")).append(")");
        } catch (Exception e) {
            LOGGER.error("Cannot read device preferences {}", e.getMessage());
            LOGGER.trace("Cannot read device preferences", e);
        }

        titleBuilder.append(" read only");

        JFrame frame = new JFrame();
        salsaFrame = frame;
        frame.setIconImages(ICONS_LIST);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(titleBuilder.toString());
        panel.setSize(1200, 1000);
        frame.setContentPane(panel);
        frame.setSize(panel.getSize());
        SPLASH.addProgress("Frame display");
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        SPLASH.addProgress("Done");
        SPLASH.setVisible(false);
    }

    private static void launchSalsa(String configName, String perspectiveName, boolean resultMode) {
        SPLASH.addProgress(String.format(ApplicationController.BUILDING_CONTROLLER, "..."));
        ApplicationController applicationController = new ApplicationController(resultMode);
        salsaFrame = applicationController.getView();
        applicationController.getView().setIconImages(ICONS_LIST);

        SwingUtilities.invokeLater(() -> {
            try {
                SPLASH.addProgress("Loading preferences ...");
                applicationController.loadPreferences();
            } catch (SalsaPreferencesException e1) {
                LOGGER.error("Cannot load preferences {}", e1.getMessage());
                LOGGER.trace("Cannot load preferences", e1);
                printCorrectUsageAndExit(e1);
            }

            SPLASH.addProgress("Building GUI ...");
            applicationController.setViewVisible(true);
            SPLASH.toFront();

            try {
                SPLASH.addProgress("Loading perspective ...");
                applicationController.loadPerspective(perspectiveName);
            } catch (Exception e) {
                LOGGER.error("Cannot load perspective {} {} ", perspectiveName, e.getMessage());
                LOGGER.trace("Cannot load perspective", e);
            }

            SPLASH.addProgress("Set default configuration ...");
            applicationController.setConfigPath(configName);

            SPLASH.addProgress("Done");
            SPLASH.setVisible(false);
            applicationController.setViewToFront();
        });
    }

    private static void checkMandatoryProperties() {
        SPLASH.addProgress("Check mandatory Properties ...");
        checkMandatorySystemProperty(SALSA_SERVER_URL);
        checkMandatorySystemProperty(SALSA_LOGIN);
    }

    private static void checkMandatorySystemProperty(String propertyName) {
        String value = SystemUtils.getSystemProperty(propertyName);
        if (!SalsaUtils.isDefined(value)) {
            printCorrectUsageAndExit(new SalsaMissingPropertyException(propertyName));
        }
    }
}

package fr.soleil.salsa.client.view.tool;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IDirectory;

/**
 * Node for a directory in the configuration tree.
 * 
 * @author Administrateur
 * 
 */
public class DirectoryTreeNode extends ATreeNode implements Comparable<DirectoryTreeNode> {

    private static final long serialVersionUID = 2329830009233820677L;

    /**
     * The wrapped directory.
     */
    private IDirectory directory;

    /**
     * The subdirectories list.
     */
    private List<DirectoryTreeNode> subDirectoriesList;

    /**
     * The config list.
     */
    private List<ConfigTreeNode> configsList;

    /**
     * Constructor for the root directory wrapper.
     * 
     * @param directory
     */
    public DirectoryTreeNode(IDirectory directory) {
        this(directory, null);
    }

    /**
     * Constructor.
     * 
     * @param config the wrapped config.
     * @param the directory wrapper containing this wrapper.
     */
    public DirectoryTreeNode(IDirectory directory, DirectoryTreeNode parentDirectory) {
        super(parentDirectory);
        assert directory != null;
        this.directory = directory;
        if (directory != null) {
            List<? extends IDirectory> unwrappedSubDirectoriesList = directory
                    .getSubDirectoriesList();

            this.subDirectoriesList = new ArrayList<DirectoryTreeNode>(
                    unwrappedSubDirectoriesList.size());
            for (IDirectory subDirectory : unwrappedSubDirectoriesList) {
                this.subDirectoriesList.add(new DirectoryTreeNode(subDirectory, this));
            }

            // Collections.sort(this.subDirectoriesList);

            List<? extends IConfig<?>> unwrappedConfigList = directory.getConfigList();
            this.configsList = new ArrayList<ConfigTreeNode>(unwrappedConfigList.size());
            for (IConfig<?> config : unwrappedConfigList) {
                this.configsList.add(new ConfigTreeNode(config, this));
            }
            // Collections.sort(this.configsList);
        }
    }

    /**
     * The wrapped directory.
     * 
     * @return the directory
     */
    public IDirectory getDirectory() {
        return directory;
    }

    /**
     * The wrapped directory.
     * 
     * @return the directory
     */
    public void setDirectory(IDirectory directory) {
        assert directory != null;
        this.directory = directory;
    }

    /**
     * The subdirectories list.
     * 
     * @return the subDirectoriesList
     */
    public List<DirectoryTreeNode> getSubDirectoriesList() {
        return subDirectoriesList;
    }

    /**
     * The subdirectories list.
     * 
     * @param subDirectoriesList the subDirectoriesList to set
     */
    public void setSubDirectoriesList(List<DirectoryTreeNode> subDirectoriesList) {
        this.subDirectoriesList = subDirectoriesList;
    }

    /**
     * The config list.
     * 
     * @return the configsList
     */
    public List<ConfigTreeNode> getConfigsList() {
        return configsList;
    }

    /**
     * The config list.
     * 
     * @param configsList the configsList to set
     */
    public void setConfigsList(List<ConfigTreeNode> configsList) {
        this.configsList = configsList;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return directory.getName();
    }

    /**
     * @see Comparable#compareTo(Object)
     * @param o
     * @return
     */
    @Override
    public int compareTo(DirectoryTreeNode o) {
        return this.getDirectory().getPositionInDirectory()
                .compareTo(o.getDirectory().getPositionInDirectory());
    }

    /**
     * @see Object#equals(Object)
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if (obj == null) {
            return false;
        }
        else if (obj instanceof DirectoryTreeNode) {
            DirectoryTreeNode that = (DirectoryTreeNode) obj;
            return this.directory.equals(that.directory);
        }
        else {
            return false;
        }
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.directory.hashCode();
    }
}

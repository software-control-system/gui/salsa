package fr.soleil.salsa.client.view.tool;

public class AboutCommons {
    public static final String copyright = "Synchrotron Soleil Software";
    public static final String jingle = "Let's scan TANGO";
    public static final String applicationName = "Salsa v3";
    public static final String aboutTitle = "About Salsa";
    public static final String logo = "/fr/soleil/salsa/client/view/icons/alpha-logo-soleil-350x175.png";
}

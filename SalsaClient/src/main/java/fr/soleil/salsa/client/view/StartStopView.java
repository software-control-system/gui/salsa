package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import fr.soleil.comete.bean.recordingmanageracknowledgebean.RecordingManagerAcknowledgeBean;
import fr.soleil.salsa.bean.ISalsaActionBeanListener;
import fr.soleil.salsa.bean.PauseButton;
import fr.soleil.salsa.bean.RecordDataPanel;
import fr.soleil.salsa.bean.ResumeButton;
import fr.soleil.salsa.bean.StartButton;
import fr.soleil.salsa.bean.StopButton;
import fr.soleil.salsa.client.controller.impl.StartStopController;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * The start / stop frame.
 * 
 * @author Katy Saintin
 * 
 */
public class StartStopView extends JToolBar implements IView<StartStopController>, ISalsaActionBeanListener {
    /** Serialization id */
    private static final long serialVersionUID = 3981113784994014347L;

    /** True if read only */
    boolean isReadOnly;

    /** The Start Button */
    private StartButton startButton = null;

    /** The Stop Button */
    private StopButton stopButton = null;

    /** The Pause Button */
    private PauseButton pauseButton = null;

    /** The Resume Button */
    private ResumeButton resumeButton = null;

    /** Recording manager acknowledge bean */
    private RecordingManagerAcknowledgeBean recordingManagerAcknowledgeBean;

    /** The dataRecorderOn */
    private RecordDataPanel recordDataPanel = null;

    /** The controller */
    private transient StartStopController controller;

    /**
     * Create a new instance
     */
    public StartStopView() {
        this(null, false);
    }

    /**
     * Create a new instance
     * 
     * @param controller the controller
     * @param ro read only if true
     */
    public StartStopView(StartStopController controller, boolean ro) {
        this.isReadOnly = ro;
        initialize();
        setController(controller);
    }

    @Override
    public StartStopController getController() {
        return controller;
    }

    @Override
    public void setController(StartStopController controller) {
        this.controller = controller;
        if (startButton != null) {
            startButton.loadConfig();
        }
        stopButton.loadConfig();
        pauseButton.loadConfig();
        resumeButton.loadConfig();
    }

    /**
     * Set data recorder visible if true
     * 
     * @param visible visible if true
     */
    public void setDataRecorderVisible(boolean visible) {
        if (recordDataPanel != null) {
            recordDataPanel.setVisible(visible);
        }
    }

    /**
     * Set config
     * 
     * @param config the config
     */
    public void setConfig(IConfig<?> config) {
        if (startButton != null) {
            startButton.setConfig(config);
        }
    }

    /**
     * Set data recorder enable
     * 
     * @param enable enable data recorder if true
     */
    public void setDataRecorderEnable(boolean enable) {
        if (recordDataPanel != null) {
            recordDataPanel.setSelected(enable);
        }
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        setLayout(new BorderLayout());
        JPanel mainPanel = new JPanel();
        add(mainPanel, BorderLayout.WEST);
        if (!isReadOnly) {
            startButton = new StartButton();
            startButton.addSalsaBeanListener(this);
            mainPanel.add(startButton);
        }

        pauseButton = new PauseButton();
        pauseButton.addSalsaBeanListener(this);

        mainPanel.add(pauseButton);

        resumeButton = new ResumeButton();
        resumeButton.addSalsaBeanListener(this);
        mainPanel.add(resumeButton);

        stopButton = new StopButton();
        stopButton.addSalsaBeanListener(this);
        mainPanel.add(stopButton);

        addSeparator();
        add(getRecordingManagerAckBean());

        if (!isReadOnly) {
            recordDataPanel = new RecordDataPanel();
            recordDataPanel.setVisible(true);
            add(recordDataPanel, BorderLayout.EAST);
        }

        this.setBorder(BorderFactory.createRaisedBevelBorder());
        this.setFloatable(false);
    }

    public void setDevicePreferences(DevicePreferences devicePreferences) {
        if (recordDataPanel != null) {
            recordDataPanel.updateFromPreferences(devicePreferences);
        }
    }

    /**
     * Get recording manager acknowledge bean
     * 
     * @return recording manager acknowledge bean
     */
    public RecordingManagerAcknowledgeBean getRecordingManagerAckBean() {
        if (recordingManagerAcknowledgeBean == null) {
            recordingManagerAcknowledgeBean = RecordingManagerAcknowledgeBean.instance();
            recordingManagerAcknowledgeBean.setOpaque(false);
        }
        return recordingManagerAcknowledgeBean;
    }

    @Override
    public void configPathChanged(String configPath) {
        // Do nothing
    }

    @Override
    public void loadingConfigSucceed(IConfig<?> config) {
        // Do nothing
    }

    @Override
    public void loadingConfigFailed(String configPath, SalsaException exception) {
        // Do nothing
    }

    @Override
    public void actionPerformed(Action action) {
        // Do nothing
    }

    @Override
    public void executedActionSucceed(Action action) {
        // Do nothing
    }

    @Override
    public void executedActionFailed(Action action, SalsaException exception) {
        JXErrorPane.showDialog(this, new ErrorInfo("Execution error", "Failed to execute " + action,
                exception.getMessage(), "SalsaAPI", exception, Level.WARNING, null));

    }

}

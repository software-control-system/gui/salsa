package fr.soleil.salsa.client.view;

import javax.swing.JToolBar;

import fr.soleil.salsa.client.controller.impl.GeneralShortcutsToolBarController;

public class GeneralShortcutsToolBarView extends JToolBar implements IView<GeneralShortcutsToolBarController> {

    private static final long serialVersionUID = 8459584819264713074L;

    /**
     * The controller.
     */
    private GeneralShortcutsToolBarController generalShortcutsToolBar;

    public GeneralShortcutsToolBarView() {
        this(null);
    }

    /**
     * Constructor.
     * 
     * @param shortcutsController
     */
    public GeneralShortcutsToolBarView(GeneralShortcutsToolBarController generalShortcutsToolBar) {
        this.generalShortcutsToolBar = generalShortcutsToolBar;
    }

    @Override
    public GeneralShortcutsToolBarController getController() {
        return generalShortcutsToolBar;
    }

    @Override
    public void setController(GeneralShortcutsToolBarController controller) {
        this.generalShortcutsToolBar = controller;
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    public void initialize() {
        this.setFloatable(false);
    }

}

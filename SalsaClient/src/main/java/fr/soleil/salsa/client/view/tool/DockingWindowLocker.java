package fr.soleil.salsa.client.view.tool;

import java.io.Serializable;
import java.util.stream.Stream;

import org.slf4j.Logger;

import com.google.common.eventbus.EventBus;

import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.EventBusHandler;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.WindowPopupMenuFactory;
import net.infonode.docking.properties.DockingWindowProperties;
import net.infonode.docking.util.ViewMap;

/**
 * Locker for docking window
 * 
 * @author madela
 */
public class DockingWindowLocker implements Serializable {
    /** Serialization id */
    private static final long serialVersionUID = -2542979842784838651L;

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(DockingWindowLocker.class);

    /** The root docking window */
    private final DockingWindow rootWindow;

    /** The window popup menu factory */
    private final transient WindowPopupMenuFactory popupMenuFactory;

    /** The viewMap for locking hidden view */
    private final transient ViewMap viewMap;

    /** The application bus event */
    private final transient EventBus eventBus;

    /**
     * Create a new instance
     * 
     * @param rootWindow the root docking window
     * @param viewMap the viewMap for locking hidden view
     * @param eventBus the application event bus
     */
    DockingWindowLocker(DockingWindow rootWindow, ViewMap viewMap, EventBus eventBus) {
        this.rootWindow = rootWindow;
        this.popupMenuFactory = rootWindow.getPopupMenuFactory();
        this.viewMap = viewMap;
        this.eventBus = eventBus;
    }

    /**
     * Create a new instance of {@link DockingWindowLocker}
     * 
     * @param rootWindow the root docking window
     * @param viewMap the viewMap for locking hidden view
     * @return new instance of {@link DockingWindowLocker}
     */
    public static DockingWindowLocker instance(DockingWindow rootWindow, ViewMap viewMap) {
        return new DockingWindowLocker(rootWindow, viewMap, EventBusHandler.getEventBus());
    }

    /**
     * Notifies all {@link DockingWindowLockerListener}s for some changes
     */
    private void fireChange() {
        DockingWindowLockerEvent dockingWindowLockerEvent = new DockingWindowLockerEvent(this, isLocked());
        eventBus.post(dockingWindowLockerEvent);
    }

    /**
     * Return true if all docking windows are locked
     * 
     * @return true if all docking windows are locked
     */
    public boolean isLocked() {
        boolean isLocked = getDockingWindows(rootWindow).noneMatch(w -> w.getWindowProperties().getCloseEnabled());
        LOGGER.debug("Docking window is locked: {}", isLocked);
        return isLocked;
    }

    /**
     * Locked if set true
     * 
     * @param locked Locked if set true
     */
    public void setLock(boolean locked) {
        if (locked) {
            lock();
        } else {
            unlock();
        }
    }

    /**
     * Lock all docking windows
     */
    public void lock() {
        getDockingWindows(rootWindow).forEach(w -> setEnable(w, false));
        rootWindow.setPopupMenuFactory(null);
        fireChange();
    }

    /**
     * Unlock all docking windows
     */
    public void unlock() {
        getDockingWindows(rootWindow).forEach(w -> setEnable(w, true));
        rootWindow.setPopupMenuFactory(popupMenuFactory);
        fireChange();
    }

    private synchronized Stream<DockingWindow> getDockingWindows(DockingWindow window) {
        return Stream.concat(DockingWindowWalker.walk(window), DockingWindowWalker.walk(viewMap)).distinct();
    }

    private void setEnable(DockingWindow window, boolean isEnable) {
        if (window != null) {
            if (isEnable) {
                LOGGER.debug("Unlock {} {}", window.getClass().getSimpleName(), window.getTitle());
            } else {
                LOGGER.debug("Lock {} {}", window.getClass().getSimpleName(), window.getTitle());
            }
            DockingWindowProperties windowProperty = window.getWindowProperties();
            windowProperty.setCloseEnabled(isEnable);
            windowProperty.setDockEnabled(isEnable);
            windowProperty.setUndockEnabled(isEnable);
            windowProperty.setDragEnabled(isEnable);
            windowProperty.setMaximizeEnabled(isEnable);
            windowProperty.setMinimizeEnabled(isEnable);
            windowProperty.setRestoreEnabled(isEnable);
        }
    }
}

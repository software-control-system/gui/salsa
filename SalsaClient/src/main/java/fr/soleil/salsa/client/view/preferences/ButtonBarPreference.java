/**
 * 
 */
package fr.soleil.salsa.client.view.preferences;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;

import com.l2fprod.common.swing.JButtonBar;
import com.l2fprod.common.swing.plaf.blue.BlueishButtonBarUI;

/***
 * @author Tarek
 */
public class ButtonBarPreference extends JPanel {

    private static final long serialVersionUID = -9199640446785096509L;

    private Component currentComponent;
    private JButtonBar toolbar;
    private ButtonGroup group;

    public ButtonBarPreference() {
        toolbar = new JButtonBar(JButtonBar.VERTICAL);

        toolbar.setUI(new BlueishButtonBarUI());

        setLayout(new BorderLayout());

        add("West", toolbar);

        group = new ButtonGroup();

    }

    /**
     * @param toolbar
     * @param group
     */
    public void addPreference(Preference pref) {
        addButton(pref.getIconTitle(), pref.getIconUrl(), makePanel(pref.getTitle(), pref
                .getComponent()), toolbar, group);
    }

    private JPanel makePanel(String title, Component propertyPanel) {
        JPanel panel = new JPanel(new BorderLayout());
        JLabel top = new JLabel(title);
        top.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        top.setFont(top.getFont().deriveFont(Font.BOLD));
        top.setOpaque(true);
        top.setBackground(panel.getBackground().brighter());
        panel.add("North", top);

        panel.add(BorderLayout.CENTER, propertyPanel);
        panel.setPreferredSize(new Dimension(400, 300));
        panel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

        return panel;
    }

    private void show(Component component) {
        if (currentComponent != null) {
            remove(currentComponent);
        }
        add("Center", currentComponent = component);
        revalidate();
        repaint();
    }

    private void addButton(String title, String iconUrl, final Component component, JButtonBar bar,
            ButtonGroup group) {
        Action action = new AbstractAction(title, new ImageIcon(ButtonBarPreference.class
                .getResource(iconUrl))) {
            private static final long serialVersionUID = 6782752452097256282L;

            public void actionPerformed(ActionEvent e) {
                show(component);
            }
        };

        JToggleButton button = new JToggleButton(action);
        bar.add(button);

        group.add(button);

        if (group.getSelection() == null) {
            button.setSelected(true);
            show(component);
        }
    }

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        JFrame frame = new JFrame();
        frame.setSize(460, 347);
        ButtonBarPreference pp = new ButtonBarPreference();
        frame.setContentPane(pp);
        frame.setVisible(true);
    }

}
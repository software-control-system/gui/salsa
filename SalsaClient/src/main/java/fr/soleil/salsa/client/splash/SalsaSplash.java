package fr.soleil.salsa.client.splash;

import java.awt.Color;

import javax.swing.ImageIcon;

import org.slf4j.Logger;

import fr.soleil.lib.project.swing.Splash;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * Extended splash with logging of progress message for debug
 * 
 * @author madela
 */
public class SalsaSplash extends Splash {
    /** Serialization id */
    private static final long serialVersionUID = 9178334326036737422L;

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(SalsaSplash.class);

    /** Start time at startup */
    private long startTime = System.currentTimeMillis();

    /** time used for measuring duration between 2 progress */
    private long lastTime = startTime;

    /**
     * Create new instance
     * 
     * @param imageIcon the image icon
     * @param color the color
     */
    public SalsaSplash(ImageIcon imageIcon, Color color) {
        super(imageIcon, color);
    }

    /**
     * Increment progress and display the specified message
     * 
     * @param message the message
     */
    public void addProgress(String message) {
        addProgress(message, null);
    }

    /**
     * Increment progress and display the specified message
     * 
     * @param message the message displayed in splash screen
     * @param source the source of message for debug log
     */
    public void addProgress(String message, String source) {
        addProgress(1, message, source);
    }

    /**
     * Increment progress to specified delta and display the specified message
     * 
     * @param delta the delta to increment to progress
     * @param message the message displayed in splash screen
     * @param source the source of message for debug log
     */
    public void addProgress(int delta, String message, String source) {
        progress(getProgress() + delta);
        setMessage(message, source);
    }

    @Override
    public void setMessage(String message) {
        setMessage(message, null);
    }

    /**
     * Set message with potential logging with source
     * 
     * @param message the progress message to display
     * @param source the source of message for logging
     */
    public void setMessage(String message, String source) {
        if (isVisible()) {
            super.setMessage(message);
            LOGGER.debug("{}/{} - {}% - {} ms - {} ms - {} - {}", getProgress(), getMaximum(), getPercentage(),
                    totalElapsed(), elapsed(), getSource(source), message);
        }
    }

    private String getSource(String source) {
        if (source == null) {
            for (StackTraceElement e : Thread.currentThread().getStackTrace()) {
                String className = e.getClassName();
                if (!Thread.class.getName().equals(className) && !this.getClass().getName().equals(className)) {
                    return e.getClassName() + "." + e.getMethodName();
                }
            }
            return "unknow";
        }
        return source;
    }

    private long elapsed() {
        long elapsed = System.currentTimeMillis() - lastTime;
        resetTime();
        return elapsed;
    }

    private long totalElapsed() {
        return System.currentTimeMillis() - startTime;
    }

    private void resetTime() {
        lastTime = System.currentTimeMillis();
    }

    private int getProgress() {
        return getProgressBar().getValue();
    }

    private int getMaximum() {
        return getProgressBar().getMaximum();
    }

    private int getPercentage() {
        return 100 * getProgress() / getMaximum();
    }
}

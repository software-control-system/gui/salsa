package fr.soleil.salsa.client.controller.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;

import fr.soleil.comete.bean.datafitter.DataFitterBean;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.api.ScanApi;
import fr.soleil.salsa.client.Salsa;
import fr.soleil.salsa.client.controller.AErrorManagingController;
import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.client.controller.event.ControllerEvent;
import fr.soleil.salsa.client.controller.impl.tool.MenuBarControllerTool;
import fr.soleil.salsa.client.controller.listener.IControllerListener;
import fr.soleil.salsa.client.exception.SalsaPerspectiveException;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.preferences.UIPreferencesPersistence;
import fr.soleil.salsa.client.tool.Perspective;
import fr.soleil.salsa.client.tool.PerspectivesManager;
import fr.soleil.salsa.client.view.ApplicationView;
import fr.soleil.salsa.client.view.tool.WindowLayout;
import fr.soleil.salsa.config.manager.ConfigManager;
import fr.soleil.salsa.configuration.controller.impl.ConfigTreeController;
import fr.soleil.salsa.configuration.listener.IConfigSelectionListener;
import fr.soleil.salsa.configuration.view.ConfigTreeView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IContext;
import fr.soleil.salsa.entity.IDimension;
import fr.soleil.salsa.entity.impl.ContextImpl;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.exception.SalsaPreferencesException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.preferences.DevicePreferencesPersistence;
import fr.soleil.salsa.tool.EventBusHandler;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * The controller for the main window.
 * 
 * @author Administrateur
 * 
 */
public class ApplicationController extends AErrorManagingController<ApplicationView>
        implements IController<ApplicationView>, IControllerListener, IConfigSelectionListener {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(ApplicationController.class);

    /** The number of controller */
    public static final int NUMBER_OF_CONTROLLERS = 22;

    /** The string format splash frame when building controller */
    public static final String BUILDING_CONTROLLER = "Building controller %s";

    /** The string format splash frame when building controller with number */
    public static final String BUILDING_CONTROLLER_WITH_NUMBER = String.format(BUILDING_CONTROLLER, "%d/%d: %s");

    /** The controller for the main tool bar shortcuts. */
    private GeneralShortcutsToolBarController generalShortcutsToolBarController;

    /** The controller for the start / stop frame. */
    private StartStopController startStopController;

    /** The controller for shortcuts toolbar. */
    private ShortcutsController shortcutsController;

    /** The controller for the actuator list view. */
    private GenericActuatorListController actuatorsListController;

    /** The controller for the Y actuator list view. */
    private GenericActuatorListController yActuatorListController;

    /** The controller for the sensor list view. */
    private GenericSensorListController sensorsListController;

    /** The controller for the configuration tree view. */
    private ConfigTreeController configTreeController;

    /** The controller for the historic log view. */
    private HistoricLogController historicLogController;

    /** The controller for the timeBase view */
    private GenericTimebaseListController timebaseListController;

    /** The controller for the error strategies */
    private ErrorStrategiesController errorStrategiesController;

    /** The controller for the scan K trajectory view. */
    private ScanKTrajectoryController scanKTrajectoryController;

    /** The controller for the X trajectory view. */
    private TrajectoryController xTrajectoryController;

    /** The controller for the Y trajectory view. */
    private TrajectoryController yTrajectoryController;

    /** The configuration currently being displayed. */
    private ScanEnergyController scanEnergyController;

    /** The controller for the hook view. */
    private HookCommandController hooksController;

    /** The controller for the general. */
    private GeneralController generalController;

    /** DataFitter management is delegated to this object. */
    private DataFitterController dataFitterController;

    /** Controller for recording control */
    private RecordingControlController recordingControlController;

    /** Recording project management is delegated to this object. */
    private RecordingProjectController recordingProjectController;

    /** Recording counters management is delegated to this object. */
    private RecordingCountersController recordingCountersController;

    /** Recording extensions management is delegated to this object. */
    private RecordingExtensionController recordingExtensionController;

    /** The configuration currently being displayed. */
    private IConfig<? extends IDimension> config;

    /** This controller delegates control of the menu bar to this tool. (@see MenuBarControllerTool) */
    private MenuBarControllerTool menuBarControllerTool;

    /** Configuration management is delegated to this object. */
    private ConfigManager configManager;

    /** Perspective management is delegated to this object. */
    private final PerspectivesManager perspectivesManager;

    /** Initialization progress for splash progress bar */
    private int initialized;

    /**
     * Default Constructor.
     */
    public ApplicationController() {
        this(false);
    }

    /**
     * Constructor
     * 
     * @param scanResult application launched in result mode if true
     */
    public ApplicationController(boolean scanResult) {
        this(null, scanResult);
    }

    /**
     * Constructor
     * 
     * @param view the application view
     * @param scanResult application launched in result mode if true
     */
    public ApplicationController(ApplicationView view, boolean scanResult) {
        super(view, new ErrorController(), scanResult);
        this.perspectivesManager = PerspectivesManager.instance();
        EventBusHandler.getEventBus().register(this);
    }

    // SCAN-824
    protected void notifySalsaSplash(String name) {
        String message = String.format(BUILDING_CONTROLLER_WITH_NUMBER, ++initialized, NUMBER_OF_CONTROLLERS, name);
        String source = Thread.currentThread().getStackTrace()[2].getClassName() + "."
                + Thread.currentThread().getStackTrace()[2].getMethodName();
        Salsa.SPLASH.addProgress(message, source);
    }

    @Override
    protected void initBeforeSetView() {
        super.initBeforeSetView();

        getConfigManager();
        getStartStopController();
        getShortcutsController();
        getGeneralShortcutsToolBarController();
        getSensorsListController();
        getActuatorsListController();
        getTimebaseListController();
        getErrorStrategiesController();
        getGeneralController();
        getScanKTrajectoryController();
        getScanEnergyController();
        getXTrajectoryController();
        getYTrajectoryController();
        getYActuatorListController();
        getHooksController();
        getHistoricLogController();
        getDataFitterController();
        getConfigTreeController();
        getRecordingControlController();
        getRecordingProjectController();
        getRecordingCountersController();
        getRecordingExtensionController();
    }

    private ConfigManager getConfigManager() {
        if (configManager == null) {
            notifySalsaSplash("Config Manager");
            configManager = new ConfigManager();
        }
        return configManager;
    }

    public StartStopController getStartStopController() {
        if (startStopController == null) {
            notifySalsaSplash("Start Stop");
            startStopController = new StartStopController(this, errorController, readOnly);
        }
        return startStopController;
    }

    public ShortcutsController getShortcutsController() {
        if (shortcutsController == null) {
            notifySalsaSplash("Shortcuts");
            shortcutsController = new ShortcutsController(this);
        }
        return shortcutsController;
    }

    public GeneralShortcutsToolBarController getGeneralShortcutsToolBarController() {
        if (generalShortcutsToolBarController == null) {
            notifySalsaSplash("General Shorcuts");
            generalShortcutsToolBarController = new GeneralShortcutsToolBarController();
        }
        return generalShortcutsToolBarController;
    }

    public GenericSensorListController getSensorsListController() {
        if (sensorsListController == null) {
            notifySalsaSplash("Sensors");
            sensorsListController = new GenericSensorListController(null, errorController);
        }
        return sensorsListController;
    }

    public GenericActuatorListController getActuatorsListController() {
        if (actuatorsListController == null) {
            notifySalsaSplash("Actuators");
            actuatorsListController = new GenericActuatorListController(null, errorController);
            actuatorsListController.addControllerListener(this);
        }
        return actuatorsListController;
    }

    public GenericTimebaseListController getTimebaseListController() {
        if (timebaseListController == null) {
            notifySalsaSplash("Timebases");
            timebaseListController = new GenericTimebaseListController(null, errorController);
        }
        return timebaseListController;
    }

    public ErrorStrategiesController getErrorStrategiesController() {
        if (errorStrategiesController == null) {
            notifySalsaSplash("Error Strategies");
            errorStrategiesController = new ErrorStrategiesController();
        }
        return errorStrategiesController;
    }

    public GeneralController getGeneralController() {
        if (generalController == null) {
            notifySalsaSplash("General");
            generalController = new GeneralController();
        }
        return generalController;
    }

    public ScanKTrajectoryController getScanKTrajectoryController() {
        if (scanKTrajectoryController == null) {
            notifySalsaSplash("Scan K");
            scanKTrajectoryController = new ScanKTrajectoryController();
            scanKTrajectoryController.addControllerListener(this);
        }
        return scanKTrajectoryController;
    }

    public ScanEnergyController getScanEnergyController() {
        if (scanEnergyController == null) {
            notifySalsaSplash("Scan Energy");
            scanEnergyController = new ScanEnergyController();
            scanEnergyController.addControllerListener(this);
        }
        return scanEnergyController;
    }

    public TrajectoryController getXTrajectoryController() {
        if (xTrajectoryController == null) {
            notifySalsaSplash("Trajectory X");
            xTrajectoryController = new TrajectoryController();
            xTrajectoryController.addControllerListener(this);
        }
        return xTrajectoryController;
    }

    public TrajectoryController getYTrajectoryController() {
        if (yTrajectoryController == null) {
            notifySalsaSplash("Trajectory Y");
            yTrajectoryController = new TrajectoryController(true);
            yTrajectoryController.addControllerListener(this);
        }
        return yTrajectoryController;
    }

    public GenericActuatorListController getYActuatorListController() {
        if (yActuatorListController == null) {
            notifySalsaSplash("Actuators Y");
            yActuatorListController = new GenericActuatorListController(null, errorController);
            yActuatorListController.setYActuator(true);
        }
        return yActuatorListController;
    }

    public HookCommandController getHooksController() {
        if (hooksController == null) {
            notifySalsaSplash("Hooks");
            hooksController = new HookCommandController();
        }
        return hooksController;
    }

    public HistoricLogController getHistoricLogController() {
        if (historicLogController == null) {
            notifySalsaSplash("Historic Log");
            historicLogController = new HistoricLogController();
        }
        return historicLogController;
    }

    public DataFitterController getDataFitterController() {
        if (dataFitterController == null) {
            notifySalsaSplash("Data Fitter");
            dataFitterController = new DataFitterController(new DataFitterBean());
        }
        return dataFitterController;
    }

    public ConfigTreeController getConfigTreeController() {
        if (configTreeController == null) {
            notifySalsaSplash("Config Tree");
            configTreeController = new ConfigTreeController(errorController);
            configTreeController.addConfigSelectionListener(this);
        }
        return configTreeController;
    }

    public AbstractRecordingController getRecordingControlController() {
        if (recordingControlController == null) {
            notifySalsaSplash("Recording Control");
            recordingControlController = new RecordingControlController();
        }
        return recordingControlController;
    }

    public RecordingProjectController getRecordingProjectController() {
        if (recordingProjectController == null) {
            notifySalsaSplash("Recording Project");
            recordingProjectController = new RecordingProjectController();
        }
        return recordingProjectController;
    }

    public RecordingCountersController getRecordingCountersController() {
        if (recordingCountersController == null) {
            notifySalsaSplash("Recording Counters");
            recordingCountersController = new RecordingCountersController();
        }
        return recordingCountersController;
    }

    public RecordingExtensionController getRecordingExtensionController() {
        if (recordingExtensionController == null) {
            notifySalsaSplash("Recording Extension");
            recordingExtensionController = new RecordingExtensionController();
        }
        return recordingExtensionController;
    }

    public MenuBarControllerTool getMenuBarControllerTool() {
        if (menuBarControllerTool == null) {
            menuBarControllerTool = new MenuBarControllerTool(this);
        }
        return menuBarControllerTool;
    }

    @Override
    public void setErrorController(IErrorController errorController) {
        super.setErrorController(errorController);
        if (view != null) {
            view.setErrorView(errorController == null ? null : errorController.getView());
        }
        getStartStopController().setErrorController(errorController);
        getSensorsListController().setErrorController(errorController);
        getActuatorsListController().setErrorController(errorController);
        getTimebaseListController().setErrorController(errorController);
        getYActuatorListController().setErrorController(errorController);
    }

    @Override
    protected ApplicationView generateView() {
        LOGGER.trace("Create ApplicationView in read only ={}", readOnly);
        Salsa.SPLASH.addProgress(String.format(ApplicationView.BUILDING_VIEW, "..."));
        ApplicationView view = ApplicationView.instance(this, readOnly);
        Salsa.SPLASH.addProgress(String.format(ApplicationView.BUILDING_VIEW, "done!"));
        return view;
    }

    @Override
    public void errorMessage(String message) {
        super.errorMessage(message);
    }

    /**
     * The configuration currently being displayed.
     * 
     * @return the config
     */
    public IConfig<? extends IDimension> getConfig() {
        return config;
    }

    /**
     * Gets the current window layout. The window layout is the position and
     * state of the views in the application.
     * 
     * @return
     * @throws SalsaPerspectiveException
     */
    public WindowLayout getWindowLayout() throws SalsaPerspectiveException {
        return this.view.getWindowLayout();
    }

    /**
     * Hides the actuators view.
     */
    public void hideActuatorListView() {
        view.hideActuatorListView();
    }

    /**
     * Hides the configuration tree view.
     */
    public void hideConfigTreeView() {
        view.hideConfigTreeView();
    }

    /**
     * Hides display manager view.
     */
    public void hideDisplayManagerView() {
        view.hideDisplayManagerDockingView();
    }

    /**
     * Hides the error strategy view.
     */
    public void hideErrorStrategiesView() {
        view.hideErrorStrategiesView();
    }

    /**
     * Hides the scan energy view.
     */
    public void hideScanEnergyView() {
        view.hideScanEnergyView();
    }

    /**
     * Hides the Scan K Trajectory view.
     */
    public void hideScanKTrajectoryView() {
        view.hideScanKTrajectoryView();
    }

    /**
     * Hides the scan result view.
     */
    public void hideScanResultsView() {
        view.hideScanResultView();
    }

    /**
     * Hides the sensors view.
     */
    public void hideSensorListView() {
        view.hideSensorListView();
    }

    /**
     * Hides the trajectories view.
     */
    public void hideTimebasesView() {
        view.hideTimebasesView();
    }

    /**
     * Hides the X trajectories 2D view.
     */
    public void hideXTrajectories2DView() {
        view.hideXTrajectories2DView();
    }

    /**
     * Hides the actuators view.
     */
    public void hideYActuatorListView() {
        view.hideYActuatorListView();
    }

    /**
     * Hides the Y trajectories 2D view.
     */
    public void hideYTrajectories2DView() {
        view.hideYTrajectories2DView();
    }

    /**
     * Hides the configuration manager view.
     */
    public void hideConfigurationManagerView() {
        view.hideConfigurationManagerView();
    }

    private void initPerspectiveManager() throws SalsaPreferencesException {
        perspectivesManager.init();
        if (SalsaUtils.isFulfilled(perspectivesManager.getRestoredDefaultPerspectiveNames())) {
            view.showRestoredDefaultPerspective(perspectivesManager.getRestoredDefaultPerspectiveNames());
        }
        if (SalsaUtils.isFulfilled(perspectivesManager.getWrongPerspectiveFiles())) {
            askForDeletionOfWrongPerspectiveFiles(perspectivesManager.getWrongPerspectiveFiles());
        }
    }

    private void askForDeletionOfWrongPerspectiveFiles(List<String> wrongPerspectiveFiles) {
        if (view.askForDeletionOfWrongPerspectiveFiles(wrongPerspectiveFiles)) {
            List<String> perspectiveFilesFailedToDelete = perspectivesManager.deleteWrongPerspectiveFiles();
            if (SalsaUtils.isFulfilled(perspectiveFilesFailedToDelete)) {
                view.showFailedToDeletePerspectiveFile(wrongPerspectiveFiles);
            }
        }
    }

    /**
     * Load preference from file.
     * 
     * @throws SalsaPreferencesException if an error occurred when loading preferences
     */
    public void loadPreferences() throws SalsaPreferencesException {
        DevicePreferences devicePreferences = DevicePreferencesPersistence.getSystemPreferences();
        initPerspectiveManager();
        setDevicePreferences(devicePreferences);
    }

    private Optional<ConfigTreeView> getConfigTreeView() {
        return Optional.ofNullable(getConfigTreeController().getView());
    }

    /**
     * Copy action.
     * 
     * @param scanType
     */
    public void notifyCopyAction() {
        getConfigTreeView().ifPresent(ConfigTreeView::copyNodes);
    }

    /**
     * Copy action.
     * 
     * @param scanType
     */
    public void notifyCutAction() {
        getConfigTreeView().ifPresent(ConfigTreeView::cutyNodes);
    }

    /**
     * Delete config action.
     */
    public void notifyDeleteConfig() {
        getConfigTreeView().ifPresent(ConfigTreeView::deleteSelectedNodes);
    }

    /**
     * Scan stopped.
     */
    public void notifyExiting() {
        boolean exit;
        List<IConfig<?>> unsavedConfigList = getConfigTreeController().getUnsavedConfigs();
        if (SalsaUtils.isFulfilled(unsavedConfigList)) {
            String[] unsavedConfigNameArray = new String[unsavedConfigList.size()];
            for (int i = 0; i < unsavedConfigNameArray.length; i++) {
                unsavedConfigNameArray[i] = unsavedConfigList.get(i).getName();
            }
            int choice = view.askCloseUnsavedConfig(unsavedConfigNameArray);
            if (choice == ApplicationView.EXIT_SAVING_CHANGES) {
                exit = true;
                getConfigTreeView().ifPresent(ConfigTreeView::saveAll);
            } else if (choice == ApplicationView.EXIT_DISCARDING_CHANGES) {
                exit = true;
            } else {
                exit = false;
            }
        } else {
            exit = true;
        }
        if (exit) {
            saveUIConfiguration();
            System.exit(0);
        }
    }

    private void saveUIConfiguration() {
        savePerspectives();
        saveWindowBounds();
        if (view != null) {
            view.refreshUIPreferences();
            UIPreferencesPersistence.save(UIPreferences.getInstance());
        }
    }

    /**
     * New directory action.
     */
    public void notifyNewDirectory() {
        getConfigTreeView().ifPresent(ConfigTreeView::newDirectory);
    }

    /**
     * New scan action
     * 
     * @param scanType the scan type
     */
    public void notifyNewScan(IConfig.ScanType scanType) {
        getConfigTreeView().ifPresent(v -> v.newScan(scanType));
    }

    /**
     * Paste action.
     * 
     * @param scanType
     */
    public void notifyPasteAction() {
        getConfigTreeView().ifPresent(ConfigTreeView::pasteNodes);
    }

    private IContext generateContext() {
        ContextImpl context = new ContextImpl();
        if (getDevicePreferences() != null) {
            context.setScanServerName(devicePreferences.getScanServer());
            context.setUserLogFile(devicePreferences.getUserLogFile());
            context.setMaxLineNumber(devicePreferences.getMaxLineNumber());
        }
        return context;
    }

    /**
     * Lock layout
     */
    public void notifyLockLayout() {
        view.lockLayout();
    }

    /**
     * Unlock layout
     */
    public void notifyUnlockLayout() {
        view.unlockLayout();
    }

    /**
     * Reset layout
     */
    public void notifyResetLayout() {
        try {
            String[] defaultPerspertivesNames = perspectivesManager.getDefaultPerspectiveNames();
            Perspective currentPerspective = perspectivesManager.getCurrentPerspective();
            if (currentPerspective != null) {
                if (Arrays.stream(defaultPerspertivesNames).anyMatch(x -> x.equals(currentPerspective.getName()))) {
                    if (view.confirmResetPerspectiveLayout(currentPerspective.getName())) {
                        perspectivesManager.resetCurrentPerspectiveLayout(currentPerspective.getName());
                        this.setWindowLayout(perspectivesManager.getCurrentPerspective().getWindowLayout());
                    }
                } else {
                    Optional<String> optionalReferencePerspectiveName = view
                            .showResetLayoutView(defaultPerspertivesNames);
                    if (optionalReferencePerspectiveName.isPresent()) {
                        String referencePerspectiveName = optionalReferencePerspectiveName.get();
                        perspectivesManager.resetCurrentPerspectiveLayout(referencePerspectiveName);
                        this.setWindowLayout(perspectivesManager.getCurrentPerspective().getWindowLayout());
                    }

                }
            }
        } catch (SalsaPerspectiveException e) {
            LOGGER.warn("Cannot reset layout {}", e.getMessage());
            LOGGER.trace("Cannot reset layout", e);
        }
    }

    /**
     * Update the window menu
     * 
     * @param selectedConfig the selected configuration
     */
    public void updateWindowMenu(IConfig<?> selectedConfig) {
        if (getView() != null) {
            if (selectedConfig instanceof IConfig1D) {
                getView().enableConfig1DMenuWindow();
            } else if (selectedConfig instanceof IConfig2D) {
                getView().enableConfig2DMenuWindow();
            } else if (selectedConfig instanceof IConfigHCS) {
                getView().enableConfigHCSMenuWindow();
            } else if (selectedConfig instanceof IConfigK) {
                getView().enableConfigKMenuWindow();
            } else if (selectedConfig instanceof IConfigEnergy) {
                getView().enableConfigEnergyMenuWindow();
            }
        }
    }

    /**
     * Saving the current scan action.
     */
    public void notifySavingAction() {
        saveUIConfiguration();
        getConfigTreeView().ifPresent(ConfigTreeView::saveSelectedNodes);
    }

    /**
     * Load scan action
     * 
     * @param config the configuration
     */
    public void notifyLoadScanAction(IConfig<?> config) {

        if (config != null) {
            boolean modified = config.isModified();
            IConfig<?> configImpl = config.toImpl();
            configImpl.setModified(modified);

            try {
                ScanApi.loadScan(configImpl, generateContext());
            } catch (Exception e) {
                LOGGER.error("Cannot load config {}: {}", config.getName(), e.getMessage());
                LOGGER.trace("Cannot load config {}", config.getName(), e);
            }
        }
    }

    /**
     * Saves window bounds in preferences. (called in notifyExiting())
     */
    public void saveWindowBounds() {
        UIPreferences.getInstance().setXWindowPosition(this.view.getBounds().x + "");
        UIPreferences.getInstance().setYWindowPosition(this.view.getBounds().y + "");
        UIPreferences.getInstance().setWindowWidth(this.view.getBounds().width + "");
        UIPreferences.getInstance().setWindowHeight(this.view.getBounds().height + "");
    }

    /**
     * The configuration to be displayed.
     * 
     * @param config
     */
    public void setConfig(IConfig<? extends IDimension> config) {
        this.config = config;
        StringBuilder builder = new StringBuilder();
        if (config != null) {
            builder.append(config.getName()).append(" - ");
            LOGGER.info("Load config {}", config);
        }
        // If view is null, this means this controller's initialization is not
        // yet over
        if (view != null) {
            builder.append(view.getPreferredTitle());
            view.setTitle(builder.toString());
            getShortcutsController().setConfig(config);
            getActuatorsListController().setConfig(config);
            getSensorsListController().setConfig(config);
            getTimebaseListController().setConfig(config);
            getErrorStrategiesController().setConfig(config);
            getGeneralController().setConfig(config);
            getScanEnergyController().setConfig(config);
            getXTrajectoryController().setConfig(config);
            getYTrajectoryController().setConfig(config);
            if (config instanceof IConfig2D) {
                getYActuatorListController().setConfig(config);
            } else {
                getYActuatorListController().setConfig(null);
            }
            getHooksController().setConfig(config);
            getScanKTrajectoryController().setConfig(config);
            getStartStopController().setConfig(config);
        }
        updateWindowMenu(this.config);

    }

    public void setConfigPath(String configPath) {
        if (SalsaUtils.isDefined(configPath)) {
            getConfigTreeView().ifPresent(v -> v.selectPath(configPath, false));
        }
    }

    /**
     * Shows or hides the view.
     * 
     * @see IController#setVisible(boolean)
     * @param visible make visible if true
     */
    @Override
    public void setViewVisible(boolean visible) {
        fireEvent(visible ? ControllerEvent.VIEW_WILL_BE_DISPLAYED : ControllerEvent.VIEW_WILL_BE_HIDDEN);
        view.setVisible(visible);
        getErrorController().setViewVisible(visible);
        getStartStopController().setViewVisible(visible);
        getGeneralShortcutsToolBarController().setViewVisible(visible);
        getShortcutsController().setViewVisible(visible);
        getSensorsListController().setViewVisible(visible);
        getActuatorsListController().setViewVisible(visible);
        getConfigTreeController().setViewVisible(visible);
        getTimebaseListController().setViewVisible(visible);
        getErrorStrategiesController().setViewVisible(visible);
        getGeneralController().setViewVisible(visible);
        getScanEnergyController().setViewVisible(visible);
        getHooksController().setViewVisible(visible);
        getScanKTrajectoryController().setViewVisible(visible);
        getHistoricLogController().setViewVisible(visible);
        fireEvent(visible ? ControllerEvent.VIEW_IS_DISPLAYED : ControllerEvent.VIEW_IS_HIDDEN);
    }

    /**
     * Sets the current window layout. The window layout is the position and
     * state of the views in the application.
     * 
     * @param windowLayout the window layout
     * @throws SalsaPerspectiveException if error occurred when setting current window layout
     */
    public void setWindowLayout(WindowLayout windowLayout) throws SalsaPerspectiveException {
        this.view.setWindowLayout(windowLayout);
    }

    /**
     * Shows the actuators view.
     * 
     * @param makeVisible show the view if true
     */
    public void showActuatorListView(boolean makeVisible) {
        view.showActuatorListView(makeVisible);
    }

    /**
     * Shows the configuration tree view.
     * 
     * @param makeVisible show the view if true
     */
    public void showConfigTreeView(boolean makeVisible) {
        view.showConfigTreeView(makeVisible);
    }

    /**
     * Shows display manager view.
     * 
     * @param makeVisible show the view if true
     */
    public void showDisplayManagerView(boolean makeVisible) {
        view.showDisplayManagerView(makeVisible);
    }

    /**
     * Shows display manager view.
     * 
     * @param makeVisible show the view if true
     */
    public void showBookmarksView(boolean makeVisible) {
        view.showBookmarkView(makeVisible);
    }

    /**
     * Shows the error strategy view
     * 
     * @param makeVisible show the view if true
     */
    public void showErrorStrategiesView(boolean makeVisible) {
        view.showErrorStrategiesView(makeVisible);
    }

    /**
     * Shows the general view.
     * 
     * @param makeVisible show the view if true
     */
    public void showGeneralView(boolean makeVisible) {
        view.showGeneralView(makeVisible);
    }

    /**
     * Shows the historic log view.
     * 
     * @param makeVisible show the view if true
     */
    public void showHistoricLogView(boolean makeVisible) {
        view.showHistoricLogView(makeVisible);
    }

    /**
     * Shows the historic view.
     * 
     * @param makeVisible show the view if true
     */
    public void showHistoricView(boolean makeVisible) {
        view.showHistoricView(makeVisible);
    }

    /**
     * Shows the DataFitter view
     * 
     * @param makeVisible show the view if true
     */
    public void showDataFitterView(boolean makeVisible) {
        view.showDataFitterView(makeVisible);
    }

    /**
     * Shows the recording view
     * 
     * @param makeVisible show the view if true
     */
    public void showRecordingControlView(boolean makeVisible) {
        view.showRecordingControlView(makeVisible);
    }

    /**
     * Shows the recording project view
     * 
     * @param makeVisible show the view if true
     */
    public void showRecordingProjectView(boolean makeVisible) {
        view.showRecordingProjectView(makeVisible);
    }

    /**
     * Shows the recording counters view
     * 
     * @param makeVisible show the view if true
     */
    public void showRecordingCountersView(boolean makeVisible) {
        view.showRecordingCountersView(makeVisible);
    }

    /**
     * Shows the recording extension view
     * 
     * @param makeVisible show the view if true
     */
    public void showRecordingExtensionView(boolean makeVisible) {
        view.showRecordingExtentionView(makeVisible);
    }

    /**
     * Shows the preferences view
     */
    public void showPreferencesView() {
        view.showPreferencesView();
    }

    /**
     * Shows the input dialog to get the name of the new perspective.
     */
    public void showNewPerspectiveView() {
        view.showNewPerspectiveView();
    }

    /**
     * Shows the dialog for renaming a perspective.
     */
    public void showRenamePerspectiveView() {
        view.showRenamePerspectiveView(perspectivesManager.getCurrentPerspective().getName());
    }

    /**
     * Remove the perspective
     */
    public void removePerspectiveView() {
        Perspective perspectiveToDelete = perspectivesManager.getCurrentPerspective();
        if (perspectiveToDelete != null && SalsaUtils.isDefined(perspectiveToDelete.getName())
                && view.confirmDeletePerspectiveView(perspectiveToDelete.getName())) {
            removePerspective(perspectiveToDelete.getName());
        }
    }

    /**
     * Shows the scan energy view
     * 
     * @param makeVisible make visible if true
     */
    public void showScanEnergyView(boolean makeVisible) {
        view.showScanEnergyView(makeVisible);
    }

    /**
     * Shows the scan functions view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanFunctionsView(boolean makeVisible) {
        view.showScanFunctionsView(makeVisible);
    }

    /**
     * Shows the hooks view.
     * 
     * @param makeVisible make visible if true
     */
    public void showHooksView(boolean makeVisible) {
        view.showHooksView(makeVisible);
    }

    /**
     * Shows the Scan K Trajectory view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanKTrajectoryView(boolean makeVisible) {
        view.showScanKTrajectoryView(makeVisible);
    }

    /**
     * Shows the scan result view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanResultView(boolean makeVisible) {
        view.showScanResultView(makeVisible);
    }

    /**
     * Shows the sensors view.
     * 
     * @param makeVisible make visible if true
     */
    public void showSensorListView(boolean makeVisible) {
        view.showSensorListView(makeVisible);
    }

    /**
     * Shows the timebase view
     * 
     * @param makeVisible make visible if true
     */
    public void showTimebasesView(boolean makeVisible) {
        view.showTimebasesView(makeVisible);
    }

    /**
     * Shows the X trajectories view.
     * 
     * @param makeVisible make visible if true
     */
    public void showXTrajectoriesView(boolean makeVisible) {
        view.showXTrajectoriesView(makeVisible);
    }

    /**
     * Shows the configurationManager view.
     * 
     * @param makeVisible make visible if true
     */
    public void showConfigurationManagerView(boolean makeVisible) {
        view.showConfigurationManagerView(makeVisible);
    }

    /**
     * Shows the X trajectories 2D view.
     * 
     * @param makeVisible make visible if true
     */
    public void showXTrajectories2DView(boolean makeVisible) {
        view.showXTrajectories2DView(makeVisible);
    }

    /**
     * Shows the Y actuators view.
     * 
     * @param makeVisible make visible if true
     */
    public void showYActuatorListView(boolean makeVisible) {
        view.showYActuatorListView(makeVisible);
    }

    /**
     * Shows the Y trajectories 2D view.
     * 
     * @param makeVisible make visible if true
     */
    public void showYTrajectories2DView(boolean makeVisible) {
        view.showYTrajectories2DView(makeVisible);
    }

    /**
     * Adds a new perspective specified by newPerspectiveName if not null.
     * 
     * @param name the perspective name
     */
    public void newPerspective(String name) {
        try {
            Perspective newPerspective = perspectivesManager.createPerspective(name);
            newPerspective.setWindowLayout(getWindowLayout());
            selectPerspective(newPerspective.getName());
        } catch (SalsaPerspectiveException e) {
            LOGGER.warn("Cannot create new perspective {} {}", name, e.getMessage());
            LOGGER.trace("Cannot create new perspective {}", name, e);
        }
    }

    /**
     * Load the specified perspective
     * 
     * @param name the perspective name
     * @throws SalsaPerspectiveException if an error when loading perspective
     */
    public void loadPerspective(String name) throws SalsaPerspectiveException {
        String perspectiveName = name;
        if (!SalsaUtils.isDefined(perspectiveName)) {
            perspectiveName = UIPreferences.getInstance().getLastPerspectiveName();
            if (!SalsaUtils.isDefined(perspectiveName)) {
                perspectiveName = perspectivesManager.getDefaultPerspectiveName();
            }
        }
        perspectiveName = perspectiveName.trim();
        Perspective newCurrentPerspective = perspectivesManager.getPerspectiveByName(perspectiveName);
        if (newCurrentPerspective != null) {
            setWindowLayout(newCurrentPerspective.getWindowLayout());
            perspectivesManager.selectCurrentPerspective(newCurrentPerspective.getName());
            UIPreferences.getInstance().setLastPerspectiveName(newCurrentPerspective.getName());
        }
    }

    /**
     * Select perspective
     * 
     * @param perspectiveName the perspective name
     */
    public void selectPerspective(String perspectiveName) {
        try {
            updateCurrentPerspective();
            loadPerspective(perspectiveName);
        } catch (SalsaPerspectiveException e) {
            LOGGER.error("Cannot select perspective {}: {}", perspectiveName, e.getMessage());
            LOGGER.trace("Cannot select perspective {}", perspectiveName, e);
        }
    }

    /**
     * Rename a perspective
     * 
     * @param oldName the old name
     * @param newName the new name
     */
    public void renamePerspective(String oldName, String newName) {
        if (SalsaUtils.isDefined(newName)) {
            selectPerspective(oldName);
            newPerspective(newName);
            removePerspective(oldName);
        }
    }

    /**
     * Removes a perspective
     * 
     * @param perspectiveName name of perspective to remove
     */
    public void removePerspective(String perspectiveName) {
        if (SalsaUtils.isDefined(perspectiveName)) {
            if (perspectiveName.equals(perspectivesManager.getCurrentPerspective().getName())) {
                selectPerspective(perspectivesManager.getDefaultPerspectiveName());
            }
            Perspective perspective = perspectivesManager.getPerspectiveByName(perspectiveName);
            perspectivesManager.deletePerspective(perspective);
        }
    }

    /**
     * Saves the perspectives
     */
    public void savePerspectives() {
        updateCurrentPerspective();
        perspectivesManager.savePerspectives();
    }

    private void updateCurrentPerspective() {
        try {
            if (perspectivesManager.getCurrentPerspective() != null) {
                perspectivesManager.getCurrentPerspective().setWindowLayout(getWindowLayout());
            }
        } catch (SalsaPerspectiveException e) {
            LOGGER.warn("Cannot update current perspective: {}", e.getMessage());
            LOGGER.trace("Cannot update current perspective", e);
        }
    }

    @Override
    public void controllerViewChanged(ControllerEvent event) {
        if (event != null) {
            if (event.getEventType() == ControllerEvent.VIEW_WILL_BE_DISPLAYED) {
                controllerViewDisplay(event);
            } else if (event.getEventType() == ControllerEvent.VIEW_WILL_BE_HIDDEN) {
                controllerViewHide(event);
            }
        }
    }

    private void controllerViewDisplay(ControllerEvent event) {
        if (event.getSource() == getScanEnergyController()) {
            showScanEnergyView(false);
        } else if (event.getSource() == getScanKTrajectoryController()) {
            showScanKTrajectoryView(false);
        } else if (event.getSource() == getXTrajectoryController()) {
            showXTrajectories2DView(false);
        } else if (event.getSource() == getYTrajectoryController()) {
            showYTrajectories2DView(false);
        } else if (event.getSource() == getActuatorsListController()) {
            // actuatorsListController is a special case
            IConfig<?> currentConf = getActuatorsListController().getConfig();
            if (currentConf instanceof IConfig2D) {
                showYActuatorListView(true);
            } else {
                hideYActuatorListView();
            }
            showActuatorListView(true);
        }
    }

    private void controllerViewHide(ControllerEvent event) {
        if (event.getSource() == getScanEnergyController()) {
            hideScanEnergyView();
        } else if (event.getSource() == getScanKTrajectoryController()) {
            hideScanKTrajectoryView();
        } else if (event.getSource() == getXTrajectoryController()) {
            hideXTrajectories2DView();
        } else if (event.getSource() == getYTrajectoryController()) {
            hideYTrajectories2DView();
        } else if (event.getSource() == getActuatorsListController()) {
            // actuatorsListController is a special case
            hideActuatorListView();
            hideYActuatorListView();
        }
    }

    /**
     * Invoked when a device preference event occurs in the event bus
     * 
     * @param devicePreferencesEvent the device preferences events
     */
    @Subscribe
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        if (devicePreferences.equals(devicePreferencesEvent.getSource())) {
            SalsaAPI.setDevicePreferences(devicePreferences);
            getStartStopController().setDevicePreferences(devicePreferences);
        }
    }

    @Override
    public void setDevicePreferences(DevicePreferences preferences) {
        super.setDevicePreferences(preferences);
        getView().setDevicePreferences(devicePreferences);
        SalsaAPI.setDevicePreferences(preferences);
        getErrorController().setDevicePreferences(preferences);
        getStartStopController().setDevicePreferences(preferences);
        getShortcutsController().setDevicePreferences(preferences);
        getGeneralShortcutsToolBarController().setDevicePreferences(preferences);
        getSensorsListController().setDevicePreferences(preferences);
        getActuatorsListController().setDevicePreferences(preferences);
        getConfigTreeController().setDevicePreferences(preferences);
        getTimebaseListController().setDevicePreferences(preferences);
        getErrorStrategiesController().setDevicePreferences(preferences);
        getGeneralController().setDevicePreferences(preferences);
        getScanKTrajectoryController().setDevicePreferences(preferences);
        getScanEnergyController().setDevicePreferences(preferences);
        getXTrajectoryController().setDevicePreferences(preferences);
        getYTrajectoryController().setDevicePreferences(preferences);
        getYActuatorListController().setDevicePreferences(preferences);
        getHooksController().setDevicePreferences(preferences);
        getHistoricLogController().setDevicePreferences(preferences);
        getDataFitterController().setDevicePreferences(preferences);
        getRecordingControlController().setDevicePreferences(preferences);
        getRecordingProjectController().setDevicePreferences(preferences);
        getRecordingCountersController().setDevicePreferences(preferences);
        getRecordingExtensionController().setDevicePreferences(preferences);
        stateChanged(new DevicePreferencesEvent(preferences, DevicePreferencesEvent.Type.LOADING));
    }

    @Override
    public void configSelectionChanged(IConfig<?> currentConfig) {
        List<IConfig<?>> configurationList = getConfigTreeController().getSelectedConfiguration();
        view.setSelectedConfiguration(configurationList);
        setConfig(currentConfig);
    }

    /**
     * If the view is visible, brings this Window to the front and may makeit the focused Window.
     */
    public void setViewToFront() {
        view.toFront();
    }

}

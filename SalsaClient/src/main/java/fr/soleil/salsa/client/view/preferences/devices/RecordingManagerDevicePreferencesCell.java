package fr.soleil.salsa.client.view.preferences.devices;

/**
 * RecordingManager Device preferences cell
 * 
 * @author Patrick Madela
 */
public class RecordingManagerDevicePreferencesCell extends AbstractDeviceCell {
    /** Class of RecordingManager */
    private static final String DEVICE_CLASS = "RecordingManager";

    /**
     * Create a new Instance
     */
    public RecordingManagerDevicePreferencesCell() {
        super(DEVICE_CLASS);
    }
}

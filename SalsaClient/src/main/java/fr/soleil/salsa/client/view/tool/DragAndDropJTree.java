package fr.soleil.salsa.client.view.tool;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;

import fr.soleil.salsa.exception.ConversionException;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * A JTree that supports inner drag and drop operations.
 * 
 * @see JTree
 */
public class DragAndDropJTree extends JTree implements TreeSelectionListener, DragGestureListener, DropTargetListener,
DragSourceListener {

    private static final long serialVersionUID = -5792298716723923263L;

    public static final Logger LOGGER = LoggingUtil.getLogger(DragAndDropJTree.class);

    /**
     * Support for the drag and drop operations in the configurations tree. Indicates the kind of
     * data being moved around.
     */
    final public static DataFlavor TREE_NODE_FLAVOR = new DataFlavor(TransferableSelection.class,
    "Config Tree Selection");

    /**
     * @see DragSource
     */
    private DragSource dragSource = null;

    /**
     * The selected nodes.
     */
    private TransferableSelection transferableSelection;

    /**
     * The listeners that must be warned of a drop operation.
     */
    private final List<IDropListener> listeners = new ArrayList<IDropListener>();

    /**
     * Constructor.
     */
    public DragAndDropJTree() {
        super();
        construct();
    }

    /**
     * Constructor.
     * 
     * @param value
     */
    public DragAndDropJTree(Object[] value) {
        super(value);
        construct();
    }

    /**
     * Constructor.
     * 
     * @param value
     */
    public DragAndDropJTree(Vector<?> value) {
        super(value);
        construct();
    }

    /**
     * Constructor.
     * 
     * @param value
     */
    public DragAndDropJTree(Hashtable<?, ?> value) {
        super(value);
        construct();
    }

    /**
     * Constructor.
     * 
     * @param root
     */
    public DragAndDropJTree(TreeNode root) {
        super(root);
        construct();
    }

    /**
     * Constructor.
     * 
     * @param newModel
     */
    public DragAndDropJTree(TreeModel newModel) {
        super(newModel);
        construct();
    }

    /**
     * Constructor.
     * 
     * @param root
     * @param asksAllowsChildren
     */
    public DragAndDropJTree(TreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
        construct();
    }

    /**
     * Common to all constructors.
     */
    private void construct() {
        addTreeSelectionListener(this);
        dragSource = DragSource.getDefaultDragSource();
        DragGestureRecognizer dragGestureRecognizer = dragSource.createDefaultDragGestureRecognizer(this,
                DnDConstants.ACTION_COPY_OR_MOVE, this);
        dragGestureRecognizer.setSourceActions(dragGestureRecognizer.getSourceActions() & ~InputEvent.BUTTON3_MASK);
        new DropTarget(this, this);
    }

    /**
     * Adds a listener to be notified if an event is triggered.
     * 
     * @param listener
     */
    public void addListener(IDropListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener.
     * 
     * @param listener
     */
    public void removeListener(IDropListener listener) {
        listeners.remove(listener);
    }

    /**
     * Returns the listeners collection.
     * 
     * @return
     */
    public Collection<IDropListener> getListeners() {
        return listeners;
    }

    // Method from TreeSelectionListener. They let DragAndDropJTree monitor its own selection so it
    // knows what is dragged.
    /**
     * Called when the selection is changed. DragAndDropJTree needs to know which content is
     * dragged.
     * 
     * @see TreeSelectionListener#valueChanged(TreeSelectionEvent)
     */
    public void valueChanged(TreeSelectionEvent evt) {
        List<Object> selectionList = new ArrayList<Object>();
        int nbRows = getRowCount();
        TreePath treePath;
        Object treeNode;
        for (int rowIndex = 0; rowIndex < nbRows; ++rowIndex) {
            if (isRowSelected(rowIndex)) {
                treePath = getPathForRow(rowIndex);
                treeNode = ((DefaultMutableTreeNode) treePath.getLastPathComponent()).getUserObject();
                selectionList.add(treeNode);
            }
        }
        transferableSelection = new TransferableSelection(selectionList);
    }

    // Method from DragGestureListener. They let DragAndDropJTree initiate drag operations.

    /**
     * The user asks for a drag.
     * 
     * @param dge the UI event.
     * @see DragGestureListener#dragGestureRecognized(DragGestureEvent)
     */
    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        try {
            if (transferableSelection != null) {
                List<?> selection = (List<?>) transferableSelection.getTransferData(TREE_NODE_FLAVOR);
                if (selection.size() > 0) {
                    dragSource.startDrag(dge, DragSource.DefaultMoveDrop, transferableSelection, this);
                    for (IDropListener listener : listeners) {
                        listener.startDragAndDrop(dge, selection);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Cannot perform dragAndDrop item {}", e.getMessage());
        }
    }

    // Methods from DragSourceListener. They let DragAndDropJTree support drag operations.

    /**
     * @see java.awt.dnd.DragSourceListener#dragDropEnd(java.awt.dnd.DragSourceDropEvent)
     */
    @Override
    public void dragDropEnd(DragSourceDropEvent dsde) {
        for (IDropListener listener : listeners) {
            listener.endDragAndDrop(dsde);
        }
    }

    /**
     * @see java.awt.dnd.DragSourceListener#dragEnter(java.awt.dnd.DragSourceDragEvent)
     */
    @Override
    public void dragEnter(DragSourceDragEvent dsde) {
        // Nothing to do.
    }

    /**
     * @see java.awt.dnd.DragSourceListener#dragExit(java.awt.dnd.DragSourceEvent)
     */
    @Override
    public void dragExit(DragSourceEvent dse) {
        // Nothing to do.
    }

    /**
     * @see java.awt.dnd.DragSourceListener#dragOver(java.awt.dnd.DragSourceDragEvent)
     */
    @Override
    public void dragOver(DragSourceDragEvent dsde) {
        // Nothing to do.
    }

    /**
     * @see java.awt.dnd.DragSourceListener#dropActionChanged(java.awt.dnd.DragSourceDragEvent)
     */
    @Override
    public void dropActionChanged(DragSourceDragEvent dsde) {
        // Nothing to do.
    }

    // Methods from DropTargetListener. They let DragAndDropJTree support drop operations.
    /**
     * @see java.awt.dnd.DropTargetListener#dragEnter(java.awt.dnd.DropTargetDragEvent)
     */
    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        // Nothing to do.
    }

    /**
     * @see java.awt.dnd.DropTargetListener#dragExit(java.awt.dnd.DropTargetEvent)
     */
    @Override
    public void dragExit(DropTargetEvent dte) {
        // Nothing to do.
    }

    /**
     * @see java.awt.dnd.DropTargetListener#dragOver(java.awt.dnd.DropTargetDragEvent)
     */
    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        // Nothing to do.
    }

    /**
     * @see java.awt.dnd.DropTargetListener#drop(java.awt.dnd.DropTargetDropEvent)
     */
    @Override
    public void drop(DropTargetDropEvent dtde) {
        Point cursorPoint = dtde.getLocation();
        TreePath destinationPath = getPathForLocation(cursorPoint.x, cursorPoint.y);
        if (destinationPath != null) {
            Object target = ((DefaultMutableTreeNode) destinationPath.getLastPathComponent()).getUserObject();
            Transferable transferable = dtde.getTransferable();
            List<?> droppedSelection;
            try {
                droppedSelection = castToObjectsList(transferable);
                for (IDropListener dropListener : listeners) {
                    dropListener.dropped(droppedSelection, target);
                }
            } catch (ConversionException e) {
                // Nothing to do.
            }
        }
    }

    /**
     * Cast to a list of objects.
     * 
     * @param transferable
     * @return
     * @throws UnsupportedFlavorException
     * @throws IOException
     */
    private List<?> castToObjectsList(Transferable transferable) throws ConversionException {
        try {
            return (List<?>) transferable.getTransferData(TREE_NODE_FLAVOR);
        } catch (Exception e) {
            throw new ConversionException(e.getMessage(), e);
        }
    }

    /**
     * @see java.awt.dnd.DropTargetListener#dropActionChanged(java.awt.dnd.DropTargetDragEvent)
     */
    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
        // Nothing to do.
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * A multiple selection that can be transfered by drag and drop.
     */
    private static class TransferableSelection implements Transferable {

        /**
         * Support for the drag and drop operations in the configurations tree. Indicates the kinds
         * of data that can be moved around.
         */
        private static DataFlavor flavorsArray[] = { TREE_NODE_FLAVOR };

        /**
         * The selected nodes.
         */
        private final List<Object> selectedNodesList;

        /**
         * Constructor.
         * 
         * @param selectedNodesList
         */
        public TransferableSelection(List<Object> selectedNodesList) {
            super();
            this.selectedNodesList = selectedNodesList;
        }

        /**
         * @see Transferable#isDataFlavorSupported(DataFlavor)
         */
        @Override
        public boolean isDataFlavorSupported(DataFlavor df) {
            return TREE_NODE_FLAVOR.equals(df);
        }

        /**
         * @see Transferable#getTransferData(DataFlavor)
         */
        @Override
        public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
            if (TREE_NODE_FLAVOR.equals(df)) {
                return selectedNodesList;
            } else {
                throw new UnsupportedFlavorException(df);
            }
        }

        /**
         * @see Transferable#getTransferDataFlavors()
         */
        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return flavorsArray;
        }
    }

}
package fr.soleil.salsa.client.controller.impl;

import java.util.Optional;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * The controller for recording bean
 * 
 * @author madela
 */
public abstract class AbstractRecordingController extends AbstractTangoBoxController {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(AbstractRecordingController.class);

    /**
     * Create a new instance
     */
    protected AbstractRecordingController() {
        super(null);
    }

    @Override
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        if (devicePreferencesEvent.getSource().equals(devicePreferences)) {
            switch (devicePreferencesEvent.getType()) {
                case SCANSERVER_CHANGED:
                case RECORDINGMANAGER_CHANGED:
                case RECORDINGMANAGER_PROFIL_CHANGED:
                case LOADING:
                    refresh();
                    break;
                default:
                    break;
            }
        }
    }

    private void refresh() {
        view.setBeanModel(null);
        getBean().ifPresent(view::setBean);
        getBeanModel().ifPresent(view::setBeanModel);
    }

    protected Optional<String> getRecordingManagerDevice() {
        String recordingManager = devicePreferences.getRecordingManager();
        return SalsaUtils.isDefined(recordingManager) ? Optional.of(recordingManager.trim()) : Optional.empty();
    }

    protected abstract Optional<AbstractTangoBox> getBean();

    protected abstract Optional<String> getBeanModel();
}

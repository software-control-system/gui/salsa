package fr.soleil.salsa.client.view.preferences.devices;

/**
 * DataFitter Device preferences cell
 * 
 * @author Patrick Madela
 */
public class DataFitterDevicePreferencesCell extends AbstractDeviceCell {
    /** Class of DataFitter */
    private static final String DEVICE_CLASS = "DataFitter";

    /**
     * Create a new Instance
     */
    public DataFitterDevicePreferencesCell() {
        super(DEVICE_CLASS);
    }
}

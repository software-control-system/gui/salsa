package fr.soleil.salsa.client.view;

import java.awt.Component;

import javax.swing.Icon;

import net.infonode.docking.DockingWindow;
import net.infonode.docking.DockingWindowListener;
import net.infonode.docking.View;

/**
 * A {@link View} that shows/hides its component on restore/close
 * 
 * @author girardot
 */
public class DefaultView extends View implements DockingWindowListener {
    /** Serialization id */
    private static final long serialVersionUID = -8159368464534293938L;

    /** Memorized timestamp */
    private long lastTimeStamp = 0;

    /**
     * Create a new instance
     * 
     * @param title the title of the view
     * @param icon the icon of the view
     * @param component the component
     */
    public DefaultView(String title, Icon icon, Component component) {
        super(title, icon, component);
        addListener(this);
    }

    @Override
    public void restore() {
        super.restore();
        setComponentVisible(true);
    }

    @Override
    public void close() {
        super.close();
        setComponentVisible(false);
    }

    @Override
    public void windowClosed(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowClosing(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowDocked(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowDocking(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowHidden(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowMaximized(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowMaximizing(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowMinimized(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowMinimizing(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowRemoved(DockingWindow removedFromWindow, DockingWindow removedWindow) {
        // nothing to do
    }

    @Override
    public void windowRestored(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowRestoring(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowShown(DockingWindow window) {
        long currentTM = System.currentTimeMillis();
        if (lastTimeStamp == 0) {
            lastTimeStamp = currentTM;
        }
        long diff = currentTM - lastTimeStamp;
        if (diff < 3000) {
            lastTimeStamp = currentTM;
            setComponentVisible(true);
        }
    }

    @Override
    public void windowUndocked(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowUndocking(DockingWindow window) {
        // nothing to do
    }

    @Override
    public void windowAdded(DockingWindow addedToWindow, DockingWindow addedWindow) {
        // nothing to do
    }

    @Override
    public void viewFocusChanged(View previouslyFocusedView, View focusedView) {
        // nothing to do
    }

    private void setComponentVisible(boolean visible) {
        Component component = getComponent();
        if (component instanceof AbstractTangoBoxView) {
            ((AbstractTangoBoxView) getComponent()).updateBeanVisibility(visible);
        }
    }

}

package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;

import fr.soleil.salsa.bean.BookmarksBean;
import fr.soleil.salsa.bean.ConfigurationManagerBean;
import fr.soleil.salsa.bean.CurrentTrajectoryBean;
import fr.soleil.salsa.client.Salsa;
import fr.soleil.salsa.client.controller.impl.ApplicationController;
import fr.soleil.salsa.client.exception.SalsaPerspectiveException;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.tool.PerspectiveEventDone;
import fr.soleil.salsa.client.util.IconsEnergy;
import fr.soleil.salsa.client.util.IconsScan;
import fr.soleil.salsa.client.util.IconsScanK;
import fr.soleil.salsa.client.util.IconsTrajectories;
import fr.soleil.salsa.client.view.preferences.PreferencesFrame;
import fr.soleil.salsa.client.view.tool.DockingWindowLockerEvent;
import fr.soleil.salsa.client.view.tool.MenuBarViewTool;
import fr.soleil.salsa.client.view.tool.WindowLayout;
import fr.soleil.salsa.client.view.tool.WindowLayoutManager;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.client.view.util.IconsHistoric;
import fr.soleil.salsa.client.view.util.IconsHooks;
import fr.soleil.salsa.client.view.util.IconsStrategies;
import fr.soleil.salsa.configuration.view.ConfigTreeView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.tool.EventBusHandler;
import fr.soleil.salsa.view.DisplayManagerBean;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.RootWindow;
import net.infonode.docking.View;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.ViewMap;
import net.infonode.util.Direction;

/**
 * Main window view.
 */
public class ApplicationView extends JFrame implements IView<ApplicationController> {

    /** Serialization id */
    private static final long serialVersionUID = -6463941089265352L;

    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(ApplicationView.class);

    /** The number of view */
    public static final int NUMBER_OF_VIEWS = 25;

    /** The string format splash frame when building view */
    public static final String BUILDING_VIEW = "Building view %s";

    /** The string format splash frame when building view with number */
    public static final String BUILDING_VIEW_WITH_NUMBER = String.format(BUILDING_VIEW, "%d/%d: %s");

    // View IDs
    private static final String GENERAL_VIEW = "General";
    private static final String DISPLAY_MANAGER_VIEW = "Display Manager";
    private static final String TIMEBASE_VIEW = "Timebase";
    private static final String SENSORS_VIEW = "Sensors";
    private static final String ACTUATORS_X_VIEW = "Actuators X";
    private static final String SCAN_K_VIEW = "Scan K";
    private static final String SCAN_ENERGY_VIEW = "Scan Energy";
    private static final String TRAJECTORY_X_VIEW = "Trajectory X";
    private static final String ACTUATORS_Y_VIEW = "Actuators Y";
    private static final String TRAJECTORY_Y_VIEW = "Trajectory Y";
    private static final String HOOKS_VIEW = "Hooks";
    private static final String SCAN_RESULT_VIEW = "Scan Result";
    private static final String ERROR_STRATEGIES_VIEW = "Error Strategies";
    private static final String SCAN_MANAGER_VIEW = "Scan Manager";
    private static final String SCAN_FUNCTIONS_VIEW = "Scan Functions";
    private static final String SCAN_HISTORIC_VIEW = "Scan Historic";
    private static final String HISTORIC_LOG_VIEW = "Historic Log";
    private static final String DATA_FITTER_VIEW = "Data Fitter";
    private static final String BOOKMARKS_VIEW = "Bookmarks";
    private static final String CURRENT_SCAN_CONFIGURATION_VIEW = "Current scan configuration";
    private static final String CONFIGURATION_MANAGER_VIEW = "Configuration manager";
    private static final String RECORDING_VIEW = "Recording";
    private static final String PROJECT_VIEW = "Project";
    private static final String COUNTERS_VIEW = "Counters";
    private static final String RECORDING_EXTENSION_VIEW = "Recording extension";

    // CardLayout IDs
    private static final String STATUS_BAR_VIEW = "StatusBarView";
    private static final String OOME_VIEW = "OutOfMemoryErrorView";

    // OutOfMermoryError message
    private static final String OOME = "<html><body><span style=\"color:red\"><u><big>Salsa encountered a memory error! Unexpected behaviors</big> <i>(freezes, wrong connections, etc.)</i><big> may happen.<br />You should quit and restart Salsa.</u></big></span></body></html>";
    private static final String OOME_SIMPLE = "Salsa encountered a memory error in thread %s";

    // Other errors message
    private static final String DEFAULT_ERROR = "Uncaught exception %s in thread %s";

    private final transient ResourceBundle resourceBundle;

    private final String preferredTitle;

    /** Constant returned by askCloseUnsavedConfig if the user chooses not to exit. */
    public static final int EXIT_CANCEL = 2;

    /** Constant returned by askCloseUnsavedConfig if the user chooses to exit without saving. */
    public static final int EXIT_DISCARDING_CHANGES = 0;

    /** Constant returned by askCloseUnsavedConfig if the user chooses to save and exit. */
    public static final int EXIT_SAVING_CHANGES = 1;

    /** Identifier for the actuator view. */
    private static final int ACTUATOR_VIEW_ID = 2;

    /** Identifier for the sensor view. */
    private static final int SENSOR_VIEW_ID = 3;

    /** Identifier for the scan result view. */
    private static final int SCAN_RESULT_VIEW_ID = 5;

    /** Identifier for the configurations tree view. */
    private static final int CONFIG_TREE_VIEW_ID = 6;

    /** Identifier for the timebases view. */
    private static final int TIMEBASE_VIEW_ID = 7;

    /** Identifier for the historic view. */
    private static final int HISTORIC_VIEW_ID = 8;

    /** Identifier for the error strategy view. */
    private static final int ERRORSTRATEGY_VIEW_ID = 9;

    /** Identifier for the general view. */
    private static final int GENERAL_VIEW_ID = 10;

    /** Identifier for the scan K trajectory view. */
    private static final int SCAN_K_TRAJECTORY_VIEW_ID = 11;

    /** Identifier for the X trajectory view (2D). */
    private static final int X_TRAJECTORY_VIEW_ID = 12;

    /** Identifier for the Y trajectory view (2D). */
    private static final int Y_TRAJECTORY_VIEW_ID = 13;

    /** Identifier for the Y actuator view. */
    private static final int Y_ACTUATOR_VIEW_ID = 14;

    /** Identifier for the scan function view. */
    private static final int SCAN_FUNCTIONS_VIEW_ID = 16;

    /** Identifier for the hooks view. */
    private static final int HOOKS_VIEW_ID = 17;

    /** Identifier for the scan energy trajectory view. */
    private static final int SCAN_ENERGY_TRAJECTORY_VIEW_ID = 20;

    /** Identifier for the display manager view. */
    private static final int DISPLAY_MANAGER_VIEW_ID = 21;

    /** Identifier for the historic log view. */
    private static final int HISTORICLOG_VIEW_ID = 22;

    /** Identifier for the recording view. */
    private static final int RECORDING_VIEW_ID = 23;

    /** Identifier for the datafitter view. */
    private static final int DATAFITTER_VIEW_ID = 24;

    /** Identifier for bookmark view. */
    private static final int BOOKMARKS_VIEW_ID = 25;

    /** Identifier for current trajectory view. */
    private static final int CURRENT_TRAJECTORY_VIEW_ID = 26;

    /** Identifier for configuration manager view. */
    private static final int CONFIGURATION_MANAGER_ID = 27;

    /** Identifier for recording project view. */
    private static final int RECORDING_PROJECT_ID = 31;

    /** Identifier for recording counters view. */
    private static final int RECORDING_COUNTERS_ID = 32;

    /** Identifier for recording extension view. */
    private static final int RECORDING_EXTENSION_ID = 33;

    /** The controller. */
    private transient ApplicationController applicationController;

    /** Main panel. */
    private JPanel mainPanel;

    /** The main window. */
    private RootWindow rootWindow;

    /** The application view delegates the menu bar management to this tool. */
    private MenuBarViewTool menuBarViewTool;

    /** The status bar. */
    private StatusBarView statusBarView;

    // SCAN-915: display a message in case of OutOfMemoryError.
    /** The bottom panel */
    private JPanel bottomPanel;
    private CardLayout bottomPanelLayout;
    private JLabel oomeLabel;
    private volatile boolean oome;

    /** The view for the main tool bar shortcuts. */
    private GeneralShortcutsToolBarView generalShortcutsToolBarView;

    /** The start / stop view. */
    private StartStopView startStopView;

    /** The shortcuts toolbar view. */
    private ShortcutsView shortcutsView;

    /** The docking element containing the actuator view. */
    private View actuatorDocking;

    /** The docking element containing the Y actuator view. */
    private View yActuatorDocking;

    /** The docking element containing the actuator view. */
    private View sensorDocking;

    /** The docking element containing the scan result view. */
    private View scanResultDocking;

    /** The docking element containing the configurations tree view. */
    private View configTreeDocking;

    /** The docking element containing the timebase view. */
    private View timebaseDocking;

    /** The docking element containing the historic view. */
    private View historicDocking;

    /** The docking for the error strategy view. */
    private View errorStrategiesDocking;

    /** The general docking */
    private View generalDocking;

    /** The docking element containing the Scan K trajectory view. */
    private View scanKTrajectoryDocking;

    /** The docking element containing the actuator view. */
    private View scanEnergyTrajectoryDocking;

    /** The docking element containing the X trajectory view(2D). */
    private View xTrajectoryDocking;

    /** The docking element containing the Y trajectory view(2D). */
    private View yTrajectoryDocking;

    /** The docking element containing the scan functions view. */
    private View scanFunctionsDocking;

    /** The docking element containing the hooks view. */
    private View hooksDocking;

    /** The docking element containing the display manager view. */
    private View displayManagerDocking;

    /** The docking element containing the historic log view. */
    private View historicLogDocking;

    /** The docking element containing the currentTrajectory. */
    private View currentTrajectoryDocking;

    /** The docking element containing the historic log view. */
    private View dataFitterDocking;

    /** The docking element containing the bookmarks. */
    private View bookmarksDocking;

    /** The docking element containing the configurationManager. */
    private View configurationManagerDocking;

    /** The docking element containing the recording view. */
    private View recordingControlDocking;

    /** The docking element containing the recording project view */
    private View recordingProjectDocking;

    /** The docking element containing the recording counters view */
    private View recordingCountersDocking;

    /** The docking element containing the recording extension view */
    private View recordingExtensionDocking;

    /** The scan result view. */
    private ScanResultBean scanResultView;

    /** The display manager view. */
    private DisplayManagerBean displayManagerView;

    private BookmarksBean bookmarsView;

    private ConfigurationManagerBean configurationManagerBean;

    private HistoryView historyView;

    /** The model from preference @see DevicePreferences */
    private transient DevicePreferences devicePreferences;

    /** Preference view */
    private PreferencesFrame preferences;

    /**
     * The error view.
     * The error view is not one of the docking windows, it's an independent dialog box.
     * That's why it has neither an id nor a docking view.
     */
    private transient ErrorView errorView;

    /** A boolean used to know whether xTrajectory docking was selected before being closed */
    private boolean xTrajectorySelected = false;

    private boolean scanResult = false;

    /** Initialization progress for splash progress bar */
    private int initialized;

    /** To manage window layout */
    private WindowLayoutManager windowLayoutManager;

    /**
     * Constructor
     * 
     * @param applicationController the application controller
     * @param scanResult application launched in result mode if true
     */
    private ApplicationView(ApplicationController applicationController, boolean scanResult) {
        super();
        // SCAN-915: display a message in case of OutOfMemoryError.
        oome = false;
        Thread.setDefaultUncaughtExceptionHandler((thread, t) -> {
            boolean memory = false;
            try {
                memory = (t instanceof OutOfMemoryError);
                if (memory) {
                    oome = true;
                    // SCAN-958: don't call System.gc() to avoid consuming CPU
                    SwingUtilities.invokeLater(() -> {
                        if ((bottomPanel != null) && (bottomPanelLayout != null) && (oomeLabel != null)) {
                            bottomPanelLayout.show(bottomPanel, OOME_VIEW);
                        }
                    });
                }
            } finally {
                if (memory) {
                    LOGGER.error(String.format(OOME_SIMPLE, thread), t);
                    // SCAN-958: In case of OutOfMemoryError, exit if main frame does not exist or is not visible.
                    JFrame mainFrame = Salsa.getSalsaFrame();
                    if ((mainFrame == null) || !mainFrame.isShowing()) {
                        System.exit(1);
                    }
                } else {
                    LOGGER.error(String.format(DEFAULT_ERROR, t.getClass().getSimpleName(), thread), t);
                }
            }
        });

        this.scanResult = scanResult;
        this.applicationController = applicationController;
        this.resourceBundle = getResourceBundle();
        StringBuilder titleBuilder = new StringBuilder("Salsa ");
        titleBuilder.append(getBundleMessage("project.version")).append(" (");
        titleBuilder.append(getBundleMessage("build.date")).append(")");
        preferredTitle = titleBuilder.toString();
        initialize();
    }

    /**
     * Factory method to get a new instance of application view
     * Using a factory method to prevent the 'this' reference from escaping during object construction
     * (see. "Safe Construction Practices" section 3.2.1 from Java Concurrency in Practice)
     * 
     * @param applicationController the application controller
     * @param scanResult application launched in result mode if true
     * @return new instance of application view
     */
    public static ApplicationView instance(ApplicationController applicationController, boolean scanResult) {
        ApplicationView applicationView = new ApplicationView(applicationController, scanResult);
        EventBusHandler.getEventBus().register(applicationView);
        return applicationView;
    }

    /**
     * Initialization.
     */
    public void initialize() {
        updateViews();
        ViewMap viewMap = getViewMap();
        viewMap.getView(X_TRAJECTORY_VIEW_ID).setVisible(true);

        rootWindow = DockingUtil.createRootWindow(viewMap, viewMap, true);
        rootWindow.getWindowBar(Direction.DOWN).setEnabled(true);
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(rootWindow, BorderLayout.CENTER);
        setContentPane(mainPanel);
        setTitle(getPreferredTitle());
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                if (applicationController != null) {
                    applicationController.notifyExiting();
                }
            }
        });

        initializeSize();

        // Create menu bar
        setJMenuBar(getMenuBarViewTool());

        initializeController();
        initializeLayouts();
        initializeDockingWindowLocker(viewMap);
        EventBusHandler.getEventBus().register(this);
    }

    /**
     * Loads the last window bounds (x, y, width, height)
     */
    private void initializeSize() {
        // use getUiPreferences() to recover from system if necessary
        String xWindowPosition = UIPreferences.getInstance().getXWindowPosition();
        String yWindowPosition = UIPreferences.getInstance().getYWindowPosition();
        String windowWidth = UIPreferences.getInstance().getWindowWidth();
        String windowHeight = UIPreferences.getInstance().getWindowHeight();

        // Those can be empty if there is no preference file yet.
        if ((xWindowPosition != null) && !"".equals(xWindowPosition.trim()) && (yWindowPosition != null)
                && !"".equals(yWindowPosition.trim()) && (windowWidth != null) && !"".equals(windowWidth.trim())
                && (windowHeight != null) && !"".equals(windowHeight.trim())) {
            setBounds(Integer.valueOf(xWindowPosition), Integer.valueOf(yWindowPosition), Integer.valueOf(windowWidth),
                    Integer.valueOf(windowHeight));
        } else {
            maximizeWindow();
        }
    }

    private void maximizeWindow() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screensize = toolkit.getScreenSize();
        setSize(screensize.width - 80, screensize.height - 80);
        if (toolkit.isFrameStateSupported(MAXIMIZED_BOTH)) {
            setExtendedState(MAXIMIZED_BOTH);
        }
        setLocationRelativeTo(null);
    }

    private void initializeController() {
        if (applicationController != null) {
            getMenuBarViewTool().setControllerTool(applicationController.getMenuBarControllerTool());
        } else {
            getMenuBarViewTool().setControllerTool(null);
        }
    }

    private void initializeLayouts() {
        mainPanel.add(getGeneralShortcutsToolBarView(), BorderLayout.NORTH);
        getGeneralShortcutsToolBarView().add(getShortcutsView());
        getGeneralShortcutsToolBarView().add(getStartStopView());
        getGeneralShortcutsToolBarView().initialize();
        add(getBottomPanel(), BorderLayout.SOUTH);

        // Show the result views only
        if (scanResult) {
            hideActuatorListView();
            hideBookmarkView();
            hideConfigTreeView();
            hideErrorStrategiesView();
            hideGeneralView();
            hideScanEnergyView();
            hideScanKTrajectoryView();
            hideSensorListView();
            hideTimebasesView();
            hideXTrajectories2DView();
            hideYActuatorListView();
            hideYTrajectories2DView();
            hideConfigurationManagerView();
        }
    }

    private void initializeDockingWindowLocker(ViewMap viewMap) {
        windowLayoutManager = new WindowLayoutManager(rootWindow, viewMap);
    }

    private ResourceBundle getResourceBundle() {
        String baseName = getClass().getPackage().getName() + ".application";
        try {
            return ResourceBundle.getBundle(baseName);
        } catch (MissingResourceException | NullPointerException e) {
            LOGGER.error("Unable get resource bundle {}: {}", baseName, e.getMessage());
            LOGGER.trace("Unable get resource bundle {}", baseName, e);
        }
        return null;
    }

    private MenuBarViewTool getMenuBarViewTool() {
        if (menuBarViewTool == null) {
            menuBarViewTool = new MenuBarViewTool(scanResult);
        }
        return menuBarViewTool;
    }

    /**
     * Lock layout
     */
    public void lockLayout() {
        windowLayoutManager.lock();
    }

    /**
     * Unlock layout
     */
    public void unlockLayout() {
        windowLayoutManager.unlock();
    }

    /**
     * Invoked when a docking window locker event occurs in the event bus
     * 
     * @param dockingWindowLockerEvent the docking window locker event
     */
    @Subscribe
    public void dockingWindowLockChanged(DockingWindowLockerEvent dockingWindowLockerEvent) {
        Preconditions.checkNotNull(dockingWindowLockerEvent, "The docking window locker event cannot be null");
        Preconditions.checkNotNull(dockingWindowLockerEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Receive {}", dockingWindowLockerEvent);
        UIPreferences.getInstance().setDockingWindowLocked(dockingWindowLockerEvent.isLocked());
    }

    // SCAN-824
    protected void notifySalsaSplash(String name) {
        String message = String.format(BUILDING_VIEW_WITH_NUMBER, ++initialized, NUMBER_OF_VIEWS, name);
        String source = Thread.currentThread().getStackTrace()[2].getClassName() + "."
                + Thread.currentThread().getStackTrace()[2].getMethodName();
        Salsa.SPLASH.addProgress(message, source);
    }

    private String getBundleMessage(String key) {
        String result = key;
        if (resourceBundle != null && key != null) {
            try {
                result = resourceBundle.getString(key);
            } catch (MissingResourceException e) {
                LOGGER.debug("Unable get bundle message for key {}", key);
            }
        }
        return result;
    }

    /**
     * Returns the controller of the view.
     */
    @Override
    public ApplicationController getController() {
        if (applicationController == null) {
            applicationController = new ApplicationController(this, scanResult);
        }
        return applicationController;
    }

    @Override
    public void setController(ApplicationController controller) {
        this.applicationController = controller;
        initializeController();
    }

    private void updateViews() {
        if (applicationController.getErrorController() != null) {
            setErrorView(applicationController.getErrorController().getView());
        }
    }

    private ShortcutsView getShortcutsView() {
        if (shortcutsView == null) {
            shortcutsView = new ShortcutsView(applicationController.getShortcutsController(), scanResult);
        }
        return shortcutsView;
    }

    private GeneralShortcutsToolBarView getGeneralShortcutsToolBarView() {
        if (generalShortcutsToolBarView == null) {
            generalShortcutsToolBarView = applicationController.getGeneralShortcutsToolBarController().getView();
        }
        return generalShortcutsToolBarView;
    }

    private StartStopView getStartStopView() {
        if (startStopView == null) {
            startStopView = applicationController.getStartStopController().getView();
        }
        return startStopView;
    }

    private JPanel getBottomPanel() {
        if (bottomPanel == null) {
            bottomPanelLayout = new CardLayout();
            bottomPanel = new JPanel(bottomPanelLayout);
            bottomPanel.add(getStatusBarView(), STATUS_BAR_VIEW);
            oomeLabel = new JLabel(OOME);
            bottomPanel.add(oomeLabel, OOME_VIEW);
            bottomPanelLayout.show(bottomPanel, oome ? OOME_VIEW : STATUS_BAR_VIEW);
        }
        return bottomPanel;
    }

    private StatusBarView getStatusBarView() {
        if (statusBarView == null) {
            statusBarView = new StatusBarView();
        }
        return statusBarView;
    }

    private ViewMap getViewMap() {
        ViewMap viewMap = new ViewMap();
        viewMap.addView(GENERAL_VIEW_ID, getGeneralDocking());
        viewMap.addView(DISPLAY_MANAGER_VIEW_ID, getDisplayManagerDocking());
        viewMap.addView(TIMEBASE_VIEW_ID, getTimebaseDocking());
        viewMap.addView(SENSOR_VIEW_ID, getSensorDocking());
        viewMap.addView(ACTUATOR_VIEW_ID, getActuatorDocking());
        viewMap.addView(SCAN_K_TRAJECTORY_VIEW_ID, getScanKTrajectoryDocking());
        viewMap.addView(SCAN_ENERGY_TRAJECTORY_VIEW_ID, getScanEnergyTrajectoryDocking());
        viewMap.addView(X_TRAJECTORY_VIEW_ID, getXTrajectoryDocking());
        viewMap.addView(Y_ACTUATOR_VIEW_ID, getYActuatorDocking());
        viewMap.addView(Y_TRAJECTORY_VIEW_ID, getYTrajectoryDocking());
        viewMap.addView(HOOKS_VIEW_ID, getHooksDocking());
        viewMap.addView(ERRORSTRATEGY_VIEW_ID, getErrorStrategiesDocking());
        viewMap.addView(SCAN_RESULT_VIEW_ID, getScanResultDocking());
        viewMap.addView(CONFIG_TREE_VIEW_ID, getConfigTreeDocking());
        viewMap.addView(SCAN_FUNCTIONS_VIEW_ID, getScanFunctionsDocking());
        viewMap.addView(HISTORIC_VIEW_ID, getHistoricDocking());
        viewMap.addView(HISTORICLOG_VIEW_ID, getHistoricLogDocking());
        viewMap.addView(DATAFITTER_VIEW_ID, getDataFitterDocking());
        viewMap.addView(BOOKMARKS_VIEW_ID, getBookmarksDocking());
        viewMap.addView(CURRENT_TRAJECTORY_VIEW_ID, getCurrentTrajectoryDocking());
        viewMap.addView(CONFIGURATION_MANAGER_ID, getConfigurationManagerDocking());
        viewMap.addView(RECORDING_VIEW_ID, getRecordingControlDocking());
        viewMap.addView(RECORDING_PROJECT_ID, getRecordingProjectDocking());
        viewMap.addView(RECORDING_COUNTERS_ID, getRecordingCountersDocking());
        viewMap.addView(RECORDING_EXTENSION_ID, getRecordingExtensionDocking());
        return viewMap;
    }

    private View getGeneralDocking() {
        if (generalDocking == null) {
            notifySalsaSplash(GENERAL_VIEW);
            GeneralView generalView = applicationController.getGeneralController().getView();
            generalDocking = new View(GENERAL_VIEW, Icons.getIcon("salsa.scanconfig.general"), generalView);
        }
        return generalDocking;
    }

    private View getDisplayManagerDocking() {
        if (displayManagerDocking == null) {
            notifySalsaSplash(DISPLAY_MANAGER_VIEW);
            displayManagerDocking = new View(DISPLAY_MANAGER_VIEW, Icons.getIcon("salsa.display_manager"),
                    getDisplayManagerView());
        }
        return displayManagerDocking;
    }

    private View getTimebaseDocking() {
        if (timebaseDocking == null) {
            notifySalsaSplash(TIMEBASE_VIEW);
            timebaseDocking = new View(TIMEBASE_VIEW, Icons.getIcon("salsa.scanconfig.timebase"),
                    getTimebaseListView());
        }
        return timebaseDocking;
    }

    private View getSensorDocking() {
        if (sensorDocking == null) {
            notifySalsaSplash(SENSORS_VIEW);
            sensorDocking = new View(SENSORS_VIEW, Icons.getIcon("salsa.scanconfig.sensor"), getSensorsListView());
        }
        return sensorDocking;
    }

    private View getActuatorDocking() {
        if (actuatorDocking == null) {
            notifySalsaSplash(ACTUATORS_X_VIEW);
            actuatorDocking = new View(ACTUATORS_X_VIEW, Icons.getIcon("salsa.scanconfig.actuatorx"),
                    getActuatorsListView());
        }
        return actuatorDocking;
    }

    private View getScanKTrajectoryDocking() {
        if (scanKTrajectoryDocking == null) {
            notifySalsaSplash(SCAN_K_VIEW);
            ScanKTrajectoryView scanKTrajectoryView = applicationController.getScanKTrajectoryController().getView();
            scanKTrajectoryDocking = new View(SCAN_K_VIEW, IconsScanK.getIconScanK("salsa.scanconfig.k.small"),
                    scanKTrajectoryView);
        }
        return scanKTrajectoryDocking;
    }

    private View getScanEnergyTrajectoryDocking() {
        if (scanEnergyTrajectoryDocking == null) {
            notifySalsaSplash(SCAN_ENERGY_VIEW);
            EnergyTrajectoryView scanEnergyTrajectoryView = applicationController.getScanEnergyController().getView();
            scanEnergyTrajectoryDocking = new View(SCAN_ENERGY_VIEW,
                    IconsEnergy.getIconEnergy("salsa.scanconfig.energy.small"), scanEnergyTrajectoryView);
        }
        return scanEnergyTrajectoryDocking;
    }

    private View getXTrajectoryDocking() {
        if (xTrajectoryDocking == null) {
            notifySalsaSplash(TRAJECTORY_X_VIEW);
            xTrajectoryDocking = new View(TRAJECTORY_X_VIEW,
                    IconsTrajectories.getIconTrajectory("salsa.scanconfig.trajectoryx"), getXTrajectoryListView());
        }
        return xTrajectoryDocking;
    }

    private View getYActuatorDocking() {
        if (yActuatorDocking == null) {
            notifySalsaSplash(ACTUATORS_Y_VIEW);
            GenericDeviceListView yActuatorListView = applicationController.getYActuatorListController().getView();
            yActuatorDocking = new View(ACTUATORS_Y_VIEW, Icons.getIcon("salsa.scanconfig.actuatory"),
                    yActuatorListView);
        }
        return yActuatorDocking;
    }

    private View getYTrajectoryDocking() {
        if (yTrajectoryDocking == null) {
            notifySalsaSplash(TRAJECTORY_Y_VIEW);
            TrajectoryListView yTrajectoryListView = applicationController.getYTrajectoryController().getView();
            yTrajectoryDocking = new View(TRAJECTORY_Y_VIEW,
                    IconsTrajectories.getIconTrajectory("salsa.scanconfig.trajectoryy"), yTrajectoryListView);
        }
        return yTrajectoryDocking;
    }

    private View getHooksDocking() {
        if (hooksDocking == null) {
            notifySalsaSplash(HOOKS_VIEW);
            HookTableListView hooksView = applicationController.getHooksController().getView();
            hooksView.setController(applicationController.getHooksController());
            hooksView.setReadOnly(scanResult);
            hooksDocking = new View(HOOKS_VIEW, IconsHooks.getIconHook("salsa.scanconfig.hook"), hooksView);
        }
        return hooksDocking;
    }

    private View getErrorStrategiesDocking() {
        if (errorStrategiesDocking == null) {
            notifySalsaSplash(ERROR_STRATEGIES_VIEW);
            ErrorStrategiesView errorStrategiesView = applicationController.getErrorStrategiesController().getView();
            errorStrategiesDocking = new View(ERROR_STRATEGIES_VIEW,
                    IconsStrategies.getIconStrategy("salsa.scanconfig.errorstrategy"), errorStrategiesView);
        }
        return errorStrategiesDocking;
    }

    private View getScanResultDocking() {
        if (scanResultDocking == null) {
            notifySalsaSplash(SCAN_RESULT_VIEW);
            scanResultDocking = new View(SCAN_RESULT_VIEW, IconsScan.getIconScan("salsa.scanserver.graph"),
                    getScanResultView());
        }
        return scanResultDocking;
    }

    private View getConfigTreeDocking() {
        if (configTreeDocking == null) {
            notifySalsaSplash(SCAN_MANAGER_VIEW);
            configTreeDocking = new View(SCAN_MANAGER_VIEW, Icons.getIcon("salsa.scanmanagement.tree"),
                    getConfigTreeView());
        }
        return configTreeDocking;
    }

    private View getScanFunctionsDocking() {
        if (scanFunctionsDocking == null) {
            notifySalsaSplash(SCAN_FUNCTIONS_VIEW);
            ScanFunctionsBean scanFunctionsView = new ScanFunctionsBean();
            scanFunctionsDocking = new View(SCAN_FUNCTIONS_VIEW, Icons.getIcon("salsa.scan_functions"),
                    scanFunctionsView);
        }
        return scanFunctionsDocking;
    }

    private View getHistoricDocking() {
        if (historicDocking == null) {
            notifySalsaSplash(SCAN_HISTORIC_VIEW);
            historicDocking = new View(SCAN_HISTORIC_VIEW, IconsHistoric.getIconHistoric("salsa.scanserver.history"),
                    getHistoryView());
        }
        return historicDocking;
    }

    private View getHistoricLogDocking() {
        if (historicLogDocking == null) {
            notifySalsaSplash(HISTORIC_LOG_VIEW);
            HistoricLogView historicLogView = applicationController.getHistoricLogController().getView();
            historicLogDocking = new View(HISTORIC_LOG_VIEW, Icons.getIcon("salsa.historic_log"), historicLogView);
        }
        return historicLogDocking;
    }

    private View getDataFitterDocking() {
        if (dataFitterDocking == null) {
            notifySalsaSplash(DATA_FITTER_VIEW);
            AbstractTangoBoxView dataFitterView = applicationController.getDataFitterController().getView();
            dataFitterDocking = new DefaultView(DATA_FITTER_VIEW, Icons.getIcon("salsa.datafitter"), dataFitterView);
        }
        return dataFitterDocking;
    }

    private View getBookmarksDocking() {
        if (bookmarksDocking == null) {
            notifySalsaSplash(BOOKMARKS_VIEW);
            bookmarksDocking = new View(BOOKMARKS_VIEW, Icons.getIcon("salsa.scantree.bookmarks"), getBookmarsView());
        }
        return bookmarksDocking;
    }

    private View getCurrentTrajectoryDocking() {
        if (currentTrajectoryDocking == null) {
            notifySalsaSplash(CURRENT_SCAN_CONFIGURATION_VIEW);
            CurrentTrajectoryBean currentTrajectoryBean = new CurrentTrajectoryBean(scanResult);
            currentTrajectoryBean.setConfigTreeController(applicationController.getConfigTreeController());
            currentTrajectoryDocking = new View(CURRENT_SCAN_CONFIGURATION_VIEW,
                    Icons.getIcon("salsa.scanconfig.actuator"), currentTrajectoryBean);
        }
        return currentTrajectoryDocking;
    }

    private View getConfigurationManagerDocking() {
        if (configurationManagerDocking == null) {
            notifySalsaSplash(CONFIGURATION_MANAGER_VIEW);
            configurationManagerDocking = new View(CONFIGURATION_MANAGER_VIEW,
                    Icons.getIcon("salsa.configuration_manager"), getConfigurationManagerBean());
        }
        return configurationManagerDocking;
    }

    private View getRecordingControlDocking() {
        if (recordingControlDocking == null) {
            notifySalsaSplash(RECORDING_VIEW);
            recordingControlDocking = new DefaultView(RECORDING_VIEW, Icons.getIcon("salsa.recording.control"),
                    getRecordingControlView());
        }
        return recordingControlDocking;
    }

    private View getRecordingProjectDocking() {
        if (recordingProjectDocking == null) {
            notifySalsaSplash(PROJECT_VIEW);
            recordingProjectDocking = new DefaultView(PROJECT_VIEW, Icons.getIcon("salsa.recording.project"),
                    getRecordingProjectView());
        }
        return recordingProjectDocking;
    }

    private View getRecordingCountersDocking() {
        if (recordingCountersDocking == null) {
            notifySalsaSplash(COUNTERS_VIEW);
            recordingCountersDocking = new DefaultView(COUNTERS_VIEW, Icons.getIcon("salsa.recording.counters"),
                    getRecordingCountersView());
        }
        return recordingCountersDocking;
    }

    private View getRecordingExtensionDocking() {
        if (recordingExtensionDocking == null) {
            notifySalsaSplash(RECORDING_EXTENSION_VIEW);
            recordingExtensionDocking = new DefaultView(RECORDING_EXTENSION_VIEW,
                    Icons.getIcon("salsa.recording.extension"), getRecordingExtensionView());
        }
        return recordingExtensionDocking;
    }

    private DisplayManagerBean getDisplayManagerView() {
        if (displayManagerView == null) {
            displayManagerView = new DisplayManagerBean();
        }
        return displayManagerView;
    }

    private GenericDeviceListView getTimebaseListView() {
        return applicationController.getTimebaseListController().getView();
    }

    private GenericDeviceListView getSensorsListView() {
        return applicationController.getSensorsListController().getView();
    }

    private GenericDeviceListView getActuatorsListView() {
        return applicationController.getActuatorsListController().getView();
    }

    private ConfigTreeView getConfigTreeView() {
        return applicationController.getConfigTreeController().getView();
    }

    private AbstractTangoBoxView getRecordingControlView() {
        return applicationController.getRecordingControlController().getView();
    }

    private AbstractTangoBoxView getRecordingProjectView() {
        return applicationController.getRecordingProjectController().getView();
    }

    private AbstractTangoBoxView getRecordingCountersView() {
        return applicationController.getRecordingCountersController().getView();
    }

    private AbstractTangoBoxView getRecordingExtensionView() {
        return applicationController.getRecordingExtensionController().getView();
    }

    private TrajectoryListView getXTrajectoryListView() {
        return applicationController.getXTrajectoryController().getView();
    }

    private ScanResultBean getScanResultView() {
        if (scanResultView == null) {
            scanResultView = new ScanResultBean();
            scanResultView.setCurrentScanConfigurationBean(getDisplayManagerView());
        }
        return scanResultView;
    }

    private BookmarksBean getBookmarsView() {
        if (bookmarsView == null) {
            bookmarsView = new BookmarksBean();
            bookmarsView.addConfigSelectionListener(getConfigTreeView());
        }
        return bookmarsView;
    }

    private ConfigurationManagerBean getConfigurationManagerBean() {
        if (configurationManagerBean == null) {
            configurationManagerBean = new ConfigurationManagerBean();
        }
        return configurationManagerBean;
    }

    public HistoryView getHistoryView() {
        if (historyView == null) {
            historyView = new HistoryView();
        }
        return historyView;
    }

    /**
     * Refresh UI preference values from views before to save them
     */
    public void refreshUIPreferences() {
        getDisplayManagerView().refreshUIPreferences();
        getScanResultView().refreshUIPreferences();
        getSensorsListView().refreshUIPreferences();
        getActuatorsListView().refreshUIPreferences();
        getTimebaseListView().refreshUIPreferences();
    }

    /**
     * Shows the actuators view.
     * 
     * @param makeVisible make visible if true
     */
    public void showActuatorListView(boolean makeVisible) {
        restore(getActuatorDocking(), makeVisible);
    }

    /**
     * Hides the actuators view.
     */
    public void hideActuatorListView() {
        getActuatorDocking().close();
    }

    /**
     * Shows the Y actuators view.
     * 
     * @param makeVisible make visible if true
     */
    public void showYActuatorListView(boolean makeVisible) {
        restore(getYActuatorDocking(), makeVisible);
    }

    /**
     * Hides the Y actuators view.
     */
    public void hideYActuatorListView() {
        getYActuatorDocking().close();
    }

    /**
     * Shows the X trajectories 2D view.
     * 
     * @param makeVisible make visible if true
     */
    public void showXTrajectories2DView(boolean makeVisible) {
        getXTrajectoryDocking().setComponent(getXTrajectoryListView());
        restoreXTrajectoryAndComputeFocus(makeVisible);
    }

    private void restoreXTrajectoryAndComputeFocus(boolean makeVisible) {
        getXTrajectoryDocking().restore();
        if (makeVisible || xTrajectorySelected) {
            SwingUtilities.invokeLater(() -> getXTrajectoryDocking().makeVisible());
        }
    }

    /**
     * Hides the X trajectories 2D view.
     */
    public void hideXTrajectories2DView() {
        if (getXTrajectoryDocking().getComponent() == getXTrajectoryListView()) {
            computeXTrajectorySelectedAndClose();
        }
    }

    /**
     * Shows the X trajectories view.
     * 
     * @param makeVisible make visible if true
     */
    public void showXTrajectoriesView(boolean makeVisible) {
        restore(getXTrajectoryDocking(), makeVisible);
    }

    /**
     * Shows the sensors list view.
     * 
     * @param makeVisible make visible if true
     */
    public void showSensorListView(boolean makeVisible) {
        restore(getSensorDocking(), makeVisible);
    }

    /**
     * Hides the sensors list view.
     */
    public void hideSensorListView() {
        getSensorDocking().close();
    }

    /**
     * Shows the Scan K Trajectory view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanKTrajectoryView(boolean makeVisible) {
        restore(getScanKTrajectoryDocking(), makeVisible);
    }

    /**
     * Hides the Scan K Trajectory view.
     */
    public void hideScanKTrajectoryView() {
        getScanKTrajectoryDocking().close();
    }

    /**
     * Shows the Y trajectories 2D view.
     * 
     * @param makeVisible make visible if true
     */
    public void showYTrajectories2DView(boolean makeVisible) {
        restore(getYTrajectoryDocking(), makeVisible);
    }

    /**
     * Hides the Y trajectories 2D view.
     */
    public void hideYTrajectories2DView() {
        getYTrajectoryDocking().close();
    }

    /**
     * Shows the configuration manager view.
     * 
     * @param makeVisible make visible if true
     */
    public void showConfigurationManagerView(boolean makeVisible) {
        restore(getConfigurationManagerDocking(), makeVisible);
    }

    /**
     * Hides the configurationManager view.
     */
    public void hideConfigurationManagerView() {
        getConfigurationManagerDocking().close();
    }

    /**
     * Shows the timebase view.
     * 
     * @param makeVisible make visible if true
     */
    public void showTimebasesView(boolean makeVisible) {
        restore(getTimebaseDocking(), makeVisible);
    }

    /**
     * Hides the timebase view.
     */
    public void hideTimebasesView() {
        getTimebaseDocking().close();
    }

    /**
     * Shows the configuration tree view.
     * 
     * @param makeVisible make visible if true
     */
    public void showConfigTreeView(boolean makeVisible) {
        restore(getConfigTreeDocking(), makeVisible);
    }

    /**
     * Hides the configuration tree view.
     */
    public void hideConfigTreeView() {
        getConfigTreeDocking().close();
    }

    /**
     * Shows the bookmarks view.
     * 
     * @param makeVisible make visible if true
     */
    public void showBookmarkView(boolean makeVisible) {
        restore(getBookmarksDocking(), makeVisible);
    }

    /**
     * Hides the bookmarks view.
     */
    public void hideBookmarkView() {
        getBookmarksDocking().close();
    }

    /**
     * Shows the scan result view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanResultView(boolean makeVisible) {
        restore(getScanResultDocking(), makeVisible);
    }

    /**
     * Hides the scan result view.
     */
    public void hideScanResultView() {
        getScanResultDocking().close();
    }

    /**
     * Shows the hooks view.
     * 
     * @param makeVisible make visible if true
     */
    public void showHooksView(boolean makeVisible) {
        restore(getHooksDocking(), makeVisible);
    }

    /**
     * Shows the scan functions view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanFunctionsView(boolean makeVisible) {
        restore(getScanFunctionsDocking(), makeVisible);
    }

    /**
     * Hides the scan functions view.
     */
    public void hideScanFunctionsView() {
        getScanFunctionsDocking().close();
    }

    /**
     * Shows the display manager view.
     * 
     * @param makeVisible make visible if true
     */
    public void showDisplayManagerView(boolean makeVisible) {
        restore(getDisplayManagerDocking(), makeVisible);
    }

    /**
     * Hides the display Manager docking view.
     */
    public void hideDisplayManagerDockingView() {
        getDisplayManagerDocking().close();
    }

    /**
     * Shows the historic view.
     * 
     * @param makeVisible make visible if true
     */
    public void showHistoricView(boolean makeVisible) {
        restore(getHistoricDocking(), makeVisible);
    }

    /**
     * Hides the historic view.
     */
    public void hideHistoricView() {
        getHistoricDocking().close();
    }

    /**
     * Shows the historic log view.
     * 
     * @param makeVisible make visible if true
     */
    public void showHistoricLogView(boolean makeVisible) {
        restore(getHistoricLogDocking(), makeVisible);
    }

    /**
     * Hides the historic log view.
     */
    public void hideHistoricLogView() {
        getHistoricLogDocking().close();
    }

    /**
     * Shows the general view.
     * 
     * @param makeVisible make visible if true
     */
    public void showGeneralView(boolean makeVisible) {
        restore(getGeneralDocking(), makeVisible);
    }

    /**
     * Hides the general view.
     */
    public void hideGeneralView() {
        getGeneralDocking().close();
    }

    /**
     * Shows the DataFitter view.
     * 
     * @param makeVisible make visible if true
     */
    public void showDataFitterView(boolean makeVisible) {
        restore(getDataFitterDocking(), makeVisible);
    }

    /**
     * Hides the DataFitter view.
     */
    public void hideDataFitterView() {
        getDataFitterDocking().close();
    }

    /**
     * Shows the recording view.
     * 
     * @param makeVisible make visible if true
     */
    public void showRecordingControlView(boolean makeVisible) {
        if (getRecordingControlView().isSetBeanModel()) {
            restore(getRecordingControlDocking(), makeVisible);
        }
    }

    /**
     * Hides the recording view
     */
    public void hideRecordingControlView() {
        getRecordingControlDocking().close();
    }

    /**
     * Shows the recording projects view
     * 
     * @param makeVisible make visible if true
     */
    public void showRecordingProjectView(boolean makeVisible) {
        if (getRecordingProjectView().isSetBeanModel()) {
            restore(getRecordingProjectDocking(), makeVisible);
        }
    }

    /**
     * Hides the recording project view.
     */
    public void hideRecordingProjectView() {
        getRecordingProjectDocking().close();
    }

    /**
     * Shows the recording counters view
     * 
     * @param makeVisible make visible if true
     */
    public void showRecordingCountersView(boolean makeVisible) {
        if (getRecordingCountersView().isSetBeanModel()) {
            restore(getRecordingCountersDocking(), makeVisible);
        }
    }

    /**
     * Hides the recording counters view.
     */
    public void hideRecordingCountersView() {
        getRecordingCountersDocking().close();
    }

    /**
     * Shows the recording experimental data view
     * 
     * @param makeVisible make visible if true
     */
    public void showRecordingExtentionView(boolean makeVisible) {
        if (getRecordingExtensionView().isSetBeanModel()) {
            restore(getRecordingExtensionDocking(), makeVisible);
        }
    }

    /**
     * Hides the recording add-ons view.
     */
    public void hideRecordingExtensionView() {
        getRecordingExtensionDocking().close();
    }

    /**
     * Shows the error strategy view.
     * 
     * @param makeVisible make visible if true
     */
    public void showErrorStrategiesView(boolean makeVisible) {
        restore(getErrorStrategiesDocking(), makeVisible);
    }

    /**
     * Hides the error strategy view.
     */
    public void hideErrorStrategiesView() {
        getErrorStrategiesDocking().close();
    }

    /**
     * Shows the scan energy view.
     * 
     * @param makeVisible make visible if true
     */
    public void showScanEnergyView(boolean makeVisible) {
        restore(getScanEnergyTrajectoryDocking(), makeVisible);
    }

    /**
     * Hides the scan energy view.
     */
    public void hideScanEnergyView() {
        getScanEnergyTrajectoryDocking().close();
    }

    public void setSelectedConfiguration(List<IConfig<?>> configList) {
        getConfigurationManagerBean().setConfigList(configList);
    }

    /**
     * Show preferences view
     */
    public void showPreferencesView() {
        try {
            if (preferences == null) {
                preferences = new PreferencesFrame(getDevicePreferences());
            } else {
                preferences.setPreferences(getDevicePreferences());
            }
            if (!preferences.isVisible()) {
                preferences.setLocationRelativeTo(this);
            }
            preferences.setVisible(true);
            preferences.toFront();
        } catch (Exception e) {
            LOGGER.error("Cannot showPreferencesView {}", e.getMessage());
            LOGGER.trace("Stack trace", e);
        }
    }

    /**
     * The error view.
     * 
     * @param errorView
     *            the errorView to set
     */
    public void setErrorView(ErrorView errorView) {
        this.errorView = errorView;
        if (errorView != null) {
            errorView.setParentWindow(this);
        }
    }

    /**
     * The error view.
     * 
     * @return the errorView
     */
    public ErrorView getErrorView() {
        return errorView;
    }

    /**
     * Shows adding a new Perspective view.
     */
    public void showNewPerspectiveView() {
        String newPerspectiveName = JOptionPane.showInputDialog(this, "Enter the name of the new perspective", "Input",
                JOptionPane.QUESTION_MESSAGE);
        if (applicationController != null && newPerspectiveName != null) {
            applicationController.newPerspective(newPerspectiveName);
        }
    }

    /**
     * Shows rename a perspective
     * 
     * @param oldName original name
     */
    public void showRenamePerspectiveView(String oldName) {
        if (oldName != null) {
            String newName = JOptionPane.showInputDialog(this, "Enter the new name for the perspective " + oldName,
                    oldName);
            if (applicationController != null) {
                applicationController.renamePerspective(oldName, newName);
            }
        }
    }

    /**
     * Shows confirmation dialog to delete a perspective
     * 
     * @param perspectiveToDelete the perspective to delete
     * @return true if perspective could be deleted
     */
    public boolean confirmDeletePerspectiveView(String perspectiveToDelete) {
        return JOptionPane.showConfirmDialog(null, "Delete perspective \"" + perspectiveToDelete + "\"",
                "Delete perspective", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

    /**
     * Shows confirmation dialog to reset layout of a perspective
     * 
     * @param perspectiveToReset the perspective to reset layout
     * @return true if the layout of perspective could be reseted
     */
    public boolean confirmResetPerspectiveLayout(String perspectiveToReset) {
        return JOptionPane.showConfirmDialog(null, "Reset layout of perspective \"" + perspectiveToReset + "\"",
                "Reset layout of perspective", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

    /**
     * Shows deleting a Perspective view.
     * 
     * @param defaultPerspertivesNames names of default perspectives
     * @return the optional selected reference perspective
     */
    public Optional<String> showResetLayoutView(String[] defaultPerspertivesNames) {
        if (applicationController != null && defaultPerspertivesNames.length > 0) {
            String referencePerspective = (String) JOptionPane.showInputDialog(this,
                    "Select the perspective to use as reference", "Input", JOptionPane.QUESTION_MESSAGE, null,
                    defaultPerspertivesNames, defaultPerspertivesNames[0]);
            return Optional.ofNullable(referencePerspective);
        }
        return Optional.empty();
    }

    /**
     * Show error dialog for restored default perspectives
     * 
     * @param restoredDefaultPerspectiveNames list of restored default perspective
     */
    public void showRestoredDefaultPerspective(Collection<String> restoredDefaultPerspectiveNames) {
        StringBuilder errorBuffer = new StringBuilder();
        errorBuffer.append("An error occured while loading following perspectives:\n");
        errorBuffer.append(
                restoredDefaultPerspectiveNames.stream().map(s -> "\'" + s + "\'").collect(Collectors.joining(", ")));
        errorBuffer.append("\n");
        errorBuffer.append("Their default versions will be used instead");
        JOptionPane.showMessageDialog(null, errorBuffer, "Restore default perspective", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Ask for deletion of wring perspective files
     * 
     * @param wrongPerspectiveFiles list of wrong perspective files
     * @return true if user confirms deletion
     */
    public boolean askForDeletionOfWrongPerspectiveFiles(Collection<String> wrongPerspectiveFiles) {
        StringBuilder askBuffer = new StringBuilder();
        askBuffer.append("An error occured while loading following perspectives:\n");
        askBuffer.append(wrongPerspectiveFiles.stream().map(s -> "\'" + s + "\'").collect(Collectors.joining(", ")));
        askBuffer.append("\n");
        askBuffer.append("Would you like to delete these perspective Files ?");
        int confirmation = JOptionPane.showConfirmDialog(null, askBuffer, "Delete wrong perspective files",
                JOptionPane.YES_NO_OPTION);
        return confirmation == JOptionPane.YES_OPTION;
    }

    /**
     * Show error dialog for wrong perspective files that were unable to delete
     * 
     * @param wrongPerspectiveFiles wrong perspective files that were unable to delete
     */
    public void showFailedToDeletePerspectiveFile(Collection<String> wrongPerspectiveFiles) {
        StringBuilder errorBuffer = new StringBuilder();
        errorBuffer.append("Failed to delete following file(s):\n");
        errorBuffer.append(wrongPerspectiveFiles.stream().map(s -> "\'" + s + "\'").collect(Collectors.joining("\n")));
        JOptionPane.showMessageDialog(null, errorBuffer, "Failed to delete wrong perspectives",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Gets the current window layout. The window layout is the position and
     * state of the views in the application. Note : do not call those methods
     * directly from outside the controller. Use the controller instead.
     * 
     * @return the current window layout
     * @throws SalsaPerspectiveException
     */
    public WindowLayout getWindowLayout() throws SalsaPerspectiveException {
        return windowLayoutManager.getWindowLayout();
    }

    /**
     * Sets the current window layout. The window layout is the position and
     * state of the views in the application. Note : do not call those methods
     * directly from outside the controller. Use the controller instead.
     * 
     * @param windowLayout
     * @throws SalsaPerspectiveException
     */
    public void setWindowLayout(WindowLayout windowLayout) throws SalsaPerspectiveException {
        windowLayoutManager.setWindowLayout(windowLayout);
    }

    /**
     * Get Model preference
     * 
     * @return {@link DevicePreferences}
     */
    public DevicePreferences getDevicePreferences() {
        return devicePreferences;
    }

    /**
     * Set preferences
     * 
     * @param devicePreferences
     *            The {@link DevicePreferences} to set
     */
    public void setDevicePreferences(DevicePreferences devicePreferences) {
        this.devicePreferences = devicePreferences;
        stateChanged(new DevicePreferencesEvent(devicePreferences, DevicePreferencesEvent.Type.LOADING));
        getHistoryView().setModel(devicePreferences == null ? null : devicePreferences.getScanServer());
        getHistoryView().start();
        getMenuBarViewTool().setDevicePreferences(devicePreferences);
    }

    /**
     * Invoked when a device preference event occurs in the event bus
     * 
     * @param devicePreferencesEvent the device preferences events
     */
    @Subscribe
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        if (devicePreferences.equals(devicePreferencesEvent.getSource())) {
            updateRecordingControlTitle();
            if (devicePreferencesEvent.getType().equals(DevicePreferencesEvent.Type.DONE)) {
                hideRecordingViewsWithoutBeanModel();
            }
        }
    }

    /**
     * Asks the user a confirmation for exiting despite unsaved configs.
     * 
     * @param configNamesArray the array of configuration names
     * @return an integer indicating the option chosen by the user: ,or CLOSED_OPTION if the user closed the dialog
     */
    public int askCloseUnsavedConfig(String[] configNamesArray) {
        Preconditions.checkNotNull(configNamesArray, "The list of configurations cannot be null");
        Preconditions.checkArgument(configNamesArray.length >= 1, "The list of configurations cannot be empty");
        String message;
        if (configNamesArray.length > 1) {
            StringBuilder sb = new StringBuilder("Those configurations have unsaved changes :");
            for (String configName : configNamesArray) {
                sb.append("\n- ").append(configName);
            }
            message = sb.toString();
        } else {
            message = "The configuration " + configNamesArray[0] + " has unsaved changes.";
        }
        String[] options = { "Exit without saving", "Save and exit", "Cancel", };
        return JOptionPane.showOptionDialog(this, message, "Exit", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[2]);
    }

    /**
     * Enable config 1D menu
     */
    public void enableConfig1DMenuWindow() {
        menuBarViewTool.enableConfig1DMenuWindow();
    }

    /**
     * Enable config 2D menu
     */
    public void enableConfig2DMenuWindow() {
        menuBarViewTool.enableConfig2DMenuWindow();
    }

    /**
     * Enable config HCS menu
     */
    public void enableConfigHCSMenuWindow() {
        menuBarViewTool.enableConfigHCSMenuWindow();
    }

    /**
     * Enable config K menu
     */
    public void enableConfigKMenuWindow() {
        menuBarViewTool.enableConfigKMenuWindow();
    }

    /**
     * Enable config Energy menu
     */
    public void enableConfigEnergyMenuWindow() {
        menuBarViewTool.enableConfigEnergyMenuWindow();
    }

    private void computeXTrajectorySelectedAndClose() {
        xTrajectorySelected = false;
        DockingWindow parent = getXTrajectoryDocking().getWindowParent();
        if (parent != null) {
            xTrajectorySelected = parent.getLastFocusedChildWindow() == getXTrajectoryDocking();
        }
        getXTrajectoryDocking().close();
    }

    public String getPreferredTitle() {
        return preferredTitle;
    }

    private void restore(View view, boolean makeVisible) {
        view.restore();
        if (makeVisible) {
            view.makeVisible();
        }
    }

    /**
     * Invoked when an done perspective event occurs in the event bus
     * 
     * @param perspectiveEventDone the perspective event
     */
    @Subscribe
    public void perspectiveChangedDone(PerspectiveEventDone perspectiveEventDone) {
        Preconditions.checkNotNull(perspectiveEventDone, "The done perspective event cannot be null");
        Preconditions.checkNotNull(perspectiveEventDone.getSource(),
                "The done source of prepspective event cannot be null");
        LOGGER.debug("Receive {}", perspectiveEventDone);
        updateRecordingControlTitle();
        hideRecordingViewsWithoutBeanModel();
    }

    private void hideRecordingViewsWithoutBeanModel() {
        if (!getRecordingProjectView().isSetBeanModel()) {
            hideRecordingProjectView();
        }
        if (!getRecordingCountersView().isSetBeanModel()) {
            hideRecordingCountersView();
        }
        if (!getRecordingExtensionView().isSetBeanModel()) {
            hideRecordingExtensionView();
        }
    }

    private void updateRecordingControlTitle() {
        if (devicePreferences != null) {
            getRecordingControlDocking().getViewProperties().setTitle(RECORDING_VIEW);
        }
    }
}

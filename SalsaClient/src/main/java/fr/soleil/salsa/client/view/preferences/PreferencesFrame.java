/**
 * 
 */
package fr.soleil.salsa.client.view.preferences;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.preferences.UIPreferencesPersistence;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesPersistence;

/**
 * The preferences frame
 * 
 * @author Tarek
 * @author Patrick Madela
 */
public class PreferencesFrame extends JFrame {
    /** Serialization id */
    private static final long serialVersionUID = -5903033303877197150L;

    /** Package of the current class */
    private static final String PACKAGE = PreferencesFrame.class.getPackage().getName();

    /** Icons resources bundle of package */
    private static final Icons ICONS = new Icons(PACKAGE + ".icons");

    /** Icon for set monitor path */
    private static final ImageIcon PREFERENCE_ICON = ICONS.getIcon("Salsa.preferences"); //$NON-NLS-1$

    private JPanel jContentPane;

    private transient DevicePreferences devicePreferences;

    private ButtonBarPreference prefsPanel;

    private BeanPreference beanPreference;

    private DevicePreferencesPanel devicePreferencesPanel;

    private ProgressDialog progressDialog;

    /**
     * This is the default constructor
     */
    public PreferencesFrame() {
        this(null);
    }

    /**
     * Create a new instance
     * 
     * @param devicePreferences the device preferences
     */
    public PreferencesFrame(DevicePreferences devicePreferences) {
        super();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        initialize();
        setPreferences(devicePreferences);
    }

    private void initialize() {
        this.setSize(460, 347);
        this.setContentPane(getJContentPane());
        this.setTitle("Preferences");
        this.setIconImage(PREFERENCE_ICON.getImage());

        getContentPane().setLayout(new BorderLayout());

        prefsPanel = new ButtonBarPreference();

        //
        getJContentPane().add(prefsPanel, BorderLayout.CENTER);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeAction();
            }
        });

    }

    protected ProgressDialog getProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setProgressIndeterminate(true);
        }
        return progressDialog;
    }

    protected void closeAction() {
        if (devicePreferencesPanel != null) {
            devicePreferencesPanel.validateTable();
        }
        ProgressDialog progressDialog = getProgressDialog();
        progressDialog.setTitle("Saving preferences...");
        progressDialog.setMainMessage(progressDialog.getTitle());
        progressDialog.pack();
        progressDialog.setSize(Math.max(300, progressDialog.getWidth()), progressDialog.getHeight());
        progressDialog.setLocationRelativeTo(this);
        progressDialog.setVisible(true);
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                UIPreferencesPersistence.save(UIPreferences.getInstance());
                DevicePreferencesPersistence.save(getDevicePreferences());
                return null;
            }

            @Override
            protected void done() {
                progressDialog.setVisible(false);
                setVisible(false);
            }
        };
        worker.execute();
    }

    /**
     * This method initializes jContentPane
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
        }
        return jContentPane;
    }

    /**
     * Get device preferences
     * 
     * @return device preferences
     */
    public DevicePreferences getDevicePreferences() {
        return devicePreferences;
    }

    /**
     * Set device preferences
     * 
     * @param devicePreferences device preferences
     */
    public void setPreferences(DevicePreferences devicePreferences) {
        this.devicePreferences = devicePreferences;

        // Add Bean Preferences
        if (beanPreference == null) {
            beanPreference = new BeanPreference(devicePreferences);
            prefsPanel.addPreference(beanPreference);
        } else {
            beanPreference.setPreferences(devicePreferences);
        }

        // Add Device specific preferences
        if (devicePreferencesPanel == null) {
            devicePreferencesPanel = new DevicePreferencesPanel(devicePreferences);
            prefsPanel.addPreference(devicePreferencesPanel);
        } else {
            devicePreferencesPanel.setPreferences(devicePreferences);
        }

        pack();
    }

    /**
     * Called when one of the fields gets the focus so that we can select the
     * focused field.
     * 
     * @param e focus event
     */
    public void focusGained(FocusEvent e) {
        Component c = e.getComponent();
        if (c instanceof JFormattedTextField) {
            selectItLater(c);
        } else if (c instanceof JTextField) {
            ((JTextField) c).selectAll();
        }
    }

    /***
     * Workaround for formatted text field focus side effects.
     * 
     * @param c
     */
    protected void selectItLater(Component c) {
        if (c instanceof JFormattedTextField) {
            JFormattedTextField ftf = (JFormattedTextField) c;
            SwingUtilities.invokeLater(ftf::selectAll);
        }
    }

    /**
     * Main for testing
     * 
     * @param args the command lines arguments
     * @throws Exception if an error occurred
     */
    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        JFrame frame = new PreferencesFrame();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}

package fr.soleil.salsa.client.tool;

/**
 * Perspective event when removing a perspective
 * 
 * @author madela
 */
public class PerspectiveEventRemove extends PerspectiveEvent {
    /** Serialization id */
    private static final long serialVersionUID = 3292838411862767997L;

    /**
     * Constructor
     * 
     * @param source the source of event
     * @param type the type of event
     * @param perspective the perspective
     * @param isDefaultPerspective True if it is event for a default perspective
     */
    public PerspectiveEventRemove(Object source, Perspective perspective, boolean isDefaultPerspective) {
        super(source, PerspectiveEvent.Type.REMOVE, perspective, isDefaultPerspective);
    }

    @Override
    public String toString() {
        return "PerspectiveEventRemove [getType()=" + getType() + ", getPerspective()=" + getPerspective()
                + ", isDefaultPerspective()=" + isDefaultPerspective() + "]";
    }
}

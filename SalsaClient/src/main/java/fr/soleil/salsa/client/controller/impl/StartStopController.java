package fr.soleil.salsa.client.controller.impl;

import java.util.Optional;

import org.slf4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;

import fr.soleil.comete.bean.recordingmanageracknowledgebean.RecordingManagerAcknowledgeBean;
import fr.soleil.salsa.client.controller.AErrorManagingController;
import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.client.controller.IErrorController;
import fr.soleil.salsa.client.view.StartStopView;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.preferences.DevicePreferencesEvent;
import fr.soleil.salsa.preferences.DevicePreferencesEvent.Type;
import fr.soleil.salsa.tool.EventBusHandler;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * The controller for the start / stop frame.
 * 
 * @author Patrick Madela
 */
public class StartStopController extends AErrorManagingController<StartStopView> implements IController<StartStopView> {
    /** The logger for messages */
    public static final Logger LOGGER = LoggingUtil.getLogger(StartStopController.class);

    /**
     * Create a new instance
     * 
     * @param applicationController the application controller
     * @param errorController the error controller
     * @param isReadOnly true if read only
     */
    public StartStopController(ApplicationController applicationController, IErrorController errorController,
            boolean isReadOnly) {
        this(null, applicationController, errorController, isReadOnly);
    }

    /**
     * Create a new instance
     * 
     * @param view the view
     * @param applicationController the application controller
     * @param errorController the error controller
     * @param isReadOnly true if read only
     */
    public StartStopController(StartStopView view, ApplicationController applicationController,
            IErrorController errorController, boolean isReadOnly) {
        super(view, errorController, isReadOnly);
        updateView();
        EventBusHandler.getEventBus().register(this);
    }

    @Override
    public void setView(StartStopView view) {
        super.setView(view);
        updateView();
    }

    @Override
    protected StartStopView generateView() {
        return new StartStopView(this, this.readOnly);
    }

    public void setConfig(IConfig<?> config) {
        if (view != null) {
            view.setConfig(config);
        }
    }

    private void updateView() {
        if (view != null) {
            view.setDevicePreferences(getDevicePreferences());
            view.setDataRecorderEnable(isDataRecorderEnable());
        }
    }

    @Override
    public void setDevicePreferences(DevicePreferences preferences) {
        super.setDevicePreferences(preferences);
        updateView();
        stateChanged(new DevicePreferencesEvent(preferences, Type.LOADING));
    }

    /**
     * Return true if data recorder is enable
     * 
     * @return true if data recorder is enable
     */
    public boolean isDataRecorderEnable() {
        boolean recordData = false;
        if (getDevicePreferences() != null) {
            recordData = getDevicePreferences().isDataRecorder();
        }
        return recordData;
    }

    /**
     * Invoked when a device preference event occurs in the event bus
     * 
     * @param devicePreferencesEvent the device preferences events
     */
    @Subscribe
    public void stateChanged(DevicePreferencesEvent devicePreferencesEvent) {
        Preconditions.checkNotNull(devicePreferencesEvent, "The device preference event cannot be null");
        Preconditions.checkNotNull(devicePreferencesEvent.getSource(),
                "The source of device preference event cannot be null");
        LOGGER.debug("Received device preferences event: {}", devicePreferencesEvent);
        if (devicePreferencesEvent.getSource().equals(devicePreferences)) {
            switch (devicePreferencesEvent.getType()) {
                case SCANSERVER_CHANGED:
                case RECORDINGMANAGER_CHANGED:
                case RECORDINGMANAGER_PROFIL_CHANGED:
                case LOADING:
                    refresh();
                    break;
                default:
                    break;
            }
        }

    }

    private void refresh() {
        RecordingManagerAcknowledgeBean recordingManagerAckBean = view.getRecordingManagerAckBean();
        recordingManagerAckBean.stop();
        getRecordingManagerDevice().ifPresent(r -> {
            recordingManagerAckBean.setModel(r);
            recordingManagerAckBean.start();
        });
    }

    private Optional<String> getRecordingManagerDevice() {
        String recordingManager = devicePreferences.getRecordingManager();
        return SalsaUtils.isDefined(recordingManager) ? Optional.of(recordingManager.trim()) : Optional.empty();
    }
}

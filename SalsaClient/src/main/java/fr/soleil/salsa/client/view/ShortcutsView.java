package fr.soleil.salsa.client.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import fr.soleil.salsa.client.controller.impl.ShortcutsController;
import fr.soleil.salsa.client.view.util.Icons;
import fr.soleil.salsa.entity.IConfig.ScanType;

public class ShortcutsView extends JToolBar implements IView<ShortcutsController> {

    private static final long serialVersionUID = -2517996675691068931L;

    /**
     * The controller.
     */
    protected ShortcutsController shortcutsController;

    // /**
    // * The new configuration.
    // */
    // private JButton newButton = null;

    /**
     * The new configuration (or directory).
     */
    private JButton newConfigButton = null;

    /**
     * The delete configuration (or directory) button.
     */
    private JButton deleteButton = null;

    /**
     * The save configurations button.
     */
    private JButton saveButton = null;

    /**
     * The save perspectives button.
     */
    // private JButton savePerspectivesButton = null;

    /**
     * Constructor
     */
    public ShortcutsView() {
        this(null, false);
    }

    public ShortcutsView(ShortcutsController shortcutsController) {
        this(shortcutsController, false);
    }

    public void setDeleteButtonEnable(boolean enable) {
        if (deleteButton != null) {
            deleteButton.setEnabled(enable);
        }
    }

    /**
     * Constructor.
     * 
     * @param shortcutsController
     */
    public ShortcutsView(ShortcutsController shortcutsController, boolean scanResult) {
        this.shortcutsController = shortcutsController;
        if (shortcutsController != null) {
            shortcutsController.setView(this);
        }
        initialize(scanResult);
    }

    @Override
    public ShortcutsController getController() {
        return shortcutsController;
    }

    @Override
    public void setController(ShortcutsController controller) {
        this.shortcutsController = controller;
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize(boolean scanResult) {
        JPanel mainPanel = new JPanel();

        final JPopupMenu jPopupMenu = new JPopupMenu();

        if (!scanResult) {

            JMenuItem newDirectory = new JMenuItem("New Directory", Icons.getIcon("salsa.scanconfig.folder-new"));
            newDirectory.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
            newDirectory.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyNewDirectory();
                    }
                }
            });

            JMenuItem new1D = new JMenuItem("New 1D", Icons.getIcon("salsa.scanconfig.1d.small"));
            new1D.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
            new1D.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyNewConfig(ScanType.SCAN_1D);
                    }
                }
            });

            JMenuItem new2D = new JMenuItem("New 2D", Icons.getIcon("salsa.scanconfig.2d.small"));
            new2D.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
            new2D.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyNewConfig(ScanType.SCAN_2D);
                    }
                }
            });

            JMenuItem newHCS = new JMenuItem("New HCS", Icons.getIcon("salsa.scanconfig.hcs.small"));
            newHCS.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyNewConfig(ScanType.SCAN_HCS);
                    }
                }
            });

            JMenuItem newK = new JMenuItem("New scan K", Icons.getIcon("salsa.scanconfig.k.small"));
            newK.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyNewConfig(ScanType.SCAN_K);
                    }
                }
            });

            JMenuItem newEnergyScan = new JMenuItem("New scan energy", Icons.getIcon("salsa.scanconfig.energy.small"));
            newEnergyScan.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyNewConfig(ScanType.SCAN_ENERGY);
                    }
                }
            });

            jPopupMenu.add(newDirectory);
            jPopupMenu.add(new1D);
            jPopupMenu.add(new2D);
            jPopupMenu.add(newHCS);
            jPopupMenu.add(newK);
            jPopupMenu.add(newEnergyScan);

            newConfigButton = new JButton(Icons.getIcon("salsa.scanconfig.new"));
            newConfigButton.setText("New");
            // newConfigButton.setHorizontalTextPosition(SwingConstants.LEFT);
            // newConfigButton.setIconTextGap(10);
            // newConfigButton.setBorderPainted(false);
            newConfigButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    jPopupMenu.show(newConfigButton, -62, 30);
                }
            });

            mainPanel.add(newConfigButton);

            deleteButton = new JButton(Icons.getIcon("salsa.scanconfig.delete"));
            deleteButton.setText("Delete");
            deleteButton.setEnabled(false);
            // deleteButton.setBorderPainted(false);
            deleteButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    if (shortcutsController != null) {
                        shortcutsController.notifyDeleteConfig();
                    }
                }
            });
            mainPanel.add(deleteButton);

        }

        saveButton = new JButton(Icons.getIcon("salsa.scanconfig.save"));
        saveButton.setText("Save");
        // File Menu
        String toolType = "Save user settings";
        if (!scanResult) {
            toolType = toolType + " and scan configurations";
        }
        saveButton.setToolTipText(toolType);
        // saveButton.setBorderPainted(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                if (shortcutsController != null) {
                    shortcutsController.notifySaveConfig();
                }
            }
        });
        mainPanel.add(saveButton);
        add(mainPanel);

        this.setBorder(BorderFactory.createRaisedBevelBorder());
        this.setFloatable(false);
    }

}

package fr.soleil.salsa.client.view.tool;

import java.io.Serializable;

/**
 * Contains a layout for the windows of the application; They can be used through the setLayout and
 * getLayout methods of ApplicationView.
 */
public class WindowLayout implements Serializable {

    private static final long serialVersionUID = 4000738902626932891L;

    /**
     * The raw content of the layout.
     */
    private byte[] contentArray;

    /**
     * The raw content of the layout.
     * 
     * @return
     */
    public byte[] getContentArray() {
        return contentArray;
    }

    /**
     * The raw content of the layout.
     * 
     * @param contentArray
     */
    public void setContentArray(byte[] contentArray) {
        this.contentArray = contentArray;
    }
}

package fr.soleil.salsa.client.tool;

/**
 * Perspective event when adding a perspective
 * 
 * @author madela
 */
public class PerspectiveEventAdd extends PerspectiveEvent {
    /** Serialization id */
    private static final long serialVersionUID = 6652082998666603493L;

    /**
     * Constructor
     * 
     * @param source the source of event
     * @param type the type of event
     * @param perspective the perspective
     * @param isDefaultPerspective True if it is event for a default perspective
     */
    public PerspectiveEventAdd(Object source, Perspective perspective, boolean isDefaultPerspective) {
        super(source, PerspectiveEvent.Type.ADD, perspective, isDefaultPerspective);
    }

    @Override
    public String toString() {
        return "PerspectiveEventAdd [source=" + source + ", getType()=" + getType() + ", getPerspective()="
                + getPerspective() + ", isDefaultPerspective()=" + isDefaultPerspective() + "]";
    }
}

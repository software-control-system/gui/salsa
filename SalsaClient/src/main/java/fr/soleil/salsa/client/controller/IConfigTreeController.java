package fr.soleil.salsa.client.controller;

import java.util.List;

import fr.soleil.salsa.client.view.ConfigTreeView;
import fr.soleil.salsa.client.view.tool.ATreeNode;
import fr.soleil.salsa.client.view.tool.ConfigTreeNode;
import fr.soleil.salsa.client.view.tool.DirectoryTreeNode;
import fr.soleil.salsa.entity.IConfig;

/**
 * Interface of a controller of a config tree view.
 */
@Deprecated
public interface IConfigTreeController extends IController<ConfigTreeView> {

    /**
     * Method called when the user selects a new directory.
     */
    public void notifyDirectorySelected(DirectoryTreeNode directoryTreeNode);

    /**
     * Method called when the user selects a new config.
     */
    public void notifyConfigSelected(ConfigTreeNode configTreeNode);

    /**
     * Method called when the user clicks on a new configuration.
     */
    public void notifyNewConfig(String configName, IConfig.ScanType type);

    /**
     * Method called when the user clicks on new directory.
     */
    public void notifyNewDirectory(String directoryName);

    /**
     * Method called when the user clicks on save.
     */
    public void notifySave();

    /**
     * Method called when the user clicks on cut.
     */
    public void notifyCut();

    /**
     * Method called when the user clicks on copy.
     */
    public void notifyCopy();

    /**
     * Method called when the user clicks on paste.
     */
    public void notifyPaste();

    /**
     * Method called when the user clicks on delete.
     */
    public void notifyDelete();

    /**
     * Method called when the user renames a config.
     */
    public void notifyRenameConfig(String newName);

    /**
     * Method called when the user renames a directory.
     */
    public void notifyRenameDirectory(String newName);

    /**
     * Method called when the user refreshes an element.
     * 
     * @param treeNode
     */
    public void notifyRefresh();

    /**
     * Method called when the user drags and drops a selection.
     * 
     * @param selection
     * @param target
     */
    public void notifyDrop(List<ATreeNode> droppedSelection, ATreeNode target);

    public void selectScanResult();
}

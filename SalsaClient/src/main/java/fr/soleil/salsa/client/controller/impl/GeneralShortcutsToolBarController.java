package fr.soleil.salsa.client.controller.impl;

import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IController;
import fr.soleil.salsa.client.view.GeneralShortcutsToolBarView;

public class GeneralShortcutsToolBarController extends AController<GeneralShortcutsToolBarView>
        implements IController<GeneralShortcutsToolBarView> {

    /**
     * Constructor
     */
    public GeneralShortcutsToolBarController() {
        this(null);
    }

    /**
     * Constructor
     */
    public GeneralShortcutsToolBarController(GeneralShortcutsToolBarView view) {
        super(view);
    }

    @Override
    protected GeneralShortcutsToolBarView generateView() {
        return new GeneralShortcutsToolBarView(this);
    }

}

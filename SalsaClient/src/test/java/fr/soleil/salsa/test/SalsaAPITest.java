package fr.soleil.salsa.test;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.exception.ScanNotFoundException;

public class SalsaAPITest {

    public static void main(String[] args) {
        initAPIParameters();
        // getActivatedSensorListBug21969();
        getConfigListBug23903();
        System.exit(0);
    }

    public static void initAPIParameters() {
        System.setProperty("SALSA_SERVER_URL", "jdbc:mysql://ORD02215/salsa");
        System.setProperty("SALSA_LOGIN", "root");
    }

    public static void getActivatedSensorListBug21969() {
        try {
            IConfig<?> config = SalsaAPI.getConfigByPath("root/Katy/Test1DOK");
            System.out.println("All sensors = "
                    + Arrays.toString(config.getSensorsList().toArray()));
            System.out.println("activated sensors = "
                    + Arrays.toString(config.getActivatedSensorsList().toArray()));
        }
        catch (ScanNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getConfigListBug23903() {
        Map<String, IConfig<?>> configMap = SalsaAPI.getConfigList();
        Set<String> keyList = configMap.keySet();
        for (String configName : keyList) {
            System.out.println(configName + "=> ID =" + configMap.get(configName).getId());
        }
    }
}

package fr.soleil.salsa.client.tool;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.inOrder;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.eventbus.EventBus;

import fr.soleil.salsa.client.exception.SalsaPerspectiveException;
import fr.soleil.salsa.client.view.tool.WindowLayout;
import fr.soleil.salsa.exception.SalsaPreferencesException;

@RunWith(MockitoJUnitRunner.class)
public class PerspectivesManagerTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Mock
    private EventBus eventBus;

    @Test
    public void testInit() throws SalsaPreferencesException {
        // Given
        doNothing().when(eventBus).post(any(PerspectiveEvent.class));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);

        // When
        perspectiveManager.init();

        // Then
        assertThat(Arrays.stream(folder.getRoot().listFiles()).filter(f -> f.isFile()).map(f -> f.getName()))
                .containsExactlyInAnyOrder("Default.dat", "Configuration.dat", "Scan.dat");
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");
        assertThat(perspectiveManager.getWrongPerspectiveFiles()).isEmpty();
        assertThat(perspectiveManager.getRestoredDefaultPerspectiveNames()).isEmpty();
        assertThat(perspectiveManager.getCurrentPerspective()).isNotNull().hasFieldOrPropertyWithValue("name",
                "Default");
        assertThat(perspectiveManager.getPerspectiveByName("Configuration")).hasFieldOrPropertyWithValue("name",
                "Configuration");
        assertThat(perspectiveManager.getPerspectiveByName("Foo")).isNull();

        // Then check if events sent to listener
        Perspective defaultPerspective = perspectiveManager.getPerspectiveByName("Default");
        Perspective configurationPerspective = perspectiveManager.getPerspectiveByName("Configuration");
        Perspective scanPerspective = perspectiveManager.getPerspectiveByName("Scan");

        InOrder inOrder = inOrder(eventBus);
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, defaultPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.DONE, defaultPerspective, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, configurationPerspective, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.DONE, configurationPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, scanPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.DONE, scanPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.SELECT, defaultPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.DONE, defaultPerspective, true));
    }

    public void testInitWithoutPerspectiveDirectory() throws SalsaPreferencesException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", null);
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When / then
        assertThatThrownBy(() -> {
            perspectiveManager.init();
        }).isInstanceOf(SalsaPreferencesException.class)
                .hasMessageContaining("Invalid or missing property: SALSA_PERSPECTIVES_DIRECTORY");
    }

    @Test
    public void testInitWithInexistantPerspectiveDirectory() throws SalsaPreferencesException {
        // Given
        Path path = Paths.get(folder.getRoot().getAbsolutePath(), "Test.dat");
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", path.toString());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When
        perspectiveManager.init();

        // Then
        assertThat(Arrays.stream(folder.getRoot().listFiles()).filter(f -> f.isFile()).map(f -> f.getName()))
                .containsExactlyInAnyOrder("Default.dat", "Configuration.dat", "Scan.dat");
        assertThat(perspectiveManager.getPerspectiveNames()).contains("Default", "Configuration", "Scan");
    }

    @Test
    public void testInitWithEmptyFile() throws SalsaPreferencesException, IOException {
        // Given
        File file = folder.newFile("empty.dat");
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", file.getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When
        perspectiveManager.init();

        // Then
        assertThat(Arrays.stream(folder.getRoot().listFiles()).filter(f -> f.isFile()).map(f -> f.getName()))
                .containsExactlyInAnyOrder("Default.dat", "Configuration.dat", "Scan.dat", "empty.dat");

        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");
        assertThat(perspectiveManager.getWrongPerspectiveFiles()).containsExactly("empty");
        assertThat(perspectiveManager.getRestoredDefaultPerspectiveNames()).isEmpty();
        assertThat(perspectiveManager.getCurrentPerspective()).isNotNull().hasFieldOrPropertyWithValue("name",
                "Default");
    }

    @Test
    public void testInitWithWrongClass() throws SalsaPreferencesException, IOException {
        // Given create file with wrong class
        InputStream is = getClass().getResourceAsStream("/fr/soleil/salsa/view/foo.dat");
        BufferedInputStream bis = new BufferedInputStream(is);
        Files.copy(bis, Paths.get(folder.getRoot().getAbsolutePath(), "foo.dat"));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When
        perspectiveManager.init();

        // Then
        assertThat(Arrays.stream(folder.getRoot().listFiles()).filter(f -> f.isFile()).map(f -> f.getName()))
                .containsExactlyInAnyOrder("Default.dat", "Configuration.dat", "Scan.dat", "foo.dat");

        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");
        assertThat(perspectiveManager.getWrongPerspectiveFiles()).containsExactly("foo");
        assertThat(perspectiveManager.getRestoredDefaultPerspectiveNames()).isEmpty();
        assertThat(perspectiveManager.getCurrentPerspective()).isNotNull().hasFieldOrPropertyWithValue("name",
                "Default");
    }

    @Test
    public void testDeleteWrongClass() throws SalsaPreferencesException, IOException {
        // Given create file with wrong class
        InputStream is = getClass().getResourceAsStream("/fr/soleil/salsa/view/foo.dat");
        BufferedInputStream bis = new BufferedInputStream(is);
        Files.copy(bis, Paths.get(folder.getRoot().getAbsolutePath(), "foo.dat"));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();
        perspectiveManager.init();

        // When
        perspectiveManager.deleteWrongPerspectiveFiles();

        // Then
        assertThat(Arrays.stream(folder.getRoot().listFiles()).filter(f -> f.isFile()).map(f -> f.getName()))
                .containsExactlyInAnyOrder("Default.dat", "Configuration.dat", "Scan.dat");
    }

    @Test
    public void testRestoreDefaultPerspectiveWhenCorrupted() throws SalsaPreferencesException, IOException {
        // Given create empty Default.dat
        folder.newFile("Default.dat");

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When
        perspectiveManager.init();

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");

        assertThat(perspectiveManager.getRestoredDefaultPerspectiveNames()).containsExactly("Default");
        assertThat(perspectiveManager.getWrongPerspectiveFiles()).isEmpty();
        assertThat(perspectiveManager.getCurrentPerspective()).isNotNull().hasFieldOrPropertyWithValue("name",
                "Default");
    }

    @Test
    public void testGetPerspective() throws SalsaPreferencesException, IOException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When
        perspectiveManager.init();

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");
        assertThat(perspectiveManager.getPerspectiveByName("Default")).hasFieldOrPropertyWithValue("name", "Default");
        assertThat(perspectiveManager.getPerspectiveByName("Test")).isNull();
    }

    @Test
    public void testGetDefaultPerspectiveName() throws SalsaPreferencesException, IOException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();

        // When
        perspectiveManager.init();

        // Then
        assertThat(perspectiveManager.getDefaultPerspectiveName()).isEqualTo("Default");
    }

    @Test
    public void testCreatePerspective() throws SalsaPreferencesException, IOException {
        // Given
        doNothing().when(eventBus).post(any(PerspectiveEvent.class));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);
        perspectiveManager.init();

        // When create perspectives
        Perspective newPerspective1 = perspectiveManager.createPerspective();
        Perspective newPerspective2 = perspectiveManager.createPerspective("");
        Perspective newPerspective3 = perspectiveManager.createPerspective();

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan",
                "New perspective", "New perspective 2", "New perspective 3");
        assertThat(perspectiveManager.getPerspectiveByName("New perspective")).isEqualTo(newPerspective1);
        assertThat(perspectiveManager.getPerspectiveByName("New perspective 2")).isEqualTo(newPerspective2);
        assertThat(perspectiveManager.getPerspectiveByName("New perspective 3")).isEqualTo(newPerspective3);

        // Then check if events sent to listener
        InOrder inOrder = inOrder(eventBus);
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, newPerspective1, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, newPerspective2, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, newPerspective3, false));
    }

    @Test
    public void testGetPerspectiveNames() throws SalsaPreferencesException, IOException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();
        perspectiveManager.init();

        // When
        perspectiveManager.createPerspective("Test");

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan",
                "Test");
        assertThat(perspectiveManager.getDefaultPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");
    }

    @Test
    public void testGetNewNamedPerspective() throws SalsaPreferencesException, IOException {
        // Given
        doNothing().when(eventBus).post(any(PerspectiveEvent.class));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);
        perspectiveManager.init();

        // When
        Perspective newPerspective1 = perspectiveManager.createPerspective("Test");
        Perspective newPerspective2 = perspectiveManager.createPerspective("Test");
        Perspective newPerspective3 = perspectiveManager.createPerspective("Test");
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan", "Test",
                "Test 2", "Test 3");

        // Then
        assertThat(perspectiveManager.getPerspectiveByName("Test")).isEqualTo(newPerspective1);
        assertThat(perspectiveManager.getPerspectiveByName("Test 2")).isEqualTo(newPerspective2);
        assertThat(perspectiveManager.getPerspectiveByName("Test 3")).isEqualTo(newPerspective3);

        // Then Check if events sent to listener
        InOrder inOrder = inOrder(eventBus);
        Perspective defaultPerspective = perspectiveManager.getPerspectiveByName("Default");
        Perspective configurationPerspective = perspectiveManager.getPerspectiveByName("Configuration");
        Perspective scanPerspective = perspectiveManager.getPerspectiveByName("Scan");

        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, defaultPerspective, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, configurationPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, scanPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.SELECT, defaultPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, newPerspective1, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, newPerspective2, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, newPerspective3, false));
    }

    @Test
    public void testSavePerspectives() throws SalsaPreferencesException, IOException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();
        perspectiveManager.init();
        perspectiveManager.createPerspective("Test");

        // When
        perspectiveManager.savePerspectives();

        // Then
        assertThat(new File(folder.getRoot().getAbsolutePath(), "Test.dat")).isFile();
    }

    @Test
    public void testLoadSavedPerspectives() throws SalsaPreferencesException, IOException {
        // Given
        doNothing().when(eventBus).post(any(PerspectiveEvent.class));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);
        perspectiveManager.init();

        perspectiveManager.createPerspective("Test");
        perspectiveManager.savePerspectives();

        // Given create new perspective manager to be sure to load perspectives from files
        perspectiveManager = new PerspectivesManager(eventBus);

        // When
        perspectiveManager.init();

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan",
                "Test");

        // Then check events if events sent to listener
        Perspective defaultPerspective = perspectiveManager.getPerspectiveByName("Default");
        Perspective configurationPerspective = perspectiveManager.getPerspectiveByName("Configuration");
        Perspective scanPerspective = perspectiveManager.getPerspectiveByName("Scan");
        Perspective testPerspective = perspectiveManager.getPerspectiveByName("Test");

        InOrder inOrder = inOrder(eventBus);
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, defaultPerspective, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, configurationPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, scanPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, testPerspective, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.SELECT, defaultPerspective, true));
    }

    @Test
    public void testRemovePerspective() throws SalsaPreferencesException, IOException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);
        perspectiveManager.init();
        Perspective perspective = perspectiveManager.createPerspective("Test");

        // When
        perspectiveManager.deletePerspective(perspective);

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");

        InOrder inOrder = inOrder(eventBus);
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.REMOVE, perspective, false));
    }

    @Test
    public void testRemoveSavedPerspective() throws SalsaPreferencesException, IOException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);
        perspectiveManager.init();
        Perspective perspective = perspectiveManager.createPerspective("Test");
        perspectiveManager.savePerspectives();
        perspectiveManager.deletePerspective(perspective);

        // When
        perspectiveManager = new PerspectivesManager(eventBus);
        perspectiveManager.init();

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan");
    }

    @Test
    public void testReInit() throws SalsaPreferencesException, IOException {
        // Given
        doNothing().when(eventBus).post(any(PerspectiveEvent.class));

        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = new PerspectivesManager(eventBus);

        // When
        perspectiveManager.init();
        Perspective defaultPerspective = perspectiveManager.getPerspectiveByName("Default");
        Perspective configurationPerspective = perspectiveManager.getPerspectiveByName("Configuration");
        Perspective scanPerspective = perspectiveManager.getPerspectiveByName("Scan");
        Perspective testPerspective = perspectiveManager.createPerspective("Test");

        perspectiveManager.init();
        Perspective defaultPerspective2 = perspectiveManager.getPerspectiveByName("Default");
        Perspective configurationPerspective2 = perspectiveManager.getPerspectiveByName("Configuration");
        Perspective scanPerspective2 = perspectiveManager.getPerspectiveByName("Scan");

        // Then
        InOrder inOrder = inOrder(eventBus);
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, defaultPerspective, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, configurationPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, scanPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.SELECT, defaultPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, testPerspective, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.REMOVE, defaultPerspective, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.REMOVE, configurationPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.REMOVE, scanPerspective, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.REMOVE, testPerspective, false));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, defaultPerspective2, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, configurationPerspective2, true));
        inOrder.verify(eventBus)
                .post(PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.ADD, scanPerspective2, true));
        inOrder.verify(eventBus).post(
                PerspectiveEvent.get(perspectiveManager, PerspectiveEvent.Type.SELECT, defaultPerspective2, true));
    }

    @Test
    public void testResetConfigurationPerspectiveLayout()
            throws SalsaPreferencesException, IOException, SalsaPerspectiveException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();
        perspectiveManager.init();

        // Given select configuration perspective and set layout to scan perspective
        perspectiveManager.selectCurrentPerspective("Configuration");
        Perspective scanPerspective = perspectiveManager.getPerspectiveByName("Scan");
        Perspective configurationPerspective = perspectiveManager.getPerspectiveByName("Configuration");
        WindowLayout originalConfigurationPerspectiveLayout = configurationPerspective.getWindowLayout();
        configurationPerspective.setWindowLayout(scanPerspective.getWindowLayout());
        assertThat(configurationPerspective.getWindowLayout().getContentArray())
                .isEqualTo(perspectiveManager.getPerspectiveByName("Scan").getWindowLayout().getContentArray());

        // When
        perspectiveManager.resetCurrentPerspectiveLayout("Configuration");

        // Then
        assertThat(configurationPerspective.getWindowLayout()).isNotNull();
        assertThat(configurationPerspective.getWindowLayout().getContentArray())
                .isNotEqualTo(perspectiveManager.getPerspectiveByName("Scan").getWindowLayout().getContentArray());
        assertThat(configurationPerspective.getWindowLayout().getContentArray()).isEqualTo(
                perspectiveManager.getPerspectiveByName("Configuration").getWindowLayout().getContentArray());
        assertThat(configurationPerspective.getWindowLayout().getContentArray())
                .isEqualTo(originalConfigurationPerspectiveLayout.getContentArray());
    }

    @Test
    public void testResetCurrentPerspectiveLayout()
            throws SalsaPreferencesException, IOException, SalsaPerspectiveException {
        // Given
        System.setProperty("SALSA_PERSPECTIVES_DIRECTORY", folder.getRoot().getAbsolutePath());
        PerspectivesManager perspectiveManager = PerspectivesManager.instance();
        perspectiveManager.init();
        Perspective testPerspective = perspectiveManager.createPerspective("Test");
        perspectiveManager.selectCurrentPerspective("Test");
        testPerspective.setWindowLayout(perspectiveManager.getPerspectiveByName("Configuration").getWindowLayout());

        // When
        perspectiveManager.resetCurrentPerspectiveLayout("Default");

        // Then
        assertThat(perspectiveManager.getPerspectiveNames()).containsExactly("Default", "Configuration", "Scan",
                "Test");
        assertThat(testPerspective.getWindowLayout()).isNotNull();
        assertThat(testPerspective.getWindowLayout().getContentArray())
                .isEqualTo(perspectiveManager.getPerspectiveByName("Default").getWindowLayout().getContentArray());
    }
}

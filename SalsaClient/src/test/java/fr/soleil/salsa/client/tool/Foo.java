package fr.soleil.salsa.client.tool;

import java.io.Serializable;

public class Foo implements Serializable {
    private static final long serialVersionUID = -553080665135299107L;

    private final String bar;

    public Foo(String bar) {
        super();
        this.bar = bar;
    }

    public String getBar() {
        return bar;
    }

}

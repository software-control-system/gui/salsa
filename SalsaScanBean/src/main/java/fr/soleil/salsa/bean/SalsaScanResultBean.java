package fr.soleil.salsa.bean;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JSplitPane;

import fr.soleil.salsa.client.view.ScanResultBean;

public class SalsaScanResultBean extends AbstractSalsaBean {

    private static final long serialVersionUID = -3802301952588804191L;

    private ScanResultBean bean = null;
    private boolean firstLoad = true;
    private boolean firstUnload = true;

    public SalsaScanResultBean() {
        bean = new ScanResultBean(false);
        setLayout(new BorderLayout());
        add(bean, BorderLayout.CENTER);
    }

    public void setSalsaDisplayManagerBean(SalsaDisplayManagerBean manager) {
        if (manager != null) {
            manager.addCurrentScanConfigurationBeanListener(bean);
        }
    }

    @Override
    public void loadConfig() {
        super.loadConfig();
        if (firstLoad) {
            firstLoad = false;
            bean.initPreferences();
            bean.start();
        }
    }

    @Override
    public void unloadConfig() {
        super.unloadConfig();
        if (firstUnload) {
            firstUnload = false;
            bean.stop();
        }
    }

    public int getSplitLocation() {
        int splitLocation = 0;
        if (bean != null) {
            splitLocation = bean.getSplitLocation();
        }
        return splitLocation;
    }

    public void setSplitLocation(int location) {
        if (bean != null) {
            bean.setDividerLocation(location);
        }
    }

    public void setManagementPanelVisible(boolean visible) {
        if (bean != null) {
            bean.setManagementPanelVisible(visible);
        }
    }

    public boolean isManagementPanelVisible() {
        boolean visible = false;
        if (bean != null) {
            visible = bean.isManagementPanelVisible();
        }
        return visible;
    }

    /**
     * Main test
     * 
     * @param args
     */
    public static void main(String[] args) {
        JSplitPane splitPanel = new JSplitPane();

        SalsaScanResultBean bean = new SalsaScanResultBean();
        SalsaDisplayManagerBean manager = new SalsaDisplayManagerBean();
        splitPanel.add(bean, JSplitPane.LEFT);
        splitPanel.add(manager, JSplitPane.RIGHT);
        configureBeanWithoutConfig(bean);
        configureBeanWithoutConfig(manager);
        bean.setSalsaDisplayManagerBean(manager);
        // configureBeanWithConfigAtLoad(bean);
        // configureBeanWithConfigAtNewScan(bean);
        // configureBeanWithConfigAtNewScanAndReload(bean);
        bean.loadConfig();
        manager.loadConfig();
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("SalsaScanResultBean");
        frame.setContentPane(splitPanel);
        frame.setVisible(true);
    }

    // Test without config
    public static void configureBeanWithoutConfig(ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(false);
        bean.setCurrentConfigAtNewScan(false);
    }

    // Test with config at lod
    public static void configureBeanWithConfigAtLoad(ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(false);
    }

    public static void configureBeanWithConfigAtNewScan(ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(true);
    }

    public static void configureBeanWithConfigAtNewScanAndReload(ISalsaBean bean) {
        bean.setCurrentConfigAtLoad(true);
        bean.setCurrentConfigAtNewScan(true);
        bean.setReloadConfigAtRead(true);
    }

    public double[] getTrajectories() {
        double[] result = this.bean.generateScan2DTrajectories();
        return result;
    }

}

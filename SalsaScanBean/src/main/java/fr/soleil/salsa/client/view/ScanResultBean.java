package fr.soleil.salsa.client.view;

import javax.swing.SwingUtilities;

import fr.soleil.bean.scanserver.CurrentScanResultBean;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.preferences.DevicePreferences;

public class ScanResultBean extends CurrentScanResultBean {
    private static final long serialVersionUID = 8867370368252920404L;
    private int splitLocation = 0;

    public ScanResultBean() {
        this(true);
    }

    public ScanResultBean(boolean autostart) {
        super(Boolean.valueOf(UIPreferences.getInstance().isShowImageAsSpectrumStack()),
                SalsaAPI.getDevicePreferences().getPublisher(),
                Boolean.valueOf(UIPreferences.getInstance().isSendSensor()));
        if (autostart) {
            initPreferences();
            start();
        }
    }

    public int getSplitLocation() {
        return splitLocation;
    }

    public void setSplitLocation(int location) {
        this.splitLocation = location;
        setDividerLocation(splitLocation);
    }

    public void initPreferences() {
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        // Load the scanServerName
        if (devicePreferences != null) {
            String scanServerName = devicePreferences.getScanServer();
            setScanServerDeviceName(scanServerName);
        }
        if (splitLocation > 0) {
            setDividerLocation(splitLocation);
        } else {
            setDividerLocation(UIPreferences.getInstance().getDividerLocation());
        }

        // If there is no DisPlayManager load the UIPreferencesMap
        if (getCurrentScanConfigurationBean() == null) {
            CurrentScanDataModel.setScanPropertiesMap(UIPreferences.getInstance().getCurrentScanPropertiesMap());
        }
    }

    @Override
    protected void dividerLocationChanged(int location) {
        UIPreferences.getInstance().setDividerLocation(location);
    }

    @Override
    protected void beforeNewScan() {
        super.beforeNewScan();
        refreshUIPreferences();
    }

    @Override
    protected void afterNewScan() {
        super.afterNewScan();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setImagePropertiesMap(UIPreferences.getInstance().getImagePropertiesMap());
                setPlotPropertiesMap(UIPreferences.getInstance().getPlotPropertiesMap());
                setChartProperties(CurrentScanDataModel.getChartPropertyScan(getModel()));
            }
        });
    }

    public void refreshUIPreferences() {
        UIPreferences.getInstance().setImagePropertiesMap(getImagePropertiesMap());
        UIPreferences.getInstance().setPlotPropertiesMap(getPlotPropertiesMap());
        if (getCurrentScanConfigurationBean() == null) {
            UIPreferences.getInstance().setCurrentScanPropertiesMap(CurrentScanDataModel.getScanPropertiesMap());
        }
    }
}

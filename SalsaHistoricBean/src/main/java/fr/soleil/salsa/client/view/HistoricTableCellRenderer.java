package fr.soleil.salsa.client.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Custom renderer for the cells of the table. Identical to the default renderer except : - it
 * doesn't display which cell has the focus, - also, it displays the content of the cell in a
 * tooltip, - rows with type "ERROR" are displayed in red.
 */
@Deprecated
public class HistoricTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -3413390432924319931L;

    private final JLabel errorComponent = new JLabel();

    public HistoricTableCellRenderer() {
        super();
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean isSelected, boolean hasFocus,
            int row, int col) {
        JComponent component = (JComponent) super.getTableCellRendererComponent(jTable, value, isSelected, hasFocus,
                row, col);
        // Find ERROR label in line
        boolean errorFound = false;

        int nbOfColumn = jTable.getColumnCount();
        for (int i = 0; i < nbOfColumn; i++) {
            Object object = jTable.getValueAt(row, i);
            if (object != null && (object.toString().equals("ERROR") || object.toString().equals("FATAL"))) {
                errorFound = true;
                break;
            }
        }

        if (errorFound) {
            if (isSelected) {
                component.setBackground(Color.RED);
            } else {
                errorComponent.setText(value.toString());
                errorComponent.setBackground(Color.ORANGE);
                if (col == 2) {
                    errorComponent.setHorizontalAlignment(SwingConstants.LEFT);
                } else {
                    errorComponent.setHorizontalAlignment(SwingConstants.CENTER);
                }
                errorComponent.setOpaque(true);
                component = errorComponent;
            }
        }
        component.setToolTipText(value.toString());
        return component;
    }
}

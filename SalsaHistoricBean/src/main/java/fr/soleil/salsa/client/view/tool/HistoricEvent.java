package fr.soleil.salsa.client.view.tool;

import java.io.Serializable;

/**
 * Event in the historic.
 */
@Deprecated
public class HistoricEvent implements Serializable {

    private static final long serialVersionUID = -309440118958388542L;

    /**
     * The period.
     */
    private String period;

    /**
     * The type.
     */
    private String type;

    /**
     * The message.
     */
    private String message;

    /**
     * The period.
     */
    public String getPeriod() {
        return period;
    }

    /**
     * The period.
     */
    public void setPeriod(String period) {
        this.period = period;
    }

    /**
     * The type.
     */
    public String getType() {
        return type;
    }

    /**
     * The type.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * The message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * The message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.util.prefs.Preferences;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.target.ADeviceLogAdapter;
import fr.soleil.comete.box.util.DeviceLogViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.model.scanserver.CurrentScanDataModel;

/**
 * Historic view. Shows the events of a scan.
 * 
 * @author GIRARDOT
 */
public class HistoryView extends AbstractTangoBox {

    private static final long serialVersionUID = -4719279750511116975L;

    private static final String CLEAR_HISTORIC = "ClearHistoric";
    private static final String CLEAR_HISTORIC_TITLE = "Clear Historic";

    protected final DeviceLogViewer historyViewer;
    protected ADeviceLogAdapter<?> logAdapter;
    protected final StringMatrixBox stringMatrixBox;

    public HistoryView() {
        super();
        historyViewer = new DeviceLogViewer(getClass().getName());
        historyViewer.getClearDeviceLogsButton().setToolTipText(CLEAR_HISTORIC_TITLE);
        historyViewer.setDefaultClearLogsButtonVisible(false);
        stringMatrixBox = new StringMatrixBox();
        stringBox.setDefaultToolTipEnabled(false);
        setLayout(new BorderLayout());
        add(historyViewer, BorderLayout.CENTER);
    }

    /**
     * Connection to log attribute
     */
    protected void connectHistory() {
        TangoKey key = generateAttributeKey(CurrentScanDataModel.HISTORIC);
        logAdapter = getAndConnectBestDeviceLogAdapter(historyViewer, key, stringMatrixBox);
        connectCommandClearHistory();
    }

    protected void disconnectHistory() {
        stringMatrixBox.disconnectWidgetFromAll(logAdapter);
        disconnectCommandClearHistory();
    }

    protected void connectCommandClearHistory() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, getModel(), CLEAR_HISTORIC);
        connectClearDeviceLogsButton(historyViewer, key, stringBox, DEFAULT_MESSAGE_MANAGER);
    }

    protected void disconnectCommandClearHistory() {
        stringBox.disconnectWidgetFromAll(historyViewer.getClearDeviceLogsButton());
    }

    @Override
    protected void clearGUI() {
        disconnectHistory();
    }

    @Override
    protected void refreshGUI() {
        disconnectHistory();
        connectHistory();
    }

    @Override
    protected void onConnectionError() {
        // not managed
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // not yet managed
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // not yet managed
    }

}

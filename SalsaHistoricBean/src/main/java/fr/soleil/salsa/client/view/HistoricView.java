package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXTable;
import org.slf4j.Logger;

import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;
import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.api.ScanApi;
import fr.soleil.salsa.api.item.HistoricRecord;
import fr.soleil.salsa.client.view.tool.HistoricEventTableModel;
import fr.soleil.salsa.exception.SalsaDeviceException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;

/**
 * Historic view. Shows the events of a scan.
 * 
 * @deprecated Use {@link HistoryView} instead
 */
@Deprecated
public class HistoricView extends JPanel implements IScanServerListener {

    private static final long serialVersionUID = -4601029610076553719L;

    public static final Logger LOGGER = LoggingUtil.getLogger(HistoricView.class);

    /**
     * Width of the period column.
     */
    private static final int PERIOD_WIDTH = 200;

    /**
     * Width of the type column.
     */
    private static final int TYPE_WIDTH = 60;

    private String scanServerName = null;

    /**
     * The scroll pane.
     */
    private JScrollPane historicTableScrollPane;

    /**
     * The table that contains the displayed data.
     */
    private JXTable historicTable;

    /**
     * The table model for the events table.
     */
    private HistoricEventTableModel historicTableModel;

    /**
     * Button that clears the historic.
     */
    private JButton clearButton;

    public HistoricView() {
        this.initialize();
        DevicePreferences devicePreferences = SalsaAPI.getDevicePreferences();
        // Load the scanServerName
        if (devicePreferences != null) {
            scanServerName = devicePreferences.getScanServer();
            CurrentScanDataModel.addScanServerListener(scanServerName, this);
        }
        refresh();
    }

    /**
     * Initializes the view.
     */
    private void initialize() {
        setLayout(new BorderLayout());

        historicTableModel = new HistoricEventTableModel();

        historicTable = new JXTable();
        historicTable.setColumnControlVisible(true);
        historicTable.setModel(historicTableModel);
        historicTable.setFillsViewportHeight(true);
        historicTable.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
        // historicTable.setEditable(false);
        HistoricTableCellRenderer historicTableRenderer = new HistoricTableCellRenderer();
        historicTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        if (historicTable.getColumnModel().getColumnCount() > 0) {
            historicTable.getColumnModel().getColumn(0).setCellRenderer(historicTableRenderer);
        }
        historicTable.setSortOrder(0, SortOrder.DESCENDING);

        if (historicTable.getColumnModel().getColumnCount() > 1) {
            historicTable.getColumnModel().getColumn(1).setCellRenderer(historicTableRenderer);
        }

        HistoricTableCellRenderer messageRenderer = new HistoricTableCellRenderer();
        messageRenderer.setHorizontalAlignment(SwingConstants.LEFT);
        if (historicTable.getColumnModel().getColumnCount() > 2) {
            historicTable.getColumnModel().getColumn(2).setCellRenderer(messageRenderer);
        }

        historicTableScrollPane = new JScrollPane(historicTable);
        add(historicTableScrollPane, BorderLayout.CENTER);

        clearButton = new JButton();
        clearButton.setText("Clear");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ScanApi.clearHistoric(scanServerName);
                } catch (SalsaDeviceException e1) {
                    LOGGER.error(e1.getMessage());
                    LOGGER.debug("Stack trace", e1);
                }
            }
        });
        add(clearButton, BorderLayout.PAGE_END);

        resetColumnsWidth();
    }

    /**
     * Reset the widths of the columns to their default.
     */
    private void resetColumnsWidth() {
        if (historicTable.getColumnModel().getColumnCount() > 0) {
            historicTable.getColumnModel().getColumn(0).setPreferredWidth(PERIOD_WIDTH);
            historicTable.getColumnModel().getColumn(0).setWidth(PERIOD_WIDTH);
            if (historicTable.getColumnModel().getColumnCount() > 1) {
                historicTable.getColumnModel().getColumn(1).setPreferredWidth(TYPE_WIDTH);
                historicTable.getColumnModel().getColumn(1).setWidth(TYPE_WIDTH);
            }
        }
    }

    /**
     * Adds an event.
     * 
     * @param period
     * @param type
     * @param message
     */
    public void addEvent(String period, String type, String message) {
        historicTableModel.addEvent(period, type, message);
    }

    /**
     * Refreshes the display with the data from the dao.
     * 
     * @param dao
     */
    private void refresh() {
        try {
            List<HistoricRecord> historicRecordsList = ScanApi.getHistoric(scanServerName);
            int size = historicRecordsList.size();
            String[][] historicAsArray = new String[size][3];
            HistoricRecord record;
            for (int index = 0; index < size; ++index) {
                record = historicRecordsList.get(index);
                // historicAsArray[index][0] = periodFormat.format(record.getPeriod());
                historicAsArray[index][0] = record.getPeriod();
                historicAsArray[index][1] = record.getType();
                historicAsArray[index][2] = record.getMessage();
            }
            SwingUtilities.invokeLater(new UpdateDisplayTask(historicAsArray));
        } catch (SalsaDeviceException e) {
            LOGGER.warn(e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Updates the display, called when the dao is changed.
     */
    private class UpdateDisplayTask implements Runnable {

        /**
         * The data from the dao.
         */
        private final String[][] historicAsStringArray;

        /**
         * Constructor
         * 
         * @param historicAsStringArray the data from the dao.
         */
        public UpdateDisplayTask(String[][] historicAsStringArray) {
            this.historicAsStringArray = historicAsStringArray;
        }

        /**
         * The task, to be executed on the EDT.
         */
        @Override
        public void run() {
            int[] selectedRows = historicTable.getSelectedRows();
            historicTableModel.clear();
            if (historicAsStringArray != null) {
                int number = historicAsStringArray.length;
                for (int index = 0; index < number; ++index) {
                    addEvent(historicAsStringArray[index][0], historicAsStringArray[index][1],
                            historicAsStringArray[index][2]);
                }
                for (int row : selectedRows) {
                    if (row < number) {
                        historicTable.getSelectionModel().addSelectionInterval(row, row);
                    }
                }
            }
        }
    }

    @Override
    public void stateChanged(String state) {
        refresh();
    }

}

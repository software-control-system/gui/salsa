package fr.soleil.salsa.client.view.tool;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 * The table model for the events table.
 * 
 * @see TableModel
 */
@Deprecated
public class HistoricEventTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 4937669864951919510L;

    /**
     * The column names.
     */
    private String[] columnNames = { "Period", "Type", "Message" };

    /**
     * The list of events.
     */
    private List<HistoricEvent> eventsList;

    /**
     * Constructor.
     */
    public HistoricEventTableModel() {
        this.eventsList = new ArrayList<HistoricEvent>();
    }

    /**
     * Returns the number of columns.
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Returns the number of lines.
     */
    @Override
    public int getRowCount() {
        return eventsList.size();
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and <code>rowIndex</code>.
     * 
     * @param rowIndex the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        ;
        HistoricEvent event = eventsList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                value = event.getPeriod();
                break;
            case 1:
                value = event.getType();
                break;
            case 2:
                value = event.getMessage();
                break;
        }
        return value;
    }

    /**
     * Return the name of the column #columnIndex (starting at 0).
     */
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    /**
     * Clears the content.
     */
    public void clear() {
        if (eventsList.size() > 0) {
            int lastRow = eventsList.size() - 1;
            eventsList.clear();
            fireTableRowsDeleted(0, lastRow);
        }
    }

    /**
     * Adds an event.
     * 
     * @param event
     */
    public void addEvent(HistoricEvent event) {
        int row = eventsList.size();
        eventsList.add(event);
        fireTableRowsInserted(row, row);
    }

    /**
     * Adds an event.
     * 
     * @param period
     * @param type
     * @param message
     */
    public void addEvent(String period, String type, String message) {
        HistoricEvent event = new HistoricEvent();
        event.setPeriod(period);
        event.setType(type);
        event.setMessage(message);
        addEvent(event);
    }
}
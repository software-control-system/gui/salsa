package fr.soleil.salsa.client.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXTable;
import org.slf4j.Logger;

import fr.soleil.lib.project.swing.ITextTransferable;
import fr.soleil.lib.project.swing.TextTansfertHandler;
import fr.soleil.salsa.client.controller.IHistoricLogController;
import fr.soleil.salsa.client.preferences.UIPreferences;
import fr.soleil.salsa.client.preferences.UIPreferencesEvent;
import fr.soleil.salsa.client.preferences.UIPreferencesEvent.UIPreferenceEventType;
import fr.soleil.salsa.client.preferences.UIPreferencesListener;
import fr.soleil.salsa.client.util.CellNoModifiableEditor;
import fr.soleil.salsa.client.util.CellToolTypeRenderer;
import fr.soleil.salsa.client.util.TrajectoryCellRenderer;
import fr.soleil.salsa.client.view.tool.CellHistoricEventEditor;
import fr.soleil.salsa.client.view.tool.CellHistoricEventRenderer;
import fr.soleil.salsa.client.view.tool.CellNexusFileEditor;
import fr.soleil.salsa.client.view.tool.CellNexusFileRenderer;
import fr.soleil.salsa.client.view.tool.HistoricLogEventTableModel;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.impl.HistoricLogLine;
import fr.soleil.salsa.logging.LoggingUtil;

/**
 * Historic Log view. Shows the logs of scan.
 * 
 */
public class HistoricLogView extends JPanel
        implements IView<IHistoricLogController>, ITextTransferable, UIPreferencesListener {

    private static final long serialVersionUID = 4384479108705812012L;

    public static final Logger LOGGER = LoggingUtil.getLogger(HistoricLogView.class);

    /**
     * Width of the date column.
     */
    private static final int DATE_WIDTH = 150;

    /**
     * Width of the action column.
     */
    private static final int ACTION_WIDTH = 40;

    /**
     * Updates the display.
     */
    private class UpdateDisplayTask implements Runnable {

        /**
         * The Historic Log Array.
         */
        private final ArrayList<HistoricLogLine> historicLogArray;

        /**
         * Constructor
         * 
         * @param historicLogArray
         */

        public UpdateDisplayTask(ArrayList<HistoricLogLine> historicLogArray) {
            this.historicLogArray = historicLogArray;
        }

        @Override
        public void run() {
            int[] selectedRows = historicLogTable.getSelectedRows();
            historicLogTableModel.clear();
            if (historicLogArray != null) {
                for (int index = 0; index < historicLogArray.size(); ++index) {
                    HistoricLogLine hll = historicLogArray.get(index);
                    addEvent(hll.getScanId(), hll.getDate(), hll.getAction(), hll.getScan(), hll.getNexusFile(),
                            hll.getNxEntry(), hll.getTrajectories());
                }
                for (int row : selectedRows) {
                    if (row < historicLogArray.size()) {
                        historicLogTable.getSelectionModel().addSelectionInterval(row, row);
                    }
                }
            }
        }
    }

    private final JPanel commandPanel = new JPanel();
    /**
     * The controller;
     */
    private IHistoricLogController controller;

    /**
     * The scroll pane.
     */
    private JScrollPane scrollPane;

    /**
     * The table that contains the displayed data.
     */
    private JXTable historicLogTable;

    /**
     * The table model for the log events table.
     */
    private HistoricLogEventTableModel historicLogTableModel;

    /**
     * Button that clears the historic log.
     */
    private JButton clearButton;

    /**
     * Button that refresh the historic log.
     */
    private JButton refreshButton;

    /**
     * Button that prints the historic log.
     */
    private JButton printButton;

    private final CellNexusFileEditor nexusCellEditor = new CellNexusFileEditor();
    private final CellNexusFileRenderer nexusCellRenderer = new CellNexusFileRenderer();

    /**
     * Constructor
     */
    public HistoricLogView() {
        this(null);
    }

    /**
     * Constructor.
     * 
     * @param controller
     */
    public HistoricLogView(IHistoricLogController controller) {
        this.controller = controller;
        this.initialize();
    }

    /**
     * Initializes the view.
     */
    private void initialize() {
        setLayout(new BorderLayout());

        historicLogTableModel = new HistoricLogEventTableModel();

        historicLogTable = new JXTable();

        TextTansfertHandler.registerTransferHandler(this, historicLogTable);

        historicLogTable.setColumnControlVisible(true);
        historicLogTable.setModel(historicLogTableModel);
        historicLogTable.setAutoResizeMode(JXTable.AUTO_RESIZE_ALL_COLUMNS);

        historicLogTable.getColumnModel().getColumn(0).setMinWidth(DATE_WIDTH);
        historicLogTable.getColumnModel().getColumn(0).setMaxWidth(DATE_WIDTH);
        historicLogTable.getColumnModel().getColumn(0).setResizable(false);

        historicLogTable.getColumnModel().getColumn(1).setMinWidth(ACTION_WIDTH);
        historicLogTable.getColumnModel().getColumn(1).setMaxWidth(ACTION_WIDTH);
        historicLogTable.getColumnModel().getColumn(1).setResizable(false);
        historicLogTable.getColumnModel().getColumn(1).setCellRenderer(new CellHistoricEventRenderer());
        historicLogTable.getColumnModel().getColumn(1).setCellEditor(new CellHistoricEventEditor(this));

        historicLogTable.getColumnModel().getColumn(4).setCellEditor(new CellNoModifiableEditor(new JTextField()));

        historicLogTable.getColumnModel().getColumn(5).setCellRenderer(new TrajectoryCellRenderer());
        historicLogTable.setAutoscrolls(true);

        CellToolTypeRenderer cellToolTypeRenderer = new CellToolTypeRenderer();
        cellToolTypeRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        historicLogTable.getColumnModel().getColumn(3).setCellRenderer(cellToolTypeRenderer);

        // historicLogTable.getColumnModel().getColumn(3)
        // .setCellEditor(new CellNoModifiableEditor(new
        // JTextField(SwingConstants.RIGHT)));
        historicLogTable.getColumnModel().getColumn(3).setCellRenderer(nexusCellRenderer);
        historicLogTable.getColumnModel().getColumn(3).setCellEditor(nexusCellEditor);

        historicLogTable.setSortOrder(0, SortOrder.DESCENDING);

        historicLogTable.setPreferredScrollableViewportSize(historicLogTable.getPreferredSize());
        scrollPane = new JScrollPane(historicLogTable);
        add(scrollPane, BorderLayout.CENTER);

        refreshButton = new JButton("Refresh");
        refreshButton.setToolTipText("Refresh logs");
        refreshButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (controller != null) {
                        controller.refresh();
                    }
                } catch (Exception e1) {
                    LOGGER.warn("Cannot refresh historic log view {}", e1.getMessage());
                    LOGGER.debug("Stack trace", e1);
                }
            }
        });
        commandPanel.add(refreshButton);

        clearButton = new JButton("Clear");
        clearButton.setToolTipText("Clear all logs");
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (controller != null) {
                        controller.notifyClear();
                    }
                } catch (Exception e1) {
                    LOGGER.warn("Cannot clear historic log view {}", e1.getMessage());
                    LOGGER.debug("Stack trace", e1);
                }
            }
        });

        commandPanel.add(clearButton);

        printButton = new JButton("Print");
        printButton.setToolTipText("Print logs");
        printButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    historicLogTable.print();
                } catch (Exception PrintException) {
                }
            }
        });
        commandPanel.add(printButton);

        add(commandPanel, BorderLayout.SOUTH);
        setBatch(UIPreferences.getInstance().getDataBrowser());

        UIPreferences.getInstance().addPreferenceListener(this);
    }

    @Override
    public IHistoricLogController getController() {
        return controller;
    }

    @Override
    public void setController(IHistoricLogController controller) {
        this.controller = controller;
    }

    /**
     * Adds an event.
     * 
     * @param date
     * @param action
     * @param name
     * @param nexus
     * @param trajectory
     */
    public void addEvent(String id, Date date, String action, String name, String nexus, String nxEntry,
            Collection<ITrajectory> trajectoriesList) {
        historicLogTableModel.addEvent(id, date, action, name, nexus, nxEntry, trajectoriesList);
    }

    public void refresh(ArrayList<HistoricLogLine> historicLogArray) {
        SwingUtilities.invokeLater(new UpdateDisplayTask(historicLogArray));
    }

    private void setBatch(String batch) {
        nexusCellEditor.setBatch(batch);
    }

    public void setScanServerName(String scanServerName) {
        nexusCellEditor.setTangoDevice(scanServerName);
    }

    @Override
    public String getTextToTransfert() {
        String textToTransfert = null;
        if (historicLogTable != null) {
            int row = historicLogTable.getSelectedRow();
            int nbColumn = historicLogTable.getColumnCount();
            if ((row > -1) && (nbColumn > 3)) {
                Object value = historicLogTable.getValueAt(row, 3);
                if (value != null) {
                    textToTransfert = value.toString();
                }
            }
        }
        return textToTransfert;
    }

    @Override
    public void preferenceChanged(UIPreferencesEvent event) {
        if ((event != null) && (event.getType() == UIPreferenceEventType.DATABROWSER_CHANGED)) {
            setBatch(UIPreferences.getInstance().getDataBrowser());
        }

    }
}

package fr.soleil.salsa.client.view.tool;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CellNexusFileRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -4509582077105383078L;

    public CellNexusFileRenderer() {
        super();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                row, column);
        Color color = comp.getBackground();
        if (value != null) {
            comp = new NexusFileButton();
            ((NexusFileButton) comp).setFullPath(value.toString());
            ((NexusFileButton) comp).setOpaque(isSelected);
            comp.setBackground(color);
        }

        return comp;
    }
}

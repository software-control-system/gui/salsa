package fr.soleil.salsa.client.view.tool;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.slf4j.Logger;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.tool.SalsaUtils;

public class NexusFileButton extends JTextField {

    private static final long serialVersionUID = 2480989558066266393L;

    public static final Logger LOGGER = LoggingUtil.getLogger(NexusFileButton.class);

    private String batch = null;
    private static final BatchExecutor batchExecutor = new BatchExecutor();
    private String fullPath = null;
    private String tangoDevice = null;

    public NexusFileButton() {
        super(SwingConstants.RIGHT);
        setEditable(false);
        setOpaque(false);
    }

    public void setFullPath(String arg0) {
        fullPath = arg0;
        if (SalsaUtils.isDefined(fullPath)) {
            int index = fullPath.lastIndexOf(File.separator);
            if (index > 0) {
                String fileName = fullPath.substring(index + 1, fullPath.length());
                setText(fileName);
            }
            // Test with the other separator
            else {
                String otherSpearator = "/";
                if (File.separator.equals("/")) {
                    otherSpearator = "\\";
                }
                index = fullPath.lastIndexOf(otherSpearator);
                if (index > 0) {
                    String fileName = fullPath.substring(index + 1, fullPath.length());
                    setText(fileName);
                } else {
                    setText(fullPath);
                }
            }
            setToolTipText(fullPath);
        }
    }

    public void setBatch(String abatch) {
        if (SalsaUtils.isDefined(abatch)) {
            batch = abatch.trim();
            batchExecutor.setBatch(batch);
        }
    }

    public void setTangoDevice(String tangoDevice) {
        this.tangoDevice = tangoDevice;
    }

    public void actionPerformed() {
        if (SalsaUtils.isDefined(batch)) {
            List<String> parameter = new ArrayList<String>();
            if (SalsaUtils.isDefined(fullPath)) {
                File nexusFile = new File(fullPath);
                if (nexusFile.isFile() && nexusFile.exists() && nexusFile.canRead()) {
                    parameter.add(fullPath);
                }
            }

            boolean isDataBrowser = batch.toLowerCase().indexOf("databrowser") > -1;
            if (isDataBrowser) {
                if (TangoDeviceHelper.isDeviceRunning(tangoDevice)) {
                    parameter.add(tangoDevice);
                }
            }

            if (SalsaUtils.isFulfilled(parameter)) {
                batchExecutor.setBatchParameters(parameter);
            }

            try {
                LOGGER.trace("Execute {} {}", batch, parameter);
                batchExecutor.execute();

            } catch (Exception e) {
                LOGGER.trace("Cannot execute {} {} {}", batch, parameter, e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
    }
}

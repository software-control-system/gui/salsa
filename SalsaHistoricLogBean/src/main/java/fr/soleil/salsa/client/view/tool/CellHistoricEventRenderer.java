package fr.soleil.salsa.client.view.tool;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import fr.soleil.salsa.client.view.ActionButton;
import fr.soleil.salsa.client.view.ActionButton.ACTION_TYPE;

public class CellHistoricEventRenderer extends DefaultTableCellRenderer implements
        TableCellRenderer {

    private static final long serialVersionUID = -4509582077105383078L;

    public CellHistoricEventRenderer() {
        super();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        if (value != null && value instanceof HistoricLogEvent) {
            HistoricLogEvent event = (HistoricLogEvent) value;
            ActionButton component = new ActionButton(ACTION_TYPE.NONE);
            String action = event.getAction();
            component.setAction(action);
            component.setToolTipText(action);
            return component;

        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    }
}

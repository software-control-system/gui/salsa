package fr.soleil.salsa.client.view.tool;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import fr.soleil.salsa.entity.ITrajectory;

public class HistoricLogEvent implements Serializable {

    private static final long serialVersionUID = -7589610736144256004L;

    /**
     * The date.
     */
    private Date date;

    /**
     * The action.
     */
    private String action;

    /**
     * The name.
     */
    private String name;

    /**
     * The id.
     */
    private String id;

    /**
     * The nexus.
     */
    private String nexus;

    /**
     * The nxEntry.
     */
    private String nxEntry;

    /**
     * The trajectory.
     */
    private Collection<ITrajectory> trajectories;

    /**
     * The date.
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * The date.
     */
    public Date getDate() {
        return date;
    }

    /**
     * The action.
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * The action.
     */
    public String getAction() {
        return action;
    }

    /**
     * The name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The name.
     */
    public String getName() {
        return name;
    }

    /**
     * The Id.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * The id.
     */
    public String getId() {
        return id;
    }

    /**
     * The nexus.
     */
    public void setNexus(String nexus) {
        this.nexus = nexus;
    }

    /**
     * The nexus.
     */
    public String getNxEntry() {
        return nxEntry;
    }

    /**
     * The nexus.
     */
    public void setNxEntry(String nxentry) {
        this.nxEntry = nxentry;
    }

    /**
     * The nexus.
     */
    public String getNexus() {
        return nexus;
    }

    /**
     * The trajectories.
     */
    public void setTrajectories(Collection<ITrajectory> trajectories) {
        this.trajectories = trajectories;
    }

    /**
     * The trajectories.
     */
    public Collection<ITrajectory> getTrajectories() {
        return trajectories;
    }

}

package fr.soleil.salsa.client.controller;

import fr.soleil.salsa.client.view.HistoricLogView;
import fr.soleil.salsa.exception.SalsaException;

/**
 * The interface of a controller for the historic log view that shows the logs of a scan.
 */
public interface IHistoricLogController extends IController<HistoricLogView> {

    /**
     * Called when the clear button is pressed.
     * 
     * @throws Exception
     */
    public void notifyClear() throws SalsaException;

    /**
     * Called when the view needs to be refresh.
     * 
     * @throws Exception
     */
    public void refresh() throws SalsaException;

}

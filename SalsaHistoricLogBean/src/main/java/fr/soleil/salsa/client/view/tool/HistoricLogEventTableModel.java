package fr.soleil.salsa.client.view.tool;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.service.ILogService;

/**
 * The table model for the events table.
 * 
 * @see TableModel
 */
public class HistoricLogEventTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -7357725011282690008L;

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");

    /**
     * The column names.
     */
    private final String[] columnNames = { "Date", "Action", "Name", "Nexus", "NxEntry", "Trajectory" };

    /**
     * The list of events.
     */
    private final List<HistoricLogEvent> eventsList;

    /**
     * Constructor.
     */
    public HistoricLogEventTableModel() {
        this.eventsList = new ArrayList<HistoricLogEvent>();
    }

    /**
     * Returns the number of columns.
     */

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Returns the number of lines.
     */

    @Override
    public int getRowCount() {
        return eventsList.size();
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and <code>rowIndex</code>.
     * 
     * @param rowIndex the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        HistoricLogEvent event = eventsList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                Date date = event.getDate();
                value = simpleDateFormat.format(date);
                break;
            case 1:
                value = event;
                break;
            case 2:
                value = event.getName();
                break;
            case 3:
                value = event.getNexus();
                break;
            case 4:
                value = event.getNxEntry();
                break;
            case 5:
                Collection<ITrajectory> trajectoryList = event.getTrajectories();
                value = getTrajectoryListToString(trajectoryList);
        }
        return value;
    }

    public static String getTrajectoryListToString(Collection<ITrajectory> trajectoryList) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean first = true;
        for (ITrajectory trajectory : trajectoryList) {
            if (first) {
                stringBuilder.append(ObjectUtils.NEW_LINE);
                first = false;
            }
            stringBuilder.append(getTrajectoryToString(trajectory));
        }
        return stringBuilder.toString();
    }

    private static String getTrajectoryToString(ITrajectory trajectory) {
        StringBuilder stringBuilder = new StringBuilder();
        if (trajectory != null) {
            stringBuilder.append(trajectory.getName());
            if (trajectory instanceof ITrajectoryK) {
                ITrajectoryK trajectoryK = (ITrajectoryK) trajectory;
                stringBuilder.append(" e0 ");
                stringBuilder.append(trajectoryK.getE0());
                stringBuilder.append(" e1 ");
                stringBuilder.append(trajectoryK.getE1());
                stringBuilder.append(" e2 ");
                stringBuilder.append(trajectoryK.getE2());
                stringBuilder.append(" edeltaedge ");
                stringBuilder.append(trajectoryK.getEDeltaEdge());
                stringBuilder.append(" edeltapreEdge ");
                stringBuilder.append(trajectoryK.getEDeltaPreEdge());
                stringBuilder.append(" emin ");
                stringBuilder.append(trajectoryK.getEMin());
                stringBuilder.append(" kdelta ");
                stringBuilder.append(trajectoryK.getKDelta());
                stringBuilder.append(" kmax ");
                stringBuilder.append(trajectoryK.getKMax());
                stringBuilder.append(" kmin ");
                stringBuilder.append(trajectoryK.getKMin());
                stringBuilder.append(" m ");
                stringBuilder.append(trajectoryK.getM());
                stringBuilder.append(" n ");
                stringBuilder.append(trajectoryK.getN());
                stringBuilder.append(" deadtime ");
                stringBuilder.append(trajectoryK.getDeadTime());
            } else {
                stringBuilder.append(" from ");
                stringBuilder.append(trajectory.getBeginPosition());
                stringBuilder.append(" to ");
                stringBuilder.append(trajectory.getEndPosition());
                stringBuilder.append(" delta ");
                stringBuilder.append(trajectory.getDelta());
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        switch (col) {
            case 1:
                Object value = getValueAt(row, col);
                if ((value != null) && (value instanceof HistoricLogEvent)
                        && ((HistoricLogEvent) value).getAction().equalsIgnoreCase(ILogService.START_ACTION)) {
                    return true;
                }
                return false;
            case 3:
                return true;
            case 4:
                return true;
            default:
                return false;
        }
    }

    /**
     * Return the name of the column #columnIndex (starting at 0).
     */
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    /**
     * Clears the content.
     */
    public void clear() {
        if (eventsList.size() > 0) {
            int lastRow = eventsList.size() - 1;
            eventsList.clear();
            fireTableRowsDeleted(0, lastRow);
        }
    }

    /**
     * Adds an event.
     * 
     * @param event
     */
    public void addEvent(HistoricLogEvent event) {
        int row = eventsList.size();
        eventsList.add(event);
        fireTableRowsInserted(row, row);
    }

    /**
     * Adds an event.
     * 
     * @param date
     * @param action
     * @param name
     * @param nexus
     * @param trajectory
     */
    public void addEvent(String id, Date date, String action, String name, String nexus, String nxEntry,
            Collection<ITrajectory> trajectoriesList) {
        HistoricLogEvent event = new HistoricLogEvent();
        event.setId(id);
        event.setDate(date);
        event.setAction(action);
        event.setName(name);
        event.setNexus(nexus);
        event.setNxEntry(nxEntry);
        event.setTrajectories(trajectoriesList);
        addEvent(event);
    }
}
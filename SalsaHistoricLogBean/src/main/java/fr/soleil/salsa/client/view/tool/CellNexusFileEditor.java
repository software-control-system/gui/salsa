package fr.soleil.salsa.client.view.tool;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;

public class CellNexusFileEditor extends DefaultCellEditor {

    private static final long serialVersionUID = -4509582077105383078L;

    private String batch = null;
    private String tangoDevice = null;

    public CellNexusFileEditor() {
        super(new NexusFileButton());
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
            int row, int column) {
        NexusFileButton comp = (NexusFileButton) super.getTableCellEditorComponent(table, value,
                isSelected, row, column);
        comp.setFullPath(value.toString());
        comp.setTangoDevice(tangoDevice);
        comp.setBatch(batch);
        comp.actionPerformed();
        return comp;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;

    }

    public String getTangoDevice() {
        return tangoDevice;
    }

    public void setTangoDevice(String tangoDevice) {
        this.tangoDevice = tangoDevice;
    }

}

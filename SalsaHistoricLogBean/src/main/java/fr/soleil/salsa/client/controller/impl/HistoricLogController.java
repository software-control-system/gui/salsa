package fr.soleil.salsa.client.controller.impl;

import org.slf4j.Logger;

import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;
import fr.soleil.salsa.api.ScanApi;
import fr.soleil.salsa.client.controller.AController;
import fr.soleil.salsa.client.controller.IHistoricLogController;
import fr.soleil.salsa.client.view.HistoricLogView;
import fr.soleil.salsa.entity.impl.ContextImpl;
import fr.soleil.salsa.exception.SalsaException;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.preferences.DevicePreferences;
import fr.soleil.salsa.tool.SalsaUtils;

/**
 * Controller for the historic view.
 */
public class HistoricLogController extends AController<HistoricLogView>
        implements IHistoricLogController, IScanServerListener {

    public static final Logger LOGGER = LoggingUtil.getLogger(HistoricLogController.class);

    private String oldDate = null;
    private String oldState = null;

    /**
     * The historic log controller.
     */
    public HistoricLogController() {
        this(null);
    }

    /**
     * The historic log controller.
     */
    public HistoricLogController(HistoricLogView view) {
        super(view);
    }

    @Override
    public void setView(HistoricLogView view) {
        super.setView(view);
        refresh();
    }

    @Override
    protected HistoricLogView generateView() {
        return new HistoricLogView(this);
    }

    @Override
    public void setDevicePreferences(DevicePreferences preferences) {
        if (getDevicePreferences() != null) {
            String scanServerName = getDevicePreferences().getScanServer();
            CurrentScanDataModel.removeScanServerListener(scanServerName, this);
        }
        super.setDevicePreferences(preferences);
        if (preferences != null) {
            String scanServerName = preferences.getScanServer();
            CurrentScanDataModel.addScanServerListener(scanServerName, this);
            if (view != null) {
                view.setScanServerName(scanServerName);
            }
        }
    }

    /**
     * Called when the clear button is pressed.
     * 
     * @throws Exception
     */
    @Override
    public void notifyClear() throws SalsaException {

        ContextImpl context = new ContextImpl();
        if (getDevicePreferences() != null) {
            context.setMaxLineNumber(devicePreferences.getMaxLineNumber());
            context.setUserLogFile(devicePreferences.getUserLogFile());
        }
        if (SalsaUtils.isDefined(context.getUserLogFile())) {
            ScanApi.clearHistoricLog(context);
            refresh();
        }
    }

    @Override
    public void refresh() {
        ContextImpl context = new ContextImpl();
        if (getDevicePreferences() != null) {
            context.setMaxLineNumber(devicePreferences.getMaxLineNumber());
            context.setUserLogFile(devicePreferences.getUserLogFile());
        }
        if (SalsaUtils.isDefined(context.getUserLogFile())) {
            try {
                if (view != null) {
                    view.refresh(ScanApi.loadHistoricLog(context));
                }
            } catch (Exception e) {
                LOGGER.warn("Cannot refresh Historic log view {}", e.getMessage());
                LOGGER.trace("Cannot refresh Historic log view", e);
            }
        }
    }

    @Override
    public void stateChanged(String state) {
        if (getDevicePreferences() != null) {
            String scanServerName = getDevicePreferences().getScanServer();
            if (CurrentScanDataModel.dateChanged(scanServerName, oldDate)) {
                oldDate = CurrentScanDataModel.getStartDate(scanServerName);
                refresh();
            } else {
                String currentState = CurrentScanDataModel.getState(scanServerName);
                if ((oldState == null) || ((currentState != null) && !oldState.equalsIgnoreCase(currentState))) {
                    oldState = currentState;
                    refresh();
                }
            }
        }
    }
}

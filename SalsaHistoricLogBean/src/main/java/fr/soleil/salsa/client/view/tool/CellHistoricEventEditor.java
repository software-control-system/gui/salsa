package fr.soleil.salsa.client.view.tool;

import java.awt.Component;
import java.util.Collection;
import java.util.EventObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import org.slf4j.Logger;

import fr.soleil.salsa.api.SalsaAPI;
import fr.soleil.salsa.client.view.ActionButton;
import fr.soleil.salsa.client.view.HistoricLogView;
import fr.soleil.salsa.entity.IActuator;
import fr.soleil.salsa.entity.IConfig;
import fr.soleil.salsa.entity.IRange;
import fr.soleil.salsa.entity.ITrajectory;
import fr.soleil.salsa.entity.scan1d.IConfig1D;
import fr.soleil.salsa.entity.scan1d.IDimension1D;
import fr.soleil.salsa.entity.scan1d.IRange1D;
import fr.soleil.salsa.entity.scan2D.IConfig2D;
import fr.soleil.salsa.entity.scan2D.IDimension2DX;
import fr.soleil.salsa.entity.scan2D.IDimension2DY;
import fr.soleil.salsa.entity.scan2D.IRange2DX;
import fr.soleil.salsa.entity.scan2D.IRange2DY;
import fr.soleil.salsa.entity.scanenergy.IConfigEnergy;
import fr.soleil.salsa.entity.scanenergy.IDimensionEnergy;
import fr.soleil.salsa.entity.scanenergy.IRangeEnergy;
import fr.soleil.salsa.entity.scanhcs.IConfigHCS;
import fr.soleil.salsa.entity.scanhcs.IDimensionHCS;
import fr.soleil.salsa.entity.scanhcs.IRangeHCS;
import fr.soleil.salsa.entity.scank.IConfigK;
import fr.soleil.salsa.entity.scank.IDimensionK;
import fr.soleil.salsa.entity.scank.IRangeK;
import fr.soleil.salsa.entity.scank.ITrajectoryK;
import fr.soleil.salsa.logging.LoggingUtil;
import fr.soleil.salsa.service.ILogService;
import fr.soleil.salsa.tool.SalsaUtils;

public class CellHistoricEventEditor extends ActionButton implements TableCellEditor {

    private static final long serialVersionUID = 3936978388823914011L;

    public static final Logger LOGGER = LoggingUtil.getLogger(CellHistoricEventEditor.class);

    private static SalsaAPI salsaAPI = null;
    private static HistoricLogView view = null;

    public CellHistoricEventEditor(HistoricLogView view) {
        super(ACTION_TYPE.NONE);
        CellHistoricEventEditor.view = view;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        if ((value != null) && (value instanceof HistoricLogEvent)) {
            HistoricLogEvent event = (HistoricLogEvent) value;
            String action = event.getAction();
            setAction(action);
            setToolTipText(action);
            notifyHistoricEventSelected(event);
        }
        return this;
    }

    @Override
    public void cancelCellEditing() {
    }

    @Override
    public boolean stopCellEditing() {
        return true;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
    }

    protected void fireCellEditing(ChangeEvent e) {

    }

    private void notifyHistoricEventSelected(HistoricLogEvent event) {
        if (event != null) {
            String action = event.getAction();
            if (action.equalsIgnoreCase(ILogService.START_ACTION)) {

                int result = JOptionPane.OK_OPTION;
                String message = "Start " + event.getName() + " with this following trajectories :\n"
                        + HistoricLogEventTableModel.getTrajectoryListToString(event.getTrajectories());
                result = JOptionPane.showConfirmDialog(null, message, "Confirmation", JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.OK_OPTION) {
                    String id = event.getId();
                    try {
                        Integer idInt = Integer.valueOf(id);
                        IConfig<?> config = SalsaAPI.getConfigById(idInt, false);
                        if (config != null) {

                            // Sort trajectory by name
                            Map<String, LinkedList<ITrajectory>> actuatorTrajectoryMap = new HashMap<String, LinkedList<ITrajectory>>();
                            Collection<ITrajectory> flatTrajectoryList = event.getTrajectories();
                            if (SalsaUtils.isFulfilled(flatTrajectoryList)) {
                                LinkedList<ITrajectory> trajectoryList = null;
                                for (ITrajectory tmpTrajectory : flatTrajectoryList) {
                                    if (tmpTrajectory != null) {
                                        String name = tmpTrajectory.getName();
                                        if (name != null) {
                                            if (actuatorTrajectoryMap.containsKey(name.toLowerCase())) {
                                                trajectoryList = actuatorTrajectoryMap.get(name.toLowerCase());
                                            } else {
                                                trajectoryList = new LinkedList<ITrajectory>();
                                                actuatorTrajectoryMap.put(name.toLowerCase(), trajectoryList);
                                            }
                                            trajectoryList.offer(tmpTrajectory);
                                        }
                                    }
                                }
                            }

                            if (SalsaUtils.isFulfilled(actuatorTrajectoryMap)) {
                                switch (config.getType()) {

                                    case SCAN_1D:
                                        IConfig1D config1d = (IConfig1D) config;
                                        IDimension1D dimensionX = config1d.getDimensionX();
                                        List<IActuator> actuatorList = dimensionX.getActuatorsList();
                                        List<IRange1D> range1DList = dimensionX.getRangesXList();

                                        for (int rangeid = 0; rangeid < range1DList.size(); rangeid++) {
                                            modifyTrajectory(range1DList.get(rangeid), actuatorList,
                                                    actuatorTrajectoryMap);
                                        }
                                        break;

                                    case SCAN_2D:
                                        IConfig2D config2d = (IConfig2D) config;
                                        IDimension2DX dimension2DX = config2d.getDimensionX();
                                        actuatorList = dimension2DX.getActuatorsList();
                                        List<IRange2DX> range2DXList = dimension2DX.getRangesList();

                                        for (int rangeid = 0; rangeid < range2DXList.size(); rangeid++) {
                                            modifyTrajectory(range2DXList.get(rangeid), actuatorList,
                                                    actuatorTrajectoryMap);
                                        }

                                        IDimension2DY dimension2DY = config2d.getDimensionY();
                                        actuatorList = dimension2DY.getActuatorsList();
                                        List<IRange2DY> range2DYList = dimension2DY.getRangesList();

                                        for (int rangeid = 0; rangeid < range2DYList.size(); rangeid++) {
                                            modifyTrajectory(range2DYList.get(rangeid), actuatorList,
                                                    actuatorTrajectoryMap);
                                        }

                                        break;

                                    case SCAN_HCS:
                                        IConfigHCS configHCS = (IConfigHCS) config;
                                        IDimensionHCS dimensionHCS = configHCS.getDimensionX();
                                        actuatorList = dimensionHCS.getActuatorsList();
                                        List<IRangeHCS> rangeListHCS = dimensionHCS.getRangesXList();

                                        for (int rangeid = 0; rangeid < rangeListHCS.size(); rangeid++) {
                                            modifyTrajectory(rangeListHCS.get(rangeid), actuatorList,
                                                    actuatorTrajectoryMap);
                                        }
                                        break;

                                    case SCAN_K:
                                        IConfigK configK = (IConfigK) config;
                                        IDimensionK dimensionK = configK.getDimensionX();
                                        IRangeK tmpRangeK = dimensionK.getRangeX();
                                        actuatorList = dimensionK.getActuatorsList();
                                        modifyTrajectory(tmpRangeK, actuatorList, actuatorTrajectoryMap);
                                        break;

                                    case SCAN_ENERGY:
                                        IConfigEnergy configE = (IConfigEnergy) config;
                                        IDimensionEnergy dimensionE = configE.getDimensionX();
                                        actuatorList = dimensionE.getActuatorsList();
                                        List<IRangeEnergy> rangeListE = dimensionE.getRangesEnergyList();

                                        for (int rangeid = 0; rangeid < rangeListE.size(); rangeid++) {
                                            modifyTrajectory(rangeListE.get(rangeid), actuatorList,
                                                    actuatorTrajectoryMap);
                                        }
                                        break;

                                    default:
                                        // nothing to do for other cases
                                        break;
                                }// end switch
                            } // if actuatorTrajectoryMap is not empty

                            if (salsaAPI == null) {
                                salsaAPI = new SalsaAPI();
                            }

                            salsaAPI.startScan(config);
                            if ((view != null) && (view.getController() != null)) {
                                view.getController().refresh();
                            }
                        } // if config not null
                    } // end try
                    catch (Exception e) {
                        LOGGER.error(e.getMessage());
                        LOGGER.debug("Stack trace", e);
                    }
                } // end if (result == JOptionPane.OK_OPTION)
            } // If start action
        } // If event not null
    }

    private void modifyTrajectory(List<ITrajectory> trajectoryList, Map<String, LinkedList<ITrajectory>> trajectoryMap,
            List<IActuator> listActuator) {

        if ((trajectoryList != null) && (listActuator != null) && (trajectoryList.size() == listActuator.size())) {

            ITrajectory readTrajectory = null;
            ITrajectory newTrajectory = null;
            IActuator actuator = null;
            String actuatorKey = null;
            LinkedList<ITrajectory> linkedTrajectoryList = null;
            for (int i = 0; i < trajectoryList.size(); i++) {
                actuator = listActuator.get(i);
                readTrajectory = trajectoryList.get(i);
                if ((actuator != null) && actuator.isEnabled() && (actuator.getName() != null)) {
                    actuatorKey = actuator.getName().toLowerCase();
                    if (trajectoryMap.containsKey(actuatorKey)) {
                        linkedTrajectoryList = trajectoryMap.get(actuatorKey);
                        newTrajectory = linkedTrajectoryList.poll();
                        modifyTrajectory(newTrajectory, readTrajectory);
                    }
                }
            }
        }
    }

    private void modifyTrajectory(ITrajectory source, ITrajectory dest) {
        if ((source != null) && (dest != null)) {
            dest.setRelative(false);
            if ((source instanceof ITrajectoryK) && (dest instanceof ITrajectoryK)) {
                ITrajectoryK trajectoryKdest = (ITrajectoryK) dest;
                ITrajectoryK trajectoryKsource = (ITrajectoryK) source;
                trajectoryKdest.setE0(trajectoryKsource.getE0());
                trajectoryKdest.setE1(trajectoryKsource.getE1());
                trajectoryKdest.setE2(trajectoryKsource.getE2());
                trajectoryKdest.setEDeltaEdge(trajectoryKsource.getEDeltaEdge());
                trajectoryKdest.setEDeltaPreEdge(trajectoryKsource.getEDeltaPreEdge());
                trajectoryKdest.setEMin(trajectoryKsource.getEMin());
                trajectoryKdest.setKDelta(trajectoryKsource.getKDelta());
                trajectoryKdest.setKMax(trajectoryKsource.getKMax());
                trajectoryKdest.setKMin(trajectoryKsource.getKMin());
                trajectoryKdest.setM(trajectoryKsource.getM());
                trajectoryKdest.setN(trajectoryKsource.getN());
                trajectoryKdest.setDeadTime(trajectoryKsource.getDeadTime());
            } else {
                dest.setBeginPosition(source.getBeginPosition());
                dest.setEndPosition(source.getEndPosition());
                dest.setDelta(source.getDelta());
            }
        }
    }

    private void modifyTrajectory(IRange range, List<IActuator> listActuator,
            Map<String, LinkedList<ITrajectory>> trajectoryMap) {
        if (range != null) {
            List<ITrajectory> readlistTrajectory = range.getTrajectoriesList();
            modifyTrajectory(readlistTrajectory, trajectoryMap, listActuator);

        }
    }

}
